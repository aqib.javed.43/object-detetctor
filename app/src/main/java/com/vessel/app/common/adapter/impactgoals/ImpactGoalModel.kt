package com.vessel.app.common.adapter.impactgoals

import androidx.annotation.DrawableRes

data class ImpactGoalModel(
    val title: String,
    val icon: String?,
    @DrawableRes val background: Int
)
