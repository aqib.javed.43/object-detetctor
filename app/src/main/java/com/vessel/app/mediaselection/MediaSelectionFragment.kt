package com.vessel.app.mediaselection

import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.helper.Album
import com.vessel.app.helper.MimeType
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_media_selection.*

@AndroidEntryPoint
class MediaSelectionFragment : BaseFragment<MediaSelectionViewModel>() {
    override val viewModel: MediaSelectionViewModel by viewModels()
    override val layoutResId: Int = R.layout.fragment_media_selection
    private val mediaAdapter: MediaAdapter by lazy { MediaAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        setupList()
    }

    private fun setupObservers() {
        val postType = arguments?.getInt(Constants.POST_TYPE)
        viewModel.postType = postType
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(medias) {
                mediaAdapter.apply {
                    submitList(it)
                }
            }
            observe(nextButtonEnabled) {
                postNextButton.isVisible = it
            }
            observe(albums) {
                displayAlbumForSelector(it)
            }
        }
        viewModel.createAlbumCollection(requireActivity(), MimeType.ofVideo())
        fetchAllMedias()
    }

    private fun displayAlbumForSelector(it: List<Album>?) {
    }

    private fun fetchAllMedias() {
        if (allPermissionsGranted(Constants.SCAN_CARD_PERMISSIONS)) {
            viewModel.fetchAllMedias()
        } else {
            getRuntimePermissions(
                Constants.SCAN_CARD_PERMISSIONS,
                Constants.SCAN_CARD_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    private fun setupList() {
        rcvMedias.adapter = ConcatAdapter(
            ConcatAdapter.Config.Builder()
                .setIsolateViewTypes(false)
                .build(),
            mediaAdapter
        )
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (!allPermissionsGranted(Constants.SCAN_CARD_PERMISSIONS)) {
            Toast.makeText(requireContext(), R.string.permissions_required, Toast.LENGTH_LONG)
                .show()
        } else {
            viewModel.fetchAllMedias()
        }
    }

    override fun onDestroy() {
        viewModel.destroyLoader()
        super.onDestroy()
    }
}
