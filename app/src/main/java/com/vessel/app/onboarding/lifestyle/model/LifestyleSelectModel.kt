package com.vessel.app.onboarding.lifestyle.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LifestyleSelectModel(
    val id: Int,
    val title: String,
    val image: String,
    val checked: Boolean
) : Parcelable
