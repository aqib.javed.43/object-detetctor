package com.vessel.app.common.net.service

import com.vessel.app.wellness.net.data.ScoresData
import retrofit2.http.*

interface SampleService {
    @GET("sample/{sampleUuid}/super")
    suspend fun getScoreForId(@Path("sampleUuid") uuid: String): ScoresData
}
