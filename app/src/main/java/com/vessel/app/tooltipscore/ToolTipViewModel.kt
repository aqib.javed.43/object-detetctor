package com.vessel.app.tooltipscore

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.ReagentsManager
import com.vessel.app.common.manager.ScoreManager
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.data.ReagentState
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.testscore.TestScoreViewModel
import com.vessel.app.tooltipscore.ui.ReagentScoreAdapter
import com.vessel.app.tooltipscore.ui.ToolTipScoreFragmentDirections
import com.vessel.app.tooltipscore.ui.ToolTipState
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.model.ReagentItem
import kotlinx.coroutines.launch

class ToolTipViewModel @ViewModelInject constructor(
    val state: ToolTipState,
    resourceProvider: ResourceRepository,
    private val scoreManager: ScoreManager,
    val preferencesRepository: PreferencesRepository,
    private val reagentsManager: ReagentsManager,
    val homeTabsManager: HomeTabsManager,
    val buildPlanManager: BuildPlanManager,
) : BaseViewModel(resourceProvider), ToolbarHandler, ReagentScoreAdapter.OnActionHandler {

    init {
        loadScores()
    }

    private fun loadScores() {
        showLoading()
        viewModelScope.launch {
            scoreManager.getWellnessScores(forceUpdate = false)
                .onSuccess { score ->
                    hideLoading()
                    state {
                        val currentTestScore = score.wellness.last().y.toInt()
                        scoreItems.value = currentTestScore.toString()
                        reagentItems.value =
                            score.reagents.filter { it.key.state != ReagentState.COMING_SOON }.map {
                                ReagentItem(
                                    id = it.key.id,
                                    title = it.key.displayName,
                                    unit = it.key.unit,
                                    chartEntries = it.value,
                                    lowerBound = it.key.lowerBound,
                                    upperBound = it.key.upperBound,
                                    minY = it.key.minValue,
                                    maxY = it.key.maxValue,
                                    dimmed = false,
                                    ranges = it.key.ranges,
                                    isBetaTesting = it.key.isBeta,
                                    isInverted = it.key.isInverted,
                                    selectedChartEntityPosition = it.value.lastIndex,
                                    consumptionUnit = it.key.consumptionUnit,
                                    info = it.key.info,
                                    state = it.key.state,
                                    impact = null
                                )
                            }
                        when (currentTestScore) {
                            in 0..25 -> TestScoreViewModel.ScoreLevel.Poor
                            in 26..50 -> TestScoreViewModel.ScoreLevel.Fair
                            in 51..75 -> TestScoreViewModel.ScoreLevel.Good
                            in 76..100 -> TestScoreViewModel.ScoreLevel.Great
                            else -> null
                        }.also {
                            scoreLevel.value = it?.level
                            scoreLevelColor.value =
                                it?.color ?: TestScoreViewModel.ScoreLevel.Poor.color
                        }
                    }
                }
                .onError {
                    hideLoading()
                    // todo handle error case
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onReagentSelected(item: ReagentItem) {
        state.navigateTo.value =
            ToolTipScoreFragmentDirections.actionToolTipScoreFragmentToReagentGraphFragment(
                com.vessel.app.reagent.model.ReagentHeader(
                    id = item.id,
                    title = item.title,
                    unit = item.unit,
                    chartEntries = item.chartEntries.map {
                        DateEntry(
                            it.x,
                            it.y,
                            it.date,
                            it.reagentsCount
                        )
                    },
                    lowerBound = item.lowerBound,
                    upperBound = item.upperBound,
                    minY = item.minY,
                    maxY = item.maxY,
                    background = reagentsManager.fromId(item.id)!!.headerImage,
                    ranges = item.ranges,
                    errorDescription = if (item.id == com.vessel.app.common.model.data.ReagentItem.B9.id) getResString(
                        R.string.b9_not_available_message
                    ) else getResString(
                        R.string.reagent_test_results_unavailable,
                        item.title
                    ),
                    isBetaReagent = item.isBetaTesting,
                    betaHint = getResString(
                        R.string.reagent_beta_test_hint,
                        item.title
                    ),
                    consumptionUnit = item.consumptionUnit
                )
            )
    }
}
