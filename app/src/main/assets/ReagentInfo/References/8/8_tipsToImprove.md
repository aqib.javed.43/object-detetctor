The best way to improve your stress (and cortisol) level is with lifestyle changes to improve your stress resilience. Some specific nutrients can also support the health of your adrenal glands.

-   Meditation: Here’s how to do mindfulness meditation - Sit still, set a timer, close your eyes, and focus your attention on the sensation of your breath moving through your nostrils. Notice when you are distracted (thoughts, emotions, physical sensations) in a nonjudgmental way, then return your attention to the breath. Continue this for the duration of your session.

-   Breathwork: Breathwork activates your relaxation response (parasympathetic nervous system). Sit in a comfortable position, close your eyes, and slow down the rate of your breathing to at least 7 seconds of inhalation, and 7 seconds of exhalation. Count in your head to keep track of your pace. Another option is called square breathing: 4 second inhale, hold for 4 seconds, 4 second exhale, hold for 4 seconds. Continue for at least 3 minutes per session.

-   Yin Yoga: Yin yoga (also called restorative yoga) helps to lower stress (and cortisol) levels. This is not the type of yoga where you sweat and get a workout. It is a slower type of yoga that involves holding deeper stretches for longer periods of time. Aim for at least 20 minutes each session.

-   Cardio: Vigorous cardiovascular exercise in the right amounts can help to revitalize your stress response system. In the short-term physical exercise actually increases cortisol levels, however in the long-term regular vigorous cardiovascular exercise leads to decreased cortisol levels by increasing the breakdown of active cortisol hormone into its inactive metabolite (cortisone). To have this effect, studies have found that exercise requires an intensity of at least 60^ of VO2max (maximum capacity of oxygen uptake). Aim for 3 cardio workouts per week (doing more can actually increase stress), 45-60 minutes per session, keeping your average heart rate at or above 70% of your age-based maximum heart rate. To calculate this do (220 - age) multiplied by 0.8. Good exercises to hit these goals are high intensity interval training, circuit training with minimal rest, or cardio-based workout classes.

-   Nighttime Routine: We are creatures of habit, and our brains are constantly looking for patterns to run on autopilot so we can free up our awareness to focus on what's in front of us. Because Western cultures often value and re-inforce stress-based activities (such as long work hours, doing more, achieving more, having more), we often unintentionally develop a nighttime routine that activates our stress-response system rather than our relaxation response system. This can lead to poor sleep quality and sleep deprivation, both of which have a big negative impact on our stress levels. Improving daily habits around sleep (called sleep hygiene) has repeatedly been shown in research to improve sleep quality and duration. One of the most useful habits to create is to practice a nighttime routine.

-   Start by calculating your ideal wake-up time. Let's call that time X.

-   Subtract 8.5 hours from X to arrive at your go-to-bed time.

-   Subtract 10 hours from X to arrive at your target start-time for your nighttime routine.

-   Let’s say you want to wake up at 6AM. So your nighttime routine would start at 8PM, lasting 1.5 hours and your go-to-bed time would be 10PM. You may want to set alarms to remind you of these set times at first.

-   When the time comes to start your nighttime routine, stop doing anything productive, close your laptop, and put away your phone. The options you can do for this 1.5 hours are any relaxing non-stimulating activities that you enjoy. Examples include conversations with friends and family, reading fiction (avoid non-fiction which is mentally stimulating and counter productive), sexy time (even though this is, and should be, stimulating it’s allowed), cooking, cleaning, doing laundry, take a walk under the moonlight, cuddle a pet, groom a pet, groom yourself, take a bath (try adding 2 cups epsom salts, 1 cup baking soda, and a few drops of lavender essential oil), drink tea (try yogi bedtime tea), watching TV is ok if it’s lighthearted such as comedy or light drama, nothing like action, thriller, or horror which activates your stress-response-system.

-   Continue these activities until your target bedtime, then go to bed. You have 30 minutes to fall asleep. It’s ok to listen to a fiction audiobook or guided meditation as long as your eyes are closed.

-   If you aren’t asleep after 30 minutes, get out of bed, go to another room, and continue a minimum of 30 minutes more of the relaxing activities until you feel very sleepy (even if it takes 2 or more hours), then go back to bed and try again. If not asleep after 30 minutes, get out of bed and repeat.

-   No matter when you go to sleep, still get out of bed at your target wake-time.

-   If you have problems with insomnia, no napping allowed!

-   There may be a rough few days, but if you commit to this, you’ll be back to restorative sleep in no time!

-   Hang out with someone: Did you know that [a recent study](https://www.cdc.gov/aging/publications/features/lonely-older-adults.html) showed loneliness increases your risk of premature death from any cause more than smoking, obesity, high blood pressure, or diabetes? And it is associated with a 50% increased risk for dementia? We are tribal creatures, and social isolation is a major stressor for us. So if you have high stress, start to invest time in building high quality relationships with others. Quality is better than quantity. It could be an intimate conversation with your spouse, going on a walk with a friend, a phone conversation with a friend to catch up, a double date with another couple… you get the point.


Here are a few other tips in addition to what you add to your plan in the lifestyle section of the app.

-   Choose foods low in sugar and refined carbohydrates, and high in healthy fats (omega 3 fatty acids, nuts, seeds, avocado, plant based oils). Eat mostly plants, the more colorful the better. Include beans, vegetables, whole fruit rather than fruit juice, nuts seeds, avocado, eggs, some dairy and lean meats if you eat animal protein.

-   Prioritize the QUALITY not quantity of your sleep.

-   Try to minimize all light exposure (especially blue light) after sunset. Most mobile devices have an option to do this, flux is a free computer app that will do this, and there are also blue light blocking glasses that you can use after sunset.

-   Keep your bedroom cool and dark. 60-65 degrees is the ideal temperature. Use black out curtains if you can, or if not a high quality eye mask.

-   Create a wind down ritual 1-2 hours before bedtime. Stop all work and get off electronic devices. We recommend sipping herbal tea, reading a fiction book, a warm bath with epsom salts, connecting with loved ones. If you must watch TV, stick to comedy, light drama, or documentaries after sunset.

-   No caffeine after 1pm.

-   Avoid over the counter and prescription sleep aids (all likely block stage N3 sleep, the deep sleep stage that helps you feel refreshed in the morning)

-   Commit to a regular cardiovascular exercise schedule, mornings are ideal.

-   Reduce PERCEIVED stress.

-   When it comes to psychological stress, the cause of the stress response (and elevated cortisol) is our PERCEPTION of danger/threat, not the event itself. As we can train our mind to have different perceptions of the events in our life, we can reduce the activation of our stress response (HPA axis) and thus help normalize our cortisol levels. A therapist or life coach can be especially helpful in coaching you through this process.

-   Eliminate chronic inflammation.

-   Physical stress can be just as damaging as psychological stress. Physical stress most often comes from the process of inflammation, which is often triggered by our immune system, tissue injury, organ dysfunction, or toxic exposures. Work with your health team to do whatever you can with lifestyle to eliminate chronic inflammation.

-   Incorporate breath work.

-   Breath work or breathing exercises helps tone the vagus nerve. Good vagal nerve tone is associated with greater stress resilience. Try this in the shower, a meeting, while driving, or to prepare for sleep. Everybody has to breathe so this is an easy tool to implement.
