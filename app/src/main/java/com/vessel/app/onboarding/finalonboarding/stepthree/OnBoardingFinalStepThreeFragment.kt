package com.vessel.app.onboarding.finalonboarding.stepthree

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.appsflyer.AppsFlyerLib
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.homescreen.HomeScreenFragmentDirections
import com.vessel.app.homescreen.MainPagerFragmentTab
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnBoardingFinalStepThreeFragment : BaseFragment<OnBoardingFinalStepThreeViewModel>() {
    override val viewModel: OnBoardingFinalStepThreeViewModel by viewModels()
    override val layoutResId = R.layout.fragment_on_boarding_final_step_three
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    fun setupObservers() {
        observe(viewModel.state.backToHome) {
            findNavController().navigate(HomeScreenFragmentDirections.globalActionToWellness(MainPagerFragmentTab.PLAN))
        }
        observe(viewModel.state.navigateTo) { destination ->
            when (destination) {
                OnBoardingFinalStepThreeState.NavigationDestination.HOME -> {
                    AppsFlyerLib.getInstance().unregisterConversionListener()
                    findNavController().navigate(OnBoardingFinalStepThreeFragmentDirections.actionThreeStepFragmentToHome())
                    activity?.finish()
                }
                else -> throw IllegalArgumentException("Unexpected Navigation destination: $destination")
            }
        }
    }
}
