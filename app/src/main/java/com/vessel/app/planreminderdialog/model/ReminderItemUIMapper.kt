package com.vessel.app.planreminderdialog.model

import android.annotation.SuppressLint
import com.vessel.app.Constants
import com.vessel.app.common.net.data.TodoReminderData
import com.vessel.app.views.weekdays.DayOfWeekUiModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReminderItemUIMapper @Inject constructor() {

    fun mapDateTo24TimeString(date: Date): String {
        val calendar = Calendar.getInstance()
        calendar.time = date
        return String.format("%s:%s", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE))
    }

    @SuppressLint("SimpleDateFormat")
    fun createTodoReminder(
        id: Int? = null,
        timeOfTheDay: String,
        selectedDays: List<DayOfWeekUiModel>,
        recurringTodoId: Int,
        title: String
    ): TodoReminderData {
        return TodoReminderData(
            created_at = SimpleDateFormat(Constants.REMINDERS_CREATED_AT_FORMAT).format(Date()),
            day_of_week = selectedDays.map { it.planIndex },
            recurring_todo_id = recurringTodoId,
            time_of_day = convertReminderToUTCTime(timeOfTheDay),
            title = title,
            id = id
        )
    }

    private fun convertReminderToUTCTime(timeOfTheDay: String): String {
        return simpleDateFormat.format(Constants.SIMPLE_TIME_24_HOUR_FORMAT.parse(timeOfTheDay)!!)
    }
    companion object {
        val simpleDateFormat = SimpleDateFormat(Constants.REMINDERS_TIME_FORMAT, Locale.US)
    }
}
