package com.vessel.app.common.manager

import com.vessel.app.BuildConfig
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ForceUpdateManager @Inject constructor(private val remoteConfiguration: RemoteConfiguration) {
    private var isOptionalUpdateDismissed: Boolean = false

    fun isRequireUpdate(): Pair<Boolean, ForceUpdateType?> {
        val suggestedVersion = remoteConfiguration.getString(APP_SUGGESTED_VERSION).split(".")
            .mapNotNull { it.filter { it.isDigit() }.toIntOrNull() }
        val minVersion = remoteConfiguration.getString(APP_MIN_VERSION).split(".")
            .mapNotNull { it.filter { it.isDigit() }.toIntOrNull() }
        val currentVersion = BuildConfig.VERSION_NAME.split(".")
            .mapNotNull { it.filter { it.isDigit() }.toIntOrNull() }

        return when {
            currentVersion.lessThan(minVersion) -> {
                Pair(true, ForceUpdateType.Required)
            }
            currentVersion.lessThan(suggestedVersion) && isOptionalUpdateDismissed.not() -> {
                Pair(true, ForceUpdateType.Optional)
            }
            else -> {
                Pair(false, null)
            }
        }
    }

    private fun List<Int>.lessThan(v: List<Int>): Boolean {
        val size = maxOf(size, v.size)
        for (i in 0 until size) {
            val digit = if (i < size) get(i) else 0
            val vDigit = if (i < v.size) v[i] else 0
            if (digit > vDigit) {
                return false
            } else if (digit < vDigit) {
                return true
            }
//            digit == vDigit continue
        }
//         if (isEmpty()) no elements to be checked -> true
//         else all elements are equals -> false
        return isEmpty()
    }

    fun setOptionalUpdateDismissed() {
        isOptionalUpdateDismissed = true
    }

    companion object {
        private const val APP_MIN_VERSION = "app_min_version"
        private const val APP_SUGGESTED_VERSION = "app_suggested_version"
    }
}

enum class ForceUpdateType {
    Required,
    Optional
}
