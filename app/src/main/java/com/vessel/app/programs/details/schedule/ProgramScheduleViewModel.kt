package com.vessel.app.programs.details.schedule

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Program
import com.vessel.app.common.model.SchedulePlanItem
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.programs.details.adapters.ProgramSchedulePlanItemAdapter
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class ProgramScheduleViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val tipMapper: TipMapper,
    private val planManager: PlanManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    ProgramSchedulePlanItemAdapter.OnActionHandler {
    val state = ProgramScheduleState(savedStateHandle.get<Program>("program")!!)

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onSchedulePlanItemClicked(item: SchedulePlanItem) {
        state.program.value?.planData?.firstOrNull { it.tip?.title == item.title }?.tip?.let {
            state.navigateTo.value =
                ProgramScheduleFragmentDirections.actionProgramScheduleFragmentToTipDetailsDialogFragment(
                    tipMapper.mapTipData(it)
                )
        }
    }

    override fun onSchedulePlanItemTimerClicked(item: SchedulePlanItem) {
        viewModelScope.launch {
            planManager.getUserPlans().onSuccess { plans ->
                plans.filter { it.getDisplayName() == item.title }.let { tipPlans ->
                    val planReminderHeader = PlanReminderHeader(
                        item.title,
                        "",
                        tipPlans.firstOrNull()?.getToDoImageUrl(),
                        null,
                        tipPlans
                    )
                    state.navigateTo.value =
                        ProgramScheduleFragmentDirections.actionProgramScheduleFragmentToPlanReminderDialog(
                            planReminderHeader
                        )
                }
            }
        }
    }
}
