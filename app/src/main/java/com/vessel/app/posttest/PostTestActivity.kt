package com.vessel.app.posttest

import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.vessel.app.R
import com.vessel.app.base.BaseActivity
import com.vessel.app.util.extensions.applyFullscreenFlags
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostTestActivity : BaseActivity<PostTestViewModel>() {
    override val viewModel: PostTestViewModel by viewModels()
    override val layoutResId = R.layout.activity_post_test

    override fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        applyFullscreenFlags()
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        (supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment).navController.let {
            val graph = it.navInflater.inflate(R.navigation.post_test_nav_graph)
            // Remove goal selection from test flow
            it.graph = graph.apply { startDestination = R.id.testScoreFragment }
        }

        setupObservers()
    }

    private fun setupObservers() {
        observe(viewModel.state.navigateTo) {
            Navigation.findNavController(this, R.id.fragmentContainerView)
                .navigate(it)
        }
    }
}
