package com.vessel.app

import org.assertj.core.api.AbstractBooleanAssert
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.ObjectAssert

infix fun Any?.isEqualTo(that: Any?): ObjectAssert<Any?> = assertThat(this).isEqualTo(that)

infix fun Any?.isNotEqualTo(that: Any?): ObjectAssert<Any?> = assertThat(this).isNotEqualTo(that)

infix fun <T> Any?.isInstanceOf(that: Class<T>): ObjectAssert<Any?> = assertThat(this).isInstanceOf(that)

infix fun <T> Any?.isNotInstanceOf(that: Class<T>): ObjectAssert<Any?> = assertThat(this).isNotInstanceOf(that)

fun Any?.isNull() = assertThat(this).isNull()

fun Any?.isNotNull(): ObjectAssert<Any?> = assertThat(this).isNotNull

fun Boolean?.isTrue(): AbstractBooleanAssert<*> = assertThat(this).isTrue

fun Boolean?.isFalse(): AbstractBooleanAssert<*> = assertThat(this).isFalse
