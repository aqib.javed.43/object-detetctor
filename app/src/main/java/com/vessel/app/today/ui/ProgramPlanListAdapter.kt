package com.vessel.app.today.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.today.model.ActivityListItem
import com.vessel.app.util.extensions.isSameDay
import kotlinx.android.synthetic.main.item_program_activity_list.view.*
import kotlinx.android.synthetic.main.item_program_activity_list.view.title
import kotlinx.android.synthetic.main.item_program_activity_list.view.wellDoneLayout
import kotlinx.android.synthetic.main.program_activity_completed.view.*
import java.util.*

class ProgramPlanListAdapter(private val handler: ActivityAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<ActivityListItem>() {
    override fun getLayout(position: Int) = R.layout.item_program_activity_list
    override fun onBindViewHolder(holder: BaseViewHolder<ActivityListItem>, position: Int) {
        if (currentList.isNullOrEmpty() || position >= currentList.size)return
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as ActivityListItem
        holder.itemView.title.text = item.getTitle()
        holder.itemView.wellDoneLayout.visibility = if (item.isFinishAll() && !item.noMoreActivity && item.isPreviousDay == false) View.VISIBLE else View.GONE
        holder.itemView.allDoneLayout.visibility = if (item.isFinishAll() && item.noMoreActivity && item.isPreviousDay == false) View.VISIBLE else View.GONE
        val isToday = item.date.isSameDay(Calendar.getInstance().time)
        val itemList = if (item.isPreviousDay == false) item.items else item.items?.filter {
            it.isCompleted
        }
        holder.itemView.notDoneLayout.visibility = if (itemList.isNullOrEmpty() && item.isPreviousDay == true) View.VISIBLE else View.GONE
        val programAdapter = ActivityAdapter(handler, position, true)
        holder.itemView.findViewById<RecyclerView>(R.id.activityList).apply {
            adapter = programAdapter
            itemAnimator = null
            setHasFixedSize(true)
        }
        programAdapter.onActivityUpdate = {
            holder.itemView.wellDoneLayout.visibility = if (item.isFinishAll() && !item.noMoreActivity && item.isPreviousDay == false) View.VISIBLE else View.GONE
            if (isAllCompleted()) {
                handler.onProgramActivityCompleted(item.items?.firstOrNull(), holder.itemView)
            }
        }
        programAdapter.submitList(itemList)
        holder.itemView.title.setOnClickListener {
            handler.onProgramInformationClicked()
        }
        holder.itemView.wellDoneLayout.unlockButton.setOnClickListener {
            handler.onGetNextPlanClicked(item.items?.lastOrNull(), item.date)
        }
    }
    private fun isAllCompleted(): Boolean {
        var isTodayCompleteAll = true
        for (activity in currentList) {
            if (!activity.isFinishAll()) isTodayCompleteAll = false
        }
        return isTodayCompleteAll
    }
}
