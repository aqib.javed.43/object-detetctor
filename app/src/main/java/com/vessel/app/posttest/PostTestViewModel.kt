package com.vessel.app.posttest

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.ResourceRepository

class PostTestViewModel @ViewModelInject constructor(
    preferencesRepository: PreferencesRepository,
    resourceRepository: ResourceRepository
) : BaseViewModel(resourceRepository) {

    val state = PostTestState(preferencesRepository.testCompletedCount)
}
