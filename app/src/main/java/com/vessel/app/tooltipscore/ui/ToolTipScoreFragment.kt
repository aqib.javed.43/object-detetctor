package com.vessel.app.tooltipscore.ui

import android.os.Bundle
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.testscore.TestScoreViewModel
import com.vessel.app.testscore.ui.TestScoreFragment
import com.vessel.app.tooltipscore.ToolTipViewModel
import com.vessel.app.util.extensions.hideKeyboard
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_test_score.resultsList
import kotlinx.android.synthetic.main.fragment_tool_tip_score.*

@AndroidEntryPoint
class ToolTipScoreFragment : BaseFragment<ToolTipViewModel>() {
    override val viewModel: ToolTipViewModel by viewModels()
    override val layoutResId = R.layout.fragment_tool_tip_score
    private val resultAdapter by lazy { ReagentScoreAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)
        setupObservers()
        setupAdapters()
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            beforeNavigateBack()
            findNavController().popBackStack()
            hideKeyboard()
        }
    }

    private fun setupAdapters() {
        resultsList.apply {
            adapter = resultAdapter
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(reagentItems) {
                resultAdapter.submitList(it)
            }
            observe(scoreLevelColor) {
                val color = ContextCompat.getColor(
                    requireContext(),
                    it ?: TestScoreViewModel.ScoreLevel.Poor.color
                )
                wellnessScoreContainerCard.setCardBackgroundColor(color)
            }
        }
    }

    override fun beforeNavigateBack() {
        findNavController().previousBackStackEntry
            ?.savedStateHandle?.apply {
                set(
                    TestScoreFragment.PREVENT_ANIMATION,
                    true
                )
            }
    }
}
