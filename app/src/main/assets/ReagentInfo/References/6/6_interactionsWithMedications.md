-   Folate supplements could interfere with methotrexate (Rheumatrex® , Trexall® ) when taken to treat cancer.
-   Taking anti-epileptic or anti-seizure medications, such as phenytoin (Dilantin®), carbamazepine (Carbatrol®, Tegretol®, Equetro®, Epitol®), and valproate (Depacon®), could reduce blood levels of folate. Also, taking folate supplements could reduce blood levels of these medications.
-   Taking sulfasalazine (Azulfidine® ) for ulcerative colitis could reduce the body’s ability to absorb folate and cause folate deficiency.
-   References for all above:
-
Fact sheet for health professionals. National Institutes of Health Office of Dietary Supplements.
    - https://ods.od.nih.gov/factsheets/Folate-HealthProfessional/

