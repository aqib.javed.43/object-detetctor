package com.vessel.app.recommendations.ui

import android.view.View
import com.vessel.app.base.BaseAdapterWithDiffUtil

class RecommendationsAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<RecommendationsPage>() {
    override fun getLayout(position: Int) = (getItem(position) as RecommendationsPage).layoutResId

    override fun getHandler(position: Int): OnActionHandler = handler

    interface OnActionHandler {
        fun onPlanActionClicked(recommendationTab: RecommendationTab, view: View)
        fun onSupplementPlanActionClicked(view: View)
        fun onSetReminderClicked(view: View, canAddReminder: Boolean)
    }
}
