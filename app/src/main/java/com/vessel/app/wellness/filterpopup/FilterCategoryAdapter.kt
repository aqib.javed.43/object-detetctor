package com.vessel.app.wellness.filterpopup

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class FilterCategoryAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<ItemFilterCategory>() {
    override fun getLayout(position: Int) = R.layout.item_filter_category

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(
        holder: BaseViewHolder<ItemFilterCategory>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
        val filterItemAdapter =
            FilterChoiceItemSelectAdapter(object : FilterChoiceItemSelectAdapter.OnActionHandler {
                override fun onFilterSelectChecked(
                    item: ItemFilterChoice,
                    checked: Boolean
                ) {
                    handler.onFilterSelectChecked(getItem(position), item, checked)
                }
            })
        holder.itemView.findViewById<RecyclerView>(R.id.choicesRecyclerView).apply {
            adapter = filterItemAdapter
            itemAnimator = null
            layoutManager = GridLayoutManager(holder.itemView.context, 5).apply {
                spanSizeLookup = FilterSpanSizeLookup()
            }
        }
        filterItemAdapter.submitList(getItem(position).choiceItems)
    }

    interface OnActionHandler {
        fun onFilterSelectChecked(
            selectedCategory: ItemFilterCategory,
            selectedChoice: ItemFilterChoice,
            checked: Boolean
        )
    }

    class FilterSpanSizeLookup() : GridLayoutManager.SpanSizeLookup() {

        override fun getSpanSize(position: Int): Int {
            return if (position % 2 != 0) 2 else 3
        }
    }
}
