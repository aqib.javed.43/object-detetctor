package com.vessel.app.createnewtpost

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.teampage.model.PostType
import com.vessel.app.util.ResourceRepository

class CreateNewPostViewModel@ViewModelInject constructor(
    val state: CreateNewPostState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler {
    var postType: Int? = PostType.TEXT.ordinal
    override fun onBackButtonClicked(view: View) {
        view.findNavController().navigateUp()
    }
    fun onContinueClicked() {
        val nav: NavDirections = when (postType) {
            PostType.TEXT.ordinal -> CreateNewPostFragmentDirections.actionCreateNewPostFragmentToPostPreviewFragment()
            PostType.VIDEO.ordinal -> CreateNewPostFragmentDirections.actionCreateNewPostFragmentToMediaPostPreviewFragment(postType!!)
            PostType.PHOTO.ordinal -> CreateNewPostFragmentDirections.actionCreateNewPostFragmentToMediaPostPreviewFragment(postType!!)
            else -> CreateNewPostFragmentDirections.actionCreateNewPostFragmentToPostPreviewFragment()
        }
        state.navigateTo.value = nav
    }
}
