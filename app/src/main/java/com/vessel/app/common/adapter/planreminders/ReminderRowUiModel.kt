package com.vessel.app.common.adapter.planreminders

import android.icu.text.NumberFormat
import android.os.Parcelable
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.util.extensions.toCurrentWeekDate
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.Locale

@Parcelize
data class ReminderRowUiModel(
    val day: String,
    val quantites: List<String>,
    val times: List<String>,
    val dayOfWeek: Int,
    val plan: PlanData
) : Parcelable

fun PlanData.toReminderList(): List<ReminderRowUiModel> {
    return getDaysOfWeek().map {
        val date = it.toCurrentWeekDate()
        val dayName = SimpleDateFormat("EEEE", Locale.ENGLISH).format(date)
        val time = if (time_of_day.isNullOrEmpty()) {
            "Set time"
        } else {
            getTimeFormattedWithMeridian().orEmpty()
        }

        val quantity = if (isSupplementPlan() || isTip())"" else "${NumberFormat.getInstance().format(getQuantityScale() * (multiple ?: 1))} ${getQuantityUnit()}"

        ReminderRowUiModel(dayName, listOf(quantity), listOf(time), it, this)
    }
}

fun List<PlanData>.toReminderList(): List<ReminderRowUiModel> {
    val plans = mutableListOf<ReminderRowUiModel>()
    forEach {
        plans.addAll(it.toReminderList())
    }

    val groupedPlans = plans.groupBy { it.day }
    val items = mutableListOf<ReminderRowUiModel>()
    groupedPlans.forEach {
        val quantities = it.value.map { it.quantites.first() }
        val times = it.value.map { it.times.first() }
        items.add(ReminderRowUiModel(it.key, quantities, times, it.value.first().dayOfWeek, it.value.first().plan))
    }

    return items.sortedBy { it.dayOfWeek }
}
