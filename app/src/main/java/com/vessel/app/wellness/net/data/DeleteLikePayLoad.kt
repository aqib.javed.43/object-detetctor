package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class DeleteLikePayLoad(
    val record_id: Int,
    val category: String,
)
