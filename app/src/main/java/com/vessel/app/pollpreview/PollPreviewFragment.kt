package com.vessel.app.pollpreview

import android.os.Bundle
import android.widget.LinearLayout
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.ItemPollView
import com.vessel.app.wellness.net.data.PollQuestions
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_poll_preview.*

@AndroidEntryPoint
class PollPreviewFragment : BaseFragment<PollPreviewViewModel>() {
    override val viewModel: PollPreviewViewModel by viewModels()
    override val layoutResId = R.layout.fragment_poll_preview

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        observeValues()
        setupBackPressedDispatcher()
    }

    private fun setupViews() {
        val pollQuestions = arguments?.getSerializable(Constants.POLL_ITEM) as PollQuestions
        updateView(pollQuestions)
        btnPost.setOnClickListener {
            viewModel.createPoll(pollQuestions)
        }
    }

    private fun updateView(pollQuestions: PollQuestions) {
        val content = pollQuestions.question
        postContent.text = content
        pollLayout.removeAllViews()
        pollQuestions.response1?.let {
            addPollOptions(it)
        }
        pollQuestions.response2?.let {
            addPollOptions(it)
        }
        pollQuestions.response3?.let {
            addPollOptions(it)
        }
        pollQuestions.response4?.let {
            addPollOptions(it)
        }
    }
    private fun addPollOptions(
        question: String?
    ) {
        if (question.isNullOrEmpty()) return
        val pollView = ItemPollView(requireContext())
        pollView.apply {
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            layoutParams.bottomMargin = 20
            setLayoutParams(layoutParams)
            setQuestion(question)
            pollLayout.addView(this)
        }
    }
    private fun observeValues() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }

    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.let { viewModel.onBackButtonClicked(it) }
        }
    }
}
