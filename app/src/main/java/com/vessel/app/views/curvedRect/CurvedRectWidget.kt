package com.vessel.app.views.curvedRect

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.vessel.app.R
import com.vessel.app.util.extensions.animateHeight
import com.vessel.app.util.extensions.setHeight
import com.vessel.app.wellness.model.GoalButton
import kotlin.math.sqrt

class CurvedRectWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    var isAnimatedBefore: Boolean = false

    private val path = Path()

    private var progressValue = 0
    private var widgetWidth = 0
    private var widgetHeight = 0

    private var progressColor = ContextCompat.getColor(context, R.color.black)
    private val lineWidth = resources.getDimension(R.dimen.curved_rect_lint_width)
    private val lineCurve = resources.getDimension(R.dimen.curved_rect_curve_width)

    private var animationDuration: Long = 3000
    private var progressAnimator: ValueAnimator? = null

    private lateinit var rectF: RectF
    private lateinit var rectFInside: RectF
    private lateinit var rectOutside: RectF

    private val paint = Paint().apply {
        // Smooth out edges of what is drawn without affecting shape.
        isAntiAlias = true
        strokeWidth = lineWidth
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawClippedRect(canvas)
    }

    private fun drawClippedRect(canvas: Canvas) {
        canvas.save()
        clipOutsideShape(canvas)
        clipInsideShape(canvas)
        drawProgressArc(canvas)
        canvas.restore()
    }

    private fun drawProgressArc(canvas: Canvas) {
        canvas.drawArc(rectOutside, RECTANGLE_DRAW_START_ANGLE.toFloat(), progressValue.toFloat(), true, paint)
    }

    private fun clipOutsideShape(canvas: Canvas) {
        path.rewind()
        path.addRoundRect(rectF, lineCurve, lineCurve, Path.Direction.CCW)
        canvas.clipPath(path)
    }

    @Suppress("DEPRECATION")
    private fun clipInsideShape(canvas: Canvas) {
        path.rewind()
        path.addRoundRect(rectFInside, lineCurve, lineCurve, Path.Direction.CCW)
        // The method clipPath(path, Region.Op.DIFFERENCE) was deprecated in
        // API level 26. The recommended alternative method is
        // clipOutPath(Path), which is currently available in
        // API level 26 and higher.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            canvas.clipPath(path, Region.Op.DIFFERENCE)
        } else {
            canvas.clipOutPath(path)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        widgetWidth = w
        widgetHeight = h
        setupBoundaries()
    }

    private fun setupBoundaries() {
        val currentWidth = widgetWidth.toFloat()
        val currentHeight = widgetHeight.toFloat()

        val radius = sqrt((currentWidth * currentWidth) + currentHeight * currentHeight)

        rectF = RectF(0f, 0f, currentWidth, currentHeight)
        rectOutside = RectF(0f - radius, 0f - radius, currentWidth + radius, currentHeight + radius)
        rectFInside =
            RectF(lineWidth, lineWidth, currentWidth - lineWidth, currentHeight - lineWidth)
    }

    private fun animate(from: Int, to: Int) {
        progressAnimator = ValueAnimator.ofInt(from, to)
        progressAnimator?.apply {
            duration = animationDuration
            addUpdateListener { valueAnimator ->
                progressValue = valueAnimator.animatedValue as Int
                invalidate()
            }

            addListener(object : CurvedRectAnimationListener() {
                override fun onAnimationCancel(animation: Animator) {
                    super.onAnimationCancel(animation)
                    progressValue = to
                    invalidate()
                    progressAnimator = null
                }

                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    isAnimatedBefore = true
                }
            })
            start()
        }
    }

    fun setProgress(start: Int, end: Int) {
        if (isAnimatedBefore) {
            progressValue = start * 360 / end
            invalidate()
        } else {
            animate(0, start * 360 / end)
        }
    }

    fun setProgressColor(@ColorRes res: Int) {
        progressColor = ContextCompat.getColor(context, res)
        paint.color = progressColor
        invalidate()
    }

    fun setProgressAnimationDuration(inMilli: Long) {
        animationDuration = inMilli
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        progressAnimator?.cancel()
    }

    companion object {
        const val RECTANGLE_DRAW_START_ANGLE = 270
    }
}

@BindingAdapter("curvedProgress")
fun bindCurvedProgress(cp: CurvedRectWidget, model: GoalButton) {
    cp.apply {
        isAnimatedBefore = model.isProgressAnimatedBefore
        setProgressColor(if (model.selected) R.color.black else R.color.white)
        setProgressAnimationDuration(1500)
        setProgress(model.scoreValue, 100)
        model.isProgressAnimatedBefore = true
    }
}

@BindingAdapter("viewHeightAnimation")
fun changeViewHeight(view: CurvedRectWidget, model: GoalButton) {

    val selectedHeight =
        view.context.resources.getDimensionPixelSize(model.selectedButtonHeight)
    val notSelectedHeight =
        view.context.resources.getDimensionPixelSize(model.unselectedButtonHeight)

    if (model.isPreviousSelected) {
        if (!model.isAnimatedBefore) {
            view.animateHeight(selectedHeight, notSelectedHeight)
            model.isAnimatedBefore = true
        } else {
            view.setHeight(notSelectedHeight)
        }
    } else {
        if (model.selected) {
            if (!model.isAnimatedBefore) {
                view.animateHeight(notSelectedHeight, selectedHeight)
                model.isAnimatedBefore = true
            } else {
                view.setHeight(selectedHeight)
            }
        } else {
            view.setHeight(notSelectedHeight)
        }
    }
}
