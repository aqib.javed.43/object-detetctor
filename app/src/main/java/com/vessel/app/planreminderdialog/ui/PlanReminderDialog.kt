package com.vessel.app.planreminderdialog.ui

import android.app.TimePickerDialog
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.planreminderdialog.PlanReminderViewModel
import com.vessel.app.planreminderdialog.model.ReminderItemUiModel
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.PlanDeleteDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_add_plan_reminder_dialog.*

@AndroidEntryPoint
class PlanReminderDialog : BaseFragment<PlanReminderViewModel>() {
    override val viewModel: PlanReminderViewModel by viewModels()
    override val layoutResId = R.layout.fragment_add_plan_reminder_dialog

    private val remindersAdapter by lazy { PlanReminderAdapter(viewModel) }
    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)

        setupList()

        observe(viewModel.state.dismissDialog) {
            if (viewModel.state.planHeader.finishTheActivity) {
                requireActivity().finish()
            } else {
                findNavController().navigateUp()
            }
        }

        observe(viewModel.state.reminderList) {
            handleReminders(it.toMutableList())
        }

        observe(viewModel.state.planItem) {
            setupReminderQuantity(it)
        }

        observe(viewModel.state.setRemindersEvent) {
            if (viewModel.isAddReminderClicked) {
                onReminderAdded()
                viewModel.isAddReminderClicked = false
            } else {
                addReminderGroup.apply {
                    isVisible = true
                }
                val plan = viewModel.state.planItem.value!!
                addToPlanQuantityDescription.isVisible =
                    plan.isSupplementPlan().not() && plan.isTestingPlan().not() && plan.isTip()
                    .not()
                quantity_widget.isVisible =
                    plan.isSupplementPlan().not() && plan.isTestingPlan()
                    .not() && plan.isTip().not()
                addToPlanDescription.isVisible = true
                if (addToPlanDescription.text == getString(R.string.add_another_reminder))addPlanButton.setText(
                    R.string.save_new_reminder_item
                )
                viewModel.isAddReminderClicked = true
            }
        }

        observe(viewModel.state.skipRemindersEvent) {
            onReminderSkipped()
        }

        observe(viewModel.state.removeReminder) {
            PlanDeleteDialog.showDialog(
                requireActivity(),
                viewModel.state.planHeaderItem.value?.title.orEmpty(),
                {
                    // Not used
                },
                {
                    viewModel.onRemovePlanConfirmed()
                },
                {
                    viewModel.onDontShowAgainClicked(it)
                }
            )
        }

        observe(viewModel.state.updateReminderTime) {
            val time = it.getTimeHoursMinutes()
            val mTimePicker = TimePickerDialog(
                requireContext(),
                { _, selectedHour, selectedMinute ->
                    viewModel.onReminderTimeUpdated(it, "${String.format("%02d", selectedHour)}:${String.format("%02d", selectedMinute)}")
                },
                time.first,
                time.second,
                false
            )
            mTimePicker.setTitle(getString(R.string.set_time))
            mTimePicker.show()
        }
    }

    private fun setupList() {
        reminders.also {
            it.itemAnimator = null
            val adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                remindersAdapter,
            )
            it.adapter = adapter
        }
    }

    private fun handleReminders(planReminders: MutableList<ReminderItemUiModel>) {
        addToPlanDescription.text =
            getString(if (planReminders.isEmpty()) R.string.add_a_reminder else R.string.add_another_reminder)
        addPlanButton.setText(
            getString(if (planReminders.isEmpty()) R.string.add_to_plan else R.string.add_new_reminder_item)
        )
        reminders.isVisible = planReminders.isNotEmpty()
        remindersTitle.isVisible = planReminders.isNotEmpty()
        dummyBackground.isVisible = planReminders.isNotEmpty()
        remindersAdapter.submitList(planReminders)
        if (viewModel.state.planHeader.skippable.not()) {
            skipThis.isVisible = false
        }
        if (planReminders.isEmpty()) {
            addToPlanDescription.isVisible = true
            viewModel.onSetRemindersClicked()
        } else {
            addReminderGroup.isVisible = false
            addToPlanQuantityDescription.isVisible = false
            addToPlanDescription.isVisible = false
            addPlanButton.setText(R.string.add_new_reminder_item)
            skipThis.isVisible = false
            viewModel.isAddReminderClicked = false
        }
    }

    private fun setupReminderQuantity(plan: PlanData) {
        quantity_widget.setMinQuantity(plan.getQuantityScale().toFloat())
        quantity_widget.setMaxQuantity(
            plan.getQuantityScale().toFloat() * plan.getQuantityMaxMultiple()
        )
        quantity_widget.setQuantityUnit(plan.getQuantityScale().toFloat())
        quantity_widget.setQuantityDescription(plan.getQuantityUnit())
        quantity_widget.setQuantity(plan.getQuantityScale().toFloat() * (plan.multiple ?: 0))

        if (plan.hasReminderHint()) {
            weekTimePicker.setHint(plan.getReminderHint())
        }
    }

    private fun onReminderAdded() {
        viewModel.onReminderAdded(
            timeOfTheDay = "${String.format("%02d", weekTimePicker.getSelectedHour())}:${String.format("%02d", weekTimePicker.getSelectedMinute())}",
            selectedDays = weekTimePicker.getSelectedDays(),
            quantity = quantity_widget.getQuantity()
        )
    }
    private fun onReminderSkipped() {
        viewModel.onReminderAdded(
            timeOfTheDay = null,
            selectedDays = null,
            quantity = quantity_widget.getQuantity()
        )
    }
}
