package com.vessel.app.today.programinfo

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_program_information.*

@AndroidEntryPoint
class ProgramInfoFragment : BaseFragment<ProgramInfoViewModel>() {
    override val viewModel: ProgramInfoViewModel by viewModels()
    override val layoutResId = R.layout.fragment_program_information

    private val programInfoAdapter by lazy { ProgramInfoAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        goalList.adapter = programInfoAdapter
        goalList.itemAnimator = null
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(items) {
                programInfoAdapter.submitList(it) {
                    programInfoAdapter.notifyDataSetChanged()
                }
            }
        }
    }
}
