package com.vessel.app.goalpage

import android.text.format.DateUtils
import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.*
import com.vessel.app.Constants.FEELING_HAPPY
import com.vessel.app.Constants.FEELING_MEH
import com.vessel.app.Constants.FEELING_NOTHING
import com.vessel.app.Constants.FEELING_UNHAPPY
import com.vessel.app.base.BaseViewModel
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.GoalAssessment
import com.vessel.app.common.model.data.ReagentState
import com.vessel.app.common.net.data.Food
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.PlanRecord
import com.vessel.app.common.net.data.plan.ReagentLifestyleRecommendation
import com.vessel.app.common.net.mapper.ProgramMapper
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.repo.ContactRepository
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.foodplan.model.FoodDetailsItemModel
import com.vessel.app.goalpage.model.*
import com.vessel.app.goalpage.ui.*
import com.vessel.app.homescreen.HomeScreenFragmentDirections
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.plan.model.PlanItem
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.stressreliefdetails.model.StressReliefDetailsItemModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.util.extensions.isSameDay
import com.vessel.app.wellness.filterpopup.ItemFilterChoiceType
import com.vessel.app.wellness.model.*
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.coroutines.launch
import java.util.*

class GoalPageViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    var state: GoalPageState,
    private val goalsManager: GoalsManager,
    private val goalsRepository: GoalsRepository,
    private val preferencesRepository: PreferencesRepository,
    private val assessmentManager: AssessmentManager,
    private val planManager: PlanManager,
    private val trackingManager: TrackingManager,
    private val programMapper: ProgramMapper,
    private val reagentsManager: ReagentsManager,
    private val programManager: ProgramManager,
    private val scoreManager: ScoreManager,
    private val tipMapper: TipMapper,
    private val reminderManager: ReminderManager,
    private val membershipManager: MembershipManager,
    private val recommendationManager: RecommendationManager,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    GoalPageReagentAdapter.OnActionHandler,
    GoalPageTodayAdapter.OnActionHandler,
    GoalPageRecommendsAdapter.OnActionHandler,
    GoalPageFooterAdapter.OnActionHandler {
    var selectedFeelings = FEELING_NOTHING
    var goalTitle = ""
    var testsThatAffectsGoal = ""
    val today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
    val goalSelected = savedStateHandle.get<GoalSample>("goal")

    init {
        state.footerItems.value = listOf(GoalPageFooter)
        goalSelected?.let {
            goalTitle = String.format(getResString(R.string.rate_your), it.name)
            testsThatAffectsGoal = String.format(getResString(R.string.tests_that_affects), it.name)
            loadRecommendations(it.id)
        }
        val today: Date =
            Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
        loadTodayActivities(today = today)
        loadScores()
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    private fun loadAssessments() {
        viewModelScope.launch {
            showLoading()
            assessmentManager.getWellnessScores()
                .onSuccess {
                    buildHeaderData(it)
                    hideLoading()
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    override fun onFilterClicked() {
        navigateToFilter()
    }

    private fun navigateToFilter(tags: List<Tag>? = null) {
        state.navigateTo.value =
            HomeNavGraphDirections.globalActionToFilterDialogFragment(
                state.filterCategories.toTypedArray(),
                tags?.toTypedArray()
            )
    }

    fun updateFilterOptions() {
        state.filterSortChoices.value = state.getFilterChoices()
        state.recommendations.value = listOf()
        goalSelected?.id?.let {
            loadRecommendations(it)
        }
    }

    private fun buildHeaderData(goalAssessmentList: List<GoalAssessment>) {
        val goalsButtonList = arrayListOf<GoalButton>()
        goalsRepository.getGoals()
            .map { it.key }
            .forEachIndexed { index, goal ->
                val entries = getAssessmentDataEntriesBasedOnTheGoal(
                    goalAssessmentList,
                    goal
                )
                goalsButtonList.add(
                    GoalButton(
                        id = goal.id,
                        title = goal.title,
                        chartEntries = entries,
                        averageChartEntries = listOf(DateEntry(0f, 0f, null)),
                        image = goal.image,
                        selected = false,
                        smallImageUrl = Constants.GOALS.firstOrNull { it.id == goal.id }?.image_small_url
                            ?: "",
                        selectedChartEntityPosition = entries.lastIndex
                    )
                )
            }
        Constants.GOALBUTTONS = goalsButtonList.toList()
        Constants.GOALBUTTONS.firstOrNull {
            it.id == goalSelected?.id
        }?.let {
            val goalHeader = GoalPageHeader(it.title, it.chartEntries, it.background)
            state.goalHeaderItems.value = goalHeader
        }
    }

    private fun getAssessmentDataEntriesBasedOnTheGoal(
        goalAssessmentList: List<GoalAssessment>,
        goal: Goal
    ): List<DateEntry> {
        val dataEntryList = mutableListOf<DateEntry>()
        val totalAssessments =
            goalAssessmentList.sumBy { it.assessments.filter { assessment -> assessment.goalId == goal.id }.size }
        var xAxis = maxOf(Constants.MIN_REQUIRED_X_POINTS, totalAssessments).toFloat()

        val todayAssessment =
            goalAssessmentList.firstOrNull { DateUtils.isToday(it.createdAt.time) }?.assessments?.firstOrNull { assessment -> assessment.goalId == goal.id }
        if (todayAssessment != null) {
            dataEntryList.add(
                0,
                DateEntry(
                    xAxis--,
                    todayAssessment.value.toFloat(),
                    todayAssessment.createdAt.time,
                    showNumber = false
                )
            )
        } else {
            if (totalAssessments >= Constants.MIN_REQUIRED_X_POINTS) {
                xAxis++
            }
            dataEntryList.add(
                0,
                DateEntry(
                    xAxis--,
                    Constants.RATE_IT_POINT,
                    Calendar.getInstance().timeInMillis,
                    showNumber = false
                )
            )
        }
        goalAssessmentList.filterNot { DateUtils.isToday(it.createdAt.time) }
            .sortedByDescending { it.createdAt }
            .onEach { goalAssessment ->
                goalAssessment.assessments.filter { assessment -> assessment.goalId == goal.id }
                    .forEach { assessment ->
                        dataEntryList.add(
                            0,
                            DateEntry(
                                xAxis--,
                                assessment.value.toFloat(),
                                assessment.createdAt.time,
                                showNumber = false
                            )
                        )
                    }
            }

        val unratedCount = Constants.MIN_REQUIRED_X_POINTS - dataEntryList.size - 1
        for (i in 0..unratedCount) {
            val calender = Calendar.getInstance()
            calender.timeInMillis = dataEntryList.first().date!!
            calender.add(Calendar.DATE, -1)
            dataEntryList.add(
                0,
                DateEntry(xAxis--, null, calender.timeInMillis, showNumber = false)
            )
        }
        return dataEntryList
    }

    fun onSplitTypeChanged(id: Int) {
        selectedFeelings = when (id) {
            R.id.sadFaceRadioButton -> FEELING_UNHAPPY
            R.id.normalFaceRadioButton -> FEELING_MEH
            R.id.happyFaceRadioButton -> FEELING_HAPPY
            else -> FEELING_NOTHING
        }
        if (selectedFeelings == FEELING_HAPPY) {
        } else {
        }
    }

    private fun loadTodayActivities(
        forceUpdate: Boolean = false,
        today: Date
    ) {
        viewModelScope.launch() {
            showLoading()
            planManager.getPlansByDate(false, today.formatDayDate())
                .onSuccess { items ->
                    val groupedFoodItems = items.filter {
                        it.food_id != null
                    }.groupBy {
                        it.food_id
                    }

                    val groupedLifeStyleItemsItems = items.filter {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null
                    }.groupBy {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                    }

                    val groupedSupplementItems = items.filter {
                        it.isSupplementPlan()
                    }

                    val groupedTips = items.filter {
                        it.isTip()
                    }.groupBy {
                        it.tip_id
                    }

                    val todayItems =
                        buildTodayActivities(
                            groupedFoodItems,
                            groupedLifeStyleItemsItems,
                            groupedSupplementItems,
                            groupedTips,
                            today
                        )
                    state.todayActivitiesItems.value = listOf(GoalPageTodayActivity(todayItems))

                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun buildTodayActivities(
        groupedFoodItems: Map<Int?, List<PlanData>>,
        groupedLifeStyleItemsItems: Map<Int?, List<PlanData>>,
        groupedSupplementItems: List<PlanData>,
        groupedTips: Map<Int?, List<PlanData>>,
        today: Date
    ): List<PlanItem> {
        val dailyItems = mutableListOf<PlanItem>()
        groupedFoodItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it,
                        grouped,
                        today.formatDayDate()
                    )
                )
            }
        }
        groupedTips.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it,
                        grouped,
                        today.formatDayDate()
                    )
                )
            }
        }
        groupedLifeStyleItemsItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it,
                        grouped,
                        today.formatDayDate()
                    )
                )
            }
        }
        groupedSupplementItems.map { item ->
            dailyItems.add(
                mapTodoToPlanItem(
                    item,
                    groupedSupplementItems,
                    today.formatDayDate()
                )
            )
        }

        val filteredTodayActivities = dailyItems.filter { planItem ->
            planItem.getOccurrenceDates().any { it.isSameDay(today) }
        }

        val planItems = togglePlanItems(filteredTodayActivities, today).sortedBy {
            it.currentPlan.getTimeFormattedAsNumber()
        }
        return planItems
    }

    private fun mapTodoToPlanItem(
        todo: PlanData,
        plans: List<PlanData>,
        date: String? = null
    ): PlanItem {
        val isCompleted = todo.plan_records?.firstOrNull { it.date == date }?.completed == true

        return PlanItem(
            todo.id ?: -1,
            todo.getDisplayName(),
            todo.getDescription(),
            todo.getTimeFormattedWithMeridian() ?: getResString(R.string.set_time),
            todo.getToDoImageUrl(),
            todo.getToDoImageBackground(),
            todo.getDisplayName(),
            isCompleted,
            false,
            1,
            plans,
            plans,
            todo.getPlanType(),
            todo.day_of_week,
            todo.program_dates?.map { Constants.DAY_DATE_FORMAT.parse(it) },
            program = todo.program?.let { programMapper.mapProgram(it) },
            todo
        )
    }

    private fun togglePlanItems(
        dayItems: List<PlanItem>,
        formatPlanDate: Date
    ): List<PlanItem> {
        return dayItems.map {
            it.copy(
                isCompleted = it.currentPlan.plan_records.orEmpty().firstOrNull {
                    it.completed == true && formatPlanDate.isSameDay(
                        Constants.DAY_DATE_FORMAT.parse(
                            it.date
                        )
                    )
                } != null
            )
        }
    }

    override fun onTalkToNutritionistClick(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TALK_TO_NUTRITIONIST_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value =
            GoalPageFragmentDirections.actionGoalPageFragmentToNutritionOptionsFragment()
    }

    override fun onCustomerSupportClick() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CHAT_WITH_SUPPORT)
                .setScreenName(TrackingConstants.FROM_REAGENT_DETAILS)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value =
            GoalPageFragmentDirections.actionGoalPageFragmentToWeb(BuildConfig.HELP_CENTER_URL)
    }

    override fun onReagentItemClick(item: ReagentItem, view: View) {
    }

    private fun loadScores(forceUpdate: Boolean = false) {
        showLoading()
        viewModelScope.launch {
            scoreManager.getWellnessScoresWithAverage(forceUpdate)
                .onSuccess { scoresData ->
                    val score = scoresData.first
                    state {
                        this.score = score
                        loadAssessments()
                        fetchAllGoals()
                    }
                }
        }
    }

    private fun fetchAllGoals() {
        viewModelScope.launch {
            goalsManager.fetchAllGoals()
                .onSuccess { goalsData ->
                    state {
                        val reagents = score?.reagents?.map {
                            val goalsImpacts = mutableMapOf<Int, Int>()
                            goalsData.forEach { goalItem ->
                                goalItem.reagents!!.forEach { reagentData ->
                                    if (reagentData.id == it.key.id) {
                                        goalsImpacts[goalItem.id] = reagentData.impact ?: 0
                                    }
                                }
                            }
                            ReagentItem(
                                id = it.key.id,
                                title = it.key.displayName,
                                unit = it.key.unit,
                                chartEntries = it.value,
                                lowerBound = it.key.lowerBound,
                                upperBound = it.key.upperBound,
                                minY = it.key.minValue,
                                maxY = it.key.maxValue,
                                dimmed = false,
                                ranges = it.key.ranges,
                                isBetaTesting = it.key.isBeta,
                                isInverted = it.key.isInverted,
                                selectedChartEntityPosition = it.value.lastIndex,
                                consumptionUnit = it.key.consumptionUnit,
                                info = it.key.info,
                                state = it.key.state,
                                goalsImpacts = goalsImpacts,
                                impact = null
                            )
                        }
                        state.reagentItems.value = listOf(
                            GoalPageReagent(
                                testsThatAffectsGoal,
                                reagents?.filter {
                                    it.state != ReagentState.COMING_SOON
                                }
                            )
                        )
                    }
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    fun loadRecommendations(goalId: Int) {
        val filterChoice = state.filterSortChoices.value?.firstOrNull()?.itemFilterChoiceType

        viewModelScope.launch {
            assessmentManager.getRecommendations(
                goalId,
                null,
                null,
                sortingByLikes = filterChoice == ItemFilterChoiceType.HIGHEST_RATE,
                sortingByNewest = filterChoice == ItemFilterChoiceType.NEWEST,
                pageNumber = null,
                pageSize = null
            )
                .onSuccess { response ->
                    viewModelScope.launch {
                        planManager.getUserPlans()
                            .onSuccess { planData ->
                                val items = buildRecommendationsList(response, planData)
                                val title = "${goalSelected?.name} Recommendations"
                                state.recommendations.value =
                                    listOf(GoalPageRecommends(title, items))
                            }
                    }
                }
                .onError {
                }
        }
    }

    private fun buildRecommendationsList(
        response: RecommendationsPagingResponse,
        planData: List<PlanData>
    ): List<RecommendationItem> {
        val recommendations = mutableListOf<RecommendationItem>()
        response.sequenceData.filter { it.type == RecommendationsSequenceType.TIP }
            .onEach { sequence ->
                var plans: List<PlanData>? = null
                val item = when (sequence.type) {
                    RecommendationsSequenceType.FOOD -> {
                        plans =
                            planData.filter { it.isFoodPlan() }.filter { it.food_id == sequence.id }
                        val isAddedToPlan = plans.isNotEmpty()
                        response.foods?.first { it.id == sequence.id }
                            ?.copy(isAddedToPlan = isAddedToPlan)
                    }
                    RecommendationsSequenceType.LIFESTYLE -> {
                        plans = planData.filter { it.isStressPlan() }
                            .filter { it.reagent_lifestyle_recommendation_id == sequence.id }
                        val isAddedToPlan = plans.isNotEmpty()
                        response.lifestyle?.first { it.id == sequence.id }
                            ?.copy(isAddedToPlan = isAddedToPlan)
                    }
                    RecommendationsSequenceType.TIP -> {
                        plans = planData.filter { it.isTip() }.filter { it.tip_id == sequence.id }
                        val isAddedToPlan = plans.isNotEmpty()
                        response.tips?.first { it.id == sequence.id }
                            ?.copy(isAddedToPlan = isAddedToPlan)
                    }
                    RecommendationsSequenceType.NOTHING -> null
                }
                item?.let { recommendations.add(RecommendationItem(plans, it)) }
            }
        return recommendations
    }

    private fun verifyReagents() {
        viewModelScope.launch {
            reagentsManager.fetchReagent()
        }
    }

    override fun onActivitiesClick() {
        state.navigateTo.value = HomeScreenFragmentDirections.globalActionToBuildPlan()
    }

    override fun onPlanItemClicked(item: Any, view: View) {
    }

    override fun onItemSelected(item: Any, position: Int) {
        if (item is PlanItem) {
            val date = today.formatDayDate()
            val planRecords = item.currentPlan.plan_records.orEmpty().toMutableList()
            planRecords.add(
                0,
                PlanRecord(
                    date = date,
                    completed = item.isCompleted.not()
                )
            )
            val updatedItem = item.currentPlan.copy(plan_records = planRecords)
            val updatedPlanItem = mapTodoToPlanItem(
                updatedItem,
                item.todos,
                date
            )
            val newItems =
                state.todayActivitiesItems.value?.firstOrNull()?.items?.map { planItem ->
                    if (planItem.id == updatedItem.id) {
                        updatedPlanItem.copy(playAnimation = true)
                    } else {
                        planItem.copy(playAnimation = false)
                    }
                }
            state.todayActivitiesItems.value = listOf(GoalPageTodayActivity(newItems))
            viewModelScope.launch {
                planManager.updatePlanToggle(item.id, date, item.isCompleted.not())
                programManager.markPlanRecordAsCompleted(updatedItem)
            }
        }
    }

    override fun onAnimationFinished(item: Any) {
    }

    override fun onAddReminderClicked(item: Any, view: View) {
        if (item is PlanItem) {
            state.navigateTo.value =
                GoalPageFragmentDirections.actionGoalPageFragmentToPlanReminderDialog(
                    PlanReminderHeader(
                        title = item.title,
                        description = item.valueProp1.orEmpty(),
                        backgroundUrl = item.backgroundUrl,
                        background = item.background,
                        todos = item.plans,
                        isTestingPlan = item.type == PlanType.TestPlan
                    )
                )
        }
    }

    override fun onExpandViewClicked(recommendation: RecommendationItem) {
    }

    override fun onRecommendationItemClicked(recommendation: RecommendationItem) {
        when (recommendation.iRecommendation) {
            is Tip -> {
                state.navigateTo.value =
                    MainNavGraphDirections.globalActionToTipDetailsDialogFragment(recommendation.iRecommendation)
            }
            is Food -> {
                state.navigateTo.value =
                    HomeNavGraphDirections.globalActionToFoodPlanDetailsFragment(
                        FoodDetailsItemModel(
                            recommendation.iRecommendation.getDisplayedTitle()!!,
                            recommendation.iRecommendation.getDisplayedFrequency().orEmpty(),
                            recommendation.iRecommendation.id!!,
                            recommendation.iRecommendation.isAddedToPlan
                        )
                    )
            }
            is Lifestyle -> {
                state.navigateTo.value =
                    HomeNavGraphDirections.globalActionToStressReliefDetailsFragment(
                        StressReliefDetailsItemModel(
                            title = recommendation.iRecommendation.getDisplayedTitle(),
                            description = recommendation.iRecommendation.description ?: "",
                            life_style_recommendation_id = recommendation.iRecommendation.lifestyleRecommendationId,
                            isAdded = recommendation.iRecommendation.isAddedToPlan
                        )
                    )
            }
        }
    }

    override fun onRecommendationItemLikeClicked(recommendation: RecommendationItem) {
        var recommendationId = 0
        var category = ""
        var updatedLikeFlag = false
        var likesCount = 0
        when (recommendation.iRecommendation) {
            is Tip -> {
                recommendationId = recommendation.iRecommendation.id
                category = LikeCategory.Tip.value
                likesCount = recommendation.iRecommendation.likesCount ?: 0
                updatedLikeFlag = recommendation.iRecommendation.likeStatus != LikeStatus.LIKE
            }
            is Food -> {
                recommendationId = recommendation.iRecommendation.id!!
                category = LikeCategory.Food.value
                likesCount = recommendation.iRecommendation.total_likes ?: 0
                updatedLikeFlag = recommendation.iRecommendation.likeStatus != LikeStatus.LIKE
            }
            is Lifestyle -> {
                recommendationId = recommendation.iRecommendation.id
                category = LikeCategory.Lifestyle.value
                likesCount = recommendation.iRecommendation.totalLikes ?: 0
                updatedLikeFlag = recommendation.iRecommendation.likeStatus != LikeStatus.LIKE
            }
        }

        if (updatedLikeFlag) likesCount++ else likesCount--
        val recommends = state.recommendations.value?.firstOrNull()?.items
        val title = state.recommendations.value?.firstOrNull()?.title
        val newItems = recommends?.map {
            val copiedIRecommendation: IRecommendation = when (it.iRecommendation) {
                is Tip -> {
                    if (it.iRecommendation.getType() == recommendation.iRecommendation.getType() && it.iRecommendation.id == recommendationId) {
                        it.iRecommendation.copy(
                            likesCount = likesCount,
                            likeStatus = if (updatedLikeFlag) LikeStatus.LIKE else LikeStatus.DISLIKE
                        )
                    } else it.iRecommendation.copy()
                }
                is Food -> {
                    if (it.iRecommendation.getType() == recommendation.iRecommendation.getType() && it.iRecommendation.id == recommendationId) {
                        it.iRecommendation.copy(
                            total_likes = likesCount,
                            like_status = if (updatedLikeFlag) LikeStatus.LIKE.value else LikeStatus.DISLIKE.value
                        )
                    } else it.iRecommendation.copy()
                }
                is Lifestyle -> {
                    if (it.iRecommendation.getType() == recommendation.iRecommendation.getType() && it.iRecommendation.id == recommendationId) {
                        it.iRecommendation.copy(
                            totalLikes = likesCount,
                            likeStatus = if (updatedLikeFlag) LikeStatus.LIKE else LikeStatus.DISLIKE
                        )
                    } else it.iRecommendation.copy()
                }
                else -> {
                    it.iRecommendation
                }
            }
            it.copy(iRecommendation = copiedIRecommendation)
        }
        title?.let {
            state.recommendations.value = listOf(GoalPageRecommends(it, newItems))
        }

        viewModelScope.launch {
            if (updatedLikeFlag) {
                recommendationManager.sendLikesStatus(
                    recommendationId,
                    category,
                    updatedLikeFlag
                )
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.CONTENT_LIKED)
                        .addProperty(TrackingConstants.KEY_ID, recommendationId.toString())
                        .addProperty(TrackingConstants.TYPE, category)
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
            } else {
                recommendationManager.unLikesStatus(recommendationId, category)
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.CONTENT_UNLIKED)
                        .addProperty(TrackingConstants.KEY_ID, recommendationId.toString())
                        .addProperty(TrackingConstants.TYPE, category)
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
            }
        }
    }

    override fun onLoadMoreClicked(paging: Paging) {
    }

    override fun onRecommendationAddToPlanClicked(recommendation: RecommendationItem) {
        val isAddedToPlan = when (recommendation.iRecommendation) {
            is Food -> recommendation.iRecommendation.isAddedToPlan
            is Lifestyle -> recommendation.iRecommendation.isAddedToPlan
            is Tip -> recommendation.iRecommendation.isAddedToPlan
            else -> false
        }
        if (isAddedToPlan.not()) {
            val plans = if (recommendation.planData.isNullOrEmpty().not()) {
                recommendation.planData!!
            } else {
                listOf(getPlanData(recommendation))
            }
            val planReminderHeader = PlanReminderHeader(
                recommendation.iRecommendation.getDisplayedTitle(),
                recommendation.iRecommendation.getDisplayedFrequency(),
                recommendation.iRecommendation.getBackgroundImageUrl(),
                null,
                plans,
                isAddedToPlan.not()
            )
            state.navigateTo.value =
                MainNavGraphDirections.globalActionToPlanReminderDialog(planReminderHeader)
        } else {
            recommendation.planData?.let { plans ->
                deletePlanItem(plans)
            }
        }
    }

    private fun getPlanData(item: RecommendationItem): PlanData = when (item.iRecommendation) {
        is Food -> PlanData(
            food_id = item.iRecommendation.id ?: 0,
            food = item.iRecommendation,
            multiple = 1
        )
        is Lifestyle -> PlanData(
            reagent_lifestyle_recommendation_id = item.iRecommendation.lifestyleRecommendationId,
            reagent_lifestyle_recommendation = ReagentLifestyleRecommendation(
                lifestyle_recommendation_id = item.iRecommendation.lifestyleRecommendationId,
                quantity = item.iRecommendation.quantity?.toFloat()!!,
                frequency = item.iRecommendation.frequency,
                unit = item.iRecommendation.unit,
                reagent_bucket_id = item.iRecommendation.reagentBucketId,
                activity_name = item.iRecommendation.activityName,
                image_url = item.iRecommendation.imageUrl,
                id = -1
            ),
            multiple = 1
        )
        is Tip -> PlanData(
            tip_id = item.iRecommendation.id,
            tip = tipMapper.mapTip(item.iRecommendation),
            multiple = 1
        )
        else -> PlanData()
    }

    private fun deletePlanItem(plans: List<PlanData>) {
        showLoading()
        viewModelScope.launch {
            planManager.deletePlanItem(plans.map { it.id ?: -1 })
                .onSuccess {
                    hideLoading(false)
                    planManager.updatePlanCachedData()
                    reminderManager.updateRemindersCachedData()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(
                                TrackingConstants.KEY_ID,
                                plans.map { it.id ?: -1 }.toString()
                            )
                            .addProperty(
                                TrackingConstants.TYPE,
                                plans.first().getPlanType().title
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Result Page")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun onRateClick(goalRate: GoalRate) {
        Constants.GOALBUTTONS.firstOrNull {
            it.id == goalSelected?.id
        }?.let { goalButton ->
            if (goalRate.value != goalButton.chartEntries[goalButton.selectedChartEntityPosition].y.toInt()) {
                val today = ContactRepository.SERVER_DATE_FORMAT.format(Date())
                val chartEntries = goalButton.chartEntries.mapIndexed { index, dateEntry ->
                    if (index == goalButton.selectedChartEntityPosition) {
                        DateEntry(
                            dateEntry.x,
                            goalRate.value.toFloat(),
                            dateEntry.date,
                            dateEntry.reagentsCount,
                            dateEntry.showNumber
                        )
                    } else {
                        dateEntry
                    }
                }
                state.goalHeaderItems.value = GoalPageHeader(chartEntries = chartEntries)
                viewModelScope.launch {
                    assessmentManager.putGoalAssessments(goalButton.id, goalRate.value, today)
                }
            }
        }
    }
}
