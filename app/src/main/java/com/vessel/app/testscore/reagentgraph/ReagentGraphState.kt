package com.vessel.app.testscore.reagentgraph

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.reagent.model.ReagentHeader
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ReagentGraphState @Inject constructor(val reagentHeader: ReagentHeader) {
    val reagentHeaderItems = MutableLiveData(reagentHeader)
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: ReagentGraphState.() -> Unit) = apply(block)
}
