package com.vessel.app.checkin.weekly

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.checkin_body_layout.*
import kotlinx.android.synthetic.main.checkin_body_layout.view.*
import kotlinx.android.synthetic.main.fragment_weekly_membership_checkin.*

@AndroidEntryPoint
class WeeklyCheckInFragment : BaseFragment<WeeklyCheckInViewModel>() {
    override val viewModel: WeeklyCheckInViewModel by viewModels()
    override val layoutResId = R.layout.fragment_weekly_membership_checkin

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()

        observe(viewModel.dismissDialog) { }
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(status) {
                body.lblStatus.text = it
            }
            observe(progress) {
                body.progressView.setProgress(it)
            }
        }
    }

    private fun setupViews() {
    }
}
