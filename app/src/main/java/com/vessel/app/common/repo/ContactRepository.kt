package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.Contact
import com.vessel.app.common.net.mapper.AuthMapper
import com.vessel.app.common.net.mapper.ContactMapper
import com.vessel.app.common.net.payload.*
import com.vessel.app.common.net.service.ContactService
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val contactService: ContactService,
    private val contactMapper: ContactMapper,
    private val authMapper: AuthMapper
) : BaseNetworkRepository(moshi, loggingManager) {
    suspend fun createContact(
        email: String,
        password: String,
        firstName: String,
        lastName: String,
        gender: Contact.Gender?,
        height: Contact.Height?,
        weight: Contact.Weight?,
        birthDate: Date?
    ) = executeWithErrorMessage(authMapper::map) {
        contactService.createContact(
            ContactPayload(
                email = email,
                password = password,
                first_name = firstName,
                last_name = lastName,
                gender = gender?.encoding,
                height = height?.toCentimeters(),
                weight = weight?.toKg(),
                birth_date = birthDate?.let { SERVER_DATE_FORMAT.format(it) }
            )
        )
    }

    suspend fun checkEmailExistence(email: String) = executeWithErrorMessage {
        contactService.checkEmailExistence(
            EmailExistencePayload(email = email)
        )
    }

    suspend fun updateContact(
        firstName: String,
        lastName: String,
        gender: Contact.Gender,
        height: Contact.Height?,
        weight: Contact.Weight?,
        birthDate: Date?,
        goals: List<Int>,
        mainGoalId: Int?
    ) = executeWithErrorMessage(contactMapper::map) {
        contactService.updateContact(
            ContactPayload(
                first_name = firstName,
                last_name = lastName,
                gender = gender.encoding,
                height = height?.toCentimeters(),
                weight = weight?.toKg(),
                birth_date = birthDate?.let { SERVER_DATE_FORMAT.format(it) },
                goals = goals,
                main_goal_id = mainGoalId
            )
        )
    }

    suspend fun updateContactTimezone(
        firstName: String,
        lastName: String,
        gender: Contact.Gender?,
        height: Contact.Height?,
        weight: Contact.Weight?,
        birthDate: Date?,
        goals: List<Int>,
        timeZone: String,
        mainGoalId: Int?
    ) {
        contactService.updateContact(
            ContactPayload(
                first_name = firstName,
                last_name = lastName,
                gender = gender?.encoding,
                height = height?.toCentimeters(),
                weight = weight?.toKg(),
                birth_date = birthDate?.let { SERVER_DATE_FORMAT.format(it) },
                goals = goals,
                time_zone = timeZone,
                main_goal_id = mainGoalId
            )
        )
    }

    suspend fun getContact() = execute(contactMapper::map) { contactService.getContact() }

    suspend fun getContactById(id: Int?) = execute(contactMapper::map) { contactService.getContactById(id) }

    suspend fun changePassword(currentPassword: String, newPassword: String) = executeWithErrorMessage {
        contactService.changePassword(
            ChangePasswordPayload(
                current_password = currentPassword,
                new_password = newPassword
            )
        )
    }

    suspend fun addDiet(diets: List<String>) = executeWithErrorMessage { contactService.addDiet(DietsPayload(diets)) }

    suspend fun addAllergies(allergies: List<String>) = executeWithErrorMessage { contactService.addAllergies(AllergiesPayload(allergies)) }

    suspend fun deleteDiet(diets: List<String>) = executeWithErrorMessage { contactService.deleteDiet(DietsPayload(diets)) }

    suspend fun deleteAllergies(allergies: List<String>) = executeWithErrorMessage { contactService.deleteAllergies(AllergiesPayload(allergies)) }

    companion object {
        val SERVER_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val SERVER_LAST_LOGIN_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH)
    }
}
