package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import com.vessel.app.Constants
import com.vessel.app.common.net.data.plan.GoalRecord
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class TipData(
    val tip_id: Int,
    val title: String,
    val image_url: String?,
    val description: String?,
    val goals: List<GoalRecord>?,
    val frequency: String?,
    val total_likes: Int?,
    val dislikes_count: Int?,
    val like_status: String?,
    val contact: ContactData?,
    val sources: List<ScienceSourceData>?,
    val main_goal_id: Int?
) : Parcelable {
    fun getDisplayedDescription(): String {
        if (contact?.is_verified == true) {
            if (main_goal_id == null)
                return ""
            val fullName = contact.first_name?.plus(" ")?.plus(contact.last_name)
            val result = StringBuilder("This is a recommendation")
            if (fullName.isNullOrEmpty().not()) {
                result.append(" from $fullName")
            }
            result.append(" to help with ${Constants.GOALS.filter { it.id == main_goal_id }.joinToString { it.name }}")
            return result.toString()
        } else {
            return ""
        }
    }
}
