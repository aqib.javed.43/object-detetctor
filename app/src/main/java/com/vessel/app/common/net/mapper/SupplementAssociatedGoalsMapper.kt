package com.vessel.app.common.net.mapper

import com.vessel.app.common.net.data.SupplementAssociatedGoalData
import com.vessel.app.supplementsbucket.model.SupplementAssociatedGoalSample
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SupplementAssociatedGoalsMapper @Inject constructor() {
    fun map(data: List<SupplementAssociatedGoalData>) = data.map { map(it) }

    fun mapToGoalData(data: List<SupplementAssociatedGoalSample>) = data.map { map(it) }

    fun map(data: SupplementAssociatedGoalData) = SupplementAssociatedGoalSample(
        id = data.id,
        name = data.name,
        impact = data.impact
    )

    fun map(data: SupplementAssociatedGoalSample) = SupplementAssociatedGoalData(
        id = data.id,
        name = data.name,
        impact = data.impact
    )
}
