package com.vessel.app.posttestlaststep

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.util.ResourceRepository

class PostTestLastStepViewModel @ViewModelInject constructor(
    val state: PostTestLastStepState,
    val resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val homeTabsManager: HomeTabsManager,
) : BaseViewModel(resourceProvider) {
    fun onGotItClicked(view: View) {
        homeTabsManager.clear()
        view.findNavController().navigate(PostTestNavGraphDirections.actionGoToHome())
    }

    fun getTipImage(): Int {
        if (preferencesRepository.testCompletedCount == SUPPLEMENTS_TEST_COUNT_STEP) {
            return R.drawable.find_supplements_tip_image
        } else {
            return R.drawable.find_nutritionist_tip_image
        }
    }

    fun getTipTitle(): Int {
        if (preferencesRepository.testCompletedCount == SUPPLEMENTS_TEST_COUNT_STEP) {
            return R.string.find_supplements_title
        } else {
            return R.string.find_nutritionist_title
        }
    }

    companion object {
        const val SUPPLEMENTS_TEST_COUNT_STEP = 2
    }
}
