Water is the most abundant molecule in our body, making up 70% of each cell. This is why hydrating with mineral water is the best way to truly hydrate. Beverages that contain other molecules like sugar, caffeine, proteins, flavorings, or dyes do not count toward your daily goal.

If you find yourself forgetting to drink throughout the day, here are a few tips for staying hydrated.

Get your water in first thing when you wake up, aim to drink 16-20 ounces of water before you head out for the day.

Use your phone to set timers and reminders.

Use a water bottle that you love - get one that is convenient to carry, attractive to look at, and easy to drink from. Keep this next to your working space, staying in sight.

Add something special to spruce it up - some cucumber slices, fresh mint, a squeeze of lime.

Avoid artificial flavorings that add chemicals and sweeteners.

Check out your recommendations in our lifestyle section to see how much more water you should be drinking based on your specific gravity and other lab findings.

**references:**

-   https://www.myfooddata.com/articles/high-electrolyte-foods.php

-   [Kalman DS](https://www.ncbi.nlm.nih.gov/pubmed/?term=Kalman%20DS%5BAuthor%5D&cauthor=true&cauthor_uid=22257640)1, [Feldman S](https://www.ncbi.nlm.nih.gov/pubmed/?term=Feldman%20S%5BAuthor%5D&cauthor=true&cauthor_uid=22257640), [Krieger DR](https://www.ncbi.nlm.nih.gov/pubmed/?term=Krieger%20DR%5BAuthor%5D&cauthor=true&cauthor_uid=22257640), [Bloomer RJ](https://www.ncbi.nlm.nih.gov/pubmed/?term=Bloomer%20RJ%5BAuthor%5D&cauthor=true&cauthor_uid=22257640). Comparison of coconut water and a carbohydrate-electrolyte sport drink on measures of hydration and physical performance in exercise-trained men.[J Int Soc Sports Nutr.](https://www.ncbi.nlm.nih.gov/pubmed/22257640#) 2012 Jan 18;9(1):1. doi: 10.1186/1550-2783-9-1. PMID 22257640

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5451964/

-   Cooper GM. The Cell: A Molecular Approach. 2nd edition. Sunderland (MA): Sinauer Associates; 2000. The Molecular Composition of Cells. Available from: https://www.ncbi.nlm.nih.gov/books/NBK9879/

-   Flasar, C. (2008). What is urine specific gravity? Nursing, 38(7), 14. https://doi.org/10.1097/01.nurse.0000325315.41513.a0
