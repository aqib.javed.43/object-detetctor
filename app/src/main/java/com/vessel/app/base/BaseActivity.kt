package com.vessel.app.base

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.*
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.*
import androidx.navigation.ActivityNavigator
import com.tapadoo.alerter.Alerter
import com.vessel.app.BR
import com.vessel.app.R
import com.vessel.app.auth.AuthActivity
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.splashscreen.SplashScreenFragmentArgs
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {
    protected abstract val viewModel: VM
    protected abstract val layoutResId: Int

    @Inject
    protected lateinit var authManager: AuthManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        observeAuthState()

        onBeforeViewLoad(savedInstanceState)

        DataBindingUtil.setContentView<ViewDataBinding>(this, layoutResId)
            ?.apply {
                setVariable(BR.viewModel, viewModel)
                lifecycleOwner = this@BaseActivity
            }

        setupObservers()

        onViewLoad(savedInstanceState)

        observe(viewModel.showAlert) {
            Alerter.create(this)
                .setTitle(it.title)
                .setText(it.body)
                .setBackgroundColorRes(R.color.colorAccent)
                .enableSound(true)
                .show()
        }
    }

    private fun observeAuthState() {
        lifecycleScope.launch {
            authManager.getAuthState().collect {
                if (!it) {
                    ActivityNavigator(this@BaseActivity).apply {
                        navigate(
                            createDestination()
                                .setIntent(Intent(this@BaseActivity, AuthActivity::class.java)),
                            SplashScreenFragmentArgs(true).toBundle(),
                            null,
                            null
                        )
                    }
                    finish()
                }
            }
        }
    }

    private fun setupObservers() {
        observe(viewModel.navigateBack) { navigateUp() }
    }

    open fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        // Intentionally empty so that subclasses can override if necessary
    }

    open fun onViewLoad(savedInstanceState: Bundle?) {
        // Intentionally empty so that subclasses can override if necessary
    }

    protected fun <T> observe(liveData: LiveData<T>, observer: (T) -> Unit) {
        liveData.observe(this, Observer { observer(it) })
    }

    protected fun getRuntimePermissions(permissions: List<String>, permissionRequestCode: Int) {
        val allNeededPermissions = mutableListOf<String>()
        if (!allPermissionsGranted(permissions)) {
            allNeededPermissions.addAll(
                permissions.filter {
                    ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED
                }
            )
        }

        if (allNeededPermissions.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                allNeededPermissions.toTypedArray(),
                permissionRequestCode
            )
        }
    }

    private fun allPermissionsGranted(permissions: List<String>) =
        permissions.all {
            ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
        }

    fun navigateUp() {
        NavUtils.getParentActivityIntent(this)?.let {
            if (NavUtils.shouldUpRecreateTask(this, it) || this.isTaskRoot) {
                TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(it)
                    .startActivities()
            } else {
                NavUtils.navigateUpTo(this, it)
            }
        }
    }
}
