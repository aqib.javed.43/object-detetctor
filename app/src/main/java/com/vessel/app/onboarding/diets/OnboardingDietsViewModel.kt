package com.vessel.app.onboarding.diets

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Diet
import com.vessel.app.common.model.Result
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.diets.model.OnboardingDietSelect
import com.vessel.app.onboarding.diets.ui.OnboardingDietSelectAdapter
import com.vessel.app.onboarding.diets.ui.OnboardingDietsFragmentDirections
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class OnboardingDietsViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    val contactManager: ContactManager,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider), ToolbarHandler, OnboardingDietSelectAdapter.OnActionHandler {

    val state = OnboardingDietsState(
        savedStateHandle["isFirstTestFlow"]!!,
        savedStateHandle["isLastStep"]!!,
    )
    val isBackVisible = savedStateHandle.get<Boolean>("showBackButton")

    init {
        state {
            items.value = Diet.values()
                .map {
                    OnboardingDietSelect(
                        it.title,
                        false,
                        it.isNone
                    )
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onDietSelectClicked(item: OnboardingDietSelect) {
        if (item.isNone && item.checked.not()) {
            state.items.value = state.items.value.orEmpty().map { it.copy(checked = false) }
        }
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, diet ->
                    if (diet.title == item.title) diet.copy(checked = !item.checked)
                    else if (diet.isNone) diet.copy(checked = false)
                    else diet
                }.toMutableList()
            refreshItems.value = true
        }
    }

    private fun getSelectedDiets() = state.items.value.orEmpty()
        .filter { it.checked }
        .mapNotNull { selectedItem ->
            Diet.values().firstOrNull {
                it.title == selectedItem.title
            }?.apiName
        }

    private fun getUnSelectedDiets() = state.items.value.orEmpty()
        .filter { !it.checked }
        .mapNotNull { unSelectedItem ->
            Diet.values().firstOrNull {
                it.title == unSelectedItem.title
            }?.apiName
        }

    fun onNextClicked() {
        showLoading()
        state {
            viewModelScope.launch {
                val addDietResponse = contactManager.addDiet(getSelectedDiets())
                val removeUnSelectedDietResponse = contactManager.deleteDiet(getUnSelectedDiets())

                if (addDietResponse is Result.Success<*> && removeUnSelectedDietResponse is Result.Success) {
                    preferencesRepository.setContactDiets(getSelectedDiets())
                    hideLoading()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.DIET_SUBMITTED)
                            .setScreenName(TrackingConstants.FROM_ONBOARDING)
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    navigateTo.value =
                        OnboardingDietsFragmentDirections.actionOnboardingDietsFragmentToOnboardingAllergiesFragment(
                            isFirstTestFlow,
                            isLastStep
                        )
                } else {
                    if (isFirstTestFlow) {
                        val errorMessage = getResString(R.string.unknown_error)
                        showError(errorMessage)
                        hideLoading()
                    } else {
                        navigateTo.value =
                            OnboardingDietsFragmentDirections.actionOnboardingDietsFragmentToOnboardingAllergiesFragment(
                                isFirstTestFlow,
                                isLastStep
                            )
                    }
                }
            }
        }
    }
}
