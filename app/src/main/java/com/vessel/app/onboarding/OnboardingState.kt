package com.vessel.app.onboarding

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.base.MutableLiveArrayList
import com.vessel.app.onboarding.ui.OnboardingPage
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnboardingState @Inject constructor() {
    var pagePosition = 0
    val title = MutableLiveData(R.string.gender)
    val subtitle = MutableLiveData(R.string.gender_subtitle)
    val goToNextPage = LiveEvent<Unit>()
    val goToPreviousPage = LiveEvent<Unit>()
    val nextButtonEnabled = MutableLiveData(true)
    val closeEvent = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()

    val pages = MutableLiveArrayList<OnboardingPage>()

    operator fun invoke(block: OnboardingState.() -> Unit) = apply(block)
}
