package com.vessel.app.nutritioncoach

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.BuildConfig
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository

class NutritionCoachViewModel @ViewModelInject constructor(
    val state: NutritionCoachState,
    val resourceRepository: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository), ToolbarHandler {

    fun onMakeAnAppointmentClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.BOOK_NUTRITIONIST_TAPPED)
                .setScreenName(TrackingConstants.FROM_MENU)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        view.findNavController().navigate(PostTestNavGraphDirections.globalActionToWeb(BuildConfig.NUTRITIONIST_URL))
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
}
