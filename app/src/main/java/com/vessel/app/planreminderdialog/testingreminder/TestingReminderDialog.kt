package com.vessel.app.planreminderdialog.testingreminder

import android.content.Intent
import android.os.Bundle
import android.widget.TimePicker
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.home.HomeActivity
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.weekdays.WeekDaysWidget
import com.vessel.app.views.weekdays.getDaysOfWeek
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class TestingReminderDialog : BaseFragment<TestingReminderViewModel>() {
    override val viewModel: TestingReminderViewModel by viewModels()
    override val layoutResId = R.layout.fragment_testing_reminder_dialog
    lateinit var timePicker: TimePicker
    lateinit var weekDaysView: WeekDaysWidget

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)
        timePicker = requireView().findViewById(R.id.timePicker)
        weekDaysView = requireView().findViewById(R.id.weekDaysView)
        val dayOfWeek = getDaysOfWeek(requireContext()).firstOrNull {
            it.calendarDay == Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        }?.planIndex ?: Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        weekDaysView.setSelectedDays(listOf(dayOfWeek))
        timePicker.hour = Calendar.getInstance().get(Calendar.HOUR)
        timePicker.minute = Calendar.getInstance().get(Calendar.MINUTE)

        observe(viewModel.state.dismissDialog) { navigateToHomeActivity() }

        observe(viewModel.state.setRemindersEvent) {
            onReminderAdded()
        }

        observe(viewModel.state.skipRemindersEvent) {
            navigateToHomeActivity()
        }
    }

    private fun navigateToHomeActivity() {
        val intent = Intent(requireContext(), HomeActivity::class.java)
        context?.startActivity(intent)
        activity?.finishAffinity()
    }

    private fun onReminderAdded() {
        viewModel.onReminderAdded(
            timeOfTheDay = "${String.format("%02d", timePicker.hour)}:${String.format("%02d", timePicker.minute)}",
            selectedDays = weekDaysView.getSelectedDays(),
            quantity = 1f
        )
    }
}
