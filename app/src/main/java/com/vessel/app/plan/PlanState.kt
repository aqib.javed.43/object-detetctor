package com.vessel.app.plan

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.plan.model.*
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PlanState @Inject constructor() {

    val planItems = MutableLiveData<Pair<List<DayPlanItem>, Boolean>>()
    val deletePlanItem = LiveEvent<Int>()
    val addPlanChatFooterItem = MutableLiveData<PlanChatFooter>()
    val showPlanNotification = LiveEvent<Boolean>()
    val readyToPopulate = LiveEvent<Unit>()
    val planNotificationCount = MutableLiveData<Int>()
    val openMessaging = LiveEvent<Unit>()
    var isDailyView: Boolean = true
    var todayTodosData: List<DayPlanItem> = listOf()
    var weeklyTodosData: List<DayPlanItem> = listOf()
    val showProgramLoading = MutableLiveData(false)
    val navigateTo = LiveEvent<NavDirections>()
    val removeReminder = LiveEvent<Triple<PlanItem, Int, Int>>()
    val programsItems = MutableLiveData<List<Program>>()
    operator fun invoke(block: PlanState.() -> Unit) = apply(block)
}
