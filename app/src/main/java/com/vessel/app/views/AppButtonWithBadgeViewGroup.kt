package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.withStyledAttributes
import com.vessel.app.R
import kotlinx.android.synthetic.main.app_button_with_badge_view_group.view.*
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.blurView
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.button

open class AppButtonWithBadgeViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.app_button_with_badge_view_group, this)
        context.withStyledAttributes(attrs, R.styleable.AppButtonWithBadgeViewGroup) {
            buttonBadge.text = getText(R.styleable.AppButtonWithBadgeViewGroup_badgeText)
            buttonBadge.visibility = if (getBoolean(R.styleable.AppButtonWithBadgeViewGroup_badgeVisibility, true)) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }

    fun setText(text: String) {
        button.text = text
    }

    fun setText(@StringRes stringResId: Int) {
        button.setText(stringResId)
    }

    fun setBadgeVisibility(boolean: Boolean) {
        buttonBadge.visibility = if (boolean) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    fun setBadgeText(text: String) {
        buttonBadge.text = text
    }

    fun setBadgeText(@StringRes stringResId: Int) {
        buttonBadge.setText(stringResId)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        button.setOnClickListener(l)
    }
}

class WhiteAppWithBadgeButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppButtonWithBadgeViewGroup(context, attrs, defStyle) {

    init {
        button.setTextColor(ContextCompat.getColor(context, R.color.darkText))
        blurView.setOverlayColor(ContextCompat.getColor(context, R.color.whiteAlpha40))
    }
}

class BlackAppWithBadgeButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppButtonWithBadgeViewGroup(context, attrs, defStyle) {

    init {
        button.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        blurView.setOverlayColor(ContextCompat.getColor(context, R.color.blackAlpha70))
    }
}
