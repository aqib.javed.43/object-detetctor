package com.vessel.app.programs.programselection

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.model.Goal
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProgramSelectionFragment : BaseFragment<ProgramSelectionViewModel>() {
    override val viewModel: ProgramSelectionViewModel by viewModels()
    override val layoutResId = R.layout.fragment_program_selection
    private lateinit var programsList: RecyclerView
    private lateinit var goalName: AppCompatTextView
    private lateinit var backButton: View
    private val programAdapter by lazy { ProgramSelectionAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        goalName = requireView().findViewById(R.id.goalNameProgram)
        programsList = requireView().findViewById(R.id.programsList)
        backButton = requireView().findViewById(R.id.toolbar)
        if (viewModel.isBackVisible == true) {
            backButton.isVisible = false
            requireActivity().onBackPressedDispatcher.addCallback(this) {}
        }
        val goal = Goal.values().firstOrNull { it.id == viewModel.state.goalIdSelected.value }
        val goalTitle = goal?.title?.let { getString(it) }
        goalName.text = getString(R.string.goal_program_template, goalTitle)
        programsList.apply {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = programAdapter
        }
    }

    private fun setupObservers() {
        observe(viewModel.state.programsItems) {
            programAdapter.submitList(it)
        }
        observe(viewModel.state.navigateTo) {
            findNavController().navigate(it)
        }
    }
}
