package com.vessel.app.stressrelief.model

data class StressReliefHeaderViewItem(
    val buttonText: String,
    val showLoadingCortisolLevel: Boolean = false,
    val isDailySelected: Boolean = true,
    val showTabs: Boolean = true
)
