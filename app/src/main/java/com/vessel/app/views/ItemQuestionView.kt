package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import kotlinx.android.synthetic.main.item_question_view.view.*

open class ItemQuestionView@JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {
    var onSelected: ((View) -> Unit)? = null
    fun setQuestion(question: String?) {
        lblQuestion.text = question
    }

    init {
        inflate(context, R.layout.item_question_view, this)
        initView()
    }

    private fun initView() {
        swSelected.setOnCheckedChangeListener { compoundButton, b ->
            onSelected?.invoke(this)
        }
    }
}
