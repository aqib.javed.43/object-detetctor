package com.vessel.app.wellness.filterpopup

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.model.Tag

class FilterDialogViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider),
    FilterCategoryAdapter.OnActionHandler {

    val dismissDialog = LiveEvent<Unit>()
    private val filterCategoryList =
        savedStateHandle.get<Array<ItemFilterCategory>>(FilterDialogFragment.SELECTED_FILTER_OPTIONS)
    val tagsList = savedStateHandle.get<Array<Tag>>(FilterDialogFragment.SELECTED_FILTER_TAGS)
    val filterCategory = MutableLiveData<List<ItemFilterCategory>>(filterCategoryList?.toList())

    override fun onFilterSelectChecked(
        selectedCategory: ItemFilterCategory,
        selectedChoice: ItemFilterChoice,
        checked: Boolean
    ) {
        filterCategory.value = filterCategory.value?.map { category ->
            val preSelectedChoice = if (category.singleSelection)
                category.choiceItems.firstOrNull { it.isChecked }
            else null
            when {
                preSelectedChoice?.title == selectedChoice.title -> category.copy()
                category.title == selectedCategory.title -> {
                    val updatedChoices = category.choiceItems.map { choice ->
                        if (choice.title == selectedChoice.title)
                            choice.copy(isChecked = checked)
                        else
                            choice.copy(isChecked = if (selectedCategory.singleSelection) checked.not() else choice.isChecked)
                    }
                    category.copy(choiceItems = updatedChoices)
                }
                else -> category.copy()
            }
        }
    }

    fun onCloseDialogClicked() {
        dismissDialog.call()
    }
}
