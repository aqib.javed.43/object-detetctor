package com.vessel.app.wellness.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScienceSource(
    val id: Int,
    val title: String,
    val description: String?,
    val sourceUrl: String
) : Parcelable
