package com.vessel.app.common.net.payload

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ForgotPasswordPayload(
    val email: String
)
