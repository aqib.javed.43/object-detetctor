package com.vessel.app.common.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ErrorMessageItem(
    val code: Int?,
    val label: String?
)
