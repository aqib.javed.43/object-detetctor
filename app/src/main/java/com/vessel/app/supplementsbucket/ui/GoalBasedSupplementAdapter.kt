package com.vessel.app.supplementsbucket.ui

import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.supplementsbucket.model.GoalViewItem
import com.vessel.app.supplementsbucket.model.Supplement

class GoalBasedSupplementAdapter(
    private val handler: SupplementBucketAdapter.OnActionHandler
) : BaseAdapterWithDiffUtil<Pair<GoalViewItem, List<Supplement>>>(
    areContentsTheSame = { oldItem, newItem ->
        oldItem.second === newItem.second
    }
) {
    override fun getLayout(position: Int) = R.layout.item_goal_based_supplements

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<Pair<GoalViewItem, List<Supplement>>>, position: Int) {
        super.onBindViewHolder(holder, position)
        val supplements = holder.itemView.findViewById<RecyclerView>(R.id.supplements)
        // TODO enhance the way of passing the adapter to the recyclerview inside onBindViewHolder
        val supplementsAdapter = SupplementBucketAdapter(handler)
        supplements.adapter = supplementsAdapter
        supplementsAdapter.submitList(getItem(position).second)
    }
}
