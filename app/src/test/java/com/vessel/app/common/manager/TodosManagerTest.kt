package com.vessel.app.common.manager

import com.vessel.app.BaseFaker.Fake.FAKER
import com.vessel.app.BaseTest
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Todo
import com.vessel.app.common.repo.TodosRepository
import com.vessel.app.isEqualTo
import com.vessel.app.todo.TodoFaker
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TodosManagerTest : BaseTest() {
    private lateinit var todosManager: TodosManager

    private val todosRepository = mockk<TodosRepository>()

    @BeforeEach
    fun setUp() {
        todosManager = TodosManager(todosRepository)
    }

    @Test
    fun `it should return the recurring todos from the repo`() = runBlockingTest {
        val result = Result.Success(TodoFaker.list())
        coEvery { todosRepository.getRecurringTodos() }
            .answers { result }

        todosManager.getRecurringTodos() isEqualTo result
    }

    @Test
    fun `it should create and add a todo to the repo with the given name and amount`() = runBlockingTest {
        val todoApiName = FAKER.cat().name()
        val todoAmount = FAKER.number().randomDigit()
        val todoInsertDate = FAKER.date().birthday()
        val result = Result.Success(TodoFaker.basic())
        coEvery { todosRepository.addRecurringTodo(Todo(-1, todoApiName, todoAmount.toFloat(), todoInsertDate, 0)) }
            .answers { result }

        todosManager.addRecurringTodo(todoApiName, todoAmount.toFloat(), todoInsertDate) isEqualTo result
    }

    @Test
    fun `it should delete the recurring todo in the repo from the given ID`() = runBlockingTest {
        val todoId = FAKER.number().randomDigit()
        coEvery { todosRepository.deleteRecurringTodo(todoId) } returns Result.Success(Unit)

        todosManager.deleteRecurringTodo(todoId)

        coVerify { todosRepository.deleteRecurringTodo(todoId) }
    }
}
