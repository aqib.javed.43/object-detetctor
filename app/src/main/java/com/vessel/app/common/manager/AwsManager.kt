package com.vessel.app.common.manager

import android.hardware.camera2.CaptureResult
import android.os.Build
import android.util.Log
import com.amazonaws.mobileconnectors.s3.transferutility.*
import com.amazonaws.services.s3.model.ObjectMetadata
import com.vessel.app.BuildConfig
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AwsManager @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    private val transferUtility: TransferUtility,
    private val loggingManager: LoggingManager,
    private val testCardQrCodeMetadataManager: TestCardQrCodeMetadataManager,
    private val remoteConfiguration: RemoteConfiguration
) {

    private val s3Bucket: String = remoteConfiguration.getString(S3_BUCKET_NAME)

    fun uploadFileToS3(
        uuid: String,
        file: File,
        jpgFile: File?,
        wellnessCardBatchId: String?,
        wellnessCardCalibrationMode: String?,
        orcaSheetName: String?,
        captureResult: CaptureResult?,
        timerStartDate: String,
        captureDate: String,
        onUploadCompleted: (() -> Unit)? = null,
        onUploadFailed: (() -> Unit)? = null,
        onProgressChanged: ((percent: Int) -> Unit)? = null,
        onUploadWaitingForNetwork: (() -> Unit)? = null
    ) {
        TransferNetworkLossHandler.getInstance()
        val dngKey = "$uuid.dng"
        val jpgKey = "$uuid.jpg"

        var metadata = ObjectMetadata().apply {
            userMetadata["contact-id"] = preferencesRepository.contactId.toString()
            userMetadata["wellness-card-batch-id"] = wellnessCardBatchId.orEmpty()
            userMetadata["orca-sheet-name"] = orcaSheetName.orEmpty()
            userMetadata["calibration-mode"] = wellnessCardCalibrationMode.orEmpty()
            userMetadata["card-version"] = "2.0"
            userMetadata["os-version"] = Build.VERSION.SDK_INT.toString()
            userMetadata["phone-model"] = "${Build.MANUFACTURER} ${Build.MODEL}"
            userMetadata["timer-start-date"] = timerStartDate
            userMetadata["exposure-duration"] =
                captureResult?.get(CaptureResult.SENSOR_EXPOSURE_TIME)
                    ?.div(1_000f)
                    ?.toString().orEmpty()
            userMetadata["exposure-iso"] = captureResult?.get(CaptureResult.SENSOR_SENSITIVITY)
                ?.toString().orEmpty()
            userMetadata["image-capture-date"] = captureDate
            userMetadata["app-version"] = "${BuildConfig.VERSION_NAME}(${BuildConfig.VERSION_CODE})"
        }

        metadata = testCardQrCodeMetadataManager.updateUploadMetaData(metadata)

        if (jpgFile != null && jpgFile.exists()) {
            transferUtility.upload(s3Bucket, jpgKey, jpgFile, metadata)
        }
        val observer = transferUtility.upload(s3Bucket, dngKey, file, metadata)
        observer.setTransferListener(object : TransferListener {
            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val percent = bytesCurrent.toDouble().div(bytesTotal).times(100.0)
                onProgressChanged?.invoke(percent.toInt())
                Log.d(
                    AwsManager::class.simpleName,
                    "uploadSampleToAWS onProgressChanged progress $bytesCurrent/$bytesTotal , $percent"
                )
            }

            override fun onStateChanged(id: Int, state: TransferState?) {
                when (state) {
                    TransferState.COMPLETED -> onUploadCompleted?.invoke()
                    TransferState.FAILED -> onUploadFailed?.invoke()
                    TransferState.WAITING_FOR_NETWORK -> onUploadWaitingForNetwork?.invoke()
                    else -> {
                        // do nothing, let's add more cases later
                    }
                }

                Log.d(AwsManager::class.simpleName, "uploadSampleToAWS onStateChanged $state")
            }

            override fun onError(id: Int, ex: Exception?) {
                onUploadFailed?.invoke()
                Log.d(AwsManager::class.simpleName, "uploadSampleToAWS onError $ex")
                ex?.let { loggingManager.logError(it) }
            }
        })
    }

    companion object {
        private const val S3_BUCKET_NAME = "S3_BUCKET_NAME"
    }
}
