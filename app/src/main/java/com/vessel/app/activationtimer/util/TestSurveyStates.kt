package com.vessel.app.activationtimer.util

enum class TestSurveyStates {
    PRETAKE, NOTTAKE, CHAINED, SKIPPED, COMPLETED
}
