package com.vessel.app.ready.videowarning

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class IncompleteVideoWarningViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val devicePreferencesRepository: DevicePreferencesRepository
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()

    init {
        devicePreferencesRepository.showTipVideoAlertDialog = false
    }
    fun onGotItClicked() {
        dismissDialog.call()
    }
}
