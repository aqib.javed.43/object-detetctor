package com.vessel.app.posttestnutritioncoach.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class BulletStringAdapter : BaseAdapterWithDiffUtil<Int>() {
    override fun getLayout(position: Int) = R.layout.item_bullet_text
}
