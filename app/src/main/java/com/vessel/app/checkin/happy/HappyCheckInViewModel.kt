package com.vessel.app.checkin.happy

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.mapper.ProgramMapper
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.plan.model.PlanItem
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.util.extensions.isSameDay
import kotlinx.coroutines.launch
import java.util.*

class HappyCheckInViewModel @ViewModelInject constructor(
    val state: HappyCheckInState,
    @Assisted savedStateHandle: SavedStateHandle,
    private val goalsRepository: GoalsRepository,
    private val resourceProvider: ResourceRepository,
    private val planManager: PlanManager,
    private val programMapper: ProgramMapper
) : BaseViewModel(resourceProvider), ToolbarHandler, HappyCheckInAdapter.OnActionHandler {
    val today: Date = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
    var notice: String = ""
    init {
        val goalSelected = goalsRepository.getUserMainGoal()
        goalSelected?.let {
            setGoal(it)
        }
        loadTodayActivities(false, today)
    }
    companion object {
        const val TAG = "HappyCheckInViewModel"
    }

    fun onDoneClicked(view: View) {
        Navigation.findNavController(view).popBackStack(R.id.homeScreenFragment, false)
    }
    fun setGoal(goal: GoalSample) {
        Constants.GOALBUTTONS.firstOrNull {
            it.id == goal.id
        }?.let {
            state.selectedGoal.value = it
        }
        notice = "Here's the activities that helped with ${goal.name}."
    }
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
    private fun loadTodayActivities(
        forceUpdate: Boolean = false,
        today: Date
    ) {
        viewModelScope.launch() {
            showLoading()
            planManager.getPlansByDate(false, today.formatDayDate())
                .onSuccess { items ->
                    val groupedFoodItems = items.filter {
                        it.food_id != null
                    }.groupBy {
                        it.food_id
                    }

                    val groupedLifeStyleItemsItems = items.filter {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null
                    }.groupBy {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                    }

                    val groupedSupplementItems = items.filter {
                        it.isSupplementPlan()
                    }

                    val groupedTips = items.filter {
                        it.isTip()
                    }.groupBy {
                        it.tip_id
                    }

                    state.activitiesItems.postValue(
                        buildTodayActivities(
                            groupedFoodItems,
                            groupedLifeStyleItemsItems,
                            groupedSupplementItems,
                            groupedTips,
                            today
                        )
                    )
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }
    private fun buildTodayActivities(
        groupedFoodItems: Map<Int?, List<PlanData>>,
        groupedLifeStyleItemsItems: Map<Int?, List<PlanData>>,
        groupedSupplementItems: List<PlanData>,
        groupedTips: Map<Int?, List<PlanData>>,
        today: Date
    ): List<PlanItem> {
        val dailyItems = mutableListOf<PlanItem>()
        groupedFoodItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it,
                        grouped,
                        today.formatDayDate()
                    )
                )
            }
        }
        groupedTips.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it,
                        grouped,
                        today.formatDayDate()
                    )
                )
            }
        }
        groupedLifeStyleItemsItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it,
                        grouped,
                        today.formatDayDate()
                    )
                )
            }
        }
        groupedSupplementItems.map { item ->
            dailyItems.add(
                mapTodoToPlanItem(
                    item,
                    groupedSupplementItems,
                    today.formatDayDate()
                )
            )
        }

        val filteredTodayActivities = dailyItems.filter { planItem ->
            planItem.getOccurrenceDates().any { it.isSameDay(today) }
        }

        val planItems = togglePlanItems(filteredTodayActivities, today).sortedBy {
            it.currentPlan.getTimeFormattedAsNumber()
        }
        return planItems
    }
    private fun mapTodoToPlanItem(
        todo: PlanData,
        plans: List<PlanData>,
        date: String? = null
    ): PlanItem {
        val isCompleted = todo.plan_records?.firstOrNull { it.date == date }?.completed == true

        return PlanItem(
            todo.id ?: -1,
            todo.getDisplayName(),
            todo.getDescription(),
            todo.getTimeFormattedWithMeridian() ?: getResString(R.string.set_time),
            todo.getToDoImageUrl(),
            todo.getToDoImageBackground(),
            todo.getDisplayName(),
            isCompleted,
            false,
            1,
            plans,
            plans,
            todo.getPlanType(),
            todo.day_of_week,
            todo.program_dates?.map { Constants.DAY_DATE_FORMAT.parse(it) },
            program = todo.program?.let { programMapper.mapProgram(it) },
            todo
        )
    }

    private fun togglePlanItems(
        dayItems: List<PlanItem>,
        formatPlanDate: Date
    ): List<PlanItem> {
        return dayItems.map {
            it.copy(
                isCompleted = it.currentPlan.plan_records.orEmpty().firstOrNull {
                    it.completed == true && formatPlanDate.isSameDay(
                        Constants.DAY_DATE_FORMAT.parse(
                            it.date
                        )
                    )
                } != null
            )
        }
    }

    override fun onItemClicked(items: List<PlanItem>) {
    }
}
