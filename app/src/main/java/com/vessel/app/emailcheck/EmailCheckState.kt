package com.vessel.app.emailcheck

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class EmailCheckState @Inject constructor() {
    val userEmail = MutableLiveData("")
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: EmailCheckState.() -> Unit) = apply(block)
}
