package com.vessel.app.reagent.model

import android.os.Parcelable
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.data.ReagentButtonAction
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.model.data.ReagentRange
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReagentHeader(
    val id: Int,
    val title: String,
    val unit: String,
    val chartEntries: List<DateEntry>,
    val lowerBound: Float,
    val upperBound: Float,
    val minY: Float,
    val maxY: Float,
    @DrawableRes
    val background: Int,
    val ranges: List<ReagentRange>,
    val errorDescription: String,
    val isBetaReagent: Boolean,
    val betaHint: String,
    val consumptionUnit: String?
) : Parcelable {
    @IgnoredOnParcel
    val displayUnit = consumptionUnit ?: unit

    @IgnoredOnParcel
    private val isBelowLowerBound = (chartEntries.lastOrNull()?.y ?: minY) < lowerBound

    @IgnoredOnParcel
    private val isAboveUpperBound = (chartEntries.lastOrNull()?.y ?: maxY) > upperBound

    @IgnoredOnParcel
    @ColorRes
    val statusBackground = when {
        isBelowLowerBound || isAboveUpperBound -> R.drawable.rose_background
        else -> R.drawable.primary_background
    }

    @IgnoredOnParcel
    val isReagentTestInformationNotAvailable = chartEntries.lastOrNull()?.y == null || chartEntries.lastOrNull()?.y == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE

    @IgnoredOnParcel
    val lastChartValue =
        if (chartEntries.lastOrNull()?.y == null) {
            Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE
        } else {
            chartEntries.lastOrNull()!!.y
        }

    @IgnoredOnParcel
    val notAvailableTitle =
        if (id == ReagentItem.B9.id) {
            R.string.b9_not_available_title
        } else {
            R.string.card_read_test_issue
        }

    @IgnoredOnParcel
    val canLearnMoreAboutErrors = id != ReagentItem.B9.id

    @IgnoredOnParcel
    val lastReagentChartRange = if (id == ReagentItem.Hydration.id) {
        ranges.firstOrNull { lastChartValue <= it.from && lastChartValue >= it.to }
    } else {
        ranges.firstOrNull { lastChartValue >= it.from && lastChartValue <= it.to }
    }

    @IgnoredOnParcel
    val hintTitle = lastReagentChartRange?.hintTitle.orEmpty()

    @IgnoredOnParcel
    val hintDescription = lastReagentChartRange?.hintDescription.orEmpty()

    @IgnoredOnParcel
    val actionType = lastReagentChartRange?.rangeRecommendationAction ?: ReagentButtonAction.NONE

    @IgnoredOnParcel
    val shouldShowPlanAction = actionType != ReagentButtonAction.NONE

    @IgnoredOnParcel
    val shouldShowSupplementAction = actionType == ReagentButtonAction.FOOD_SUPPLEMENT_PLAN

    @IgnoredOnParcel
    @StringRes
    val planActionTitle = when (lastReagentChartRange?.rangeRecommendationAction) {
        ReagentButtonAction.FOOD_SUPPLEMENT_PLAN -> R.string.food_plan
        ReagentButtonAction.STRESS_RELIEF_PLAN -> R.string.stress_relief_plan
        ReagentButtonAction.HYDRATION_PLAN -> R.string.hydration_plan
        ReagentButtonAction.FOOD_PLAN -> R.string.food_plan
        else -> R.string.empty
    }
}
