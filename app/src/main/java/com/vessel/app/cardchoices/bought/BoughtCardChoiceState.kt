package com.vessel.app.cardchoices.bought

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class BoughtCardChoiceState @Inject constructor() {
    val userEmail = MutableLiveData("")
    val userPassword = MutableLiveData("")
    val closeEvent = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: BoughtCardChoiceState.() -> Unit) = apply(block)
}
