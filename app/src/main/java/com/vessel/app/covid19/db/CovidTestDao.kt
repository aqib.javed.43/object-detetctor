package com.vessel.app.covid19.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface CovidTestDao {
    @Query("SELECT * from CovidTestDb")
    fun getAllTests(): Flow<List<CovidTestDb>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(tests: List<CovidTestDb>)
}
