package com.vessel.app.foodplan.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentModel
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.foodplan.model.FoodDetailsItemModel
import com.vessel.app.foodplan.model.FoodPlanDetailsHeader
import com.vessel.app.foodplan.model.FoodReviewItem
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class FoodDetailsState @Inject constructor(val itemDetails: FoodDetailsItemModel) {
    val header = MutableLiveData<FoodPlanDetailsHeader>()
    val reminderItems = MutableLiveData<List<ReminderRowUiModel>>()
    val reagentInfo = MutableLiveData<List<ImpactTestReagentModel>>()
    val goalsItems = MutableLiveData<List<ImpactGoalModel>>()
    val navigateTo = LiveEvent<NavDirections>()
    val removeReminder = LiveEvent<FoodPlanDetailsHeader>()
    val plans = MutableLiveData<List<PlanData>>()
    val reviews = MutableLiveData<List<FoodReviewItem>>()

    operator fun invoke(block: FoodDetailsState.() -> Unit) = apply(block)
}
