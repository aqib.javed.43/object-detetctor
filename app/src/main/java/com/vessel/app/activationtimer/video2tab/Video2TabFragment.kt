package com.vessel.app.activationtimer.video2tab

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.viewModels
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.LoopingMediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.AssetDataSource
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import com.google.android.exoplayer2.upstream.DataSource.Factory

@AndroidEntryPoint
class Video2TabFragment : BaseFragment<Video2TabViewModel>() {
    override val viewModel: Video2TabViewModel by viewModels()
    override val layoutResId = R.layout.fragment_video_2_tab

    private val exoPlayer by lazy { SimpleExoPlayer.Builder(requireContext()).build() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        if (viewModel.afterFirstTest)
            setupVideo()
    }

    private fun setupVideo() {
        requireView().findViewById<PlayerView>(R.id.video).player = exoPlayer
        exoPlayer.apply {
            val assetSourceFactory = Factory { AssetDataSource(requireContext()) }
            val mediaItem = MediaItem.fromUri(Uri.parse(VIDEO_PATH))
            val assetVideoSource = ProgressiveMediaSource.Factory(assetSourceFactory)
                .createMediaSource(mediaItem)
            val loopingMediaSource = LoopingMediaSource(assetVideoSource)
            addMediaSource(loopingMediaSource)
            prepare()
        }
    }

    override fun onResume() {
        super.onResume()
        exoPlayer.playWhenReady = true
    }

    override fun onPause() {
        super.onPause()
        exoPlayer.playWhenReady = false
    }

    override fun onStop() {
        super.onStop()
        exoPlayer.playWhenReady = false
        exoPlayer.stop()
    }

    companion object {
        const val VIDEO_PATH = "assets:///video_tutorial.mp4"
    }
}
