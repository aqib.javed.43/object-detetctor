package com.vessel.app.common.model.data

import com.vessel.app.common.net.data.BadHabits
import com.vessel.app.common.net.data.plan.BadHabitRecord

object BadHabitMapper {
    fun map(item: BadHabits) = BadHabitRecord(
        item.id,
        item.title,
        item.is_active
    )
}
