package com.vessel.app.posttest

import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PostTestState @Inject constructor(val testCompletedCount: Int) {
    val navigateTo = LiveEvent<Int>()

    operator fun invoke(block: PostTestState.() -> Unit) = apply(block)
}
