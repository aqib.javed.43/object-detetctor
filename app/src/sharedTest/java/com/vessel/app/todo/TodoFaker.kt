package com.vessel.app.todo

import com.vessel.app.BaseFaker
import com.vessel.app.BaseFaker.Fake.FAKER
import com.vessel.app.Constants
import com.vessel.app.common.model.Todo
import com.vessel.app.common.net.data.TodoData

object TodoFaker {
    fun basic(name: String = FAKER.gameOfThrones().character()) = FAKER.run {
        Todo(
            id = number().randomDigit(),
            name = name,
            quantity = number().randomDigit().toFloat(),
            insertDate = date().birthday(),
            usdaNdbNumber = number().randomDigit()
        )
    }

    fun data() = FAKER.run {
        TodoData(
            id = number().randomDigit(),
            name = gameOfThrones().character(),
            quantity = number().randomDigit().toFloat(),
            insert_date = Constants.SERVER_INSERT_DATE_FORMAT.format(date().birthday()),
            usda_ndb_number = number().randomDigit()
        )
    }

    fun list() = BaseFaker.list { basic() }

    fun dataList() = BaseFaker.list { data() }
}
