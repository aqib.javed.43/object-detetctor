package com.vessel.app.foodplan.ui

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.animation.AlphaAnimation
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.base.BaseFragment
import com.vessel.app.checkscience.model.CheckOutScienceHeaderItem
import com.vessel.app.checkscience.ui.CheckOutScienceHeaderAdapter
import com.vessel.app.common.util.DisclaimerFooter
import com.vessel.app.common.util.DisclaimerFooterAdapter
import com.vessel.app.foodplan.FoodPlanViewModel
import com.vessel.app.foodplan.model.EmptySearchResultFoodItem
import com.vessel.app.foodplan.model.FoodHeaderItem
import com.vessel.app.foodplan.model.FoodNutrientNeedsHeaderItem
import com.vessel.app.plan.model.AddPlanFooter
import com.vessel.app.plan.ui.AddPlanFooterAdapter
import com.vessel.app.util.extensions.addImage
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.PlanDeleteDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_food_plan.*

@AndroidEntryPoint
class FoodPlanFragment : BaseFragment<FoodPlanViewModel>() {
    override val viewModel: FoodPlanViewModel by viewModels()
    override val layoutResId = R.layout.fragment_food_plan

    private val foodPlanHeaderAdapter by lazy { FoodPlanHeaderAdapter(viewModel) }
    private val foodDayAdapter by lazy { FoodDayAdapter(viewModel) }

    private val foodPlanFooterAdapter = DisclaimerFooterAdapter()
    private val cutOutAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_cut_out
    }
    private val dailyNutrientNeedsHeaderAdapter by lazy { FoodNutrientNeedHeaderAdapter(viewModel) }
    private val foodRecommendationAdapter by lazy { FoodRecommendationAdapter(viewModel) }
    private val foodPlanLevelsAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_food_plan_level
    }
    private val stickyFoodPlanLevelsAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_sticky_food_plan_level
    }
    private val foodTabsHeaderAdapter by lazy { FoodTabsHeaderAdapter(viewModel) }
    private val foodCheckOutScienceHeaderAdapter by lazy { CheckOutScienceHeaderAdapter(viewModel) }
    private val supplementCheckOutScienceHeaderAdapter by lazy { CheckOutScienceHeaderAdapter(viewModel) }

    private val emptySearchResultFoodItemsAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_empty_search_result_food
    }
    private val addPlanFooterAdapter by lazy {
        AddPlanFooterAdapter(
            viewModel,
            R.string.add_to_your_food_plan
        )
    }

    private val emptyViewAdapter = object : BaseAdapterWithDiffUtil<Spanned>() {
        override fun getLayout(position: Int) = R.layout.item_food_plan_empty_view
        override fun onBindViewHolder(holder: BaseViewHolder<Spanned>, position: Int) {
            super.onBindViewHolder(holder, position)
            holder.itemView.findViewById<TextView>(R.id.description).text = getItem(position)
        }
    }

    private val plansScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            (foodPlanRecyclerView.layoutManager as? LinearLayoutManager)?.findFirstVisibleItemPosition()?.let {
                if (viewModel.state.showStickyFoodPlanRecyclerView.value != (it > FOOD_PLAN_LEVEL_POSITION)) {
                    viewModel.state.showStickyFoodPlanRecyclerView.value = it > FOOD_PLAN_LEVEL_POSITION
                }

                // All items till search bar
                val searchItemPosition = foodPlanHeaderAdapter.itemCount +
                    dailyNutrientNeedsHeaderAdapter.itemCount +
                    foodPlanLevelsAdapter.itemCount +
                    foodRecommendationAdapter.itemCount - 1

                if (viewModel.state.showStickySearchLayoutRecyclerView.value != it > searchItemPosition) {
                    viewModel.state.showStickySearchLayoutRecyclerView.value = it > searchItemPosition
                }
            }
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        view?.findViewById<RecyclerView>(R.id.foodPlanRecyclerView)
            ?.apply {
                itemAnimator = null
                adapter = ConcatAdapter(
                    ConcatAdapter.Config.Builder()
                        .setIsolateViewTypes(false)
                        .build(),
                    foodPlanHeaderAdapter,
                    dailyNutrientNeedsHeaderAdapter,
                    foodPlanLevelsAdapter,
                    foodTabsHeaderAdapter,
                    addPlanFooterAdapter,
                    foodDayAdapter,
                    foodCheckOutScienceHeaderAdapter,
                    supplementCheckOutScienceHeaderAdapter,
                    foodPlanFooterAdapter
                ).apply { addItemDecoration(FoodPlanItemDecoration(requireContext())) }
                addOnScrollListener(plansScrollListener)
            }

        view?.findViewById<RecyclerView>(R.id.stickyFoodPlanRecyclerView)?.adapter = stickyFoodPlanLevelsAdapter

        view?.findViewById<RecyclerView>(R.id.foodPlanRecyclerView)?.addOnScrollListener(plansScrollListener)

        foodPlanHeaderAdapter.submitList(listOf(FoodHeaderItem))
        dailyNutrientNeedsHeaderAdapter.submitList(listOf(FoodNutrientNeedsHeaderItem))

        observe(viewModel.state.navigateTo) {
            findNavController().navigate(it)
        }

        observe(viewModel.state.foodItems) {
            if (it.isEmpty()) {
                addPlanFooterAdapter.submitList((listOf(AddPlanFooter)))
                foodDayAdapter.submitList(listOf())
            } else {
                addPlanFooterAdapter.submitList(listOf())
                foodDayAdapter.submitList(it)
            }
            foodDayAdapter.submitList(it)
        }

        observe(viewModel.state.levels) {
            foodPlanLevelsAdapter.submitList(it)
            stickyFoodPlanLevelsAdapter.submitList(it)
        }

        cutOutAdapter.submitList(listOf(Unit))

        observe(viewModel.state.foodTabs) {
            foodTabsHeaderAdapter.submitList(it)
        }

        foodCheckOutScienceHeaderAdapter.submitList(
            listOf(
                CheckOutScienceHeaderItem(
                    viewModel.getFoodCheckOutScienceHeaderContent(),
                    title = R.string.science_behind_food_recommendations
                )
            )
        )
        supplementCheckOutScienceHeaderAdapter.submitList(
            listOf(
                CheckOutScienceHeaderItem(
                    viewModel.getSupplementCheckOutScienceHeaderContent(),
                    title = R.string.science_behind_supplement_recommendations
                )
            )
        )
        foodPlanFooterAdapter.submitList(listOf(DisclaimerFooter.Default))

        observe(viewModel.state.showEmptySearchResultFoodItem) {
            if (it) {
                emptySearchResultFoodItemsAdapter.submitList(listOf(EmptySearchResultFoodItem))
            } else {
                emptySearchResultFoodItemsAdapter.submitList(emptyList())
            }
        }

        observe(viewModel.state.showFoodPlanAddedRemovePopUp) {
            findNavController()
                .navigate(
                    FoodPlanFragmentDirections.actionFoodPlanFragmentToFoodPlanPopupDialog(it)
                )
        }
        observe(viewModel.state.removeReminder) {
            PlanDeleteDialog.showDialog(
                requireActivity(),
                "",
                {
                    // Not used
                },
                {
                    viewModel.deletePlanItem(it)
                },
                {
                    viewModel.onDontShowAgainClicked(it)
                }
            )
        }
    }

    override fun onDestroyView() {
        view?.findViewById<RecyclerView>(R.id.foodPlanRecyclerView)?.removeOnScrollListener(plansScrollListener)
        super.onDestroyView()
    }

    private fun getEmptyViewText(): SpannableStringBuilder {
        return requireContext().addImage(
            getString(R.string.choose_food_plan),
            "[empty-image]",
            R.drawable.ic_add_rounded_button,
            80,
            80
        )
    }

    companion object {
        const val FOOD_PLAN_LEVEL_POSITION = 3
        private const val ANIMATION_DURATION = 1000L
        val fadingInAnimation = AlphaAnimation(0.0f, 1.0f).apply {
            duration = ANIMATION_DURATION
        }
        val fadingOutAnimation = AlphaAnimation(1.0f, 0.0f).apply {
            duration = ANIMATION_DURATION
        }
    }
}
