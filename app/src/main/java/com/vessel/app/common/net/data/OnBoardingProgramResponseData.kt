package com.vessel.app.common.net.data
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OnBoardingProgramResponseData(
    val program: List<OnBoardingProgramData>
)
@JsonClass(generateAdapter = true)
data class OnBoardingProgramData(
    val id: Int
)
