package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TodoReminderData(
    val contact_id: Int? = null,
    val created_at: String?,
    val day_of_week: List<Int>?,
    val description: String? = null,
    val id: Int? = null,
    val image_url: String? = null,
    val meta_data: String? = null,
    val recurring_todo_id: Int?,
    val time_of_day: String?,
    val title: String?
)
