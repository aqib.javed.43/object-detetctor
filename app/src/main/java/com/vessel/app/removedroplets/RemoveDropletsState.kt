package com.vessel.app.removedroplets

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class RemoveDropletsState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val navigateToCapture = LiveEvent<Unit>()

    operator fun invoke(block: RemoveDropletsState.() -> Unit) = apply(block)
}
