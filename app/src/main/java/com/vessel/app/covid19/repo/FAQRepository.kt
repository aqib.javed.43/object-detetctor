package com.vessel.app.covid19.repo

import com.vessel.app.R
import com.vessel.app.covid19.model.FAQ
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FAQRepository @Inject constructor() {

    val faqs = listOf(
        FAQ(
            question = R.string.faq1_title,
            answer = R.string.faq1_body
        ),
        FAQ(
            question = R.string.faq2_title,
            answer = R.string.faq2_body
        ),
        FAQ(
            question = R.string.faq3_title,
            answer = R.string.faq3_body
        ),
        FAQ(
            question = R.string.faq4_title,
            answer = R.string.faq4_body
        ),
        FAQ(
            question = R.string.faq5_title,
            answer = R.string.faq5_body
        ),
        FAQ(
            question = R.string.faq6_title,
            answer = R.string.faq6_body
        )
    )
}
