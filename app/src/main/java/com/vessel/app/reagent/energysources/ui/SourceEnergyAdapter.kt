package com.vessel.app.reagent.energysources.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.reagent.model.InfoSource

class SourceEnergyAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<InfoSource>() {
    override fun getLayout(position: Int) = R.layout.item_source

    override fun getHandler(position: Int): Any = handler

    interface OnActionHandler {
        fun onSourceLinkButtonClick(source: InfoSource)
    }
}
