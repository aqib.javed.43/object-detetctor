package com.vessel.app.programs.complete.stepone

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.appreview.feedback.AppReviewFeedbackDialogViewModel
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.RecommendationManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Program
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import com.vessel.app.wellness.net.data.ProgramAction
import kotlinx.coroutines.launch

class CompleteProgramStep1ViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val recommendationManager: RecommendationManager,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    private val programManager: ProgramManager,
) : BaseViewModel(resourceProvider),
    ToolbarHandler {
    private var feedbackText: String? = null
    private val remainingCharacters = MutableLiveData<String>(
        getRemainingCharacters()
    )
    val state = CompleteProgramStep1State(savedStateHandle.get<Program>("program")!!)

    fun onSubmitClicked() {
        completeProgram()
    }

    fun onMaybeLaterClicked() {
        completeProgram()
    }

    private fun completeProgram() {
        showLoading()
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PROGRAM_COMPLETED)
                .addProperty(TrackingConstants.KEY_ID, state.passedProgram.id.toString())
                .addProperty(TrackingConstants.IS_PROGRAM_COMPLETE, "true")
                .addProperty(TrackingConstants.USER_FEEDBACK, feedbackText.toString())
                .addProperty(TrackingConstants.END_TYPE, ProgramAction.KEEP.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        viewModelScope.launch {
            programManager.completeProgram(
                state.passedProgram,
                ProgramAction.KEEP,
                feedbackText
            ).onSuccess {
                state.navigateTo.value =
                    CompleteProgramStep1FragmentDirections.actionCompleteProgramStep1FragmentToCompleteProgramStep2Fragment(
                        state.program.value!!
                    )
                hideLoading(false)
            }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    fun onLikeClicked(program: Program) {
        if (program.likeStatus != LikeStatus.LIKE) {
            val likesCount = program.totalLikes + 1
            state.program.value =
                program.copy(likeStatus = LikeStatus.LIKE, totalLikes = likesCount)
            viewModelScope.launch {
                recommendationManager.sendLikesStatus(
                    program.id,
                    LikeCategory.Program.value,
                    true
                )
            }
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.CONTENT_LIKED)
                    .addProperty(TrackingConstants.KEY_ID, program.id.toString())
                    .addProperty(TrackingConstants.TYPE, LikeCategory.Program.value)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }

    fun onDisLikeClicked(program: Program) {
        if (program.likeStatus != LikeStatus.DISLIKE) {
            val likesCount = program.totalLikes - 1
            state.program.value =
                program.copy(likeStatus = LikeStatus.DISLIKE, totalLikes = likesCount)
            viewModelScope.launch {
                recommendationManager.unLikesStatus(
                    program.id,
                    LikeCategory.Program.value
                )
            }
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.CONTENT_UNLIKED)
                    .addProperty(TrackingConstants.KEY_ID, program.id.toString())
                    .addProperty(TrackingConstants.TYPE, LikeCategory.Program.value)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }

    fun onFeedbackTextChanged(s: CharSequence) {
        feedbackText = s.toString()
        remainingCharacters.value = getRemainingCharacters()
    }

    private fun getRemainingCharacters() = getResString(
        R.string.remaining_characters,
        AppReviewFeedbackDialogViewModel.MAX_ALLOWED_FEEDBACK_CHARS_LIMIT - (
            feedbackText?.length
                ?: 0
            )
    )

    override fun onBackButtonClicked(view: View) {
        preventCheckingCompletedProgramAndNavigateBack()
    }

    fun preventCheckingCompletedProgramAndNavigateBack() {
        preferencesRepository.checkCompletedProgram = false
        navigateBack()
    }
}
