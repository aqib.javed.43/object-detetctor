package com.vessel.app.programs.join

import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class JoinProgramState @Inject constructor(val program: Program) {

    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: JoinProgramState.() -> Unit) = apply(block)
}
