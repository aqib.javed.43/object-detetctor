package com.vessel.app.stressreliefdetails

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.activityreview.model.ActivityReviewItem
import com.vessel.app.activityreview.model.RecommendationReview
import com.vessel.app.activityreview.ui.ActivityReviewAdapter
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.adapter.genericlikeordislikeadapter.LikeOrDislikeAdapter
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.impactgoals.ImpactGoalsAdapter
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentAdapter
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentModel
import com.vessel.app.common.adapter.planedit.PlanEditAdapter
import com.vessel.app.common.adapter.planedit.PlanEditItem
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.common.adapter.planreminders.toReminderList
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Sample
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.ReagentLifestyleRecommendation
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.stressreliefdetails.model.StressReliefDetailsHeader
import com.vessel.app.stressreliefdetails.model.StressReliefReviewItem
import com.vessel.app.stressreliefdetails.ui.StressReliefDetailsFragmentDirections
import com.vessel.app.stressreliefdetails.ui.StressReliefHeaderAdapter
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.DeleteReviewRequest
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class StressReliefDetailsViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val planManager: PlanManager,
    private val reminderManager: ReminderManager,
    private val reagentsManager: ReagentsManager,
    private val recommendationManager: RecommendationManager,
    val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    StressReliefHeaderAdapter.OnActionHandler,
    PlanEditAdapter.OnActionHandler,
    ReminderRowAdapter.OnActionHandler,
    ImpactGoalsAdapter.OnActionHandler,
    LikeOrDislikeAdapter.OnActionHandler<StressReliefDetailsHeader>,
    ActivityReviewAdapter.OnActionHandler,
    ImpactTestReagentAdapter.OnActionHandler {
    private var userPlans = listOf<PlanData>()
    private var recommendationItem: Sample? = null

    val state =
        StressReliefDetailsState(savedStateHandle["stressReliefViewItem"]!!)

    init {
        observeRemindersDataState()
        loadHeaderData()
        getGoals()
        getReviews()
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_VIEWED)
                .addProperty(TrackingConstants.KEY_ID, state.header.value?.id.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Lifestyle.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun loadHeaderData(forceUpdate: Boolean = false) {
        viewModelScope.launch {
            showLoading(false)
            val recommendationsResponse = recommendationManager.getLatestSampleRecommendations()
            val plansResponse = planManager.getUserPlans(forceUpdate)
            if (plansResponse is Result.Success && recommendationsResponse is Result.Success) {
                hideLoading(false)
                val plans = plansResponse.value.filter {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null && it.reagent_lifestyle_recommendation.lifestyle_recommendation_id == state.itemDetails.life_style_recommendation_id
                }

                val recommendedItem =
                    recommendationsResponse.value.lifestyle.firstOrNull { it.lifestyle_recommendation_id == state.itemDetails.life_style_recommendation_id }
                        ?: return@launch

                recommendationItem = recommendedItem

                setupReminderPlans(plans)
                state.header.value = StressReliefDetailsHeader(
                    state.itemDetails.title,
                    state.itemDetails.description,
                    recommendedItem.image_url.orEmpty(),
                    plans.isNotEmpty(),
                    recommendedItem.lifestyle_recommendation_id!!,
                    recommendedItem.total_likes ?: 0,
                    recommendedItem.dislikes_count ?: 0,
                    recommendedItem.like_status ?: LikeStatus.NO_ACTION
                )
            }
        }
    }

    private fun setupReminderPlans(lifestylePlans: List<PlanData>) {
        val plans = lifestylePlans.toReminderList()
        userPlans = lifestylePlans
        state.reminderItems.value = plans
    }

    private fun getGoals() {
        viewModelScope.launch {
            recommendationManager.getLatestSampleRecommendations().onSuccess {
                val stressPlan =
                    it.lifestyle.firstOrNull { it.lifestyle_recommendation_id == state.itemDetails.life_style_recommendation_id }
                        ?: return@onSuccess
                val goals = stressPlan.goals.orEmpty().mapIndexed { index, goal ->
                    ImpactGoalModel(
                        goal.name,
                        goal.image_small_url.orEmpty(),
                        Goal.alternateBackground(index),
                    )
                }

                val cortisolReagent = reagentsManager.fromId(ReagentItem.Cortisol.id)
                if (cortisolReagent != null) {
                    state.reagentInfo.value = listOf(
                        ImpactTestReagentModel(
                            cortisolReagent.displayName,
                            cortisolReagent.headerImage,
                            ""
                        )
                    )
                }

                state.goalsItems.value = goals
                state.imagesItems.value = stressPlan.extra_images.orEmpty()
                state.descriptionData.value = stressPlan.description.orEmpty()
            }
        }
    }

    private fun observeRemindersDataState() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    planManager.clearCachedData()
                    loadHeaderData(true)
                }
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onAddToPlanClicked(item: StressReliefDetailsHeader) {
        if (item.isAdded) {
            if (userPlans.size > 1 || preferencesRepository.dontShowPlanRemoveConfirmationDialog) {
                navigateToReminders(item)
            } else {
                state.removeReminder.value = item
            }
        } else {
            navigateToReminders(item)
        }
    }

    fun deletePlanItem() {
        viewModelScope.launch {
            showLoading(false)
            planManager.deletePlanItem(userPlans.map { it.id ?: -1 })
                .onSuccess {
                    hideLoading(false)
                    planManager.updatePlanCachedData()
                    reminderManager.updateRemindersCachedData()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(
                                TrackingConstants.KEY_ID,
                                userPlans.map { it.id ?: -1 }.toString()
                            )
                            .addProperty(
                                TrackingConstants.TYPE,
                                LikeCategory.Lifestyle.value
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Stress Relief Details")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    navigateBack()
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    override fun onCheckStressReliefPlanClicked() {
        state.navigateTo.value =
            StressReliefDetailsFragmentDirections.actionStressReliefDetailsFragmentToStressReliefFragment()
    }

    override fun onItemLikeClicked(item: StressReliefDetailsHeader) {
        onLikeClicked(item)
    }

    override fun onPlanEditItemClick(item: PlanEditItem, view: View) {
        navigateToReminders(state.header.value!!)
    }

    override fun onReminderItemClick(item: ReminderRowUiModel, view: View) {
        navigateToReminders(state.header.value!!)
    }

    override fun onGoalItemClick(item: ImpactGoalModel, view: View) {
    }

    override fun onReagentItemClick(item: ImpactTestReagentModel, view: View) {
    }

    fun navigateToReminders(item: StressReliefDetailsHeader) {
        state.navigateTo.value =
            StressReliefDetailsFragmentDirections.actionStressReliefDetailsFragmentToPlanReminderDialog(
                PlanReminderHeader(
                    item.tittle,
                    getPlans(item).first().getDescription(),
                    item.image,
                    0,
                    getPlans(item),
                    item.isAdded.not()
                )
            )
    }

    private fun getPlans(item: StressReliefDetailsHeader) = if (item.isAdded) userPlans else listOf(
        PlanData(
            reagent_lifestyle_recommendation_id = recommendationItem?.id,
            reagent_lifestyle_recommendation = ReagentLifestyleRecommendation(
                lifestyle_recommendation_id = recommendationItem?.id ?: -1,
                quantity = recommendationItem?.amount ?: 0f,
                unit = recommendationItem?.unit,
                reagent_bucket_id = recommendationItem?.reagent?.id,
                activity_name = recommendationItem?.apiName,
                image_url = item.image,
                id = -1,
                frequency = recommendationItem?.frequency
            ),
            multiple = 1
        )
    )

    fun onDontShowAgainClicked(isChecked: Boolean) {
        preferencesRepository.dontShowPlanRemoveConfirmationDialog = isChecked
    }

    override fun onLikeClicked(header: StressReliefDetailsHeader) {
        val likesCount = header.totalLikes
        val dislikesCount = header.totalDislikes
        val newLikesCount: Int
        var newDislikesCount: Int
        if (header.likeStatus != LikeStatus.LIKE) {
            newLikesCount = likesCount.plus(1) ?: 0
            newDislikesCount = header.totalDislikes ?: 0
            if (header.likeStatus == LikeStatus.DISLIKE) newDislikesCount = newDislikesCount.minus(1)
            sendLikeStatus(header, LikeStatus.LIKE, newLikesCount, newDislikesCount)
        } else {
            newLikesCount = likesCount.minus(1) ?: 0
            unLikeStatus(header, LikeStatus.NO_ACTION, newLikesCount, dislikesCount)
        }
    }

    override fun onDisLikeClicked(header: StressReliefDetailsHeader) {
        val likesCount = header.totalLikes
        val dislikesCount = header.totalDislikes
        val newLikesCount: Int
        val newDislikesCount: Int
        if (header.likeStatus != LikeStatus.DISLIKE) {
            newLikesCount =
                when (header.likeStatus) {
                    LikeStatus.NO_ACTION -> likesCount ?: 0
                    LikeStatus.LIKE -> likesCount.minus(1) ?: 0
                    else -> return
                }
            newDislikesCount = dislikesCount.plus(1) ?: 0
            sendLikeStatus(header, LikeStatus.DISLIKE, newLikesCount, newDislikesCount)
        } else {
            newLikesCount = dislikesCount.minus(1) ?: 0
            unLikeStatus(header, LikeStatus.NO_ACTION, likesCount, newLikesCount)
        }
    }

    private fun sendLikeStatus(
        header: StressReliefDetailsHeader,
        likeStatus: LikeStatus,
        likesCount: Int,
        dislikesCount: Int
    ) {
        state.header.value =
            state.header.value?.copy(likeStatus = likeStatus, totalLikes = likesCount, totalDislikes = dislikesCount)
        viewModelScope.launch {
            recommendationManager.sendLikesStatus(
                header.id,
                LikeCategory.Lifestyle.value,
                likeStatus == LikeStatus.LIKE
            )
            if (likeStatus == LikeStatus.LIKE) preferencesRepository.updateLikedPlans(state.itemDetails.life_style_recommendation_id.toString())
            else preferencesRepository.updateDislikedPlans(state.itemDetails.life_style_recommendation_id.toString())
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CONTENT_LIKED)
                .addProperty(TrackingConstants.KEY_ID, header.id.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Lifestyle.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun unLikeStatus(
        header: StressReliefDetailsHeader,
        likeStatus: LikeStatus,
        likesCount: Int,
        dislikesCount: Int
    ) {
        state.header.value =
            state.header.value?.copy(likeStatus = likeStatus, totalLikes = likesCount, totalDislikes = dislikesCount)
        viewModelScope.launch {
            recommendationManager.unLikesStatus(
                header.id,
                LikeCategory.Lifestyle.value
            )
            preferencesRepository.updateUnlikedPlans(state.itemDetails.life_style_recommendation_id.toString())
        }
    }
    override fun onWriteNewReview() {
        gotoReview()
    }

    override fun onDeleteReview(item: RecommendationReview, position: Int) {
        deleteReview(position)
    }

    override fun onEditReview(item: RecommendationReview) {
        gotoReview()
    }

    override fun onFlagReview(item: RecommendationReview) {
    }
    fun gotoReview() {
        val reviewItem = ActivityReviewItem()
        reviewItem.category = LikeCategory.Lifestyle.value
        reviewItem.record_id = state.itemDetails.life_style_recommendation_id
        reviewItem.likeStatus = state.header.value?.likeStatus
        reviewItem.image = state.header.value?.image
        reviewItem.title = state.itemDetails.title
        reviewItem.programId = state.itemDetails.life_style_recommendation_id
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalActivityReviewFragment(reviewItem)
    }
    fun deleteReview(position: Int) {
        val request = DeleteReviewRequest(record_id = state.itemDetails.life_style_recommendation_id, category = LikeCategory.Lifestyle.value)
        viewModelScope.launch {
            showLoading()
            recommendationManager.deleteReview(
                request
            ).onSuccess {
                hideLoading()
                state {
                    val items = reviews.value?.firstOrNull()?.items?.toMutableList() ?: arrayListOf()
                    if (!items.isNullOrEmpty())items.removeAt(position)
                    state.reviews.value = state.reviews.value.orEmpty().mapIndexed { index, foodReviewItem ->
                        if (index == 0) foodReviewItem.copy(items = items, title = String.format(getResString(R.string.member_review_count), items.size.toString()))
                        else foodReviewItem.copy()
                    }
                }
            }.onError {
                hideLoading()
                showError(it.toString())
            }
        }
    }
    private fun getReviews() {
        state.itemDetails.life_style_recommendation_id.let {
            viewModelScope.launch {
                recommendationManager.getReviews(
                    it,
                    LikeCategory.Lifestyle.value
                )
                    .onSuccess { response ->
                        val items = response.reviews?.map {
                            it.copy(myContact = preferencesRepository.contactId)
                        } ?: arrayListOf()
                        val title = String.format(getResString(R.string.member_review_count), items.size.toString())
                        val reviewItem = StressReliefReviewItem(title, items)
                        state.reviews.value = listOf(reviewItem)
                    }
            }
        }
    }
}
