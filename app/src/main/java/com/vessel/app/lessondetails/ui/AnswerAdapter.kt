package com.vessel.app.lessondetails.ui

import android.annotation.SuppressLint
import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Answer
import kotlinx.android.synthetic.main.item_answer_view.view.*

class AnswerAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Answer>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is Answer -> R.layout.item_answer_view
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any = handler

    interface OnActionHandler {
        fun onAnswered(item: Any, view: View)
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<Answer>,
        @SuppressLint("RecyclerView") position: Int
    ) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position)
        holder.itemView.swSelected?.apply {
            isClickable = item.isSelected == false
            isEnabled = item.isSelected == false
        }
        holder.itemView.swSelected?.setOnClickListener {
            item.isChecked = true
            handler.onAnswered(item, it)
        }
    }
}
