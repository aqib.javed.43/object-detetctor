package com.vessel.app.homescreen

import android.os.Build
import android.os.CountDownTimer
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Program
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.BottomNavHandler
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class HomeScreenViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    val state: HomeScreenState,
    private val scoreManager: ScoreManager,
    private val buildPlanManager: BuildPlanManager,
    val homeTabsManager: HomeTabsManager,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val trackingManager: TrackingManager,
    private val authManager: AuthManager,
    private val reminderManager: ReminderManager,
    private val chatManager: ChatManager,
    private val membershipManager: MembershipManager,
    private val loggingManager: LoggingManager,
    private val programManager: ProgramManager,
    private val forceUpdateManager: ForceUpdateManager,
    private val supplementsManager: SupplementsManager
) : BaseViewModel(resourceProvider), BottomNavHandler {
    private var previousTab: BottomNavHandler.BottomNavItem = BottomNavHandler.BottomNavItem.PLAN
    var selectedTab = savedStateHandle.get<MainPagerFragmentTab>("selectedTab")

    override val resultsSelected = state.resultsSelected
    override val planSelected = state.planSelected
    override val chatSelected = state.chatSelected
    override val moreSelected = state.moreSelected
    override val showPlanNotification = state.showPlanNotification
    override val showChatNotification = state.showChatNotification
    override val planNotificationCount = state.planNotificationCount

    override fun onBottomNavClicked(item: BottomNavHandler.BottomNavItem) {
        if (item != BottomNavHandler.BottomNavItem.RESULTS) previousTab = item
        state {
            when (item) {
                BottomNavHandler.BottomNavItem.RESULTS -> onResultsClicked()
                BottomNavHandler.BottomNavItem.TAKETEST -> onTakeTestClicked()
                BottomNavHandler.BottomNavItem.PLAN -> onPlanClicked()
                BottomNavHandler.BottomNavItem.CHAT -> onChatClicked()
                BottomNavHandler.BottomNavItem.MORE -> onMoreClicked()
            }
        }
    }

    init {
        observeTestRecommendationState()
        observeOpenChatChannelState()
        calculateNumberOfRecommendations()
        observeTodayTodoDataState()
        observeViewPlanState()
        setTrackingUserIdentification()
        state.showChatNotification.value = preferencesRepository.showChatNotification
        preferencesRepository.isDailyPlanView = true
        checkEnrollProgram()
        Constants.onGotoPreviousScreen = {
            onBottomNavClicked(previousTab)
        }
        Constants.onTakeTest = {
            onTakeTestClicked()
        }
        checkWeeklyCheckIn()
    }

    private fun checkWeeklyCheckIn() {
        // Todo VH-1280 | VWELLANDR-1286: Remove the congratulations screen [Bugsee] - https://app.asana.com/0/1201640107857833/1202059329495896
//        if (System.currentTimeMillis() - preferencesRepository.lastCheckIn >= Constants.WEEK_TIME) {
//            if (membershipManager.hasActiveMembership) {
//                state.showMonthlyCheckIn.call()
//            } else {
//                state.showWeeklyCheckIn.call()
//            }
//            preferencesRepository.lastCheckIn = System.currentTimeMillis()
//        }
    }

    fun navigateToTabOnStart() {
        when (selectedTab) {
            MainPagerFragmentTab.RESULT -> onResultsClicked()
            MainPagerFragmentTab.PLAN -> onPlanClicked()
            MainPagerFragmentTab.CHAT -> onChatClicked()
            MainPagerFragmentTab.MORE -> onMoreClicked()
        }
        selectedTab = null
    }
    private fun onTakeTestClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_TAPPED)
                .setScreenName(TrackingConstants.FROM_HOME_LOGO_BUTTON)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        val isSupportedDevice =
            Constants.SUPPORTED_DEVICES_MODELS_PREFIX.any { Build.MODEL.startsWith(it, true) }
        if (isSupportedDevice) {
            onTestClicked()
        } else {
            state.navigateTo.value = HomeNavGraphDirections.globalActionToTestNotAccurate()
        }
    }

    private fun onTestClicked() {
        val now = Date()
        val fourThirtyAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 4)
                set(Calendar.MINUTE, 30)
                set(Calendar.SECOND, 0)
            }.time
        val nineAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 9)
                set(Calendar.MINUTE, 0)
                set(Calendar.SECOND, 0)
            }.time
        if (now.after(fourThirtyAm) && now.before(nineAm)) {
            state.navigateTo.value = if (preferencesRepository.skipPreTestTips) {
                HomeNavGraphDirections.globalActionToTakeTest()
            } else {
                HomeNavGraphDirections.globalActionToPreTest()
            }
        } else {
            state.navigateTo.value = HomeNavGraphDirections.globalActionToTimeOfDay()
        }
    }

    private fun onResultsClicked() {
        if (preferencesRepository.testCompletedCount == 0) state.navigateToPopupLock.call()
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.RESULTS_TAB_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.resultsSelected.value = true
        state.planSelected.value = false
        state.moreSelected.value = false
        state.chatSelected.value = false
        state.tabNavigate.value = MainPagerFragmentTab.RESULT
    }

    fun onPlanClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PLAN_TAB_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.resultsSelected.value = false
        state.moreSelected.value = false
        state.chatSelected.value = false
        state.planSelected.value = true
        state.tabNavigate.value = MainPagerFragmentTab.PLAN
        if (devicePreferencesRepository.showPlanReadyHint) {
            state.showPlanRecommendation.call()
        }

        if (devicePreferencesRepository.showPlanIntroduction &&
            SimpleDateFormat("dd/MM/yyyy", Locale.US)
                .parse("15/07/2021")!!
                .after(preferencesRepository.getContact()?.insertDate ?: Date())
        ) {
            devicePreferencesRepository.showPlanIntroduction = false
        }
    }

    private fun checkEnrollProgram() {
        viewModelScope.launch() {
            programManager.getEnrolledPrograms()
                .onSuccess { enrolledPrograms ->
                    showTheCompletedProgramsIfNeeded(enrolledPrograms)
                }
                .onServiceError {
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                }
        }
    }

    private fun showTheCompletedProgramsIfNeeded(enrolledPrograms: List<Program>) {
        if (enrolledPrograms.isNullOrEmpty()) {
            if (preferencesRepository.isExistingUserFirstTime) {
                if (preferencesRepository.getUserMainGoal() != null) {
                    state.showIntroForCheckIn.call()
                }
            }
        }
    }

    fun onChatClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CHAT_TAB_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.resultsSelected.value = false
        state.moreSelected.value = false
        state.chatSelected.value = true
        state.planSelected.value = false
        state.tabNavigate.value = MainPagerFragmentTab.CHAT
        if (preferencesRepository.showChatNotification) {
            preferencesRepository.showChatNotification = false
            object : CountDownTimer(
                NOTIFICATION_CHAT_FLAG_APPEARANCE_DURATION,
                NOTIFICATION_CHAT_FLAG_APPEARANCE_DURATION
            ) {
                override fun onTick(millisUntilFinished: Long) {
                }

                override fun onFinish() {
                    state.showChatNotification.value = false
                }
            }.start()
        }
    }

    private fun onMoreClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.MORE_TAB_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.resultsSelected.value = false
        state.moreSelected.value = true
        state.chatSelected.value = false
        state.planSelected.value = false
        state.tabNavigate.value = MainPagerFragmentTab.MORE
    }

    fun onPlanActionClicked() {
        state.showPlanNotification.value = false
        onBottomNavClicked(BottomNavHandler.BottomNavItem.PLAN)
    }

    private fun calculateNumberOfRecommendations() {
        viewModelScope.launch(Dispatchers.IO) {
            // get latest plan data
            val count = buildPlanManager.countRedBadges()
            state.planNotificationCount.postValue(count)
            state.showPlanNotification.postValue(count > 0)
        }
    }

    fun showAppReview() {
        if (preferencesRepository.testCompletedCount > 1 &&
            devicePreferencesRepository.showAppReviewPopup &&
            forceUpdateManager.isRequireUpdate().first.not()
        ) {
            trackingManager.log(
                TrackedEvent.Builder("SHOW APP REVIEW DIALOG")
                    .addProperty("FLAG", devicePreferencesRepository.showAppReviewPopup.toString())
                    .addProperty("TEST COUNT", preferencesRepository.testCompletedCount.toString())
                    .addProperty(
                        "REQUIRE_UPDATE",
                        forceUpdateManager.isRequireUpdate().first.not().toString()
                    )
                    .addProperty("SUCCESS", "TRUE")
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
            state.navigateTo.value =
                HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToAppReviewFirstStepDialogFragment()
        } else {
            trackingManager.log(
                TrackedEvent.Builder("SHOW APP REVIEW DIALOG")
                    .addProperty("FLAG", devicePreferencesRepository.showAppReviewPopup.toString())
                    .addProperty("TEST COUNT", preferencesRepository.testCompletedCount.toString())
                    .addProperty(
                        "REQUIRE_UPDATE",
                        forceUpdateManager.isRequireUpdate().first.not().toString()
                    )
                    .addProperty("SUCCESS", "FALSE")
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }

    private fun observeTodayTodoDataState() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    calculateNumberOfRecommendations()
                }
            }
        }
    }

    private fun observeViewPlanState() {
        viewModelScope.launch {
            buildPlanManager.getViewPlanState().collect {
                if (it) {
                    buildPlanManager.clearViewPlanState()
                    onPlanClicked()
                }
            }
        }
    }

    private fun observeOpenChatChannelState() {
        viewModelScope.launch {
            chatManager.getOpenChatChannelState().collect {
                if (it) {
                    chatManager.clearOpenChatChannelState()
                    onChatClicked()
                }
            }
        }
    }

    private fun observeTestRecommendationState() {
        viewModelScope.launch {
            scoreManager.getFirstTestState().collect {
                if (it) {
                    scoreManager.clearFirstTestState()
                    state.showPlanHint = true
                }
            }
            scoreManager.getPlanRecommendationTestState().collect {
                if (it) {
                    calculateNumberOfRecommendations()
                }
            }
        }
    }

    private fun setTrackingUserIdentification() {
        trackingManager.setTrackingUserIdentification()
        loggingManager.setUserId(preferencesRepository.contactId)
    }

    fun handleDeepLinkNavigation() {
        val deepLink = preferencesRepository.appsFlyerAttributeDeepLink
        deepLink?.let {
            // reset the value
            preferencesRepository.appsFlyerAttributeDeepLink = null
            showLoading()
            viewModelScope.launch {
                scoreManager.getWellnessScores()
                    .onSuccess { score ->
                        if (!score.wellness.isNullOrEmpty()) {
                            when (deepLink) {
                                GO_TO_PLAN -> onPlanClicked()
                                GO_TO_PLAN_BUILD ->
                                    state.navigateTo.value =
                                        HomeNavGraphDirections.globalActionToRecommendationsFragment()
                                GO_TO_ORDER_SUPPLEMENTS -> orderSupplements()
                                GO_TO_ORDER_NUTRITIONAL_COACHING ->
                                    state.navigateTo.value =
                                        HomeNavGraphDirections.globalActionToNutritionCoachFragment()
                                GO_TO_ORDER_NUTRITIONIST_CHAT -> onChatClicked()
                                GO_TO_HYDRATION_PLAN ->
                                    state.navigateTo.value =
                                        HomeNavGraphDirections.globalactionToHydrationPlanFragment()
                                GO_TO_SUPPLEMENTS_PLAN ->
                                    state.navigateTo.value =
                                        HomeNavGraphDirections.globalActionToSupplements()
                                GO_TO_FOOD_PLAN ->
                                    state.navigateTo.value =
                                        HomeNavGraphDirections.globalActionToFoodPlanFragment()
                                GO_TO_STRESS_PLAN ->
                                    state.navigateTo.value =
                                        HomeNavGraphDirections.globalActionToStressReliefPlan()
                                GO_TO_TAKE_TEST -> {
                                    preferencesRepository.appsFlyerAttributeUrl?.let {
                                        trackingManager.log(
                                            TrackedEvent.Builder(TrackingConstants.EXTERNAL_CARD_SCAN)
                                                .addProperty(
                                                    TrackingConstants.KEY_QR_CODE,
                                                    it.split(QR_DELIMITER).last()
                                                )
                                                .withKlaviyo()
                                                .withFirebase()
                                                .withAmplitude()
                                                .create()
                                        )
                                    }
                                }
                            }
                        }
                    }
                    .onError {
                        hideLoading()
                    }
            }
        }

        val pushWooshDeepLink = preferencesRepository.pushWooshAttributeDeepLink
        pushWooshDeepLink?.let {
            preferencesRepository.pushWooshAttributeDeepLink = null
            onPlanClicked()
        }
    }

    private fun orderSupplements() {
        showLoading()
        viewModelScope.launch {
            authManager.multipass(Constants.PATH_SUPPLEMENTS)
                .onSuccess {
                    hideLoading(false)
                    state.navigateTo.value =
                        HomeNavGraphDirections.globalActionToWeb(it.multipass_url)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onHttpOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun onScreenShown() {
        if (devicePreferencesRepository.showPlanUpdatedDialog) {
            calculateNumberOfRecommendations()
            onResultsClicked()
        }
    }

    fun updatePlanHintState() {
        devicePreferencesRepository.showPlanUpdatedDialog = false
    }

    companion object {
        const val GO_TO_ORDER_SUPPLEMENTS = "/order/supplements"
        const val GO_TO_ORDER_NUTRITIONAL_COACHING = "/order/nutritionalCoaching"
        const val GO_TO_ORDER_NUTRITIONIST_CHAT = "/order/nutritionistChat"
        const val GO_TO_PLAN_BUILD = "/plan/build"
        const val GO_TO_PLAN = "/plan"
        const val GO_TO_HYDRATION_PLAN = "/plan/hydration"
        const val GO_TO_SUPPLEMENTS_PLAN = "/plan/supplements"
        const val GO_TO_FOOD_PLAN = "/plan/food"
        const val GO_TO_STRESS_PLAN = "/plan/cortisol"
        const val GO_TO_TAKE_TEST = "/takeTest"
        private const val QR_DELIMITER = "uuid="
        private const val NOTIFICATION_CHAT_FLAG_APPEARANCE_DURATION = 1000L
    }
}
