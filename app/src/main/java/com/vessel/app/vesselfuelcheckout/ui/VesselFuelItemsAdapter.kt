package com.vessel.app.vesselfuelcheckout.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.vesselfuelcheckout.model.VesselFuelItemUiModel

class VesselFuelItemsAdapter : BaseAdapterWithDiffUtil<VesselFuelItemUiModel>() {
    override fun getLayout(position: Int) = R.layout.item_vessl_fuel

    override fun getHandler(position: Int) = null
}
