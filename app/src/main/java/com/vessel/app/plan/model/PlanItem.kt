package com.vessel.app.plan.model

import androidx.annotation.DrawableRes
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.common.model.Program
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.util.extensions.isSameDay
import com.vessel.app.util.extensions.toCalender
import com.vessel.app.util.extensions.toCurrentWeekDate
import com.vessel.app.wellness.net.data.LikeStatus
import java.util.*
import kotlin.math.abs

data class PlanItem(
    val id: Int,
    val title: String,
    val valueProp1: String? = null,
    val reminderTime: String? = null,
    val backgroundUrl: String,
    @DrawableRes val background: Int,
    val planName: String,
    var isCompleted: Boolean,
    val isGrouped: Boolean,
    val groupCount: Int,
    val todos: List<PlanData>,
    val plans: List<PlanData>,
    val type: PlanType,
    val dayOfWeek: List<Int>? = null,
    val programDates: List<Date?>? = null,
    val program: Program?,
    val currentPlan: PlanData,
    val playAnimation: Boolean = false,
    var likeStatus: LikeStatus = LikeStatus.NO_ACTION,
    var isProgramActivity: Boolean? = false
) {

    fun getOccurrenceDates(): List<Date> {
        val splitDays = mutableListOf<Date>()
        val futureLimitCalender = Calendar.getInstance()
        futureLimitCalender.add(Calendar.DATE, 7)
        val today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())!!
        val futureLimitDate =
            Constants.DAY_DATE_FORMAT.parse(futureLimitCalender.time.formatDayDate())

        if (programDates?.isEmpty() == false) {
            programDates.filterNotNull().onEach { unformattedDate ->
                val date = Constants.DAY_DATE_FORMAT.parse(unformattedDate.formatDayDate())
                date?.let {
                    if (date.isSameDay(today) || (date.after(today) && date.before(futureLimitDate)))
                        splitDays.add(date)
                }
            }
        } else {
            dayOfWeek?.onEach {
                val date = Constants.DAY_DATE_FORMAT.parse(it.toCurrentWeekDate().formatDayDate())
                date?.let {
                    if (date.isSameDay(today) || (date.after(today) && date.before(futureLimitDate)))
                        splitDays.add(date)
                    val nextWeekOccurrence = date.toCalender()
                    nextWeekOccurrence.add(Calendar.DATE, 7)
                    val nextOccurrenceDate = nextWeekOccurrence.time
                    if (nextOccurrenceDate.before(futureLimitDate))
                        splitDays.add(Constants.DAY_DATE_FORMAT.parse(nextOccurrenceDate.formatDayDate()))
                }
            }
        }
        return splitDays
    }
    fun getListDates(): List<Date> {
        val splitDays = mutableListOf<Date>()
        val pastLimitCalender = Calendar.getInstance()
        pastLimitCalender.add(Calendar.DATE, -7)
        val today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())!!
        val pastLimitDate =
            Constants.DAY_DATE_FORMAT.parse(pastLimitCalender.time.formatDayDate())

        if (programDates?.isEmpty() == false) {
            programDates.filterNotNull().onEach { unformattedDate ->
                val date = Constants.DAY_DATE_FORMAT.parse(unformattedDate.formatDayDate())
                date?.let {
                    if (date.isSameDay(today) || (date.before(today) && date.after(pastLimitDate)))
                        splitDays.add(date)
                }
            }
        } else {
            dayOfWeek?.onEach {
                val date = Constants.DAY_DATE_FORMAT.parse(it.toCurrentWeekDate().formatDayDate())
                date?.let {
                    if (date.isSameDay(today) || (date.before(today) && date.after(pastLimitDate)))
                        splitDays.add(date)
                    val previousWeekOccurrence = date.toCalender()
                    previousWeekOccurrence.add(Calendar.DATE, -7)
                    val previousOccurrenceDate = previousWeekOccurrence.time
                    if (previousOccurrenceDate.after(pastLimitDate))
                        splitDays.add(Constants.DAY_DATE_FORMAT.parse(previousOccurrenceDate.formatDayDate()))
                }
            }
        }
        return splitDays
    }

    fun isAllTodayItemsChecked(): Boolean {
        val today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())!!
        val occurrenceTodayCount = todos.sumBy {
            it.day_of_week?.filter {
                Constants.DAY_DATE_FORMAT.parse(it.toCurrentWeekDate().formatDayDate())
                    .isSameDay(today)
            }?.size ?: 0
        }
        val completedTodayItemsCount = todos.sumBy {
            it.plan_records?.filter {
                Constants.DAY_DATE_FORMAT.parse(it.date).isSameDay(today) && it.completed == true
            }?.size ?: 0
        }
        return occurrenceTodayCount == completedTodayItemsCount
    }

    fun uncheckedGroupItems(): String {
        return abs(programDates?.size ?: 0 - plans.size).toString() + "x"
    }

    fun isBonus(date: Date): Boolean {
        var isBonus = false
        for (planItem in plans) {
            isBonus = currentPlan.program_id == null
            planItem.goals?.let { goals ->
                for (goal in goals) {
                    if (goal.impact != null && goal.impact < 3) {
                        isBonus = true
                        planItem.plan_records?.let { records ->

                            val record = records.firstOrNull { planRecord ->
                                planRecord.date == date.formatDayDate()
                            }

                            isBonus = !(record?.is_deleted ?: true)
                        }
                    } else {
                        return false
                    }
                }
            }
        }
        return isBonus
    }

    fun isTodayActivity(date: Date): Boolean {
        var isTodayActivity = false
        if (isBonus(date)) return false
        else {
            for (planItem in plans) {
                planItem.goals?.let { goals ->
                    isTodayActivity = true
                    planItem.plan_records?.let { records ->
                        val record = records.firstOrNull { planRecord ->
                            planRecord.date == date.formatDayDate()
                        }
                        isTodayActivity = !(record?.is_deleted ?: true)
                    }
                }
            }
        }
        return isTodayActivity
    }
    fun getLikeBackground() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.ic_like_button
    else
        R.drawable.ic_thumb_up

    fun getDisLikeBackground() = if (likeStatus == LikeStatus.DISLIKE)
        R.drawable.ic_thumb_down_red
    else
        R.drawable.ic_thumb_down
}
