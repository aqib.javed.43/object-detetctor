package com.vessel.app.changepassword.ui

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.changepassword.ChangePasswordViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChangePasswordFragment : BaseFragment<ChangePasswordViewModel>() {
    override val viewModel: ChangePasswordViewModel by viewModels()
    override val layoutResId = R.layout.fragment_change_password

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observe(viewModel.state.showSuccessToast) {
            Toast.makeText(
                requireContext(),
                getString(R.string.password_successfully_updated),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
