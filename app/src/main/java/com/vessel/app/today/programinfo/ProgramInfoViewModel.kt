package com.vessel.app.today.programinfo

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.Program
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class ProgramInfoViewModel @ViewModelInject constructor(
    val state: ProgramsInfoState,
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    private val programManager: ProgramManager
) : BaseViewModel(resourceProvider),
    ProgramInfoAdapter.OnActionHandler,
    ToolbarHandler {
    private val userChosenGoals = goalsRepository.getUserChosenGoals()

    init {
        var position = 0
        val goals = goalsRepository.getGoals()
            .map { entry ->
                entry.key.let { goal ->
                    GoalSelect(
                        id = goal.id,
                        title = goal.title,
                        recommendation1 = goal.recommendation1,
                        recommendation2 = goal.recommendation2,
                        background = Goal.alternateBackground(position++),
                        image = goal.icon,
                        checked = userChosenGoals.any { it.id == goal.id },
                    )
                }
            }
        loadPrograms(goals)
    }
    fun loadPrograms(goals: List<GoalSelect>) {
        showLoading()
        viewModelScope.launch() {
            programManager.getEnrolledPrograms()
                .onSuccess { enrolledPrograms ->
                    state.items.value = enrolledPrograms.map { pro ->
                        pro.copy(image = goals.firstOrNull { it.id == pro.mainGoalId }?.image)
                    }
                    hideLoading()
                }
                .onServiceError {
                    hideLoading()
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }
                .onNetworkIOError {
                    hideLoading()
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    hideLoading()
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    override fun onGoalSelectSelected(item: Program) {
        redirectProgramDetails(item)
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    private fun redirectProgramDetails(program: Program) {
        state.navigateTo.value =
            ProgramInfoFragmentDirections.globalActionToProgramDetailsFragment(
                program
            )
    }
}
