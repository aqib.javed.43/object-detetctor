package com.vessel.app.createaccount

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.*
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.createaccount.ui.CreateAccountFragmentDirections
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.validators.*
import kotlinx.coroutines.launch

class CreateAccountViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceRepository: ResourceRepository,
    private val contactManager: ContactManager,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository), ToolbarHandler {
    val state = CreateAccountState(
        savedStateHandle.get<String>("email")!!,
        savedStateHandle.get<Boolean>("fromAppleGoogleSSOAuth")!!
    )
    init {
        if (state.fromAppleGoogleSSOAuth) {
            fillUserFromGoogleCreate()
        }
    }
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun continueClicked(view: View) {
        if (!validateParams()) {
            return
        }

        if (state.fromAppleGoogleSSOAuth) {
            saveFirstNameLastNameAndContinueToOnboarding(view)
        } else {
            createContact(view)
        }
    }

    private fun createContact(view: View) {
        showLoading()
        viewModelScope.launch {
            contactManager.createContact(
                email = state.userEmail.value!!,
                password = state.userPassword.value!!,
                firstName = state.userFirstName.value!!,
                lastName = state.userLastName.value!!
            )
                .onSuccess {
                    hideLoading()

                    preferencesRepository.userFirstName =
                        it.firstName ?: state.userFirstName.value!!
                    preferencesRepository.userLastName = it.lastName ?: state.userLastName.value!!
                    preferencesRepository.userEmail = it.email ?: state.userEmail.value!!
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.CREATE_ACCOUNT_SUCCESS)
                            .setEmail(state.userEmail.value!!)
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    view.findNavController()
                        .navigate(CreateAccountFragmentDirections.actionCreateAccountFragmentToWelcomeSignUpFragment())
                }
                .onServiceError {
                    val errorMessage = it.error.message ?: getResString(R.string.unknown_error)
                    logCreateAccountFailure(errorMessage)
                    showError(errorMessage)
                }
                .onNetworkIOError {
                    val errorMessage = getResString(R.string.network_error)
                    logCreateAccountFailure(errorMessage)
                    showError(errorMessage)
                }
                .onUnknownError {
                    val errorMessage = getResString(R.string.unknown_error)
                    logCreateAccountFailure(errorMessage)
                    showError(errorMessage)
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun saveFirstNameLastNameAndContinueToOnboarding(view: View) {
        preferencesRepository.userFirstName = state.userFirstName.value!!
        preferencesRepository.userLastName = state.userLastName.value!!
        view.findNavController()
            .navigate(CreateAccountFragmentDirections.actionCreateAccountFragmentToWelcomeSignUpFragment())
    }

    private fun logCreateAccountFailure(errorMessage: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CREATE_ACCOUNT_FAIL)
                .setEmail(state.userEmail.value!!)
                .setError(errorMessage)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun validateParams() = try {
        NameValidator(NameType.First).validate(state.userFirstName)
        NameValidator(NameType.Last).validate(state.userLastName)
        if (state.fromAppleGoogleSSOAuth) {
            true
        } else {
            PasswordValidator().validate(state.userPassword)
        }
    } catch (exception: ValidatorException) {
        showError(exception)
        false
    }
    private fun fillUserFromGoogleCreate() {
        showLoading()
        viewModelScope.launch {
            contactManager.getContact()
                .onSuccess {
                    hideLoading()
                    state {
                        userFirstName.postValue(it.firstName)
                        userLastName.postValue(it.lastName)
                        userEmail.postValue(it.email)
                    }
                }
                .onError {
                    hideLoading()
                }
        }
    }
}
