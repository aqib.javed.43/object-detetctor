package com.vessel.app.pretest.incuppee.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.pretest.incuppee.InCupPeeViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InCupPeeFragment : BaseFragment<InCupPeeViewModel>() {
    override val viewModel: InCupPeeViewModel by viewModels()
    override val layoutResId = R.layout.fragment_in_cup_pee

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
                requireActivity().finish()
            }
        }
    }
}
