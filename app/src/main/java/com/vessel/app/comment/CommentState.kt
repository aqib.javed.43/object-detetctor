package com.vessel.app.comment

import androidx.navigation.NavDirections
import com.vessel.app.teampage.model.TeamPostItem
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CommentState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val postItem = LiveEvent<TeamPostItem>()
    operator fun invoke(block: CommentState.() -> Unit) = apply(block)
}
