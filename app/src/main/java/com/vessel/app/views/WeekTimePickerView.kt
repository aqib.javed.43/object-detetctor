package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.vessel.app.databinding.WeekTimePickerViewBinding
import com.vessel.app.views.weekdays.WeekDaysClickListener
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.button
import java.util.*

open class WeekTimePickerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private lateinit var binding: WeekTimePickerViewBinding

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WeekTimePickerViewBinding.inflate(layoutInflater, this, true)
    }

    fun setSelectedWeekDays(list: List<Int>?) {
        binding.weekDaysView.setSelectedDays(list ?: (0..WEEK_DAYS_COUNT).toMutableList())
    }

    fun setTime(time: Date?) {
        time?.let {
            val calendar = Calendar.getInstance()
            calendar.time = time
            binding.timePicker.hour = calendar.get(Calendar.HOUR_OF_DAY)
            binding.timePicker.minute = calendar.get(Calendar.MINUTE)
        }
    }

    fun setText(@StringRes stringResId: Int) {
        button.setText(stringResId)
    }

    fun setHint(@StringRes stringResId: Int) {
        binding.daysOfWeekHint.isVisible = true
        binding.daysOfWeekHint.setText(stringResId)
    }

    fun setHint(hint: String) {
        binding.daysOfWeekHint.isVisible = true
        binding.daysOfWeekHint.text = hint
    }

    fun setOnWeekDaysClickListener(onWeekDaysClickListener: WeekDaysClickListener?) {
        binding.weekDaysView.onWeekDaysClickListener = onWeekDaysClickListener
    }
    fun getSelectedDays() = binding.weekDaysView.getSelectedDays()

    fun getSelectedHour() = binding.timePicker.hour
    fun getSelectedMinute() = binding.timePicker.minute

    companion object {
        const val WEEK_DAYS_COUNT = 7
    }
}
