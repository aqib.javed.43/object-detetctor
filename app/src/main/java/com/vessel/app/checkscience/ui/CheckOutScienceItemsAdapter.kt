package com.vessel.app.checkscience.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.checkscience.model.CheckOutScienceViewItem

class CheckOutScienceItemsAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<CheckOutScienceViewItem>() {

    override fun getLayout(position: Int) =
        if (getItem(position).showAsReference)
            R.layout.item_reference
        else
            R.layout.item_check_out_science_source

    override fun getHandler(position: Int): Any? = handler
}
