Here are the reference ranges that we use for folate:
Low: 0 - 20 ug/L
Good: 20 - 60 ug/L
High: >60 ug/L

Folate is absorbed in the middle part of the small intestine. Approx 5-10 mg is stored in the average adult, half of which is in the liver. Because excretion of excess folate occurs through both bile and urine we use urine levels to estimate the body’s folate levels.

When you’re in the good zone (darker purple), your body is getting enough folate to support your wellness. Too high means you may be getting more than you need. Although there isn’t much evidence of danger from high levels of folate from food, if you’re taking supplements, you may be taking more than you need, and this can put you at risk for hiding a B12 deficiency.  Too low means your kidneys are holding on to folate to improve your body’s stores, and you may want to up your intake. Check out the recommendations in the app for how to do that.

**References:**

-   https://pubmed.ncbi.nlm.nih.gov/18635909/
-  All urinary vitamins and their metabolite levels except vitamin B(12) increased linearly in a dose-dependent manner, and highly correlated with vitamin intake (r=0.959 for vitamin B(1), r=0.927 for vitamin B(2), r=0.965 for vitamin B(6), r=0.957 for niacin, r=0.934 for pantothenic acid, r=0.907 for folic acid, r=0.962 for biotin, and r=0.952 for vitamin C). These results suggest that measuring urinary water-soluble vitamins and their metabolite levels can be used as good nutritional markers for assessing vitamin intakes.

-   https://pubmed.ncbi.nlm.nih.gov/20417877/
- "We conclude that urinary levels of water-soluble vitamins, except for B12, reflected their recent intake in free-living Japanese elderly females and could be used as a measure of their intake during the previous few days."

-   https://pubmed.ncbi.nlm.nih.gov/16392702/
- A study to determine average values of urine B vitamins and vitamin c in college students eating a monitored diet.

-   https://pubmed.ncbi.nlm.nih.gov/20086315/
- A study of Japanese college students given a strict diet for 7 days, and measuring differences in urinary vitamin C and B vitamin levels.

-   https://pubmed.ncbi.nlm.nih.gov/23883688/
- Urinary excretion levels of water-soluble vitamins in pregnant and lactating women in Japan- "No change was observed in the urinary excretion levels of vitamin B2, vitamin B6, vitamin B12, biotin or vitamin C among stages."

-   https://www.ncbi.nlm.nih.gov/pubmed/16760376
- The molecular mass of folate (440 Da) suggests that free folate is freely filtered in the glomeruli. Due to the association of folate with serum proteins, the filtration of folate in humans is estimated to be 50–65% of that of inulin, suggesting a filtered load of free folate of 1 mg/24 h in humans.

-   https://www.ncbi.nlm.nih.gov/pubmed/16392702
- 1 study of Japanese males, urine folate was: 19.4+/-2.8 nmol/d

-   https://www.ncbi.nlm.nih.gov/books/NBK114318/
- Most folate filtered in the kidneys is reabsorbed
