package com.vessel.app.common.net.payload

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SearchFilterPayload(
    val allergies: List<String> = emptyList(),
    val food_categories: List<String> = emptyList(),
    val diets: List<String> = emptyList(),
    val food_title: String = "",
    val nutrients: List<String> = emptyList()
)
