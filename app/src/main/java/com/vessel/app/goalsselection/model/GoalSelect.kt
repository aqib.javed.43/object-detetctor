package com.vessel.app.goalsselection.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GoalSelect(
    val id: Int,
    @StringRes val title: Int,
    @StringRes val recommendation1: Int,
    @StringRes val recommendation2: Int,
    @DrawableRes val background: Int,
    @DrawableRes val image: Int,
    val checked: Boolean,
    @DrawableRes val checkBoxBackground: Int = R.drawable.linen_circle,
) : Parcelable
