package com.vessel.app.cardchoices.bought

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BoughtCardChoiceFragment : BaseFragment<BoughtCardChoiceViewModel>() {
    override val viewModel: BoughtCardChoiceViewModel by viewModels()
    override val layoutResId = R.layout.fragment_bought_card_choice

    override fun onViewLoad(savedInstanceState: Bundle?) {
        requireView().findViewById<TextView>(R.id.subtitle).movementMethod =
            LinkMovementMethod.getInstance()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(closeEvent) { requireActivity().finish() }
        }
    }
}
