package com.vessel.app.goal.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.goal.model.GoalHeader

class GoalHeaderAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is GoalHeader -> R.layout.item_goal_header
        else -> throw IllegalStateException("Unexpected GoalHeaderAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any? = when (getItem(position)) {
        else -> handler
    }

    interface OnActionHandler {
        fun onInfoButtonClick()
    }
}
