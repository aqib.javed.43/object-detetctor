package com.vessel.app.views

import android.animation.ValueAnimator
import android.content.Context
import android.text.Layout
import android.text.style.AlignmentSpan
import android.text.style.RelativeSizeSpan
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import com.vessel.app.R

class ChartCurveSelectView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var reagentId: Int = -1
    private val rangeLinesLayout by lazy { findViewById<ConstraintLayout>(R.id.rangeLinesLayout) }
    private val selectView by lazy { findViewById<WhiteRoundedCornersBlurView>(R.id.selected_view) }
    private val entryLabelValue by lazy { findViewById<AppCompatTextView>(R.id.entryLabelValue) }
    private val averageLabelValue by lazy { findViewById<AppCompatTextView>(R.id.averageLabelValue) }
    var isFirstAppear: Boolean = true
    init {
        inflate(getContext(), R.layout.view_chart_curve_select, this)
    }

    private fun startFadeAnimation() {
        val anim = AlphaAnimation(0f, 1f)
        anim.duration = HIGHLIGHT_FADE_ANIMATION_DURATION
        entryLabelValue.startAnimation(anim)
    }

    private fun startDownAnimation() {
        ValueAnimator.ofInt(0, measuredHeight).apply {
            addUpdateListener { valueAnimator ->
                val currentHeight = valueAnimator.animatedValue as Int
                selectView.updateLayoutParams {
                    height = currentHeight
                }
            }
            duration = HIGHLIGHT_GROUP_ANIMATION_DURATION
            start()
        }
    }

    fun startTransition(x: Float, y: Float, xValue: String, hint: String? = null, showNumber: Boolean) {
        if (isFirstAppear) {
            translationX = x - X_AXIS_SPACING
            isFirstAppear = false
            startDownAnimation()
            startFadeAnimation()
        } else {
            animate().translationX(x - X_AXIS_SPACING)
                .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
        }
        updateLabelValue(y, xValue, hint, showNumber)
    }

    fun updateLabelValue(newPoint: Float, value: String, hint: String? = null, showNumber: Boolean) {
        entryLabelValue.apply {
            animate()
                .translationY(newPoint)
                .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
            text = getLabelValueText(value, hint)
            isVisible = showNumber
        }
    }

    fun updateAverageValue(newPoint: Float, showAverageValue: Boolean = true) {
        averageLabelValue.apply {
            isVisible = showAverageValue
            animate()
                .translationX(X_AXIS_SPACING)
                .translationY(newPoint + Y_AXIS_SPACING)
                .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
        }
    }

    private fun getLabelValueText(value: String, hint: String?) = buildSpannedString {
        append(value)
        if (hint != null) {
            appendLine()
            inSpans(
                RelativeSizeSpan(SCALED_HINT_LABEL_TEXT_SIZE),
                AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER)
            ) {
                append(hint)
            }
        }
    }

    fun setReagentId(reagentId: Int) {
        this.reagentId = reagentId
        rangeLinesLayout.apply {
            visibility = View.GONE
        }
    }

    companion object {
        const val X_AXIS_SPACING = 55f
        const val Y_AXIS_SPACING = 50f
        const val X_LABEL_ANIMATION_DURATION = 500L
        const val HIGHLIGHT_GROUP_ANIMATION_DURATION = 700L
        const val HIGHLIGHT_FADE_ANIMATION_DURATION = 900L
        const val SCALED_HINT_LABEL_TEXT_SIZE = 0.7f
    }
}
