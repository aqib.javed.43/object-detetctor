Since pH is so closely related to our diet, the fastest way to improve urinary pH in healthy adults is through diet & lifestyle changes.

It is important to stay well hydrated, avoiding beverages that have a diuretic effect (caffeine) or increase acidity (alcohol, sugary sodas). A good rule of thumb is to drink about half of your body weight (pounds) in ounces. For example, a 150-pound adult should aim for 75 ounces (7.5 8oz glasses) of water per day. Adding a squeeze of citrus may also help reduce the acidity of the urine.

**references:**

-   https://www.webmd.com/diet/a-z/alkaline-diets

-   https://draxe.com/alkaline-diet/

-   https://www.healthline.com/nutrition/the-alkaline-diet-myth

-   https://sciencebasedpharmacy.wordpress.com/2009/11/13/your-urine-is-not-a-window-to-your-body-ph-balancing-a-failed-hypothesis/

-   Israni AK, Kasiske BL. Laboratory assessment of kidney disease: glomerular filtration rate, urinalysis, and proteinuria. In: Taal MW, Chertow GM, et al, eds. Brenner & Rector's The Kidney. 9th ed. Philadelphia, PA: Saunders Elsevier; 2011:chap 25.

-   Michael J. Bono; Wanda C. Reygaert. Urinary Tract Infection last update December 2, 2019. https://www.ncbi.nlm.nih.gov/books/NBK470195/
