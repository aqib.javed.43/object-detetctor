package com.vessel.app.newtesttimer.dropletstab

import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DropletsTabFragment : BaseFragment<DropletsTabViewModel>() {
    override val viewModel: DropletsTabViewModel by viewModels()
    override val layoutResId = R.layout.fragment_droplets_tab
}
