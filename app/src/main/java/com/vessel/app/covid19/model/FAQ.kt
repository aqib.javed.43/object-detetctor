package com.vessel.app.covid19.model

import androidx.annotation.StringRes

data class FAQ(
    @StringRes val question: Int,
    @StringRes val answer: Int
) {
    var bodyHeight = -1
    var expanded = false
}
