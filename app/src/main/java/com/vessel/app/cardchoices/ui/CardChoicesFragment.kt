package com.vessel.app.cardchoices.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.cardchoices.CardChoicesViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CardChoicesFragment : BaseFragment<CardChoicesViewModel>() {
    override val viewModel: CardChoicesViewModel by viewModels()
    override val layoutResId = R.layout.fragment_card_choices

    override fun onViewLoad(savedInstanceState: Bundle?) {
        requireView().findViewById<RecyclerView>(R.id.choices).adapter = cardChoicesAdapter
        setupObservers()
    }

    private val cardChoicesAdapter by lazy {
        CardChoicesAdapter(viewModel)
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(choices) {
                cardChoicesAdapter.submitList(it)
            }
        }
    }
}
