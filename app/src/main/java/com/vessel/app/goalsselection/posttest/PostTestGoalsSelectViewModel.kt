package com.vessel.app.goalsselection.posttest

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants.USER_ALLOWED_GOALS_SELECTION_COUNT
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.ScoreManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Goal.Companion.alternateBackground
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.goalsselection.ui.GoalSelectAdapter
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import kotlinx.coroutines.launch

class PostTestGoalsSelectViewModel @ViewModelInject constructor(
    val state: PostTestGoalsSelectState,
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    private val scoreManager: ScoreManager,
    private val contactManager: ContactManager,
    private val buildPlanManager: BuildPlanManager,
    val homeTabsManager: HomeTabsManager,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider),
    GoalSelectAdapter.OnActionHandler,
    ToolbarHandler {

    private val userChosenGoals = goalsRepository.getUserChosenGoals()

    init {
        var position = 0
        state.items.value = goalsRepository.getGoals()
            .map { entry ->
                entry.key.let { goal ->
                    GoalSelect(
                        id = goal.id,
                        title = goal.title,
                        recommendation1 = goal.recommendation1,
                        recommendation2 = goal.recommendation2,
                        background = alternateBackground(position++),
                        image = goal.icon,
                        checked = userChosenGoals.any { it.id == goal.id },
                    )
                }
            }
    }

    override fun onGoalSelectClicked(item: GoalSelect) {
        state {
            val selectedItems = items.value.orEmpty().filter { it.checked }

            if (selectedItems.size >= USER_ALLOWED_GOALS_SELECTION_COUNT && item.checked.not()) {
                refreshItems.value = true
                return@state
            }

            items.value =
                items.value.orEmpty().mapIndexed { _, goalSelect ->
                    if (goalSelect.id == item.id) goalSelect.copy(checked = !item.checked)
                    else goalSelect
                }
            refreshItems.value = true
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        val selectedItems = state.items.value.orEmpty().filter { it.checked }
        if (selectedItems.size == USER_ALLOWED_GOALS_SELECTION_COUNT) {
            updateGoals(selectedItems)
        }
    }

    fun onSkipThisClicked() {
        state.navigateTo.value =
            PostTestGoalsSelectFragmentDirections.actionPostTestGoalsSelectFragmentToCreatePlanRecommendationFragment(
                RecommendationType.Food
            )
    }

    private fun updateGoals(selectedItems: List<GoalSelect>) {
        showLoading()
        logSelectedGoals(selectedItems.map { it.id })
        viewModelScope.launch {
            contactManager.updateGoals(
                selectedItems.map { it.id }
            ).onSuccess {
                hideLoading(false)
                scoreManager.updateFirstTestState()
                goalsRepository.setUserChosenGoals(
                    selectedItems.map {
                        GoalSample(
                            it.id,
                            getResString(it.title)
                        )
                    }
                )

                val userMainGoal = goalsRepository.getUserMainGoal()
                if (userMainGoal != null) {
//                    if (!goalsRepository.getUserHasEnrolledProgram()) {
//                        state.navigateTo.value =
//                            PostTestGoalsSelectFragmentDirections.actionPostTestGoalsSelectFragmentToProgramSelectionFragment(
//                                userMainGoal.id,
//                                false
//                            )
//                    } else { onSkipThisClicked() }
                    onSkipThisClicked()
                } else {
                    state.navigateTo.value =
                        PostTestGoalsSelectFragmentDirections.actionPostTestGoalsSelectFragmentToOneGoalSelectionFragment(
                            false
                        )
                }
            }.onServiceError {
                showError(it.error.message ?: getResString(R.string.unknown_error))
            }.onNetworkIOError {
                showError(getResString(R.string.network_error))
            }.onUnknownError {
                showError(getResString(R.string.unknown_error))
            }
            hideLoading()
        }
    }
    private fun logSelectedGoals(goals: List<Int>) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.GOALS_UPDATED)
                .addProperty(TrackingConstants.KEY_SELECT_GOALS, goals.joinToString(","))
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
