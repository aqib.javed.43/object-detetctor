package com.vessel.app.testscore.reagentgraph

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.squareup.moshi.Moshi
import com.vessel.app.BuildConfig
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import javax.inject.Inject

class ReagentGraphViewModel @ViewModelInject @Inject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    val moshi: Moshi,
    val resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
) : BaseViewModel(resourceProvider), ToolbarHandler {
    val state = ReagentGraphState(savedStateHandle["reagentHeader"]!!)

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onBetaTestingInfoClicked() {
        state.navigateTo.value =
            ReagentGraphFragmentDirections.globalActionToWeb(BuildConfig.HOME_PAGE_URL)
    }

    fun onCardErrorActionClicked() {
        state.navigateTo.value =
            ReagentGraphFragmentDirections.actionReagentGraphFragmentToTestErrorHintDialog()
    }
}
