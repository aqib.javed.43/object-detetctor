package com.vessel.app.common.manager

import com.vessel.app.common.repo.RecommendationRepository
import com.vessel.app.wellness.net.data.DeleteReviewRequest
import com.vessel.app.wellness.net.data.ReviewRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RecommendationManager @Inject constructor(
    private val recommendationRepository: RecommendationRepository
) {
    suspend fun getLatestSampleRecommendations(forceUpdate: Boolean = false) =
        recommendationRepository.getLatestSampleRecommendations(forceUpdate)

    suspend fun getLatestSampleFoodRecommendations(forceUpdate: Boolean = false) =
        recommendationRepository.getLatestSampleFoodRecommendations(forceUpdate)

    suspend fun getReagentFoodRecommendations(forceUpdate: Boolean = false, reagentId: Int) =
        recommendationRepository.getFoodRecommendations(forceUpdate, reagentId)

    fun updateLatestSampleRecommendationsCachedData() {
        CoroutineScope(SupervisorJob()).launch {
            getLatestSampleRecommendations(true)
        }
    }
    fun updateLatestSampleFoodRecommendationsCachedData() {
        CoroutineScope(SupervisorJob()).launch {
            getLatestSampleFoodRecommendations(true)
        }
    }

    suspend fun sendLikesStatus(
        record_id: Int,
        category: String,
        likeStatus: Boolean
    ) = recommendationRepository.sendLikeStatus(record_id, category, likeStatus)

    suspend fun unLikesStatus(
        record_id: Int,
        category: String
    ) = recommendationRepository.unLikeStatus(record_id, category)
    suspend fun postReview(
        request: ReviewRequest
    ) = recommendationRepository.postReview(request)
    suspend fun getReviews(
        record_id: Int,
        category: String
    ) = recommendationRepository.getReviews(record_id, category)
    fun clearCachedData() {
        recommendationRepository.clearCachedData()
    }
    suspend fun deleteReview(
        request: DeleteReviewRequest
    ) = recommendationRepository.deleteReview(request)
}
