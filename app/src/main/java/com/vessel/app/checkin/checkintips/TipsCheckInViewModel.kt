package com.vessel.app.checkin.checkintips

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.net.data.ProgramPayLoadNew
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.TipsProgramCardWidgetOnActionHandler
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.model.RecommendationItem
import com.vessel.app.wellness.model.RecommendationsPagingResponse
import com.vessel.app.wellness.model.RecommendationsSequenceType
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.coroutines.launch

class TipsCheckInViewModel @ViewModelInject constructor(
    val state: TipsCheckInState,
    private val planManager: PlanManager,
    private val resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    private val preferencesRepository: PreferencesRepository,
    private val programManager: ProgramManager,
    private val trackingManager: TrackingManager,
    private val recommendationManager: RecommendationManager,
    private val assessmentManager: AssessmentManager
) : BaseViewModel(resourceProvider), ToolbarHandler, TipsProgramCardWidgetOnActionHandler {
    var notice: String = ""
    var description: String = ""
    private val contactId = preferencesRepository.contactId

    init {
        val goalSelected = goalsRepository.getUserMainGoal()
        goalSelected?.let {
            setGoal(it)
            fetchUserTips(it.id)
        }
    }

    fun fetchUserTips(goalId: Int) {
        val badHabitList = Constants.BADHABIT_SELECTS
        description = "Based on '${badHabitList[0].title}'"
        val iBadHabit = badHabitList.map { it.id }.toList()
        viewModelScope.launch {
            assessmentManager.getRecommendationsWithHabits(
                goalId,
                sortingByLikes = true,
                sortingByNewest = true,
                habitIds = iBadHabit
            ).onSuccess { response ->
                viewModelScope.launch {
                    planManager.getUserPlans()
                        .onSuccess { planData ->
                            state.recommendations.value =
                                buildRecommendationsList(response, planData)
                        }
                }
            }
                .onServiceError {
                    hideLoading()
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }
    private fun buildRecommendationsList(
        response: RecommendationsPagingResponse,
        planData: List<PlanData>,
    ): List<RecommendationItem> {
        val recommendations = mutableListOf<RecommendationItem>()
        response.sequenceData.filter { it.type == RecommendationsSequenceType.TIP }.onEach { sequence ->
            var plans: List<PlanData>? = null
            val item = when (sequence.type) {
                RecommendationsSequenceType.FOOD -> {
                    plans = planData.filter { it.isFoodPlan() }.filter { it.food_id == sequence.id }
                    val isAddedToPlan = plans.isNotEmpty()
                    response.foods?.first { it.id == sequence.id }
                        ?.copy(isAddedToPlan = isAddedToPlan)
                }
                RecommendationsSequenceType.LIFESTYLE -> {
                    plans = planData.filter { it.isStressPlan() }
                        .filter { it.reagent_lifestyle_recommendation_id == sequence.id }
                    val isAddedToPlan = plans.isNotEmpty()
                    response.lifestyle?.first { it.id == sequence.id }
                        ?.copy(isAddedToPlan = isAddedToPlan)
                }
                RecommendationsSequenceType.TIP -> {
                    plans = planData.filter { it.isTip() }.filter { it.tip_id == sequence.id }
                    val isAddedToPlan = plans.isNotEmpty()
                    response.tips?.first { it.id == sequence.id }
                        ?.copy(isAddedToPlan = isAddedToPlan)
                }
                RecommendationsSequenceType.NOTHING -> null
            }
            item?.let { recommendations.add(RecommendationItem(plans, it)) }
        }
        return recommendations
    }
    private fun enrollProgram(programId: Int, programAction: ProgramPayLoadNew) {
        state {
            viewModelScope.launch {
                showLoading()
                programManager.joinProgramString(programId, programAction)
                    .onSuccess {
                        hideLoading()
                    }
                    .onServiceError {
                        hideLoading()
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }
                    .onError {
                        hideLoading()
                    }
            }
        }
    }

    companion object {
        const val TAG = "TipsCheckInViewModel"
    }

    fun onDoneClicked(view: View) {
        Navigation.findNavController(view).popBackStack(R.id.homeScreenFragment, false)
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun setGoal(goal: GoalSample) {
        notice = "Add these to your program, they'll help with ${goal.name}."
    }

    override fun onProgramItemClicked(program: RecommendationItem) {
    }

    override fun onProgramInfoClicked(program: RecommendationItem) {
    }

    override fun onJoinProgramClicked(program: RecommendationItem) {
    }

    override fun onItemLikeClicked(item: RecommendationItem) {
        item.planData?.let {
            if (it.isEmpty() || item.planData[0].program_id == null) return
            if (item.iRecommendation.getLikedStatus() != LikeStatus.LIKE) {
                sendLikeStatus(item.planData[0].program_id!!)
            } else {
                unLikeStatus(item.planData[0].program_id!!)
            }
        }
    }

    private fun sendLikeStatus(programId: Int) {
        viewModelScope.launch {
            recommendationManager.sendLikesStatus(
                programId,
                LikeCategory.Program.value,
                true
            )
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CONTENT_LIKED)
                .addProperty(TrackingConstants.KEY_ID, programId.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Program.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun unLikeStatus(
        programId: Int
    ) {
        viewModelScope.launch {
            recommendationManager.unLikesStatus(
                programId,
                LikeCategory.Program.value
            )
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CONTENT_UNLIKED)
                .addProperty(TrackingConstants.KEY_ID, programId.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Program.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
