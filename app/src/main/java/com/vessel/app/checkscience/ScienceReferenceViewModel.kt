package com.vessel.app.checkscience

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.checkscience.ui.OnActionHandler
import com.vessel.app.checkscience.utils.CheckOutScienceMapper
import com.vessel.app.util.ResourceRepository

class ScienceReferenceViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    private val resourceProvider: ResourceRepository,
) : BaseViewModel(resourceProvider),
    OnActionHandler {

    val state = ScienceReferenceState(savedStateHandle["scienceReference"]!!)

    fun mapReferencesToScienceViewItems() = CheckOutScienceMapper.mapReferencesToScienceViewItems(state.scienceReferences, this, resourceProvider)

    override fun onLinkButtonClick(view: View, link: String) {
        state.navigateTo.value = HomeNavGraphDirections.globalActionToWeb(link)
    }

    fun onGotItClicked() {
        state.dismissDialog.call()
    }
}
