package com.vessel.app.views

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import kotlinx.android.synthetic.main.widget_score_impact.view.*

class ScoreImpactWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.widget_score_impact, this)
    }

    fun setText(text: String) {
        textView.text = text
    }

    fun setSrc(drawable: Drawable) {
        imageView.setImageDrawable(drawable)
    }

    fun setImageBackground(drawable: Drawable) {
        imageView.background = drawable
    }
}
