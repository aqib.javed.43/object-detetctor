package com.vessel.app.filterfoodplandialog.ui

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.filterfoodplandialog.FoodPlanFilterViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FoodPlanFilterDialog : BaseDialogFragment<FoodPlanFilterViewModel>() {
    override val viewModel: FoodPlanFilterViewModel by viewModels()
    override val layoutResId = R.layout.fragment_food_plan_filter_dialog
    private val listSpanCount by lazy { resources.getInteger(R.integer.food_filters_list_span_count) }
    private val sortHeaderAdapter by lazy { FilterHeaderAdapter() }
    private val sortOptionsAdapter by lazy { FilterSelectAdapter(viewModel) }

    private val filterByCategoryHeaderAdapter by lazy { FilterHeaderAdapter() }
    private val filterFooterAdapter by lazy { FilterFooterAdapter(viewModel) }
    private val filterByCategoryAdapter by lazy { FilterSelectAdapter(viewModel) }

    private val filterByNutrientHeaderAdapter by lazy { FilterHeaderAdapter() }
    private val filterByNutrientAdapter by lazy { FilterSelectAdapter(viewModel) }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        findNavController().previousBackStackEntry
            ?.savedStateHandle
            ?.set(SELECTED_FILTER_OPTIONS, viewModel.state.selectedFilterAndSortOptions)
        super.onDismiss(dialog)
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        view?.findViewById<RecyclerView>(R.id.dialogRecyclerView)
            ?.also {
                val adapter = ConcatAdapter(
                    ConcatAdapter.Config.Builder()
                        .setIsolateViewTypes(false)
                        .build(),
                    sortHeaderAdapter,
                    sortOptionsAdapter,
                    filterByCategoryHeaderAdapter,
                    filterByCategoryAdapter,
                    filterByNutrientHeaderAdapter,
                    filterByNutrientAdapter,
                    filterFooterAdapter
                )
                val filterSpanSizeLookup = FilterSpanSizeLookup(requireContext(), adapter)
                it.adapter = adapter
                it.itemAnimator = null
                it.layoutManager = GridLayoutManager(requireContext(), listSpanCount).apply {
                    spanSizeLookup = filterSpanSizeLookup
                }
            }
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(sortHeaderItem) {
                sortHeaderAdapter.submitList(listOf(it))
            }

            observe(sortOptionsList) {
                sortOptionsAdapter.submitList(it)
            }

            observe(foodCategoriesHeader) {
                filterByCategoryHeaderAdapter.submitList(listOf(it))
            }
            observe(foodCategoriesList) {
                filterByCategoryAdapter.submitList(it)
            }

            observe(nutrientHeaderItem) {
                filterByNutrientHeaderAdapter.submitList(listOf(it))
            }

            observe(nutrientOptionsList) {
                filterByNutrientAdapter.submitList(it)
            }

            observe(dismissDialog) {
                dismiss()
            }

            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }

        filterFooterAdapter.submitList(listOf(Unit))
    }
    companion object {
        const val SELECTED_FILTER_OPTIONS = "selectedFilterOptions"
    }
}
