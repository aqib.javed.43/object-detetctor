package com.vessel.app.buildplan.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.buildplan.BuildPlanViewModel
import com.vessel.app.common.util.DisclaimerFooter
import com.vessel.app.common.util.DisclaimerFooterAdapter
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_plan.*
import kotlinx.android.synthetic.main.item_add_program_header.*

@AndroidEntryPoint
class BuildPlanFragment : BaseFragment<BuildPlanViewModel>() {
    override val viewModel: BuildPlanViewModel by viewModels()
    override val layoutResId = R.layout.fragment_build_plan

    private val planAdapter by lazy { MakePlanAdapter(viewModel) }
    private val planFooterAdapter by lazy { DisclaimerFooterAdapter() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupHeader()
        setupList()
        setupObservers()
    }

    private fun setupHeader() {
        btnAddNewProgram.setOnClickListener {
            viewModel.onAddProgramClicked(it)
        }
    }

    private fun setupList() {
        planList.also {
            val adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                planAdapter,
                planFooterAdapter
            )
            it.adapter = adapter
            it.layoutManager = GridLayoutManager(requireContext(), 1)
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(makePlanItems) {
                planAdapter.submitList(it) {
                    planList.scrollToPosition(0)
                }
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
        planFooterAdapter.submitList(listOf(DisclaimerFooter.Default))
        viewModel.loadMakePlan()
    }
}
