package com.vessel.app.common.adapter.planedit

data class PlanEditItem(
    val title: String
)
