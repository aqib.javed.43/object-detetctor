package com.vessel.app.stressreliefdetails.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StressReliefDetailsItemModel(
    val title: String,
    val description: String,
    val life_style_recommendation_id: Int,
    val isAdded: Boolean
) : Parcelable
