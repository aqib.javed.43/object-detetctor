package com.vessel.app.newtesttimer

import androidx.lifecycle.MutableLiveData
import com.vessel.app.R
import com.vessel.app.activationtimer.ActivationTimerState
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class NewTestTimerState @Inject constructor() {
    val showBackWarning = LiveEvent<Unit>()
    val navigateToCapture = LiveEvent<Unit>()
    val moveToTab = MutableLiveData<Int>()

    companion object {
        const val PROGRESS_MAX = 10000
        const val FINISH_VALUE = "00"
    }

    var timerStart: String? = null
    val minutes = MutableLiveData("03")
    val seconds = MutableLiveData("30")
    val progress = MutableLiveData(0)
    val showConfirmSkipAlertEvent = LiveEvent<Unit>()
    val stopTimerEvent = LiveEvent<Unit>()
    val showNotificationSettingsAlert = LiveEvent<Boolean>()
    val isNotificationSettingsAlertChecked = MutableLiveData(true)
    val stopForegroundTimer = LiveEvent<Unit>()
    val startForegroundTimer = LiveEvent<Unit>()
    var completed = false
        private set

    fun updateTimerText(minutes: String, seconds: String) {
        this.minutes.value = minutes
        this.seconds.value = seconds
    }

    fun setCompleted() {
        updateTimerText(ActivationTimerState.FINISH_VALUE, ActivationTimerState.FINISH_VALUE)
        progress.value = ActivationTimerState.PROGRESS_MAX
        buttonText.value = R.string.scan_your_card
        completed = true
    }

    val buttonText = MutableLiveData(R.string.alert_me_when_ready)
    operator fun invoke(block: NewTestTimerState.() -> Unit) = apply(block)
}
