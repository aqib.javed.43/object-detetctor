package com.vessel.app.newtesttimer.ui

import com.vessel.app.base.BaseFragment
import com.vessel.app.newtesttimer.dropletstab.DropletsTabFragment
import com.vessel.app.newtesttimer.glaretab.GlareTabFragment
import com.vessel.app.newtesttimer.lightingtab.LightingTabFragment
import com.vessel.app.newtesttimer.surveytab.SurveyTabFragment
import javax.inject.Inject

class NewTimerTabsManager @Inject constructor() {

    lateinit var tabs: List<BaseFragment<*>>

    init {
        createTabs()
    }

    private fun createTabs() {
        tabs = listOf(
            SurveyTabFragment(),
            DropletsTabFragment(),
            LightingTabFragment(),
            GlareTabFragment()
        )
    }

    fun getCurrentTabs(): List<BaseFragment<*>> {
        if (::tabs.isInitialized.not() || (::tabs.isInitialized && tabs.isEmpty())) {
            createTabs()
        }
        return tabs
    }

    fun clear() {
        tabs = emptyList()
    }
}
