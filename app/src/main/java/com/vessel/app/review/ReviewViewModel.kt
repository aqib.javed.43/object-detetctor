package com.vessel.app.review

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class ReviewViewModel @ViewModelInject constructor(
    val state: ReviewState,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {
    fun onContinueClicked() {
        state.closeEvent.call()
    }
}
