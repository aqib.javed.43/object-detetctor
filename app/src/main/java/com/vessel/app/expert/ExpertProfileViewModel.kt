package com.vessel.app.expert

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.MainNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Contact
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.Program
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ProgramCardWidgetOnActionHandler
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.util.ResourceRepository
import com.vessel.app.views.RecommendationListWidgetOnActionHandler
import com.vessel.app.wellness.model.Paging
import com.vessel.app.wellness.model.RecommendationItem
import com.vessel.app.wellness.model.Tip
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ExpertProfileViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    val state: ExpertProfileState,
    resourceRepository: ResourceRepository,
    private val contactManager: ContactManager,
    private val planManager: PlanManager,
    private val tipMapper: TipMapper,
    private val reminderManager: ReminderManager,
    private val recommendationManager: RecommendationManager,
    private val programManager: ProgramManager,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceRepository),
    ToolbarHandler,
    RecommendationListWidgetOnActionHandler,
    CollapsedTitleHandler,
    ProgramCardWidgetOnActionHandler {
    val contactExpert = savedStateHandle.get<Contact>("expertContact")!!

    init {
        loadContact()
        observeLoadRecommendation()
    }

    private fun observeLoadRecommendation() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect { reminder ->
                if (reminder) {
                    planManager.getUserPlans(true)
                        .onSuccess { planData ->
                            state.goalWithTips.value =
                                state.goalWithTips.value?.map { goalWithTipsItem ->
                                    val tips = goalWithTipsItem.tips.map { recommendation ->
                                        val tip = recommendation.iRecommendation as Tip
                                        val plans: List<PlanData> = planData.filter {
                                            it.isTip()
                                        }.filter {
                                            it.tip_id == tip.id
                                        }
                                        val isAddedToPlan = plans.isNotEmpty()
                                        RecommendationItem(
                                            plans,
                                            tip.copy(isAddedToPlan = isAddedToPlan)
                                        )
                                    }
                                    goalWithTipsItem.copy(tips = tips)
                                }
                        }
                }
            }
        }
    }

    private fun loadContact() {
        showLoading()
        viewModelScope.launch {
            contactManager.getContactById(contactExpert.id)
                .onSuccess {
                    state.contactExpert.value = it
                    buildGoalsWithTipsList(it)
                    buildProgramsList(it)
                    hideLoading(false)
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    private fun buildProgramsList(contact: Contact) {
        viewModelScope.launch {
            programManager.getEnrolledPrograms()
                .onSuccess { enrolledPrograms ->
                    state.programsItem.value = contact.programs?.map { program ->
                        val enrolledProgram = enrolledPrograms.firstOrNull {
                            it.id == program.id
                        }
                        if (enrolledProgram != null) {
                            program.copy(
                                isEnrolled = true,
                                enrolledDate = enrolledProgram.enrolledDate,
                                contact = contact
                            )
                        } else {
                            program.copy(
                                contact = contact
                            )
                        }
                    }.orEmpty()
                }
                .onServiceError {
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    private fun buildGoalsWithTipsList(contact: Contact) {
        if (contact.tips.isNullOrEmpty()) {
            state.goalWithTips.value = listOf()
        } else {
            viewModelScope.launch {
                planManager.getUserPlans()
                    .onSuccess { planData ->
                        state.goalWithTips.value =
                            contact.tips.filter {
                                it.mainGoalId != null
                            }.groupBy {
                                it.mainGoalId!!
                            }.map { item ->
                                val goalName =
                                    item.value.first().impactsGoals?.firstOrNull {
                                        it.id == item.value.first().mainGoalId
                                    }?.name ?: getResString(
                                        Goal.values()
                                            .first {
                                                it.id == item.key
                                            }.title
                                    )
                                val tips = item.value.map { tip ->
                                    val plans: List<PlanData> = planData.filter {
                                        it.isTip()
                                    }.filter {
                                        it.tip_id == tip.id
                                    }
                                    val isAddedToPlan = plans.isNotEmpty()
                                    RecommendationItem(
                                        plans,
                                        tip.copy(
                                            isAddedToPlan = isAddedToPlan,
                                            currentGoalName = goalName
                                        )
                                    )
                                }
                                GoalWithRecommendations(goalName, tips)
                            }
                    }
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onExpandViewClicked(recommendation: RecommendationItem) {
        // Unused
    }

    override fun onRecommendationItemClicked(recommendation: RecommendationItem) {
        state.navigateTo.value =
            HomeNavGraphDirections.globalActionToTipDetailsFragment(recommendation.iRecommendation as Tip)
    }

    override fun onRecommendationItemLikeClicked(recommendation: RecommendationItem) {
        val clickedTip = recommendation.iRecommendation as Tip
        val recommendationId = clickedTip.id
        val category = LikeCategory.Tip.value
        val updatedLikeFlag = clickedTip.likeStatus != LikeStatus.LIKE
        var likesCount = clickedTip.likesCount ?: 0
        if (updatedLikeFlag) likesCount++ else likesCount--
        state.goalWithTips.value = state.goalWithTips.value?.map { goalWithRecommendation ->
            val tips = goalWithRecommendation.tips.map { item ->
                val tip = (item.iRecommendation as Tip)
                val copiedRecommendation = if (tip.id == recommendationId)
                    tip.copy(
                        likesCount = likesCount,
                        likeStatus = if (updatedLikeFlag) LikeStatus.LIKE else LikeStatus.DISLIKE
                    )
                else {
                    tip.copy()
                }
                item.copy(iRecommendation = copiedRecommendation)
            }
            goalWithRecommendation.copy(tips = tips)
        }
        viewModelScope.launch {
            if (updatedLikeFlag) {
                recommendationManager.sendLikesStatus(
                    recommendationId,
                    category,
                    updatedLikeFlag
                )
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.CONTENT_LIKED)
                        .addProperty(TrackingConstants.KEY_ID, recommendationId.toString())
                        .addProperty(TrackingConstants.TYPE, category)
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
            } else {
                recommendationManager.unLikesStatus(recommendationId, category)
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.CONTENT_UNLIKED)
                        .addProperty(TrackingConstants.KEY_ID, recommendationId.toString())
                        .addProperty(TrackingConstants.TYPE, category)
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
            }
        }
    }

    override fun onRecommendationAddToPlanClicked(recommendation: RecommendationItem) {
        val tipItem = (recommendation.iRecommendation as Tip)
        if (tipItem.isAddedToPlan.not()) {
            val plans = if (recommendation.planData.isNullOrEmpty().not()) {
                recommendation.planData!!
            } else {
                listOf(
                    PlanData(
                        tip_id = tipItem.id,
                        tip = tipMapper.mapTip(tipItem),
                        multiple = 1
                    )
                )
            }
            val planReminderHeader = PlanReminderHeader(
                tipItem.getDisplayedTitle(),
                tipItem.getDisplayedFrequency(),
                tipItem.getBackgroundImageUrl(),
                null,
                plans,
                tipItem.isAddedToPlan.not()
            )
            state.navigateTo.value =
                MainNavGraphDirections.globalActionToPlanReminderDialog(planReminderHeader)
        } else {
            recommendation.planData?.let { plans ->
                deletePlanItem(recommendation, plans)
            }
        }
    }

    private fun deletePlanItem(recommendation: RecommendationItem, plans: List<PlanData>) {
        showLoading()
        viewModelScope.launch {
            planManager.deletePlanItem(plans.map { it.id ?: -1 })
                .onSuccess {
                    hideLoading(false)
                    planManager.updatePlanCachedData()
                    reminderManager.updateRemindersCachedData()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(
                                TrackingConstants.KEY_ID,
                                plans.map { it.id ?: -1 }.toString()
                            )
                            .addProperty(
                                TrackingConstants.TYPE,
                                recommendation.iRecommendation.getType()?.name.toString()
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Expert Profile")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    override fun onLoadMoreClicked(paging: Paging) {}
    override fun onCollapsedTitleClicked(goal: GoalWithRecommendations) {
        state.goalWithTips.value = state.goalWithTips.value?.map { goalWithRecommendation ->
            if (goal.goalName == goalWithRecommendation.goalName)
                goalWithRecommendation.copy(isCollapsed = goalWithRecommendation.isCollapsed.not())
            else
                goalWithRecommendation.copy()
        }
    }

    override fun onProgramItemClicked(program: Program) {
        state.navigateTo.value =
            ExpertProfileFragmentDirections.globalActionToProgramDetailsFragment(program)
    }

    override fun onProgramInfoClicked(program: Program) {
        state.navigateTo.value =
            ExpertProfileFragmentDirections.globalActionToProgramInfoDialogFragment(program)
    }

    override fun onJoinProgramClicked(program: Program) {
        if (program.isEnrolled.not()) {
            state.navigateTo.value =
                ExpertProfileFragmentDirections.globalActionToJoinProgramFragment(
                    program,
                    Constants.EXPERT_PAGE
                )
        } else {
            onProgramItemClicked(program)
        }
    }
}
