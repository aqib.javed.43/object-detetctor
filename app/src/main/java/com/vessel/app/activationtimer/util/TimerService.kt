package com.vessel.app.activationtimer.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.os.bundleOf
import androidx.navigation.NavDeepLinkBuilder
import com.vessel.app.R
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.taketest.TakeTestActivity
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class TimerService : Service() {

    companion object {
        private val ACTION_UPDATE_TIME = "${TimerService::class.java.canonicalName}.updateTime"
        private val ACTION_UPDATE_COMPLETE = "${TimerService::class.java.canonicalName}.updateComplete"
        private val ACTION_CANCEL_TIMER = "${TimerService::class.java.canonicalName}.cancelTimer"
        private val EXTRA_TIME = "${TimerService::class.java.canonicalName}.time"
        private const val REQUEST_CODE_UPDATE_TIME = 0
        private const val REQUEST_CODE_UPDATE_COMPLETE = 1
        private const val REQUEST_CODE_CANCEL_TIMER = 2
        private const val ONGOING_NOTIFICATION_ID = 1
        private const val INTERVAL_MS = 100L
        private val TIMER_TEXT_INTERVAL = TimeUnit.SECONDS.toMillis(1)
        private val DURATION = TimeUnit.MINUTES.toMillis(3) + TimeUnit.SECONDS.toMillis(30)
        private const val START_TIME_TEXT = "03:30"
        private const val FORMAT = "%02d"
    }

    @Inject
    internal lateinit var notificationManager: NotificationManager

    @Inject
    internal lateinit var notificationManagerCompat: NotificationManagerCompat

    private val binder = TimerBinder()
    private var handler: TimerHandler? = null
    private var foreground = false
    private var textUpdateMillis = 0L
    private lateinit var countDownTimer: CountDownTimer
    var timerCountDownState = TimerStates.IDEAL

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (foreground) buildNotification(intent)
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onCreate() {
        super.onCreate()
        setupTimer()
    }

    private fun setupTimer() {
        countDownTimer = object : CountDownTimer(DURATION, INTERVAL_MS) {
            override fun onTick(millisUntilFinished: Long) {
                timerCountDownState = TimerStates.RUNNING
                textUpdateMillis += DURATION - millisUntilFinished
                TimeUnit.MILLISECONDS.apply {
                    val minutes = String.format(FORMAT, toMinutes(millisUntilFinished + TIMER_TEXT_INTERVAL))
                    val seconds = String.format(FORMAT, toSeconds(millisUntilFinished + TIMER_TEXT_INTERVAL) % 60)

                    handler?.onUpdateProgress((DURATION - millisUntilFinished).toDouble() / DURATION)

                    if (TIMER_TEXT_INTERVAL - textUpdateMillis <= 0) {
                        textUpdateMillis = 0L
                        handler?.onUpdateText(minutes, seconds)
                        if (foreground) {
                            val updateTimerIntent = Intent(this@TimerService, TimerService::class.java)
                                .setAction(ACTION_UPDATE_TIME)
                                .putExtra(EXTRA_TIME, "$minutes:$seconds")
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                                PendingIntent.getService(
                                    applicationContext,
                                    REQUEST_CODE_UPDATE_TIME,
                                    updateTimerIntent,
                                    PendingIntent.FLAG_MUTABLE
                                ).send()
                            } else {
                                PendingIntent.getService(
                                    applicationContext,
                                    REQUEST_CODE_UPDATE_TIME,
                                    updateTimerIntent,
                                    PendingIntent.FLAG_CANCEL_CURRENT
                                ).send()
                            }
                        }
                    }
                }
            }

            override fun onFinish() {
                MediaPlayer.create(applicationContext, R.raw.alarm)?.apply {
                    start()
                }
                if (foreground) {
                    val updateNotificationIntent =
                        Intent(this@TimerService, TimerService::class.java)
                            .setAction(ACTION_UPDATE_COMPLETE)
                    PendingIntent.getService(
                        applicationContext,
                        REQUEST_CODE_UPDATE_COMPLETE,
                        updateNotificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    ).send()
                }
                timerCountDownState = TimerStates.COMPLETED
                handler?.onFinish()
            }
        }
    }

    fun startTimerCountDown() {
        countDownTimer.start()
    }

    fun startForeground() {
        if (timerCountDownState == TimerStates.RUNNING) {
            buildNotification()
            foreground = true
        }
    }

    fun stopForeground() {
        if (timerCountDownState == TimerStates.RUNNING || timerCountDownState == TimerStates.COMPLETED) {
            stopForeground(true)
            foreground = false
        }
    }

    fun setTimerHandler(timerHandler: TimerHandler) {
        handler = timerHandler
        if (timerCountDownState == TimerStates.COMPLETED) {
            handler?.onFinish()
        }
    }

    fun removeTimerHandler() {
        handler = null
    }

    fun stopTimerCountDown() {
        countDownTimer.cancel()
        timerCountDownState = TimerStates.IDEAL
    }

    fun forceCompleteTimerCountDown() {
        countDownTimer.cancel()
        stopForeground()
        timerCountDownState = TimerStates.COMPLETED
        handler?.onFinish()
    }

    private fun buildNotification(intent: Intent? = null) {
        var time = START_TIME_TEXT
        var setCompleteText = false
        when (intent?.action) {
            ACTION_UPDATE_TIME -> intent.getStringExtra(EXTRA_TIME)?.let { time = it }
            ACTION_UPDATE_COMPLETE -> setCompleteText = true
            ACTION_CANCEL_TIMER -> {
                stopForeground()
                return
            }
        }

        val pendingContentIntent = NavDeepLinkBuilder(this)
            .setComponentName(TakeTestActivity::class.java)
            .setGraph(R.navigation.take_test_nav_graph)
            .setDestination(R.id.activationTimerFragment)
            .setArguments(
                bundleOf(
                    Pair(
                        TrackingConstants.KEY_LOCATION,
                        TrackingConstants.FROM_NOTIFICATION
                    )
                )
            )
            .createPendingIntent()

        val cancelIntent = Intent(this, TimerService::class.java)
            .setAction(ACTION_CANCEL_TIMER)
        val pendingCancelIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.getService(
                this,
                REQUEST_CODE_CANCEL_TIMER,
                cancelIntent,
                PendingIntent.FLAG_MUTABLE
            )
        } else {
            PendingIntent.getService(
                this, REQUEST_CODE_CANCEL_TIMER, cancelIntent, PendingIntent.FLAG_CANCEL_CURRENT
            )
        }

        val channelIdValue = getString(R.string.timer_notification_channel_id)
        val channelId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                channelIdValue,
                getString(R.string.timer_notification_channel_name),
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(notificationChannel)
            channelIdValue
        } else ""

        val notification = NotificationCompat.Builder(this, channelId)
            .setContentTitle(
                if (setCompleteText) {
                    getString(R.string.activation_timer_complete_title)
                } else {
                    getString(R.string.wait_for_test)
                }
            )
            .setContentText(
                if (setCompleteText) {
                    getString(R.string.tap_here_to_continue)
                } else {
                    getString(R.string.scan_your_card_in, time)
                }
            )
            .setSmallIcon(R.mipmap.ic_notification)
            .setPriority(NotificationManagerCompat.IMPORTANCE_HIGH)
            .setOnlyAlertOnce(true)
            .setContentIntent(pendingContentIntent)
            .addAction(
                android.R.color.transparent,
                getString(R.string.btn_cancel),
                pendingCancelIntent
            )
            .build()

        if (intent != null && intent.hasExtra(EXTRA_TIME)) {
            notificationManagerCompat.notify(ONGOING_NOTIFICATION_ID, notification)
        } else {
            startForeground(ONGOING_NOTIFICATION_ID, notification)
        }
    }

    inner class TimerBinder : Binder() {
        fun getService() = this@TimerService
    }

    interface TimerHandler {
        fun onUpdateText(minutes: String, seconds: String)

        fun onUpdateProgress(percentComplete: Double)

        fun onFinish()
    }

    enum class TimerStates {
        IDEAL, RUNNING, COMPLETED
    }
}
