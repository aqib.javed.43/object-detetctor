package com.vessel.app.wellness.impactscorepopup

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImpactScorePopUpDialogFragment : BaseDialogFragment<ImpactScorePopUpDialogViewModel>() {
    override val viewModel: ImpactScorePopUpDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_impact_popup_dialog
    private val tagsAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_tag
    }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
            setGravity(Gravity.BOTTOM)
        }
        isCancelable = true

        observe(viewModel.dismissDialog) { dismiss() }
    }
}
