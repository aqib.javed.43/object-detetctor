package com.vessel.app.education

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.education.ui.EducationFragmentDirections
import com.vessel.app.education.ui.EducationPage
import com.vessel.app.util.ResourceRepository

class EducationViewModel @ViewModelInject constructor(
    val state: EducationState,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {
    init {
        state.pages.addAll(EducationPage.values())
    }

    fun onBackClicked() {
        state {
            if (position.value == 0) {
                navigateBack()
            } else {
                goToPreviousPage.call()
            }
        }
    }

    fun onContinueClicked(view: View) {
        state.apply {
            if (isOnLastPage()) {
                view.findNavController()
                    .navigate(EducationFragmentDirections.actionEducationToActivationTimer())
            } else {
                state.goToNextPage.call()
            }
        }
    }

    fun onPageChanged(position: Int) {
        state.position.value = position
    }

    private fun isOnLastPage() = state.pages.isEmpty() || state.position.value == state.pages.size - 1
}
