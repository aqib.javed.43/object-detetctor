package com.vessel.app.ready

import android.text.SpannableString
import android.view.View
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.ready.ui.Tip
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.makeClickableSpan
import javax.inject.Inject

class ReadyState @Inject constructor(private val resourceProvider: ResourceRepository) {
    val closeEvent = LiveEvent<Unit>()

    fun getTips(onSpanClicked: (() -> Unit)): List<Tip> {
        return listOf(
            Tip(
                tip = resourceProvider.getString(R.string.tip1).makeClickableSpan(
                    Pair(
                        resourceProvider.getString(R.string.tip1_click_area),
                        View.OnClickListener {
                            onSpanClicked.invoke()
                        }
                    )
                ),
                drawableRes = R.drawable.tip_1_icon,
            ),
            Tip(
                tip = SpannableString(resourceProvider.getString(R.string.tip2)),
                drawableRes = R.drawable.tip_2_icon
            ),
            Tip(
                tip = SpannableString(resourceProvider.getString(R.string.tip3)),
                drawableRes = R.drawable.tip_3_icon
            )
        )
    }
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: ReadyState.() -> Unit) = apply(block)
}
