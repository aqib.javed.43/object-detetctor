// TODO fix the unit test

// package com.vessel.app.supplement
//
// import android.net.Uri
// import androidx.lifecycle.Observer
// import com.vessel.app.*
// import com.vessel.app.BaseFaker.Fake.FAKER
// import com.vessel.app.common.manager.RecommendationManager
// import com.vessel.app.common.manager.TodosManager
// import com.vessel.app.common.model.*
// import com.vessel.app.recommendation.SampleRecommendationsFaker
// import com.vessel.app.supplement.ui.SupplementFragmentDirections
// import com.vessel.app.todo.TodoFaker
// import com.vessel.app.util.ResourceRepository
// import io.mockk.*
// import org.assertj.core.api.Assertions.assertThat
// import org.junit.jupiter.api.*
//
// class SupplementViewModelTest : BaseTest() {
//    private lateinit var supplementViewModel: SupplementViewModel
//
//    private lateinit var state: SupplementState
//
//    private val resourceProvider = mockk<ResourceRepository>(relaxed = true)
//    private val recommendationManager = mockk<RecommendationManager>()
//    private val todosManager = mockk<TodosManager>()
//    private val eventObserver = mockk<Observer<Unit>>(relaxed = true)
//
//    private val sampleRecommendations = SampleRecommendationsFaker.basic()
//    private val todos = listOf(
//        TodoFaker.basic(sampleRecommendations.supplements[0].reagent!!.apiName),
//        TodoFaker.basic(Reagent.Ketones.apiName)
//    )
//    private val unitFullName = FAKER.funnyName().name()
//
//    @BeforeEach
//    fun setUp() {
//        coEvery { recommendationManager.getLatestSampleRecommendations() }
//            .answers { Result.Success(sampleRecommendations) }
//        coEvery { todosManager.getRecurringTodos() }
//            .answers { Result.Success(todos) }
//        sampleRecommendations.supplements.forEach {
//            every { resourceProvider.getString(it.reagent!!.unit) } returns unitFullName
//        }
//
//        state = SupplementState()
//
//        supplementViewModel = SupplementViewModel(
//            state,
//            resourceProvider,
//            recommendationManager,
//            todosManager
//        )
//    }
//
//    @Test
//    fun `it should show the recommendations`() {
//        state.showSupplementRecommendations.value.isTrue()
//        state.supplementNutrients.value!!.forEachIndexed { index, supplementFormula ->
//            val supplement = sampleRecommendations.supplements[index]
//            supplementFormula.nutrient isEqualTo supplement.reagent!!.displayName
//            supplementFormula.amount isEqualTo "${supplement.amount} $unitFullName"
//            assertThat(supplementFormula.isInPlan).isEqualTo(supplement.reagent == Reagent.Magnesium)
//        }
//    }
//
//    @Test
//    fun `it should hide the recommendations view if the manager call fails`() {
//        coEvery { recommendationManager.getLatestSampleRecommendations() }
//            .answers { Result.UnknownError(Throwable()) }
//
//        supplementViewModel = SupplementViewModel(
//            state,
//            resourceProvider,
//            recommendationManager,
//            todosManager
//        )
//
//        state.showSupplementRecommendations.value.isFalse()
//    }
//
//    @Test
//    fun `it should delete the todo on the server if the supplement clicked is already in the plan and then update the cell`() =
//        runBlockingTest {
//            val nutrient1 = SupplementFormulaFaker.basic(Reagent.Ketones)
//            val nutrient2 =
//                SupplementFormulaFaker.basic(sampleRecommendations.supplements[0].reagent!!, true)
//            state.supplementNutrients.value = listOf(nutrient1, nutrient2)
//            coEvery { todosManager.deleteRecurringTodo(todos[0].id) }
//                .answers { Result.Success(Unit) }
//
//            supplementViewModel.onAddToPlanClick(nutrient2)
//
//            state.supplementNutrients.value!![0] isEqualTo nutrient1
//            state.supplementNutrients.value!![1].isInPlan.isFalse()
//        }
//
//    @Test
//    fun `it should do nothing when it fails to delete the todo on the server with generic error`() =
//        runBlockingTest {
//            val nutrient1 = SupplementFormulaFaker.basic(Reagent.Ketones)
//            val nutrient2 =
//                SupplementFormulaFaker.basic(sampleRecommendations.supplements[0].reagent!!, true)
//            state.supplementNutrients.value = listOf(nutrient1, nutrient2)
//            val error = ErrorMessage(FAKER.chuckNorris().fact())
//            coEvery { todosManager.deleteRecurringTodo(todos[0].id) }
//                .answers { Result.GenericError(FAKER.number().randomDigit(), error) }
//
//            supplementViewModel.onAddToPlanClick(nutrient2)
//
//            state.supplementNutrients.value!![0] isEqualTo nutrient1
//            state.supplementNutrients.value!![1].isInPlan.isTrue()
//            supplementViewModel.showAlert.value!!.body isEqualTo error.message
//        }
//
//    @Test
//    fun `it should do nothing when it fails to delete the todo on the server with network error`() =
//        runBlockingTest {
//            val nutrient1 = SupplementFormulaFaker.basic(Reagent.Ketones)
//            val nutrient2 =
//                SupplementFormulaFaker.basic(sampleRecommendations.supplements[0].reagent!!, true)
//            state.supplementNutrients.value = listOf(nutrient1, nutrient2)
//            coEvery { todosManager.deleteRecurringTodo(todos[0].id) }
//                .answers { Result.NetworkError(Throwable()) }
//            val errorMessage = FAKER.chuckNorris().fact()
//            every { resourceProvider.getString(R.string.network_error) } returns errorMessage
//
//            supplementViewModel.onAddToPlanClick(nutrient2)
//
//            state.supplementNutrients.value!![0] isEqualTo nutrient1
//            state.supplementNutrients.value!![1].isInPlan.isTrue()
//            supplementViewModel.showAlert.value!!.body isEqualTo errorMessage
//        }
//
//    @Test
//    fun `it should do nothing when it fails to delete the todo on the server with unknown error`() =
//        runBlockingTest {
//            val nutrient1 = SupplementFormulaFaker.basic(Reagent.Ketones)
//            val nutrient2 =
//                SupplementFormulaFaker.basic(sampleRecommendations.supplements[0].reagent!!, true)
//            state.supplementNutrients.value = listOf(nutrient1, nutrient2)
//            coEvery { todosManager.deleteRecurringTodo(todos[0].id) }
//                .answers { Result.UnknownError(Throwable()) }
//            val errorMessage = FAKER.chuckNorris().fact()
//            every { resourceProvider.getString(R.string.unknown_error) } returns errorMessage
//
//            supplementViewModel.onAddToPlanClick(nutrient2)
//
//            state.supplementNutrients.value!![0] isEqualTo nutrient1
//            state.supplementNutrients.value!![1].isInPlan.isTrue()
//            supplementViewModel.showAlert.value!!.body isEqualTo errorMessage
//        }
//
//    @Test
//    fun `it should add the todo on the server if the supplement clicked is already in the plan and then update the cell`() =
//        runBlockingTest {
//            val nutrient1 = SupplementFormulaFaker.basic(Reagent.Ketones)
//            val nutrient2 =
//                SupplementFormulaFaker.basic(sampleRecommendations.supplements[0].reagent!!, false)
//            state.supplementNutrients.value = listOf(nutrient1, nutrient2)
//            val todo = TodoFaker.basic()
//            coEvery { todosManager.addRecurringTodo(any(), any(), any(), any()) }
//                .answers { Result.Success(todo) }
//
//            supplementViewModel.onAddToPlanClick(nutrient2)
//
//            state.supplementNutrients.value!![0] isEqualTo nutrient1
//            state.supplementNutrients.value!![1].isInPlan.isTrue()
//        }
//
//    @Test
//    fun `it should do nothing when it fails to add the todo on the server with generic error`() =
//        runBlockingTest {
//            val nutrient1 = SupplementFormulaFaker.basic(Reagent.Ketones)
//            val nutrient2 =
//                SupplementFormulaFaker.basic(sampleRecommendations.supplements[0].reagent!!, false)
//            state.supplementNutrients.value = listOf(nutrient1, nutrient2)
//            val error = ErrorMessage(FAKER.chuckNorris().fact())
//            coEvery {
//                todosManager.addRecurringTodo(any(), any(), any(), any())
//            }.answers { Result.GenericError(FAKER.number().randomDigit(), error) }
//
//            supplementViewModel.onAddToPlanClick(nutrient2)
//
//            state.supplementNutrients.value!![0] isEqualTo nutrient1
//            state.supplementNutrients.value!![1].isInPlan.isFalse()
//            supplementViewModel.showAlert.value!!.body isEqualTo error.message
//        }
//
//    @Test
//    fun `it should do nothing when it fails to add the todo on the server with network error`() =
//        runBlockingTest {
//            val nutrient1 = SupplementFormulaFaker.basic(Reagent.Ketones)
//            val nutrient2 =
//                SupplementFormulaFaker.basic(sampleRecommendations.supplements[0].reagent!!, false)
//            state.supplementNutrients.value = listOf(nutrient1, nutrient2)
//            coEvery { todosManager.addRecurringTodo(any(), any(), any(), any()) }
//                .answers { Result.NetworkError(Throwable()) }
//            val errorMessage = FAKER.chuckNorris().fact()
//            every { resourceProvider.getString(R.string.network_error) } returns errorMessage
//
//            supplementViewModel.onAddToPlanClick(nutrient2)
//
//            state.supplementNutrients.value!![0] isEqualTo nutrient1
//            state.supplementNutrients.value!![1].isInPlan.isFalse()
//            supplementViewModel.showAlert.value!!.body isEqualTo errorMessage
//        }
//
//    @Test
//    fun `it should do nothing when it fails to add the todo on the server with unknown error`() =
//        runBlockingTest {
//            val nutrient1 = SupplementFormulaFaker.basic(Reagent.Ketones)
//            val nutrient2 =
//                SupplementFormulaFaker.basic(sampleRecommendations.supplements[0].reagent!!, false)
//            state.supplementNutrients.value = listOf(nutrient1, nutrient2)
//            coEvery { todosManager.addRecurringTodo(any(), any(), any(), any()) }
//                .answers { Result.UnknownError(Throwable()) }
//            val errorMessage = FAKER.chuckNorris().fact()
//            every { resourceProvider.getString(R.string.unknown_error) } returns errorMessage
//
//            supplementViewModel.onAddToPlanClick(nutrient2)
//
//            state.supplementNutrients.value!![0] isEqualTo nutrient1
//            state.supplementNutrients.value!![1].isInPlan.isFalse()
//            supplementViewModel.showAlert.value!!.body isEqualTo errorMessage
//        }
//
//    @Disabled("this doesn't work because Uri.Builder can't be mocked. If we wanted to we could use a sort of provider class that would take in the authority, path and params and return a Uri or String. But I don't want to bloat the code to get one test to pass.")
//    @Test
//    fun `it should open the vessel fuel website and pass the supplements in plan`() {
//        val (view, navController) = MockProvider.mockedNavigation()
//        val unitShortName = FAKER.lorem().word()
//        every { resourceProvider.getString(any()) } returns unitShortName
//        mockkStatic(Uri::class)
//        val uriBuilder = mockk<Uri.Builder>()
//        every { Uri.Builder() } returns uriBuilder
//        every { uriBuilder.scheme("https") } returns uriBuilder
//        every { uriBuilder.authority("store.vesselhealth.com") } returns uriBuilder
//        every { uriBuilder.path("pages/supplement") } returns uriBuilder
//        every { uriBuilder.appendQueryParameter(any(), any()) } returns uriBuilder
//        val url = FAKER.internet().url()
//        every { uriBuilder.build() } returns Uri.parse(url)
//
//        supplementViewModel.onVesselFuelClicked(view)
//
//        verify { navController.navigate(SupplementFragmentDirections.actionSupplementToWeb(url)) }
//    }
//
//    @Test
//    fun onVitaminAngelsLearnMoreClicked() {
//        // todo
//    }
//
//    @Test
//    fun `it should navigate back when the back button is clicked`() {
//        supplementViewModel.navigateBack.observeForever(eventObserver)
//
//        supplementViewModel.onBackButtonClicked(mockk())
//
//        verify { eventObserver.onChanged(null) }
//    }
// }
