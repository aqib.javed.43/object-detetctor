package com.vessel.app.wellness.model

import com.vessel.app.common.net.data.plan.PlanData

data class RecommendationItem(
    val planData: List<PlanData>?,
    val iRecommendation: IRecommendation
)
