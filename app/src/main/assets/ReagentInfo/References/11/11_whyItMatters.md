Biotin, also called vitamin B7, is an essential vitamin that helps your body turn the carbohydrates, fats, and proteins in the food you eat into the energy you need. It is a cofactor in metabolic pathways of certain amino acids, and also helps to regulate gene expression. In addition to coming from food or supplements, biotin is also made by microbes in our gut. While Biotin exists in free forms and protein-bound forms in foods, only the free form is functional.

In addition to not getting enough in your food, Biotin deficiency can be caused by intestinal surgery, protracted antibiotic use, excessive alcohol intake, and long-term use of certain anticonvulsant medications.

Biotin deficiency can lead to fatigue, depression, nausea and vomiting, thinning hair, hair loss, scaly red rashes around the eyes, nose, and mouth, conjunctivitis, brittle nails, and numbness and tingling in the arms and legs. Symptoms typically appear gradually over time.

Regular consumption of raw egg whites can inactivate biotin causing similar signs and symptoms of biotin deficiency. The protein avidin, found in raw egg whites, but denatured upon cooking, binds to biotin tightly preventing it from being used as an essential cofactor.

When people are deficient in biotin, urinary excretion decreases rapidly, suggesting that urinary excretion is an early and sensitive indicator of biotin deficiency. Similarly when people have ingested more biotin than they need, urinary biotin levels increase.

**references:**

-   https://www.ncbi.nlm.nih.gov/books/NBK114310/

-   https://www.ncbi.nlm.nih.gov/pubmed/23302490

-   https://www.nature.com/articles/1600389

-   https://books.google.com/books/about/The_Concise_Encyclopedia_of_Foods_and_Nu.html?id=-Mlq4-JYXlIC

-   Mock DM. Biotin. In: Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds. Modern Nutrition in Health and Disease

-   Zempleni Present Knowledge in Nutrition 2012

-   Pacheco-Alvarez Biotin in metabolism and its relationship to human disease. Arch Med Res 2002

-   Book: An Evidence-based Approach to Vitamins and Minerals: Health Implications and Intake Recommendations

-   https://www.ncbi.nlm.nih.gov/pubmed/20620758

-   https://www.ncbi.nlm.nih.gov/pubmed/23159185

-   https://www.ncbi.nlm.nih.gov/pubmed/19727438

-   Book: Nutrition in Clinical Practice by David Katz, 2nd edition.
