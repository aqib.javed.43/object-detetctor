package com.vessel.app.photocapture

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.*
import android.hardware.SensorManager
import android.hardware.camera2.*
import android.hardware.camera2.CameraCaptureSession.CaptureCallback
import android.hardware.camera2.params.SessionConfiguration
import android.hardware.camera2.params.StreamConfigurationMap
import android.media.*
import android.os.*
import android.util.*
import android.view.*
import android.view.TextureView.SurfaceTextureListener
import android.widget.Toast
import androidx.activity.addCallback
import androidx.camera.core.impl.CameraCaptureCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.mlkit.vision.barcode.*
import com.google.zxing.Result
import com.google.zxing.ResultPoint
import com.tapadoo.alerter.Alerter
import com.vessel.app.Constants
import com.vessel.app.Constants.CAPTURE_DATE_FORMAT
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.widget.AutoFitTextureView
import com.vessel.app.photocapture.PhotoCaptureFragment.ImageSaver.ImageSaverBuilder
import com.vessel.app.photocapture.processing.GetContentWithMimeTypes
import com.vessel.app.taketest.TakeTestViewModel
import com.vessel.app.taketest.model.UploadTestFileModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import org.opencv.android.*
import org.opencv.core.Mat
import org.opencv.core.Rect
import java.io.*
import java.lang.Thread.sleep
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.abs
import kotlin.math.max

@AndroidEntryPoint
class PhotoCaptureFragment : BaseFragment<PhotoCaptureViewModel>() {
    override val viewModel: PhotoCaptureViewModel by viewModels()
    override val layoutResId = R.layout.fragment_photo_capture

    // 4 places to change
    private var qrCodeWellnessCardUuid: String? = null
    private var captureButton: View? = null
    private var shouldProcessNextQualifiedImage = true
    private var isTakePhotoManually = false
    private var isFirstCapture = false
    private lateinit var imageProcessor: UpdatedImageProcessor

    private val imageProcessorThread = HandlerThread("imageProcessorThread").apply {
        start()
    }
    private val imageProcessorHandler = Handler(imageProcessorThread.looper)

    private val imageReaderThread = HandlerThread("imageReaderThread").apply {
        start()
    }
    private val imageReaderHandler = Handler(imageReaderThread.looper)

    private val selectPicture = registerForActivityResult(GetContentWithMimeTypes()) { uri ->
        uri?.let {
            val takeTestViewModel = parentViewModel<TakeTestViewModel>()
            viewModel.copyImageFromUri(
                uri,
                CAPTURE_DATE_FORMAT.format(Date()),
                takeTestViewModel.firstFailedTestUUID,
                takeTestViewModel.testStartDate
            )
        }
    }
    private val imageProcessorListener = object : UpdatedImageProcessor.Listener {
        override fun onBlockScanErrorDim() {
            Log.d("IMAGE_PROCESS_LISTENER", "onBlockScanErrorDim: ")
            handleTakePhotoManuallyButtonState()
            if (!isFirstCapture || viewModel.isCameraMovedAfterScan) {
                viewModel.onLightingDetected()
            }
        }

        override fun onBlockScanErrorBright() {
            Log.d("IMAGE_PROCESS_LISTENER", "onBlockScanErrorBright: ")
            handleTakePhotoManuallyButtonState()
            if (!isFirstCapture || viewModel.isCameraMovedAfterScan) {
                viewModel.onGlareDetected()
            }
        }

        override fun onBlockScanErrorShadow() {
            Log.d("IMAGE_PROCESS_LISTENER", "onBlockScanErrorShadow: ")
            handleTakePhotoManuallyButtonState()
            if (!isFirstCapture || viewModel.isCameraMovedAfterScan) {
                viewModel.onShadowDetected()
            }
        }

        override fun onFiducialsDetected(items: List<UpdatedImageProcessor.MatchTemplateLocation>) {
            Log.d("IMAGE_PROCESS_LISTENER", "onFiducialsDetected: ")
            handleTakePhotoManuallyButtonState()
            if (!isFirstCapture || viewModel.isCameraMovedAfterScan) {
                viewModel.onFiducialsDetected(items)
            }
        }

        override fun onBlockScanErrorTooFar() {
            Log.d("IMAGE_PROCESS_LISTENER", "onBlockScanErrorTooFar: ")
            handleTakePhotoManuallyButtonState()
            if (!isFirstCapture || viewModel.isCameraMovedAfterScan) {
                viewModel.onCardIssueDetected(R.string.move_closer, false)
            }
        }

        override fun onMoveCameraErrorAfterScan() {
            Log.d("IMAGE_PROCESS_LISTENER", "onMoveCameraErrorAfterScan: ")
            handleTakePhotoManuallyButtonState()
            viewModel.onMoveCameraErrorAfterScan()
            requireActivity().runOnUiThread {
                findNavController().navigate(
                    PhotoCaptureFragmentDirections.actionCapturePhotoToSelf(
                        false, true, viewModel.startTestTimeInMilli
                    )
                )
            }
        }

        override fun onBlockScanErrorTooClose() {
            Log.d("IMAGE_PROCESS_LISTENER", "onBlockScanErrorTooClose: ")
            handleTakePhotoManuallyButtonState()
            if (!isFirstCapture || viewModel.isCameraMovedAfterScan) {
                viewModel.onCardIssueDetected(R.string.move_further_away, false)
            }
        }

        override fun onQrDataDetected(qrCodeMetadataResult: Result?) {
            Log.d("IMAGE_PROCESS_LISTENER", "onQrDataDetected: ")
            qrCodeMetadataResult?.let {
                qrCodeWellnessCardUuid = qrCodeMetadataResult.text
            }
            handleTakePhotoManuallyButtonState()
        }

        override fun onAllQualifyingImagesCreated(
            bitmap: Bitmap,
            qrCodeMetadataResult: Result?,
            foundTemplateMatches: MutableList<Rect>,
            resizeScale: Double,
            isFinalProcessing: Boolean,
            resultPoints: List<ResultPoint>
        ) {
            Log.d("IMAGE_PROCESS_LISTENER", "onAllQualifyingImagesCreated: ")
            qrCodeMetadataResult?.let {
                qrCodeWellnessCardUuid = qrCodeMetadataResult.text
            }
            Log.d("IMAGE_PROCESS_LISTENER", "handleTakePhotoManuallyButtonState: ENTRY")
            handleTakePhotoManuallyButtonState()
            Log.d("IMAGE_PROCESS_LISTENER", "handleTakePhotoManuallyButtonState: EXIT")
            Log.d("IMAGE_PROCESS_LISTENER", "onCardDetected: ENTRY")
            viewModel.onCardDetected(
                bitmap, qrCodeMetadataResult, foundTemplateMatches, resizeScale, resultPoints
            )
            Log.d("IMAGE_PROCESS_LISTENER", "onCardDetected: EXIT")
            if (isFinalProcessing) {
                processFinalImage(rawResultQueue)
            } else if (viewModel.isCameraVisionTester().not()) {
                lifecycleScope.launch {
                    handleImageRecognized()
                    delay(CAMERA_CARD_STEADY_WAITING_TIME)
                }
            }
        }
    }

    /**
     * An [OrientationEventListener] used to determine when device rotation has occurred.
     * This is mainly necessary for when the device is rotated by 180 degrees, in which case
     * onCreate or onConfigurationChanged is not called as the view dimensions remain the same,
     * but the orientation of the has changed, and thus the preview rotation must be updated.
     */
    private var orientationListener: OrientationEventListener? = null

    /**
     * [TextureView.SurfaceTextureListener] handles several lifecycle events of a [TextureView].
     */
    private val surfaceTextureListener: SurfaceTextureListener = object : SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(texture: SurfaceTexture, width: Int, height: Int) {
            configureTransform(width, height)
        }

        override fun onSurfaceTextureSizeChanged(texture: SurfaceTexture, width: Int, height: Int) {
            configureTransform(width, height)
        }

        override fun onSurfaceTextureDestroyed(texture: SurfaceTexture): Boolean {
            synchronized(cameraStateLock) {
                previewSize = null
            }
            return true
        }

        override fun onSurfaceTextureUpdated(texture: SurfaceTexture) {}
    }

    private var textureView: AutoFitTextureView? = null

    /**
     * An additional thread for running tasks that shouldn't block the UI.  This is used for all
     * callbacks from the [CameraDevice] and [CameraCaptureSession]s.
     */
    private var backgroundThread: HandlerThread? = null

    /**
     * A counter for tracking corresponding [CaptureRequest]s and [CaptureResult]s
     * across the [CameraCaptureSession] capture callbacks.
     */
    private val requestCounter = AtomicInteger()

    /**
     * A [Semaphore] to prevent the app from exiting before closing the camera.
     */
    private val cameraOpenCloseLock = Semaphore(1)

    /** A lock protecting camera state.
     *
     * State protected by cameraStateLock.
     *
     * The following state is used across both the UI and background threads.  Methods with "Locked"
     * in the name expect cameraStateLock to be held while calling.
     */
    private val cameraStateLock = Any()

    /**
     * ID of the current [CameraDevice].
     */
    private var cameraId: String? = null

    /**
     * A [CameraCaptureSession] for camera preview.
     */
    private var captureSession: CameraCaptureSession? = null

    /**
     * A reference to the open [CameraDevice].
     */
    private var cameraDevice: CameraDevice? = null

    /**
     * The [Size] of camera preview.
     */
    private var previewSize: Size? = null

    /**
     * The [CameraCharacteristics] for the currently configured camera device.
     */
    private var characteristics: CameraCharacteristics? = null

    /**
     * A [Handler] for running tasks in the background.
     */
    private var backgroundHandler: Handler? = null

    /**
     * A reference counted holder wrapping the [ImageReader] that handles RAW image captures.
     * This is used to allow us to clean up the [ImageReader] when all background tasks using
     * its [Image]s have completed.
     */
    private var rawImageReader: RefCountedAutoCloseable<ImageReader>? = null
    private var jpgImageReader: RefCountedAutoCloseable<ImageReader>? = null

    private var qrCodeImageReader: ImageReader? = null

    private var openCVImageReader: ImageReader? = null

    /**
     * Whether or not the currently configured camera device is fixed-focus.
     */
    private var noAFRun = false

    /**
     * Request ID to [ImageSaver.ImageSaverBuilder] mapping for in-progress RAW captures.
     */
    private val rawResultQueue = TreeMap<Int, ImageSaverBuilder>()

    /**
     * [CaptureRequest.Builder] for the camera preview
     */
    private var previewRequestBuilder: CaptureRequest.Builder? = null

    /**
     * The state of the camera device.
     *
     * @see [preCaptureCallback]
     */
    private var state = STATE_CLOSED

    /**
     * Timer to use with pre-capture sequence to ensure a timely capture if 3A convergence is
     * taking too long.
     */
    private var captureTimer: Long = 0

    /**
     * [CameraDevice.StateCallback] is called when the currently active [CameraDevice]
     * changes its state.
     */
    private val stateCallback: CameraDevice.StateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(cameraDevice: CameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here if
            // the TextureView displaying this has been set up.
            synchronized(cameraStateLock) {
                state = STATE_OPENED
                viewModel.logCameraOpenEvent()
                cameraOpenCloseLock.release()
                this@PhotoCaptureFragment.cameraDevice = cameraDevice

                // Start the preview session if the TextureView has been set up already.
                if (previewSize != null && textureView?.isAvailable == true) {
                    createCameraPreviewSessionLocked()
                }
            }
        }

        override fun onDisconnected(cameraDevice: CameraDevice) {
            synchronized(cameraStateLock) {
                state = STATE_CLOSED
                this@PhotoCaptureFragment.cameraDevice = cameraDevice
                this@PhotoCaptureFragment.cameraDevice?.close()
                cameraOpenCloseLock.release()
                this@PhotoCaptureFragment.cameraDevice = null
                viewModel.onCameraDisconnected()
            }
        }

        override fun onError(cameraDevice: CameraDevice, error: Int) {
            synchronized(cameraStateLock) {
                state = STATE_CLOSED
                cameraOpenCloseLock.release()
                cameraDevice.close()
                this@PhotoCaptureFragment.cameraDevice = null
            }
            val activity: Activity? = activity
            activity?.finish()
        }
    }

    /**
     * This a callback object for the [ImageReader]. "onImageAvailable" will be called when a
     * RAW image is ready to be saved.
     */
    private fun getImageAvailableListener(
        reader: RefCountedAutoCloseable<ImageReader>, isRaw: Boolean
    ): ImageReader.OnImageAvailableListener {
        return ImageReader.OnImageAvailableListener {
            dequeueAndSaveImage(
                rawResultQueue, reader, isRaw
            )
        }
    }

    /**
     * A [CameraCaptureSession.CaptureCallback] that handles events for the preview and
     * pre-capture sequence.
     */
    private val preCaptureCallback: CaptureCallback = object : CaptureCallback() {
        private fun process(result: CaptureResult) {
            Log.d("IMAGE_PROCESS_LISTENER", "process:")
            synchronized(cameraStateLock) {
                Log.d("IMAGE_PROCESS_LISTENER", "process: state : $state")
                if (state == STATE_WAITING_FOR_3A_CONVERGENCE) {
                    var readyToCapture = true
                    if (!noAFRun) {
                        val afState = result.get(CaptureResult.CONTROL_AF_STATE) ?: return

                        // If auto-focus has reached locked state, we are ready to capture
                        readyToCapture =
                            afState == CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED || afState == CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED
                    }

                    // If we are running on an non-legacy device, we should also wait until
                    // auto-exposure and auto-white-balance have converged as well before
                    // taking a picture.
                    if (!isLegacyLocked()) {
                        val aeState = result.get(CaptureResult.CONTROL_AE_STATE)
                        val awbState = result.get(CaptureResult.CONTROL_AWB_STATE)
                        if (aeState == null || awbState == null) {
                            return
                        }
                        readyToCapture =
                            readyToCapture && aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED && awbState == CaptureResult.CONTROL_AWB_STATE_CONVERGED
                    }

                    // If we haven't finished the pre-capture sequence but have hit our maximum
                    // wait timeout, too bad! Begin capture anyway.
                    if (!readyToCapture && hitTimeoutLocked()) {
                        readyToCapture = true
                    }
                    if (readyToCapture && viewModel.isCameraVisionTester().not()) {
                        captureStillPictureLocked()
                        // After this, the camera will go back to the normal state of preview.
                        state = STATE_PREVIEW
                        addBackActionWarningInterpreter()
                    }
                }
            }
        }

        override fun onCaptureProgressed(
            session: CameraCaptureSession, request: CaptureRequest, partialResult: CaptureResult
        ) {
            Log.d("IMAGE_PROCESS_LISTENER", "onCaptureProgressed: ")
            process(partialResult)
        }

        override fun onCaptureCompleted(
            session: CameraCaptureSession, request: CaptureRequest, result: TotalCaptureResult
        ) {
            Log.d("IMAGE_PROCESS_LISTENER", "onCaptureCompleted: ")
            process(result)
        }
    }

    /**
     * A [CameraCaptureSession.CaptureCallback] that handles the still RAW capture request.
     */
    private val captureCallback: CaptureCallback = object : CaptureCallback() {
        override fun onCaptureStarted(
            session: CameraCaptureSession,
            request: CaptureRequest,
            timestamp: Long,
            frameNumber: Long
        ) {
            val imageSuffix = "IMG_${fileSdf.format(Date())}"
            val rawFile = File(getOutputDirectory(), imageSuffix)
            val jpgFile = File(getOutputDirectory(), "${imageSuffix}_JPG")

            // Look up the ImageSaverBuilder for this request and update it with the file name
            // based on the capture start time.
            var rawBuilder: ImageSaverBuilder?
            val requestId = request.tag as Int
            synchronized(cameraStateLock) {
                rawBuilder = rawResultQueue[requestId]
            }
            if (rawBuilder != null) {
                rawBuilder!!.setFile(rawFile)
                rawBuilder!!.setJpegFile(jpgFile)
            }
        }

        override fun onCaptureCompleted(
            session: CameraCaptureSession, request: CaptureRequest, result: TotalCaptureResult
        ) {
            val requestId = request.tag as Int

            // Look up the ImageSaverBuilder for this request and update it with the CaptureResult
            synchronized(cameraStateLock) {
                val rawBuilder = rawResultQueue[requestId]?.setResult(result)

                // If we have all the results necessary, save the image to a file in the background.
                handleCompletionLocked(requestId, rawBuilder, rawResultQueue, isRaw = false)
                finishedCaptureLocked()
            }
        }

        override fun onCaptureFailed(
            session: CameraCaptureSession, request: CaptureRequest, failure: CaptureFailure
        ) {
            val requestId = request.tag as Int
            synchronized(cameraStateLock) {
                rawResultQueue.remove(requestId)
                finishedCaptureLocked()
            }
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()

        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        captureButton = view?.findViewById(R.id.takePhotoButton)
        textureView = view?.findViewById(R.id.autoFitTextureView)

        if (allPermissionsGranted(Constants.SCAN_CARD_PERMISSIONS)) {
            Log.d("setup_capture", "allPermissionsGranted")
            setupCapture()
        } else {
            Log.d("setup_capture", " Else allPermissionsGranted")
            getRuntimePermissions(
                Constants.SCAN_CARD_PERMISSIONS, Constants.SCAN_CARD_PERMISSIONS_REQUEST_CODE
            )
        }

        view?.findViewById<View>(R.id.openFileManager)?.setOnClickListener {
            selectPicture.launch(ACCEPTED_MIMETYPES)
        }

        val takeTestViewModel = parentViewModel<TakeTestViewModel>()
        viewModel.state.uuid = takeTestViewModel.uuid
        addBackActionInterpreter()
        if (viewModel.isCameraMovedAfterScan) {
            isFirstCapture = true
        }
    }

    private fun setupObservers() {
        observe(viewModel.state.onBackAction) {
            onBackPressedRunnable?.run()
        }

        observe(viewModel.state.retakePhoto) {
            Alerter.create(requireActivity()).setText(getString(it))
                .setBackgroundColorRes(R.color.colorAccent).enableSound(true)
                .enableInfiniteDuration(true)
                .addButton(text = getString(R.string.retake), onClick = {
                    Alerter.hide()
                    preOpenCamera()
                }).show()
        }

        observe(viewModel.state.uploadImageFile) {
            val takeTestViewModel = parentViewModel<TakeTestViewModel>()
            takeTestViewModel.uploadTestFileModel = it
            navigateToPreviewFragment()
        }

        observe(viewModel.state.takePhotoManuallyButtonDelayTimerStates) {
            handleTakePhotoManuallyButtonState()
        }
    }

    private fun handleTakePhotoManuallyButtonState() {

        viewModel.state.showTakePhotoButton.postValue(
            viewModel.state.takePhotoManuallyButtonDelayTimerStates.value == TakePhotoManuallyButtonDelayTimerStates.ENDED
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (!allPermissionsGranted(Constants.SCAN_CARD_PERMISSIONS)) {
            Toast.makeText(requireContext(), R.string.permissions_required, Toast.LENGTH_LONG)
                .show()
            viewModel.onPermissionsDenied()
        } else {
            setupCapture()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.recordStartCaptureTimer()
        if (allPermissionsGranted(Constants.SCAN_CARD_PERMISSIONS)) {
            preOpenCamera()
        }
    }

    private var onBackPressedRunnable: (Runnable)? = null

    private fun addBackActionInterpreter() {
        if (viewModel.fromActivationTimerFragment()) {
            onBackPressedRunnable = Runnable {
                viewModel.logImageCaptureAbandonedAndNavigateBack()
            }
            requireActivity().onBackPressedDispatcher.addCallback(this) {
                onBackPressedRunnable?.run()
            }
        } else {
            addBackActionWarningInterpreter()
        }
    }

    private fun addBackActionWarningInterpreter() {
        if (isAdded) {
            MainScope().launch {
                addBackActionCallback()
            }
        }
    }

    private fun addBackActionCallback() {
        onBackPressedRunnable = Runnable {
            showBackWarning(R.string.are_you_sure_to_back) {
                viewModel.logImageCaptureAbandoned()
            }
        }

        activity?.onBackPressedDispatcher?.addCallback(this) {
            onBackPressedRunnable?.run()
        }
    }

    private fun preOpenCamera() {
        // Setup a new OrientationEventListener.  This is used to handle rotation events like a
        // 180 degree rotation that do not normally trigger a call to onCreate to do view re-layout
        // or otherwise cause the preview TextureView's size to change.
        orientationListener = object : OrientationEventListener(
            requireActivity(), SensorManager.SENSOR_DELAY_NORMAL
        ) {
            override fun onOrientationChanged(orientation: Int) {
                if (textureView != null && textureView!!.isAvailable) {
                    configureTransform(textureView!!.width, textureView!!.height)
                }
            }
        }

        startBackgroundThread()
        openCamera()

        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we should
        // configure the preview bounds here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        textureView?.let {
            if (it.isAvailable) {
                configureTransform(it.width, it.height)
            } else {
                it.surfaceTextureListener = surfaceTextureListener
            }
        }

        if (orientationListener != null && orientationListener!!.canDetectOrientation()) {
            orientationListener!!.enable()
        }
    }

    override fun onPause() {
        Alerter.hide()
        orientationListener?.disable()
        closeCamera()
        stopBackgroundThread()
        super.onPause()
    }

    /**
     * Sets up state related to camera that is needed before opening a [CameraDevice].
     */
    private fun setUpCameraOutputs(): Boolean {
        Log.d("IMAGE_PROCESS_LISTENER", "Setting up camera Outputs")
        val manager = requireActivity().getSystemService(Context.CAMERA_SERVICE) as CameraManager
        // Find a CameraDevice that supports RAW captures, and configure state.
        for (cameraId in manager.cameraIdList) {
            Log.d("IMAGE_PROCESS_LISTENER", "Setting up camera $cameraId")
            val characteristics = manager.getCameraCharacteristics(cameraId)

            // We only use a camera that supports RAW in this sample.
            if (characteristics.get(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES)
                    ?.contains(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_RAW) == false
            ) {
                continue
            }
            Log.d("IMAGE_PROCESS_LISTENER", "Raw Capability found in camera $cameraId")

            val map = characteristics.get(
                CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP
            )

            // For still image captures, we use the largest available size.
            val largestRaw = Collections.max(
                listOf(*map!!.getOutputSizes(ImageFormat.RAW_SENSOR)), CompareSizesByArea()
            )

            synchronized(cameraStateLock) {
                // Set up ImageReader for RAW output. Place this in a reference
                // counted wrapper to ensure it's only closed when all background tasks
                // using it are finished.
                if (rawImageReader == null || rawImageReader?.andRetain == null) {
                    Log.d("IMAGE_PROCESS_LISTENER", "Setting up raw image reader")
                    rawImageReader = RefCountedAutoCloseable(
                        ImageReader.newInstance(
                            largestRaw.width, largestRaw.height, ImageFormat.RAW_SENSOR, 5
                        )
                    )
                }
                if (jpgImageReader == null || jpgImageReader?.andRetain == null) {
                    Log.d("IMAGE_PROCESS_LISTENER", "Setting up jpg image reader")
                    jpgImageReader = RefCountedAutoCloseable(
                        ImageReader.newInstance(
                            largestRaw.width, largestRaw.height, ImageFormat.JPEG, 5
                        )
                    )
                }
                Log.d("IMAGE_PROCESS_LISTENER", "Set Raw Listener")
                rawImageReader?.get()?.setOnImageAvailableListener(
                    getImageAvailableListener(rawImageReader!!, true), backgroundHandler
                )
                Log.d("IMAGE_PROCESS_LISTENER", "Set jpeg Listener")
                jpgImageReader?.get()?.setOnImageAvailableListener(
                    getImageAvailableListener(jpgImageReader!!, false), backgroundHandler
                )
                this.characteristics = characteristics
                this.cameraId = cameraId
            }
            return true
        }

        // We found no suitable cameras for capturing RAW, warn the user.
        showCameraNotSupportedAlert()

        return false
    }

    private fun showCameraNotSupportedAlert() {
        Alerter.create(requireActivity()).setTitle(R.string.whoops)
            .setText(R.string.camera_not_supported).setBackgroundColorRes(R.color.colorAccent)
            .enableSound(true).setDismissable(false).enableInfiniteDuration(true)
            .addButton(text = getString(R.string.ok), onClick = {
                Alerter.hide()
                viewModel.onCameraNotSupportedButtonClicked()
            }).show()
    }

    private fun createQrCodeImageReader(): ImageReader {
        val size = characteristics!!.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!
            .getOutputSizes(ImageFormat.JPEG).filter {
                it.width < 2000 && it.height < 2000
            }.maxByOrNull {
                it.height * it.width
            }!!

        return ImageReader.newInstance(
            size.width, size.height, ImageFormat.JPEG, IMAGE_BUFFER_SIZE
        )
    }

    private fun createOpenCVImageReader(): ImageReader {
        val size = characteristics!!.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!
            .getOutputSizes(ImageFormat.JPEG).filter {
                it.width < 2000 && it.height < 2000
            }.maxByOrNull {
                it.height * it.width
            }!!

        return ImageReader.newInstance(
            size.width, size.height, ImageFormat.JPEG, 5
        ).apply {
            setOnImageAvailableListener(
                {
                    it.acquireLatestImage()?.let { image ->
                        if (shouldProcessNextQualifiedImage) {
                            processImage(image)
                        }
                        image.close()
                    }
                }, imageReaderHandler
            )
        }
    }

    private var imageCounter: Int = 0

    private fun processImage(image: Image) {
        imageCounter = (imageCounter + 1) % 100
        if (imageCounter % 5 == 0) {
            return
        }

        val bitmap = makeBitmapFromImage(image)
        bitmap?.let {
            imageProcessorHandler.removeCallbacksAndMessages(null)
            imageProcessorHandler.post {
                val mat = Mat()
                Utils.bitmapToMat(it, mat)
                imageProcessor.processImage(mat, false)
            }
        }
    }

    private fun makeBitmapFromImage(image: Image): Bitmap? {
        if (image.planes.count() != 1) {
            return null
        }

        val buffer = image.planes[0].buffer
        val imageBytes = ByteArray(buffer.remaining())
        buffer.get(imageBytes)
        val originalBitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        return Bitmap.createBitmap(
            originalBitmap, 0, 0, originalBitmap.width, originalBitmap.height, Matrix().apply {
                postRotate(90f)
            }, true
        )
    }

    private fun setupCapture() {
        Log.d("setup_capture", "Setting up capture")
        Log.d("setup_capture", "Setting setOnClickListener")
        captureButton?.setOnClickListener {
            if (qrCodeWellnessCardUuid.isNullOrBlank()) {
                val errorMessage = getString(R.string.could_not_detect_card)
                viewModel.logImageFailure(errorMessage)
                Toast.makeText(
                    requireContext(), errorMessage, Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            viewModel.validCardDetected(R.string.hold_your_phone_steady)
            viewModel.autoScan = false
            isTakePhotoManually = true
            handleImageRecognized()
        }
        Log.d("setup_capture", "Setting loaderCallback")
        val loaderCallback = object : BaseLoaderCallback(requireContext()) {
            override fun onManagerConnected(status: Int) {
                when (status) {
                    LoaderCallbackInterface.SUCCESS -> {
                    }
                    else -> super.onManagerConnected(status)
                }
            }
        }

        Log.d("setup_capture", "Initializing OpenCV")
        if (!OpenCVLoader.initDebug()) {
            Log.d("setup_capture", "Initializing OpenCV not Successful. Retrying with initAsync")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, requireContext(), loaderCallback)
        } else {
            Log.d("setup_capture", "Initializing OpenCV was Successful")
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
        Log.d("setup_capture", "Instantiating image processor")
        imageProcessor = UpdatedImageProcessor(
            listener = imageProcessorListener,
            detectionBundle = viewModel.tfliteObjectDetectionManager.bundle
        )
    }

    /**
     * Opens the camera specified by [cameraId].
     */
    private fun openCamera() {
        Log.d("IMAGE_PROCESS_LISTENER", "openCamera: ")
        if (!setUpCameraOutputs()) {
            Log.d("IMAGE_PROCESS_LISTENER", "camera output failed")
            return
        }
        Log.d("IMAGE_PROCESS_LISTENER", "camera output setup successfully")
        if (!allPermissionsGranted(Constants.SCAN_CARD_PERMISSIONS)) {
            getRuntimePermissions(
                Constants.SCAN_CARD_PERMISSIONS, Constants.SCAN_CARD_PERMISSIONS_REQUEST_CODE
            )
            return
        }
        // Ktlint showing error about the granting the permission for user that's why we adding this check
        if (ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                requireContext(), if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    Manifest.permission.READ_MEDIA_IMAGES
                    Manifest.permission.READ_MEDIA_VIDEO
                } else {
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                }
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            setupCapture()
            val manager =
                requireActivity().getSystemService(Context.CAMERA_SERVICE) as CameraManager
            try {
                // Wait for any previously running session to finish.
                if (!cameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                    throw java.lang.RuntimeException("Time out waiting to lock camera opening.")
                }
                var cameraId: String?
                var backgroundHandler: Handler?
                synchronized(cameraStateLock) {
                    cameraId = this.cameraId
                    backgroundHandler = this.backgroundHandler
                }

                // Attempt to open the camera. mStateCallback will be called on the background handler's
                // thread when this succeeds or fails.
                manager.openCamera(cameraId!!, stateCallback, backgroundHandler)
            } catch (e: CameraAccessException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                throw java.lang.RuntimeException(
                    "Interrupted while trying to lock camera opening.", e
                )
            }
        }
    }

    /**
     * Closes the current [CameraDevice].
     */
    private fun closeCamera() {
        try {
            cameraOpenCloseLock.acquire()
            synchronized(cameraStateLock) {
                // Reset state and clean up resources used by the camera.
                // Note: After calling this, the ImageReaders will be closed after any background
                // tasks saving Images from these readers have been completed.
                state = STATE_CLOSED

                captureSession?.close()
                captureSession = null

                cameraDevice?.close()
                cameraDevice = null

                rawImageReader?.close()
                rawImageReader = null

                jpgImageReader?.close()
                jpgImageReader = null

                qrCodeImageReader?.close()
                qrCodeImageReader = null

                openCVImageReader?.close()
                openCVImageReader = null
            }
        } catch (e: InterruptedException) {
            throw java.lang.RuntimeException("Interrupted while trying to lock camera closing.", e)
        } finally {
            cameraOpenCloseLock.release()
        }
    }

    /**
     * Starts a background thread and its [Handler].
     */
    private fun startBackgroundThread() {
        synchronized(cameraStateLock) {
            backgroundThread = HandlerThread("CameraBackground")
            backgroundThread!!.start()
            backgroundHandler = Handler(backgroundThread!!.looper)
        }
    }

    /**
     * Stops the background thread and its [Handler].
     */
    private fun stopBackgroundThread() {
        backgroundThread?.quitSafely()
        try {
            backgroundThread?.join()
            backgroundThread = null
            synchronized(cameraStateLock) {
                backgroundHandler = null
            }
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    private val onQrCodeImageListener = ImageReader.OnImageAvailableListener { imageReader ->
        imageReader.acquireLatestImage()?.let { image ->
            extractQrCode()
            image.close()
        }
    }

    private fun safeQrCodeImageReader() = qrCodeImageReader ?: createQrCodeImageReader().apply {
        qrCodeImageReader = this
    }

    /**
     * Creates a new [CameraCaptureSession] for camera preview.
     *
     * Call this only with [.cameraStateLock] held.
     */
    private fun createCameraPreviewSessionLocked() {
        try {
            val surface = previewSurface()

            safeQrCodeImageReader().setOnImageAvailableListener(
                onQrCodeImageListener, backgroundHandler
            )

            // We set up a CaptureRequest.Builder with the output Surface.
            previewRequestBuilder = cameraDevice!!.createCaptureRequest(
                CameraDevice.TEMPLATE_PREVIEW
            ).apply {
                addTarget(surface)
                addTarget(safeQrCodeImageReader().surface)
            }

            createCaptureSession(listOf(surface, safeQrCodeImageReader().surface))
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    private fun createCaptureSession(outputs: List<Surface>) {
        // Here, we create a CameraCaptureSession for camera preview.
        cameraDevice!!.createCaptureSession(
            outputs, object : CameraCaptureSession.StateCallback() {
                override fun onConfigured(cameraCaptureSession: CameraCaptureSession) {
                    Log.d(
                        "set_opencv_capture_session",
                        "executing onConfigured for camera capture session"
                    )
                    synchronized(cameraStateLock) {
                        // The camera is already closed
                        if (cameraDevice == null) {
                            return
                        }
                        state = try {
                            // Finally, we start displaying the camera preview.
                            cameraCaptureSession.setRepeatingRequest(
                                previewRequestBuilder!!.build(), null, null
                            )
                        } catch (e: Exception) {
                            e.printStackTrace()
                            return
                        }
                        // When the session is ready, we start displaying the preview.
                        Log.d("set_opencv_capture_session", "Update capture session in callback")
                        captureSession = cameraCaptureSession
                    }
                }

                override fun onConfigureFailed(cameraCaptureSession: CameraCaptureSession) {
                    // Failed to configure camera.
                }
            }, backgroundHandler
        )
    }

    private fun previewSurface(): Surface {
        Log.d("IMAGE_PROCESS_LISTENER", "previewSurface: navigateUp")
        textureView ?: findNavController().navigateUp()
        val texture = textureView!!.surfaceTexture
        // We configure the size of default buffer to be the size of camera preview we want.
        texture!!.setDefaultBufferSize(previewSize!!.width, previewSize!!.height)

        // This is the output Surface we need to start preview.
        return Surface(texture)
    }

    /**
     * Configure the given [CaptureRequest.Builder] to use auto-focus, auto-exposure, and
     * auto-white-balance controls if available.
     *
     *
     * Call this only with [.cameraStateLock] held.
     *
     * @param builder the builder to configure.
     */
    private fun setup3AControlsLocked(builder: CaptureRequest.Builder) {
        // Enable auto-magical 3A run by camera device
        builder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO)
        val minFocusDist =
            characteristics?.get(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE)

        // If MINIMUM_FOCUS_DISTANCE is 0, lens is fixed-focus and we need to skip the AF run.
        noAFRun = minFocusDist == null || minFocusDist == 0f
        if (!noAFRun) {
            // If there is a "continuous picture" mode available, use it, otherwise default to AUTO.
            if (characteristics?.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES)
                    ?.contains(CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE) == true
            ) {
                builder.set(
                    CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE
                )
            } else {
                builder.set(
                    CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO
                )
            }
        }

        // If there is an auto-magical flash control mode available, use it, otherwise default to
        // the "on" mode, which is guaranteed to always be available.
        if (characteristics?.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES)
                ?.contains(CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH) == true
        ) {
            builder.set(
                CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH
            )
        } else {
            builder.set(
                CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON
            )
        }

        // If there is an auto-magical white balance control mode available, use it.
        if (characteristics?.get(CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES)
                ?.contains(CaptureRequest.CONTROL_AWB_MODE_AUTO) == true
        ) {
            // Allow AWB to run auto-magically if this device supports this
            builder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_AUTO)
        }
    }

    /**
     * Configure the necessary [android.graphics.Matrix] transformation to `mTextureView`,
     * and start/restart the preview capture session if necessary.
     *
     *
     * This method should be called after the camera state has been initialized in
     * setUpCameraOutputs.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private fun configureTransform(viewWidth: Int, viewHeight: Int) {
        val activity: Activity? = activity
        synchronized(cameraStateLock) {
            if (textureView == null || activity == null || characteristics == null || characteristics?.get(
                    CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP
                ) == null
            ) {
                return
            }
            val map: StreamConfigurationMap = characteristics!!.get(
                CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP
            )!!

            // Find the rotation of the device relative to the native device orientation.
            val deviceRotation = activity.windowManager.defaultDisplay.rotation
            val displaySize = Point()
            activity.windowManager.defaultDisplay.getSize(displaySize)

            // Find the rotation of the device relative to the camera sensor's orientation.
            val totalRotation = sensorToDeviceRotation(characteristics, deviceRotation)

            // Swap the view dimensions for calculation as needed if they are rotated relative to
            // the sensor.
            val swappedDimensions = totalRotation == 90 || totalRotation == 270
            var rotatedViewWidth = viewWidth
            var rotatedViewHeight = viewHeight
            if (swappedDimensions) {
                rotatedViewWidth = viewHeight
                rotatedViewHeight = viewWidth
            }

            // Find the best preview size for these view dimensions
            val previewSize = chooseOptimalSize(
                map.getOutputSizes(SurfaceTexture::class.java), rotatedViewWidth, rotatedViewHeight
            )
            if (swappedDimensions) {
                textureView!!.setAspectRatio(previewSize.height, previewSize.width)
            } else {
                textureView!!.setAspectRatio(previewSize.width, previewSize.height)
            }

            // Find rotation of device in degrees (reverse device orientation for front-facing
            // cameras).
            val rotation =
                if (characteristics?.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT) {
                    (360 + orientations[deviceRotation]) % 360
                } else {
                    (360 - orientations[deviceRotation]) % 360
                }
            val matrix = Matrix()
            val viewRect = RectF(0f, 0f, viewWidth.toFloat(), viewHeight.toFloat())
            val bufferRect =
                RectF(0f, 0f, previewSize.height.toFloat(), previewSize.width.toFloat())
            val centerX = viewRect.centerX()
            val centerY = viewRect.centerY()

            // Initially, output stream images from the Camera2 API will be rotated to the native
            // device orientation from the sensor's orientation, and the TextureView will default to
            // scaling these buffers to fill it's view bounds.  If the aspect ratios and relative
            // orientations are correct, this is fine.
            //
            // However, if the device orientation has been rotated relative to its native
            // orientation so that the TextureView's dimensions are swapped relative to the
            // native device orientation, we must do the following to ensure the output stream
            // images are not incorrectly scaled by the TextureView:
            //   - Undo the scale-to-fill from the output buffer's dimensions (i.e. its dimensions
            //     in the native device orientation) to the TextureView's dimension.
            //   - Apply a scale-to-fill from the output buffer's rotated dimensions
            //     (i.e. its dimensions in the current device orientation) to the TextureView's
            //     dimensions.
            //   - Apply the rotation from the native device orientation to the current device
            //     rotation.
            if (Surface.ROTATION_90 == deviceRotation || Surface.ROTATION_270 == deviceRotation) {
                bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY())
                matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL)
                val scale = max(
                    viewHeight.toFloat() / previewSize.height,
                    viewWidth.toFloat() / previewSize.width
                )
                matrix.postScale(scale, scale, centerX, centerY)
            }
            matrix.postRotate(rotation.toFloat(), centerX, centerY)
            textureView!!.setTransform(matrix)

            // Start or restart the active capture session if the preview was initialized or
            // if its aspect ratio changed significantly.
            if (this.previewSize == null || !checkAspectsEqual(previewSize, this.previewSize!!)) {
                this.previewSize = previewSize

                if (state != STATE_CLOSED) {
                    createCameraPreviewSessionLocked()
                }
            }
        }
    }

    /**
     * Initiate a still image capture.
     *
     *
     * This function sends a capture request that initiates a pre-capture sequence in our state
     * machine that waits for auto-focus to finish, ending in a "locked" state where the lens is no
     * longer moving, waits for auto-exposure to choose a good exposure value, and waits for
     * auto-white-balance to converge.
     */
    private fun takePicture() {
        Log.d("IMAGE_PROCESS_LISTENER", "take picture")
        synchronized(cameraStateLock) {
            // If we already triggered a pre-capture sequence, or are in a state where we cannot
            // do this, return immediately.
            Log.d("IMAGE_PROCESS_LISTENER", "state : $state")
            if (state != STATE_PREVIEW) {
                return
            }
            try {
                // If this is not a legacy device, we can also trigger an auto-exposure metering
                // run.
                if (!isLegacyLocked()) {
                    Log.d("IMAGE_PROCESS_LISTENER", "isLegacyLocked: ")
                    // Tell the camera to lock focus.
                    previewRequestBuilder?.set(
                        CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                        CameraMetadata.CONTROL_AE_PRECAPTURE_TRIGGER_START
                    )
                }

                // Update state machine to wait for auto-focus, auto-exposure, and
                // auto-white-balance (aka. "3A") to converge.
                state = STATE_WAITING_FOR_3A_CONVERGENCE

                // Start a timer for the pre-capture sequence.
                startTimerLocked()

                // Replace the existing repeating request with one with updated 3A triggers.
                captureSession?.capture(
                    previewRequestBuilder!!.build(), preCaptureCallback, backgroundHandler
                )
            } catch (e: CameraAccessException) {
                Log.d("IMAGE_PROCESS_LISTENER", "take picture catch : ${e.localizedMessage}")
                e.printStackTrace()
            }
        }
    }

    /**
     * Send a capture request to the camera device that initiates a capture targeting the RAW output
     * Call this only with [cameraStateLock] held.
     */
    private fun captureStillPictureLocked() {
        try {
            val activity: Activity = activity ?: return
            val wellnessCardUuid = qrCodeWellnessCardUuid ?: return
            val cameraDevice = cameraDevice ?: return

            // This is the CaptureRequest.Builder that we use to take a picture.
            val captureBuilder =
                cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE)
            captureBuilder.addTarget(rawImageReader!!.get()!!.surface)
            captureBuilder.addTarget(jpgImageReader!!.get()!!.surface)

            // Use the same AE and AF modes as the preview.
            setup3AControlsLocked(captureBuilder)
            captureBuilder.set(
                CaptureRequest.CONTROL_EFFECT_MODE, CameraMetadata.CONTROL_EFFECT_MODE_OFF
            )

            // Set request tag to easily track results in callbacks.
            captureBuilder.setTag(requestCounter.getAndIncrement())
            val request = captureBuilder.build()

            // Create an ImageSaverBuilder in which to collect results, and add it to the queue
            // of active requests.
            val takeTestViewModel = parentViewModel<TakeTestViewModel>()
            val rawBuilder = ImageSaverBuilder(activity).setCharacteristics(characteristics)
                .setFileSavedCallback { scannedRawFile, scannedJpgFile, captureResult ->
                    viewModel.logSampleImageCaptured(wellnessCardUuid)
                    if ((scannedJpgFile == null || (scannedJpgFile.exists())) && scannedRawFile != null && scannedRawFile.exists()) {
                        val rotatedImage = viewModel.rotatedImage(scannedRawFile)
                        takeTestViewModel.uploadTestFileModel = UploadTestFileModel(
                            uuid = viewModel.state.uuid!!,
                            file = scannedRawFile,
                            jpgFile = scannedJpgFile,
                            firstFailedTestUUID = takeTestViewModel.firstFailedTestUUID,
                            autoScan = viewModel.autoScan,
                            captureResult = captureResult,
                            timerStartDate = takeTestViewModel.testStartDate,
                            captureDate = CAPTURE_DATE_FORMAT.format(Date()),
                            wellnessCardUuid = wellnessCardUuid,
                            rotatedImage
                        )
                        navigateToPreviewFragment()
                    }
                }
            rawResultQueue[request.tag as Int] = rawBuilder
            captureSession?.capture(request, captureCallback, backgroundHandler)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun navigateToPreviewFragment() {
        Log.d("IMAGE_PROCESS_LISTENER", "navigateToPreviewFragment: ")
        lifecycleScope.launch(Dispatchers.Main) {
            if (isAdded) {
                findNavController().navigate(
                    PhotoCaptureFragmentDirections.actionCapturePhotoToPreview(viewModel.startTestTimeInMilli)
                )
            }
        }
    }

    private fun finishedCaptureLocked() {
        captureSession?.stopRepeating()
    }

    /**
     * Retrieve the next [Image] from a reference counted [ImageReader], retaining
     * that [ImageReader] until that [Image] is no longer in use, and set this
     * [Image] as the result for the next request in the queue of pending requests.  If
     * all necessary information is available, begin saving the image to a file in a background
     * thread.
     *
     * @param pendingQueue the currently active requests.
     * @param reader       a reference counted wrapper containing an [ImageReader] from which
     * to acquire an image.
     */
    private fun dequeueAndSaveImage(
        pendingQueue: TreeMap<Int, ImageSaverBuilder>,
        reader: RefCountedAutoCloseable<ImageReader>?,
        isRaw: Boolean
    ) {
        Log.d("dequeue_and_save", "Starting dequeue and Save Image")
        synchronized(cameraStateLock) {
            val entry = pendingQueue.firstEntry()!!
            val builder = entry.value

            // Increment reference count to prevent ImageReader from being closed while we
            // are saving its Images in a background thread (otherwise their resources may
            // be freed while we are writing to a file).
            if (reader?.andRetain == null) {
                pendingQueue.remove(entry.key)
                return
            }
            Log.d("dequeue_and_save", "Get image through Acquire Next Image: isRaw = $isRaw")
            val image: Image = try {
                reader.get()!!.acquireNextImage()
            } catch (e: java.lang.IllegalStateException) {
                Log.d("dequeue_and_save", "Get image failed, trying acquire Latest image")
                jpgImageReader?.get()?.acquireLatestImage()
                pendingQueue.remove(entry.key)
                return
            }
            if (!isRaw && !isTakePhotoManually) {
                val bitmap = makeBitmapFromImage(image) ?: return
                imageProcessorHandler.removeCallbacksAndMessages(null)
                viewModel.playScanAnimation()
                imageProcessorHandler.post {
                    val mat = Mat()
                    Utils.bitmapToMat(bitmap, mat)
                    Log.d("dequeue_and_save", "Final process image")
                    imageProcessor.processImage(mat, true)
                }
            } else if (isTakePhotoManually) {
                viewModel.playScanAnimation()
            }

            builder?.setRefCountedReader(reader)?.setImage(image, isRaw)
                ?.setExifRotation(getExifRotation())
            handleCompletionLocked(entry.key, builder, pendingQueue, isRaw)
        }
    }

    private fun processFinalImage(pendingQueue: TreeMap<Int, ImageSaverBuilder>) {
        Log.d("IMAGE_PROCESS_LISTENER", "processFinalImage: ")
        val entry = pendingQueue.firstEntry()
        entry?.let {
            val builder = it.value
            builder?.setFinalImageProcessed()?.setFileDescription(viewModel.getFileDescription())
            handleCompletionLocked(it.key, builder, pendingQueue, isRaw = false)
        }
    }

    private fun extractQrCode() {
        if (qrCodeWellnessCardUuid.isNullOrEmpty()) {
            previewRequestBuilder?.removeTarget(safeQrCodeImageReader().surface)

            var waitForCaptureSessionInMs = 0
            // wait up to 3 seconds for capture session to become reprocessable
            // Fix crash: Session has been closed; further changes are illegal
            while (waitForCaptureSessionInMs < CAMERA_SESSION_RE_PROCCESSABLE_WAITING_TIME) {
                if (captureSession?.isReprocessable == false) {
                    Log.d("extract_qr_code", "Setting OpenCV capture session")
                    setOpenCvCaptureSession()
                    break
                }

                sleep(CAMERA_SESSION_RE_PROCCESSABLE_SLEEP_DURATION_TIME.toLong())
                waitForCaptureSessionInMs += CAMERA_SESSION_RE_PROCCESSABLE_SLEEP_DURATION_TIME
            }
        }
    }

    private fun safeOpenCVImageReader() = openCVImageReader ?: createOpenCVImageReader().apply {
        openCVImageReader = this
    }

    private fun setOpenCvCaptureSession() {
        Log.d("set_opencv_capture_session", "Setting OpenCV Capture Session")
        previewRequestBuilder?.addTarget(safeOpenCVImageReader().surface)

        val surface = previewSurface()

        createCaptureSession(listOf(surface, safeOpenCVImageReader().surface))
    }

    private fun setRawCaptureSessionAndTakePicture() {
        Log.d("IMAGE_PROCESS_LISTENER", "setRawCaptureSessionAndTakePicture: ")
        val surface = previewSurface()

        // Here, we create a CameraCaptureSession for camera preview.

        cameraDevice!!.createCaptureSession(
            listOf(surface, rawImageReader?.get()?.surface, jpgImageReader?.get()?.surface),
            object : CameraCaptureSession.StateCallback() {
                override fun onConfigured(cameraCaptureSession: CameraCaptureSession) {
                    Log.d("IMAGE_PROCESS_LISTENER", "onConfigured: ")
                    synchronized(cameraStateLock) {
                        // The camera is already closed
                        Log.d("IMAGE_PROCESS_LISTENER", "camera already closed : ${cameraDevice==null}")
                        if (cameraDevice == null) {
                            return
                        }
                        state = try {
                            Log.d("IMAGE_PROCESS_LISTENER", "setup3AControlsLocked: ENTRY")
                            setup3AControlsLocked(previewRequestBuilder!!)
                            Log.d("IMAGE_PROCESS_LISTENER", "setup3AControlsLocked: EXIT")
                            // Finally, we start displaying the camera preview.
                            cameraCaptureSession.setRepeatingRequest(
                                previewRequestBuilder!!.build(),
                                preCaptureCallback,
                                backgroundHandler
                            )

                            STATE_PREVIEW

                        } catch (e: Exception) {
                            Log.d("IMAGE_PROCESS_LISTENER", "setting state catch")
                            e.printStackTrace()
                            return
                        }
                        Log.d("IMAGE_PROCESS_LISTENER", "state = $state")
                        // When the session is ready, we start displaying the preview.
                        captureSession = cameraCaptureSession
                        takePicture()
                    }
                }

                override fun onConfigureFailed(cameraCaptureSession: CameraCaptureSession) {
                    // Failed to configure camera.
                }
            },
            backgroundHandler
        )
    }

    private fun getExifRotation(): Int {
        val deviceRotation = requireActivity().windowManager.defaultDisplay.rotation
        return when (sensorToDeviceRotation(characteristics, deviceRotation)) {
            90 -> ExifInterface.ORIENTATION_ROTATE_90
            180 -> ExifInterface.ORIENTATION_ROTATE_180
            270 -> ExifInterface.ORIENTATION_ROTATE_270
            else -> ExifInterface.ORIENTATION_NORMAL
        }
    }

    /**
     * Runnable that saves an [Image] into the specified [File], and updates
     * [android.provider.MediaStore] to include the resulting file.
     *
     *
     * This can be constructed through an [ImageSaverBuilder] as the necessary image and
     * result information becomes available.
     */
    private class ImageSaver private constructor(
        /**
         * The image to save.
         */
        private val mRawImage: Image?,
        /**
         * The image to save.
         */
        private val mJpegImage: Image?,
        /**
         * The file we save the image into.
         */
        private val mFile: File?,
        /**
         * The file we save the image into.
         */
        private val mJpegFile: File?,
        /**
         * The CaptureResult for this image capture.
         */
        private val mCaptureResult: CaptureResult?,
        /**
         * The CameraCharacteristics for this camera device.
         */
        private val mCharacteristics: CameraCharacteristics?,
        /**
         * The CameraCharacteristics for this camera device.
         */
        private val mFileDescription: String?,
        /**
         * The Context to use when updating MediaStore with the saved images.
         */
        private val mContext: Context,
        /**
         * A reference counted wrapper for the ImageReader that owns the given image.
         */
        private val mReader: RefCountedAutoCloseable<ImageReader>?,
        private val mExifOrientation: Int,
        private val postFileSavedBlock: (File?, File?, CaptureResult?) -> Unit
    ) : Runnable {
        override fun run() {
            var success = false
            if (mRawImage!!.format == ImageFormat.RAW_SENSOR) {
                val dngCreator = DngCreator(mCharacteristics!!, mCaptureResult!!)
                dngCreator.setOrientation(mExifOrientation)
                mFileDescription?.let {
                    dngCreator.setDescription(it)
                }

                var output: FileOutputStream? = null
                try {
                    output = FileOutputStream(mFile)
                    dngCreator.writeImage(output, mRawImage)
                    success = true
                } catch (e: IOException) {
                    e.printStackTrace()
                } finally {
                    mRawImage.close()
                    closeOutput(output)
                }
            }

            if (mJpegImage != null && mJpegImage.format == ImageFormat.JPEG) {
                val buffer = mJpegImage.planes[0].buffer
                val bytes = ByteArray(buffer.remaining())
                buffer.get(bytes)
                var output: FileOutputStream? = null
                try {
                    output = FileOutputStream(mJpegFile)
                    output.write(bytes)
                } catch (e: IOException) {
                    e.printStackTrace()
                } finally {
                    mJpegImage.close()
                    closeOutput(output)
                }
                success = true
            }

            // Decrement reference count to allow ImageReader to be closed to free up resources.
            mReader!!.close()

            // If saving the file succeeded, update MediaStore.
            if (success && ((mFile != null && mFile.exists()) || (mJpegFile != null && mJpegFile.exists()))) {
                postFileSavedBlock.invoke(
                    mFile, mJpegFile, mCaptureResult
                )
                if (mFile != null && mFile.exists()) {
                    MediaScannerConnection.scanFile(
                        mContext, arrayOf(mFile.path), /*mimeTypes*/
                        null, null
                    )
                }
            }
        }

        /**
         * Cleanup the given [OutputStream].
         *
         * @param outputStream the stream to close.
         */
        private fun closeOutput(outputStream: OutputStream?) {
            try {
                outputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        /**
         * Builder class for constructing [ImageSaver]s.
         *
         * This class is thread safe.
         *
         * Construct a new ImageSaverBuilder using the given [Context].
         * @param mContext a [Context] to for accessing the
         * [android.provider.MediaStore].
         */
        class ImageSaverBuilder(private val mContext: Context) {
            private var mRawImage: Image? = null
            private var mJpegImage: Image? = null
            private var mFile: File? = null
            private var mJpegFile: File? = null
            private var mCaptureResult: CaptureResult? = null
            private var mCharacteristics: CameraCharacteristics? = null
            private var mFileDescription: String? = null
            private var mReader: RefCountedAutoCloseable<ImageReader>? = null
            private var mExifRotation = 0
            private var isFinalImageProcessed = false
            private lateinit var postFileSaveBlock: (File?, File?, CaptureResult?) -> Unit

            @Synchronized
            fun setRefCountedReader(
                reader: RefCountedAutoCloseable<ImageReader>?
            ): ImageSaverBuilder {
                if (reader == null) {
                    throw NullPointerException()
                }
                mReader = reader
                return this
            }

            @Synchronized
            fun setExifRotation(rotation: Int): ImageSaverBuilder {
                mExifRotation = rotation
                return this
            }

            @Synchronized
            fun setImage(image: Image?, isRaw: Boolean): ImageSaverBuilder {
                if (image == null) {
                    throw NullPointerException()
                }
                if (isRaw) {
                    mRawImage = image
                } else {
                    mJpegImage = image
                }
                return this
            }

            @Synchronized
            fun setFile(file: File?): ImageSaverBuilder {
                if (file == null) {
                    throw NullPointerException()
                }
                mFile = file
                return this
            }

            @Synchronized
            fun setJpegFile(file: File?): ImageSaverBuilder {
                if (file == null) {
                    throw NullPointerException()
                }
                mJpegFile = file
                return this
            }

            @Synchronized
            fun setResult(result: CaptureResult?): ImageSaverBuilder {
                if (result == null) {
                    throw NullPointerException()
                }
                mCaptureResult = result
                return this
            }

            @Synchronized
            fun setCharacteristics(
                characteristics: CameraCharacteristics?
            ): ImageSaverBuilder {
                mCharacteristics = characteristics
                return this
            }

            @Synchronized
            fun setFileDescription(
                description: String
            ): ImageSaverBuilder {
                mFileDescription = description
                return this
            }

            @Synchronized
            fun setFileSavedCallback(block: (File?, File?, CaptureResult?) -> Unit): ImageSaverBuilder {
                postFileSaveBlock = block
                return this
            }

            @Synchronized
            fun setFinalImageProcessed(): ImageSaverBuilder {
                isFinalImageProcessed = true
                return this
            }

            @Synchronized
            fun buildIfComplete(isTakePhotoManually: Boolean): ImageSaver? {
                return if (!isComplete(isTakePhotoManually)) {
                    null
                } else ImageSaver(
                    mRawImage,
                    mJpegImage,
                    mFile,
                    mJpegFile,
                    mCaptureResult,
                    mCharacteristics,
                    mFileDescription,
                    mContext,
                    mReader,
                    mExifRotation,
                    postFileSaveBlock
                )
            }

            private fun isComplete(isTakePhotoManually: Boolean): Boolean {
                return (mRawImage != null && mJpegImage != null && mFile != null && mJpegFile != null && mCaptureResult != null && mCharacteristics != null && isFinalImageProcessed) || (isTakePhotoManually && mCharacteristics != null && mCaptureResult != null && mRawImage != null && mJpegImage != null && mJpegFile != null && mFile != null)
            }
        }
    }

    /**
     * Comparator based on area of the given [Size] objects.
     */
    internal class CompareSizesByArea : Comparator<Size> {
        override fun compare(lhs: Size, rhs: Size): Int {
            // We cast here to ensure the multiplications won't overflow
            return java.lang.Long.signum(lhs.width.toLong() * lhs.height - rhs.width.toLong() * rhs.height)
        }
    }

    /**
     * A wrapper for an [AutoCloseable] object that implements reference counting to allow
     * for resource management.
     */
    class RefCountedAutoCloseable<T : AutoCloseable?>(lock: T?) : AutoCloseable {
        private var mObject: T?
        private var mRefCount: Long = 0

        init {
            if (lock == null) {
                throw NullPointerException()
            }
            mObject = lock
        }

        /**
         * Increment the reference count and return the wrapped object.
         *
         * @return the wrapped object, or null if the object has been released.
         */
        @get:Synchronized
        val andRetain: T?
            get() {
                if (mRefCount < 0) {
                    return null
                }
                mRefCount++
                return mObject
            }

        /**
         * Return the wrapped object.
         *
         * @return the wrapped object, or null if the object has been released.
         */
        @Synchronized
        fun get(): T? {
            return mObject
        }

        /**
         * Decrement the reference count and release the wrapped object if there are no other
         * users retaining this object.
         */
        @Synchronized
        override fun close() {
            if (mRefCount >= 0) {
                mRefCount--
                if (mRefCount < 0) {
                    try {
                        mObject!!.close()
                    } catch (e: Exception) {
                        throw java.lang.RuntimeException(e)
                    } finally {
                        mObject = null
                    }
                }
            }
        }
    }

    /**
     * Given `choices` of `Size`s supported by a camera, choose the one that has the ratio closest
     * to the textureView size
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     * class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @return The optimal `Size`, or an arbitrary one if none matched
     */
    private fun chooseOptimalSize(
        choices: Array<Size>, textureViewWidth: Int, textureViewHeight: Int
    ): Size {
        return choices.minByOrNull { option ->
            textureViewWidth.toFloat() / textureViewHeight.toFloat() - option.width.toFloat() / option.height.toFloat()
        } ?: choices[0]
    }

    /**
     * Return true if the two given [Size]s have the same aspect ratio.
     *
     * @param a first [Size] to compare.
     * @param b second [Size] to compare.
     * @return true if the sizes have the same aspect ratio, otherwise false.
     */
    private fun checkAspectsEqual(a: Size, b: Size): Boolean {
        val aAspect = a.width / a.height.toDouble()
        val bAspect = b.width / b.height.toDouble()
        return abs(aAspect - bAspect) <= ASPECT_RATIO_TOLERANCE
    }

    /**
     * Rotation need to transform from the camera sensor orientation to the device's current
     * orientation.
     *
     * @param characteristics the [CameraCharacteristics] to query for the camera sensor
     * orientation.
     * @param deviceOrientation the current device orientation relative to the native device
     * orientation.
     * @return the total rotation from the sensor orientation to the current device orientation.
     */
    private fun sensorToDeviceRotation(
        characteristics: CameraCharacteristics?, deviceOrientation: Int
    ): Int {
        val sensorOrientation = characteristics?.get(CameraCharacteristics.SENSOR_ORIENTATION)!!

        // Get device orientation in degrees
        val deviceOrientationInDegrees = orientations[deviceOrientation]

        // Calculate desired orientation relative to camera orientation to make
        // the image upright relative to the device orientation
        return (sensorOrientation - deviceOrientationInDegrees + 360) % 360
    }

    /**
     * If the given request has been completed, remove it from the queue of active requests and
     * send an [ImageSaver] with the results from this request to a background thread to
     * save a file.
     *
     *
     * Call this only with [cameraStateLock] held.
     *
     * @param requestId the ID of the [CaptureRequest] to handle.
     * @param builder   the [ImageSaver.ImageSaverBuilder] for this request.
     * @param queue     the queue to remove this request from, if completed.
     */
    private fun handleCompletionLocked(
        requestId: Int,
        builder: ImageSaverBuilder?,
        queue: TreeMap<Int, ImageSaverBuilder>,
        isRaw: Boolean
    ) {
        if (builder == null) return
        val saver: ImageSaver? = builder.buildIfComplete(isTakePhotoManually)
        if (saver != null) {
            if (isTakePhotoManually) {
                if (!isRaw) {
                    queue.remove(requestId)
                }
            } else {
                queue.remove(requestId)
            }
            AsyncTask.THREAD_POOL_EXECUTOR.execute(saver)
        }
    }

    /**
     * Check if we are using a device that only supports the LEGACY hardware level.
     *
     * Call this only with [.cameraStateLock] held.
     *
     * @return true if this is a legacy device.
     */
    private fun isLegacyLocked(): Boolean {
        return characteristics?.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL) == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY
    }

    /**
     * Start the timer for the pre-capture sequence.
     *
     * Call this only with [cameraStateLock] held.
     */
    private fun startTimerLocked() {
        Log.d("IMAGE_PROCESS_LISTENER", "startTimerLocked")
        captureTimer = SystemClock.elapsedRealtime()
    }

    /**
     * Check if the timer for the pre-capture sequence has been hit.
     *
     * Call this only with [cameraStateLock] held.
     *
     * @return true if the timeout occurred.
     */
    private fun hitTimeoutLocked(): Boolean {
        return SystemClock.elapsedRealtime() - captureTimer > PRE_CAPTURE_TIMEOUT_MS
    }

    private fun handleImageRecognized() {
        Log.d("IMAGE_PROCESS_LISTENER", "handleImageRecognized: Entry")
        if (!shouldProcessNextQualifiedImage) {
            Log.d("IMAGE_PROCESS_LISTENER", "handleImageRecognized: Entry Failed")
            return
        }
        shouldProcessNextQualifiedImage = false

        openCVImageReader?.let {
            previewRequestBuilder?.removeTarget(it.surface)
        }

        // increase the delay time between when all conditions are satisfied and the picture is actually taken
        lifecycleScope.launch {
            if (!isFirstCapture) {
                isFirstCapture = true
                viewModel.validCardDetected(R.string.hold_your_phone_steady)
                delay(CAMERA_SESSION_FINAL_DETECTION)
            }
            viewModel.validCardDetected(R.string.card_detected)
            setRawCaptureSessionAndTakePicture()
        }
    }

    /** Use external media if it is available, our app's file directory otherwise */
    private fun getOutputDirectory(): File {
        val mediaDir = requireContext().externalMediaDirs.firstOrNull()?.let {
            File(it, getString(R.string.app_name)).apply {
                mkdirs()
            }
        }
        return if (mediaDir != null && mediaDir.exists()) {
            mediaDir
        } else {
            requireContext().filesDir
        }
    }

    companion object {
        private const val IMAGE_BUFFER_SIZE: Int = 3

        // Timeout for the pre-capture sequence.
        private const val PRE_CAPTURE_TIMEOUT_MS: Long = 1000

        // Tolerance when comparing aspect ratios.
        private const val ASPECT_RATIO_TOLERANCE = 0.005

        // Camera state: Device is closed.
        private const val STATE_CLOSED = 0

        // Camera state: Device is opened, but is not capturing.
        private const val STATE_OPENED = 1

        // Camera state: Showing camera preview.
        private const val STATE_PREVIEW = 2

        // Camera state: Waiting for 3A convergence before capturing a photo.
        private const val STATE_WAITING_FOR_3A_CONVERGENCE = 3

        /**
         * Conversion from screen rotation to JPEG orientation.
         */
        private val orientations = SparseIntArray(4).apply {
            append(Surface.ROTATION_0, 0)
            append(Surface.ROTATION_90, 90)
            append(Surface.ROTATION_180, 180)
            append(Surface.ROTATION_270, 270)
        }

        private val fileSdf = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US)

        private const val CAMERA_SESSION_RE_PROCCESSABLE_WAITING_TIME = 3_000
        private const val CAMERA_SESSION_RE_PROCCESSABLE_SLEEP_DURATION_TIME = 200
        private const val CAMERA_CARD_STEADY_WAITING_TIME = 0L
        private const val CAMERA_SESSION_FINAL_DETECTION = 2_000L
        val ACCEPTED_MIMETYPES = arrayOf("image/x-adobe-dng")
    }
}
