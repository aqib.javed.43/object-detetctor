package com.vessel.app.plan.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.plan.model.DayPlanItem
import kotlinx.android.synthetic.main.item_bonus_activities.view.*

class BonusActivityAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<DayPlanItem>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is DayPlanItem -> R.layout.item_bonus_activities
        else -> throw IllegalStateException(
            "Unexpected WellnessAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }
    interface OnActionHandler {
        fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int)
        fun showBonusActivityInformation()
    }
    override fun onBindViewHolder(holder: BaseViewHolder<DayPlanItem>, position: Int) {
        val item = getItem(position)
        holder.itemView.title.visibility = if (position == 0) View.VISIBLE else View.GONE
        holder.itemView.title.setOnClickListener {
            handler.showBonusActivityInformation()
        }
        super.onBindViewHolder(holder, position)
    }
}
