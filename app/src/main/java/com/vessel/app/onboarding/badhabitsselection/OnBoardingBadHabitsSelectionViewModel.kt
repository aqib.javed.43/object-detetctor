package com.vessel.app.onboarding.badhabitsselection

import android.util.Log
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BadHabitManager
import com.vessel.app.common.manager.OnBoardingProgramManager
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.net.data.ProgramPayLoadNew
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class OnBoardingBadHabitsSelectionViewModel @ViewModelInject constructor(
//    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    private val badHabitManager: BadHabitManager,
    private val programManager: ProgramManager,
    private val preferencesRepository: PreferencesRepository,
    private val onBoardingProgramManager: OnBoardingProgramManager
) : BaseViewModel(resourceProvider),
    BadHabitAdapter.OnActionHandler,
    ToolbarHandler {
    val bgColor = if (preferencesRepository.isExistingUserFirstTime) R.color.beige_color else R.color.primaryLight
    val state = OnBoardingBadHabitsSelectionState()
    var badHabitList: List<BadHabitSelect>? = null
    private val contactId = preferencesRepository.contactId

    init {
        showLoading(true)
        val subGoals = goalsRepository.getUserSubGoal()
        fetchAllBadHabits(subGoals)
    }
    private fun fetchAllBadHabits(subGoalId: String?) {
        viewModelScope.launch {
            badHabitManager.fetchAllBadHabit(subGoalId)
                .onSuccess { badHabitData ->
                    state {
                        items.value = badHabitData.map {
                            BadHabitSelect(
                                it.id,
                                it.title,
                                false
                            )
                        }
                    }
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        val goalId = goalsRepository.getUserMainGoal()!!.id
        badHabitList = state.items.value.orEmpty()
            .filter { it.checked }
        fetchOnBoardingProgram(goalId = goalId)
    }
    private fun fetchOnBoardingProgram(goalId: Int?) {
        val subGoals: List<String> = goalsRepository.getUserSubGoal()!!.split(",").map { it -> it.trim() }
        val iSubGoals = subGoals.map { it.toInt() }.toTypedArray()
        val iBadHabit = badHabitList!!.map { it.id.toInt() }.toTypedArray()
        val d1 = ProgramPayLoadNew(notes = "", contact_id = contactId, date = "", subgoals_ids = iSubGoals, bad_habits_ids = iBadHabit)
        viewModelScope.launch {
            showLoading(true)
            onBoardingProgramManager.onBoardingProgram(goalId)
                .onSuccess { onBoardingProgramData ->
                    state {
                        Log.d("kachraProgram", onBoardingProgramData[0].id.toString())
                        enrollProgram(programId = onBoardingProgramData[0].id!!, programAction = d1)
                    }
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    private fun enrollProgram(programId: Int, programAction: ProgramPayLoadNew) {
        state {
            viewModelScope.launch {
                showLoading()
                programManager.joinProgramString(programId, programAction)
                    .onSuccess {
                        navigateToSuccessfullyJoin()
                        hideLoading()
                    }
                    .onServiceError {
                        navigateToSuccessfullyJoin()
                        hideLoading()
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }
                    .onError {
                        hideLoading()
                    }
            }
        }
    }

    private fun navigateToSuccessfullyJoin() {
        state.navigateTo.value = OnBoardingBadHabitsSelectionFragmentDirections.actionOnBoardingBadHabitsSelectionFragmentToOnBoardingFinalStepOneFragment()
    }

    override fun onHabitSelectClicked(item: BadHabitSelect) {
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, diet ->
                    if (diet.title == item.title) diet.copy(checked = !item.checked)
                    else diet
                }.toMutableList()
        }
    }
}
