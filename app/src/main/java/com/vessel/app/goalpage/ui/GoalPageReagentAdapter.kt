package com.vessel.app.goalpage.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.goalpage.model.GoalPageReagent
import com.vessel.app.wellness.model.ReagentItem
import com.vessel.app.wellness.ui.ReagentAdapter

class GoalPageReagentAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<GoalPageReagent>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is GoalPageReagent -> R.layout.item_goal_page_reagents
        else -> throw IllegalStateException(
            "Unexpected WellnessFooterAdapter type at position $position for item ${getItem(position)}"
        )
    }

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onReagentItemClick(item: ReagentItem, view: View)
    }
    override fun onBindViewHolder(holder: BaseViewHolder<GoalPageReagent>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as GoalPageReagent
        val reagentAdapter = ReagentAdapter(
            handler = object : ReagentAdapter.OnActionHandler {
                override fun onReagentItemClick(item: ReagentItem, view: View) {
                    handler.onReagentItemClick(item, view)
                }
            }
        )
        holder.itemView.findViewById<RecyclerView>(R.id.reagentList).apply {
            adapter = reagentAdapter
            itemAnimator = null
            setHasFixedSize(true)
        }

        reagentAdapter.submitList(item.reagents)
    }
}
