package com.vessel.app.common.manager

import com.vessel.app.common.model.ErrorMessage
import com.vessel.app.common.model.Program
import com.vessel.app.common.model.ProgramModel
import com.vessel.app.common.model.Result
import com.vessel.app.common.net.HttpStatusCode.BAD_REQUEST
import com.vessel.app.common.net.data.ProgramPayLoadNew
import com.vessel.app.common.net.data.ProgramSummaryData
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.ProgramRepository
import com.vessel.app.wellness.net.data.ProgramAction
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProgramManager @Inject constructor(
    private val programRepository: ProgramRepository,
    private val preferencesRepository: PreferencesRepository,
    private val planManager: PlanManager,
) {

    private var programList: ProgramModel? = null
    private var oldGoalId: Int? = null
    private var enrolledProgramList: MutableList<Program>? = null

    suspend fun getProgramsByGoal(
        goalId: Int,
        forceUpdate: Boolean = false,
        perPage: Int? = 10,
        pageNumber: Int? = 1
    ): Result<ProgramModel, ErrorMessage> {
        return if (forceUpdate || programList == null || goalId != oldGoalId) {
            programRepository.getProgramsByGoal(
                goalId,
                perPage,
                pageNumber
            ).onSuccess {
                oldGoalId = goalId
                programList = it
            }
        } else {
            Result.Success(programList!!)
        }
    }

    suspend fun getEnrolledPrograms(forceUpdate: Boolean = false): Result<List<Program>, ErrorMessage> {
        return if (forceUpdate || enrolledProgramList == null) {
            when (val programList = programRepository.getPrograms()) {
                is Result.Success -> {
                    when (val programSchedule = programRepository.getProgramSchedule()) {
                        is Result.Success -> {
                            enrolledProgramList = programList.value.program
                                .filter { it1 -> it1.isEnrolled }
                                .onEach { program ->
                                    program.buildScheduleUiModelFromScheduleItems(
                                        programSchedule.value.firstOrNull { it.programId == program.id }?.schedule
                                    )
                                }.toMutableList()
                            Result.Success(enrolledProgramList!!)
                        }
                        is Result.UnknownError -> {
                            programSchedule
                        }
                        is Result.NetworkIOError -> {
                            programSchedule
                        }
                        is Result.HttpError -> {
                            programSchedule
                        }
                        is Result.ServiceError -> {
                            programSchedule
                        }
                    }
                }
                is Result.UnknownError -> {
                    programList
                }
                is Result.NetworkIOError -> {
                    programList
                }
                is Result.HttpError -> {
                    programList
                }
                is Result.ServiceError -> {
                    programList
                }
            }
        } else {
            Result.Success(enrolledProgramList!!)
        }
    }

    suspend fun joinProgram(
        program: Program,
        action: ProgramAction,
    ): Result<Any, ErrorMessage> {
        val joinResponse = programRepository.joinProgram(program, action)
        if (joinResponse is Result.Success) {
            when (val programSchedule = programRepository.getProgramSchedule(true)) {
                is Result.Success -> {
                    updateUserPlan()
                    val addedProgram =
                        program.copy(isEnrolled = true, enrolledDate = Date()).apply {
                            buildScheduleUiModelFromScheduleItems(programSchedule.value.firstOrNull { it.programId == program.id }?.schedule)
                        }
                    if (enrolledProgramList == null || action == ProgramAction.CLEAR) {
                        enrolledProgramList = mutableListOf()
                    }
                    enrolledProgramList?.add(addedProgram)
                }
                else -> {
                    return programSchedule
                }
            }
        }
        return joinResponse
    }

    suspend fun joinProgramString(
        program: Int,
        action: ProgramPayLoadNew,
    ): Result<Any, ErrorMessage> {
        return programRepository.joinProgramString(program, action)
    }

    suspend fun getProgramDetails(programId: Int): Result<Program, ErrorMessage> {
        val enrolledProgram = enrolledProgramList?.firstOrNull {
            it.id == programId
        }

        return if (enrolledProgram != null) {
            Result.Success(enrolledProgram)
        } else {
            programRepository.getProgramDetails(programId)
        }
    }

    suspend fun completeProgram(
        program: Program,
        action: ProgramAction,
        notes: String? = null,
    ): Result<Any, ErrorMessage> {
        when (val result = programRepository.completeProgram(program, action, notes)) {
            is Result.Success -> {
                updateUserPlan()
                if (action == ProgramAction.RE_ENROLL) {
                    val programSchedule = programRepository.getProgramSchedule(true)
                    if (programSchedule is Result.Success) {
                        val addedProgram = program.copy(isEnrolled = true, enrolledDate = Date())
                        program.buildScheduleUiModelFromScheduleItems(programSchedule.value.firstOrNull { it.programId == program.id }?.schedule)
                        enrolledProgramList?.removeIf { it.id == program.id }
                        enrolledProgramList?.add(addedProgram)
                    } else {
                        return programSchedule
                    }
                } else {
                    enrolledProgramList?.removeIf { it.id == program.id }
                }

                return result
            }
            is Result.ServiceError -> {
                return if (result.code == BAD_REQUEST &&
                    result.error.message.equals("program does not exist or not enrolled!")
                ) {
                    enrolledProgramList?.removeIf {
                        it.id == program.id
                    }
                    updateUserPlan()
                    Result.Success(Unit)
                } else {
                    result
                }
            }
            else -> {
                return result
            }
        }
    }

    private suspend fun updateUserPlan() {
        planManager.todayPlanItemCheckChange()
        val userPlans = planManager.getUserPlans(true)
        if (userPlans is Result.Success) {
            // don't do anything
        } else {
            planManager.updatePlanCachedData()
        }
    }

    suspend fun markPlanRecordAsCompleted(planData: PlanData): Result<Any, ErrorMessage> {
        val isCompleted = planData.plan_records?.firstOrNull()?.completed
        preferencesRepository.updateCompletedPlans(isCompleted, planData.id)
        return when (val programSchedule = programRepository.getProgramSchedule(true)) {
            is Result.Success -> {
                enrolledProgramList?.filter { it.id == planData.program_id }?.onEach {
                    it.apply {
                        buildScheduleUiModelFromScheduleItems(programSchedule.value.first { it.programId == id }.schedule)
                    }
                }
                Result.Success(Unit)
            }
            else -> {
                programSchedule
            }
        }
    }

    suspend fun rebuildScheduleForProgram(programId: Int?) {
        programId?.let {
            when (val programSchedule = programRepository.getProgramSchedule(true)) {
                is Result.Success -> {
                    updateUserPlan()
                    enrolledProgramList?.filter { it.id == programId }?.onEach {
                        it.apply {
                            buildScheduleUiModelFromScheduleItems(programSchedule.value.first { it.programId == id }.schedule)
                        }
                    }
                    Result.Success(Unit)
                }
                else -> {
                    programSchedule
                }
            }
        }
    }

    suspend fun fetchProgramSummary(programId: Int): Result<ProgramSummaryData, ErrorMessage> {
        return programRepository.fetchProgramSummary(programId)
    }
    fun clear() {
        enrolledProgramList = null
        programList = null
        oldGoalId = null
        programRepository.clear()
    }
}
