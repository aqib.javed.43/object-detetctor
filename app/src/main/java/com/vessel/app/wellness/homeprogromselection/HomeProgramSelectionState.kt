package com.vessel.app.wellness.homeprogromselection

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.common.net.data.PaginationData
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class HomeProgramSelectionState @Inject constructor(private val passedGoalIdSelected: Int) {

    var programPagination =
        MutableLiveData(PaginationData(page = 0, per_page = 3, pages = null, total = null))
    val goalIdSelected = MutableLiveData(passedGoalIdSelected)
    val programsItems = MutableLiveData<MutableList<Program>>()
    val isLoadingMore = MutableLiveData(false)
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: HomeProgramSelectionState.() -> Unit) = apply(block)
}
