package com.vessel.app.onboarding.weight

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.base.MutableLiveArrayList
import com.vessel.app.common.model.Contact
import com.vessel.app.common.util.DefaultAdapter
import com.vessel.app.util.SliderLayoutManager
import com.vessel.app.util.WindowUtils
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeightDialogFragment : BaseDialogFragment<WeightDialogViewModel>() {
    override val viewModel: WeightDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_weight_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()

        setupWeightSelector(
            R.id.hundreds,
            Contact.Weight.rangeHundreds,
            viewModel.hundreds
        )
        setupWeightSelector(
            R.id.tens,
            Contact.Weight.rangeTens,
            viewModel.tens
        )
        setupWeightSelector(
            R.id.units,
            Contact.Weight.rangeUnits,
            viewModel.units
        )
    }

    private fun setupObservers() {
        observe(viewModel.dismissDialog) {
            dismiss()
        }

        observe(viewModel.weightErrorEvent) {
            Toast.makeText(
                requireContext(),
                getString(R.string.invalid_weight_value),
                Toast.LENGTH_SHORT
            ).show()
        }

        observe(viewModel.weightSet) {
            findNavController().previousBackStackEntry
                ?.savedStateHandle
                ?.set(WEIGHT_KEY, it)
        }
    }

    private fun setupWeightSelector(
        @IdRes recyclerViewIdRes: Int,
        range: List<String>,
        initialPosition: Int
    ) {
        view?.findViewById<RecyclerView>(recyclerViewIdRes)?.let { recyclerView ->
            val padding = WindowUtils.getDisplayMetrics(requireContext()).widthPixels / 2
            recyclerView.setPadding(0, padding, 0, padding)

            val layoutManager = SliderLayoutManager(recyclerView) { positionSelected ->
                when (recyclerViewIdRes) {
                    R.id.hundreds -> viewModel.onHundredsSelected(positionSelected)
                    R.id.tens -> viewModel.onTensSelected(positionSelected)
                    R.id.units -> viewModel.onUnitsSelected(positionSelected)
                }
            }
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = DefaultAdapter(
                MutableLiveArrayList(range),
                viewLifecycleOwner,
                R.layout.item_radio_option_vertical
            )

            layoutManager.slideToPosition(initialPosition)
        }
    }

    companion object {
        const val WEIGHT_KEY = "weight"
    }
}
