package com.vessel.app.checkscience.model

import android.text.SpannableString
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R

data class CheckOutScienceHeaderItem(
    val content: SpannableString?,
    @DrawableRes val itemBackground: Int = R.color.white,
    @DrawableRes val contentBackground: Int = R.drawable.gray_background,
    @StringRes val title: Int = R.string.check_the_science
)
