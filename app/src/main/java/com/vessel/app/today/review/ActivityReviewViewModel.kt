package com.vessel.app.today.review

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.activityreview.model.ActivityReviewItem
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.RecommendationManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.LikeStatus
import com.vessel.app.wellness.net.data.ReviewRequest
import kotlinx.coroutines.launch

class ActivityReviewViewModel @ViewModelInject constructor(
    val state: ActivityReviewState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    private val recommendationManager: RecommendationManager,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    var feedbackText = ""
    var item = savedStateHandle.get<ActivityReviewItem>("reviewItem")!!
    var likeStatus: LikeStatus = item.likeStatus ?: LikeStatus.NO_ACTION
    init {
        feedbackText = ""
        state.remainingCharacters.value = getRemainingCharacters()
        state.likeBackground.value = getLikeBackground()
        state.disLikeBackground.value = getDisLikeBackground()
    }

    fun onLikeClicked() {
        likeStatus = LikeStatus.LIKE
        state.likeBackground.value = getLikeBackground()
        state.disLikeBackground.value = getDisLikeBackground()
    }
    fun onDisLikeClicked() {
        likeStatus = LikeStatus.DISLIKE
        state.disLikeBackground.value = getDisLikeBackground()
        state.likeBackground.value = getLikeBackground()
    }
    fun onSubmit() {
        if (likeStatus == LikeStatus.NO_ACTION) {
            state.navigateTo.value = ActivityReviewFragmentDirections.actionActivityReviewFragmentToAnswerNoticeDialogFragment(
                "Please make a selection\n" +
                    "to continue"
            )
            return
        }
        if (feedbackText.isEmpty()) {
            state.navigateTo.value = ActivityReviewFragmentDirections.actionActivityReviewFragmentToAnswerNoticeDialogFragment(
                "Please make a feedback\n" +
                    "to continue"
            )
            return
        }
        if (feedbackText.length < MIN_FEEDBACK_LENGTHS || feedbackText.length > MAX_FEEDBACK_LENGTHS) {
            state.navigateTo.value = ActivityReviewFragmentDirections.actionActivityReviewFragmentToAnswerNoticeDialogFragment(
                "Length must be between 3 and 500."
            )
            return
        }
        sendLikeStatus(likeStatus)
        val request = ReviewRequest(review = feedbackText, record_id = item.record_id, category = item.category)
        viewModelScope.launch {
            showLoading()
            recommendationManager.postReview(
                request
            ).onSuccess {
                if (likeStatus == LikeStatus.LIKE)preferencesRepository.updateLikedPlans(item.record_id.toString())
                else preferencesRepository.updateDislikedPlans(item.record_id.toString())
                hideLoading()
                state.navigateTo.value = ActivityReviewFragmentDirections.actionActivityReviewFragmentToFinishReviewFragment()
            }.onError {
                hideLoading()
                showError(it.toString())
            }
        }
    }
    private fun sendLikeStatus(likeStatus: LikeStatus) {
        viewModelScope.launch {
            item.record_id?.let {
                item.category?.let { category ->
                    recommendationManager.sendLikesStatus(
                        it,
                        category,
                        likeStatus == LikeStatus.LIKE
                    )
                }
            }
            if (likeStatus == LikeStatus.LIKE) preferencesRepository.updateLikedPlans(item.programId.toString())
            else preferencesRepository.updateDislikedPlans(item.programId.toString())
        }
    }
    fun onLaterClicked() {
        navigateBack()
    }
    fun onFeedbackTextChanged(s: CharSequence) {
        feedbackText = s.toString()
        state.remainingCharacters.value = getRemainingCharacters()
        state.textColor.value = getTextColor()
    }
    private fun getRemainingCharacters() = getResString(
        R.string.remaining_characters,
        MAX_FEEDBACK_LENGTHS - feedbackText.length
    )
    private fun getTextColor() =
        if (MAX_FEEDBACK_LENGTHS - feedbackText.count() > 20) R.color.black
        else R.color.red

    private fun getLikeBackground() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.green_background
    else
        R.drawable.gray_background

    private fun getDisLikeBackground() = if (likeStatus == LikeStatus.DISLIKE)
        R.drawable.red_rose_background
    else
        R.drawable.gray_background

    fun getThumbShapeIcon() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.ic_filled_thumbs_up_green
    else
        R.drawable.ic_outline_thumb_up

    companion object {
        const val MAX_FEEDBACK_LENGTHS = 500
        const val MIN_FEEDBACK_LENGTHS = 3
    }
}
