package com.vessel.app.foodplan.model

import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.vessel.app.BuildConfig
import com.vessel.app.R

data class FoodRecommendationViewItem(
    val name: String,
    val description: String,
    val foodNutrientsText: String,
    val isInPlan: Boolean,
    val usdaNdbNumber: Int,
    val amount: Float,
) {
    val imageUrl = "${BuildConfig.FOOD_IMAGE_URL_PREFIX}$usdaNdbNumber.jpg"

    @StringRes
    val buttonResId = if (isInPlan) {
        R.string.remove
    } else {
        R.string.add_it
    }

    @ColorRes
    val backgroundColorResId = if (isInPlan) {
        R.color.white
    } else {
        R.color.grayBg
    }

    companion object {
        val FOOD_RECOMMENDATION_VIEW_ITEM_LOADING =
            FoodRecommendationViewItem("", "", "", false, -1, 0f)
    }
}
