package com.vessel.app.goalpage.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.goalpage.model.GoalPageTodayActivity
import com.vessel.app.views.todayactivities.TodayActivitiesWidgetOnActionHandler
import kotlinx.android.synthetic.main.item_goal_page_today.view.*

class GoalPageTodayAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<GoalPageTodayActivity>(),
    TodayActivitiesWidgetOnActionHandler {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is GoalPageTodayActivity -> R.layout.item_goal_page_today
        else -> throw IllegalStateException(
            "Unexpected WellnessFooterAdapter type at position $position for item ${getItem(position)}"
        )
    }

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onActivitiesClick()
        fun onPlanItemClicked(item: Any, view: View)
        fun onItemSelected(item: Any, position: Int)
        fun onAnimationFinished(item: Any)
        fun onAddReminderClicked(item: Any, view: View)
    }
    override fun onBindViewHolder(holder: BaseViewHolder<GoalPageTodayActivity>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as GoalPageTodayActivity
        item.items?.let {
            holder.itemView.todayActivities.setHandler(this)
            holder.itemView.todayActivities.setList(items = it)
        }
    }
    override fun onActivitiesClick() {
        handler.onActivitiesClick()
    }

    override fun onPlanItemClicked(item: Any, view: View) {
        handler.onPlanItemClicked(item, view)
    }

    override fun onItemSelected(item: Any, position: Int) {
        handler.onItemSelected(item, position)
    }

    override fun onAddReminderClicked(item: Any, view: View) {
        handler.onAddReminderClicked(item, view)
    }

    override fun onAnimationFinished(item: Any) {
        handler.onAnimationFinished(item)
    }
}
