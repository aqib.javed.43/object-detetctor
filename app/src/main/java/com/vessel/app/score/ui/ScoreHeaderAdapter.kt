package com.vessel.app.score.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.score.model.ScoreHeaderItem

class ScoreHeaderAdapter : BaseAdapterWithDiffUtil<ScoreHeaderItem>() {
    override fun getLayout(position: Int) = R.layout.item_score_header

    override fun getHandler(position: Int): Any? = null
}
