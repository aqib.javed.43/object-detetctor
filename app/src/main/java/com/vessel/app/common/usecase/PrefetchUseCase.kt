package com.vessel.app.common.usecase

import android.icu.util.TimeZone
import com.vessel.app.common.manager.*
import com.vessel.app.common.repo.PreferencesRepository
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class PrefetchUseCase @Inject constructor(
    private val contactManager: ContactManager,
    private val todosManager: TodosManager,
    private val planManager: PlanManager,
    private val reagentsManager: ReagentsManager,
    private val recommendationManager: RecommendationManager,
    private val scoreManager: ScoreManager,
    private val preferencesRepository: PreferencesRepository,
    private val foodManager: FoodManager,
    private val reminderManager: ReminderManager,
    private val supplementsManager: SupplementsManager,
    private val goalsManager: GoalsManager,
    private val membershipManager: MembershipManager,
    private val buildPlanManager: BuildPlanManager
) {

    fun loadOptionalAppData() {
        MainScope().launch {
            foodManager.getRecommendedFoodItems(true)
            membershipManager.apply {
                checkMembershipStatus()
            }
            supplementsManager.getMemberships(true)
            scoreManager.apply {
                getWellnessScoresWithAverage(true)
                reagentDeficiencies(true)
            }
        }
    }

    suspend fun fetchAppData() {
        contactManager.getContact()
        if (preferencesRepository.userTimezone.equals(TimeZone.getDefault().id).not()) {
            contactManager.updateContactTimezone()
        }
        reagentsManager.fetchReagent()
        goalsManager.fetchAllGoals()
        recommendationManager.apply {
            getLatestSampleRecommendations(true)
        }
        planManager.getUserPlans(true)
    }

    fun clearCachedData() {
        todosManager.run {
            clearTodayTodosState()
            clearCachedData()
        }
        planManager.clearCachedData()
        foodManager.clearCachedData()
        recommendationManager.clearCachedData()
        scoreManager.run {
            clearFirstTestState()
            clearScoreDataState()
            clearPlanRecommendationTestState()
            clearCachedData()
        }
        buildPlanManager.clearViewPlanState()
        reminderManager.clearCachedData()
    }
}
