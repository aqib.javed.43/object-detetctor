package com.vessel.app.profile.editprofile

import android.view.View
import androidx.annotation.IdRes
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Contact
import com.vessel.app.common.model.Contact.Companion.BIRTH_DATE_FORMAT
import com.vessel.app.common.model.Contact.Companion.BIRTH_DATE_REGEX
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.validators.EmailValidator
import com.vessel.app.util.validators.NameType
import com.vessel.app.util.validators.NameValidator
import com.vessel.app.util.validators.ValidatorException
import kotlinx.coroutines.launch
import java.util.*

class EditProfileViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    val state: EditProfileState,
    val contactManager: ContactManager,
    val preferencesRepository: PreferencesRepository,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository), ToolbarHandler {
    private var initialContact: Contact? = null

    init {
        showLoading()

        viewModelScope.launch {
            contactManager.getContact()
                .onSuccess {
                    hideLoading()
                    state {
                        userFirstName.postValue(it.firstName)
                        userLastName.postValue(it.lastName)
                        userEmail.postValue(it.email)
                        gender.postValue(genderToRadioButton(it.gender))
                        userHeight.postValue(it.height?.toString() ?: "")
                        userWeight.postValue(it.weight?.toString() ?: "")
                        userBirthday.postValue(formatBirthDate(it.birthDateString))
                    }
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.USER_PROFILE_VIEWED)
                            .addProperty(TrackingConstants.KEY_ID, it.id.toString())
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun formatBirthDate(birthDateString: String?): String =
        if (birthDateString.isNullOrBlank()) {
            ""
        } else {
            getResString(R.string.born_date, birthDateString)
        }

    private fun genderToRadioButton(gender: Contact.Gender?) = when (gender) {
        Contact.Gender.MALE -> R.id.radioMale
        Contact.Gender.FEMALE -> R.id.radioFemale
        Contact.Gender.OTHER -> R.id.radioOther
        else -> R.id.radioMale
    }

    override fun onBackButtonClicked(view: View) {
        state.dismissView.call()
    }

    fun onChangePasswordClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CHANGE_PASSWORD_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )

        view.findNavController().navigate(
            EditProfileFragmentDirections.actionEditProfileToChangePasswordFragment()
        )
    }
    fun onManageDietAndAllergies(view: View) {
        view.findNavController().navigate(
            EditProfileFragmentDirections.actionEditProfileToFoodPreferencesFragment(getResString(R.string.food_preferences))
        )
    }

    fun onSaveClicked() {
        if (valuesDidNotChange()) {
            state.dismissView.call()
            return
        }

        if (!validateParams()) return

        showLoading()

        state {
            viewModelScope.launch {
                contactManager.updateContact(
                    firstName = userFirstName.value!!,
                    lastName = userLastName.value!!,
                    gender = convertGenderValue(gender.value!!),
                    height = convertHeight(userHeight.value),
                    weight = convertWeight(userWeight.value),
                    birthDate = convertAgeValue(userBirthday.value)
                )
                    .onSuccess {
                        trackingManager.log(
                            TrackedEvent.Builder(TrackingConstants.UPDATE_PROFILE_SUCCESS)
                                .withKlaviyo()
                                .withFirebase()
                                .withAmplitude()
                                .create()
                        )
                        dismissView.call()
                    }
                    .onServiceError {
                        val errorMessage = it.error.message ?: getResString(R.string.unknown_error)
                        logErrorEvent(errorMessage)
                        showError(errorMessage)
                    }
                    .onNetworkIOError {
                        val errorMessage = getResString(R.string.network_error)
                        logErrorEvent(errorMessage)
                        showError(errorMessage)
                    }
                    .onUnknownError {
                        val errorMessage = getResString(R.string.unknown_error)
                        logErrorEvent(errorMessage)
                        showError(errorMessage)
                    }
                    .onError {
                        hideLoading()
                    }
            }
        }
    }

    private fun logErrorEvent(errorMessage: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.UPDATE_PROFILE_FAIL)
                .setError(errorMessage)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun convertHeight(height: String?): Contact.Height? {
        return if (height.isNullOrBlank()) {
            null
        } else {
            Contact.Height.fromString(height)
        }
    }

    private fun convertWeight(weight: String?): Contact.Weight? {
        return if (weight.isNullOrBlank()) {
            null
        } else {
            Contact.Weight.fromString(weight)
        }
    }

    private fun convertAgeValue(birthday: String?): Date? {
        return if (birthday.isNullOrBlank()) {
            null
        } else {
            val date = BIRTH_DATE_REGEX.find(birthday)!!.value
            BIRTH_DATE_FORMAT.parse(date)
        }
    }

    private fun convertGenderValue(@IdRes userValue: Int) = when (userValue) {
        R.id.radioMale -> Contact.Gender.MALE
        R.id.radioFemale -> Contact.Gender.FEMALE
        R.id.radioOther -> Contact.Gender.OTHER
        else -> throw IllegalArgumentException("Unexpected gender value selected: $userValue!")
    }

    fun onWeightClicked(view: View) {
        view.findNavController()
            .navigate(
                EditProfileFragmentDirections.actionEditProfileToWeightDialog(
                    if (state.userWeight.value.isNullOrBlank()) Contact.Weight.DEFAULT
                    else state.userWeight.value!!.toFloat()
                )
            )
    }

    fun onHeightClicked(view: View) {
        view.findNavController()
            .navigate(
                EditProfileFragmentDirections.actionEditProfileToHeightDialog(
                    state.userHeight.value?.let { Contact.Height.fromString(it) }
                        ?: Contact.Height.DEFAULT_HEIGHT
                )
            )
    }

    fun onHeightSet(value: Contact.Height) {
        state.userHeight.value = value.toString()
    }

    fun onWeightSet(value: Float) {
        state.userWeight.value = value.toString()
    }

    fun onBirthdaySet(date: String) {
        state.userBirthday.value = formatBirthDate(date)
    }

    private fun validateParams() = try {
        EmailValidator().validate(state.userEmail)
        NameValidator(NameType.First).validate(state.userFirstName)
        NameValidator(NameType.Last).validate(state.userLastName)
    } catch (exception: ValidatorException) {
        showError(exception)
        false
    }

    private fun valuesDidNotChange() = initialContact?.firstName == state.userFirstName.value &&
        initialContact?.lastName == state.userLastName.value &&
        initialContact?.gender == convertGenderValue(state.gender.value!!) &&
        initialContact?.height?.toString().orEmpty() == state.userHeight.value &&
        initialContact?.weight?.toString().orEmpty() == state.userWeight.value &&
        initialContact?.birthDateString == state.userBirthday.value

    fun onUploadPhotoClick(view: View) {
        view.findNavController()
            .navigate(
                EditProfileFragmentDirections.actionEditProfileFragmentToUploadPhotoDialogFragment()
            )
    }
}
