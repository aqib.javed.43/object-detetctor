package com.vessel.app.nutritionsoptions.notmemberdialog

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.livechatinc.inappchat.ChatWindowActivity
import com.livechatinc.inappchat.ChatWindowConfiguration
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity

@AndroidEntryPoint
class NotMemberDialogFragment : BaseDialogFragment<NotMemberDialogViewModel>() {
    override val viewModel: NotMemberDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_not_member_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setGravity(Gravity.BOTTOM)
            setBackgroundDrawable(inset)
        }
        isCancelable = false

        observe(viewModel.dismissScreen) { dismiss() }

        observe(viewModel.openMessaging) {
            if (viewModel.isLiveChat()) {
                val intent = Intent(requireContext(), ChatWindowActivity::class.java)
                intent.putExtra(
                    ChatWindowConfiguration.KEY_GROUP_ID,
                    BuildConfig.NUTRITIONIST_COACH_GROUP
                )
                intent.putExtra(
                    ChatWindowConfiguration.KEY_LICENCE_NUMBER,
                    BuildConfig.LIVE_CHAT_LICENSE
                )
                intent.putExtra(
                    ChatWindowConfiguration.KEY_VISITOR_NAME,
                    viewModel.getUserName()
                )
                intent.putExtra(
                    ChatWindowConfiguration.KEY_VISITOR_EMAIL,
                    viewModel.getUserEmail()
                )
                requireContext().startActivity(intent)
            } else {
                MessagingActivity.builder()
                    .withToolbarTitle(getString(R.string.chat_to_nutritionist))
                    .withEngines(ChatEngine.engine())
                    .show(requireContext(), viewModel.getChatConfiguration())
            }
        }
    }
}
