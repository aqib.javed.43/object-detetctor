<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="item"
            type="com.vessel.app.plan.model.PlanItem" />

        <variable
            name="handler"
            type="com.vessel.app.views.todayactivities.TodayActivitiesWidgetOnActionHandler" />

    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="@dimen/today_activity_width"
        android:layout_height="@dimen/today_activity_height"
        android:layout_marginHorizontal="@dimen/spacing_xs"
        android:background="@drawable/white_rounded_background"
        app:clipToOutline="@{true}">

        <androidx.appcompat.widget.AppCompatImageView
            android:id="@+id/background"
            placeholder="@{item.background}"
            srcFromUrl="@{item.backgroundUrl}"
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:layout_marginTop="@dimen/spacing_large"
            android:background="@drawable/rounded_corners"
            android:backgroundTint="@color/white"
            android:scaleType="centerCrop"
            app:clipToOutline="@{true}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            tools:src="@drawable/plan_placeholder" />

        <com.vessel.app.views.RoundedBottomCornersBlurView
            android:id="@+id/blurForeground"
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:alpha="0"
            android:background="@drawable/white_rounded_background"
            android:backgroundTint="@color/zxing_transparent"
            app:layout_constraintBottom_toBottomOf="@id/background"
            app:layout_constraintEnd_toEndOf="@id/background"
            app:layout_constraintStart_toStartOf="@id/background"
            app:layout_constraintTop_toTopOf="@id/background" />

        <View
            android:id="@+id/checkboxBg"
            android:layout_width="@dimen/linen_circle_size"
            android:layout_height="@dimen/linen_circle_size"
            android:background="@drawable/white_circle"
            app:layout_constraintBottom_toBottomOf="@id/checkbox"
            app:layout_constraintEnd_toEndOf="@id/checkbox"
            app:layout_constraintStart_toStartOf="@id/checkbox"
            app:layout_constraintTop_toTopOf="@id/checkbox"
            tools:visibility="visible" />

        <androidx.appcompat.widget.AppCompatCheckBox
            android:id="@+id/checkbox"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="@dimen/spacing_normal"
            android:layout_marginTop="@dimen/spacing_3xs"
            android:button="@drawable/ic_radio_selector"
            android:checked="@{item.isCompleted}"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <androidx.appcompat.widget.AppCompatTextView
            android:id="@+id/title"
            style="@style/Title.Bold"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/spacing_normal"
            android:layout_marginEnd="@dimen/spacing_small"
            android:text="@{item.title}"
            android:textColor="@color/white"
            android:textSize="@dimen/font_medium"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="@id/checkbox"
            app:layout_constraintTop_toBottomOf="@id/checkbox"
            app:lineHeight="25sp"
            tools:text="@string/supplements" />

        <androidx.appcompat.widget.AppCompatTextView
            android:id="@+id/valueProp1"
            style="@style/ItemBody"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/spacing_small"
            android:text="@{item.valueProp1}"
            android:textSize="@dimen/font_2xs"
            app:bulletGapWidth="@{@dimen/spacing_small}"
            app:layout_constraintEnd_toEndOf="@id/title"
            app:layout_constraintStart_toStartOf="@id/title"
            app:layout_constraintTop_toBottomOf="@id/title"
            tools:text="@string/magnesium_value_prop1" />

        <View
            android:id="@+id/reminder"
            android:layout_width="0dp"
            android:layout_height="@dimen/reminder_background_height"
            android:layout_marginHorizontal="@dimen/spacing_large"
            android:layout_marginBottom="@dimen/spacing_normal"
            android:background="@drawable/black_alpha70_background"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent" />

        <androidx.appcompat.widget.AppCompatTextView
            android:id="@+id/reminderTime"
            style="@style/Title.Small"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="end"
            android:text="@{item.reminderTime}"
            android:textColor="@color/white"
            app:lineHeight="17sp"
            android:textSize="@dimen/font_xs"
            app:layout_constraintBottom_toBottomOf="@id/reminder"
            app:layout_constraintEnd_toEndOf="@id/reminder"
            app:layout_constraintStart_toStartOf="@id/reminder"
            app:layout_constraintTop_toTopOf="@id/reminder"
            tools:text="@string/reminders" />

        <com.airbnb.lottie.LottieAnimationView
            android:id="@+id/itemAddingAnimationView"
            android:layout_width="@dimen/linen_circle_size"
            android:layout_height="@dimen/linen_circle_size"
            app:layout_constraintEnd_toEndOf="@id/checkboxBg"
            app:layout_constraintStart_toStartOf="@id/checkboxBg"
            app:layout_constraintTop_toTopOf="@id/checkboxBg"
            app:lottie_autoPlay="false"
            app:lottie_loop="false"
            app:lottie_rawRes="@raw/plan_checkbox_success"
            app:lottie_repeatMode="restart"
            tools:ignore="ImageContrastCheck" />

        <com.airbnb.lottie.LottieAnimationView
            android:id="@+id/itemSuccessAnimationView"
            android:layout_width="0dp"
            android:layout_height="0dp"
            app:layout_constraintBottom_toBottomOf="@id/background"
            app:layout_constraintEnd_toEndOf="@id/background"
            app:layout_constraintStart_toStartOf="@id/background"
            app:layout_constraintTop_toTopOf="@id/background"
            app:lottie_autoPlay="false"
            app:lottie_loop="false"
            app:lottie_rawRes="@raw/plan_success"
            app:lottie_repeatMode="restart" />

    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
