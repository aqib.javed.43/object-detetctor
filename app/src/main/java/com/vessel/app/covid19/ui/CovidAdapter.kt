package com.vessel.app.covid19.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.covid19.model.CovidHeader
import com.vessel.app.covid19.model.CovidTest
import com.vessel.app.covid19.model.FAQ
import com.vessel.app.covid19.model.SectionHeader

class CovidAdapter(
    private val covidHeaderHandler: CovidHeaderHandler,
    private val covidTestHandler: CovidTestHandler
) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        CovidHeader -> R.layout.item_covid_header
        is SectionHeader -> R.layout.item_section_header
        is CovidTest -> R.layout.item_covid_test
        is FAQ -> R.layout.item_faq
        else -> throw IllegalStateException("Unexpected CovidAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any? = when (getItem(position)) {
        CovidHeader -> covidHeaderHandler
        is CovidTest -> covidTestHandler
        else -> null
    }

    interface CovidHeaderHandler {
        fun onTakeTestClicked(view: View)

        fun onBuyTestClicked(view: View)
    }

    interface CovidTestHandler {
        fun onCovidTestButtonClicked(view: View, item: CovidTest)
    }
}
