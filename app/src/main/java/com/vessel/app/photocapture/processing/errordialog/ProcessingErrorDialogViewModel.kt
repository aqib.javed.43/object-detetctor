package com.vessel.app.photocapture.processing.errordialog

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.MembershipManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.photocapture.processing.ProcessingErrorDialogActionType
import com.vessel.app.photocapture.processing.ProcessingErrorUiModel
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration

class ProcessingErrorDialogViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    val trackingManager: TrackingManager,
    val membershipManager: MembershipManager,
    val preferencesRepository: PreferencesRepository,
    val remoteConfiguration: RemoteConfiguration,
) : BaseViewModel(resourceProvider) {
    val navigateTo = LiveEvent<NavDirections>()
    val navigateToHomeScreen = LiveEvent<NavDirections>()
    val openChat = LiveEvent<Unit>()
    val processingErrorUiModel =
        savedStateHandle.get<ProcessingErrorUiModel>("processingErrorUiModel")!!
    val uuid = savedStateHandle.get<String>("uuid")!!
    private val wellnessCardUuid = savedStateHandle.get<String>("wellnessCardUuid")!!

    init {
        remoteConfiguration.fetchConfigFromRemote()
    }

    fun onScanNewCardClicked() {
        logErrorEvent(TrackingConstants.RESCAN_ERROR_RETRY)
        navigateTo.value =
            ProcessingErrorDialogFragmentDirections.actionProcessingErrorDialogFragmentToCapture()
    }

    fun onGotItClicked() {
        when (processingErrorUiModel.processingErrorDialogActionType) {
            ProcessingErrorDialogActionType.TRY_AGAIN -> {
                logErrorEvent(TrackingConstants.BE_ERROR_TRY_AGAIN)
                navigateTo.value =
                    ProcessingErrorDialogFragmentDirections.actionProcessingErrorDialogFragmentToCapture()
            }
            ProcessingErrorDialogActionType.VIEW_RESULT -> {
                logErrorEvent(TrackingConstants.RESCAN_ERROR_CONTINUE)
                navigateToHomeScreen.value =
                    ProcessingErrorDialogFragmentDirections.actionProcessingErrorDialogFragmentToHomeActivity(
                        uuid
                    )
            }
            ProcessingErrorDialogActionType.CHAT -> {
                logErrorEvent(TrackingConstants.CHAT_WITH_SUPPORT)
                openChat.call()
            }
        }
    }

    fun getChatConfiguration() =
        membershipManager.getChatConfiguration(MembershipManager.ZENDESK_SUPPORT_DEPARTMENT)

    fun isLiveChat() = remoteConfiguration.getBoolean("use_livechat")
    fun getUserName() = preferencesRepository.userFirstName
    fun getUserEmail() = preferencesRepository.userEmail

    private fun logErrorEvent(eventName: String) {
        trackingManager.log(
            TrackedEvent.Builder(eventName)
                .addProperty(TrackingConstants.KEY_UUID, uuid)
                .addProperty(TrackingConstants.KEY_WELLNESS_CARD_UUID, wellnessCardUuid)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
