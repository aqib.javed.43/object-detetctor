package com.vessel.app.buildplan.model

import androidx.annotation.DrawableRes

data class MakePlanItem(
    val title: String,
    val buttonText: String,
    @DrawableRes val background: Int,
    @DrawableRes val image: Int,
    val type: PlanType,
    val subTitle: String = "",
)
