package com.vessel.app.covid19.model

import android.content.Context
import android.view.View
import androidx.annotation.StringRes
import com.vessel.app.R
import java.text.SimpleDateFormat
import java.util.*

data class CovidTest(
    val uuid: String,
    val date: Date,
    val status: Status,
    val body: String
) {
    companion object {
        val DATE_FORMAT = SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH)
    }

    constructor(
        context: Context,
        uuid: String,
        date: Date,
        status: Status,
        email: String
    ) : this(
        uuid = uuid,
        date = date,
        status = status,
        body = context.run {
            when (status) {
                Status.CONSULTATION_REQUIRED, Status.UNREADABLE_PHOTO -> getString(R.string.start_review_body)
                Status.NO_CONSULTATION_REQUIRED -> getString(R.string.gift_for_you_body)
                Status.PENDING_DOCTOR_EVALUATION -> getString(R.string.pending_body, email)
            }
        }
    )

    val formattedDate: String = DATE_FORMAT.format(date)
    @StringRes val statusText = when (status) {
        Status.CONSULTATION_REQUIRED, Status.NO_CONSULTATION_REQUIRED, Status.UNREADABLE_PHOTO -> R.string.test_complete
        Status.PENDING_DOCTOR_EVALUATION -> R.string.pending
    }
    val buttonVisibility = if (status == Status.PENDING_DOCTOR_EVALUATION) View.GONE else View.VISIBLE
    val buttonText = when (status) {
        Status.CONSULTATION_REQUIRED, Status.UNREADABLE_PHOTO -> R.string.start_the_review
        Status.NO_CONSULTATION_REQUIRED -> R.string.a_gift_for_you
        else -> null
    }

    enum class Status(val value: Int) {
        CONSULTATION_REQUIRED(0),
        NO_CONSULTATION_REQUIRED(1),
        PENDING_DOCTOR_EVALUATION(2),
        UNREADABLE_PHOTO(3);

        companion object {
            fun valueOf(status: Int) = when (status) {
                0 -> CONSULTATION_REQUIRED
                1 -> NO_CONSULTATION_REQUIRED
                2 -> PENDING_DOCTOR_EVALUATION
                else -> UNREADABLE_PHOTO
            }
        }
    }
}
