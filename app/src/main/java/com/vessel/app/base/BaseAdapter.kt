package com.vessel.app.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<VH : BaseAdapter.ViewHolder>(
    protected val items: MutableLiveArrayList<*>,
    protected val lifecycleOwner: LifecycleOwner?
) : RecyclerView.Adapter<VH>() {
    private lateinit var layoutInflater: LayoutInflater
    protected abstract fun getLayout(position: Int): Int

    open fun getHandler(position: Int): Any? = null

    init {
        registerObserver()
    }

    @Suppress("UNCHECKED_CAST")
    open fun createViewHolder(viewDataBinding: ViewDataBinding) = ViewHolder(viewDataBinding) as VH

    private fun registerObserver() {
        if (lifecycleOwner != null) {
            items.observe(lifecycleOwner) {
                while (it?.isNotEmpty() == true) {
                    when (val action = it.remove()) {
                        is AdapterAction.NotifyItemRangeInserted -> notifyItemRangeInserted(
                            action.positionStart,
                            action.itemCount
                        )
                        is AdapterAction.NotifyItemRangeRemoved -> notifyItemRangeRemoved(
                            action.positionStart,
                            action.itemCount
                        )
                        is AdapterAction.NotifyItemRangeChanged -> notifyItemRangeChanged(
                            action.positionStart,
                            action.itemCount
                        )
                        AdapterAction.NotifyDataSetChanged -> notifyDataSetChanged()
                    }
                }
            }
        }
    }

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int) = getLayout(position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        if (!::layoutInflater.isInitialized) layoutInflater = LayoutInflater.from(parent.context)
        return createViewHolder(DataBindingUtil.inflate(layoutInflater, viewType, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(items[position], getHandler(position))
    }

    open class ViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        open fun bind(item: Any, handler: Any?) {
            binding.setVariable(BR.item, item)
            if (handler != null) binding.setVariable(BR.handler, handler)
            binding.executePendingBindings()
        }
    }
}

sealed class AdapterAction {
    class NotifyItemRangeInserted(val positionStart: Int, val itemCount: Int) : AdapterAction()
    class NotifyItemRangeRemoved(val positionStart: Int, val itemCount: Int) : AdapterAction()
    class NotifyItemRangeChanged(val positionStart: Int, val itemCount: Int) : AdapterAction()
    object NotifyDataSetChanged : AdapterAction()
}
