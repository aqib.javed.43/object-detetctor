package com.vessel.app.wellness.net.service

import com.vessel.app.wellness.net.data.GoalResponseData
import retrofit2.http.GET

interface GoalService {
    @GET("goal")
    suspend fun getAllGoals(): GoalResponseData
}
