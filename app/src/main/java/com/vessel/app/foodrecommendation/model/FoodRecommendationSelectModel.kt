package com.vessel.app.foodrecommendation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FoodRecommendationSelectModel(
    val id: Int,
    val title: String,
    val image: String,
    val checked: Boolean
) : Parcelable
