package com.vessel.app.onboarding.allergies.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.onboarding.allergies.model.OnboardingAllergySelect

class OnboardingAllergiesSelectAdapter(
    private val handler: OnActionHandler
) : BaseAdapterWithDiffUtil<OnboardingAllergySelect>({ oldItem, newItem ->
    oldItem.title == newItem.title
}) {
    override fun getLayout(position: Int) = R.layout.item_onboarding_allergy_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onAllergySelectClicked(item: OnboardingAllergySelect)
    }
}
