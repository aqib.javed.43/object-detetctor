package com.vessel.app.common.widget.loaderwidget

import com.vessel.app.R

data class LoaderUiModel(
    var isCompleted: Boolean = false,
) {
    val backgroundResId
        get() = if (isCompleted) {
            R.drawable.ic_active
        } else {
            R.drawable.ic_inactive
        }

    val lineColorResId
        get() = if (isCompleted) {
            R.color.smokey_black
        } else {
            R.color.white
        }
}
