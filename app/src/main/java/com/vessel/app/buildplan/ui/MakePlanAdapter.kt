package com.vessel.app.buildplan.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.buildplan.model.MakePlanHeader
import com.vessel.app.buildplan.model.MakePlanItem

class MakePlanAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        MakePlanHeader -> R.layout.item_make_plan_header
        is MakePlanItem -> R.layout.item_make_plan
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any? = when (getItem(position)) {
        is MakePlanItem -> handler
        else -> null
    }

    interface OnActionHandler {
        fun onMakeAPlanClick(item: MakePlanItem, view: View)
    }
}
