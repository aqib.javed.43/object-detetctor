package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.Constants
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.data.GoalMapper
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.wellness.net.service.GoalService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GoalsRepository @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val goalService: GoalService,
) : BaseNetworkRepository(moshi, loggingManager) {
    fun getGoals(): Map<Goal, Boolean> {
        val goalsListOrder = getGoalsOrder()
        return Goal.values()
            .sortedBy { goalsListOrder.indexOf(it.id) }
            .sortedByDescending { isGoalSelected(it.id) }
            .map { it to preferencesRepository.getUserChosenGoals().any { g -> g.id == it.id } }
            .toMap()
    }

    fun getSortedGoals(): List<Goal> {
        val goalsListOrder = getGoalsOrder()
        return Goal.values()
            .sortedBy { goalsListOrder.indexOf(it.id) }
            .sortedByDescending { isGoalSelected(it.id) }
    }

    private fun isGoalSelected(goalId: Int) =
        preferencesRepository.getUserChosenGoals().any { g -> g.id == goalId }

    fun setGoalsOrder(goalsOrder: List<Int>) {
        preferencesRepository.goalsOrder = goalsOrder.joinToString(",")
    }

    private fun getGoalsOrder(): List<Int> {
        return preferencesRepository.goalsOrder?.split(",").orEmpty().map { it.toInt() }
    }

    fun getUserChosenGoals() = preferencesRepository.getUserChosenGoals()
    fun getUserMainGoal() = preferencesRepository.getUserMainGoal()
    fun getUserSubGoal() = preferencesRepository.getUserSubGoalId()
    fun getUserHasEnrolledProgram() = preferencesRepository.getUserHasEnrolledProgram()
    fun setUserHasEnrolledProgram() {
        preferencesRepository.userHasAtLeastEnrolledProgram = true
    }

    fun setUserChosenGoals(goals: List<GoalSample>) =
        preferencesRepository.setUserChosenGoals(goals)

    fun setUserMainGoal(mainGoalId: Int) =
        preferencesRepository.setUserMainGoal(mainGoalId)

    fun setUserSubGoal(subGoalId: String) =
        preferencesRepository.setUserSubGoal(subGoalId)

    suspend fun getAllGoals() = executeWithErrorMessage {
        goalService.getAllGoals().goals.map {
            GoalMapper.map(it)
        }.also {
            Constants.GOALS = it
        }
    }
}
