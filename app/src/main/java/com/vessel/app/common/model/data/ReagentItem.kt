package com.vessel.app.common.model.data

import android.os.Parcelable
import androidx.annotation.ColorRes
import com.squareup.moshi.JsonClass
import com.vessel.app.R
import kotlinx.android.parcel.Parcelize

enum class ReagentItem(val id: Int, val apiName: String) {
    PH(1, "ph"),
    Hydration(2, "specific_gravity"),
    Ketones(3, "ketones"),
    VitaminC(4, "vitamin_c"),
    Magnesium(5, "magnesium"),
    B9(6, "b9"),
    Cortisol(8, "cortisol"),
    B7(11, "b7"),
    Calcium(18, "calcium"),
    Sodium(20, "sodium"),
    Nitrites(21, "nitrites"),
    Chloride(23, "chloride");

    companion object {
        fun fromId(id: Int) = when (id) {
            PH.id -> PH
            Hydration.id -> Hydration
            Ketones.id -> Ketones
            VitaminC.id -> VitaminC
            Magnesium.id -> Magnesium
            B9.id -> B9
            Cortisol.id -> Cortisol
            B7.id -> B7
            Calcium.id -> Calcium
            Sodium.id -> Sodium
            Chloride.id -> Chloride
            Nitrites.id -> Nitrites
            else -> null
        }
    }
}

@JsonClass(generateAdapter = true)
@Parcelize
data class ReagentRange(
    val from: Float,
    var to: Float,
    val label: String,
    val color: Int,
    val hintTitle: String,
    val hintDescription: String,
    val reagentType: ReagentLevel,
    val rangeRecommendationAction: ReagentButtonAction = ReagentButtonAction.NONE,
    val rangeRecommendation: RangeRecommendation?,
    val reportedValue: Float?,
) : Parcelable

enum class ReagentLevel(val level: String, @ColorRes val color: Int) {
    Low("low", R.color.linen),
    Good("good", R.color.primaryLight),
    High("high", R.color.linen),
    Great("great", R.color.linen)
}
enum class UtiLevel(val level: String, @ColorRes val color: Int) {
    Positive("positive", R.color.linen),
    Negative("negative", R.color.primaryLight),
    Abnormal("abnormal", R.color.linen),
    Normal("normal", R.color.primaryLight)
}
enum class ReagentButtonAction(val actionType: String?) {
    FOOD_PLAN("food"),
    STRESS_RELIEF_PLAN("stress"),
    FOOD_SUPPLEMENT_PLAN("food_supplement"),
    HYDRATION_PLAN("hydration"),
    NONE(null)
}
@JsonClass(generateAdapter = true)
@Parcelize
data class RangeRecommendation(
    val lifestyle: List<RangeLifestyleRecommendation>?,
    val food: RangeRDARecommendation?,
    val supplement: RangeRDARecommendation?,
) : Parcelable
@JsonClass(generateAdapter = true)
@Parcelize
data class RangeLifestyleRecommendation(
    val name: String?,
    val quantity: Float?,
) : Parcelable
@JsonClass(generateAdapter = true)
@Parcelize
data class RangeRDARecommendation(
    val rda_multiplier: Float?,
) : Parcelable
