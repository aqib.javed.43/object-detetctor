package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.Constants
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.data.SubGoalMapper
import com.vessel.app.wellness.net.service.SubGoalService
import javax.inject.Inject

class SubGoalRepository @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val subGoalService: SubGoalService,
) : BaseNetworkRepository(moshi, loggingManager) {
    suspend fun getAllSubGoals(goalId: Int?) = executeWithErrorMessage {
        subGoalService.getAllSubGoals(goalId).subgoals.map {
            SubGoalMapper.map(it)
        }.also {
            Constants.SUBGOALS = it
        }
    }
}
