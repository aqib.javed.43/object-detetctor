package com.vessel.app.foodplan.model

import com.vessel.app.common.manager.ReagentsManager
import com.vessel.app.common.model.FoodRecommendation
import com.vessel.app.common.model.FoodRecommendationServingSize
import com.vessel.app.common.model.Nutrient
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.FoodResponse
import com.vessel.app.common.net.data.SearchedFilteredFoodList
import com.vessel.app.wellness.net.data.LikeStatus
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FoodSearchMapper @Inject constructor(private val reagentsManager: ReagentsManager) {
    fun map(searchedFilteredFoodList: SearchedFilteredFoodList): List<FoodRecommendation> =
        searchedFilteredFoodList.food_response?.map { searchedFilteredFoodItem ->
            searchedFilteredFoodListToFoodPlanList(searchedFilteredFoodItem)
        } ?: emptyList()

    private fun searchedFilteredFoodListToFoodPlanList(
        searchedFilteredFoodItem: FoodResponse
    ): FoodRecommendation {
        return FoodRecommendation(
            usdaNdbNumber = searchedFilteredFoodItem.usda_ndb_number ?: 0,
            name = searchedFilteredFoodItem.food_title.orEmpty(),
            nutrients = extractNutrients(searchedFilteredFoodItem),
            servingSize = FoodRecommendationServingSize(
                quantity = searchedFilteredFoodItem.serving_quantity,
                display_quantity = searchedFilteredFoodItem.serving_quantity.toString(),
                unit = searchedFilteredFoodItem.serving_unit
            ),
            goals = searchedFilteredFoodItem.goals.orEmpty(),
            foodId = searchedFilteredFoodItem.id,
            imageUrl = searchedFilteredFoodItem.image_url.orEmpty(),
            likeStatus = LikeStatus.from(searchedFilteredFoodItem.like_status),
            totalLikes = searchedFilteredFoodItem.total_likes
        )
    }
    private fun extractNutrients(searchedFilteredFoodItem: FoodResponse) =
        mutableListOf<Nutrient>().apply {
            if (searchedFilteredFoodItem.nutrient_VitaminB7 != null && searchedFilteredFoodItem.nutrient_VitaminB7 > 0) {
                add(
                    Nutrient(
                        reagentsManager.fromId(ReagentItem.B7.id) ?: return@apply,
                        searchedFilteredFoodItem.nutrient_VitaminB7
                    )
                )
            }
            if (searchedFilteredFoodItem.nutrient_VitaminB9 != null && searchedFilteredFoodItem.nutrient_VitaminB9 > 0) {
                add(
                    Nutrient(
                        reagentsManager.fromId(ReagentItem.B9.id) ?: return@apply,
                        searchedFilteredFoodItem.nutrient_VitaminB9
                    )
                )
            }
            if (searchedFilteredFoodItem.nutrient_Magnesium != null && searchedFilteredFoodItem.nutrient_Magnesium > 0) {
                add(
                    Nutrient(
                        reagentsManager.fromId(ReagentItem.Magnesium.id) ?: return@apply,
                        searchedFilteredFoodItem.nutrient_Magnesium
                    )
                )
            }
            if (searchedFilteredFoodItem.nutrient_VitaminC != null && searchedFilteredFoodItem.nutrient_VitaminC > 0) {
                add(
                    Nutrient(
                        reagentsManager.fromId(ReagentItem.VitaminC.id) ?: return@apply,
                        searchedFilteredFoodItem.nutrient_VitaminC
                    )
                )
            }

            sortByDescending { it.amount }
        }
}
