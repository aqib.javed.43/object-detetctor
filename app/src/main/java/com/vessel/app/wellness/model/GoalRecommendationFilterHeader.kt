package com.vessel.app.wellness.model

import com.vessel.app.wellness.filterpopup.ItemFilterChoice

data class GoalRecommendationFilterHeader(val goalButton: GoalButton, val filterItems: List<ItemFilterChoice>)
