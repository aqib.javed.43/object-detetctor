package com.vessel.app.wellness.net.data
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SubGoalData(
    val id: Int,
    val rank: String? = null,
    val name: String,
    val goal_id: Int,
    val description: String? = null,
    val symptom_name: String?,
    val image_small_url: String? = null,
    val image_medium_url: String? = null,
    val image_large_url: String? = null,
)
