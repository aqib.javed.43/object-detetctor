package com.vessel.app.common.manager

import android.util.Base64
import com.squareup.moshi.Moshi
import com.vessel.app.common.net.data.SupplementDataJsonAdapter
import com.vessel.app.common.net.mapper.SupplementsMapper
import com.vessel.app.common.repo.SupplementsRepository
import com.vessel.app.supplementsbucket.model.Supplement
import com.vessel.app.vesselfuelcheckout.model.request.VesselFuelSupplementItemModel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SupplementsManager @Inject constructor(
    private val moshi: Moshi,
    private val supplementsMapper: SupplementsMapper,
    private val supplementsRepository: SupplementsRepository
) {
    private val vesselFuelCheckoutSuccessState = ConflatedBroadcastChannel(false)

    suspend fun getSupplements() = supplementsRepository.getSupplements()

    suspend fun getMemberships(forceUpdate: Boolean = false) =
        supplementsRepository.getMemberships(forceUpdate)
    suspend fun getMembership(forceUpdate: Boolean = false, email: String) =
        supplementsRepository.getMembership(forceUpdate, email)
    suspend fun getMembershipsAddresses() = supplementsRepository.getMembershipsAddresses()

    suspend fun getMembershipsPayment() = supplementsRepository.getMembershipsPayment()

    fun encodeSupplements(payList: List<Supplement>): String {
        val supplements = supplementsMapper.map(payList)
        val jsonString = supplements.map {
            SupplementDataJsonAdapter(moshi).toJson(it)
        }.toString().toByteArray()
        return Base64.encodeToString(jsonString, Base64.NO_WRAP)
    }

    suspend fun checkoutVesselFuel(
        addressId: Int,
        supplementItems: List<VesselFuelSupplementItemModel>
    ) = supplementsRepository.checkoutVesselFuel(addressId, supplementItems)

    suspend fun deleteIngredientsFromSupplements(supplementId: Int) =
        supplementsRepository.deleteIngredientsFromSupplements(supplementId)

    suspend fun addIngredientsToSupplements(supplementId: Int) =
        supplementsRepository.addIngredientsToSupplements(supplementId)

    fun getVesselFuelCheckoutSuccess() = vesselFuelCheckoutSuccessState.asFlow()

    fun updateVesselFuelCheckoutSuccess() {
        vesselFuelCheckoutSuccessState.offer(true)
    }

    fun clearVesselFuelCheckoutSuccess() {
        vesselFuelCheckoutSuccessState.cancel()
    }
    companion object {
        const val VESSEL_FUEL_PREFIX = "/pages/vessel-fuel?data="
    }
}
