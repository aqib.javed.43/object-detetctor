package com.vessel.app.testwinscore.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class TestWinScoreGoalItem(
    val scoreValue: Int,
    val winScoreValue: Int,
    @StringRes val title: Int,
    @DrawableRes val background: Int,
    @DrawableRes val image: Int
) {
    val score = scoreValue.toString()
    val winScore = winScoreValue.toString()
}
