package com.vessel.app.onboarding.food.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.home.HomeActivity
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingFoodFragment : BaseFragment<OnboardingFoodViewModel>() {
    override val viewModel: OnboardingFoodViewModel by viewModels()
    override val layoutResId = R.layout.fragment_food_onboarding

    val onboardingFoodSelectAdapter by lazy { FoodSelectAdapter(viewModel) }
    private lateinit var backButton: View
    override fun onViewLoad(savedInstanceState: Bundle?) {
        backButton = requireView().findViewById(R.id.toolbar)
        if (viewModel.isBackVisible == true) {
            backButton.isVisible = false
            requireActivity().onBackPressedDispatcher.addCallback(this) {}
        }
        val foodList = requireView().findViewById<RecyclerView>(R.id.foodList)
        foodList.layoutManager = GridLayoutManager(context, Constants.SPAN_COUNT_GOAL_GRID)
        foodList.adapter = onboardingFoodSelectAdapter
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(navigateToHome) {
                navigateToHomeActivity()
            }
            observe(items) {
                onboardingFoodSelectAdapter.submitList(it)
            }
            observe(refreshItems) {
                onboardingFoodSelectAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun navigateToHomeActivity() {
        val intent = Intent(requireContext(), HomeActivity::class.java)
        context?.startActivity(intent)
        activity?.finishAffinity()
    }
}
