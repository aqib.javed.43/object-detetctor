package com.vessel.app.onboarding.allergies

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Allergies
import com.vessel.app.common.model.Result
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.allergies.model.OnboardingAllergySelect
import com.vessel.app.onboarding.allergies.ui.OnboardingAllergiesFragmentDirections
import com.vessel.app.onboarding.allergies.ui.OnboardingAllergiesSelectAdapter
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class OnboardingAllergiesViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    val contactManager: ContactManager,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    OnboardingAllergiesSelectAdapter.OnActionHandler {

    val isBackVisible = savedStateHandle.get<Boolean>("showBackButton")
    val state = OnboardingAllergiesState(
        savedStateHandle["isFirstTestFlow"]!!,
        savedStateHandle["isLastStep"]!!,
    )

    init {
        state {
            items.value = Allergies.values()
                .map {
                    OnboardingAllergySelect(
                        it.title,
                        false,
                        it.isNone
                    )
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onAllergySelectClicked(item: OnboardingAllergySelect) {
        if (item.isNone && item.checked.not()) {
            state.items.value = state.items.value.orEmpty().map { it.copy(checked = false) }
        }

        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, diet ->
                    if (diet.title == item.title) diet.copy(checked = !item.checked)
                    else if (diet.isNone) diet.copy(checked = false)
                    else diet
                }.toMutableList()
            refreshItems.value = true
        }
    }

    private fun getSelectedAllergies() = state.items.value.orEmpty()
        .filter { it.checked }
        .mapNotNull { selectedItem ->
            Allergies.values().firstOrNull {
                it.title == selectedItem.title
            }?.apiName
        }

    private fun getUnSelectedAllergies() = state.items.value.orEmpty()
        .filter { !it.checked }
        .mapNotNull { unSelectedItem ->
            Allergies.values().firstOrNull {
                it.title == unSelectedItem.title
            }?.apiName
        }

    fun onNextClicked() {
        showLoading()
        state {
            viewModelScope.launch {
                val addAllergiesResponse = contactManager.addAllergies(getSelectedAllergies())
                val removeUnSelectedAllergiesResponse =
                    contactManager.deleteAllergies(getUnSelectedAllergies())

                if (addAllergiesResponse is Result.Success<*> && removeUnSelectedAllergiesResponse is Result.Success) {
                    preferencesRepository.setContactAllergies(getSelectedAllergies())
                    hideLoading()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.ALLERGIES_SUBMITTED)
                            .setScreenName(TrackingConstants.FROM_ONBOARDING)
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    if (isFirstTestFlow) {
                        navigateTo.value =
                            OnboardingAllergiesFragmentDirections.actionOnboardingAllergiesFragmentToOnboardingFoodFragment(
                                state.isLastStep,
                                false
                            )
                    } else {
                        navigateTo.value =
                            OnboardingAllergiesFragmentDirections.actionOnboardingAllergiesFragmentToOnboardingTermsFragment(
                                isFirstTestFlow
                            )
                    }
                } else {
                    if (isFirstTestFlow) {
                        val errorMessage = getResString(R.string.unknown_error)
                        showError(errorMessage)
                        hideLoading()
                    } else {
                        navigateTo.value =
                            OnboardingAllergiesFragmentDirections.actionOnboardingAllergiesFragmentToOnboardingTermsFragment(
                                isFirstTestFlow
                            )
                    }
                }
            }
        }
    }
}
