package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.net.data.plan.*
import com.vessel.app.common.net.service.PlanService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlanRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val planService: PlanService,
    private val preferencesRepository: PreferencesRepository
) : BaseNetworkRepository(moshi, loggingManager) {
    private var programPlansData: List<PlanData>? = null
    private var plansData: List<PlanData>? = null
    private var nextPlansData: PlanData? = null
    private var nextPlansDataList: ArrayList<PlanData> = arrayListOf()

    private var plansByDate: List<PlanData>? = null
    private var plansByDateRange: List<PlanData>? = null

    suspend fun getPlans(forceUpdate: Boolean = false): Result<List<PlanData>, Nothing> {
        return if (forceUpdate || plansData.isNullOrEmpty()) {
            executeWithCallback(
                {
                    planService.getUserPlans().plans
                },
                {
                    plansData = it
                }
            )
        } else {
            Result.Success(plansData!!)
        }
    }
    suspend fun getPlans(forceUpdate: Boolean = false, isToday: Boolean? = false, date: String? = null): Result<List<PlanData>, Nothing> {
        return if (forceUpdate || plansData.isNullOrEmpty()) {
            executeWithCallback(
                {
                    planService.getUserPlans(goalId = preferencesRepository.getUserMainGoalId(), isToday = if (isToday == true) "true" else "false", date = date).plans
                },
                {
                    plansData = it
                }
            )
        } else {
            Result.Success(plansData!!)
        }
    }
    suspend fun getProgramPlans(forceUpdate: Boolean = false, isToday: Boolean? = false, date: String? = null, programId: Int?): Result<List<PlanData>, Nothing> {
        return if (forceUpdate || programPlansData.isNullOrEmpty()) {
            executeWithCallback(
                {
                    planService.getUserPlans(goalId = preferencesRepository.getUserMainGoalId(), isToday = if (isToday == true) "true" else "false", date = date, programId = programId).plans
                },
                { data ->
                    val programsList: ArrayList<PlanData> = arrayListOf()
                    programsList.addAll(data)
                    nextPlansData?.let { nextPlan ->
                        if (programsList.firstOrNull {
                            it.id == nextPlan.id
                        } == null
                        )programsList.add(nextPlan)
                    }
                    programPlansData = programsList
                }
            )
        } else {
            Result.Success(programPlansData!!)
        }
    }
    suspend fun getPlansByDate(
        forceUpdate: Boolean = false,
        date: String? = null
    ): Result<List<PlanData>, Nothing> {
        return if (forceUpdate || plansByDate.isNullOrEmpty()) {
            executeWithCallback(
                {
                    planService.getUserPlans(date, isToday = "false").plans
                },
                {
                    plansByDate = it
                }
            )
        } else {
            Result.Success(plansByDate!!)
        }
    }

    suspend fun addPlanItem(plan: CreatePlanRequest) = executeWithErrorMessage {
        planService.addNewPlan(plan)
    }

    suspend fun updatePlanItem(planId: Int, plan: CreatePlanRequest) = executeWithErrorMessage {
        planService.updatePlanItem(planId, plan)
    }

    suspend fun deletePlanItem(planId: List<Int>) = executeWithErrorMessage {
        planService.deletePlanItem(DeletePlanItemRequest(planId))
    }

    suspend fun updatePlanToggle(planId: Int, date: String, isCompleted: Boolean, programDay: Int? = 0) =
        executeWithErrorMessage {
            planService.updatePlanToggle(planId, PlanToggleRequest(date, isCompleted, null, program_day = programDay))
            nextPlansDataList.removeIf {
                it.id == planId
            }
        }

    suspend fun buildPlan(request: BuildPlanRequest) = executeWithErrorMessage {
        planService.buildPlan(request)
    }

    fun clearCachedData() {
        plansData = null
    }
    fun getNextPlans(): List<PlanData> {
        return nextPlansDataList
    }

    suspend fun getPlansByDateRange(
        date: String? = null,
        endDate: String? = null
    ): Result<List<PlanData>, Nothing> {
        return if (plansByDateRange.isNullOrEmpty()) {
            executeWithCallback(
                {
                    planService.getPlansByRange(date, endDate).plans
                },
                {
                    val programsList: ArrayList<PlanData> = arrayListOf()
                    programsList.addAll(it)
                    nextPlansData?.let {
                        programsList.add(it)
                    }
                    plansByDateRange = programsList
                }
            )
        } else {
            Result.Success(plansByDateRange!!)
        }
    }
    suspend fun getNextPlans(
        forceUpdate: Boolean = false,
        contactId: Int?,
        programId: Int?
    ): Result<PlanData, Nothing> {
        return if (forceUpdate || nextPlansData == null) {
            executeWithCallback(
                {
                    planService.getNextUserPlans(contactId, programId)
                },
                {
                    nextPlansData = it
                    nextPlansDataList.add(it)
                }
            )
        } else {
            Result.Success(nextPlansData!!)
        }
    }
    suspend fun updateEssentialToggle(essentialToggleRequest: EssentialToggleRequest) =
        executeWithErrorMessage {
            planService.updateEssentialToggle(essentialToggleRequest)
        }

    fun clearNextPlaData() {
        nextPlansData = null
    }
}
