package com.vessel.app.wellness

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class RecommendationType : Parcelable {
    Lifestyle,
    Food,
    Tip,
    Supplement,
    Hydration
}
