package com.vessel.app.today

import android.util.Log
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.BuildConfig
import com.vessel.app.Constants
import com.vessel.app.Constants.DAY_DATE_FORMAT
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.activityreview.model.ActivityReviewItem
import com.vessel.app.base.BaseViewModel
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.*
import com.vessel.app.common.net.data.plan.EssentialToggleRequest
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.PlanRecord
import com.vessel.app.common.net.data.plan.toPlanRequest
import com.vessel.app.common.net.mapper.ProgramMapper
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.loaderwidget.PlanProgramsCardWidgetOnActionHandler
import com.vessel.app.foodplan.model.FoodDetailsItemModel
import com.vessel.app.homescreen.HomeScreenFragmentDirections
import com.vessel.app.plan.model.*
import com.vessel.app.plan.ui.*
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.stressreliefdetails.model.StressReliefDetailsItemModel
import com.vessel.app.today.model.ActivityListItem
import com.vessel.app.today.model.EssentialListItem
import com.vessel.app.today.model.LessonListItem
import com.vessel.app.today.ui.*
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.*
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap

class TodayViewModel @ViewModelInject constructor(
    val state: TodayState,
    resourceProvider: ResourceRepository,
    private val planManager: PlanManager,
    private val buildPlanManager: BuildPlanManager,
    val trackingManager: TrackingManager,
    val tipMapper: TipMapper,
    private val programMapper: ProgramMapper,
    private val preferencesRepository: PreferencesRepository,
    private val reminderManager: ReminderManager,
    private val programManager: ProgramManager,
    private val membershipManager: MembershipManager,
    private val lessonManager: LessonManager

) : BaseViewModel(resourceProvider), ActivityAdapter.OnActionHandler,
    AddToProgramFooterAdapter.OnActionHandler, EmptyPlanAdapter.OnActionHandler,
    EssentialAdapter.OnActionHandler, BonusActivityListAdapter.OnActionHandler,
    LessonAdapter.OnActionHandler, PlanProgramsCardWidgetOnActionHandler {
    private var enrolledPrograms: List<Program> = arrayListOf()
    private var selectedDate: Date? = null
    private var items: List<DayPlanItem> = arrayListOf()
    override val showPlanNotification = state.showPlanNotification
    override val planNotificationCount = state.planNotificationCount
    private var lessons: ArrayList<LessonListItem> = arrayListOf()
    private var essentials: ArrayList<EssentialListItem> = arrayListOf()
    private var programPlans: ArrayList<ActivityListItem> = arrayListOf()
    private var lessonFetchMap: HashMap<Int, Int> = HashMap()
    private var programFetchMap: HashMap<Int, Int> = HashMap()

    var isLoadingInProgress = false
    var today = DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())

    init {
        MainScope().launch {
            membershipManager.apply {
                checkMembershipStatus()
                checkTrialLock()
            }
        }
        state.keepDateState.value = false
        state.addPlanChatFooterItem.value = PlanChatFooter
        calculateNumberOfRecommendations()
        observeTodayTodoDataState()
        observeRemindersDataState()
    }

    fun checkTrialLock() {
        val trailLockItem = TrailLockItem()
        trailLockItem.isDisPlayIcon = false
        trailLockItem.title = getResString(R.string.lock_title)
        trailLockItem.message = getResString(R.string.membership_lock_description)
        trailLockItem.onDismiss = {}
        trailLockItem.onActiveMember = {
            state.navigateTo.value =
                HomeScreenFragmentDirections.globalActionToWeb(BuildConfig.BUY_TEST_URL)
        }
        state.trialLockItem.value = trailLockItem
        state.isTrialLocked.value = membershipManager.hasTrailLocked()
    }

    private fun loadEssentials() {
        for (program in enrolledPrograms) {
            program.essentials?.let { it ->
                val items = ArrayList<Essential>()
                for (es in it) {
                    var needAdded = true
                    es.essential_record?.let { list ->
                        val record = list.firstOrNull()
                        record?.let { esRecord ->
                            needAdded =
                                (esRecord.date != today.formatDayDate() && esRecord.completed).not()
                            es.completed =
                                esRecord.date == today.formatDayDate() && esRecord.completed
                        }
                    }
                    if (needAdded && !es.days.isNullOrEmpty()) {

                        if (items.firstOrNull { essentials ->
                                essentials.days!![0] == es.days!![0]
                            } == null) {
                            if (preferencesRepository.servedEssentials.contains(es.id.toString())
                                    .not()
                            ) {
                                trackingEssential(TrackingConstants.ESSENTIALS_SERVED, es)
                                preferencesRepository.updateServedLessons(es.id.toString())
                            }
                            items.add(es)
                        }
                    }
                }
                essentials.add(
                    EssentialListItem(
                        items.sortedBy { es ->
                            es.days?.get(0)
                        }, programTitle = program.title
                    )
                )
            }
        }
        state.essentialItems.value = essentials
    }

    fun loadPrograms(showLoader: Boolean = false) {
        if (state.keepDateState.value == true) {
            state.currentDate.value = selectedDate
            reloadData()
            state.keepDateState.value = false
        } else {
            this.enrolledPrograms = arrayListOf()
            today = DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
            if (isLoadingInProgress) return
            isLoadingInProgress = true
            showLoading()
            if (showLoader) state.showProgramLoading.value = true
            viewModelScope.launch() {
                programManager.getEnrolledPrograms(true).onSuccess { enrolledPrograms ->
                    state.programsItems.postValue(enrolledPrograms.filterNot { it.isEnded() })
                    state.showProgramLoading.postValue(false)
                    showTheCompletedProgramsIfNeeded(enrolledPrograms)
                }.onServiceError {
                    isLoadingInProgress = false
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }.onNetworkIOError {
                    isLoadingInProgress = false
                    showError(getResString(R.string.network_error))
                }.onUnknownError {
                    isLoadingInProgress = false
                    showError(getResString(R.string.unknown_error))
                }.onError {
                    isLoadingInProgress = false
                    state.showProgramLoading.postValue(false)
                }
            }
        }
    }

    private fun showTheCompletedProgramsIfNeeded(enrolledPrograms: List<Program>) {
        val endedProgram = enrolledPrograms.firstOrNull { it.isEnded() }
        this.enrolledPrograms = enrolledPrograms.filter {
            !it.isEnded()
        }.sortedBy { it.id }
        if (preferencesRepository.checkCompletedProgram) {
            if (endedProgram == null) {
                preferencesRepository.checkCompletedProgram = false
            }
        }
        val dates = today.getListDates()
        val progressDates: ArrayList<DayPlanProgressItem> = arrayListOf()
        dates.map {
            progressDates.add(DayPlanProgressItem(0, 0, it, it.formatPlanDate()))
        }
        isLoadingInProgress = false
        state.dateList.value = progressDates.sortedByDescending {
            it.date
        }
    }

    private fun fetchCompletedProgramPlans(program: Program) {
        viewModelScope.launch {
            planManager.getProgramPlans(
                forceUpdate = true, isToday = false, today?.formatDayDate(), programId = program.id
            ).onSuccess { plans ->
                val unlockedPlan = planManager.getUnlockedPlans()
                val items: ArrayList<PlanData> = arrayListOf()
                unlockedPlan.firstOrNull {
                    it.program_id == program.id
                }?.let {
                    items.add(it)
                }
                val groupedFoodItems = items.filter {
                    it.food_id != null
                }.groupBy {
                    it.food_id
                }

                val groupedLifeStyleItemsItems = items.filter {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null
                }.groupBy {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                }

                val groupedSupplementItems = items.filter {
                    it.isSupplementPlan()
                }

                val groupedTips = items.filter {
                    it.isTip()
                }.groupBy {
                    it.tip_id
                }
                val completedPlans = buildProgramActivities(
                    groupedFoodItems,
                    groupedLifeStyleItemsItems,
                    groupedSupplementItems,
                    groupedTips,
                    today,
                    program
                )
                if (completedPlans.isNullOrEmpty()) {
                    fetchNewProgramPlans(program, completedPlans)
                } else {
                    val currentDayDate = preferencesRepository.getUnlockedPrograms().firstOrNull {
                        it.id == program.id
                    }?.count ?: 0
                    val item = ActivityListItem(
                        completedPlans,
                        selectedDate!!,
                        program.title,
                        isPreviousDay = false,
                        noMoreActivity = currentDayDate >= MAX_ACTIVITIES_DAYS
                    )
                    programPlans.add(item)
                    if (programPlans.size == enrolledPrograms.size) {
                        processAllProgramPlansResponse()
                    }
                }
            }.onError {
                hideLoading(false)
            }
        }
    }

    private fun fetchNewProgramPlans(program: Program, completedPlans: List<PlanItem>) {
        viewModelScope.launch {
            planManager.getProgramPlans(
                forceUpdate = true, isToday = true, today?.formatDayDate(), programId = program.id
            ).onSuccess { plans ->
                val unlockedPlan = planManager.getUnlockedPlans()
                val items: ArrayList<PlanData> = arrayListOf()
                items.addAll(plans)
                unlockedPlan.firstOrNull {
                    it.program_id == program.id
                }?.let {
                    items.add(it)
                }
                val groupedFoodItems = items.filter {
                    it.food_id != null
                }.groupBy {
                    it.food_id
                }

                val groupedLifeStyleItemsItems = items.filter {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null
                }.groupBy {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                }

                val groupedSupplementItems = items.filter {
                    it.isSupplementPlan()
                }

                val groupedTips = items.filter {
                    it.isTip()
                }.groupBy {
                    it.tip_id
                }
                val plans = buildProgramActivities(
                    groupedFoodItems,
                    groupedLifeStyleItemsItems,
                    groupedSupplementItems,
                    groupedTips,
                    today,
                    program
                )
                val allPlans = arrayListOf<PlanItem>()
                allPlans.addAll(completedPlans)
                allPlans.addAll(plans)
                val currentDayDate = preferencesRepository.getUnlockedPrograms().firstOrNull {
                    it.id == program.id
                }?.count ?: 0
                val item = ActivityListItem(
                    allPlans,
                    selectedDate!!,
                    program.title,
                    isPreviousDay = false,
                    noMoreActivity = currentDayDate >= MAX_ACTIVITIES_DAYS
                )
                programPlans.add(item)
                if (programPlans.size == enrolledPrograms.size) {
                    processAllProgramPlansResponse()
                }
            }.onError {
                hideLoading()
            }
        }
    }

    fun loadUserPlans(forceUpdate: Boolean = false, isFromUpdatedState: Boolean = false) {
        if (!isFromUpdatedState) showLoading()
        selectedDate?.let { date ->
            fetchWellnessActivities(date = date, forceUpdate = forceUpdate)
        }
    }

    private fun setupDailyData(
        groupedFoodItems: Map<Int?, List<PlanData>>,
        groupedLifeStyleItemsItems: Map<Int?, List<PlanData>>,
        groupedSupplementItems: List<PlanData>,
        groupedTips: Map<Int?, List<PlanData>>
    ) {
        val dailyItems = mutableListOf<PlanItem>()
        groupedFoodItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapToDoToPlanItem(
                        it, true, 1, grouped, grouped
                    )
                )
            }
        }
        groupedTips.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapToDoToPlanItem(
                        it, true, 1, grouped, grouped
                    )
                )
            }
        }
        groupedLifeStyleItemsItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapToDoToPlanItem(
                        it, true, 1, grouped, grouped
                    )
                )
            }
        }
        groupedSupplementItems.map { item ->
            dailyItems.add(
                mapToDoToPlanItem(
                    item, true, 1, groupedSupplementItems, groupedSupplementItems
                )
            )
        }

        val daysPlans = mutableMapOf<Date, MutableList<PlanItem>>()
        dailyItems.onEach { planItem ->
            planItem.getListDates().onEach { day ->
                if (daysPlans.containsKey(day)) {
                    if (daysPlans[day]?.contains(planItem) == false) daysPlans[day]?.add(planItem)
                } else {
                    daysPlans[day] = mutableListOf(planItem)
                }
            }
        }

        val sortedDaysPlans: MutableMap<Date, MutableList<PlanItem>> = LinkedHashMap()
        daysPlans.keys.sortedDescending().forEach { date ->
            sortedDaysPlans[date] = daysPlans[date]!!
        }

        val dailyPlans = mutableListOf<DayPlanItem>()
        sortedDaysPlans.forEach { daysPlan ->
            val dayItems = daysPlan.value
            val date = daysPlan.key
            val planItems = togglePlanItems(dayItems, date)
            dailyPlans.add(
                DayPlanItem(
                    planItems.count { it.isCompleted },
                    planItems.size,
                    date.formatPlanDate(),
                    getSortedPlans(planItems),
                    date
                )
            )
        }

        dailyPlans.removeIf { it.items.isEmpty() }
        state.todayTodosData = dailyPlans
    }

    private fun togglePlanItems(
        dayItems: List<PlanItem>, formatPlanDate: Date, isProgramActivity: Boolean? = false
    ): List<PlanItem> {
        return dayItems.map { item ->
            val recordId = when (item.type) {
                PlanType.StressPlan -> {
                    item.currentPlan.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                }
                PlanType.FoodPlan -> {
                    item.currentPlan.food_id
                }
                PlanType.TIP -> {
                    item.currentPlan.tip_id
                }
                else -> item.currentPlan.id
            }
            val likeStatus = when {
                preferencesRepository.likedPlans.contains(recordId.toString()) -> LikeStatus.LIKE
                preferencesRepository.disLikedPlans.contains(recordId.toString()) -> LikeStatus.DISLIKE
                else -> LikeStatus.NO_ACTION
            }
            item.copy(

                isCompleted = item.currentPlan.plan_records.orEmpty().firstOrNull { record ->
                    record.completed == true && formatPlanDate.isSameDay(
                        Constants.DAY_DATE_FORMAT.parse(
                            record.date
                        )
                    )
                } != null, likeStatus = likeStatus, isProgramActivity = isProgramActivity

            )
        }
    }

    private fun toggleProgramPlanItems(
        dayItems: List<PlanItem>, formatPlanDate: Date, isProgramActivity: Boolean? = false
    ): List<PlanItem> {
        return dayItems.map { item ->
            val completedRecord = item.currentPlan.plan_records.orEmpty().firstOrNull { record ->
                record.completed == true && formatPlanDate.isSameDay(
                    Constants.DAY_DATE_FORMAT.parse(
                        record.date
                    )
                ) && record.is_deleted == false
            }
            val isContainCompletedRecord = completedRecord != null

            val likeStatus = when {
                preferencesRepository.likedPlans.contains(item.id.toString()) -> LikeStatus.LIKE
                preferencesRepository.disLikedPlans.contains(item.id.toString()) -> LikeStatus.DISLIKE
                else -> LikeStatus.NO_ACTION
            }

            item.copy(
                isCompleted = isContainCompletedRecord,
                likeStatus = likeStatus,
                isProgramActivity = isProgramActivity
            )
        }
    }

    private fun getSortedPlans(planItems: List<PlanItem>): List<PlanItem> {
        return planItems.sortedBy {
            it.currentPlan.getTimeFormattedAsNumber()
        }
    }

    private fun calculateNumberOfRecommendations() {
        viewModelScope.launch(Dispatchers.IO) {
            buildPlanManager.loadPlanData()
            val count = buildPlanManager.countRedBadges()
            if (count > 0) state.planNotificationCount.postValue(count)
            state.showPlanNotification.postValue(count > 0)
        }
    }

    private fun observeTodayTodoDataState() {
        viewModelScope.launch {
            planManager.getPlanDataState().collect {
                if (it) {
                    planManager.clearPlanState()
                    loadUserPlans(true, isFromUpdatedState = true)
                }
            }
        }
    }

    private fun observeRemindersDataState() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    loadUserPlans(true)
                }
            }
        }
    }

    private fun loadPlan(items: List<DayPlanItem>, withUpdate: Boolean = false) {
        this.items = items
        if (withUpdate) state.planItems.value = Pair(items, withUpdate)
        state.isTakeFirstTest.value = preferencesRepository.afterFirstTest()
    }

    fun setDate(date: Date) {
        if (state.keepDateState.value == true) return
        Log.d(TAG, "setDate: ${date.formatDayDateWithSecond()}")
        this.selectedDate = date
        fetchData()
    }

    private fun fetchData() {
        loadProgramPlans()
        loadUserPlans(true)
        essentials.clear()
        loadEssentials()
        loadLesson()
    }

    private fun loadLesson() {
        lessons.clear()
        if (enrolledPrograms.isNullOrEmpty()) {
            state.lessonItems.value = arrayListOf()
        } else {
            for (pr in this.enrolledPrograms) {
                selectedDate?.let {
                    if (it.isSameDay(today)) fetchCompletedLessons(pr)
                    else fetchPreviousCompletedLessons(pr, it)
                }
            }
        }
    }

    private fun loadProgramPlans() {
        programPlans.clear()
        if (enrolledPrograms.isNullOrEmpty()) {
            state.programsItems.value = arrayListOf()
        } else {
            for (pr in this.enrolledPrograms) {
                selectedDate?.let {
                    if (it.isSameDay(today)) fetchNewProgramPlans(pr, arrayListOf())
                    else fetchPreviousCompletedPlans(pr, it)
                }
            }
        }
    }

    private fun fetchPreviousCompletedPlans(program: Program, date: Date) {
        viewModelScope.launch {
            planManager.getProgramPlans(
                forceUpdate = true, isToday = false, date.formatDayDate(), programId = program.id
            ).onSuccess { items ->
                val groupedFoodItems = items.filter {
                    it.food_id != null
                }.groupBy {
                    it.food_id
                }

                val groupedLifeStyleItemsItems = items.filter {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null
                }.groupBy {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                }

                val groupedSupplementItems = items.filter {
                    it.isSupplementPlan()
                }

                val groupedTips = items.filter {
                    it.isTip()
                }.groupBy {
                    it.tip_id
                }
                val completedPlans = buildProgramActivities(
                    groupedFoodItems,
                    groupedLifeStyleItemsItems,
                    groupedSupplementItems,
                    groupedTips,
                    date,
                    program
                )

                val item = ActivityListItem(
                    completedPlans, date, program.title, isPreviousDay = true
                )
                programPlans.add(item)
                if (programPlans.size == enrolledPrograms.size) {
                    processAllProgramPlansResponse()
                }
            }.onError {}
        }
    }

    override fun onPlanItemClicked(item: Any, view: View, parentPosition: Int) {
        if (item is PlanItem && item.type == PlanType.StressPlan) {
            view.findNavController().navigate(
                HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToStressReliefDetailsFragment(
                    StressReliefDetailsItemModel(
                        item.planName,
                        item.valueProp1.orEmpty(),
                        item.currentPlan.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                            ?: return,
                        true
                    )
                )
            )
        } else if (item is PlanItem && item.type == PlanType.FoodPlan) {
            view.findNavController().navigate(
                HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToFoodPlanDetailsFragment(
                    FoodDetailsItemModel(
                        item.title,
                        item.valueProp1.orEmpty(),
                        item.plans.first().food_id ?: return,
                        true
                    )
                )
            )
        } else if (item is PlanItem && item.type == PlanType.WaterPlan) {
            view.findNavController()
                .navigate(HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToHydrationPlanFragment())
        } else if (item is PlanItem && item.type == PlanType.TestPlan) {
            view.findNavController()
                .navigate(HomeScreenFragmentDirections.globalActionToTestingPlanFragment())
        } else if (item is PlanItem && item.type == PlanType.SupplementPlan) {
            view.findNavController()
                .navigate(HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToSupplementFragment())
        } else if (item is PlanItem && item.type == PlanType.TIP) {
            view.findNavController().navigate(
                HomeScreenFragmentDirections.globalActionToTipDetailsFragment(
                    tipMapper.mapTipData(
                        item.currentPlan.tip!!
                    )
                )
            )
        } else if (item is AddPlanFooter) {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.ADD_TO_YOUR_PLAN_TAPPED).withKlaviyo()
                    .withFirebase().withAmplitude().create()
            )
            view.findNavController()
                .navigate(HomeScreenFragmentDirections.globalActionToBuildPlan())
        } else if (item is PlanChatFooter) {
            state.openMessaging.call()
        }
    }

    override fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int) {
        Log.d(TAG, "onItemSelected: $position")
        if (selectedDate != today) {
            showError("Previous Date !")
            return
        }
        if (item is PlanItem) {
            if (item.isCompleted.not()) {
                if (item.isProgramActivity == true) trackingProgramActivity(
                    TrackingConstants.ACTIVITY_COMPLETED, item
                )
                else trackingWellnessActivity(TrackingConstants.WELLNESS_ACTIVITY_COMPLETED, item)
            }
            if (item.isGrouped) {
                updateWeeklyItemToggle(position, parentPosition)
            } else {
                updateDailyItemToggle(item, position, parentPosition)
            }
        }
    }

    override fun showBonusActivityInformation() {
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalBonusActivitiesDialogFragment()
    }

    override fun onTakeTest() {
        Constants.onTakeTest?.invoke()
    }

    override fun onEssentialSelected(item: Essential) {
    }

    override fun showEssentialInformation() {
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalEssentialsDialogFragment()
    }

    override fun onEssentialCompleted(item: Essential, view: View, parentPosition: Int) {
        preferencesRepository.completedEssentials.add(item.id.toString())
        val currentItems = state.essentialItems.value ?: arrayListOf()
        if (currentItems.isNotEmpty()) {
            val essentials = currentItems[parentPosition]
            val newItems = essentials.items?.map {
                if (it.id == item.id) it.copy(completed = item.completed.not())
                else it.copy()
            }
            state.essentialItems.value = currentItems.mapIndexed { index, essentialListItem ->
                if (index == parentPosition) essentialListItem.copy(items = newItems)
                else essentialListItem.copy()
            }
        }
        markEssentialUpdate(item)
    }

    private fun markEssentialUpdate(item: Essential) {
        if (item.completed.not()) {
            trackingEssential(TrackingConstants.ESSENTIALS_COMPLETED, item)
        }
        val essentialToggleRequest = EssentialToggleRequest()
        essentialToggleRequest.completed = item.completed.not()
        essentialToggleRequest.essential_id = item.id
        essentialToggleRequest.contact_id = preferencesRepository.contactId
        essentialToggleRequest.date = today.formatDayDate()
        essentialToggleRequest.program_day = item.days?.firstOrNull()
        viewModelScope.launch {
            planManager.updateEssentialToggle(essentialToggleRequest).onSuccess {
                Log.d(TAG, "markEssentialUpdate: $it")
            }.onError {
                Log.e(TAG, "markEssentialUpdate: $it")
            }
        }
    }

    override fun onEssentialLinkSelected(item: Essential, view: View) {
        item.link_url?.let {
            state.openEssentialLink.value = it
        }
    }

    private fun updateDailyItemToggle(
        item: PlanItem, position: Int, parentPosition: Int
    ) {

        var date = DAY_DATE_FORMAT.format(selectedDate)
        var checkDate = today
        val planRecords = item.currentPlan.plan_records.orEmpty().toMutableList()
        planRecords.add(0, createPlanRecord(date, item.isCompleted.not()))
        val updatedItem = item.currentPlan.copy(plan_records = planRecords)
        updatePlanItem(
            mapToDoToPlanItem(
                updatedItem,
                state.isDailyView,
                item.groupCount,
                item.todos,
                item.todos,
                date,
                item.isProgramActivity
            ), position, parentPosition
        )

        viewModelScope.launch {
            planManager.updatePlanToggle(
                item.id, date, item.isCompleted.not(), item.currentPlan.program_days?.firstOrNull()
            ).onSuccess {
                planManager.updatePlanCachedData()
                planManager.todayPlanItemCheckChange()
            }.onError {}
            if (updatedItem.program_id != null && checkDate?.isSameDay(today) == true) checkAndShowCompletedProgram(
                updatedItem.program_id, includeLesson = true
            )
            markPlanItemInAProgramAsCompleted(updatedItem)
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_CHECKED)
                .addProperty(TrackingConstants.KEY_ID, item.id.toString())
                .addProperty(TrackingConstants.TYPE, item.type.title)
                .addProperty(TrackingConstants.KEY_LOCATION, "Plan Page")
                .addProperty("program_id", item.currentPlan.program_id.toString()).withKlaviyo()
                .withFirebase().withAmplitude().create()
        )
    }

    private fun createPlanRecord(date: String, completed: Boolean) = PlanRecord(
        date = date, completed = completed
    )

    private fun updateWeeklyItemToggle(
        position: Int, parentPosition: Int
    ) {
        val items = state.planItems.value?.first.orEmpty().toMutableList()
        val parentItem = items[parentPosition]
        val planItems = parentItem.items.toMutableList()
        val selectedPlan = planItems[position]
        val today = DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())!!
        selectedPlan.todos.forEachIndexed { index, todoItem ->
            val matchingRecords = todoItem.plan_records.orEmpty()
                .firstOrNull { DAY_DATE_FORMAT.parse(it.date).isSameDay(today) }
            if (matchingRecords == null || matchingRecords.completed != true) {
                updatePlanWeeklyItemToggle(
                    selectedPlan, todoItem, position, parentPosition, index, today.formatDayDate()
                )
                planManager.todayPlanItemCheckChange()
                return
            }
        }
    }

    private fun updatePlanWeeklyItemToggle(
        selectedPlan: PlanItem,
        item: PlanData,
        position: Int,
        parentPosition: Int,
        internalIndex: Int,
        date: String
    ) {

        val items = state.planItems.value?.first.orEmpty().toMutableList()
        val parentItem = items[parentPosition]

        val planRecords = item.plan_records.orEmpty().toMutableList()
        planRecords.add(0, createPlanRecord(date, true))
        val updatedItem = item.copy(plan_records = planRecords)
        val todos = selectedPlan.todos.toMutableList()
        todos[internalIndex] = updatedItem
        val updatedPlan = selectedPlan.copy(todos = todos,
            isCompleted = todos.sumBy { it.getNonCompletedDays().size } == 0,
            groupCount = todos.sumBy { it.getNonCompletedDays().size })

        val weeklyItems = parentItem.items.toMutableList()
        weeklyItems[position] = updatedPlan

        val updatedWeekItem = parentItem.copy(
            currentStep = getWeeklyCompletedItems(weeklyItems),
            stepsCount = getWeeklyItemsCount(weeklyItems),
            title = parentItem.title,
            items = weeklyItems
        )

        items[parentPosition] = updatedWeekItem

        loadPlan(items, false)

        viewModelScope.launch {
            planManager.updatePlanToggle(item.id ?: return@launch, date, true).onSuccess {
                planManager.updatePlanCachedData()
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_CHECKED)
                        .addProperty(TrackingConstants.KEY_ID, item.id.toString())
                        .addProperty(TrackingConstants.TYPE, item.getPlanType().title)
                        .addProperty(TrackingConstants.KEY_LOCATION, "Plan Page")
                        .addProperty("program_id", item.program_id.toString()).withKlaviyo()
                        .withFirebase().withAmplitude().create()
                )
//                hideLoading(false)
            }.onError {
//                hideLoading(false)
                // todo handle error case
            }
            markPlanItemInAProgramAsCompleted(updatedItem)
        }
    }

    private fun getWeeklyItemsCount(weeklyItems: MutableList<PlanItem>): Int {
        var items = 0
        weeklyItems.forEach {
            items += it.todos.sumBy {
                it.getDaysOfWeek().size
            }
        }
        return items
    }

    private fun getWeeklyCompletedItems(weeklyItems: MutableList<PlanItem>): Int {
        var completedItems = 0

        weeklyItems.forEach {

            it.todos.forEach { plan ->
                val daysOfWeek = plan.getDaysOfWeek()
                daysOfWeek.forEach {
                    val day = DAY_DATE_FORMAT.format(it.toInt().toCurrentWeekDate())
                    val completedRecord =
                        plan.plan_records.orEmpty().firstOrNull { it.date == day }?.completed
                            ?: false
                    if (completedRecord) {
                        completedItems += 1
                    }
                }
            }
        }
        return completedItems
    }

    private fun updatePlanItem(planItem: PlanItem, position: Int, parentPosition: Int) {
        Log.d(TAG, "updatePlanItem: $planItem")
        if (planItem.isProgramActivity == true) {
            val items = state.todayActivitiesItems.value?.toMutableList() ?: arrayListOf()
            val programPlans = items.firstOrNull {
                it.items?.firstOrNull()?.program?.id == planItem.program?.id
            }
            val plans = programPlans?.items?.toMutableList() ?: arrayListOf()
            plans[position] = planItem
            programPlans?.items = plans
            programPlans?.let {
                items[parentPosition] = it
            }
            state.todayActivitiesItems.value = items
        } else {
            val items = state.dayPlanItems.value?.toMutableList() ?: arrayListOf()
            val parentItem = items[parentPosition]
            val planItems = parentItem.items.toMutableList()
            planItems[position] = planItem
            val updatedParentItem = parentItem.copy(
                currentStep = planItems.count { it.isCompleted },
                stepsCount = planItems.size,
                title = parentItem.title,
                items = planItems
            )
            items[parentPosition] = updatedParentItem
            if (planItems.isEmpty()) {
                items.removeAt(parentPosition)
                state.deletePlanItem.value = parentPosition
            }
            state.dayPlanItems.value = items
        }
    }

    override fun onAddReminderClicked(item: Any, view: View, parentPosition: Int) {
        if (selectedDate != today) {
            showError("Previous Date !")
            return
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_PLAN_REMINDER_TAPPED).withKlaviyo()
                .withFirebase().withAmplitude().create()
        )
        if (item is PlanItem) {
            openReminderDialog(view, item)
        }
    }

    override fun onReminderNotificationClicked(
        item: Any, view: View, position: Int, parentPosition: Int
    ) {
        if (selectedDate != today) {
            showError("Previous Date !")
            return
        }
        if (item is PlanItem) {
            showLoading()
            viewModelScope.launch {
                val updatedPlan = item.currentPlan.copy(
                    notification_enabled = item.currentPlan.notification_enabled?.not() ?: false
                )
                planManager.updatePlanItem(
                    updatedPlan.id ?: return@launch, updatedPlan.toPlanRequest()
                )
                updatePlanItem(item.copy(currentPlan = updatedPlan), position, parentPosition)
                loadUserPlans(true)
            }
        }
    }

    override fun onItemDeleteClicked(item: Any, view: View, position: Int, parentPosition: Int) {
        if (item is PlanItem) {
            if (item.isGrouped && !item.plans.isNullOrEmpty()) {
                state.removeReminder.value = Triple(item, parentPosition, position)
            } else {
                deletePlanItems(item, parentPosition, position)
            }
        }
    }

    fun deletePlanItems(item: Any, parentPosition: Int, position: Int) {
        showLoading()
        viewModelScope.launch {
            if (item is PlanItem) {
                val result = if (item.isGrouped) {
                    planManager.deletePlanItem(item.todos.map { it.id ?: -1 })
                } else {
                    planManager.deletePlanItem(listOf(item.currentPlan.id ?: -1))
                }
                result.onSuccess {
                    planManager.todayPlanItemCheckChange()
                    val items = state.planItems.value?.first.orEmpty().toMutableList()
                    val parentItem = items[parentPosition]
                    val todos = parentItem.items.toMutableList()
                    todos.removeAt(position)

                    val updatedItem = if (item.isGrouped) {
                        parentItem.copy(
                            currentStep = getWeeklyCompletedItems(todos),
                            stepsCount = getWeeklyItemsCount(todos),
                            items = todos
                        )
                    } else {
                        parentItem.copy(
                            currentStep = parentItem.currentStep ?: 1 - 1,
                            stepsCount = parentItem.stepsCount ?: 1 - 1,
                            items = todos
                        )
                    }

                    items[parentPosition] = updatedItem
                    if (updatedItem.items.isEmpty()) {
                        items.removeAt(parentPosition)
                        state.deletePlanItem.value = parentPosition
                    }
                    loadPlan(items, false)
                    loadUserPlans(true)
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED).addProperty(
                            TrackingConstants.KEY_ID, item.todos.map { it.id ?: -1 }.toString()
                        ).addProperty(
                            TrackingConstants.TYPE, item.currentPlan.getPlanType().title
                        ).addProperty(TrackingConstants.KEY_LOCATION, "Plan Page").withKlaviyo()
                            .withFirebase().withAmplitude().create()
                    )
                }.onError {
                    hideLoading(false)
                    showError(getResString(R.string.unknown_error))
                }
                markPlanItemInAProgramAsCompleted(item.currentPlan)
            }
        }
    }

    private suspend fun markPlanItemInAProgramAsCompleted(updatedItem: PlanData) {
        programManager.markPlanRecordAsCompleted(updatedItem).onServiceError {
            showError(it.error.message ?: getResString(R.string.unknown_error))
        }.onNetworkIOError {
            showError(getResString(R.string.network_error))
        }.onUnknownError {
            showError(getResString(R.string.unknown_error))
        }
    }

    override fun onItemEditClicked(item: Any, view: View, parentPosition: Int) {
        if (selectedDate != today) {
            showError("Previous Date !")
            return
        }
        if (item is PlanItem) {
            openReminderDialog(view, item)
        }
    }

    override fun onGroupedItemsClicked(item: Any, view: View, parentPosition: Int) {
    }

    private fun openReminderDialog(
        view: View, item: PlanItem
    ) {
        view.findNavController().navigate(
            HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToPlanReminderDialog(
                PlanReminderHeader(
                    title = item.title,
                    description = item.valueProp1.orEmpty(),
                    backgroundUrl = item.backgroundUrl,
                    background = item.background,
                    todos = item.plans,
                    isTestingPlan = item.type == PlanType.TestPlan
                )
            )
        )
    }

    private fun mapToDoToPlanItem(
        todo: PlanData,
        isDailyView: Boolean,
        groupedItemsCount: Int,
        todos: List<PlanData>,
        plans: List<PlanData>,
        date: String? = null,
        isProgramActivity: Boolean? = false
    ): PlanItem {
        val isCompleted = todo.plan_records?.firstOrNull { it.date == date }?.completed == true
        return PlanItem(
            todo.id ?: -1,
            todo.getDisplayName(),
            todo.getDescription(),
            getRemindersTitle(todo, isDailyView),
            todo.getToDoImageUrl(),
            todo.getToDoImageBackground(),
            todo.getDisplayName(),
            isCompleted,
            !isDailyView,
            groupedItemsCount,
            todos,
            plans,
            todo.getPlanType(),
            todo.day_of_week,
            todo.program_dates?.map { DAY_DATE_FORMAT.parse(it) },
            program = todo.program?.let { programMapper.mapProgram(it) },
            todo,
            isProgramActivity = isProgramActivity
        )
    }

    override fun onClosePlanClicked(view: View) {
        preferencesRepository.emptyPlanRecommendationItemDismissed = true
    }

    fun onDailyFilterSelected(updateSelectedTab: Boolean) {
        if (updateSelectedTab) {
            state.isDailyView = true
            preferencesRepository.isDailyPlanView = true
            loadPlan(state.todayTodosData, true)
        } else if (preferencesRepository.isDailyPlanView) {
            loadPlan(state.todayTodosData, true)
        }
    }

    private fun getRemindersTitle(todo: PlanData?, isDailyView: Boolean): String {
        return if (isDailyView) {
            todo?.getTimeFormattedWithMeridian() ?: getResString(R.string.set_time)
        } else {
            if (todo?.day_of_week.isNullOrEmpty()) getResString(R.string.set_time) else getResString(
                R.string.reminders
            )
        }
    }

    override fun onAddPlanFooterClicked(item: Any, view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.ADD_TO_YOUR_PLAN_TAPPED).withKlaviyo()
                .withFirebase().withAmplitude().create()
        )
        view.findNavController().navigate(HomeScreenFragmentDirections.globalActionToBuildPlan())
    }

    override fun onAnimationFinished(item: Any, view: View, parentPosition: Int) {
        val items = if (state.isDailyView) {
            state.todayTodosData
        } else {
            state.weeklyTodosData
        }
        loadPlan(items, false)
    }

    override fun onLessonInformationClicked() {
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalLessonDialogFragment()
    }

    override fun onAddOtherGoal() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.ADD_TO_YOUR_PLAN_TAPPED).withKlaviyo()
                .withFirebase().withAmplitude().create()
        )
        state.navigateTo.value = HomeScreenFragmentDirections.globalActionToBuildPlan()
    }

    fun resetLessonFetchCount() {
        lessonFetchMap.clear()
    }

    override fun onUnlockLesson(item: Any, programId: Int?, date: Date) {
        if (item is Lesson) {
            trackingLesson(TrackingConstants.LESSON_UNLOCK_TAPPED, item)
            programId?.let {
                val currentDayDate = today.getDateDayDiff(date) + 1
                if (currentDayDate >= MAX_LESSON_DAYS) {
                    processLessDone(programId)
                    return
                }
                fetchNextLessons(programId, currentDayDate)
            }
        }
    }

    private fun processLessDone(programId: Int) {
        state {
            lessonItems.value = lessonItems.value.orEmpty().mapIndexed { index, lessonListItem ->
                if (lessonListItem.items?.firstOrNull {
                        it.programId == programId
                    } != null) {
                    lessonListItem.copy(noMoreLesson = true)
                } else {
                    lessonListItem.copy()
                }
            }
        }
    }

    private fun processProgramDone(programId: Int) {
        state {
            todayActivitiesItems.value =
                todayActivitiesItems.value.orEmpty().mapIndexed { index, planListItem ->
                    if (planListItem.items?.firstOrNull {
                            it.currentPlan.program_id == programId
                        } != null) {
                        planListItem.copy(noMoreActivity = true)
                    } else {
                        planListItem.copy()
                    }
                }
        }
    }

    override fun onProgramInformationClicked() {
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalActivityDialogFragment()
    }

    override fun onProgramItemClicked(item: Program) {
    }

    override fun onProgramInfoClicked(item: Program) {
        state.navigateTo.value =
            HomeScreenFragmentDirections.actionHomeScreenFragmentToProgramInfoDialogFragment(
                item
            )
    }

    fun getTitle(): CharSequence {
        val contact = preferencesRepository.getContact()?.firstName
        return "Hi $contact!"
    }

    companion object {
        const val TAG = "TodayViewModel"
        const val MAX_LESSON_DAYS = 3
        const val MAX_ACTIVITIES_DAYS = 3
    }

    private fun fetchPreviousCompletedLessons(program: Program, date: Date) {
        showLoading()
        viewModelScope.launch {
            lessonManager.fetchAllLessons(
                contactId = preferencesRepository.contactId,
                programId = program.id,
                isToday = false,
                showAll = false,
                fetchAll = true,
                date = date.formatLessonDayDate()
            ).onSuccess { it ->
                Log.d(TAG, "fetchAllCompletedLessons: $it")
                hideLoading()
                val completedLessons = it.lessons?.map { lesson ->
                    lesson.copy(
                        programId = program.id, programName = program.title, completed = true
                    )
                } ?: arrayListOf()

                val lessonListItem = LessonListItem(
                    completedLessons,
                    program.title,
                    preferencesRepository.getUserMainGoal()?.name,
                    noMoreLesson = true,
                    isPreviousDay = true,
                    date = date
                )
                lessons.add(lessonListItem)
                if (lessons.size == enrolledPrograms.size) {
                    processAllLessonResponse()
                }
            }.onServiceError {
                showError(it.error.message ?: getResString(R.string.unknown_error))
            }.onNetworkIOError {
                showError(getResString(R.string.network_error))
            }.onUnknownError {
                showError(getResString(R.string.unknown_error))
            }
        }
    }

    private fun fetchCompletedLessons(program: Program) {
        showLoading()
        viewModelScope.launch {
            lessonManager.fetchAllLessons(
                contactId = preferencesRepository.contactId,
                programId = program.id,
                isToday = false,
                showAll = false,
                fetchAll = true,
                date = today.formatLessonDayDate()
            ).onSuccess { it ->
                Log.d(TAG, "fetchAllCompletedLessons: $it")
                hideLoading()
                val completedLessons = it.lessons?.map { lesson ->
                    lesson.copy(
                        programId = program.id, programName = program.title, completed = true
                    )
                } ?: arrayListOf()
                if (completedLessons.isNullOrEmpty() || preferencesRepository.unlockedLessons.contains(
                        program.id.toString()
                    )
                ) {
                    fetchNewTodayLessons(program, completedLessons)
                } else {
                    val lessonListItem = LessonListItem(
                        completedLessons.mapIndexed { index, lesson ->
                            lesson.copy(isUnlockedLesson = index != 0)
                        },
                        program.title,
                        preferencesRepository.getUserMainGoal()?.name,
                        noMoreLesson = completedLessons.size > MAX_LESSON_DAYS,
                        date = today
                    )
                    lessons.add(lessonListItem)
                    if (lessons.size == enrolledPrograms.size) {
                        processAllLessonResponse()
                    }
                }
            }.onServiceError {
                showError(it.error.message ?: getResString(R.string.unknown_error))
            }.onNetworkIOError {
                showError(getResString(R.string.network_error))
            }.onUnknownError {
                showError(getResString(R.string.unknown_error))
            }
        }
    }

    private fun fetchNewTodayLessons(program: Program, completedLessons: List<Lesson>?) {
        showLoading()
        viewModelScope.launch {
            lessonManager.fetchAllLessons(
                contactId = preferencesRepository.contactId,
                programId = program.id,
                isToday = true,
                showAll = false,
                fetchAll = true
            ).onSuccess { it ->
                Log.d(TAG, "fetchNewTodayLessons: $it")
                hideLoading()
                val newLessons = it.lessons?.map { lesson ->
                    lesson.copy(
                        programId = program.id, programName = program.title, completed = false
                    )
                } ?: arrayListOf()
                val allLessons = arrayListOf<Lesson>()
                allLessons.addAll(completedLessons ?: arrayListOf())
                allLessons.addAll(newLessons)
                val lessonListItem = LessonListItem(
                    allLessons,
                    program.title,
                    preferencesRepository.getUserMainGoal()?.name,
                    noMoreLesson = allLessons.isNullOrEmpty(),
                    date = today
                )
                lessons.add(lessonListItem)
                if (lessons.size == enrolledPrograms.size) {
                    processAllLessonResponse()
                }
            }.onServiceError {
                showError(it.error.message ?: getResString(R.string.unknown_error))
            }.onNetworkIOError {
                showError(getResString(R.string.network_error))
            }.onUnknownError {
                showError(getResString(R.string.unknown_error))
            }
        }
    }

    private fun processAllLessonResponse() {
        lessons.map {
            it.items?.map { lesson ->
                if (preferencesRepository.servedLessons.contains(lesson.id.toString()).not()) {
                    trackingLesson(TrackingConstants.LESSON_SERVED, lesson)
                    preferencesRepository.updateServedLessons(lesson.id.toString())
                }
            }
        }
        state.lessonItems.postValue(lessons.sortedBy {
            it.programTitle
        })
        Constants.onLessonCompleted = { programId, lessonId ->
            checkAndShowCompletedProgram(programId, lessonId = lessonId, true)
        }
    }

    fun checkCompletedToday() {
        for (pro in enrolledPrograms) {
            checkAndShowCompletedProgram(pro.id)
        }
    }

    private fun processAllProgramPlansResponse() {
        programPlans.map {
            it.items?.map { plan ->
                trackingPlan(plan)
            }
        }

        state.todayActivitiesItems.postValue(programPlans.sortedBy {
            it.programTitle
        })
    }

    private fun trackingPlan(plan: PlanItem) {
        if (preferencesRepository.servedActivities.contains(plan.id.toString()).not()) {
            if (plan.isProgramActivity == true) trackingProgramActivity(
                TrackingConstants.ACTIVITY_SERVED, plan
            )
            else trackingWellnessActivity(TrackingConstants.WELLNESS_ACTIVITY_SERVED, plan)
            preferencesRepository.updateServedActivities(plan.id.toString())
        }
    }

    private fun fetchNextLessons(programId: Int, currentDayDate: Int) {
        val date = if (currentDayDate == 0) today else today.getDateAfter(currentDayDate)
        val isToday = currentDayDate == 0
        showLoading()
        viewModelScope.launch {
            lessonManager.fetchAllLessons(
                contactId = preferencesRepository.contactId,
                programId = programId,
                isToday = true,
                showAll = false,
                fetchAll = true,
                date = date.formatLessonDayDate()
            ).onSuccess { it ->
                Log.d(TAG, "fetchNextLessons: $it")
                processLessonResponse(it, programId, currentDayDate, date)
                hideLoading()
            }.onServiceError {
                showError(it.error.message ?: getResString(R.string.unknown_error))
            }.onNetworkIOError {
                showError(getResString(R.string.network_error))
            }.onUnknownError {
                showError(getResString(R.string.unknown_error))
            }
        }
    }

    private fun processLessonResponse(
        response: LessonResponse, programId: Int, currentDayDate: Int = 0, date: Date
    ) {
        val newLessons = response.lessons
        if (newLessons.isNullOrEmpty()) {
            preferencesRepository.removeUnlockedLesson(programId.toString())
            if (currentDayDate < MAX_LESSON_DAYS) {
                fetchNextLessons(programId, currentDayDate + 1)
            } else {
                state {
                    lessonItems.value =
                        lessonItems.value.orEmpty().mapIndexed { index, lessonListItem ->
                            if (lessonListItem.items?.firstOrNull {
                                    it.programId == programId
                                } != null) {
                                lessonListItem.copy(noMoreLesson = true, date = date)
                            } else {
                                lessonListItem.copy()
                            }
                        }
                }
            }
        } else {
            preferencesRepository.updateUnlockedLesson(programId.toString())
            state {
                lessonItems.value =
                    lessonItems.value.orEmpty().mapIndexed { index, lessonListItem ->
                        if (lessonListItem.items?.firstOrNull {
                                it.programId == programId
                            } != null) {
                            val currentItems = lessonListItem.items
                            val newLessonList: ArrayList<Lesson> = arrayListOf()
                            currentItems?.let { newLessonList.addAll(it) }
                            if (currentItems?.containsAll(newLessons) == false) {
                                newLessons.let { newLessonList.addAll(it) }
                            }
                            lessonListItem.copy(noMoreLesson = currentItems?.containsAll(newLessons) == true,
                                items = newLessonList.map { lesson ->
                                    lesson.copy(
                                        programId = programId,
                                        isUnlockedLesson = true,
                                        programName = getProgramName(programId)
                                    )
                                })
                        } else {
                            lessonListItem.copy()
                        }
                    }
            }
        }
    }

    private fun processNextPlanResponse(
        newPlans: List<PlanItem>, program: Program, currentDayDate: Int = 0, date: Date
    ) {
        if (newPlans.isNullOrEmpty()) {
            if (currentDayDate < MAX_LESSON_DAYS) {
                fetchNextPlans(program, currentDayDate + 1)
            } else {
                state {
                    todayActivitiesItems.value =
                        todayActivitiesItems.value.orEmpty().mapIndexed { index, item ->
                            if (item.items?.firstOrNull {
                                    it.currentPlan.program_id == program.id
                                } != null) {
                                item.copy(noMoreActivity = true, date = date)
                            } else {
                                item.copy()
                            }
                        }
                }
            }
        } else {
            state {
                todayActivitiesItems.value =
                    todayActivitiesItems.value.orEmpty().mapIndexed { index, item ->
                        if (item.items?.firstOrNull {
                                it.currentPlan.program_id == program.id
                            } != null) {
                            val currentItems = item.items
                            val newPlanList: ArrayList<PlanItem> = arrayListOf()
                            currentItems?.let { newPlanList.addAll(it) }
                            newPlanList.addAll(newPlans)
                            item.copy(
                                noMoreActivity = currentDayDate >= MAX_ACTIVITIES_DAYS,
                                items = newPlanList,
                                date = date
                            )
                        } else {
                            item.copy()
                        }
                    }
            }
        }
        preferencesRepository.updateUnlockedProgram(program.id, currentDayDate)
    }

    override fun onLessonCompleted(item: Any, view: View) {
        Log.d(TAG, "onLessonCompleted: $item")
    }

    override fun onLessonClicked(item: Any, view: View) {
        Log.d(TAG, "onLessonClicked: $item")
        if (item is Lesson) {
            state.keepDateState.value = selectedDate?.isSameDay(today) == false
            trackingLesson(TrackingConstants.LESSON_STARTED, item)
            state.navigateTo.value = HomeNavGraphDirections.actionGlobalLessonDetailsFragment(item)
        }
    }

    fun trackingLesson(event: String, lesson: Lesson) {
        trackingManager.log(
            TrackedEvent.Builder(event).addProperty("goal", getGoalName())
                .addProperty("program name", lesson.programName.toString())
                .addProperty("lesson name", lesson.title.toString()).addProperty("time", getTime())
                .setEmail(preferencesRepository.getContact()?.email ?: "").withKlaviyo()
                .withFirebase().withAmplitude().create()
        )
    }

    fun trackingProgramActivity(event: String, planItem: PlanItem) {
        trackingManager.log(
            TrackedEvent.Builder(event).addProperty("goal", getGoalName())
                .addProperty("program name", planItem.currentPlan.getDisplayName())
                .addProperty("activity name", planItem.title).addProperty("time", getTime())
                .setEmail(preferencesRepository.getContact()?.email ?: "").withKlaviyo()
                .withFirebase().withAmplitude().create()
        )
    }

    fun trackingWellnessActivity(event: String, planItem: PlanItem) {
        trackingManager.log(
            TrackedEvent.Builder(event)
                .addProperty("program name", planItem.currentPlan.getDisplayName())
                .addProperty("activity name", planItem.title).addProperty("time", getTime())
                .setEmail(preferencesRepository.getContact()?.email ?: "").withKlaviyo()
                .withFirebase().withAmplitude().create()
        )
    }

    fun trackingEssential(event: String, essential: Essential) {
        trackingManager.log(
            TrackedEvent.Builder(event).addProperty("goal", getGoalName())
                .addProperty("program name", essential.programTitle.toString())
                .addProperty("essentials name", essential.title.toString())
                .addProperty("time", getTime())
                .setEmail(preferencesRepository.getContact()?.email ?: "").withKlaviyo()
                .withFirebase().withAmplitude().create()
        )
    }

    fun trackingCompletedProgram(event: String, program: Program) {
        trackingManager.log(
            TrackedEvent.Builder(event).addProperty("goal", getGoalName())
                .addProperty("program name", program.title.toString())
                .addProperty("time", getTime())
                .setEmail(preferencesRepository.getContact()?.email ?: "").withKlaviyo()
                .withFirebase().withAmplitude().create()
        )
    }

    private fun buildProgramActivities(
        groupedFoodItems: Map<Int?, List<PlanData>>,
        groupedLifeStyleItemsItems: Map<Int?, List<PlanData>>,
        groupedSupplementItems: List<PlanData>,
        groupedTips: Map<Int?, List<PlanData>>,
        today: Date,
        program: Program
    ): List<PlanItem> {
        val dailyItems = ArrayList<PlanItem>()
        groupedFoodItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it, grouped, today.formatDayDate()
                    )
                )
            }
        }
        groupedTips.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it, grouped, today.formatDayDate()
                    )
                )
            }
        }
        groupedLifeStyleItemsItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it, grouped, today.formatDayDate()
                    )
                )
            }
        }
        groupedSupplementItems.map { item ->
            dailyItems.add(
                mapTodoToPlanItem(
                    item, groupedSupplementItems, today.formatDayDate()
                )
            )
        }

        val filteredTodayActivities = dailyItems.filter { planItem ->
            planItem.currentPlan.program_id != null && program.id == planItem.currentPlan.program_id
        }

        return toggleProgramPlanItems(
            filteredTodayActivities, today, true
        )
    }

    private fun mapTodoToPlanItem(
        todo: PlanData, plans: List<PlanData>, date: String? = null
    ): PlanItem {
        val isCompleted = todo.plan_records?.firstOrNull { it.date == date }?.completed == true

        return PlanItem(
            todo.id ?: -1,
            todo.getDisplayName(),
            todo.getDescription(),
            todo.getTimeFormattedWithMeridian() ?: getResString(R.string.set_time),
            todo.getToDoImageUrl(),
            todo.getToDoImageBackground(),
            todo.getDisplayName(),
            isCompleted,
            false,
            1,
            plans,
            plans,
            todo.getPlanType(),
            todo.day_of_week,
            todo.program_dates?.map { DAY_DATE_FORMAT.parse(it) },
            program = todo.program?.let { programMapper.mapProgram(it) },
            todo
        )
    }

    private fun getGoalName(): String {
        return preferencesRepository.getUserMainGoal()?.name.toString()
    }

    private fun getProgramName(programId: Int?): String {
        return enrolledPrograms.firstOrNull { it.id == programId }?.title.toString()
    }

    private fun getTime(): String {
        return selectedDate?.formatDayDate().toString()
    }

    override fun onProgramActivityCompleted(item: PlanItem?, view: View) {
        item?.let { plan ->
            val program = enrolledPrograms.firstOrNull {
                it.id == plan.currentPlan.program_id
            }
            showCompletedProgram(program)
        }
    }

    override fun onWellnessActivityInformationClicked() {
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalWellnessActivityDialogFragment()
    }

    override fun onReactItemClicked(item: Any, view: View, parentPosition: Int) {
        val plan = item as PlanItem
        if (plan.isProgramActivity == true) trackingProgramActivity(
            TrackingConstants.ACTIVITY_RATING, plan
        )
        else trackingWellnessActivity(TrackingConstants.WELLNESS_ACTIVITY_RATING, plan)
        if (!plan.isCompleted) return
        val category = when (item.type) {
            PlanType.StressPlan -> {
                LikeCategory.Lifestyle.value
            }
            PlanType.FoodPlan -> {
                LikeCategory.Food.value
            }
            PlanType.TIP -> {
                LikeCategory.Tip.value
            }
            else -> LikeCategory.Program.value
        }
        val recordId = when (item.type) {
            PlanType.StressPlan -> {
                plan.currentPlan.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
            }
            PlanType.FoodPlan -> {
                plan.currentPlan.food_id
            }
            PlanType.TIP -> {
                plan.currentPlan.tip_id
            }
            else -> plan.currentPlan.id
        }
        val reviewItem = ActivityReviewItem()
        reviewItem.category = category
        reviewItem.record_id = recordId
        reviewItem.likeStatus = plan.likeStatus
        reviewItem.image = plan.backgroundUrl
        reviewItem.title = plan.title
        reviewItem.programId = plan.id
        state.navigateTo.value =
            HomeNavGraphDirections.actionGlobalActivityReviewFragment(reviewItem)
    }

    fun showCompletedProgram(program: Program?) {
        program?.let {
            state.navigateTo.value =
                HomeNavGraphDirections.actionGlobalCompleteProgramFragment(program = it)
        }
    }

    override fun onGetNextPlanClicked(item: PlanItem?, date: Date) {
        item?.let {
            if (it.isProgramActivity == true) trackingProgramActivity(
                TrackingConstants.ACTIVITY_UNLOCK_TAPPED, it
            )
        }
        val program = item?.program
        program?.let { pr ->
            val currentDayDate = preferencesRepository.getUnlockedPrograms().firstOrNull {
                it.id == pr.id
            }?.count ?: 0
            Log.d(TAG, "onGetNextPlanClicked: $currentDayDate , ${pr.id}")
            if (currentDayDate + 1 > MAX_ACTIVITIES_DAYS) {
                processProgramDone(pr.id)
                return
            }
            fetchNextPlans(program, currentDayDate + 1)
        }
    }

    private fun fetchNextPlans(program: Program, currentDayDate: Int) {
        val date = if (currentDayDate == 0) today else today.getDateAfter(currentDayDate)
        Log.d(TAG, "fetchNextPlans: $date")
        showLoading()
        viewModelScope.launch {
            planManager.getNextUserPlans(
                forceUpdate = true,
                programId = program.id,
                contactId = preferencesRepository.contactId
            ).onSuccess { plan ->
                val items = listOf(plan)
                val groupedFoodItems = items.filter {
                    it.food_id != null
                }.groupBy {
                    it.food_id
                }

                val groupedLifeStyleItemsItems = items.filter {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null
                }.groupBy {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                }

                val groupedSupplementItems = items.filter {
                    it.isSupplementPlan()
                }

                val groupedTips = items.filter {
                    it.isTip()
                }.groupBy {
                    it.tip_id
                }
                val nextPlans = buildProgramActivities(
                    groupedFoodItems,
                    groupedLifeStyleItemsItems,
                    groupedSupplementItems,
                    groupedTips,
                    date,
                    program
                )
                processNextPlanResponse(nextPlans, program, currentDayDate, date)
                hideLoading()
            }.onError {}
        }
    }

    private fun fetchWellnessActivities(
        showLoading: Boolean = true, forceUpdate: Boolean = false, date: Date
    ) {
        showLoading()
        var shouldSentToday = false
        if (date.isSameDay(today)) {
           shouldSentToday = !preferencesRepository.isTodayDateSent(today.formatDayDate())
        } else {
            shouldSentToday = false
        }

        viewModelScope.launch(Dispatchers.IO, CoroutineStart.ATOMIC) {
            planManager.getUserPlans(forceUpdate, shouldSentToday, date.formatDayDate())
                .onSuccess { items ->
                    preferencesRepository.setSendDate(today.formatDayDate())
                    val groupedFoodItems = items.filter {
                        it.food_id != null
                    }.groupBy {
                        it.food_id
                    }

                    val groupedLifeStyleItemsItems = items.filter {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null
                    }.groupBy {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                    }

                    val groupedSupplementItems = items.filter {
                        it.isSupplementPlan()
                    }

                    val groupedTips = items.filter {
                        it.isTip()
                    }.groupBy {
                        it.tip_id
                    }
                    val plans = buildTodayActivities(
                        groupedFoodItems,
                        groupedLifeStyleItemsItems,
                        groupedSupplementItems,
                        groupedTips,
                        date
                    )
                    plans.map {
                        trackingPlan(it)
                    }
                    setupDailyData(
                        groupedFoodItems,
                        groupedLifeStyleItemsItems,
                        groupedSupplementItems,
                        groupedTips
                    )
                    state.dayPlanItems.postValue(
                        listOf(
                            DayPlanItem(
                                0,
                                items = plans,
                                date = date,
                                isTested = preferencesRepository.afterFirstTest(),
                                programTitle = preferencesRepository.getUserMainGoal()?.name,
                                stepsCount = 0,
                                title = "",
                                isPreviousDay = date.isSameDay(today).not()
                            )
                        )
                    )
                    hideLoading(false)
                }.onError {
                    hideLoading(false)
                }
        }
    }

    private fun buildTodayActivities(
        groupedFoodItems: Map<Int?, List<PlanData>>,
        groupedLifeStyleItemsItems: Map<Int?, List<PlanData>>,
        groupedSupplementItems: List<PlanData>,
        groupedTips: Map<Int?, List<PlanData>>,
        today: Date
    ): List<PlanItem> {
        val dailyItems = mutableListOf<PlanItem>()
        groupedFoodItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it, grouped, today.formatDayDate()
                    )
                )
            }
        }
        groupedTips.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it, grouped, today.formatDayDate()
                    )
                )
            }
        }
        groupedLifeStyleItemsItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it, grouped, today.formatDayDate()
                    )
                )
            }
        }
        groupedSupplementItems.map { item ->
            dailyItems.add(
                mapTodoToPlanItem(
                    item, groupedSupplementItems, today.formatDayDate()
                )
            )
        }

        val filteredTodayActivities = dailyItems.filter { planItem ->
            planItem.currentPlan.program_id == null
        }

        val planItems = togglePlanItems(
            filteredTodayActivities.filter { it.currentPlan.program_id == null }, today
        ).sortedBy {
            it.currentPlan.getTimeFormattedAsNumber()
        }
        return planItems
    }

    override fun onTakeATest() {
        state.navigateTo.value = HomeNavGraphDirections.globalActionToPreTest()
    }

    fun navigateToProgramInformation() {
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalProgramInfoFragment()
    }

    private fun checkAndShowCompletedProgram(
        programId: Int, lessonId: Long? = 0, includeLesson: Boolean? = false
    ) {
        val isAlreadyShow = preferencesRepository.showCompleteDays.contains(today.formatDayDate())
        if (selectedDate?.isSameDay(today) == true && isAlreadyShow.not()) {
            var isAllComplete = true
            for (item in state.todayActivitiesItems.value ?: arrayListOf()) {
                if (!item.isFinishAll()) isAllComplete = false
            }
            if (includeLesson == true) {
                for (item in state.lessonItems.value ?: arrayListOf()) {
                    if (!item.isFinishAllExcludeLesson(lessonId ?: 0)) isAllComplete = false
                }
            }
            if (isAllComplete) {
                preferencesRepository.updateShowCompleteForDay(today.formatDayDate())
                state.navigateTo.value = enrolledPrograms.firstOrNull { it.id == programId }?.let {
                    trackingCompletedProgram(TrackingConstants.PROGRAM_COMPLETED, it)
                    HomeNavGraphDirections.actionGlobalCompleteProgramFragment(
                        it
                    )
                }
            }
        }
    }

    fun reloadData() {
        selectedDate?.let {
            fetchData()
        }
        state.readyToPopulate.call()
    }

    fun resetDateList() {
        if (state.keepDateState.value == false) state.dateList.value = arrayListOf()
    }
}
