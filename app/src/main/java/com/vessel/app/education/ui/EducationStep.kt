package com.vessel.app.education.ui

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class EducationStep(
    @DrawableRes val image: Int,
    @StringRes val body: Int
)
