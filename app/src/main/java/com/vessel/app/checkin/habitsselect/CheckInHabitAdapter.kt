package com.vessel.app.checkin.habitsselect

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.onboarding.badhabitsselection.BadHabitSelect

class CheckInHabitAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<BadHabitSelect>() {
    override fun getLayout(position: Int) = R.layout.item_checkin_habit

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onHabitSelectClicked(item: BadHabitSelect)
    }
}
