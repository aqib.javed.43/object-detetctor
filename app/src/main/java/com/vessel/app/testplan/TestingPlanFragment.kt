package com.vessel.app.testplan

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.adapter.planedit.PlanEditAdapter
import com.vessel.app.common.adapter.planedit.PlanEditItem
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TestingPlanFragment : BaseFragment<TestingPlanViewModel>() {
    override val viewModel: TestingPlanViewModel by viewModels()
    override val layoutResId = R.layout.fragment_test_plan

    private val testingPlanHeaderAdapter by lazy { TestingPlanHeaderAdapter(viewModel) }
    private val reminderRowAdapter by lazy { ReminderRowAdapter(viewModel) }
    private val planEditAdapter by lazy { PlanEditAdapter(viewModel) }
    private val hintAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_testing_plan_hint
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        view?.findViewById<RecyclerView>(R.id.testPlanRecyclerView)
            ?.apply {
                val testingAdapter = ConcatAdapter(
                    ConcatAdapter.Config.Builder()
                        .setIsolateViewTypes(false)
                        .build(),
                    testingPlanHeaderAdapter,
                    planEditAdapter,
                    reminderRowAdapter,
                    hintAdapter
                )

                adapter = testingAdapter
                itemAnimator = null
            }

        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(headerItems) {
                testingPlanHeaderAdapter.submitList(listOf(it))
                view?.findViewById<RecyclerView>(R.id.testPlanRecyclerView)
                    ?.scrollToPosition(0)
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(reminderItems) {
                if (it.isNotEmpty()) {
                    planEditAdapter.submitList(listOf(PlanEditItem(getString(R.string.schedule))))
                    reminderRowAdapter.submitList(it) {
                        view?.findViewById<RecyclerView>(R.id.testPlanRecyclerView)
                            ?.scrollToPosition(0)
                    }
                } else {
                    reminderRowAdapter.submitList(listOf())
                    planEditAdapter.submitList(listOf())
                }
            }
        }
        hintAdapter.submitList(listOf(Unit))
    }
}
