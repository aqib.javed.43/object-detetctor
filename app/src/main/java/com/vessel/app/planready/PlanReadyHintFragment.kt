package com.vessel.app.planready

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PlanReadyHintFragment : BaseFragment<PlanReadyHintViewModel>() {
    override val viewModel: PlanReadyHintViewModel by viewModels()
    override val layoutResId = R.layout.fragment_plan_ready_hint

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)
        viewModel.navigate.observe(
            viewLifecycleOwner,
            {
                findNavController().navigateUp()
            }
        )
    }
}
