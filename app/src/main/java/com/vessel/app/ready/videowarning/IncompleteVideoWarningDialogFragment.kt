package com.vessel.app.ready.videowarning

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class IncompleteVideoWarningDialogFragment : BaseDialogFragment<IncompleteVideoWarningViewModel>() {
    override val viewModel: IncompleteVideoWarningViewModel by viewModels()
    override val layoutResId = R.layout.fragment_in_complete_video_warning_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
            setGravity(Gravity.BOTTOM)
        }
        isCancelable = false

        observe(viewModel.dismissDialog) {
            requireActivity().supportFragmentManager.setFragmentResult(REQUEST_KEY, bundleOf())
            dismiss()
        }
    }

    override fun onPause() {
        super.onPause()
        super.dismiss()
    }

    companion object {
        const val REQUEST_KEY = "IncompleteVideoWarningRequestkey"
    }
}
