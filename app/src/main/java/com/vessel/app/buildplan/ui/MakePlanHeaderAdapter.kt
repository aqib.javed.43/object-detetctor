package com.vessel.app.buildplan.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.buildplan.model.MakePlanHeader

class MakePlanHeaderAdapter() : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        MakePlanHeader -> R.layout.item_make_plan_header
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any? = null
}
