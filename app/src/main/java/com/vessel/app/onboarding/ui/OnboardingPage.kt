package com.vessel.app.onboarding.ui

import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import com.vessel.app.R

sealed class OnboardingPage(
    @StringRes val title: Int,
    @StringRes val subtitle: Int,
    @LayoutRes val layoutResId: Int
) {
    class Gender(@IdRes var userValue: Int = R.id.radioMale) :
        OnboardingPage(
            R.string.gender,
            R.string.gender_subtitle,
            R.layout.page_onboarding_gender
        )

    class Height(var userValue: String = "", val onClickListener: View.OnClickListener) :
        OnboardingPage(
            R.string.height,
            R.string.height_subtitle,
            R.layout.page_onboarding_height
        )

    class Weight(var userValue: String = "", val onClickListener: View.OnClickListener) :
        OnboardingPage(
            R.string.weight,
            R.string.weight_subtitle,
            R.layout.page_onboarding_weight
        )

    class Age(private val onDateSet: () -> Unit) : OnboardingPage(
        R.string.age,
        R.string.age_subtitle,
        R.layout.page_onboarding_age
    ) {
        var userValue: String = ""
            set(value) {
                field = value
                onDateSet.invoke()
            }
    }
}
