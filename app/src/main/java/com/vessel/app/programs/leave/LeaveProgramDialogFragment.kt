package com.vessel.app.programs.leave

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LeaveProgramDialogFragment : BaseDialogFragment<LeaveProgramViewModel>() {
    override val viewModel: LeaveProgramViewModel by viewModels()
    override val layoutResId = R.layout.fragment_program_leave_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_large).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
            setGravity(Gravity.BOTTOM)
        }
        isCancelable = false

        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(dismissDialog) { dismiss() }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }

    override fun dismiss() {
        findNavController().previousBackStackEntry
            ?.savedStateHandle?.apply {
                set(
                    REMOVE_PROGRAM,
                    viewModel.state.dismissDialog.value
                )
            }
        super.dismiss()
    }

    companion object {
        const val REMOVE_PROGRAM = "remove program"
    }
}
