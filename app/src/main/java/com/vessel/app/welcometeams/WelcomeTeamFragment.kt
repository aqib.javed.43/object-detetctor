package com.vessel.app.welcometeams

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.binding.srcFromUrl
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home_team_page.*
import kotlinx.android.synthetic.main.fragment_welcome_teams.*

@AndroidEntryPoint
class WelcomeTeamFragment : BaseFragment<WelcomeTeamViewModel>() {
    override val viewModel: WelcomeTeamViewModel by viewModels()
    override val layoutResId = R.layout.fragment_welcome_teams

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observeValues()
        setupBackPressedDispatcher()
    }

    private fun observeValues() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(goal) { goal ->
                goal?.let {
                    lblTitle.text = String.format(getString(R.string.your_team_welcome), it.name)
                    imgTeams.srcFromUrl(
                        it.image_small_url
                    )
                }
            }
        }
        viewModel.getTeamData()
    }

    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.let { viewModel.onBackButtonClicked(it) }
        }
    }
}
