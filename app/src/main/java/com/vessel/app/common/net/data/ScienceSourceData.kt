package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ScienceSourceData(
    val id: Int,
    val title: String,
    val description: String?,
    val source_url: String
) : Parcelable
