package com.vessel.app.programs.details.schedule

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.programs.details.adapters.ProgramScheduleRowAdapter
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProgramScheduleFragment : BaseFragment<ProgramScheduleViewModel>() {
    override val viewModel: ProgramScheduleViewModel by viewModels()
    override val layoutResId = R.layout.fragment_program_schedule

    private val scheduleAdapter by lazy { ProgramScheduleRowAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        requireView().findViewById<RecyclerView>(R.id.scheduleList).apply {
            adapter = scheduleAdapter
            itemAnimator = null
        }
        viewModel.state {
            observe(program) {
                scheduleAdapter.submitList(it.getScheduleUiModel().programScheduleList)
            }
            observe(navigateTo) { findNavController().navigate(it) }
        }
    }
}
