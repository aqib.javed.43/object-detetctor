package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SurveyAnswerResponse(
    val answers: List<SurveyAnswerWrapperData>,
    val questionId: Int? = null
)
