package com.vessel.app.ready.instructionstips

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class InstructionsTipsViewModel @ViewModelInject constructor(
    val state: InstructionsTipsState,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {

    fun onBackButtonClicked() {
        navigateBack()
    }
}
