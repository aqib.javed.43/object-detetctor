package com.vessel.app.common.net.data.plan

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class BuildPlanRequest(
    val food_ids: List<Int>,
    val reagent_lifestyle_recommendations_ids: List<Int>,
    val supplements_ids: List<Int>,
    val is_auto_build: Boolean?,
) : Parcelable
