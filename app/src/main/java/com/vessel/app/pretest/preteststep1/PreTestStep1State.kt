package com.vessel.app.pretest.preteststep1

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PreTestStep1State @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val finish = LiveEvent<Unit>()
    val enableCheckBoxes = MutableLiveData(true)
    val peeOnACardChecked = MutableLiveData(false)
    val directPeeChecked = MutableLiveData(false)

    operator fun invoke(block: PreTestStep1State.() -> Unit) = apply(block)
}
