package com.vessel.app.reagent.energysources

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.reagent.model.InfoSource
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class SourceState @Inject constructor(_infoSource: Array<InfoSource>, energyName: String, val headerTitle: String) {
    val navigateTo = LiveEvent<NavDirections>()
    val dismissDialog = LiveEvent<Unit>()
    val infoSource = MutableLiveData(_infoSource)
    val energyName = MutableLiveData(energyName)
    operator fun invoke(block: SourceState.() -> Unit) = apply(block)
}
