package com.vessel.app.common.manager

import android.util.Log
import com.vessel.app.Constants
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.supplement.ViewModel
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.util.extensions.getDateDayDiff
import com.vessel.app.util.remoteconfig.FirebaseRemoteConfiguration
import com.zendesk.service.ErrorResponse
import com.zendesk.service.ZendeskCallback
import zendesk.chat.Chat
import zendesk.chat.ChatConfiguration
import zendesk.chat.VisitorInfo
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MembershipManager @Inject constructor(
    private val zendeskChat: Chat,
    val scoreManager: ScoreManager,
    val supplementsManager: SupplementsManager,
    val remoteConfiguration: FirebaseRemoteConfiguration,
    private val preferencesRepository: PreferencesRepository
) {
    var hasActiveMembership = false
    var isNewMember = false

    suspend fun checkMembershipStatus() {
        Constants.TRIAL_FREE_DAYS = remoteConfiguration.getLong(Constants.FREE_TRIAL_PERIOD).toInt()
        Log.d("TAG", "checkMembershipStatus: ${Constants.TRIAL_FREE_DAYS}")
        supplementsManager.getMemberships()
            .onSuccess { memberships ->
                val membership =
                    memberships.firstOrNull { it.status == ViewModel.STATUS_ACTIVE }
                if (membership != null) {
                    preferencesRepository.freeTrialLocked = false
                    hasActiveMembership = true
                    isNewMember = false
                } else {
                    preferencesRepository.getContact()?.insertDate?.let { insertEmailDate ->
                        val today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
                        preferencesRepository.freeTrialLocked = insertEmailDate.getDateDayDiff(today) > Constants.TRIAL_FREE_DAYS
                    }
                    hasActiveMembership = false
                    isNewMember = false
                }
            }.onError {
                preferencesRepository.getContact()?.insertDate?.let { insertEmailDate ->
                    val today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
                    preferencesRepository.freeTrialLocked = insertEmailDate.getDateDayDiff(today) > Constants.TRIAL_FREE_DAYS
                    isNewMember = true
                    hasActiveMembership = false
                }
            }
    }

    fun getChatConfiguration(department: String): ChatConfiguration? {
        val profileProvider = zendeskChat.providers()!!.profileProvider()
        val visitorInfo = VisitorInfo.builder()
            .withEmail(preferencesRepository.userEmail.orEmpty())
            .withName(preferencesRepository.userFirstName.orEmpty())
            .build()
        profileProvider.setVisitorInfo(visitorInfo, null)

        zendeskChat.providers()!!.chatProvider()
            .setDepartment(
                department,
                object : ZendeskCallback<Void>() {
                    override fun onSuccess(p0: Void?) {
                        // not used
                    }

                    override fun onError(p0: ErrorResponse?) {
                        // not used
                    }
                }
            )

        return ChatConfiguration.builder()
            .withPreChatFormEnabled(false)
            .withTranscriptEnabled(false)
            .withChatMenuActions()
            .build()
    }

    fun hasMembership() = hasActiveMembership
    fun hasTrailLocked() = hasActiveMembership.not() && preferencesRepository.freeTrialLocked
    companion object {
        const val ZENDESK_NUTRITIONIST_DEPARTMENT = "Certified Nutritionist"
        const val ZENDESK_SUPPORT_DEPARTMENT = "Support"
    }
}
