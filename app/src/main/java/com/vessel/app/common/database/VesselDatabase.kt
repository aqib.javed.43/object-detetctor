package com.vessel.app.common.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vessel.app.covid19.db.CovidTestDao
import com.vessel.app.covid19.db.CovidTestDb

@Database(
    entities = [
        CovidTestDb::class
    ],
    version = 1,
    exportSchema = false
)
abstract class VesselDatabase : RoomDatabase() {
    abstract fun covidTestDao(): CovidTestDao
}

const val DATABASE_NAME = "vessel_db"
