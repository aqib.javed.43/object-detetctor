package com.vessel.app.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.renderer.XAxisRenderer
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.Transformer
import com.github.mikephil.charting.utils.Utils
import com.github.mikephil.charting.utils.ViewPortHandler
import com.vessel.app.R

class DateXAxisRenderer(
    private val context: Context,
    viewPortHandler: ViewPortHandler,
    xAxis: XAxis,
    trans: Transformer
) : XAxisRenderer(viewPortHandler, xAxis, trans) {

    private val paint = Paint(mAxisLabelPaint)
    private val monthTypeface by lazy { ResourcesCompat.getFont(context, R.font.noetext_regular) }
    private val dayTypeface by lazy { ResourcesCompat.getFont(context, R.font.bananagrotesk) }
    private val monthTextSize by lazy { context.resources.getDimensionPixelSize(R.dimen.font_1xs).toFloat() }
    private val monthMarginTop by lazy { context.resources.getDimensionPixelSize(R.dimen.spacing_small) }
    private val dayTextSize by lazy { context.resources.getDimensionPixelSize(R.dimen.font_small).toFloat() }
    private val dayMarginTop by lazy { context.resources.getDimensionPixelSize(R.dimen.spacing_small) }
    private var marginStart = 0

    override fun drawLabel(
        c: Canvas?,
        formattedLabel: String?,
        x: Float,
        y: Float,
        anchor: MPPointF?,
        angleDegrees: Float
    ) {
        var newY: Float
        formattedLabel?.let {
            it.split("\n").take(2).forEachIndexed { index, string ->
                if (index == 0) {
                    paint.typeface = monthTypeface
                    paint.textSize = monthTextSize
                    marginStart = context.resources.getDimensionPixelSize(R.dimen.spacing_xs)
                    paint.color = ContextCompat.getColor(context, R.color.darkText3)
                    newY = y + monthTextSize + monthMarginTop
                } else {
                    paint.typeface = dayTypeface
                    paint.textSize = dayTextSize
                    marginStart = context.resources.getDimensionPixelSize(R.dimen.spacing_4xs)
                    paint.color = ContextCompat.getColor(context, R.color.black)
                    newY = y + dayTextSize + dayMarginTop + monthMarginTop
                }
                Utils.drawXAxisValue(
                    c,
                    string,
                    x + marginStart,
                    newY,
                    paint,
                    anchor,
                    angleDegrees
                )
            }
        }
    }
}
