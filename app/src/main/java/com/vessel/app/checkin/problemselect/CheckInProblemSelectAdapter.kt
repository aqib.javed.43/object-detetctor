package com.vessel.app.checkin.problemselect

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.onboarding.problemselection.model.ProblemSelect
import kotlinx.android.synthetic.main.item_issue.view.*

class CheckInProblemSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<ProblemSelect>() {
    override fun getLayout(position: Int) = R.layout.item_checkin_issue

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onGoalSelectClicked(items: List<ProblemSelect>)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ProblemSelect>, position: Int) {
        val item = getItem(holder.bindingAdapterPosition)
        holder.itemView.checkbox.setOnCheckedChangeListener { v, isSelected ->
            item.checked = isSelected
            handler.onGoalSelectClicked(
                getSelectedItems()
            )
        }
        super.onBindViewHolder(holder, position)
    }
    private fun getSelectedItems(): List<ProblemSelect> {
        return currentList.filter {
            it.checked
        }
    }
}
