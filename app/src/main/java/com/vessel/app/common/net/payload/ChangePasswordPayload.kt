package com.vessel.app.common.net.payload

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ChangePasswordPayload(
    val current_password: String,
    val new_password: String
)
