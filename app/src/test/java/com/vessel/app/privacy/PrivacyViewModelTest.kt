package com.vessel.app.privacy

import com.vessel.app.BaseTest
import com.vessel.app.MockProvider
import com.vessel.app.R
import com.vessel.app.util.ResourceRepository
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class PrivacyViewModelTest : BaseTest() {
    private val resourceRepository = mockk<ResourceRepository>()
    private lateinit var viewModel: PrivacyViewModel

    @BeforeEach
    fun setUp() {
        viewModel = PrivacyViewModel(resourceRepository)
    }

    @Test
    fun `onContinueClicked navigates to onboarding screen`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onContinueClicked(view)

        verify { navController.navigate(R.id.onboardingFragment) }
    }
}
