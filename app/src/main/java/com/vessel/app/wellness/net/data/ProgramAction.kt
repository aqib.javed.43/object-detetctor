package com.vessel.app.wellness.net.data

enum class ProgramAction(val value: String) {
    CLEAR("clear"),
    KEEP("keep"),
    RE_ENROLL("re-enroll");

    companion object {
        fun from(state: String?) = LikeStatus.values().firstOrNull { it.value == state }
    }
}
