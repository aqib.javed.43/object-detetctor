package com.vessel.app.wellness.comingsoonpopup

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.data.ReagentInfo
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class ComingSoonReagentDialogViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    val comingSoonTagItems = MutableLiveData<List<String>>()
    val reagentInfo = savedStateHandle.get<ReagentInfo>("reagentInfo")!!

    init {
        comingSoonTagItems.value = reagentInfo.comingSoonTagItems
    }

    fun onGotItClicked() {
        dismissDialog.call()
    }
}
