package com.vessel.app.common.manager

import com.vessel.app.common.repo.PreferencesRepository
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.asFlow
import zendesk.chat.ChatConfiguration
import zendesk.chat.PreChatFormFieldStatus
import zendesk.core.JwtIdentity
import zendesk.core.Zendesk
import zendesk.support.Support
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChatManager @Inject constructor(
    private val zendesk: Zendesk,
    private val preferencesRepository: PreferencesRepository
) {
    private val openChatChannel = BroadcastChannel<Boolean>(1)

    fun setUserInfo() {
        zendesk.setIdentity(JwtIdentity(preferencesRepository.accessToken))
        Support.INSTANCE.init(zendesk)
    }

    fun getOpenChatChannelState() = openChatChannel.asFlow()

    fun clearOpenChatChannelState() {
        openChatChannel.offer(false)
    }

    companion object {
        val chatConfiguration: ChatConfiguration = ChatConfiguration.builder()
            .withAgentAvailabilityEnabled(true)
            .withNameFieldStatus(PreChatFormFieldStatus.REQUIRED)
            .withEmailFieldStatus(PreChatFormFieldStatus.REQUIRED)
            .withPhoneFieldStatus(PreChatFormFieldStatus.OPTIONAL)
            .build()
    }
}
