package com.vessel.app.supplementsbucket

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.supplement.model.SupplementFormula
import com.vessel.app.supplementsbucket.model.GoalViewItem
import com.vessel.app.supplementsbucket.model.Supplement
import com.vessel.app.supplementsbucket.model.SupplementHeader
import com.vessel.app.util.LiveEvent
import com.vessel.app.vesselfuelcheckout.model.VesselFuelItem
import javax.inject.Inject

class SupplementsBucketState @Inject constructor(val recommendedSupplements: Array<SupplementFormula>) {
    val goalBasedSupplement = MutableLiveData<Map<GoalViewItem, List<Supplement>>>()
    val supplementHeader = MutableLiveData<SupplementHeader>()
    val navigateTo = LiveEvent<NavDirections>()
    val navigateToWeb = LiveEvent<String>()
    val navigateToCheckout = LiveEvent<VesselFuelItem>()
    val descriptionPopupPosition = LiveEvent<Int>()
    val descriptionPopupCloseAction = LiveEvent<Int>()
    val descriptionPopupText = MutableLiveData<String>()

    operator fun invoke(block: SupplementsBucketState.() -> Unit) = apply(block)
}
