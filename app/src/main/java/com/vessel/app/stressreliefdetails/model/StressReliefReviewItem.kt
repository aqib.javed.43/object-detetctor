package com.vessel.app.stressreliefdetails.model
import com.vessel.app.activityreview.model.RecommendationReview

data class StressReliefReviewItem(
    val title: String,
    val items: List<RecommendationReview>
)
