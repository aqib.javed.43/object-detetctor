package com.vessel.app.covid19.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CovidTestDb(
    @PrimaryKey @ColumnInfo(name = "uuid") val uuid: String,
    @ColumnInfo(name = "status") val status: Int,
    @ColumnInfo(name = "insertDate") val insertDate: String
)
