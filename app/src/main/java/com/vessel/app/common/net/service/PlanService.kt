package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.plan.*
import retrofit2.http.*

interface PlanService {
    @GET("plan/")
    suspend fun getUserPlans(
        @Query("date") date: String? = null,
        @Query("is_today") isToday: String? = null,
        @Query("goal_id") goalId: Int? = null,
        @Query("program_id") programId: Int? = null
    ): UserPlansResponse

    @POST("plan/")
    suspend fun addNewPlan(@Body plan: CreatePlanRequest): PlanData

    @PUT("plan/{planId}")
    suspend fun updatePlanItem(@Path("planId") planId: Int, @Body plan: CreatePlanRequest): PlanData

    @HTTP(method = "DELETE", path = "plan/", hasBody = true)
    suspend fun deletePlanItem(@Body request: DeletePlanItemRequest)

    @POST("plan/{planId}/toggle")
    suspend fun updatePlanToggle(
        @Path("planId") planId: Int,
        @Body request: PlanToggleRequest
    ): PlanRecord

    @POST("plan/build")
    suspend fun buildPlan(
        @Body request: BuildPlanRequest
    ): Unit

    @GET("plan/")
    suspend fun getPlansByRange(
        @Query("date") date: String? = null,
        @Query("endDate") endDate: String? = null
    ): UserPlansResponse

    @GET("plan/next")
    suspend fun getNextUserPlans(
        @Query("contact_id") contactId: Int? = 0,
        @Query("program_id") programId: Int? = 0
    ): PlanData

    @POST("essential/toggle")
    suspend fun updateEssentialToggle(
        @Body request: EssentialToggleRequest
    ): EssentialRecord
}
