package com.vessel.app.common.net

/**
 * Simple class of HTTP status code constants.
 */
@Suppress("unused")
object HttpStatusCode {
    /** 100 - 199: Informational  */
    const val CONTINUE = 100
    const val SWITCHING_PROTOCOLS = 101
    const val PROCESSING = 102

    /** 200 - 299: Success  */
    const val SUCCESS = 200
    const val OK = SUCCESS
    const val CREATED = 201
    const val ACCEPTED = 202
    const val NON_AUTHORITATIVE_INFORMATION = 203
    const val NO_CONTENT = 204
    const val RESET_CONTENT = 205
    const val PARTIAL_CONTENT = 206
    const val MULTI_STATUS = 207

    /** 300 - 399: Redirection  */
    const val MULTIPLE_CHOICES = 300
    const val MOVED_PERMANENTLY = 301
    const val MOVED_TEMPORARILY = 302
    const val SEE_OTHER = 303
    const val NOT_MODIFIED = 304
    const val USE_PROXY = 305
    const val TEMPORARY_REDIRECT = 307

    /** 400 - 499: Client Error  */
    const val BAD_REQUEST = 400
    const val UNAUTHORIZED = 401
    const val PAYMENT_REQUIRED = 402
    const val FORBIDDEN = 403
    const val NOT_FOUND = 404
    const val METHOD_NOT_ALLOWED = 405
    const val NOT_ACCEPTABLE = 406
    const val PROXY_AUTHENTICATION_REQUIRED = 407
    const val REQUEST_TIMEOUT = 408
    const val CONFLICT = 409
    const val GONE = 410
    const val LENGTH_REQUIRED = 411
    const val PRECONDITION_FAILED = 412
    const val REQUEST_TOO_LONG = 413
    const val REQUEST_URI_TOO_LONG = 414
    const val UNSUPPORTED_MEDIA_TYPE = 415
    const val REQUESTED_RANGE_NOT_SATISFIABLE = 416
    const val EXPECTATION_FAILED = 417
    const val INSUFFICIENT_SPACE_ON_RESOURCE = 419
    const val METHOD_FAILURE = 420
    const val UNPROCESSABLE_ENTITY = 422
    const val LOCKED = 423
    const val FAILED_DEPENDENCY = 424

    /** 500 - 599: Server Error  */
    const val INTERNAL_SERVER_ERROR = 500
    const val NOT_IMPLEMENTED = 501
    const val BAD_GATEWAY = 502
    const val SERVICE_UNAVAILABLE = 503
    const val GATEWAY_TIMEOUT = 504
    const val HTTP_VERSION_NOT_SUPPORTED = 505
    const val INSUFFICIENT_STORAGE = 507
}
