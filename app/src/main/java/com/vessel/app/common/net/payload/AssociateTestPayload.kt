package com.vessel.app.common.net.payload

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AssociateTestPayload(
    val uuid: String,
    val wellness_card_uuid: String?,
    val replacement_parent_uuid: String?,
    val auto_scan: Boolean
)
