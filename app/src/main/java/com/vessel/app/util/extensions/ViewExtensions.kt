package com.vessel.app.util.extensions

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Point
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import com.vessel.app.util.VerticalImageSpan

inline fun View.afterMeasured(crossinline afterMeasureCallback: View.() -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredWidth > 0 && measuredHeight > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                afterMeasureCallback()
            }
        }
    })
}

fun View.animateHeight(from: Int, to: Int) {
    val anim = ValueAnimator.ofInt(from, to)
    anim.addUpdateListener { valueAnimator ->
        val currentHeight = valueAnimator.animatedValue as Int
        this.updateLayoutParams {
            height = currentHeight
        }
    }
    anim.duration = 300
    anim.start()
}

fun View.setHeight(currentHeight: Int) {
    this.updateLayoutParams {
        height = currentHeight
    }
}

fun View.getPointLocationOnScreen(): Point {
    val location = IntArray(2)
    this.getLocationOnScreen(location)
    return Point(location[0], location[1])
}

fun View.setMargins(
    leftMarginPx: Int? = null,
    topMarginPx: Int? = null,
    rightMarginPx: Int? = null,
    bottomMarginPx: Int? = null
) {
    if (layoutParams is ViewGroup.MarginLayoutParams) {
        val params = layoutParams as ViewGroup.MarginLayoutParams
        leftMarginPx?.run { params.leftMargin = this }
        topMarginPx?.run { params.topMargin = this }
        rightMarginPx?.run { params.rightMargin = this }
        bottomMarginPx?.run { params.bottomMargin = this }
        requestLayout()
    }
}

fun View.hideKeyboard() {
    val imm = ContextCompat.getSystemService(context, InputMethodManager::class.java) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun Context.addImage(
    fullText: String,
    atText: String,
    @DrawableRes imgSrc: Int,
    imgWidth: Int,
    imgHeight: Int
): SpannableStringBuilder {
    val ssb = SpannableStringBuilder(fullText)

    val drawable =
        ContextCompat.getDrawable(this, imgSrc) ?: return ssb
    drawable.mutate()
    drawable.setBounds(
        0, 0,
        imgWidth,
        imgHeight
    )
    val start = fullText.indexOf(atText)
    ssb.setSpan(
        VerticalImageSpan(drawable),
        start,
        start + atText.length,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )
    return ssb
}
