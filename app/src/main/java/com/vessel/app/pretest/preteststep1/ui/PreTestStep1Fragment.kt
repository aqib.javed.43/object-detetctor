package com.vessel.app.pretest.preteststep1.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.pretest.preteststep1.PreTestStep1ViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PreTestStep1Fragment : BaseFragment<PreTestStep1ViewModel>() {
    override val viewModel: PreTestStep1ViewModel by viewModels()
    override val layoutResId = R.layout.fragment_pre_test_step1

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(finish) { requireActivity().finish() }
        }
    }
}
