package com.vessel.app.hydrationplan.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.base.BaseFragment
import com.vessel.app.checkscience.model.CheckOutScienceHeaderItem
import com.vessel.app.checkscience.ui.CheckOutScienceHeaderAdapter
import com.vessel.app.checkscience.ui.CheckOutScienceItemsAdapter
import com.vessel.app.common.adapter.impactgoals.ImpactGoalsAdapter
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentAdapter
import com.vessel.app.common.adapter.planedit.PlanEditAdapter
import com.vessel.app.common.adapter.planedit.PlanEditItem
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.hydrationplan.HydrationPlanViewModel
import com.vessel.app.hydrationplan.model.HydrationPlanInfoItem
import com.vessel.app.reagent.ui.ReagentEnergyAdapter
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HydrationPlanFragment : BaseFragment<HydrationPlanViewModel>() {
    override val viewModel: HydrationPlanViewModel by viewModels()
    override val layoutResId = R.layout.fragment_hydration_plan

    private val hydrationPlanHeaderAdapter by lazy { HydrationPlanHeaderAdapter(viewModel) }
    private val hydrationPlanInfoAdapter by lazy { HydrationPlanInfoAdapter() }
    private val reagentEnergyAdapter by lazy { ReagentEnergyAdapter(viewModel) }
    private val planEditAdapter by lazy { PlanEditAdapter(viewModel) }
    private val impactGoalsAdapter by lazy { ImpactGoalsAdapter(viewModel) }
    private val reminderRowAdapter by lazy { ReminderRowAdapter(viewModel) }
    private val impactTestReagentsAdapter by lazy { ImpactTestReagentAdapter(viewModel) }
    private val checkOutScienceHeaderAdapter by lazy { CheckOutScienceHeaderAdapter(viewModel) }
    private val checkOutScienceItemsAdapter by lazy { CheckOutScienceItemsAdapter(viewModel) }
    private val otherBenefitsHeaderAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_other_benefits_header
    }

    private val impactGoalsTitleAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_impact_goals_title
    }
    private val impactTestResultsTitleAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_impact_test_results_title
    }
    private val impactTestResultsHeaderTitleAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_impact_test_results_title
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        view?.findViewById<RecyclerView>(R.id.hydrationPlanRecyclerView)
            ?.apply {
                val hydrationAdapter = ConcatAdapter(
                    ConcatAdapter.Config.Builder()
                        .setIsolateViewTypes(false)
                        .build(),
                    hydrationPlanHeaderAdapter,
                    planEditAdapter,
                    reminderRowAdapter,
                    impactTestResultsHeaderTitleAdapter,
                    impactTestReagentsAdapter,
                    impactGoalsTitleAdapter,
                    impactGoalsAdapter,
                    hydrationPlanInfoAdapter,
                    checkOutScienceHeaderAdapter,
                    checkOutScienceItemsAdapter,
                    otherBenefitsHeaderAdapter,
                    reagentEnergyAdapter
                )

                val hydrationSizeLookup = HydrationSizeLookup(hydrationAdapter)

                adapter = hydrationAdapter
                layoutManager = GridLayoutManager(requireContext(), 2).apply {
                    spanSizeLookup = hydrationSizeLookup
                }
                itemAnimator = null
            }

        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(headerItems) {
                hydrationPlanHeaderAdapter.submitList(listOf(it))
                view?.findViewById<RecyclerView>(R.id.hydrationPlanRecyclerView)
                    ?.scrollToPosition(0)
            }
            observe(hydrationGoalsItems) {
                impactGoalsAdapter.submitList(it)
            }
            observe(hydrationReagentInfo) {
                reagentEnergyAdapter.submitList(it.benefitsOfBeingOptimal)
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(reagentInfo) {
                impactTestReagentsAdapter.submitList(it)
            }
            observe(reminderItems) {
                if (it.isNotEmpty()) {
                    planEditAdapter.submitList(listOf(PlanEditItem(getString(R.string.schedule))))
                } else {
                    planEditAdapter.submitList(listOf())
                }
                reminderRowAdapter.submitList(it)
            }
        }
        otherBenefitsHeaderAdapter.submitList(listOf(Unit))
        checkOutScienceHeaderAdapter.submitList(
            listOf(
                CheckOutScienceHeaderItem(
                    viewModel.getCheckOutScienceHeaderContent(),
                    R.color.linen,
                    R.drawable.white_alpha70_background
                )
            )
        )
        checkOutScienceItemsAdapter.submitList(viewModel.getCheckOutScienceItems())
        hydrationPlanInfoAdapter.submitList(listOf(HydrationPlanInfoItem))
        impactGoalsTitleAdapter.submitList(listOf(Unit))
        impactTestResultsHeaderTitleAdapter.submitList(listOf(Unit))
    }
}
