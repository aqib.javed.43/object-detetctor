package com.vessel.app.filterfoodplandialog.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class FilterFooterAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = R.layout.item_filter_footer

    override fun getHandler(position: Int): Any? = handler

    interface OnActionHandler {
        fun onManageDietAllergiesClicked()
    }
}
