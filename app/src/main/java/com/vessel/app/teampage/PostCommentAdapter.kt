package com.vessel.app.teampage

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.teampage.model.CommentItem
import kotlinx.android.synthetic.main.item_comment.view.*
import kotlinx.android.synthetic.main.item_post_footer.view.*
import kotlinx.android.synthetic.main.post_action_view.view.*

class PostCommentAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<CommentItem>() {
    override fun getLayout(position: Int) = R.layout.item_comment

    override fun getHandler(position: Int): Any = handler

    interface OnActionHandler {
        fun onLikeClick(item: Any, view: View, parentPosition: Int)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<CommentItem>, position: Int) {
        val item = getItem(position)
        holder.itemView.imgInteraction.isSelected = item.isLiked
        holder.itemView.commentator.text = item.name
        holder.itemView.lblComment.text = item.comment
        holder.itemView.imgInteraction.setOnClickListener {
            handler.onLikeClick(item, it, position)
            item.isLiked = !item.isLiked
            holder.itemView.imgInteraction.isSelected = item.isLiked
        }
    }
}
