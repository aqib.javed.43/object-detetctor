package com.vessel.app.homescreen

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class MainPagerFragmentTab(val index: Int) : Parcelable {
    PLAN(0),
    RESULT(1),
    CHAT(2),
    MORE(3);

    companion object {
        fun from(tabNumber: Int) = values().firstOrNull { it.index == tabNumber } ?: PLAN
    }
}
