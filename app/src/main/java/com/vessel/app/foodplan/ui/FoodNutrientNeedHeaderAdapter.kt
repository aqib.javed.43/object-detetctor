package com.vessel.app.foodplan.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodplan.model.FoodNutrientNeedsHeaderItem

class FoodNutrientNeedHeaderAdapter(
    val handler: FoodNutrientNeedHandler
) : BaseAdapterWithDiffUtil<FoodNutrientNeedsHeaderItem>() {
    override fun getLayout(position: Int) = R.layout.item_food_plan_daily_nutrient_needs

    override fun getHandler(position: Int): FoodNutrientNeedHandler = handler

    interface FoodNutrientNeedHandler {
        fun onAddPlanClicked()
    }
}
