package com.vessel.app.onboardingintro.introresult

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class IntroResultState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: IntroResultState.() -> Unit) = apply(block)
}
