package com.vessel.app.newtesttimer.ui

import android.os.Bundle
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.tapadoo.alerter.Alerter
import com.vessel.app.R
import com.vessel.app.activationtimer.util.TimerServiceManager
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.ext.showNotificationSettingsSnackbar
import com.vessel.app.homescreen.HomeScreenPagerAdapter
import com.vessel.app.newtesttimer.NewTestTimerViewModel
import com.vessel.app.taketest.TakeTestViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewTestTimerFragment : BaseFragment<NewTestTimerViewModel>() {
    override val viewModel: NewTestTimerViewModel by viewModels()
    override val layoutResId = R.layout.fragment_new_test_timer
    private val takeTestViewModel by lazy { parentViewModel<TakeTestViewModel>() }

    val tabSelectedIcon by lazy { ContextCompat.getDrawable(requireContext(), R.drawable.ic_dark_circle_8dp) }
    val tabUnselectedIcon by lazy { ContextCompat.getDrawable(requireContext(), R.drawable.ic_dark_circle_alpha70_8dp) }
    private var pagerAdapter: HomeScreenPagerAdapter? = null
    private lateinit var viewPager: ViewPager2
    private lateinit var tabLayout: TabLayout

    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabSelected(tab: TabLayout.Tab) {
            tab.icon = tabSelectedIcon
        }

        override fun onTabUnselected(tab: TabLayout.Tab) {
            tab.icon = tabUnselectedIcon
        }

        override fun onTabReselected(tab: TabLayout.Tab) {
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViewPager()
        setupObservers()
        setupBackPressedDispatcher()
    }

    private fun setupObservers() {
        viewModel.state.apply {
            observe(showBackWarning) {
                showBackWarning(R.string.are_you_sure_to_back)
            }

            observe(navigateToCapture) {
                takeTestViewModel.generateSampleUUID()
                findNavController().navigate(NewTestTimerFragmentDirections.actionNewTestTimerFragmentToCapture(false))
            }

            observe(showConfirmSkipAlertEvent) {
                showConfirmSkipAlert()
            }

            observe(stopTimerEvent) { TimerServiceManager.stopTimerCountDown() }

            observe(startForegroundTimer) { TimerServiceManager.startForeground() }

            observe(stopForegroundTimer) { TimerServiceManager.stopForeground() }

            observe(showNotificationSettingsAlert) { showNotificationSettingsSnackbar(it) }
        }
    }

    private fun setupViewPager() {
        pagerAdapter = HomeScreenPagerAdapter(this)
        viewPager = requireView().findViewById(R.id.tipsViewpager)
        pagerAdapter?.addFragments(viewModel.newTimerTabsManager.getCurrentTabs())
        viewPager.adapter = pagerAdapter
        tabLayout = requireView().findViewById(R.id.tabLayout)
        tabLayout.addOnTabSelectedListener(onTabSelectedListener)

        TabLayoutMediator(tabLayout, viewPager) { tab, _ ->
            tab.icon = if (tab.isSelected) tabSelectedIcon else tabUnselectedIcon
        }.attach()

        for (index in viewModel.newTimerTabsManager.getCurrentTabs().lastIndex downTo 0) {
            tabLayout.getTabAt(index)?.select()
        }
    }

    private fun showConfirmSkipAlert() {
        Alerter.create(requireActivity())
            .setTitle(R.string.warning)
            .setText(R.string.skip_confirm_message)
            .setBackgroundColorRes(R.color.colorAccent)
            .enableSound(true)
            .enableInfiniteDuration(true)
            .addButton(
                text = getString(R.string.btn_cancel),
                onClick = { Alerter.hide() }
            )
            .addButton(
                text = getString(R.string.btn_continue),
                onClick = {
                    view?.let {
                        viewModel.onSkipConfirm()
                    }
                    Alerter.hide()
                }
            )
            .show()
    }

    override fun onResume() {
        super.onResume()
        setupTimerHelper()
    }

    private fun setupTimerHelper() {
        TimerServiceManager.setHandler(viewModel)
        TimerServiceManager.stopForeground()
    }
    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.let { viewModel.onBackPressed() }
        }
    }

    override fun onDestroyView() {
        viewPager.adapter = null
        pagerAdapter = null
        viewModel.newTimerTabsManager.clear()
        super.onDestroyView()
    }
}
