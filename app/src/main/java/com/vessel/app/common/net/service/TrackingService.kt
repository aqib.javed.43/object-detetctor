package com.vessel.app.common.net.service

import com.vessel.app.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface TrackingService {
    @GET
    suspend fun logKlaviyoEvent(@Url url: String = BuildConfig.KLAVIYO_BASE_URL, @Query("data") data: String): Int
}
