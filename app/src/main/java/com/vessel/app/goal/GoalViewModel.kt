package com.vessel.app.goal

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.goal.ui.GoalHeaderAdapter
import com.vessel.app.util.ResourceRepository

class GoalViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider),
    GoalHeaderAdapter.OnActionHandler {
    val state = GoalState(savedStateHandle["goalHeader"]!!)

    override fun onInfoButtonClick() {
        // todo open the goal score dialog
    }
}
