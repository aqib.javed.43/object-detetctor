package com.vessel.app.addsupplements.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.R
import com.vessel.app.addsupplements.AddSupplementsViewModel
import com.vessel.app.base.BaseFragment
import com.vessel.app.posttestnutritioncoach.ui.BulletStringAdapter
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_add_supplements.*

@AndroidEntryPoint
class AddSupplementsFragment : BaseFragment<AddSupplementsViewModel>() {
    override val viewModel: AddSupplementsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_add_supplements

    private val addSupplementsAdapter by lazy { AddSupplementsAdapter() }
    private val bulletStringAdapter by lazy { BulletStringAdapter() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        goalsList.apply {
            adapter = addSupplementsAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }
        alignedList.adapter = bulletStringAdapter

        observe(viewModel.state.navigateTo) {
            findNavController().navigate(it)
        }
    }
}
