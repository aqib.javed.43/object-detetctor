package com.vessel.app.filterfoodplandialog.model

import androidx.annotation.StringRes
import com.vessel.app.R

enum class FoodFilterAndSortOptions(@StringRes val title: Int) {
    SORT(R.string.sort),
    CATEGORY(R.string.filter_by_category),
    NUTRIENT(R.string.filter_by_nutrient)
}
