package com.vessel.app.postreport

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PostReportState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val remainingTextCount = LiveEvent<Int>()
    operator fun invoke(block: PostReportState.() -> Unit) = apply(block)
}
