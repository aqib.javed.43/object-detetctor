package com.vessel.app.onboarding.problemselection

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProblemSelectionFragment : BaseFragment<ProblemSelectionViewModel>() {
    override val viewModel: ProblemSelectionViewModel by viewModels()
    override val layoutResId = R.layout.fragment_on_boarding_problem_selection

    val goalSelectAdapter by lazy { ProblemSelectAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        val subGoalsList = requireView().findViewById<RecyclerView>(R.id.rcvProblems)
        subGoalsList.layoutManager = LinearLayoutManager(context)
        subGoalsList.adapter = goalSelectAdapter
        setupObservers()
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(items) {
                goalSelectAdapter.submitList(it)
            }
        }
    }
}
