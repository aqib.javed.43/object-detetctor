package com.vessel.app.foodplan.ui.details

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.adapter.genericlikeordislikeadapter.LikeOrDislikeAdapter
import com.vessel.app.common.adapter.impactgoals.ImpactGoalsAdapter
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentAdapter
import com.vessel.app.common.adapter.planedit.PlanEditAdapter
import com.vessel.app.common.adapter.planedit.PlanEditItem
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.PlanDeleteDialog
import dagger.hilt.android.AndroidEntryPoint

@SuppressLint("ResourceType")
@AndroidEntryPoint
class FoodPlanDetailsFragment : BaseFragment<FoodPlanDetailsViewModel>() {
    override val viewModel: FoodPlanDetailsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_food_plan_details
    private val headerAdapter by lazy { FoodPlanDetailsHeaderAdapter(viewModel) }
    private val foodDetailsLikeOrDislikeAdapter by lazy {
        LikeOrDislikeAdapter(
            viewModel,
            R.layout.item_like_or_dislike_food_details
        )
    }
    private val foodReviewAdapter by lazy { FoodReviewAdapter(viewModel) }
    private val planEditAdapter by lazy { PlanEditAdapter(viewModel) }
    private val impactGoalsAdapter by lazy { ImpactGoalsAdapter(viewModel) }
    private val reminderRowAdapter by lazy { ReminderRowAdapter(viewModel) }
    private val impactTestReagentsAdapter by lazy { ImpactTestReagentAdapter(viewModel) }
    private val impactGoalsTitleAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_impact_goals_title
    }
    private val impactTestResultsHeaderTitleAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_impact_test_results_title
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
        setupAdapter()
    }

    private fun setupAdapter() {
        view?.findViewById<RecyclerView>(R.id.contentRecyclerView)?.apply {
            val detailsAdapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                headerAdapter,
                planEditAdapter,
                reminderRowAdapter,
                impactTestResultsHeaderTitleAdapter,
                impactTestReagentsAdapter,
                impactGoalsTitleAdapter,
                impactGoalsAdapter,
                foodDetailsLikeOrDislikeAdapter,
                foodReviewAdapter
            )

            val foodDetailsSizeLookup = FoodPlanDetailsSpanSizeLookup(detailsAdapter)

            adapter = detailsAdapter
            layoutManager = GridLayoutManager(requireContext(), 2).apply {
                spanSizeLookup = foodDetailsSizeLookup
            }
            itemAnimator = null
        }
    }

    fun setupObservers() {
        viewModel.state {
            observe(header) {
                headerAdapter.submitList(listOf(it))
                view?.findViewById<RecyclerView>(R.id.contentRecyclerView)?.scrollToPosition(0)
                foodDetailsLikeOrDislikeAdapter.submitList(listOf(it))
            }
            observe(goalsItems) {
                impactGoalsAdapter.submitList(it) {
                    view?.findViewById<RecyclerView>(R.id.contentRecyclerView)?.scrollToPosition(0)
                }
            }
            observe(reagentInfo) {
                impactTestReagentsAdapter.submitList(it) {
                    view?.findViewById<RecyclerView>(R.id.contentRecyclerView)?.scrollToPosition(0)
                }
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(reminderItems) {
                if (it.isNotEmpty()) {
                    planEditAdapter.submitList(listOf(PlanEditItem(getString(R.string.schedule))))
                    reminderRowAdapter.submitList(it) {
                        view?.findViewById<RecyclerView>(R.id.contentRecyclerView)
                            ?.scrollToPosition(0)
                    }
                } else {
                    reminderRowAdapter.submitList(listOf())
                    planEditAdapter.submitList(listOf())
                }
            }
            observe(viewModel.state.removeReminder) {
                PlanDeleteDialog.showDialog(
                    requireActivity(),
                    viewModel.state.header.value?.tittle.orEmpty(),
                    {
                        // Not used
                    },
                    {
                        viewModel.deletePlanItem()
                    },
                    {
                        viewModel.onDontShowAgainClicked(it)
                    }
                )
            }
            observe(reviews) { reviews ->
                foodReviewAdapter.submitList(reviews)
            }
        }

        impactGoalsTitleAdapter.submitList(listOf(Unit))
        impactTestResultsHeaderTitleAdapter.submitList(listOf(Unit))
    }
}
