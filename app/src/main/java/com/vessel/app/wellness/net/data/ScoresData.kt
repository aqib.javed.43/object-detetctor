package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ScoresData(
    val wellness_score: Float,
    val errors: List<ScoreErrorData>?,
    val reagents: List<ReagentData>?
)
