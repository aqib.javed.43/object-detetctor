Folate, Vitamin B9, plays a key role in several biological processes, including the formation of DNA and other genetic material, the metabolism of amino acids, and the process of cell division.

Folate is required for methylation reactions such as converting homocysteine (an inflammatory compound) to methionine and S-adenosylmethionine, a compound which can help boost our mood, improve pain from arthritis, and more. There is conclusive evidence that folate can reduce the risk of neural tube defects in infants, and there is growing evidence of the role of folate in reducing the risk of cardiovascular disease and improving treatment of depression.

Studies show that between 80% - 90% of adults in the US are deficient in folate, and it is considered the most common nutrient deficiency in this country. Folate comes in several different forms. Most fortified foods and dietary supplements use the synthetic folic acid form, whereas folate from food and our Vessel fuel supplements are in the active tetrahydrofolate (THF) form also known as 5-MTHF. We cannot make it on our own, so we are totally dependent on either dietary folate or folate produced by our gut bacteria.

**references:**

-   Nutrition in Clinical Practice: A Comprehensive, Evidence-Based Manual for the Practitioner (Nutrition in Clinical Practice), 2nd Edition by David Katz. (many additional references listed on page 518)
