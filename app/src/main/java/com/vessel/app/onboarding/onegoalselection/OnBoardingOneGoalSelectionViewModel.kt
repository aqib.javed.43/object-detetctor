package com.vessel.app.onboarding.onegoalselection

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.goalsselection.ui.GoalSelectAdapter
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.util.ResourceRepository
import java.util.*

class OnBoardingOneGoalSelectionViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    val homeTabsManager: HomeTabsManager,
    private val contactManager: ContactManager,
    private val preferencesRepository: PreferencesRepository,
) : BaseViewModel(resourceProvider),
    GoalSelectAdapter.OnActionHandler,
    ToolbarHandler {
    val bgColor = if (preferencesRepository.isExistingUserFirstTime) R.color.beige_color else R.color.primaryLight
    val state = OnBoardingOneGoalSelectionState()
    private val userChosenGoals = goalsRepository.getUserChosenGoals()

    init {
        state.items.value = goalsRepository.getGoals()
            .filter { goal -> userChosenGoals.any { it.id == goal.key.id } }
            .map { entry ->
                entry.key.let { goal ->
                    GoalSelect(
                        id = goal.id,
                        title = goal.title,
                        recommendation1 = goal.recommendation1,
                        recommendation2 = goal.recommendation2,
                        background = R.drawable.yellow_light_background,
                        image = goal.icon,
                        checked = false,
                        checkBoxBackground = R.drawable.green_circle,
                    )
                }
            }
    }

    override fun onGoalSelectClicked(item: GoalSelect) {
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, goalSelect ->
                    if (goalSelect.id == item.id) goalSelect.copy(checked = item.checked.not())
                    else goalSelect.copy(checked = false)
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        val selectedItem = state.items.value?.firstOrNull { it.checked }
        if (selectedItem != null) {
            val goals = goalsRepository.getSortedGoals()
            val indexOfMainGoal = goals.indexOfFirst { it.id == selectedItem.id }
            Collections.swap(
                goals,
                0,
                indexOfMainGoal
            )
            goalsRepository.setUserMainGoal(selectedItem.id)
            goalsRepository.setGoalsOrder(
                goals.map { it.id }
            )
            state.navigateTo.value = OnBoardingOneGoalSelectionFragmentDirections.actionOnBoardingOneGoalSelectionFragmentToRateMainGoalSelectionFragment()
        }
    }
}
