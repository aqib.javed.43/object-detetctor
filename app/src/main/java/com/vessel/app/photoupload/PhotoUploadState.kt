package com.vessel.app.photoupload

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PhotoUploadState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: PhotoUploadState.() -> Unit) = apply(block)
}
