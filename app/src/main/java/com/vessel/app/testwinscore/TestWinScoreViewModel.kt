package com.vessel.app.testwinscore

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ScoreManager
import com.vessel.app.common.model.Goal
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.score.ui.ScoreGoalHeaderAdapter
import com.vessel.app.testwinscore.model.TestWinScoreGoalItem
import com.vessel.app.testwinscore.model.TestWinScoreWellnessItem
import com.vessel.app.testwinscore.ui.TestWinScoreFragmentDirections
import com.vessel.app.testwinscore.ui.TestWinScoreWellnessAdapter
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import kotlinx.coroutines.launch
import kotlin.math.max

class TestWinScoreViewModel @ViewModelInject constructor(
    val state: TestWinScoreState,
    resourceProvider: ResourceRepository,
    private val scoreManager: ScoreManager,
    private val goalsRepository: GoalsRepository,
    val preferencesRepository: PreferencesRepository,
    val homeTabsManager: HomeTabsManager,
) : BaseViewModel(resourceProvider),
    TestWinScoreWellnessAdapter.OnActionHandler,
    ScoreGoalHeaderAdapter.OnActionHandler {

    init {
        loadScores()
    }

    private fun loadScores() {
        showLoading()
        viewModelScope.launch {
            scoreManager.getWellnessScores(true)
                .onSuccess { score ->
                    hideLoading()
                    var position = 0
                    state {
                        scoreWellnessItems.value = listOf(
                            TestWinScoreWellnessItem(
                                score.wellness.last().y.toInt(),
                                max(
                                    score.wellness.last().y.toInt() - (
                                        score.wellness.getOrNull(
                                            score.wellness.lastIndex - 1
                                        )?.y?.toInt() ?: 0
                                        ),
                                    0
                                )
                            )
                        )

                        scoreGoalItems.value = goalsRepository.getGoals()
                            .map {
                                val lastGoalIndex = score.goals[it.key]?.lastIndex ?: 1
                                val winScore = (
                                    score.goals[it.key]?.lastOrNull()?.y
                                        ?: 0f
                                    ).toInt() - (
                                    score.goals[it.key]?.getOrNull(lastGoalIndex - 1)?.y
                                        ?: 0f
                                    ).toInt()
                                TestWinScoreGoalItem(
                                    scoreValue = score.goals[it.key]?.lastOrNull()?.y?.toInt() ?: 0,
                                    winScoreValue = winScore,
                                    title = it.key.title,
                                    background = Goal.alternateBackground(position++),
                                    image = it.key.icon
                                )
                            }.filter {
                                it.winScoreValue > 0
                            }.toList().apply {
                                if (this.isEmpty()) {
                                    emptyWinScoreItems.call()
                                }
                            }
                    }
                }
                .onError {
                    hideLoading()
                    // todo handle error case
                }
        }
    }

    override fun onWellnessScoreToolTipClicked(view: View) {
        view.findNavController()
            .navigate(
                TestWinScoreFragmentDirections.actionTestWinScoreFragmentToScoreDialogFragment(
                    true
                )
            )
    }

    override fun onGoalScoreHeaderToolTipClicked(view: View) {
        view.findNavController()
            .navigate(
                TestWinScoreFragmentDirections.actionTestWinScoreFragmentToScoreDialogFragment(
                    false
                )
            )
    }

    fun onBackClicked() {
        navigateBack()
    }

    fun onContinueClicked(view: View) {
        when (preferencesRepository.testCompletedCount) {
            1 -> {
                view.findNavController()
                    .navigate(
                        TestWinScoreFragmentDirections.actionTestWinScoreFragmentToCreatePlanRecommendationFragment(
                            RecommendationType.Tip
                        )
                    )
            }
            2 -> {
                view.findNavController()
                    .navigate(TestWinScoreFragmentDirections.actionTestWinScoreFragmentToAddSupplementsFragment())
            }
            3 -> {
                view.findNavController()
                    .navigate(TestWinScoreFragmentDirections.actionTestWinScoreFragmentToPostTestNutritionCoachFragment())
            }
            else -> {
                homeTabsManager.clear()
                view.findNavController().navigate(PostTestNavGraphDirections.actionGoToHome())
            }
        }
    }
}
