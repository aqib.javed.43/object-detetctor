package com.vessel.app.homescreen

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class HomeScreenPagerAdapter(fragment: Fragment) :
    FragmentStateAdapter(fragment.childFragmentManager, fragment.lifecycle) {

    private val fragmentList = mutableListOf<Fragment>()

    fun addFragment(fragment: Fragment) {
        fragmentList.add(fragment)
        notifyDataSetChanged()
    }

    fun addFragment(fragment: Fragment, position: Int) {
        fragmentList.add(position, fragment)
        notifyDataSetChanged()
    }

    fun addFragments(fragments: List<Fragment>) {
        try {
            fragmentList.addAll(fragments)
        } catch (e: Exception) {
            Log.e("HomeScreenPagerAdapter:", e.toString())
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }
}
