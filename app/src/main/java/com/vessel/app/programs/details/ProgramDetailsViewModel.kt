package com.vessel.app.programs.details

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.checkscience.ui.OnActionHandler
import com.vessel.app.common.adapter.genericlikeordislikeadapter.LikeOrDislikeAdapter
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.impactgoals.ImpactGoalsAdapter
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Program
import com.vessel.app.common.model.SchedulePlanItem
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.homescreen.MainPagerFragmentTab
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.programs.details.adapters.*
import com.vessel.app.programs.details.uimodels.ProgramScheduleUiModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.coroutines.launch

class ProgramDetailsViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val recommendationManager: RecommendationManager,
    private val programManager: ProgramManager,
    private val planManager: PlanManager,
    private val trackingManager: TrackingManager,
    private val tipMapper: TipMapper
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    OnActionHandler,
    ImpactGoalsAdapter.OnActionHandler,
    ProgramScheduleAdapter.OnActionHandler,
    ProgramHeaderAdapter.OnActionHandler,
    RecommendedByAdapter.OnActionHandler,
    LikeOrDislikeAdapter.OnActionHandler<Program>,
    ProgramFooterAdapter.OnActionHandler,
    EssentialAdapter.OnActionHandler,
    ProgramSchedulePlanItemAdapter.OnActionHandler {

    val state = ProgramDetailsState(savedStateHandle.get<Program>("program")!!)

    init {
        getProgramDetails()
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun getProgramDetails() {
        showLoading()
        logProgramDetailsView(state.passedProgram)
        viewModelScope.launch {
            programManager.getProgramDetails(state.passedProgram.id)
                .onSuccess { program ->
                    state.program.value = program
                    hideLoading(false)
                }
                .onServiceError {
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    private fun logProgramDetailsView(program: Program) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PROGRAM_DETAILS_VIEWED)
                .addProperty(
                    TrackingConstants.KEY_ID,
                    program.id.toString()
                )
                .addProperty(TrackingConstants.IS_ENROLLED, program.isEnrolled.toString())
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun markRemoved() {
        state.program.value = state.program.value?.copy(enrolledDate = null, isEnrolled = false)
            ?.apply { reBuildScheduleUiModel() }
    }

    override fun onJoinedTheProgramClicked(item: Program) {
        state.navigateTo.value =
            ProgramDetailsFragmentDirections.actionProgramDetailsFragmentToJoinProgramFragment(
                item,
                Constants.PROGRAM_DETAILS_PAGE
            )
    }

    override fun onViewYourPlanClicked(item: Program) {
        onViewYourPlanClicked()
    }

    override fun onViewYourPlanClicked(item: ProgramScheduleUiModel) {
        onViewYourPlanClicked()
    }

    private fun onViewYourPlanClicked() {
        state.navigateTo.value =
            ProgramDetailsFragmentDirections.globalActionToWellness(MainPagerFragmentTab.PLAN)
    }

    override fun onLikeClicked(header: Program) {
        val likesCount = header.totalLikes
        val newLikesCount: Int
        if (header.likeStatus != LikeStatus.LIKE) {
            newLikesCount = likesCount.plus(1)
            sendLikeStatus(header, LikeStatus.LIKE, newLikesCount)
        } else {
            newLikesCount = likesCount.minus(1)
            unLikeStatus(header, newLikesCount)
        }
    }

    override fun onDisLikeClicked(header: Program) {
        val likesCount = header.totalLikes
        val newLikesCount: Int
        if (header.likeStatus != LikeStatus.DISLIKE) {
            newLikesCount =
                when (header.likeStatus) {
                    LikeStatus.NO_ACTION -> likesCount
                    LikeStatus.LIKE -> likesCount.minus(1)
                    else -> return
                }
            sendLikeStatus(header, LikeStatus.DISLIKE, newLikesCount)
        } else {
            unLikeStatus(header, likesCount)
        }
    }

    private fun sendLikeStatus(program: Program, likeStatus: LikeStatus, likesCount: Int) {
        state.program.value =
            state.program.value?.copy(likeStatus = likeStatus, totalLikes = likesCount)
        saveLikeState(likeStatus, likesCount)
        viewModelScope.launch {
            recommendationManager.sendLikesStatus(
                program.id,
                LikeCategory.Program.value,
                likeStatus == LikeStatus.LIKE
            )
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CONTENT_LIKED)
                .addProperty(TrackingConstants.KEY_ID, program.id.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Program.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun unLikeStatus(
        program: Program,
        likesCount: Int,
        likeStatus: LikeStatus = LikeStatus.NO_ACTION
    ) {
        state.program.value =
            state.program.value?.copy(likeStatus = likeStatus, totalLikes = likesCount)
        saveLikeState(likeStatus, likesCount)
        viewModelScope.launch {
            recommendationManager.unLikesStatus(
                program.id,
                LikeCategory.Program.value
            )
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CONTENT_UNLIKED)
                .addProperty(TrackingConstants.KEY_ID, program.id.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Program.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    override fun onLinkButtonClick(view: View, link: String) {
        state.navigateTo.value = HomeNavGraphDirections.globalActionToWeb(link)
    }

    override fun onGoalItemClick(item: ImpactGoalModel, view: View) {}

    override fun onItemLikeClicked(item: Program) {
        var likesCount = item.totalLikes
        val updatedLikeFlag = item.likeStatus != LikeStatus.LIKE
        if (updatedLikeFlag) likesCount++ else likesCount--
        if (updatedLikeFlag) sendLikeStatus(
            item,
            LikeStatus.fromBoolean(updatedLikeFlag),
            likesCount
        )
        else unLikeStatus(item, likesCount)
    }

    override fun onExpertClicked() {
        val expertContact = state.program.value?.contact
        if (expertContact != null) {
            state.navigateTo.value =
                HomeNavGraphDirections.globalActionToExpertProfileFragment(expertContact)
        }
    }

    private fun saveLikeState(likeStatus: LikeStatus, likesCount: Int) {
        savedStateHandle.get<Program>("program")?.totalLikes = likesCount
        savedStateHandle.get<Program>("program")?.likeStatus = likeStatus
    }

    override fun onLeaveProgramClicked(item: Program) {
        state.navigateTo.value =
            ProgramDetailsFragmentDirections.actionProgramDetailsFragmentToLeaveProgramDialogFragment(
                item
            )
    }

    override fun onShowMoreClicked() {
        state.program.value?.getScheduleUiModel()?.programScheduleList?.let {
            logProgramScheduleView(state.program.value!!)
            state.navigateTo.value =
                ProgramDetailsFragmentDirections.actionProgramDetailsFragmentToProgramScheduleFragment(
                    state.program.value!!
                )
        }
    }

    private fun logProgramScheduleView(program: Program) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PROGRAM_SCHEDULE_VIEW)
                .addProperty(TrackingConstants.KEY_ID, program.id.toString())
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    override fun onSchedulePlanItemClicked(item: SchedulePlanItem) {
        state.program.value?.planData?.firstOrNull { it.tip?.title == item.title }?.tip?.let {
            state.navigateTo.value =
                ProgramDetailsFragmentDirections.actionProgramDetailsFragmentToTipDetailsDialogFragment(
                    tipMapper.mapTipData(it)
                )
        }
    }

    override fun onSchedulePlanItemTimerClicked(item: SchedulePlanItem) {
        viewModelScope.launch {
            planManager.getUserPlans().onSuccess { plans ->
                plans.filter { it.getDisplayName() == item.title }.let { tipPlans ->
                    val planReminderHeader = PlanReminderHeader(
                        item.title,
                        "",
                        tipPlans.firstOrNull()?.getToDoImageUrl(),
                        null,
                        tipPlans
                    )
                    state.navigateTo.value =
                        ProgramDetailsFragmentDirections.actionProgramDetailsFragmentToPlanReminderDialog(
                            planReminderHeader
                        )
                }
            }
        }
    }

    override fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int) {
    }

    override fun showEssentialInformation() {
    }
}
