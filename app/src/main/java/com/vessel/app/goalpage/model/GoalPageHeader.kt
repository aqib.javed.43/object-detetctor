package com.vessel.app.goalpage.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.common.model.DateEntry
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GoalPageHeader(
    @StringRes val title: Int? = 0,
    val chartEntries: List<DateEntry>,
    @DrawableRes val background: Int? = 0
) : Parcelable
