package com.vessel.app.appreview.review

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_height_dialog.*
import kotlinx.android.synthetic.main.fragment_review_dialog.*

@AndroidEntryPoint
class AppReviewDialogFragment : BaseDialogFragment<AppReviewDialogViewModel>() {
    override val viewModel: AppReviewDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_review_dialog
    private val teamPostAdapter: ReviewChoiceAdapter by lazy { ReviewChoiceAdapter(viewModel) }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                0
            )
            setLayout(width, height)
            setGravity(Gravity.BOTTOM)
            setBackgroundDrawable(inset)
        }
        isCancelable = false

        observe(viewModel.dismissDialog) { dismiss() }
        observe(viewModel.navigateTo) {
            dismiss()
            findNavController().navigate(it)
        }
        setupViews()
    }

    private fun setupViews() {
        selectButton.setOnClickListener {
            viewModel.processFeedBack()
        }
        setupList()
    }

    private fun setupList() {
        rcvReviews.adapter = ConcatAdapter(
            ConcatAdapter.Config.Builder()
                .setIsolateViewTypes(false)
                .build(),
            teamPostAdapter
        )
    }
}
