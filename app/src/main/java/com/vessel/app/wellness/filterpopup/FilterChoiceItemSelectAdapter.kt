package com.vessel.app.wellness.filterpopup

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class FilterChoiceItemSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<ItemFilterChoice>() {
    override fun getLayout(position: Int) = R.layout.item_choice_filter_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onFilterSelectChecked(item: ItemFilterChoice, checked: Boolean)
    }
}
