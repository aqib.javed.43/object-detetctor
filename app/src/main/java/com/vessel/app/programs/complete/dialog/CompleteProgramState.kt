package com.vessel.app.programs.complete.dialog

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.common.net.data.ProgramSummaryData
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CompleteProgramState @Inject constructor(val passedProgram: Program) {
    val openMessaging = LiveEvent<Any>()
    val program = MutableLiveData(passedProgram)
    val dismissDialog = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()
    val programSummaryData = MutableLiveData<ProgramSummaryData>()

    operator fun invoke(block: CompleteProgramState.() -> Unit) = apply(block)
}
