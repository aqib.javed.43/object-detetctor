package com.vessel.app.emailcheck

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.BuildConfig
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.AuthToken
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.util.extensions.isSameDay
import com.vessel.app.util.validators.EmailValidator
import com.vessel.app.util.validators.ValidatorException
import kotlinx.coroutines.launch
import java.util.*

class EmailCheckViewModel @ViewModelInject constructor(
    val state: EmailCheckState,
    resourceRepository: ResourceRepository,
    private val authManager: AuthManager,
    private val trackingManager: TrackingManager,
    private val contactManager: ContactManager
) : BaseViewModel(resourceRepository), ToolbarHandler {
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
    val today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())

    fun onAuthTokenReceived(authToken: AuthToken) {
        showLoading()

        viewModelScope.launch {
            authManager.onAppleGoogleAuth(
                authToken
            )
                .onSuccess { contact ->
                    hideLoading()
                    if (contact.insertDate.isSameDay(today)) {
                        state.navigateTo.value = EmailCheckFragmentDirections.actionEmailCheckFragmentToCardChoicesFragment(
                            "",
                            true
                        )
                        authManager.logout()
                    } else {
                        state.userEmail.value = contact.email
                        checkEmailExistence()
                    }
                }
                .onServiceError {
                    val errorMessage = it.error.message ?: getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onNetworkIOError {
                    val errorMessage = getResString(R.string.network_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onUnknownError {
                    val errorMessage = getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun continueClicked() {
        if (!validateParams()) {
            return
        }
        checkEmailExistence()
    }

    fun onPrivacyClicked() {
        state.navigateTo.value =
            EmailCheckFragmentDirections.actionEmailCheckFragmentToWeb(BuildConfig.PRIVACY_POLICY_URL)
    }

    fun onTermsClicked() {
        state.navigateTo.value =
            EmailCheckFragmentDirections.actionEmailCheckFragmentToWeb(BuildConfig.TERMS_URL)
    }

    private fun checkEmailExistence() {
        showLoading()
        viewModelScope.launch {
            contactManager.checkEmailExistence(email = state.userEmail.value!!)
                .onSuccess {
                    hideLoading(false)
                    state.navigateTo.value = if (it.exists == true) {
                        EmailCheckFragmentDirections.actionEmailCheckFragmentToSignInFragment(state.userEmail.value!!)
                    } else {
                        EmailCheckFragmentDirections.actionEmailCheckFragmentToCardChoicesFragment(
                            state.userEmail.value!!,
                            false
                        )
                    }
                }
                .onServiceError {
                    val errorMessage = it.error.message ?: getResString(R.string.unknown_error)
                    showError(errorMessage)
                }
                .onNetworkIOError {
                    val errorMessage = getResString(R.string.network_error)
                    showError(errorMessage)
                }
                .onUnknownError {
                    val errorMessage = getResString(R.string.unknown_error)
                    showError(errorMessage)
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun validateParams() = try {
        EmailValidator().validate(state.userEmail)
    } catch (exception: ValidatorException) {
        showError(exception)
        false
    }

    private fun logFailureEvent(errorMessage: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SSO_SIGN_UP_FAIL)
                .setError(errorMessage)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
