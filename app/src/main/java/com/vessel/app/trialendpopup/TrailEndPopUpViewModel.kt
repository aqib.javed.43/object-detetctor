package com.vessel.app.trialendpopup

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class TrailEndPopUpViewModel @ViewModelInject constructor(
    val state: TrialEndPopUpState,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    @Assisted savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()

    fun onActive(view: View) {
    }

    fun dismiss() {
        dismissDialog.call()
    }
}
