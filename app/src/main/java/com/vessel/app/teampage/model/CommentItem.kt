package com.vessel.app.teampage.model

data class CommentItem(
    val name: String = "",
    val comment: String = "",
    val isMyComment: Boolean = false,
    var isLiked: Boolean = false

)
