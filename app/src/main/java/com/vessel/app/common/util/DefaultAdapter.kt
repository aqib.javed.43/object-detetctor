package com.vessel.app.common.util

import androidx.annotation.LayoutRes
import androidx.lifecycle.LifecycleOwner
import com.vessel.app.base.BaseAdapter
import com.vessel.app.base.MutableLiveArrayList

class DefaultAdapter(
    items: MutableLiveArrayList<String>,
    lifecycleOwner: LifecycleOwner,
    @LayoutRes val layout: Int
) : BaseAdapter<BaseAdapter.ViewHolder>(items, lifecycleOwner) {
    override fun getLayout(position: Int) = layout
}
