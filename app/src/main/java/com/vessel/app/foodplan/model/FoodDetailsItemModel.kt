package com.vessel.app.foodplan.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FoodDetailsItemModel(
    val title: String,
    val description: String,
    val foodId: Int,
    val isAdded: Boolean
) : Parcelable
