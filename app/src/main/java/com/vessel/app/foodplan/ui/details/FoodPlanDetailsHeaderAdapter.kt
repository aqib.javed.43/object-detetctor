package com.vessel.app.foodplan.ui.details

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodplan.model.FoodPlanDetailsHeader

class FoodPlanDetailsHeaderAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<FoodPlanDetailsHeader>() {

    override fun getLayout(position: Int) = R.layout.item_food_plan_details_header

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onAddToPlanClicked(item: FoodPlanDetailsHeader)
        fun onCheckFoodPlanClicked()
        fun onItemLikeClicked(item: FoodPlanDetailsHeader)
        fun onDisLikeClicked(item: FoodPlanDetailsHeader)
    }
}
