package com.vessel.app.foodplan.ui

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodplan.model.FoodPlanItem
import com.vessel.app.util.extensions.afterMeasured
import com.vessel.app.views.QuantityWidget

class FoodItemsAdapter(private val handler: OnActionHandler, private val parentPosition: Int) :
    BaseAdapterWithDiffUtil<FoodPlanItem>() {

    override fun getLayout(position: Int) = when (getItem(position)) {
        is FoodPlanItem -> R.layout.item_plan_food
        else -> throw IllegalStateException(
            "Unexpected SupplementAdapter type at position $position for item ${getItem(position)}"
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<FoodPlanItem>, position: Int) {
        super.onBindViewHolder(holder, position)

        val item = getItem(position) as FoodPlanItem

        holder.itemView.findViewById<SwipeLayout>(R.id.swipe).apply {
            isSwipeEnabled = item.canSwipe
            addSwipeListener(object : SimpleSwipeListener() {
                override fun onStartOpen(layout: SwipeLayout?) {
                    super.onStartOpen(layout)
                    holder.itemView.findViewById<View>(R.id.dummyBackgroundView).isVisible = true
                }

                override fun onClose(layout: SwipeLayout?) {
                    super.onClose(layout)
                    holder.itemView.findViewById<View>(R.id.dummyBackgroundView).isVisible = false
                }
            })
        }

        holder.itemView.findViewById<View>(R.id.deleteSwipe).setOnClickListener {
            handler.onItemDeleteClicked(item, it, position, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.background).setOnClickListener {
            handler.onPlanItemClicked(item, it, parentPosition)
        }
        holder.itemView.findViewById<View>(R.id.actionButton).setOnClickListener {
            handler.onPlanItemClicked(item, it, parentPosition)
        }
        holder.itemView.findViewById<View>(R.id.actionAddButton).setOnClickListener {
            handler.onPlanItemClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.editSwipe).setOnClickListener {
            handler.onItemEditClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.background).setOnClickListener {
            handler.onItemSelected(item, it, position, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.swipeView).afterMeasured {
            this.apply {
                updateLayoutParams {
                    val findViewById = holder.itemView.findViewById<View>(R.id.dummyBackground)
                    height = findViewById.height
                    y = findViewById.y
                }
            }
        }

        holder.itemView.findViewById<QuantityWidget>(R.id.quantity_widget).apply {
            setMinQuantity(item.currentPlan.getQuantityScale().toFloat())
            setMaxQuantity(
                item.currentPlan.getQuantityScale()
                    .toFloat() * item.currentPlan.getQuantityMaxMultiple()
            )
            setQuantityUnit(item.currentPlan.getQuantityScale().toFloat())
            setQuantityDescription(item.currentPlan.getQuantityUnit())
            setQuantity(
                item.currentPlan.getQuantityScale().toFloat() * (item.currentPlan.multiple ?: 0)
            )
            setSubtractClickListener {
                this@FoodItemsAdapter.handler.onSubtractQuantityClicked(
                    item,
                    (it / item.currentPlan.getQuantityScale().toFloat()).toInt(),
                    this,
                    position,
                    this@FoodItemsAdapter.parentPosition
                )
            }
            setAddClickListener {
                this@FoodItemsAdapter.handler.onAddQuantityClicked(
                    item,
                    (it / item.currentPlan.getQuantityScale().toFloat()).toInt(),
                    this,
                    position,
                    this@FoodItemsAdapter.parentPosition
                )
            }
        }

        holder.itemView.findViewById<ConstraintLayout>(R.id.groupBackground).apply {
            isVisible = item.isGrouped && item.groupCount > 1
            findViewById<TextView>(R.id.planItemsCount).apply {
                text = "+ ${item.groupCount - 1}"
                setOnClickListener {
                    this@FoodItemsAdapter.handler.onGroupedItemsClicked(
                        item,
                        it,
                        this@FoodItemsAdapter.parentPosition
                    )
                }
            }
        }
    }

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onPlanItemClicked(item: FoodPlanItem, view: View, parentPosition: Int)
        fun onItemSelected(
            item: FoodPlanItem,
            view: View,
            position: Int,
            parentPosition: Int
        )

        fun onItemDeleteClicked(
            item: FoodPlanItem,
            view: View,
            position: Int,
            parentPosition: Int
        )

        fun onItemEditClicked(item: FoodPlanItem, view: View, parentPosition: Int)
        fun onGroupedItemsClicked(item: FoodPlanItem, view: View, parentPosition: Int)
        fun onAddQuantityClicked(
            item: FoodPlanItem,
            multiple: Int,
            view: View,
            position: Int,
            parentPosition: Int
        )

        fun onSubtractQuantityClicked(
            item: FoodPlanItem,
            multiple: Int,
            view: View,
            position: Int,
            parentPosition: Int
        )
    }
}
