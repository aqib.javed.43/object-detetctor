package com.vessel.app.common.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.vessel.app.checkin.checkintips.TipsCheckInProgramAdapter
import com.vessel.app.databinding.WidgetProgramCardBinding
import com.vessel.app.wellness.model.RecommendationItem

class TipsProgramCardWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private lateinit var binding: WidgetProgramCardBinding
    private lateinit var programAdapter: TipsCheckInProgramAdapter
    private var handler: TipsProgramCardWidgetOnActionHandler? = null

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetProgramCardBinding.inflate(layoutInflater, this, true)
    }

    fun setHandler(programsHandler: TipsProgramCardWidgetOnActionHandler) {
        handler = programsHandler
        if (::programAdapter.isInitialized.not()) {
            programAdapter = TipsCheckInProgramAdapter(programsHandler)
            binding.programsList.apply {
                layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = programAdapter
                itemAnimator = null
            }
            binding.listIndicator.attachToRecyclerView(binding.programsList)
            val snapHelper = PagerSnapHelper()
            snapHelper.attachToRecyclerView(binding.programsList)
        }
    }

    fun setList(programList: List<RecommendationItem>?) {
        if (programList == null)
            binding.programsList.scrollToPosition(0)
        programAdapter.submitList(programList)
        programAdapter.notifyDataSetChanged()
    }
}

interface TipsProgramCardWidgetOnActionHandler {
    fun onProgramItemClicked(program: RecommendationItem)
    fun onProgramInfoClicked(program: RecommendationItem)
    fun onJoinProgramClicked(program: RecommendationItem)
    fun onItemLikeClicked(item: RecommendationItem)
}
