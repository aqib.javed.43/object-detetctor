package com.vessel.app.filterfoodplandialog.ui

import android.content.Context
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.R

class FilterSpanSizeLookup(
    context: Context,
    private val adapter: ConcatAdapter
) : GridLayoutManager.SpanSizeLookup() {
    private val spanCount = context.resources.getInteger(R.integer.food_filters_list_span_count)

    override fun getSpanSize(position: Int): Int {
        return when (adapter.getItemViewType(position)) {
            R.layout.item_filter_header -> spanCount
            R.layout.item_filter_footer -> spanCount
            else -> {
                if (LARGE_TITLE_POSITIONS.contains(position)) 3 else 2
            }
        }
    }

    companion object {
        // due to static list items and these items will not changed and any update in the filters will be from frontend side
        // We save the big titles positions to show it in big row and normal items to be shown in smaller one
        // to avoid using multiple recyclers or flexbox which will not achieve our needs
        private val LARGE_TITLE_POSITIONS = mutableListOf(1, 4, 6, 9, 11)
    }
}
