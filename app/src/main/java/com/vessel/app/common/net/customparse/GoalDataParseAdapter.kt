package com.vessel.app.common.net.customparse

import com.squareup.moshi.*
import com.vessel.app.common.net.data.SupplementAssociatedGoalData
import org.json.JSONObject
import java.lang.NullPointerException

/***
 *  No need to stick to the BE hierarchy since it duplicated thing like goal inside goal
 * Sample of Goal Data
 * {
 *  "impact": null,
 *  "goal_id": 3,
 *      "goal": {
 *      "name": "Calm",
 *      "id": 3
 *      }
 *  }
 */
class GoalDataParseAdapter {

    @FromJson
    fun fromJson(reader: Map<String, *>): SupplementAssociatedGoalData {
        val jsonObject = JSONObject(reader)
        var impact: Int? = null
        val id: Int = jsonObject.getInt(KEY_ID)
        if (jsonObject.has(KEY_IMPACT) && jsonObject.isNull(KEY_IMPACT).not()) {
            impact = jsonObject.getInt(KEY_IMPACT)
        }
        val goal: JSONObject = jsonObject.getJSONObject(KEY_GOAL)
        val name: String = goal.getString(KEY_NAME)
        return SupplementAssociatedGoalData(
            id = id,
            name = name,
            impact = impact
        )
    }

    @ToJson
    fun toJson(value: SupplementAssociatedGoalData?): String {
        if (value == null) {
            throw NullPointerException("value was null! Wrap in .nullSafe() to write nullable values.")
        }
        val writer = JSONObject()
        writer.put(KEY_ID, value.id)
        writer.put(KEY_IMPACT, value.impact)
        val goalObject = JSONObject()
        goalObject.put(KEY_NAME, value.name)
        writer.put(KEY_GOAL, goalObject)
        return writer.toString()
    }

    companion object {
        const val KEY_ID = "goal_id"
        const val KEY_NAME = "name"
        const val KEY_GOAL = "goal"
        const val KEY_IMPACT = "impact"
    }
}
