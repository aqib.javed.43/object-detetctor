package com.vessel.app.appreview.secondstep

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class AppReviewSecondStepDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    val gotToPlayStore = LiveEvent<Unit>()

    fun dismiss() {
        devicePreferencesRepository.showAppReviewPopup = false
        logNavigationEvent(false)
        dismissDialog.call()
    }

    fun goToPlayStore() {
        devicePreferencesRepository.showAppReviewPopup = false
        logNavigationEvent(true)
        gotToPlayStore.call()
    }

    private fun logNavigationEvent(selectGo: Boolean) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.APP_REVIEW_GO_TO_STORE)
                .addProperty(TrackingConstants.KEY_SELECT_GO, selectGo.toString())
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
