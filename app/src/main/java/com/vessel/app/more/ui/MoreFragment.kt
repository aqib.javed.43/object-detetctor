package com.vessel.app.more.ui

import android.content.Intent
import android.os.Bundle
import android.widget.PopupWindow
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.livechatinc.inappchat.ChatWindowActivity
import com.livechatinc.inappchat.ChatWindowConfiguration
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.more.MoreViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity

@AndroidEntryPoint
class MoreFragment : BaseFragment<MoreViewModel>() {
    override val viewModel: MoreViewModel by viewModels()
    override val layoutResId = R.layout.fragment_more
    private lateinit var popupWindow: PopupWindow
    private val moreAdapter by lazy { MoreAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        view?.findViewById<RecyclerView>(R.id.moreList)?.adapter = moreAdapter
        setupPopup()
        setupObservers()
    }

    fun setupObservers() {
        viewModel.state.apply {
            observe(moreItems) {
                moreAdapter.submitList(it)
            }
            observe(navigateTo) {
                findNavController()
                    .navigate(it)
            }
            observe(openMessaging) {
                if (viewModel.isLiveChat()) {
                    val intent = Intent(requireContext(), ChatWindowActivity::class.java)
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_GROUP_ID,
                        BuildConfig.NUTRITIONIST_COACH_GROUP
                    )
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_LICENCE_NUMBER,
                        BuildConfig.LIVE_CHAT_LICENSE
                    )
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_VISITOR_NAME,
                        viewModel.getUserName()
                    )
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_VISITOR_EMAIL,
                        viewModel.getUserEmail()
                    )
                    requireContext().startActivity(intent)
                } else {
                    MessagingActivity.builder()
                        .withToolbarTitle(getString(R.string.chat_to_nutritionist))
                        .withEngines(ChatEngine.engine())
                        .show(requireContext(), viewModel.getChatConfiguration())
                }
            }
        }
    }

    fun setupPopup() {
    }
}
