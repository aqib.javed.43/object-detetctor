package com.vessel.app.onboarding.goals.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.onboarding.goals.OnboardingGoalsViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingGoalsFragment : BaseFragment<OnboardingGoalsViewModel>() {
    override val viewModel: OnboardingGoalsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_goals_onboarding

    val onboardingGoalSelectAdapter by lazy { OnboardingGoalSelectAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        val goalsList = requireView().findViewById<RecyclerView>(R.id.goalsList)
        goalsList.layoutManager = GridLayoutManager(context, Constants.SPAN_COUNT_GOAL_GRID)
        goalsList.adapter = onboardingGoalSelectAdapter
        setupObservers()
    }

    override fun onResume() {
        super.onResume()
        viewModel.mapGoals()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(items) {
                onboardingGoalSelectAdapter.submitList(it)
            }
            observe(refreshItems) {
                onboardingGoalSelectAdapter.notifyDataSetChanged()
            }
        }
    }
}
