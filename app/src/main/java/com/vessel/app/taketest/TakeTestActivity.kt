package com.vessel.app.taketest

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.Navigation
import com.vessel.app.R
import com.vessel.app.activationtimer.util.TimerServiceManager
import com.vessel.app.base.BaseActivity
import com.vessel.app.util.extensions.applyFullscreenFlags
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TakeTestActivity : BaseActivity<TakeTestViewModel>() {
    public override val viewModel: TakeTestViewModel by viewModels()
    override val layoutResId = R.layout.activity_take_test

    override fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        TimerServiceManager.bindService(this)
        applyFullscreenFlags()
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        Navigation.findNavController(this, R.id.fragmentContainerView)
            .addOnDestinationChangedListener { _, destination, _ ->
                requestedOrientation = when (destination.id) {
                    R.id.captureFragment -> ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                    else -> ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
                }
            }
    }

    override fun onStop() {
        super.onStop()
        if (viewModel.preferencesRepository.enableActivationTimerNotification) {
            TimerServiceManager.startForeground()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        TimerServiceManager.killService()
    }
}
