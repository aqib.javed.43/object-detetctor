-   Certain medications such as aspirin, aminopyrine, barbiturates, hydantoins and paraldehyde as well as cold or heat stress are known to increase the excretion of vitamin C in the urine.
-   
    [https://drive.google.com/file/d/17colxudi6NdXyOjKwFt0mxNCB9gsCfAf/view](https://drive.google.com/file/d/17colxudi6NdXyOjKwFt0mxNCB9gsCfAf/view)
-
    Jacob RA: Assessment of human vitamin C status, J Nutr, 120(suppl 11): 1480, 1990.
-
    Washko PW, et al: Ascorbic acid and dehydro-ascorbic acid analysis in biological samples.  Analyt Biochem, 204:1, 1992.
-   Warfarin: Large doses of vitamin C may block the action of warfarin and thus lower its effectiveness. Individuals on anticoagulants should limit their vitamin C intake to <1 g/day and have their prothrombin time monitored by the clinician following their anticoagulant therapy.
-   
    Natural Medicines. Vitamin C/Professional handout/Interactions with drugs. Available at: https://naturalmedicines.therapeuticresearch.com. Accessed 7/2/18.  
-   Antacids: Vitamin C may increase the absorption of aluminum-containing antacids (e.g. Tums, Pepto-Bismol, Milk of Magnesia, Alka-Seltzer). People with impaired kidney function may be at risk for aluminum toxicity when supplemental vitamin C is taken at the same time as these compounds
- 
    Hendler SS, Rorvik DM. PDR for Nutritional Supplements. 2nd ed. Montvale: Thomson Reuters; 2008.  
- 
    Natural Medicines. Vitamin C/Professional handout/Interactions with drugs. Available at: https://naturalmedicines.therapeuticresearch.com. Accessed 7/2/18.  
-   Oral Contraceptives: supplemental vitamin C may increase blood estrogen concentrations in women using oral contraceptives or hormone replacement therapy
-
    Natural Medicines. Vitamin C/Professional handout/Interactions with drugs. Available at: https://naturalmedicines.therapeuticresearch.com. Accessed 7/2/18. 

