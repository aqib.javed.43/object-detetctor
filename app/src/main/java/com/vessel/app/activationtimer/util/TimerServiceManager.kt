package com.vessel.app.activationtimer.util

import android.content.*
import android.os.IBinder
import android.view.WindowManager
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*

object TimerServiceManager {
    var activity: FragmentActivity? = null
    private var handler: TimerService.TimerHandler? = null

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            this@TimerServiceManager.timerService = (service as TimerService.TimerBinder).getService()
            this@TimerServiceManager.timerService?.let {
                handler?.let { handler ->
                    it.setTimerHandler(handler)
                }
                bound = true
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            killService()
        }
    }

    private var timerService: TimerService? = null
    var bound = false
        private set

    fun bindService(ownerActivity: FragmentActivity) {
        if (activity == null) {
            activity = ownerActivity
        }
        activity?.apply {
            Intent(this, TimerService::class.java).also {
                bindService(it, connection, Context.BIND_AUTO_CREATE)
                activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
        }
    }

    fun startTimerCountDown() {
        timerService?.startTimerCountDown()
    }

    fun stopTimerCountDown() {
        timerService?.stopTimerCountDown()
    }

    fun forceCompleteTimerCountDown() {
        timerService?.forceCompleteTimerCountDown()
    }

    fun startForeground() {
        timerService?.startForeground()
    }

    fun stopForeground() {
        timerService?.stopForeground()
    }

    fun setHandler(actionHandler: TimerService.TimerHandler) {
        handler = actionHandler
        handler?.let {
            timerService?.setTimerHandler(it)
        }
    }

    fun unbound() {
        if (bound) {
            activity?.unbindService(connection)
            bound = false
        }
    }

    fun killService() {
        unbound()
        timerService?.removeTimerHandler()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        activity = null
        handler = null
    }
}
