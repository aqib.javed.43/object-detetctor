The best way to improve your calcium levels is with fresh whole foods because calcium in nature is accompanied by other important nutrients that assist the body in utilizing calcium. Some people who have challenges getting enough calcium from food may benefit from additional calcium in the form of supplementation. If taking supplemental calcium, try to find active forms such as calcium citrate or calcium malate to improve absorption. Also, don’t take more than 500mg of elemental calcium at one time to maximize intestinal absorption.  

Also, dietary protein leads to increased calcium loss in the urine. Every 50g increment of protein ingested leads to an additional 60 mg of calcium lost in the urine.  Sodium and caffeine also increase urinary loss of calcium.

Click on the “grow” link at the bottom of the app to see your personalized food and supplement recommendations.

Here’s how we make your food and supplement suggestions. Every 5 years the US government has a team of scientific experts review the literature on nutrition and health to determine the recommended daily intake (RDI) of many different micronutrients and publish this information for free to the public. We start here as the foundation for our recommendations. We then build upon this to customize recommendations for each user based on their Vessel test results. To determine this formula we take into account the expert opinion of our medical and scientific board of doctors, scientists, and registered dietitians looking at the medical evidence for the risks and benefits of being high vs low in a certain nutrient. For example, if a nutrient could be dangerous at high levels (such as potassium), we would never suggest higher than the US recommended daily intake (RDI).

  

**references:**

-   https://health.gov/sites/default/files/2019-09/2015-2020_Dietary_Guidelines.pdf
-   Nutrition in Clinical Practice: A Comprehensive, Evidence-Based Manual for the Practitioner (Nutrition in Clinical Practice), 2nd Edition by David Katz. 

