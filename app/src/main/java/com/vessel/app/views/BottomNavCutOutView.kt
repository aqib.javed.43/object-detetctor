package com.vessel.app.views

import android.content.Context
import android.graphics.*
import android.os.Build
import android.util.AttributeSet
import com.github.mmin18.widget.RealtimeBlurView
import com.vessel.app.R

class BottomNavCutOutView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
) : RealtimeBlurView(context, attrs) {

    private val path = Path()

    private val circleRadius = resources.getDimension(R.dimen.bottom_nav_cut_out_radius)

    override fun onDraw(canvas: Canvas?) {
        // Clears any lines and curves from the path but unlike reset(),
        // keeps the internal data structure for faster reuse.
        path.rewind()
        path.addCircle(
            width.toFloat() / 2,
            0f,
            circleRadius,
            Path.Direction.CCW
        )
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            canvas?.clipPath(path, Region.Op.DIFFERENCE)
        } else {
            canvas?.clipOutPath(path)
        }
        super.onDraw(canvas)
    }
}
