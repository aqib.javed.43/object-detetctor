package com.vessel.app.goal.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.goal.GoalViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_goal.*

@AndroidEntryPoint
class GoalFragment : BaseFragment<GoalViewModel>() {
    override val viewModel: GoalViewModel by viewModels()
    override val layoutResId = R.layout.fragment_goal

    private val goalHeaderAdapter by lazy { GoalHeaderAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        goalList.also {
            val adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                goalHeaderAdapter
            )

            it.adapter = adapter
            it.layoutManager = GridLayoutManager(requireContext(), 1)
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(goalHeaderItems) { goalHeaderAdapter.submitList(it) }
        }
    }
}
