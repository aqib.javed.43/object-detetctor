package com.vessel.app.takesurvey.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SurveyAnswer(
    val id: Int,
    val questionId: Int,
    val primaryText: String?,
    val secondaryText: String?,
    val imageUrl: String?,
    val incorrect: Boolean,
    var chainedSurveyId: String?
) : Parcelable
