package com.vessel.app.common.net.mapper

import com.vessel.app.common.net.data.SupplementAssociationData
import com.vessel.app.common.net.data.SupplementData
import com.vessel.app.common.net.data.SupplementDataResponse
import com.vessel.app.supplementsbucket.model.Supplement
import com.vessel.app.supplementsbucket.model.SupplementAssociation
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SupplementsMapper @Inject constructor(val goalsMapper: SupplementAssociatedGoalsMapper) {
    fun map(data: SupplementDataResponse) = data.supplements.map { map(it) }

    fun map(data: List<Supplement>) = data.map { map(it) }

    private fun map(data: SupplementData) = Supplement(
        id = data.id,
        name = data.name,
        warningDescription = data.warning_description,
        description = data.description,
        dosageUnit = data.dosage_unit,
        dosage = data.dosage ?: 0.0,
        volume = data.volume ?: 0.0,
        price = data.price ?: 0.0,
        supplementAssociatedGoals = data.goals_supplement.let { goalsMapper.map(it!!) },
        isMultivitamin = data.is_multivitamin ?: false,
        supplementAssociation = data.supplement_association?.map { supplementAssociationMap(it) }
    )

    private fun supplementAssociationMap(data: SupplementAssociationData) =
        SupplementAssociation(
            id = data.supplement_id_b ?: 0,
            type = data.association_type ?: ""
        )

    private fun supplementAssociationMap(data: SupplementAssociation) =
        SupplementAssociationData(
            supplement_id_b = data.id,
            association_type = data.type
        )

    private fun map(supplement: Supplement) = SupplementData(
        id = supplement.id,
        name = supplement.name,
        warning_description = supplement.warningDescription,
        description = supplement.description,
        dosage_unit = supplement.dosageUnit,
        dosage = supplement.dosage,
        volume = supplement.volume,
        price = supplement.price,
        goals_supplement = goalsMapper.mapToGoalData(supplement.supplementAssociatedGoals),
        is_multivitamin = supplement.isMultivitamin,
        supplement_association = supplement.supplementAssociation?.map { supplementAssociationMap(it) }
    )
}
