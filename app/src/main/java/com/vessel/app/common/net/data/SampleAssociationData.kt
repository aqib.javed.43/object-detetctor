package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SampleAssociationData(
    val wellness_card_batch_id: String?,
    val wellness_card_calibration_mode: String?,
    val orca_sheet_name: String?
)
