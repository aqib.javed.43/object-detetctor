package com.vessel.app.common.widget

import android.view.View

interface ToolbarHandler {
    fun onBackButtonClicked(view: View)
}
