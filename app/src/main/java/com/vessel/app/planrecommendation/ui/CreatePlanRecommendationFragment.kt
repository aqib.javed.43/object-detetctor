package com.vessel.app.planrecommendation.ui

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.home.HomeActivity
import com.vessel.app.planrecommendation.CreatePlanRecommendationViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreatePlanRecommendationFragment : BaseFragment<CreatePlanRecommendationViewModel>() {
    override val viewModel: CreatePlanRecommendationViewModel by viewModels()
    override val layoutResId = R.layout.fragment_create_plan_recommendations

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)
        viewModel.state.navigate.observe(
            viewLifecycleOwner,
            {
                navigateToHomeActivity()
            }
        )
        viewModel.state.navigateTo.observe(
            viewLifecycleOwner,
            {
                findNavController().navigate(it)
            }
        )
    }

    private fun navigateToHomeActivity() {
        val intent = Intent(requireContext(), HomeActivity::class.java)
        context?.startActivity(intent)
        activity?.finishAffinity()
    }
}
