package com.vessel.app.createpost

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.teampage.model.PostType
import com.vessel.app.util.ResourceRepository

class CreatePostViewModel@ViewModelInject constructor(
    val state: CreatePostState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler {
    override fun onBackButtonClicked(view: View) {
        view.findNavController().navigateUp()
    }
    fun onTextButtonClicked() {
        gotoCreatePost(PostType.TEXT)
    }
    fun onPhotoButtonClicked() {
        gotoMediaSelect(PostType.PHOTO)
    }
    fun onVideoButtonClicked() {
        gotoMediaSelect(PostType.VIDEO)
    }
    fun onPollButtonClicked() {
        val nav = CreatePostFragmentDirections.actionCreatePostFragmentToCreateNewPollFragment()
        state.navigateTo.value = nav
    }
    fun gotoCreatePost(type: PostType) {
        val nav = CreatePostFragmentDirections.actionCreatePostFragmentToCreateNewPostFragment(type.ordinal)
        state.navigateTo.value = nav
    }
    fun gotoMediaSelect(type: PostType) {
        val nav = CreatePostFragmentDirections.actionCreatePostFragmentToMediaSelectionFragment(type.ordinal)
        state.navigateTo.value = nav
    }
}
