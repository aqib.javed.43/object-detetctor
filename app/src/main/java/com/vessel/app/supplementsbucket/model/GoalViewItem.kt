package com.vessel.app.supplementsbucket.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class GoalViewItem(
    val id: Int? = null,
    @StringRes val title: Int,
    @DrawableRes val formulaIcon: Int
)
