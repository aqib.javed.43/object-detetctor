package com.vessel.app.views.weekdayview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import com.vessel.app.databinding.WidgetWeekDaysBinding
import com.vessel.app.util.decoration.HorizontalSpaceItemDecoration
import com.vessel.app.views.weekdays.DayOfWeekUiModel
import com.vessel.app.views.weekdays.getDaysOfWeek

class WeekDaysWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var items: List<DayOfWeekUiModel> = getDaysOfWeek(context)
    private lateinit var binding: WidgetWeekDaysBinding
    private lateinit var adapter: DayOfWeekAdapter
    var onWeekDaysClickListener: WeekDaysClickListener? = null

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetWeekDaysBinding.inflate(layoutInflater, this, true)

        adapter = DayOfWeekAdapter(
            clickListener = DayItemClickListener { _: DayOfWeekUiModel, position: Int ->
                items = items.mapIndexed { index, dayOfWeekUiModel ->
                    if (index == position) dayOfWeekUiModel.copy(isSelected = !dayOfWeekUiModel.isSelected)
                    else dayOfWeekUiModel
                }
                adapter.submitList(items)
                onWeekDaysClickListener?.isNoneWeekDaysSelectedListener(items.none { it.isSelected })
                onWeekDaysClickListener?.onWeekDaysSelectedListener(items.filter { it.isSelected })
            }
        )

        val decoration = HorizontalSpaceItemDecoration(
            context,
            R.dimen.spacing_3xs
        )

        binding.recyclerView.addItemDecoration(decoration)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.itemAnimator = null
        adapter.submitList(items)
    }

    fun getSelectedDays() = items.filter { it.isSelected }

    fun setSelectedDays(list: List<Int>) {
        items.forEach {
            it.isSelected = list.contains(it.planIndex)
        }

        adapter.submitList(items)
        adapter.notifyDataSetChanged()
    }
}

interface WeekDaysClickListener {
    fun isNoneWeekDaysSelectedListener(isAnyWeekDaysSelected: Boolean)
    fun onWeekDaysSelectedListener(days: List<DayOfWeekUiModel>)
}
