package com.vessel.app.overview

import androidx.lifecycle.MutableLiveData
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OverviewState @Inject constructor() {
    val title = MutableLiveData("")
    val backToHome = LiveEvent<Unit>()
    val showForceUpdate = LiveEvent<Boolean>()

    operator fun invoke(block: OverviewState.() -> Unit) = apply(block)
}
