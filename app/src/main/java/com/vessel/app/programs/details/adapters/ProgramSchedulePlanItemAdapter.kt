package com.vessel.app.programs.details.adapters

import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.SchedulePlanItem

class ProgramSchedulePlanItemAdapter(
    private val handler: OnActionHandler,
    private val isUserEnrolled: Boolean
) :
    BaseAdapterWithDiffUtil<SchedulePlanItem>() {
    override fun getLayout(position: Int) = R.layout.item_schedule_plan
    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<SchedulePlanItem>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.findViewById<AppCompatTextView>(R.id.planTime).isVisible = isUserEnrolled
    }

    interface OnActionHandler {
        fun onSchedulePlanItemClicked(item: SchedulePlanItem)
        fun onSchedulePlanItemTimerClicked(item: SchedulePlanItem)
    }
}
