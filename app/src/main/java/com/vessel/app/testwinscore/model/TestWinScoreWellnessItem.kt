package com.vessel.app.testwinscore.model

data class TestWinScoreWellnessItem(
    val scoreValue: Int,
    val winScoreValue: Int
) {
    val score = scoreValue.toString()
    val winScore = winScoreValue.toString()

    val showWinScore = winScoreValue > 0
}
