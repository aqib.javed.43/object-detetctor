package com.vessel.app.wellness.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R
import com.vessel.app.common.model.DateEntry

data class ReagentDetailItem(
    val id: Int,
    @StringRes val title: Int,
    val subtitle: String,
    val chartEntries: List<DateEntry>,
    val lowerBound: Float,
    val upperBound: Float,
    val minY: Float,
    val maxY: Float
) {

    private val isBelowLowerBound = (chartEntries.lastOrNull()?.y ?: minY) < lowerBound
    private val isAboveUpperBound = (chartEntries.lastOrNull()?.y ?: maxY) > upperBound

    @DrawableRes val statusBackground = when {
        isBelowLowerBound || isAboveUpperBound -> R.drawable.log_cabin_background
        else -> R.drawable.primary_background
    }

    @StringRes val status = when {
        isBelowLowerBound -> R.string.low
        isAboveUpperBound -> R.string.high
        else -> R.string.good
    }

    val lowerBoundBias = 1f - (lowerBound - minY) / (maxY - minY)
    val upperBoundBias = 1f - (upperBound - minY) / (maxY - minY)
}
