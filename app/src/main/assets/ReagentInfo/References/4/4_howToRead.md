Here are the reference ranges we use for Vitamin C:

Low: 0 - 240 mg/L

Good: 240 - 800 mg/L

High: >800 mg/L

Vitamin C is absorbed via a sodium-dependent transport mechanism in the small intestine. Body stores are largely intracellular and saturate in adults at a level of approximately 3 grams. Most studies have found that the maximum amount of vitamin C that can be absorbed orally is about 500mg/day. Vitamin C is stored throughout the body and in very high concentrations in white blood cells that fight infection.

Vitamin C has a half-life of 16-20 days in tissue. Assuming a tissue level of 5000 mg, a lack of vitamin C in the diet for 16 days would reduce the tissue stores to about 2500 mg; in 32 days it would be about 1250 mg; in 44 days it would be 625 mg and in 64 days it would be about 313 mg and clinical signs of scurvy should start to develop.

If people consume more Vitamin C than they need, plasma (blood) levels rise and when they reach 80 - 170 mg/L further intake leads to rapid clearance of vitamin C in the urine. On the other hand, if people are vitamin C deficient, very little vitamin C appears in the urine after a test dose. Studies found that people on a regular diet without illness or severe stress excrete about 200 - 300mg/L each day in their urine. Some experts recommend that with optimal vitamin C intake, urine vitamin C concentration should be 400mg/L or higher.

When you’re in the good zone (darker purple), your body is getting sufficient vitamin c to support your wellness.

Too high means you may be getting more than you need. Although there isn’t much evidence of danger from high levels of vitamin c, it could cause diarrhea.

Too low means your kidneys are holding on to vitamin C, and little is excreted in your urine. In this case, you might want to up your intake. Check out the recommendations in the app for how to do that.

**Sources:**

-   [Screening for Vitamin C in the Urine: Is it Clinically Significant?](http://orthomolecular.org/library/jom/2005/pdf/2005-v20n04-p259.pdf)

-   [Nutrition in Clinical Practice: A Comprehensive, Evidence-Based Manual for the Practitioner (Nutrition in Clinical Practice), 2nd Edition by David Katz.](https://www.academia.edu/41923878/Nutrition_in_Clinical_Practice_A_Comprehensive_Evidence_Based_Manual_for_the_Practitioner_Nutrition_in_Clinical_Practice_2nd_Edition_by)

-   [Pharmacokinetic perspectives on megadoses of ascorbic acid](https://academic.oup.com/ajcn/article/66/5/1165/4655903)

-   [Groff JL, Gropper SS, Hunt SM: The water soluble vitamins. In: Advanced Nutrition and Human Metabolism. Minneapolis: West Publishing Company, 1995: 222-237.](https://www.amazon.com/Advanced-Nutrition-Metabolism-Sareen-Gropper/dp/1133104053)

-   [McEvoy GR: Drug Information: The American Hospital Formulary Service, American Society of Health System Pharmacists, Inc., 1993, MD.](https://www.ahfsdruginformation.com/)

-   [Sauberlich HE: Bioavailability of vitamins. Proc Food Nutr Sci, 1985: 1-33.](https://pubmed.ncbi.nlm.nih.gov/3911266/)

-   [Product Insert, Urine Reagent Strip-11, TecoDiagnostics 1268 N. Lakeview Ave, Anaheim, CA. 92807.](https://www.tecodiagnostics.com/urine-reagent-strips)

-   [Urinary water-soluble vitamins and their metabolite contents as nutritional markers for evaluating vitamin intakes in young Japanese women.](https://pubmed.ncbi.nlm.nih.gov/18635909/)

-   [Urinary excretion of vitamin B1, B2, B6, niacin, pantothenic acid, folate, and vitamin C correlates with dietary intakes of free-living elderly, female Japanese.](https://pubmed.ncbi.nlm.nih.gov/20417877/)

-   [Values of water-soluble vitamins in blood and urine of Japanese young men and women consuming a semi-purified diet based on the Japanese Dietary Reference Intakes](https://pubmed.ncbi.nlm.nih.gov/16392702/)

-   [Intra- and inter-individual variations of blood and urinary water-soluble vitamins in Japanese young adults consuming a semi-purified diet for 7 days.](https://pubmed.ncbi.nlm.nih.gov/20086315/)

-   [Urinary excretion levels of water-soluble vitamins in pregnant and lactating women in Japan.](https://pubmed.ncbi.nlm.nih.gov/23883688/)
