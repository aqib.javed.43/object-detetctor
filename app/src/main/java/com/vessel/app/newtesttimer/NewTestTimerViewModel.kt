package com.vessel.app.newtesttimer

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.activationtimer.util.TimerService
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.newtesttimer.ui.NewTimerTabsManager
import com.vessel.app.util.ResourceRepository
import com.vessel.app.views.retrytabstips.OnTabSelectedListener
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

class NewTestTimerViewModel @ViewModelInject constructor(
    val state: NewTestTimerState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val newTimerTabsManager: NewTimerTabsManager
) : BaseViewModel(resourceProvider),
    OnTabSelectedListener,
    TimerService.TimerHandler {

    var currentTabPosition = FIRST_TAB

    init {
        state.isNotificationSettingsAlertChecked.value = preferencesRepository.enableActivationTimerNotification
    }

    override fun setCurrentTab(position: Int) {
        currentTabPosition = position
    }

    fun onBackPressed() {
        state.stopTimerEvent.call()
        state.showBackWarning.call()
    }

    override fun onUpdateText(minutes: String, seconds: String) {
        state.updateTimerText(minutes, seconds)
    }

    override fun onUpdateProgress(percentComplete: Double) {
        state {
            if (timerStart == null) {
                timerStart = Constants.CAPTURE_DATE_FORMAT.format(Date())
            }
            progress.value = (percentComplete * NewTestTimerState.PROGRESS_MAX).toInt()
        }
    }

    override fun onFinish() {
        state {
            updateTimerText(NewTestTimerState.FINISH_VALUE, NewTestTimerState.FINISH_VALUE)
            progress.value = NewTestTimerState.PROGRESS_MAX
            viewModelScope.launch {
                delay(DELAY_TIME)
                setCompleted()
            }
            navigateToCapture()
        }
    }

    fun onSkipClicked() {
        state.showConfirmSkipAlertEvent.call()
    }

    fun onSkipConfirm() {
        state {
            setCompleted()
            navigateToCapture()
        }
    }

    private fun navigateToCapture() {
        state {
            stopTimerEvent.call()
            navigateToCapture.call()
        }
    }

    fun onContinueClicked() {
        if (state.completed) {
            onSkipConfirm()
        } else {
            state.startForegroundTimer.call()
        }
    }

    fun showNotification(isChecked: Boolean) {
        preferencesRepository.enableActivationTimerNotification = isChecked
        state.showNotificationSettingsAlert.value = isChecked
        if (!isChecked) {
            state.stopForegroundTimer.call()
        }
    }
    companion object {
        const val FIRST_TAB = 0
        const val DELAY_TIME = 500L
        private const val DEFAULT_WAITING_TIME_SECONDS = 120
    }
}
