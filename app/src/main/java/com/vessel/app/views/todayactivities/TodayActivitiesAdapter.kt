package com.vessel.app.views.todayactivities

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.view.View
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.core.animation.doOnEnd
import androidx.core.view.isVisible
import com.airbnb.lottie.LottieAnimationView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.plan.model.PlanItem

class TodayActivitiesAdapter(private val handler: TodayActivitiesWidgetOnActionHandler) :
    BaseAdapterWithDiffUtil<PlanItem>() {

    override fun getLayout(position: Int) = R.layout.item_today_activity
    override fun getHandler(position: Int): Any = handler

    override fun onBindViewHolder(
        holder: BaseViewHolder<PlanItem>,
        @SuppressLint("RecyclerView") position: Int
    ) {
        super.onBindViewHolder(holder, position)

        val item = getItem(position) as PlanItem

        holder.itemView.findViewById<View>(R.id.reminderTime).setOnClickListener {
            handler.onAddReminderClicked(item, it)
        }

        holder.itemView.findViewById<View>(R.id.background).setOnClickListener {
            handler.onPlanItemClicked(item, it)
        }

        val checkbox = holder.itemView.findViewById<AppCompatCheckBox>(R.id.checkbox)
        val checkboxBg = holder.itemView.findViewById<View>(R.id.checkboxBg)
        checkbox.setOnClickListener {
            handlePlanCheckClick(holder, position, it)
        }
        checkboxBg.setOnClickListener {
            handlePlanCheckClick(holder, position, it)
        }
        val itemAddingAnimationView = holder.itemView.findViewById<LottieAnimationView>(R.id.itemAddingAnimationView)
        val itemSuccessAnimationView = holder.itemView.findViewById<LottieAnimationView>(R.id.itemSuccessAnimationView)
        itemSuccessAnimationView.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                this@TodayActivitiesAdapter.handler.onAnimationFinished(item)
            }
            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })
        val reminderV = holder.itemView.findViewById<View>(R.id.reminder)
        val reminderTimeV = holder.itemView.findViewById<View>(R.id.reminderTime)
        val blurForegroundV = holder.itemView.findViewById<View>(R.id.blurForeground)
        if (item.playAnimation) {
            if (item.isCompleted) {
                itemAddingAnimationView.isVisible = true
                itemSuccessAnimationView.isVisible = true
                itemAddingAnimationView.playAnimation()
                itemSuccessAnimationView.playAnimation()
                val reminderFadeOut = ObjectAnimator.ofFloat(reminderV, "alpha", 0f)
                val reminderTimeFadeOut = ObjectAnimator.ofFloat(reminderTimeV, "alpha", 0f)
                val foregroundFadeIn = ObjectAnimator.ofFloat(blurForegroundV, "alpha", 1f)
                AnimatorSet().apply {
                    duration = ANIMATION_DURATION
                    playTogether(
                        reminderFadeOut,
                        reminderTimeFadeOut,
                        foregroundFadeIn
                    )
                    start()
                }
            } else {
                itemAddingAnimationView.isVisible = false
                itemSuccessAnimationView.isVisible = false
                val reminderFadeIn = ObjectAnimator.ofFloat(reminderV, "alpha", 1f)
                val reminderTimeFadeIn = ObjectAnimator.ofFloat(reminderTimeV, "alpha", 1f)
                val foregroundFadeOut = ObjectAnimator.ofFloat(blurForegroundV, "alpha", 0f)
                AnimatorSet().apply {
                    duration = ANIMATION_DURATION
                    playTogether(
                        reminderFadeIn,
                        reminderTimeFadeIn,
                        foregroundFadeOut
                    )
                    doOnEnd {
                        this@TodayActivitiesAdapter.handler.onAnimationFinished(item)
                    }
                    start()
                }
            }
        } else {
            itemAddingAnimationView.isVisible = false
            itemSuccessAnimationView.isVisible = false
            blurForegroundV.alpha = if (item.isCompleted) 1f else 0f
            reminderV.alpha = if (item.isCompleted) 0f else 1f
            reminderTimeV.alpha = if (item.isCompleted) 0f else 1f
        }
    }

    private fun handlePlanCheckClick(holder: BaseViewHolder<PlanItem>, position: Int, view: View) {
        val item = getItem(position)
        this@TodayActivitiesAdapter.handler.onItemSelected(item, position)
    }

    companion object {
        private const val ANIMATION_DURATION: Long = 400
    }
}
