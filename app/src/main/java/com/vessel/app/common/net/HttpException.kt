package com.vessel.app.common.net

class HttpException(val code: Int) : RuntimeException()
