package com.vessel.app.lessondetails

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.LessonManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Answer
import com.vessel.app.common.model.AnswerParams
import com.vessel.app.common.model.CompleteAnswer
import com.vessel.app.common.model.Lesson
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.lessondetails.ui.AnswerAdapter
import com.vessel.app.lessondetails.ui.LessonDetailsFragmentDirections
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.*
import kotlinx.coroutines.launch
import java.util.*

class LessonDetailsViewModel @ViewModelInject constructor(
    val state: LessonDetailsState,
    val resourceProvider: ResourceRepository,
    val trackingManager: TrackingManager,
    private val lessonManager: LessonManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider),
    AnswerAdapter.OnActionHandler {
    var currentSelectAnswer: Answer? = null
    var answerText = ""
    var item: Lesson = savedStateHandle.get<Lesson>("lesson")!!
    var index = 0
    var questionCount = 0
    init {
        state.lesson.value = item
        state.isCompletedLesson.value = false
        if (!item.questions.isNullOrEmpty()) {
            state.currentQuestion.value = item.questions!!.first()
            questionCount = (item.questionCount ?: 0L).toInt()
            state.currentIndex.value = 1
            state.answers.value = item.questions!!.first().answers
        }
    }
    private fun setAnswer(answer: Answer) {
        currentSelectAnswer = answer
        val questionType = state.currentQuestion.value?.type
        // For Survey answer always correct
        // For quiz, correct answer based on response data from server
        when (questionType) {
            Constants.QuestionType.SURVEY.name -> {
                state {
                    answers.value = answers.value.orEmpty().map {
                        it.copy(answer = it.answer?.copy(is_incorrect = it.answer_id != currentSelectAnswer?.answer_id))
                    }
                }
                showOrHideQuestion(false)
                state.rightAnswer.value = currentSelectAnswer?.answer?.primary_text
                currentSelectAnswer?.answer?.is_incorrect = false
                state.navigateTo.value = LessonDetailsFragmentDirections.actionLessonDetailsFragmentToAnswerResultDialogFragment(currentSelectAnswer?.answer?.is_incorrect == false)
            }
            Constants.QuestionType.QUIZ.name -> {
                var rightAnswer = state.currentQuestion.value?.answers?.firstOrNull {
                    it.answer?.is_incorrect == false
                }
                if (rightAnswer == null) { // In case all answer is_incorrect = null, the selected one become correct answer
                    val hasFalseAnswer = state.currentQuestion.value?.answers?.firstOrNull {
                        it.answer?.is_incorrect == true
                    } != null
                    if (hasFalseAnswer) {
                        state {
                            answers.value = answers.value.orEmpty().map {
                                if (it.answer?.is_incorrect == null)it.copy(answer = it.answer?.copy(is_incorrect = false))
                                else it.copy()
                            }
                        }
                        rightAnswer = state.answers.value?.firstOrNull {
                            it.answer?.is_incorrect == false
                        }
                        currentSelectAnswer?.answer?.is_incorrect = (rightAnswer?.answer_id == answer.answer_id).not()
                    } else {
                        state {
                            answers.value = answers.value.orEmpty().map {
                                it.copy(answer = it.answer?.copy(is_incorrect = it.answer_id != currentSelectAnswer?.answer_id))
                            }
                        }
                        rightAnswer = answer
                        currentSelectAnswer?.answer?.is_incorrect = false
                    }
                }
                showOrHideQuestion(false)
                state.rightAnswer.value = rightAnswer?.answer?.primary_text ?: ""
                state.navigateTo.value = LessonDetailsFragmentDirections.actionLessonDetailsFragmentToAnswerResultDialogFragment(currentSelectAnswer?.answer?.is_incorrect == false)
            }
            else -> {
                state.rightAnswer.value = ""
                postAnswer(currentSelectAnswer)
            }
        }
    }

    private fun showOrHideQuestion(isShow: Boolean) {
        state.showOrHideQuestion.value = isShow
    }

    fun onAnswerTextChanged(s: CharSequence) {
        answerText = s.toString()
    }
    private fun postAnswer(answer: Answer?) {
        val answerPost = CompleteAnswer()
        answerPost.lesson_id = item.id?.toInt()
        answerPost.program_id = item.programId
        val answers = AnswerParams()
        val questionType = state.currentQuestion.value?.type
        if (questionType == Constants.QuestionType.QUIZ.name || questionType == Constants.QuestionType.SURVEY.name) {
            answers.answer_id = answer?.answer_id
            answers.answer_text = ""
            answers.question_read = true
        } else if (questionType == Constants.QuestionType.READONLY.name) {
            answers.question_read = true
            answers.answer_text = ""
        } else {
            answers.answer_text = answerText
            answers.question_read = true
        }
        answers.question_id = state.currentQuestion.value?.id?.toInt()
        answers.day = item.day?.toInt() ?: 0
        val questions = listOf(answers)
        answerPost.questions = questions
        viewModelScope.launch {
            showLoading()
            lessonManager.postAnswer(answerPost).onSuccess {
                hideLoading()
                processNextStep()
            }
                .onServiceError {
                    hideLoading()
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }
                .onNetworkIOError {
                    hideLoading()
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    hideLoading()
                    showError(getResString(R.string.unknown_error))
                }
        }
    }

    private fun processNextStep() {
        currentSelectAnswer = null
        state.rightAnswer.value = ""
        if (index + 1 < questionCount) {
            state.showOrHideQuestion.value = true
            index += 1
            state.currentQuestion.value = item.questions!![index]
            state.answers.value = item.questions!![index].answers
            state.currentIndex.value = index + 1
            state.isCompletedLesson.value = index + 1 == questionCount
        } else {
            completedLesson()
        }
    }

    fun onNextButtonClicked(view: View) {

        if (state.currentQuestion.value?.isSkippable == false) {
            val qs = state.currentQuestion.value!!
            when (qs.type) {
                Constants.QuestionType.QUIZ.name -> {
                    if (currentSelectAnswer == null) {
                        state.navigateTo.value =
                            LessonDetailsFragmentDirections.actionLessonDetailsFragmentToAnswerNoticeDialogFragment(
                                getResString(
                                    R.string.answer_notice
                                )
                            )
                        return
                    } else {
                        postAnswer(currentSelectAnswer)
                    }
                }
                Constants.QuestionType.SURVEY.name -> {
                    if (currentSelectAnswer == null) {
                        state.navigateTo.value =
                            LessonDetailsFragmentDirections.actionLessonDetailsFragmentToAnswerNoticeDialogFragment(
                                getResString(
                                    R.string.answer_notice
                                )
                            )
                        return
                    } else {
                        postAnswer(currentSelectAnswer)
                    }
                }
                Constants.QuestionType.INPUT.name -> {
                    if (answerText.isEmpty()) {
                        state.navigateTo.value =
                            LessonDetailsFragmentDirections.actionLessonDetailsFragmentToAnswerNoticeDialogFragment(
                                getResString(
                                    R.string.answer_input_notice
                                )
                            )
                        return
                    } else {
                        postAnswer(currentSelectAnswer)
                    }
                } else -> {
                    postAnswer(null)
                }
            }
        } else {
            postAnswer(null)
        }
    }

    private fun completedLesson() {
        if (item.isUnlockedLesson == false) {
            preferencesRepository.updateCompletedLessons(item.id.toString())
            item.programId?.let { Constants.onLessonCompleted?.invoke(it, item.id) }
        }
        preferencesRepository.removeUnlockedLesson(item.programId.toString())
        trackingLesson(TrackingConstants.LESSON_COMPLETED, item)
        state.backToHome.call()
    }

    override fun onAnswered(item: Any, view: View) {
        state {
            answers.value = answers.value.orEmpty().mapIndexed { _, answer ->
                answer.copy(isSelected = true, isChecked = (item as Answer).answer_id == answer.answer_id)
            }.toMutableList()
        }
        setAnswer(item as Answer)
    }
    fun trackingLesson(event: String, lesson: Lesson) {
        trackingManager.log(
            TrackedEvent.Builder(event)
                .addProperty("goal", getGoalName())
                .addProperty("program name", lesson.programName.toString())
                .addProperty("lesson name", lesson.title.toString())
                .addProperty("time", getTime())
                .setEmail(preferencesRepository.getContact()?.email ?: "")
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun getGoalName(): String {
        return preferencesRepository.getUserMainGoal()?.name.toString()
    }

    private fun getProgramName(): String {
        return item.title.toString()
    }

    private fun getTime(): String {
        val today: Date = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
        return today.formatDayDate().toString()
    }
    fun backToHome() {
        navigateBack()
    }
}
