package com.vessel.app.plan.model

import java.util.Date

data class DayPlanItem(
    val currentStep: Int,
    val stepsCount: Int,
    val title: String,
    var items: List<PlanItem>,
    val date: Date,
    var programTitle: String? = null,
    var isTested: Boolean = false,
    var isPreviousDay: Boolean? = false
) {
    fun isFinishAll(): Boolean {
        var isFinishAll = true
        for (item in items) {
            if (!item.isCompleted)isFinishAll = false
        }
        return isFinishAll
    }
}
