package com.vessel.app.pretest.directpee

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class DirectPeeState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: DirectPeeState.() -> Unit) = apply(block)
}
