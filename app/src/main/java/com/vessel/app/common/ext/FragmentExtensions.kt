package com.vessel.app.common.ext

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.vessel.app.Constants
import com.vessel.app.R

fun Fragment.gotToPlayStore() {
    try {
        startActivity(
            Intent(
                android.content.Intent.ACTION_VIEW,
                android.net.Uri.parse("market://details?id=${Constants.APPLICATION_ID}")
            )
        )
    } catch (anfe: ActivityNotFoundException) {
        startActivity(
            Intent(
                android.content.Intent.ACTION_VIEW,
                android.net.Uri.parse("https://play.google.com/store/apps/details?id=${Constants.APPLICATION_ID}")
            )
        )
    }
}

fun Fragment.showNotificationSettingsSnackbar(enable: Boolean) {
    val snackbar = Snackbar.make(requireView(), "", Snackbar.LENGTH_SHORT)
    val layout = snackbar.view as Snackbar.SnackbarLayout
    layout.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.notification_snackbar_background_color))
    val snackView: View = LayoutInflater.from(requireContext()).inflate(R.layout.notification_snakbar_layout, null, false)
    snackView.findViewById<TextView>(R.id.snakebarText).setText(if (enable) R.string.turn_on_notification else R.string.turn_off_notification)
    layout.setPadding(0, 0, 0, 0)
    layout.addView(snackView, 0)
    snackbar.show()
}

fun Fragment.showMessageSnackbar(message: String) {
    val snackbar = Snackbar.make(requireView(), "", Snackbar.LENGTH_LONG)
    val layout = snackbar.view as Snackbar.SnackbarLayout
    layout.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.notification_snackbar_background_color))
    val snackView: View = LayoutInflater.from(requireContext()).inflate(R.layout.notification_snakbar_layout, null, false)
    snackView.findViewById<TextView>(R.id.snakebarText).setText(message)
    layout.setPadding(0, 0, 0, 0)
    layout.addView(snackView, 0)
    snackbar.show()
}

fun Activity.showMessageSnackbar(message: String) {
    val snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "", Snackbar.LENGTH_LONG)
    val layout = snackbar.view as Snackbar.SnackbarLayout
    layout.setBackgroundColor(ContextCompat.getColor(this, R.color.notification_snackbar_background_color))
    val snackView: View = LayoutInflater.from(this).inflate(R.layout.notification_snakbar_layout, null, false)
    snackView.findViewById<TextView>(R.id.snakebarText).setText(message)
    layout.setPadding(0, 0, 0, 0)
    layout.addView(snackView, 0)
    snackbar.show()
}
