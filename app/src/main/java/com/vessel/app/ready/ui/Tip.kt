package com.vessel.app.ready.ui

import android.os.Parcel
import android.os.Parcelable
import android.text.SpannableString
import androidx.annotation.DrawableRes
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.TypeParceler

@Parcelize
@TypeParceler<SpannableString, SpannableStringParceler>()
data class Tip(
    val tip: SpannableString,
    @DrawableRes val drawableRes: Int
) : Parcelable

object SpannableStringParceler : Parceler<SpannableString> {
    override fun create(parcel: Parcel) = SpannableString(parcel.readString())
    override fun SpannableString.write(parcel: Parcel, flags: Int) {
        parcel.writeString(this.toString())
    }
}
