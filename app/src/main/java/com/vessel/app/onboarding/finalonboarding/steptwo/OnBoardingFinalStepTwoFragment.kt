package com.vessel.app.onboarding.finalonboarding.steptwo

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnBoardingFinalStepTwoFragment : BaseFragment<OnBoardingFinalStepTwoViewModel>() {
    override val viewModel: OnBoardingFinalStepTwoViewModel by viewModels()
    override val layoutResId = R.layout.fragment_on_boarding_final_step_two

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
        }
    }
}
