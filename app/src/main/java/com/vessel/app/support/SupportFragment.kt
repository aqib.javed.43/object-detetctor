package com.vessel.app.support

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity

@AndroidEntryPoint
class SupportFragment : BaseFragment<SupportViewModel>() {
    override val viewModel: SupportViewModel by viewModels()
    override val layoutResId = R.layout.fragment_support

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)
        observe(viewModel.openMessaging) {
            MessagingActivity.builder()
                .withToolbarTitle(getString(R.string.customer_support))
                .withEngines(ChatEngine.engine())
                .show(requireContext(), viewModel.getChatConfiguration())
        }
    }
}
