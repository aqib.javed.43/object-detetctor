package com.vessel.app.reagent.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.reagent.model.BenefitsOfBeingOptimal

class ReagentEnergyAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<BenefitsOfBeingOptimal>() {
    override fun getLayout(position: Int) = R.layout.item_energy

    override fun getHandler(position: Int): Any? = handler

    interface OnActionHandler {
        fun onEnergyButtonClick(energy: BenefitsOfBeingOptimal)
    }
}
