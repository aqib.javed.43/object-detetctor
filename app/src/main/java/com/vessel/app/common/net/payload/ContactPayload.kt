package com.vessel.app.common.net.payload

import android.icu.util.TimeZone
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ContactPayload(
    val email: String? = null,
    val password: String? = null,
    val first_name: String,
    val last_name: String,
    val gender: Char? = null,
    val height: Double? = null,
    val weight: Float? = null,
    val birth_date: String? = null,
    val goals: List<Int>? = null,
    val time_zone: String = TimeZone.getDefault().id,
    val main_goal_id: Int? = null
)
