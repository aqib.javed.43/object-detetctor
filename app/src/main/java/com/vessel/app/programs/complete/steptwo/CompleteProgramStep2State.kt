package com.vessel.app.programs.complete.steptwo

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CompleteProgramStep2State @Inject constructor(val passedProgram: Program, val feedback: String?) {

    val program = MutableLiveData(passedProgram)
    val dismissDialog = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()
    val suggestedPrograms = MutableLiveData<List<Program>>()

    operator fun invoke(block: CompleteProgramStep2State.() -> Unit) = apply(block)
}
