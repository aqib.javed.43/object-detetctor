package com.vessel.app.onboarding.onegoalselection

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnBoardingOneGoalSelectionState @Inject constructor() {
    val items = MutableLiveData<List<GoalSelect>>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: OnBoardingOneGoalSelectionState.() -> Unit) = apply(block)
}
