package com.vessel.app.testwinscore.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.testwinscore.model.TestWinScoreWellnessItem

class TestWinScoreWellnessAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<TestWinScoreWellnessItem>() {
    override fun getLayout(position: Int) = R.layout.item_test_win_score_wellness

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onWellnessScoreToolTipClicked(view: View)
    }
}
