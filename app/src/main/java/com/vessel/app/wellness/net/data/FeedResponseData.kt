package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FeedResponseData(
    val duration: String? = null,
    val next: String? = null,
    val results: List<FeedData> = arrayListOf()
)
@JsonClass(generateAdapter = true)
data class FeedActionResponseData(
    val result: String? = null
)
