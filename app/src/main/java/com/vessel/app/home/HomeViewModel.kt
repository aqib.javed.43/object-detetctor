package com.vessel.app.home

import android.annotation.SuppressLint
import android.os.Build
import android.provider.Settings
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.google.firebase.iid.FirebaseInstanceId
import com.pushwoosh.Pushwoosh
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ReminderManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import io.cobrowse.CobrowseIO
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*

class HomeViewModel @ViewModelInject constructor(
    val state: HomeState,
    val remoteConfiguration: RemoteConfiguration,
    private val resourceRepository: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    private val reminderManager: ReminderManager,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceRepository) {

    init {
        observeRemindersChanges()
        observeUpdateRemindersChanges()
        addBugseeAttributes()
        state.showNavigationItems.value = false
        setupFcmToken()
        setupCoBrowseUserData()
    }

    private fun observeRemindersChanges() {
        viewModelScope.launch {
            reminderManager.getFirstRemindersState().collectLatest {
                if (it && reminderManager.getIsReminderClear().not()) {
                    state.showFirstReminderHint.call()
                }
            }
        }
    }

    fun clearFirstReminderState() = reminderManager.clearFirstReminderState()

    private fun observeUpdateRemindersChanges() {
        viewModelScope.launch {
            reminderManager.getUpdateRemindersState().collectLatest {
                if (it) {
                    state.showUpdateReminderHint.call()
                    reminderManager.clearUpdateReminderState()
                }
            }
        }
    }

    private fun setupFcmToken() {
        if (preferencesRepository.fcmToken.isNullOrEmpty()) {
            FirebaseInstanceId.getInstance().instanceId
                .addOnSuccessListener {
                    val fcmToken = it.token
                    preferencesRepository.fcmToken = fcmToken
                }
        }
    }

    fun registerForPushNotifications() {
        Pushwoosh.getInstance().apply {
            preferencesRepository.userEmail?.let {
                setEmail(it)
            }
            setUserId(preferencesRepository.contactId.toString())
            registerForPushNotifications()
        }
    }

    fun onNewDestinationReached() {
        state.showNavigationItems.value = false
    }

    private fun addBugseeAttributes() {
        if (preferencesRepository.inTestingMode) {
            preferencesRepository.addBugseeAttributes()
        }
    }

    @SuppressLint("HardwareIds")
    private fun setupCoBrowseUserData() {
        val isCoBrowseEnabled = remoteConfiguration.getBoolean("cobrowse_enabled")
        if (isCoBrowseEnabled) {
            val userData = mutableMapOf<String, Any>()
            userData[CobrowseIO.DEVICE_ID_KEY] = Settings.Secure.getString(
                resourceRepository.getContentResolver(),
                Settings.Secure.ANDROID_ID
            )
            userData[CobrowseIO.DEVICE_NAME_KEY] = Build.MODEL
            userData[CobrowseIO.USER_NAME_KEY] =
                "${preferencesRepository.userFirstName} ${preferencesRepository.userLastName}"
            userData[CobrowseIO.USER_EMAIL_KEY] = preferencesRepository.userEmail.orEmpty()
            userData[CobrowseIO.USER_ID_KEY] = preferencesRepository.contactId.toString()
            state.setupCoBrowseData.value = userData
        }
    }

    fun trackingLatestScreen(screenName: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.LAST_SCREEN)
                .addProperty("screen name", screenName)
                .addProperty("time", Calendar.getInstance().time.formatDayDate())
                .setEmail(preferencesRepository.getContact()?.email ?: "")
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun trackingScreenShot(screenName: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SCREENSHOT_TAKEN)
                .addProperty("screen name", screenName)
                .addProperty("time", Calendar.getInstance().time.formatDayDate())
                .setEmail(preferencesRepository.getContact()?.email ?: "")
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
