package com.vessel.app.stressreliefdetails.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.stressreliefdetails.model.StressReliefDetailsHeader

class StressReliefHeaderAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<StressReliefDetailsHeader>() {

    override fun getLayout(position: Int) = R.layout.item_stress_relief_details_header

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onAddToPlanClicked(item: StressReliefDetailsHeader)
        fun onCheckStressReliefPlanClicked()
        fun onItemLikeClicked(item: StressReliefDetailsHeader)
        fun onDisLikeClicked(item: StressReliefDetailsHeader)
    }
}
