package com.vessel.app.foodplan.model

data class TabsHeaderItem(val isDailySelected: Boolean, val showTabs: Boolean = true) {
    companion object {
        const val DAILY_TAB_POSITION = 0
        const val WEEKLY_TAB_POSITION = 1
    }
}
