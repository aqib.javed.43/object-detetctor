package com.vessel.app.onboarding.lifestyle.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.onboarding.lifestyle.model.LifestyleSelectModel

class LifestyleSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<LifestyleSelectModel>({ oldItem, newItem ->
        oldItem.id == newItem.id
    }) {
    override fun getLayout(position: Int) = R.layout.item_lifestyle_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onLifestyleSelectClicked(item: LifestyleSelectModel)
    }
}
