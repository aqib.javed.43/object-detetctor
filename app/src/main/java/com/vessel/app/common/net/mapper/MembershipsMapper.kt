package com.vessel.app.common.net.mapper

import com.vessel.app.common.net.data.*
import com.vessel.app.supplement.model.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MembershipsMapper @Inject constructor() {
    fun map(data: List<MembershipData>) = data.map { map(it) }

    fun map(data: MembershipData) = Membership(
        id = data.id,
        shopifyVariantId = data.shopify_variant_id,
        productTitle = data.product_title,
        quantity = data.quantity,
        properties = data.properties?.map { mapProperties(it) },
        status = data.status
    )

    fun mapAddresses(data: List<MembershipAddressData>) = data.map { map(it) }

    private fun map(data: MembershipAddressData) = MembershipAddress(
        id = data.id,
        customer_id = data.customer_id,
        address1 = data.address1,
        address2 = data.address2,
        city = data.city,
        country = data.country,
        created_at = data.created_at,
        first_name = data.first_name,
        last_name = data.last_name,
        province = data.province,
        zip = data.zip
    )

    fun mapPayment(data: List<MembershipPaymentData>) = data.map { map(it) }

    private fun map(data: MembershipPaymentData) = MembershipPayment(
        card_exp_month = data.card_exp_month,
        card_exp_year = data.card_exp_year,
        card_last4 = data.card_last4,
        paymentType = paymentType.from(data.payment_type)
    )

    private fun mapProperties(data: MembershipPropertiesData) = MembershipProperties(
        name = data.name,
        value = data.value
    )
}
