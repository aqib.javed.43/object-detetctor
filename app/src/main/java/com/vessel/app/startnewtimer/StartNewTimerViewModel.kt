package com.vessel.app.startnewtimer

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.startnewtimer.ui.StartNewTimerFragmentDirections
import com.vessel.app.util.ResourceRepository

class StartNewTimerViewModel @ViewModelInject constructor(
    val state: StartNewTimerState,
    resourceProvider: ResourceRepository,
) : BaseViewModel(resourceProvider) {

    fun onStartNewTimerClicked() {
        state.navigateTo.value = StartNewTimerFragmentDirections.actionStartNewTimerToPreActivationTimerFragment()
    }

    fun onBackPressed() {
        state.showBackWarning.call()
    }
}
