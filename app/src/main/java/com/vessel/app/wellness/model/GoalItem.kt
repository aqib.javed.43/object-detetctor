package com.vessel.app.wellness.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R
import com.vessel.app.common.model.DateEntry
import kotlin.math.roundToInt

data class GoalItem(
    @StringRes val title: Int,
    val chartEntries: List<DateEntry>,
    @DrawableRes val background: Int
) {
    val goalValue = chartEntries.last().y.roundToInt().toString()

    @DrawableRes val trend = chartEntries.let {
        when {
            it.last().y > it[it.size - 2].y -> R.drawable.ic_up_right_arrow
            it.last().y < it[it.size - 2].y -> R.drawable.ic_down_right_arrow
            else -> null
        }
    }
}
