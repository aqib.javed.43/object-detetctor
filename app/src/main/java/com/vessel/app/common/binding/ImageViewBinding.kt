package com.vessel.app.common.binding

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.vessel.app.R
import com.vessel.app.common.util.GlideApp
import com.vessel.app.takesurvey.ImageAlignment

@BindingAdapter("android:src")
fun ImageView.setSrcResource(@DrawableRes resource: Int?) {
    if (resource != null) setImageResource(resource)
}

@BindingAdapter("srcFromUrl")
fun ImageView.srcFromUrl(url: String?) {
    GlideApp.with(context)
        .load(url)
        .error(getErrorDrawable(context))
        .into(this)
}
@BindingAdapter("srcScaleTypeFromImageAlignment")
fun ImageView.srcScaleTypeFromImageAlignment(imageAlignment: ImageAlignment?) {
    scaleType = when (imageAlignment) {
        ImageAlignment.LEFT -> ImageView.ScaleType.FIT_START
        ImageAlignment.RIGHT -> ImageView.ScaleType.FIT_END
        else -> ImageView.ScaleType.FIT_CENTER
    }
}

@BindingAdapter("srcFromUrl", "placeholder")
fun ImageView.srcFromUrl(url: String?, @DrawableRes placeholder: Int) {
    GlideApp.with(context)
        .load(url)
        .placeholder(placeholder)
        .into(this)
}

private var errorDrawable: ColorDrawable? = null

private fun getErrorDrawable(context: Context): ColorDrawable {
    if (errorDrawable == null) {
        errorDrawable = ColorDrawable(ContextCompat.getColor(context, R.color.grayMist))
    }
    return errorDrawable!!
}
