package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R

class ScoreInfoSelectView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    val btnInfo by lazy { findViewById<View>(R.id.btnInfo) }
    var isFirstAppear: Boolean = true
    init {
        inflate(getContext(), R.layout.view_score_info_select, this)
    }

    private fun startFadeAnimation() {
        val anim = AlphaAnimation(0f, 1f)
        anim.duration = HIGHLIGHT_FADE_ANIMATION_DURATION
        btnInfo.startAnimation(anim)
    }

    fun startTransition(x: Float, y: Float) {
        if (isFirstAppear) {
            translationX = x - X_AXIS_SPACING
            isFirstAppear = false
            startFadeAnimation()
        } else {
            animate().translationX(x - X_AXIS_SPACING)
                .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
        }
        updateLabelValue(y)
    }

    fun updateLabelValue(newPoint: Float) {
        btnInfo.apply {
            animate()
                .translationY(newPoint)
                .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
        }
    }

    companion object {
        const val X_AXIS_SPACING = 55f
        const val Y_AXIS_SPACING = 50f
        const val X_LABEL_ANIMATION_DURATION = 500L
        const val HIGHLIGHT_GROUP_ANIMATION_DURATION = 700L
        const val HIGHLIGHT_FADE_ANIMATION_DURATION = 900L
        const val SCALED_HINT_LABEL_TEXT_SIZE = 0.7f
    }
}
