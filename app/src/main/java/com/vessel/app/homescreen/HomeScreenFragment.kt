package com.vessel.app.homescreen

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.planintroduction.PlanIntroductionHintDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeScreenFragment : BaseFragment<HomeScreenViewModel>() {
    override val viewModel: HomeScreenViewModel by viewModels()
    override val layoutResId: Int = R.layout.fragment_home_screen

    private var pagerAdapter: HomeScreenPagerAdapter? = null
    private var homeScreenPager: ViewPager2? = null

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupPager2()
        observeData()
    }

    private fun observeData() {
        viewModel.state.apply {
            observe(tabNavigate) {
                homeScreenPager?.currentItem = it.index
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
//            observe(showPlanRecommendation) {
//                findNavController().navigate(HomeScreenFragmentDirections.actionHomeScreenFragmentToPlanReadyHintFragment())
//            }
            observe(showPlanComingDialog) {
                PlanIntroductionHintDialog.showIntroductionDialog(
                    requireActivity(), testCount = it, isPlanIntroductionHint = false
                ) {
                    viewModel.updatePlanHintState()
                }
            }
            observe(showIntroForCheckIn) {
                findNavController().navigate(HomeScreenFragmentDirections.actionGlobalIntroHomePageFragment())
            }
            observe(showMonthlyCheckIn) {
                findNavController().navigate(HomeScreenFragmentDirections.actionGlobalMonthlyCheckInFragment())
            }
            observe(showWeeklyCheckIn) {
                findNavController().navigate(HomeScreenFragmentDirections.actionGlobalWeeklyCheckInFragment())
            }
            observe(navigateToPopupLock) {
                findNavController().navigate(HomeScreenFragmentDirections.actionHomeScreenFragmentResultTabToLock())
            }
            observe(showEndTrial) {
                findNavController().navigate(HomeScreenFragmentDirections.actionGlobalTrialEndPopUpDialogFragment())
            }
        }
    }

    override fun onDestroyView() {
        homeScreenPager?.adapter = null
        pagerAdapter = null
        super.onDestroyView()
        Log.i("Destroy", "HomeScreenFragment destroy")
    }

    override fun onResume() {
        super.onResume()
        viewModel.handleDeepLinkNavigation()
        viewModel.showAppReview()
        viewModel.onScreenShown()
        lifecycleScope.launch {
            delay(10) // to fix the navigation bug
            viewModel.navigateToTabOnStart()
        }
    }

    private fun setupPager2() {
        homeScreenPager = requireView().findViewById(R.id.homeScreenPager)
        pagerAdapter = HomeScreenPagerAdapter(this)
        homeScreenPager?.isUserInputEnabled = false
        pagerAdapter?.addFragments(viewModel.homeTabsManager.getCurrentTabs())
        homeScreenPager?.adapter = pagerAdapter
        homeScreenPager?.offscreenPageLimit = pagerAdapter?.itemCount!!
    }
}
