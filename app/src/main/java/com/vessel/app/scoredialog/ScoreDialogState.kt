package com.vessel.app.scoredialog

import androidx.lifecycle.MutableLiveData
import com.vessel.app.R
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ScoreDialogState @Inject constructor(wellness: Boolean) {
    val title = MutableLiveData(if (wellness) R.string.wellness_score else R.string.goals)
    val body = MutableLiveData(if (wellness) R.string.score_dialog_wellness_body else R.string.score_dialog_goals_body)
    val dismissDialog = LiveEvent<Unit>()

    operator fun invoke(block: ScoreDialogState.() -> Unit) = apply(block)
}
