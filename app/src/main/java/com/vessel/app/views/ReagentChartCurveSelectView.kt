package com.vessel.app.views

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.text.Layout
import android.text.Spannable
import android.text.style.AlignmentSpan
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import com.vessel.app.R
import com.vessel.app.common.model.data.ReagentItem

class ReagentChartCurveSelectView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var currentHeight: Int = 0
    private var reagentId: Int = -1
    private val rangeLinesLayout by lazy { findViewById<ConstraintLayout>(R.id.rangeLinesLayout) }
    private val selectView by lazy { findViewById<WhiteRoundedCornersBlurView>(R.id.selected_view) }
    private val entryLabelValue by lazy { findViewById<AppCompatTextView>(R.id.entryLabelValue) }
    private val averageLabelValue by lazy { findViewById<AppCompatTextView>(R.id.averageLabelValue) }
    var isFirstAppear: Boolean = true

    init {
        inflate(getContext(), R.layout.view_reagent_chart_curve_select, this)
    }

    private fun startFadeAnimation() {
        val anim = AlphaAnimation(0f, 1f)
        anim.duration = HIGHLIGHT_FADE_ANIMATION_DURATION
        entryLabelValue.startAnimation(anim)
    }

    private fun startDownAnimation(y: Float, xValue: String, hint: String?, showNumber: Boolean) {
        ValueAnimator.ofInt(0, measuredHeight).apply {
            addUpdateListener { valueAnimator ->
                currentHeight = valueAnimator.animatedValue as Int
                selectView.updateLayoutParams {
                    height = currentHeight
                }
                updateLabelAndHeight(y, xValue, hint, showNumber)
            }
            duration = HIGHLIGHT_GROUP_ANIMATION_DURATION
            start()
        }
    }
    private fun moveDownAnimation(y: Float) {
        selectView.updateLayoutParams {
            val needUpdateHeight = y >= Y_AXIS_SPACING * 2
            height = if (needUpdateHeight) (currentHeight - y).toInt()
            else currentHeight
            translationY = if (needUpdateHeight) y else 0f
        }
    }
    fun startTransition(x: Float, y: Float, xValue: String, hint: String? = null, showNumber: Boolean) {
        if (isFirstAppear) {
            translationX = x - X_AXIS_SPACING
            isFirstAppear = false
            startDownAnimation(y, xValue, hint, showNumber)
            startFadeAnimation()
        } else {
            animate().translationX(x - X_AXIS_SPACING)
                .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
            updateLabelAndHeight(y, xValue, hint, showNumber)
        }
    }

    private fun updateLabelAndHeight(y: Float, xValue: String, hint: String?, showNumber: Boolean) {
        moveDownAnimation(y)
        updateLabelValue((currentHeight - height + TOP_MARGIN).toFloat(), xValue, hint, showNumber)
    }

    fun updateLabelValue(newPoint: Float, value: String, hint: String? = null, showNumber: Boolean) {
        entryLabelValue.apply {
            animate()
                .translationY(newPoint + entryLabelValue.height)
                .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
            text = getLabelValueText(value, hint)
            isVisible = showNumber
        }
    }

    fun updateAverageValue(newPoint: Float, showAverageValue: Boolean = true) {
        averageLabelValue.apply {
            isVisible = showAverageValue
            animate()
                .translationX(X_AXIS_SPACING)
                .translationY(newPoint + Y_AXIS_SPACING)
                .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
        }
    }

    private fun getLabelValueText(value: String, hint: String?) = buildSpannedString {
        if (reagentId == ReagentItem.Nitrites.id) append("Nitrites") else append(value)
        if (hint != null) {
            appendLine()
            inSpans(
                RelativeSizeSpan(SCALED_HINT_LABEL_TEXT_SIZE),
                if (reagentId == ReagentItem.Nitrites.id) AlignmentSpan.Standard(Layout.Alignment.ALIGN_NORMAL)else AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
            ) {
                if (reagentId == ReagentItem.Nitrites.id)append(value) else append(hint)
                if (reagentId == ReagentItem.Nitrites.id)setSpan(AlignmentSpan.Standard(Layout.Alignment.ALIGN_NORMAL), 0, "Nitrites".length + 1 + value.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                if (reagentId == ReagentItem.Nitrites.id && value.isNotEmpty())setSpan(ForegroundColorSpan(Color.GRAY), "Nitrites".length, "Nitrites".length + 1 + value.length, 0)
            }
        }
    }

    fun setReagentId(reagentId: Int) {
        this.reagentId = reagentId
        rangeLinesLayout.apply {
            visibility = View.GONE
        }
    }

    companion object {
        const val X_AXIS_SPACING = 55f
        const val Y_AXIS_SPACING = 50f
        const val X_LABEL_ANIMATION_DURATION = 500L
        const val HIGHLIGHT_GROUP_ANIMATION_DURATION = 700L
        const val HIGHLIGHT_FADE_ANIMATION_DURATION = 900L
        const val SCALED_HINT_LABEL_TEXT_SIZE = 0.7f
        const val TOP_MARGIN = 25
    }
}
