package com.vessel.app.teampage

import android.util.Log
import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.Constants
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.TeamsRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.teampage.model.TeamPostItem
import com.vessel.app.util.ResourceRepository
import com.vessel.app.welcometeams.WelcomeTeamViewModel
import com.vessel.app.wellness.net.data.Action
import com.vessel.app.wellness.net.data.FeedReaction
import com.vessel.app.wellness.net.data.ResponseData
import kotlinx.coroutines.launch

class HomeTeamViewModel@ViewModelInject constructor(
    val state: HomeTeamState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    private val teamsRepository: TeamsRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler, TeamPostAdapter.OnActionHandler {

    init {
        preferencesRepository.isFirstTimeClickTeam = false
    }
    fun getTeamData() {
        if (hadMainGoal()) {
            val id = getMainGoal()!!.id
            state.goal.value = Constants.GOALS.firstOrNull {
                it.id == id
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        Log.d(WelcomeTeamViewModel.TAG, "onBackButtonClicked: ")
        view.findNavController().navigateUp()
    }

    fun onComment(item: Any) {
        state.navigateTo.value =
            HomeTeamFragmentDirections.actionHomeTeamFragmentToCommentFragment(item as TeamPostItem)
    }

    override fun onCommentClicked(item: Any, view: View, parentPosition: Int) {
        onComment(item)
    }

    override fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int) {
    }

    override fun onReportClicked(item: Any, view: View, parentPosition: Int) {
        onReport(item)
    }

    override fun onLikeClick(item: Any, view: View, parentPosition: Int) {
        val postItem = item as TeamPostItem
        val id = item.id.toString()
        reactionPost(id, postItem.isLiked)
    }

    override fun onFollowClick(item: Any, view: View, parentPosition: Int) {
        val postItem = item as TeamPostItem
        val id = preferencesRepository.contactId
        val targetId = postItem.actor
        followContact(id, targetId)
    }

    override fun onUnFollowClick(item: Any, view: View, parentPosition: Int) {
        val postItem = item as TeamPostItem
        val id = preferencesRepository.contactId
        val targetId = postItem.actor
        unFollowContact(id, targetId)
    }

    override fun onResponsePoll(item: Any, view: View, response: Int) {
        val postItem = item as TeamPostItem
        reactionPoll(postItem.id.toString(), response)
    }

    fun hadMainGoal(): Boolean {
        return preferencesRepository.getUserMainGoal() != null
    }

    fun hadTested(): Boolean {
        return preferencesRepository.testCompletedCount > 0
    }

    fun getMainGoal(): GoalSample? {
        return preferencesRepository.getUserMainGoal()
    }

    fun onCreatePost() {
        state.navigateTo.value =
            HomeTeamFragmentDirections.actionHomeTeamFragmentToCreatePostFragment()
    }

    fun onReport(item: Any) {
        state.navigateTo.value =
            HomeTeamFragmentDirections.actionHomeTeamFragmentToPostReportFragment(item as TeamPostItem)
    }

    fun fetchAllFeeds() {
        viewModelScope.launch {
            showLoading()
            teamsRepository.fetchNewFeeds(1)
                .onSuccess { feedDataResponse ->
                    hideLoading()
                    val items = ArrayList<TeamPostItem>()
                    for (result in feedDataResponse.results) {
                        val postItem = TeamPostItem().valueOf(result)
                        items.add(postItem)
                    }
                    state.feeds.value = items
                }
                .onError {
                    hideLoading()
                }
        }
    }
    fun getMainGoalId(): Int {
        return preferencesRepository.getUserMainGoal()?.id ?: 0
    }

    // Follow contact
    fun followContact(id: Int, targetId: String) {
        viewModelScope.launch {
            showLoading()
            teamsRepository.followContact(id, targetId)
                .onSuccess { result ->
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }
    // Unfollow contact
    fun unFollowContact(id: Int, targetId: String) {
        viewModelScope.launch {
            showLoading()
            teamsRepository.unFollowContact(id, targetId)
                .onSuccess { result ->
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }
    // Like post
    fun reactionPost(id: String, isLike: Boolean) {
        val feedResponseAction = FeedReaction()
        feedResponseAction.activityId = id
        feedResponseAction.userId = preferencesRepository.contactId.toString()
        feedResponseAction.kind = if (isLike) Action.LIKE.kind else Action.UNLIKE.kind
        viewModelScope.launch {
            showLoading()
            teamsRepository.reactionPost(feedResponseAction)
                .onSuccess { result ->
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }
    // Response a poll
    private fun reactionPoll(id: String, response: Int) {
        val feedResponseAction = FeedReaction()
        feedResponseAction.activityId = id
        feedResponseAction.userId = preferencesRepository.contactId.toString()
        feedResponseAction.kind = Action.POLL_RESPONSE.kind
        val data = ResponseData()
        data.response = response
        feedResponseAction.data = data
        viewModelScope.launch {
            showLoading()
            teamsRepository.reactionPost(feedResponseAction)
                .onSuccess { result ->
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }
}
