package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.data.*
import com.vessel.app.common.net.data.ReagentResponseItemData
import com.vessel.app.common.net.service.ReagentService
import com.vessel.app.util.ResourceRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReagentRepository @Inject constructor(
    private val resourceRepository: ResourceRepository,
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val reagentService: ReagentService
) : BaseNetworkRepository(moshi, loggingManager) {
    private var failureCount: Int = 0
    private var reagentsData: List<Reagent>? = null

    // todo: to be refactored to sealed class with two types of info one for the backendReagent and one for the original/local,
    // Discussion link: https://github.com/vessel-health/vessel-android/pull/522#discussion_r627827219
    suspend fun fetchReagent(): List<Reagent> {
        var backendReagents: List<ReagentResponseItemData?>? = null
        val backendReagentsResponse = getBackendReagents()
        if (backendReagentsResponse is Result.Success) {
            backendReagents = backendReagentsResponse.value
        }
        val localReagentsFileContent = resourceRepository.readFile(REAGENT_BUCKETS_FILE).orEmpty()
        val localReagents =
            ReagentBucketsDataJsonAdapter(moshi).fromJson(localReagentsFileContent)?.reagents
        reagentsData = backendReagents?.mapNotNull { backendReagent ->
            backendReagent?.let {
                val item =
                    localReagents?.firstOrNull { localReagent -> backendReagent.id == localReagent?.reagentId }
                item?.copy(info = backendReagent.info, state = backendReagent.state)
                    ?: ReagentDataItem(
                        reagentId = backendReagent.id,
                        reagentName = backendReagent.name,
                        displayName = backendReagent.name,
                        queryString = null,
                        isBeta = false,
                        isInverted = false,
                        unit = backendReagent.unit,
                        minValue = null,
                        maxValue = null,
                        lowerBound = null,
                        upperBound = null,
                        recommendedDailyAllowance = backendReagent.recommended_daily_allowance,
                        consumptionUnit = backendReagent.consumption_unit,
                        detailsSuggestions = null,
                        buckets = null,
                        info = backendReagent.info,
                        state = backendReagent.state,
                        impact = null
                    )
            }
        }?.map {
            ReagentMapper.map(it, resourceRepository)
        }

        if (reagentsData.isNullOrEmpty()) {
            failureCount += 1
            if (failureCount <= MAX_FAILURE_COUNT)fetchReagent()
        } else {
            failureCount = 0
        }

        return reagentsData.orEmpty()
    }

    private suspend fun getBackendReagents() = executeWithErrorMessage { reagentService.getReagents().reagents }

    fun fromId(id: Int?): Reagent? {
        return reagentsData.orEmpty().firstOrNull { it.id == id }
    }

    fun getReagent() = reagentsData.orEmpty()

    fun fromApiName(apiName: String) =
        reagentsData.orEmpty().firstOrNull { it.apiName.equals(apiName, true) }

    companion object {
        private const val REAGENT_BUCKETS_FILE = "v2_reagent_buckets.json"
        private const val MAX_FAILURE_COUNT = 5
    }
}
