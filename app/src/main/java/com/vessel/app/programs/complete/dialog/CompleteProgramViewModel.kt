package com.vessel.app.programs.complete.dialog

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.MembershipManager
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Program
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.wellness.net.data.ProgramAction
import kotlinx.coroutines.launch
import java.util.*

class CompleteProgramViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val programManager: ProgramManager,
    val membershipManager: MembershipManager,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider) {
    val state = CompleteProgramState(
        savedStateHandle.get<Program>("program")!!
    )
    init {
        fetchProgramSummary()
    }
    private fun fetchProgramSummary() {
        val programId = state.passedProgram.id
        showLoading()
        viewModelScope.launch {
            programManager.fetchProgramSummary(programId)
                .onSuccess {
                    state.programSummaryData.value = it
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    fun onRejoinProgramClicked() {
        enrollProgram()
    }

    fun onCloseClicked() {
        state.dismissDialog.call()
    }
    private fun enrollProgram() {
        state {
            viewModelScope.launch {
                showLoading()
                program.value?.let {
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PROGRAM_ENROLLED)
                            .addProperty(TrackingConstants.KEY_ID, it.id.toString())
                            .addProperty(TrackingConstants.ENROLL_TYPE, ProgramAction.KEEP.value)
                            .addProperty(TrackingConstants.KEY_LOCATION, Constants.CONGRATULATION_SCREEN)
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    programManager.joinProgram(it, ProgramAction.KEEP)
                        .onSuccess {
                            hideLoading()
                        }
                        .onServiceError {
                            showError(it.error.message ?: getResString(R.string.unknown_error))
                        }
                        .onNetworkIOError {
                            showError(getResString(R.string.network_error))
                        }
                        .onUnknownError {
                            showError(getResString(R.string.unknown_error))
                        }
                        .onError {
                            hideLoading()
                        }
                }
            }
        }
    }

    fun onAskCommunityClicked() {
        state.openMessaging.call()
    }
    fun onAskCoachClicked() {
        state.openMessaging.call()
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CONGRATS_TODAYS_PROGRAM_ASK_COACH)
                .addProperty("goal", preferencesRepository.getUserMainGoal()?.name.toString())
                .addProperty("program name", state.program.value?.title.toString())
                .addProperty("time", Calendar.getInstance().time.formatDayDate())
                .setEmail(preferencesRepository.getContact()?.email ?: "")
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
    fun getChatConfiguration() =
        membershipManager.getChatConfiguration(MembershipManager.ZENDESK_NUTRITIONIST_DEPARTMENT)
}
