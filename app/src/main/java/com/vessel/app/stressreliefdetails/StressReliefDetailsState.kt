package com.vessel.app.stressreliefdetails
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentModel
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.stressreliefdetails.model.StressReliefDetailsHeader
import com.vessel.app.stressreliefdetails.model.StressReliefDetailsItemModel
import com.vessel.app.stressreliefdetails.model.StressReliefReviewItem
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class StressReliefDetailsState @Inject constructor(val itemDetails: StressReliefDetailsItemModel) {
    val header = MutableLiveData<StressReliefDetailsHeader>()
    val reminderItems = MutableLiveData<List<ReminderRowUiModel>>()
    val reagentInfo = MutableLiveData<List<ImpactTestReagentModel>>()
    val goalsItems = MutableLiveData<List<ImpactGoalModel>>()
    val imagesItems = MutableLiveData<List<String>>()
    val descriptionData = MutableLiveData<String>()
    val navigateTo = LiveEvent<NavDirections>()
    val removeReminder = LiveEvent<StressReliefDetailsHeader>()
    val reviews = MutableLiveData<List<StressReliefReviewItem>>()
    operator fun invoke(block: StressReliefDetailsState.() -> Unit) = apply(block)
}
