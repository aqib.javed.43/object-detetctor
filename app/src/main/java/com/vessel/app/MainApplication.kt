package com.vessel.app

import android.app.Application
import android.content.IntentFilter
import android.net.ConnectivityManager
import com.appsflyer.AppsFlyerLib
import com.github.pedrovgs.lynx.LynxShakeDetector
import com.pushwoosh.Pushwoosh
import com.pushwoosh.notification.PushwooshNotificationSettings
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.net.ConnectionChangeReceiver
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.BugseeUtils
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import dagger.hilt.android.HiltAndroidApp
import io.cobrowse.CobrowseIO
import javax.inject.Inject

@HiltAndroidApp
class MainApplication : Application(), ConnectionChangeReceiver.ConnectionChangeReceiverListener {
    @Suppress("ProtectedInFinal")
    @Inject // initialize AuthManager so it's dependencies are created and setup on app create
    lateinit var authManager: AuthManager
    @Inject
    lateinit var connectionChangeReceiver: ConnectionChangeReceiver
    @Inject
    lateinit var preferencesRepository: PreferencesRepository
    @Inject
    lateinit var remoteConfiguration: RemoteConfiguration

    override fun onCreate() {
        super.onCreate()
        initLynx()
        setupBugsee()
        setupAppsFlyer()
        setupCobrowse()
        setupConnectionChangeReceiver()
    }

    fun setupBugsee() {
        if (preferencesRepository.isTestingPeriodExpired() == true) {
            preferencesRepository.setTestingMode(false)
        } else {
            if (preferencesRepository.inTestingMode) {
                preferencesRepository.addBugseeAttributes()
                BugseeUtils.launch(this)
            }
        }
    }

    private fun setupAppsFlyer() {
        AppsFlyerLib.getInstance().init(BuildConfig.APPSFLYER_DEV_KEY, null, this)
    }

    private fun setupConnectionChangeReceiver() {
        connectionChangeReceiver.setConnectionChangeReceiverListener(this)
        registerReceiver(connectionChangeReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onConnectionChangeReceiverListener(isConnected: Boolean) {
        if (isConnected) {
            setupPushwoosh()
        }
    }

    private fun setupPushwoosh() {
        Pushwoosh.getInstance().apply {
            appId = BuildConfig.PUSHWOOSH_APP_ID
            senderId = BuildConfig.FCM_SENDER_ID
        }
        PushwooshNotificationSettings.setMultiNotificationMode(true)
    }

    private fun setupCobrowse() {
        CobrowseIO.instance().license(remoteConfiguration.getString("cobrowse_key"))
    }

    private fun initLynx() {
        val lynxShakeDetector = LynxShakeDetector(this)
        lynxShakeDetector.init()
    }

    fun isConnectionEnabled() = connectionChangeReceiver.isConnectionEnabled()
}
