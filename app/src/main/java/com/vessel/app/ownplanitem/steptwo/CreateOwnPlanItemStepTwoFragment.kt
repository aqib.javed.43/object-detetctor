package com.vessel.app.ownplanitem.steptwo

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.PagerIndicator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateOwnPlanItemStepTwoFragment : BaseFragment<CreateOwnPlanItemStepTwoViewModel>() {
    override val viewModel: CreateOwnPlanItemStepTwoViewModel by viewModels()
    override val layoutResId = R.layout.fragment_create_own_plan_item_step_two
    lateinit var imageList: RecyclerView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var pagerIndicator: PagerIndicator
    private val imageAdapter = object : BaseAdapterWithDiffUtil<String>() {
        override fun getLayout(position: Int) = R.layout.item_rounded_image
    }
    private val imagesScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            viewModel.state.setCurrentSelectedImageUrl(linearLayoutManager.findLastCompletelyVisibleItemPosition())
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupImageList()
        setupObservers()
    }

    private fun setupImageList() {
        imageList = requireView().findViewById(R.id.imageList)
        linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        val snapHelper = PagerSnapHelper()
        imageList.apply {
            layoutManager = linearLayoutManager
            adapter = imageAdapter
            addOnScrollListener(imagesScrollListener)
        }
        imageAdapter.submitList(viewModel.state.imagesUrls)
        snapHelper.attachToRecyclerView(imageList)
        pagerIndicator = requireView().findViewById(R.id.pagerIndicator)
        pagerIndicator.attachToRecyclerView(imageList)
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
