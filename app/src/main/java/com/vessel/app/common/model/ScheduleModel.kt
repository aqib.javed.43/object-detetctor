package com.vessel.app.common.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScheduleModel(
    val programId: Int,
    val schedule: List<ScheduleItem>
) : Parcelable
