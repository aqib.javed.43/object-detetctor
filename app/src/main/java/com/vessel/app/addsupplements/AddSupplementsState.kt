package com.vessel.app.addsupplements

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.supplement.model.SupplementFormula
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class AddSupplementsState @Inject constructor() {
    val supplementNutrients = MutableLiveData<List<SupplementFormula>>()
    val showSupplementRecommendations = MutableLiveData(false)
    val navigateTo = LiveEvent<NavDirections>()

    val infoText = listOf(
        R.string.add_supplements_to_your_plan_info_text_1,
        R.string.add_supplements_to_your_plan_info_text_2,
        R.string.add_supplements_to_your_plan_info_text_3,
        R.string.add_supplements_to_your_plan_info_text_4
    )
    operator fun invoke(block: AddSupplementsState.() -> Unit) = apply(block)
}
