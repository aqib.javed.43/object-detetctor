package com.vessel.app.common.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.vessel.app.BuildConfig

@JsonClass(generateAdapter = true)
data class KlavyioEvent(
    val event: String,
    @Json(name = "customer_properties")
    val customerProperties: Map<String, String>,
    val properties: Map<String, String>? = null,
    val token: String = BuildConfig.KLAVIYO_TOKE
)
