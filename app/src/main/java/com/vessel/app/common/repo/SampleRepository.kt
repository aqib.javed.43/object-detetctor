package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.net.payload.AssociateTestPayload
import com.vessel.app.common.net.service.SampleService
import com.vessel.app.common.net.service.UploadSampleService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SampleRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val sampleService: SampleService,
    private val uploadSampleService: UploadSampleService,
) : BaseNetworkRepository(moshi, loggingManager) {

    suspend fun associateTestUuid(uuid: String, wellnessCardUuid: String, replacementCardUuid: String?, autoScan: Boolean) = executeWithErrorMessage {
        uploadSampleService.associateTestUuid(AssociateTestPayload(uuid, wellnessCardUuid, replacementCardUuid, autoScan))
    }

    suspend fun getScoreForId(uuid: String) = executeWithErrorMessage { sampleService.getScoreForId(uuid) }
}
