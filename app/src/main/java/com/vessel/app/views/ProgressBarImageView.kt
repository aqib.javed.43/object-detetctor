package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import com.vessel.app.R
import kotlinx.android.synthetic.main.app_progress_bar_with_end_image.view.*

class ProgressBarImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.app_progress_bar_with_end_image, this)
        context.withStyledAttributes(attrs, R.styleable.AppProgressBarViewGroup) {
            progress.progress = getInteger(R.styleable.AppProgressBarViewGroup_progressValue, 0)
            endDrawable.setImageResource(getResourceId(R.styleable.AppProgressBarViewGroup_src, 0))
        }
    }

    fun setProgress(progressValue: Int) {
        progress.progress = progressValue
    }

    fun setEndDrawable(@DrawableRes src: Int) {
        endDrawable.setImageResource(src)
    }
}
