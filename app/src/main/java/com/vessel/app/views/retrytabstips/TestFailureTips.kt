package com.vessel.app.views.retrytabstips

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.databinding.LayoutTestFailureTipsBinding

class TestFailureTips @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    private lateinit var binding: LayoutTestFailureTipsBinding

    val tabSelectedIcon by lazy { ContextCompat.getDrawable(context, R.drawable.ic_dark_circle_8dp) }
    val tabUnselectedIcon by lazy { ContextCompat.getDrawable(context, R.drawable.ic_dark_circle_alpha70_8dp) }

    private var tipsList: List<TestFailureTipViewUiModel> = getTipsList()

    private val tipsPagerAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_first_test_failure_tips
    }

    private var onTabSelected: OnTabSelectedListener ? = null
    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabSelected(tab: TabLayout.Tab) {
            onTabSelected?.setCurrentTab(tab.position)
            tab.icon = tabSelectedIcon
        }

        override fun onTabUnselected(tab: TabLayout.Tab) {
            tab.icon = tabUnselectedIcon
        }

        override fun onTabReselected(tab: TabLayout.Tab) {
            onTabSelected?. setCurrentTab(tab.position)
        }
    }

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = LayoutTestFailureTipsBinding.inflate(layoutInflater, this, true)
        setupViewPager()
    }
    private fun setupViewPager() {
        binding.tipsViewpager.adapter = tipsPagerAdapter
        binding.tabLayout.addOnTabSelectedListener(onTabSelectedListener)

        TabLayoutMediator(binding.tabLayout, binding.tipsViewpager) { tab, _ ->
            tab.icon = if (tab.isSelected) tabSelectedIcon else tabUnselectedIcon
        }.attach()

        tipsPagerAdapter.submitList(tipsList)

        for (index in tipsList.lastIndex downTo 0) {
            binding.tabLayout.getTabAt(index)?.select()
        }
    }

    fun setOnTabSelectedListener(onTabSelectedListener: OnTabSelectedListener?) {
        this.onTabSelected = onTabSelectedListener
    }

    fun setCurrentItem(item: Int) {
        binding.tipsViewpager.setCurrentItem(item, true)
    }
}

interface OnTabSelectedListener {
    fun setCurrentTab(position: Int)
}
