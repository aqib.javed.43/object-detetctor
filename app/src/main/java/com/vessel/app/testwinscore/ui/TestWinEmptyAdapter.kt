package com.vessel.app.testwinscore.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.testwinscore.model.TestWinScoreEmptyModel

class TestWinEmptyAdapter : BaseAdapterWithDiffUtil<TestWinScoreEmptyModel>() {
    override fun getLayout(position: Int) = R.layout.item_test_win_empty_score
}
