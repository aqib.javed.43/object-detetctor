package com.vessel.app.testerrordialog

import androidx.lifecycle.MutableLiveData
import com.vessel.app.testerrordialog.model.TestErrorHintModel
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class TestErrorHintState @Inject constructor() {
    val dismissDialog = LiveEvent<Unit>()
    val errorHints = MutableLiveData<List<TestErrorHintModel>>()
    operator fun invoke(block: TestErrorHintState.() -> Unit) = apply(block)
}
