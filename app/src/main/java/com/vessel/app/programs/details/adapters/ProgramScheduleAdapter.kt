package com.vessel.app.programs.details.adapters

import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.programs.details.uimodels.ProgramScheduleUiModel
import com.vessel.app.views.stepper.StepperWidget

class ProgramScheduleAdapter(private val handler: OnActionHandler, private val scheduleRowHandler: ProgramSchedulePlanItemAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<ProgramScheduleUiModel>() {

    override fun getLayout(position: Int) = R.layout.item_schedule
    override fun getHandler(position: Int): Any = handler

    override fun onBindViewHolder(holder: BaseViewHolder<ProgramScheduleUiModel>, position: Int) {
        super.onBindViewHolder(holder, position)
        val scheduleRowAdapter = ProgramScheduleRowAdapter(scheduleRowHandler)
        val item = getItem(position)
        holder.itemView.findViewById<RecyclerView>(R.id.scheduleList).let {
            it.adapter = scheduleRowAdapter
            it.itemAnimator = null
        }
        scheduleRowAdapter.submitList(item.getFocusedList())
        holder.itemView.findViewById<StepperWidget>(R.id.scheduleStepper).apply {
            if (item.showStreaks) {
                isVisible = item.showStreaks
                setStepsCount(item.totalItems, item.completedItems)
                setActiveStep(item.completedItems)
                setProgressHint(
                    context.resources.getQuantityString(
                        R.plurals.days_in_a_row,
                        item.maxStreaks,
                        item.maxStreaks
                    )
                )
                setScheduleCompleteHint()
            }
        }
    }

    interface OnActionHandler {
        fun onShowMoreClicked()
        fun onViewYourPlanClicked(item: ProgramScheduleUiModel)
    }
}
