package com.vessel.app.pollpreview

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PollPreviewState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: PollPreviewState.() -> Unit) = apply(block)
}
