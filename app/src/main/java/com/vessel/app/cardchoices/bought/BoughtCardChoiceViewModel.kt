package com.vessel.app.cardchoices.bought

import android.text.SpannedString
import android.view.View
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.util.CustomTypefaceSpan
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.makeClickableSpan
import com.vessel.app.util.validators.EmailValidator
import com.vessel.app.util.validators.PasswordValidator
import com.vessel.app.util.validators.ValidatorException
import kotlinx.coroutines.launch

class BoughtCardChoiceViewModel @ViewModelInject constructor(
    val state: BoughtCardChoiceState,
    resourceRepository: ResourceRepository,
    private val authManager: AuthManager,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository) {
    fun subTitleTextFormat(): SpannedString {
        val prefix = getResString(R.string.bought_card_choice_subtitle_text_prefix)
        val boldPart = getResString(R.string.chat_with_support)
        return buildSpannedString {
            append(prefix)
            append(" ")
            inSpans(CustomTypefaceSpan("", getFont(R.font.bananagrotesk_bold)!!)) {
                append(
                    boldPart.makeClickableSpan(
                        Pair(
                            boldPart,
                            View.OnClickListener {
                                state.navigateTo.value =
                                    BoughtCardChoiceFragmentDirections.actionBoughtCardChoiceFragmentToWeb(
                                        BuildConfig.HELP_CENTER_URL
                                    )
                            }
                        )
                    )
                )
            }
        }
    }

    fun onBackButtonClicked() {
        navigateBack()
    }

    fun onContinueClicked() {
        logContinueEvent()

        if (!validateParams()) {
            return
        }

        showLoading()

        viewModelScope.launch {
            authManager.login(state.userEmail.value.orEmpty(), state.userPassword.value.orEmpty())
                .onSuccess {
                    hideLoading()

                    state.navigateTo.value =
                        BoughtCardChoiceFragmentDirections.actionBoughtCardChoiceFragmentToHome()
                    state.closeEvent.call()
                }
                .onServiceError {
                    val errorMessage = it.error.message ?: getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onNetworkIOError {
                    val errorMessage = getResString(R.string.network_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onUnknownError {
                    val errorMessage = getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun onForgotPasswordClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.FORGOT_PASSWORD_TAPPED)
                .withFirebase()
                .withAmplitude()
                .create()
        )

        state.navigateTo.value =
            BoughtCardChoiceFragmentDirections.actionBoughtCardChoiceFragmentToForgotPasswordFragment()
    }

    private fun logFailureEvent(errorMessage: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SIGN_IN_FAIL)
                .setEmail(state.userEmail.value!!)
                .setError(errorMessage)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun validateParams() = try {
        EmailValidator().validate(state.userEmail)
        PasswordValidator().validate(state.userPassword)
    } catch (exception: ValidatorException) {
        showError(exception)
        false
    }

    private fun logContinueEvent() {
        state.userEmail.value?.let {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.SIGN_UP_LOG_IN)
                    .setEmail(it)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }
}
