package com.vessel.app.wellness.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.wellness.filterpopup.ItemFilterChoice

class SelectedFilterItemAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<ItemFilterChoice>() {

    override fun getLayout(position: Int) = R.layout.item_filter_selected

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onFilterItemRemoved(item: ItemFilterChoice)
    }
}
