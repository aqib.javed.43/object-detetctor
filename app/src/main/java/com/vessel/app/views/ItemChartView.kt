package com.vessel.app.views

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet

class ItemChartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LineChart(context, attrs, defStyle) {

    companion object {
        private const val CHART_LABEL = "Data"
    }

    init {
        xAxis.initAxis()
        axisLeft.initAxis()
        axisRight.initAxis()

        setDrawGridBackground(false)
        setDrawBorders(false)
        setPinchZoom(false)
        setScaleEnabled(false)
        setTouchEnabled(false)
        isDoubleTapToZoomEnabled = false
        description.isEnabled = false
        legend.isEnabled = false

        setViewPortOffsets(0f, 0f, 0f, 0f)

        invalidate()
    }

    private fun AxisBase.initAxis() {
        setDrawGridLines(false)
        setDrawAxisLine(false)
        setDrawLabels(false)
    }

    fun setEntries(entries: List<Entry>) {
        data = LineData(
            LineDataSet(entries, CHART_LABEL).apply {
                color = Color.BLACK
                lineWidth = 1.5f
                mode = LineDataSet.Mode.CUBIC_BEZIER
                setDrawCircles(false)
                setDrawValues(false)
            }
        )
    }

    fun setMinY(min: Float) {
        axisLeft.apply {
            axisMinimum = min
        }
        axisRight.apply {
            axisMinimum = min
        }
    }

    fun setMaxY(max: Float) {
        axisLeft.apply {
            axisMaximum = max
        }
        axisRight.apply {
            axisMaximum = max
        }
    }
}
