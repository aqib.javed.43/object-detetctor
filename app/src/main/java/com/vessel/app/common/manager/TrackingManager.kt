package com.vessel.app.common.manager

import com.amplitude.api.AmplitudeClient
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.TrackingRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackerPlatform
import com.vessel.app.common.tracking.TrackingConstants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import org.json.JSONObject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrackingManager @Inject constructor(
    private val firebaseAnalytics: FirebaseAnalytics,
    private val amplitudeClient: AmplitudeClient,
    private val trackingRepository: TrackingRepository,
    private val preferencesRepository: PreferencesRepository
) {

    fun log(trackedEvent: TrackedEvent) {
        trackedEvent.apply {
            platforms.forEach {
                when (it) {
                    TrackerPlatform.KLAVIYO -> {
                        val userEmail = email ?: preferencesRepository.userEmail ?: "unknown"
                        logToKlaviyo(name, properties, userEmail, preferencesRepository.userFirstName, preferencesRepository.userLastName, preferencesRepository.contactId)
                    }
                    TrackerPlatform.FIREBASE -> logToFirebase(name, properties)
                    TrackerPlatform.AMPLITUDE -> logToAmplitude(name, properties)
                }
            }
        }
    }

    private fun logToKlaviyo(eventName: String, properties: Map<String, String>?, email: String, firstName: String?, lastName: String?, userId: Int) {
        email.let { it ->
            CoroutineScope(SupervisorJob()).launch {
                try {
                    trackingRepository.logKlaviyoEvent(eventName, it, firstName, lastName, userId, properties)
                } catch (e: Exception) {
                    // it is not the end of the world if klaviyo events don't send
                }
            }
        }
    }

    private fun logToFirebase(eventName: String, properties: Map<String, String>?) {
        firebaseAnalytics.logEvent(eventName.replace(" ", "_")) {
            properties?.let {
                for ((k, v) in properties) {
                    param(k, v)
                }
            }
        }
    }

    private fun logToAmplitude(eventName: String, properties: Map<String, String>?) {
        val jsonObject = JSONObject()
        properties?.let {
            for ((k, v) in properties) {
                jsonObject.put(k, v)
            }
        }
        amplitudeClient.logEvent(eventName, jsonObject)
    }

    fun setTrackingUserIdentification() {
        val jsonObjectUser = JSONObject()
        preferencesRepository.userEmail?.let {
            jsonObjectUser.put(TrackingConstants.KEY_EMAIL, it)
            firebaseAnalytics.setUserProperty(TrackingConstants.KEY_EMAIL, it)
        }
        preferencesRepository.userFirstName?.let {
            jsonObjectUser.put(TrackingConstants.KEY_FIRST_NAME, it)
            firebaseAnalytics.setUserProperty(TrackingConstants.KEY_FIRST_NAME, it)
        }
        preferencesRepository.userLastName?.let {
            jsonObjectUser.put(TrackingConstants.KEY_LAST_NAME, it)
            firebaseAnalytics.setUserProperty(TrackingConstants.KEY_LAST_NAME, it)
        }
        amplitudeClient.setUserProperties(jsonObjectUser)

        if (preferencesRepository.contactId > 0) {
            val userId = preferencesRepository.contactId.toString()
            amplitudeClient.userId = userId
            firebaseAnalytics.setUserId(userId)
        }
    }
}
