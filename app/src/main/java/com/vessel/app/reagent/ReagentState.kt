package com.vessel.app.reagent

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.reagent.model.ReachInfo
import com.vessel.app.reagent.model.ReagentDetails
import com.vessel.app.reagent.model.ReagentFoodRecommendation
import com.vessel.app.reagent.model.ReagentHeader
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ReagentState @Inject constructor() {
    val reagentHeaderItems = MutableLiveData<List<ReagentHeader>>()
    val reagentDetails = MutableLiveData<ReagentDetails>()
    val foodRecommendations = MutableLiveData<List<ReagentFoodRecommendation>>()
    val reachInfo = MutableLiveData<List<ReachInfo>>()
    val navigateTo = LiveEvent<NavDirections>()
    val reagentFooterItems = MutableLiveData<List<Any>>()
    val talkToNutritionist = LiveEvent<Unit>()
    operator fun invoke(block: ReagentState.() -> Unit) = apply(block)
}
