package com.vessel.app.overview

import androidx.lifecycle.Observer
import com.vessel.app.*
import com.vessel.app.BaseFaker.Fake.FAKER
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.ForceUpdateManager
import com.vessel.app.common.manager.ScoreManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.overview.ui.OverviewFragmentDirections
import com.vessel.app.util.ResourceRepository
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class OverviewViewModelTest : BaseTest() {
    companion object {
        private val FIRST_NAME = FAKER.name().firstName()
        private val TITLE = FAKER.lorem().sentence()
    }

    private val authManager = mockk<AuthManager>()
    private val scoreManager = mockk<ScoreManager>()
    private val resourceRepository = mockk<ResourceRepository>()
    private val preferencesRepository = mockk<PreferencesRepository>()
    private val trackingManager = mockk<TrackingManager>()
    private val forceUpdateManager = mockk<ForceUpdateManager>()
    private val eventObserver = mockk<Observer<Unit>>()
    private lateinit var state: OverviewState
    private lateinit var viewModel: OverviewViewModel

    @BeforeEach
    fun setUp() {
        every { preferencesRepository.userFirstName } returns FIRST_NAME
        every { resourceRepository.getString(R.string.hi, FIRST_NAME) } returns TITLE
        every { eventObserver.onChanged(null) } just runs
        state = OverviewState()
        viewModel = OverviewViewModel(state, authManager, resourceRepository, scoreManager, preferencesRepository, trackingManager, forceUpdateManager)
    }

    @Test
    fun `init sets the title`() {
        state.title.value isEqualTo TITLE
    }

    @Test
    fun `onTakeYourFirstTestClicked navigates to ready screen`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onTakeYourFirstTestClicked(view)

        verify { navController.navigate(HomeNavGraphDirections.globalActionToTakeTest()) }
    }

    @Test
    fun `onOrderTestCardsClicked navigates to buy test url`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onOrderTestCardsClicked(view)

        verify { navController.navigate(OverviewFragmentDirections.actionOverviewToWeb(BuildConfig.HOME_PAGE_URL)) }
    }

    @Test
    fun `onLogoutClicked logs out the user`() {
        viewModel.onLogoutClicked()

        coVerify { authManager.logout() }
    }

    @Test
    fun `onBackPressed logs out the user`() {
        viewModel.onBackPressed()

        coVerify { authManager.logout() }
    }
}
