package com.vessel.app.common.net.data.plan

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PlanToggleRequest(
    val date: String,
    val completed: Boolean,
    val contact_id: Int?,
    var program_day: Int? = 0
)
@JsonClass(generateAdapter = true)
data class EssentialToggleRequest(
    var date: String? = null,
    var completed: Boolean? = false,
    var contact_id: Int? = null,
    var program_day: Int? = null,
    var essential_id: Int? = null
)
