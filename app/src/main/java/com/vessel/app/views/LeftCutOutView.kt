package com.vessel.app.views

import android.content.Context
import android.graphics.*
import android.os.Build
import android.util.AttributeSet
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.vessel.app.R

class LeftCutOutView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
) : RoundedCornersBlurView(context, attrs) {

    private val path = Path()

    private val circleRadius = resources.getDimension(R.dimen.left_cut_out_radius)

    override fun onDraw(canvas: Canvas?) {
        // Clears any lines and curves from the path but unlike reset(),
        // keeps the internal data structure for faster reuse.
        path.rewind()
        path.addCircle(
            0f,
            height.toFloat() / 2,
            circleRadius,
            Path.Direction.CCW
        )
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            canvas?.clipPath(path, Region.Op.DIFFERENCE)
        } else {
            canvas?.clipOutPath(path)
        }
        super.onDraw(canvas)
    }

    fun setBackroundRoundedColor(@ColorRes color: Int) {
        setOverlayColor(ContextCompat.getColor(context, color))
    }
}
