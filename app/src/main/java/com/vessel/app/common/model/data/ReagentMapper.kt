package com.vessel.app.common.model.data

import com.vessel.app.common.net.data.ReagentResponseInfoData
import com.vessel.app.util.ResourceRepository
import java.util.*

object ReagentMapper {
    fun map(item: ReagentDataItem, resourceProvide: ResourceRepository) = Reagent(
        item.reagentId ?: 0,
        item.reagentName ?: "",
        item.displayName ?: "",
        item.unit ?: "",
        item.buckets.orEmpty().filterNotNull()
            .map {
                findReagentRange(
                    item.detailsSuggestions, it, item.isInverted,
                    item.reagentId == ReagentItem.VitaminC.id || item.reagentId == ReagentItem.B7.id,
                    item.reagentId == ReagentItem.Sodium.id || item.reagentId == ReagentItem.Chloride.id
                )
            },
        item.lowerBound ?: 0f,
        item.upperBound ?: 0f,
        item.minValue ?: 0f,
        item.maxValue ?: 0f,
        getReagentImage(item.reagentId ?: 0, resourceProvide),
        item.isBeta ?: false,
        item.isInverted ?: false,
        item.queryString,
        item.consumptionUnit,
        item.recommendedDailyAllowance,
        item.info?.map { mapReagentInfo(it) },
        ReagentState.from(item.state!!),
        item.impact
    )

    private fun map(
        recommendationSuggestions: RecommendationSuggestions?,
        item: Bucket,
        isInverted: Boolean? = false,
        isVitaminC: Boolean = false
    ): ReagentRange {
        var level: ReagentLevel =
            ReagentLevel.values().firstOrNull { it.level.equals(item.evaluation, true) }
                ?: ReagentLevel.Good
        // Todo remove reagent level high for Vitamin C
        if (level == ReagentLevel.High && isVitaminC) level = ReagentLevel.Good
        val recommendedLevel = when (level) {
            ReagentLevel.Low -> recommendationSuggestions?.low
            ReagentLevel.Good -> recommendationSuggestions?.good
            ReagentLevel.High -> recommendationSuggestions?.high
            ReagentLevel.Great -> recommendationSuggestions?.high
        }

        val recommendedPlan: ReagentButtonAction =
            ReagentButtonAction.values()
                .firstOrNull { it.actionType.equals(recommendedLevel, true) }
                ?: ReagentButtonAction.NONE

        val lowValue = if (isInverted == true) item.high else item.low
        val highValue = if (isInverted == true) item.low else item.high

        return ReagentRange(
            lowValue ?: 0f,
            highValue ?: 0f,
            item.evaluation?.capitalize(Locale.ENGLISH).orEmpty(),
            level.color,
            item.hint?.title.orEmpty(),
            item.hint?.description.orEmpty(),
            level,
            recommendedPlan,
            mapRecommendation(item.recommendations),
            item.reportedValue
        )
    }

    private fun mapRecommendation(recommendation: BucketRecommendation?): RangeRecommendation? {
        if (recommendation == null) return null
        return RangeRecommendation(
            lifestyle = recommendation.lifestyle.orEmpty().map {
                RangeLifestyleRecommendation(
                    it.name,
                    it.quantity,
                )
            },
            supplement = recommendation.supplement?.map(),
            food = recommendation.food?.map()
        )
    }

    private fun BucketRDARecommendation.map() = RangeRDARecommendation(rda_multiplier)

    private fun getReagentImage(reagentId: Int, resourceProvider: ResourceRepository) =
        resourceProvider.getDrawableIdentifier("reagent_header_$reagentId")

    private fun mapReagentInfo(reagentInfo: ReagentResponseInfoData) = ReagentInfo(
        id = reagentInfo.id,
        reagentId = reagentInfo.reagent_id,
        title = reagentInfo.title,
        comingSoonTagItems = reagentInfo.coming_soon_tag_items,
        description = reagentInfo.description,
        infoType = ReagentInfoType.valueOf(reagentInfo.info_type),
        imageUrl = reagentInfo.coming_soon_image
    )
    private fun findReagentRange(
        recommendationSuggestions: RecommendationSuggestions?,
        item: Bucket,
        isInverted: Boolean? = false,
        ignoreHighLevel: Boolean = false,
        ignoreLowLevel: Boolean = false
    ): ReagentRange {
        var level: ReagentLevel =
            ReagentLevel.values().firstOrNull { it.level.equals(item.evaluation, true) }
                ?: ReagentLevel.Good
        if (level == ReagentLevel.High && ignoreHighLevel) level = ReagentLevel.Good
        if (level == ReagentLevel.Low && ignoreLowLevel) level = ReagentLevel.Good
        val recommendedLevel = when (level) {
            ReagentLevel.Low -> recommendationSuggestions?.low
            ReagentLevel.Good -> recommendationSuggestions?.good
            ReagentLevel.High -> recommendationSuggestions?.high
            ReagentLevel.Great -> recommendationSuggestions?.high
        }

        val recommendedPlan: ReagentButtonAction =
            ReagentButtonAction.values()
                .firstOrNull { it.actionType.equals(recommendedLevel, true) }
                ?: ReagentButtonAction.NONE

        val lowValue = if (isInverted == true) item.high else item.low
        val highValue = if (isInverted == true) item.low else item.high

        return ReagentRange(
            lowValue ?: 0f,
            highValue ?: 0f,
            item.evaluation?.capitalize(Locale.ENGLISH).orEmpty(),
            level.color,
            item.hint?.title.orEmpty(),
            item.hint?.description.orEmpty(),
            level,
            recommendedPlan,
            mapRecommendation(item.recommendations),
            item.reportedValue
        )
    }
    private fun findUtiLevel(
        recommendationSuggestions: RecommendationSuggestions?,
        item: Bucket
    ): UtiLevel {
        return UtiLevel.Negative
    }
}
