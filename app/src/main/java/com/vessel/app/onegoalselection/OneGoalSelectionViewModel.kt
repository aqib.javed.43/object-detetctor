package com.vessel.app.onegoalselection

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.model.Goal.Companion.alternateBackground
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.goalsselection.ui.GoalSelectAdapter
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import kotlinx.coroutines.launch
import java.util.*

class OneGoalSelectionViewModel @ViewModelInject constructor(
    val state: OneGoalSelectionState,
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    private val buildPlanManager: BuildPlanManager,
    val homeTabsManager: HomeTabsManager,
    private val preferencesRepository: PreferencesRepository,
    private val contactManager: ContactManager
) : BaseViewModel(resourceProvider),
    GoalSelectAdapter.OnActionHandler,
    ToolbarHandler {

    private val userChosenGoals = goalsRepository.getUserChosenGoals()

    init {
        var position = 0
        state.items.value = goalsRepository.getGoals()
            .filter { goal -> userChosenGoals.any { it.id == goal.key.id } }
            .map { entry ->
                entry.key.let { goal ->
                    GoalSelect(
                        id = goal.id,
                        title = goal.title,
                        recommendation1 = goal.recommendation1,
                        recommendation2 = goal.recommendation2,
                        background = alternateBackground(position++),
                        image = goal.icon,
                        checked = false,
                    )
                }
            }
    }

    override fun onGoalSelectClicked(item: GoalSelect) {
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, goalSelect ->
                    if (goalSelect.id == item.id) goalSelect.copy(checked = item.checked.not())
                    else goalSelect.copy(checked = false)
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    private fun updateMainGoal() {
        val selectedItem = state.items.value?.firstOrNull { it.checked }
        if (selectedItem != null) {
            showLoading()
            viewModelScope.launch {
                contactManager.updateMainGoal(selectedItem.id)
                    .onSuccess {
                        hideLoading(false)
                        val goals = goalsRepository.getSortedGoals()
                        val indexOfMainGoal = goals.indexOfFirst { it.id == selectedItem.id }
                        Collections.swap(
                            goals,
                            0,
                            indexOfMainGoal
                        )
                        goalsRepository.setGoalsOrder(
                            goals.map { it.id }
                        )
//                        if (!goalsRepository.getUserHasEnrolledProgram()) {
//                            state.navigateTo.value =
//                                OneGoalSelectionFragmentDirections.actionOneGoalSelectionFragmentToProgramSelectionFragment(
//                                    selectedItem.id,
//                                    false
//                                )
//                        } else {
//                            onSkipThisClicked()
//                        }
                        onSkipThisClicked()
                    }
                    .onServiceError {
                        showError(it.error.message ?: getResString(R.string.unknown_error))
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }
                hideLoading()
            }
        }
    }

    fun onDoneClicked() {
        updateMainGoal()
    }

    fun onSkipThisClicked() {
        state.navigateTo.value =
            OneGoalSelectionFragmentDirections.actionOneGoalSelectionFragmentToCreatePlanRecommendationFragment(
                RecommendationType.Food
            )
    }
}
