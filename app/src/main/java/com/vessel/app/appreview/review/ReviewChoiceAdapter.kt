package com.vessel.app.appreview.review

import com.vessel.app.R
import com.vessel.app.appreview.firststep.AppReviewFirstStepDialogFragmentDirections
import com.vessel.app.appreview.review.model.ReviewItem
import com.vessel.app.base.BaseAdapterWithDiffUtil
import kotlinx.android.synthetic.main.item_review.view.*

class ReviewChoiceAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<ReviewItem>() {
    override fun getLayout(position: Int) = R.layout.item_review
    override fun getHandler(position: Int): Any = handler
    private val choicesMock = listOf<ReviewItem>(
        ReviewItem(
            "I had trouble scanning",
            AppReviewFirstStepDialogFragmentDirections.actionAppReviewFirstStepDialogFragmentToScanningTipDialogFragment(),
            true
        ),
        ReviewItem(
            "I didn't get all of my results",
            AppReviewFirstStepDialogFragmentDirections.actionAppReviewFirstStepDialogFragmentToResultTipDialogFragment(),
            false
        ),
        ReviewItem(
            "Having trouble with my program",
            AppReviewFirstStepDialogFragmentDirections.actionAppReviewFirstStepDialogFragmentToProgramTipDialogFragment(),
            false
        ),
        ReviewItem(
            "My results or goals aren’t improving",
            AppReviewFirstStepDialogFragmentDirections.actionAppReviewFirstStepDialogFragmentToExpertChatDialogFragment(),
            false
        ),
        ReviewItem(
            "Other",
            AppReviewFirstStepDialogFragmentDirections.actionAppReviewFirstStepDialogFragmentToAppReviewFeedbackDialogFragment(),
            false
        )
    )

    init {
        submitList(choicesMock)
        handler.onItemSelected(
            choicesMock[0]
        )
    }

    interface OnActionHandler {
        fun onItemSelected(item: Any)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ReviewItem>, position: Int) {
        val item = getItem(position)
        holder.itemView.lblTitle.text = item.title
        holder.itemView.swSelected.isClickable = !item.isSelected
        holder.itemView.swSelected.isChecked = item.isSelected
        holder.itemView.rootBg.setBackgroundResource(if (item.isSelected) R.drawable.white_rounded_background else R.drawable.white_alpha70_background)
        holder.itemView.swSelected.setOnCheckedChangeListener { v, isSelected ->
            if (isSelected) {
                uncheckAllReviewItems()
                item.isSelected = true
                handler.onItemSelected(
                    item
                )
                notifyDataSetChanged()
            }
        }
    }

    private fun uncheckAllReviewItems() {
        for (item in choicesMock) {
            item.isSelected = false
        }
    }
}
