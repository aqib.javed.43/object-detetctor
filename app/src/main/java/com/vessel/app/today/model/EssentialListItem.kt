package com.vessel.app.today.model

import com.vessel.app.common.model.Essential

data class EssentialListItem(
    var items: List<Essential>? = arrayListOf(),
    var programTitle: String? = null,
) {
    fun isFinishAll(): Boolean {
        if (items.isNullOrEmpty()) return false
        var isFinishAll = true
        for (item in items!!) {
            if (!item.completed)isFinishAll = false
        }
        return isFinishAll
    }
    fun getTitle(): String {
        return programTitle?.replace("Program", "") + " Essentials"
    }
}
