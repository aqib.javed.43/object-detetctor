package com.vessel.app.views

import android.content.Context
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.AttributeSet
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import androidx.appcompat.widget.AppCompatTextView

class ExpandableWithMoreTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
) : AppCompatTextView(context, attrs, defStyle) {
    var showMore = "More"
    var threeDot = "…"
    var collapsedLineCount = 5
    var isExpanded = false
        private set
    private var mainText: String? = null
    private var isAlreadySet = false
    init {
        viewTreeObserver.addOnGlobalLayoutListener(object :
                OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    showMoreButton()
                    viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })
    }

    fun showMoreButton() {
        if (isAlreadySet || layout == null) return
        val text = text.toString()
        if (!isAlreadySet) {
            mainText = text
            isAlreadySet = true
        }
        var showingText = ""
        var start = 0
        var end: Int
        for (i in 0 until collapsedLineCount) {
            end = layout.getLineEnd(i)
            if (start <= end)showingText += text.substring(start, end)
            start = end
        }
        var specialSpace = 0
        var newText: String
        do {
            newText = showingText.substring(
                0, showingText.length - (specialSpace)
            )
            newText += "$threeDot $showMore"
            val content = SpannableString(newText)
            content.setSpan(UnderlineSpan(), newText.length - showMore.length, newText.length, 0)
            setText(content)
            specialSpace++
        } while (lineCount > collapsedLineCount)
        isExpanded = false
    }
}
