package com.vessel.app.common.net.mapper

import com.vessel.app.common.net.data.*
import com.vessel.app.takesurvey.ImageAlignment
import com.vessel.app.takesurvey.LayoutType
import com.vessel.app.takesurvey.SurveyQuestion
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SurveyMapper @Inject constructor(val answerMapper: SurveyAnswerMapper) {
    fun map(data: SurveyDataResponse) = data.questions.map { map(it) }
    fun map(data: SurveyAnswerResponse) = data.answers.map { answerMapper.map(it, data.questionId ?: 0) }

    private fun map(data: SurveyQuestionWrapperData) = SurveyQuestion(
        id = data.question_id,
        text = data.question.text,
        imageUrl = data.question.image,
        layoutType = LayoutType.valueOf(data.question.layout),
        tipText = data.question.tip_text,
        tipImageUrl = data.question.tip_image,
        answers = data.question.answers.let { answerMapper.map(it, data.question_id) },
        tipImageAlignment = if (data.question.tip_image_alignment != null) ImageAlignment.valueOf(data.question.tip_image_alignment) else ImageAlignment.CENTER,
        imageAlignment = if (data.question.image_alignment != null) ImageAlignment.valueOf(data.question.image_alignment) else ImageAlignment.CENTER,
        successHeading = data.question.success_heading,
        successText = data.question.success_text,
        isSkippable = data.question.is_skippable ?: true
    )
}
