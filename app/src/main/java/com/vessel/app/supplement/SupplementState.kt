package com.vessel.app.supplement

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.checkscience.model.ScienceReference
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.supplement.model.SupplementFormula
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class SupplementState @Inject constructor() {
    val supplementNutrients = MutableLiveData<List<SupplementFormula>>()
    val showSupplementRecommendations = MutableLiveData(false)
    val descriptionPopupCloseAction = LiveEvent<Unit>()
    val vesselFuelOrderConfirmationEvent = LiveEvent<Unit>()
    val descriptionPopupPosition = LiveEvent<Int>()
    val reminderItems = MutableLiveData<List<ReminderRowUiModel>>()
    val descriptionPopupText = MutableLiveData<String>()
    val hasVesselFuel = MutableLiveData<Boolean>(false)
    val navigateTo = LiveEvent<NavDirections>()

    fun getCheckOutReferences(): Array<ScienceReference> {
        return arrayOf(
            ScienceReference(
                R.string.food_supplement_reference_1,
                R.string.food_supplement_text_1,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_2,
                R.string.food_supplement_text_2,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_3,
                R.string.food_supplement_text_3,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_4,
                R.string.food_supplement_text_4,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_5,
                R.string.food_supplement_text_5,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_6,
                R.string.food_supplement_text_6,
                android.R.color.transparent
            )
        )
    }

    operator fun invoke(block: SupplementState.() -> Unit) = apply(block)
}
