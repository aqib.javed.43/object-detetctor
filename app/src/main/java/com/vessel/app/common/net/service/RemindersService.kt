package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.RemindersData
import com.vessel.app.common.net.data.TodoReminderData
import retrofit2.Response
import retrofit2.http.*

interface RemindersService {
    @GET("reminder/")
    suspend fun getReminders(): RemindersData

    @POST("reminder/")
    suspend fun addReminder(@Body todoReminder: TodoReminderData): TodoReminderData

    @DELETE("reminder/{reminderId}")
    suspend fun deleteReminder(@Path("reminderId") reminderId: Int): Response<Unit>

    @PUT("reminder/")
    suspend fun updateReminder(@Body todoReminder: TodoReminderData): TodoReminderData
}
