package com.vessel.app.checkscience.model

import android.text.SpannableString
import androidx.annotation.DrawableRes

data class CheckOutScienceViewItem(
    val content: SpannableString?,
    val showAsReference: Boolean = false,
    @DrawableRes val background: Int
)
