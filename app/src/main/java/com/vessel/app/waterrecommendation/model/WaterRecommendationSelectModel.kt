package com.vessel.app.waterrecommendation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WaterRecommendationSelectModel(
    val unit: Int,
    val count: Int,
    val checked: Boolean = false,
    val title: String
) : Parcelable
