package com.vessel.app.reagent.energysources

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.reagent.energysources.ui.SourceEnergyAdapter
import com.vessel.app.reagent.model.InfoSource
import com.vessel.app.util.ResourceRepository

class ReagentSourceViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider), SourceEnergyAdapter.OnActionHandler {
    val state = SourceState(
        savedStateHandle.get<Array<InfoSource>>("infoSources")!!,
        savedStateHandle["energyName"]!!,
        savedStateHandle["headerTitle"]!!
    )

    fun onGotItClicked() {
        state.dismissDialog.call()
    }

    override fun onSourceLinkButtonClick(source: InfoSource) {
        state.navigateTo.value = HomeNavGraphDirections.globalActionToWeb(source.source)
    }
}
