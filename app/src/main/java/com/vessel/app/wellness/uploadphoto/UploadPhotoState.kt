package com.vessel.app.wellness.uploadphoto

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class UploadPhotoState @Inject constructor() {

    val navigateTo = LiveEvent<NavDirections>()
    val dismiss = LiveEvent<Unit>()

    operator fun invoke(block: UploadPhotoState.() -> Unit) = apply(block)
}
