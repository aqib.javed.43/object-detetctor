package com.vessel.app.helper

import android.content.Context
import android.database.Cursor
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import com.vessel.app.helper.AlbumMediaLoader.Companion.newInstance
import java.lang.ref.WeakReference
import kotlin.jvm.JvmOverloads

class AlbumMediaCollection : LoaderManager.LoaderCallbacks<Cursor> {
    private var mContext: WeakReference<Context>? = null
    private var mLoaderManager: LoaderManager? = null
    private var mCallbacks: AlbumMediaCallbacks? = null
    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        val context = mContext!!.get()
        val album: Album = args!!.getParcelable(ARGS_ALBUM)!!
        return newInstance(
            context!!, album,
            album.isAll && args.getBoolean(ARGS_ENABLE_CAPTURE, false)
        )
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        val context = mContext!!.get() ?: return
        mCallbacks!!.onAlbumMediaLoad(data)
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        val context = mContext!!.get() ?: return
        mCallbacks!!.onAlbumMediaReset()
    }

    fun onCreate(context: FragmentActivity, callbacks: AlbumMediaCallbacks) {
        mContext = WeakReference(context)
        mLoaderManager = context.supportLoaderManager
        mCallbacks = callbacks
    }

    fun onDestroy() {
        if (mLoaderManager != null) {
            mLoaderManager!!.destroyLoader(LOADER_ID)
        }
        mCallbacks = null
    }

    @JvmOverloads
    fun load(target: Album?, enableCapture: Boolean = false) {
        val args = Bundle()
        args.putParcelable(ARGS_ALBUM, target)
        args.putBoolean(ARGS_ENABLE_CAPTURE, enableCapture)
        mLoaderManager!!.initLoader(LOADER_ID, args, this)
    }

    interface AlbumMediaCallbacks {
        fun onAlbumMediaLoad(cursor: Cursor?)
        fun onAlbumMediaReset()
    }

    companion object {
        private const val LOADER_ID = 2
        private const val ARGS_ALBUM = "args_album"
        private const val ARGS_ENABLE_CAPTURE = "args_enable_capture"
    }
}
