package com.vessel.app.profile

import android.os.Bundle
import androidx.activity.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseActivity
import com.vessel.app.util.extensions.applyFullscreenFlags
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileActivity : BaseActivity<ProfileViewModel>() {
    override val viewModel: ProfileViewModel by viewModels()
    override val layoutResId = R.layout.activity_profile

    override fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        applyFullscreenFlags()
    }
}
