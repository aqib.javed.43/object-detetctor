package com.vessel.app.common.net.mapper

import com.vessel.app.common.manager.ReagentsManager
import com.vessel.app.common.model.Sample
import com.vessel.app.common.net.data.SampleData
import com.vessel.app.wellness.net.data.LikeStatus
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SampleMapper @Inject constructor(private val reagentsManager: ReagentsManager) {
    fun map(data: SampleData) = reagentsManager.fromId(data.reagent_id).let {
        Sample(
            it,
            data.goals,
            data.quantity ?: data.dosage ?: 0f,
            data.activity_name ?: data.name.orEmpty(),
            data.supplement_id,
            data.description,
            data.lifestyle_recommendation_id,
            data.id,
            data.unit ?: data.dosage_unit,
            data.frequency,
            data.image_url,
            data.in_plan,
            data.extra_images,
            LikeStatus.from(data.like_status),
            data.total_likes,
            data.dislikes_count
        )
    }

    fun map(data: List<SampleData>): List<Sample> = data.map { map(it) }
}
