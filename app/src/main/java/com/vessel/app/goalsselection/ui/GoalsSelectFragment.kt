package com.vessel.app.goalsselection.ui

import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.goalsselection.GoalsSelectViewModel
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.BlackWithoutOverlayAppButtonViewGroup
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_goal.*

@AndroidEntryPoint
class GoalsSelectFragment : BaseFragment<GoalsSelectViewModel>() {
    override val viewModel: GoalsSelectViewModel by viewModels()
    override val layoutResId = R.layout.fragment_goals_select

    val goalSelectAdapter by lazy { GoalSelectAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.state.goToOneGoalAfterFinish) {
            requireView().findViewById<TextView>(R.id.title).text = getString(R.string.top_three_goals)
            requireView().findViewById<BlackWithoutOverlayAppButtonViewGroup>(R.id.doneButton).setText(getString(R.string.next))
        }
    }

    private fun setupList() {
        goalList.layoutManager = GridLayoutManager(context, Constants.SPAN_COUNT_GOAL_GRID)
        goalList.adapter = goalSelectAdapter
        goalList.itemAnimator = null
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
        }
    }
}
