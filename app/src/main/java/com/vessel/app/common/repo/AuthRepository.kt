package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.net.TokenAuthenticator.Companion.BEARER_TOKEN
import com.vessel.app.common.net.mapper.AuthMapper
import com.vessel.app.common.net.payload.ForgotPasswordPayload
import com.vessel.app.common.net.payload.LoginPayload
import com.vessel.app.common.net.payload.MultiPassPayload
import com.vessel.app.common.net.service.AuthService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val authService: AuthService,
    private val authMapper: AuthMapper
) : BaseNetworkRepository(moshi, loggingManager) {
    suspend fun login(email: String, password: String) = executeWithErrorMessage(authMapper::map) {
        authService.login(LoginPayload(email, password))
    }

    suspend fun refreshToken(refreshToken: String) = execute(authMapper::map) {
        authService.refreshToken("$BEARER_TOKEN $refreshToken")
    }

    suspend fun forgotPassword(email: String) = execute {
        authService.forgotPassword(ForgotPasswordPayload(email))
    }

    suspend fun multipass(token: String, path: String) = execute {
        val bearerToken = "$BEARER_TOKEN $token"
        authService.multipass(bearerToken, MultiPassPayload(path))
    }
}
