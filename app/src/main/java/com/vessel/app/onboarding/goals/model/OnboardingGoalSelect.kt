package com.vessel.app.onboarding.goals.model

import android.os.Parcelable
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OnboardingGoalSelect(
    val id: Int,
    @StringRes val title: Int,
    var checked: Boolean
) : Parcelable {
    @ColorRes
    @IgnoredOnParcel
    val background = if (checked)
        R.color.whiteAlpha70
    else
        R.color.whiteAlpha40
}
