package com.vessel.app.testscore.ui

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.testscore.TestScoreViewModel
import com.vessel.app.util.extensions.observe
import com.vessel.app.wellness.model.ReagentItem
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TestScoreFragment : BaseFragment<TestScoreViewModel>() {
    override val viewModel: TestScoreViewModel by viewModels()
    override val layoutResId = R.layout.fragment_test_score
    private val resultAdapter by lazy { ResultAdapter(viewModel) }
    private lateinit var reagentList: List<ReagentItem>
    private var animationDuration: Long = 1500
    private lateinit var outOfScore: AppCompatTextView
    private lateinit var score: AppCompatTextView
    private lateinit var resultsTitle: AppCompatTextView
    private lateinit var subTitle: AppCompatTextView
    private lateinit var rangeValue: AppCompatTextView
    private lateinit var dash: AppCompatTextView
    private lateinit var image: AppCompatImageView
    private lateinit var resultsList: RecyclerView
    private lateinit var scoreHeader: MaterialCardView
    private lateinit var rangeContainer: ConstraintLayout
    private lateinit var firstBg: View
    private lateinit var poorRange: AppCompatTextView
    private lateinit var poor: AppCompatTextView
    private lateinit var secondBg: View
    private lateinit var fairRange: AppCompatTextView
    private lateinit var fair: AppCompatTextView
    private lateinit var thirdBg: View
    private lateinit var goodRange: AppCompatTextView
    private lateinit var good: AppCompatTextView
    private lateinit var fourthBg: View
    private lateinit var greatRange: AppCompatTextView
    private lateinit var great: AppCompatTextView

    fun initViews() {
        outOfScore = requireView().findViewById(R.id.outOfScore)
        score = requireView().findViewById(R.id.score)
        resultsTitle = requireView().findViewById(R.id.resultsTitle)
        subTitle = requireView().findViewById(R.id.subTitle)
        rangeValue = requireView().findViewById(R.id.rangeValue)
        dash = requireView().findViewById(R.id.dash)
        image = requireView().findViewById(R.id.image)
        resultsList = requireView().findViewById(R.id.resultsList)
        scoreHeader = requireView().findViewById(R.id.scoreHeader)
        rangeContainer = requireView().findViewById(R.id.rangeContainer)
        firstBg = requireView().findViewById(R.id.firstBg)
        poorRange = requireView().findViewById(R.id.poorRange)
        poor = requireView().findViewById(R.id.poor)
        secondBg = requireView().findViewById(R.id.secondBg)
        fairRange = requireView().findViewById(R.id.fairRange)
        fair = requireView().findViewById(R.id.fair)
        thirdBg = requireView().findViewById(R.id.thirdBg)
        goodRange = requireView().findViewById(R.id.goodRange)
        good = requireView().findViewById(R.id.good)
        fourthBg = requireView().findViewById(R.id.fourthBg)
        greatRange = requireView().findViewById(R.id.greatRange)
        great = requireView().findViewById(R.id.great)
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        initViews()
        setupObservers()
        setupAdapters()
        findNavController().currentBackStackEntry
            ?.savedStateHandle?.apply {
                getLiveData<Boolean>(PREVENT_ANIMATION)
                    .observe(viewLifecycleOwner) {
                        if (it) {
                            animationDuration = 0
                        }
                    }
            }
    }

    private fun setupAdapters() {
        resultsList.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = resultAdapter
            itemAnimator = null
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                animationDuration = 0
                findNavController().navigate(it)
            }
            observe(scoreItems) {
                showFirstTestHeaderWithAnimation(it.first, it.second, it.third)
            }
            observe(startReagentAndHeaderScaleAnimation) {
                animationDuration = 1500
                showResultsGridViewWithAnimation()
            }
            observe(reagentItems) {
                reagentList = it
            }
        }
    }

    private fun showFirstTestHeaderWithAnimation(fromScore: Int, toScore: Int, maxScore: Int) {
        val fromScoreLevel = TestScoreViewModel.ScoreLevel.fromScorePoints(fromScore)
        val toScoreLevel = TestScoreViewModel.ScoreLevel.fromScorePoints(toScore)
        val backgroundColorInitialValue = ContextCompat.getColor(
            requireContext(),
            fromScoreLevel?.color ?: TestScoreViewModel.ScoreLevel.Poor.color
        )
        val endValue = ContextCompat.getColor(
            requireContext(),
            toScoreLevel?.color ?: TestScoreViewModel.ScoreLevel.Poor.color
        )
        val backgroundColorValueAnimator =
            ValueAnimator.ofArgb(backgroundColorInitialValue, endValue).apply {
                addUpdateListener {
                    if (animatedValue != null)
                        scoreHeader.setCardBackgroundColor(animatedValue as Int)
                }
            }

        val scoreValueAnimator = ValueAnimator.ofInt(fromScore, toScore).apply {
            addUpdateListener {
                score.text = animatedValue.toString()
                rangeValue.text =
                    TestScoreViewModel.ScoreLevel.fromScorePoints(animatedValue as Int)?.level
            }
        }

        val scoreScaleXAnimator = scaleXAnimator(score, 0.65f)
        val scoreScaleYAnimator = scaleYAnimator(score, 0.65f)
        val dashScaleXAnimator = scaleXAnimator(dash)
        val dashScaleYAnimator = scaleYAnimator(dash)
        val outOfScoreScaleXAnimator = scaleXAnimator(outOfScore)
        val outOfScoreScaleYAnimator = scaleYAnimator(outOfScore)
        val rangeValueScaleXAnimator = scaleXAnimator(rangeValue)
        val rangeValueScaleYAnimator = scaleYAnimator(rangeValue)

        val scoreTranslateXAnimator =
            translationXAnimatorBy(score, resources.getDimension(R.dimen.first_score_x_translation))
        val scoreTranslateYAnimator =
            translationYAnimatorBy(score, resources.getDimension(R.dimen.first_score_y_translation))
        val dashTranslateXAnimator =
            translationXAnimatorBy(dash, resources.getDimension(R.dimen.first_dash_x_translation))
        val dashTranslateYAnimator =
            translationYAnimatorBy(dash, resources.getDimension(R.dimen.first_dash_y_translation))
        val outOfScoreTranslateXAnimator = translationXAnimatorBy(
            outOfScore,
            resources.getDimension(R.dimen.first_outOfScore_x_translation)
        )
        val outOfScoreTranslateYAnimator = translationYAnimatorBy(
            outOfScore,
            resources.getDimension(R.dimen.first_outOfScore_y_translation)
        )
        val rangeValueTranslateXAnimator = translationXAnimatorBy(
            rangeValue,
            resources.getDimension(R.dimen.first_rangeValue_x_translation)
        )
        val rangeValueTranslateYAnimator = translationYAnimatorBy(
            rangeValue,
            resources.getDimension(R.dimen.first_rangeValue_y_translation)
        )

        val subtitle = if (viewModel.isFirstTest()) {
            getString(R.string.score_info_wellness_body)
        } else {
            when {
                toScore == fromScore -> {
                    buildString {
                        appendLine(getString(R.string.well_done))
                        append(getString(R.string.keep_going))
                    }
                }
                toScore < fromScore -> {
                    buildString {
                        append(getString(R.string.you_got_this))
                        append(" ")
                        append(getString(R.string.make_a_plan_stick))
                    }
                }
                toScore > maxScore -> {
                    buildString {
                        appendLine(getString(R.string.a_new_record))
                        append(getString(R.string.new_record, toScore))
                    }
                }
                else -> {
                    getString(R.string.you_have_improved, (toScore - fromScore))
                }
            }
        }

        val subTitleValueAnimator = ValueAnimator.ofInt(0, subtitle.length).apply {
            duration = if (animationDuration != 0L) subtitle.length.toLong() * 100 else 0

            addUpdateListener {
                subTitle.text = subtitle.subSequence(0, animatedValue as Int)
            }
            doOnEnd {
                if (viewModel.isFirstTest()) {
                    // this condition added to fix the back behaviour
                    if (viewModel.state.isReagentAnimationDone) {
                        showResultsGridViewWithAnimation()
                    } else {
                        showRangeCardsWithAnimation()
                    }
                    animationDuration = 0
                } else {
                    rangeContainer.isVisible = false
                    resultsTitle.isVisible = true
                    resultsList.isVisible = true
                    resultAdapter.playStartingAnimation = (animationDuration != 0L)
                    resultAdapter.fadingAndColorScalingStartingAnimation = true
                    resultAdapter.submitList(reagentList)
                    animationDuration = 0
                }
                viewModel.state.isFirstAnimationDone = true
            }
        }

        AnimatorSet().apply {
            duration = animationDuration
            play(backgroundColorValueAnimator)
                .with(scoreValueAnimator)
            doOnEnd {
                AnimatorSet().apply {
                    duration = animationDuration
                    play(scoreScaleXAnimator)
                        .with(scoreScaleYAnimator)
                        .with(scoreTranslateXAnimator)
                        .with(scoreTranslateYAnimator)
                        .with(dashScaleXAnimator)
                        .with(dashScaleYAnimator)
                        .with(dashTranslateXAnimator)
                        .with(dashTranslateYAnimator)
                        .with(outOfScoreScaleXAnimator)
                        .with(outOfScoreScaleYAnimator)
                        .with(outOfScoreTranslateXAnimator)
                        .with(outOfScoreTranslateYAnimator)
                        .with(rangeValueScaleXAnimator)
                        .with(rangeValueScaleYAnimator)
                        .with(rangeValueTranslateXAnimator)
                        .with(rangeValueTranslateYAnimator)
                    doOnStart {
                        subTitleValueAnimator.start()
                    }
                    start()
                }
            }
            start()
        }
    }

    private fun showResultsGridViewWithAnimation() {
        val subTitleFadeout = fadeOutAnimator(subTitle)
        val yTranslation = resources.getDimension(R.dimen.second_score_y_translation)
        val scoreTranslateYAnimation = translationYAnimatorBy(score, yTranslation)
        val dashTranslateYAnimation = translationYAnimatorBy(dash, yTranslation)
        val outOfScoreTranslateYAnimation = translationYAnimatorBy(outOfScore, yTranslation)
        val rangeValueTranslateYAnimation = translationYAnimatorBy(
            rangeValue,
            resources.getDimension(R.dimen.second_rangeValue_y_translation)
        )

        val scoreHeaderScaleYAnimation = scaleYAnimator(scoreHeader)
        val scoreHeaderTranslateYAnimation = translationYAnimatorBy(
            scoreHeader,
            resources.getDimension(R.dimen.score_header_y_translation)
        )

        val imageScaleXAnimation = scaleXAnimator(image)
        val imageScaleYAnimation = scaleYAnimator(image)
        val imageTranslationXAnimation =
            translationXAnimatorBy(image, resources.getDimension(R.dimen.image_x_translation))
        val imageTranslationYAnimation =
            translationYAnimatorBy(image, resources.getDimension(R.dimen.image_y_translation))
        val resultsTitleTranslateYAnimation = translationYAnimatorBy(
            resultsTitle,
            resources.getDimension(R.dimen.results_container_y_translation)
        )
        val resultsListTranslateYAnimation = translationYAnimatorBy(
            resultsList,
            resources.getDimension(R.dimen.results_container_y_translation)
        )

        AnimatorSet().apply {
            duration = animationDuration
            playTogether(
                subTitleFadeout,
                scoreTranslateYAnimation,
                dashTranslateYAnimation,
                outOfScoreTranslateYAnimation,
                rangeValueTranslateYAnimation,
                scoreHeaderTranslateYAnimation,
                scoreHeaderScaleYAnimation,
                imageScaleXAnimation,
                imageScaleYAnimation,
                imageTranslationXAnimation,
                imageTranslationYAnimation,
                resultsTitleTranslateYAnimation,
                resultsListTranslateYAnimation
            )
            doOnStart {
                rangeContainer.isVisible = false
            }
            doOnEnd {
                resultsTitle.isVisible = true
                resultsList.isVisible = true
                resultAdapter.playStartingAnimation = (animationDuration != 0L)
                resultAdapter.fadingStartingAnimation = true
                resultAdapter.submitList(reagentList)
                animationDuration = 0
                viewModel.state.isReagentAnimationDone = true
            }
            start()
        }
    }

    private fun showRangeCardsWithAnimation() {
        val firstBgFadeIn = fadeInAnimator(firstBg)
        val poorRangeFadeIn = fadeInAnimator(poorRange)
        val poorFadeIn = fadeInAnimator(poor)

        val secondBgFadeIn = fadeInAnimator(secondBg)
        val fairRangeFadeIn = fadeInAnimator(fairRange)
        val fairFadeIn = fadeInAnimator(fair)

        val thirdBgFadeIn = fadeInAnimator(thirdBg)
        val goodRangeFadeIn = fadeInAnimator(goodRange)
        val goodFadeIn = fadeInAnimator(good)

        val fourthBgFadeIn = fadeInAnimator(fourthBg)
        val greatRangeFadeIn = fadeInAnimator(greatRange)
        val greatFadeIn = fadeInAnimator(great)
        val fourthCardAnimation = AnimatorSet().apply {
            duration = animationDuration / 2
            playTogether(
                fourthBgFadeIn,
                greatRangeFadeIn,
                greatFadeIn
            )
        }
        val thirdCardAnimation = AnimatorSet().apply {
            duration = animationDuration / 2
            playTogether(
                thirdBgFadeIn,
                goodRangeFadeIn,
                goodFadeIn
            )
        }

        val secondCardAnimation = AnimatorSet().apply {
            duration = animationDuration / 2
            playTogether(
                secondBgFadeIn,
                fairRangeFadeIn,
                fairFadeIn
            )
        }

        val firstCardAnimation = AnimatorSet().apply {
            duration = animationDuration / 2
            playTogether(
                firstBgFadeIn,
                poorRangeFadeIn,
                poorFadeIn
            )
        }
        AnimatorSet().apply {
            playSequentially(
                firstCardAnimation,
                secondCardAnimation,
                thirdCardAnimation,
                fourthCardAnimation
            )
            doOnStart {
                rangeContainer.isVisible = true
            }
            start()
        }
    }

    private fun fadeInAnimator(target: View) =
        ObjectAnimator.ofFloat(target, "alpha", 1f)

    private fun fadeOutAnimator(target: View) =
        ObjectAnimator.ofFloat(target, "alpha", 0f)

    private fun translationYAnimatorBy(target: View, value: Float) =
        ObjectAnimator.ofFloat(target, "translationY", value)

    private fun translationXAnimatorBy(target: View, value: Float) =
        ObjectAnimator.ofFloat(target, "translationX", value)

    private fun scaleXAnimator(target: View, value: Float = 0.5f) =
        ObjectAnimator.ofFloat(target, "scaleX", value)

    private fun scaleYAnimator(target: View, value: Float = 0.5f) =
        ObjectAnimator.ofFloat(target, "scaleY", value)

    companion object {
        const val PREVENT_ANIMATION = "preventAnimation"
    }
}
