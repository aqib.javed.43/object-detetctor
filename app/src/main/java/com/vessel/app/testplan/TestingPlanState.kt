package com.vessel.app.testplan

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.testplan.model.TestingPlanHeader
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class TestingPlanState @Inject constructor() {
    val headerItems = MutableLiveData<TestingPlanHeader>()
    val reminderItems = MutableLiveData<List<ReminderRowUiModel>>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: TestingPlanState.() -> Unit) = apply(block)
}
