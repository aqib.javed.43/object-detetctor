package com.vessel.app.plan.model

import java.util.*

data class DayPlanProgressItem(
    val currentStep: Int,
    val stepsCount: Int,
    val date: Date,
    val title: String
)
