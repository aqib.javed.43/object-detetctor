package com.vessel.app.common.net.service

import com.vessel.app.activityreview.model.ReviewResponse
import com.vessel.app.common.net.data.FoodRecommendationData
import com.vessel.app.common.net.data.ReagentFoodRecommendationData
import com.vessel.app.common.net.data.SampleRecommendationsData
import com.vessel.app.wellness.net.data.*
import retrofit2.http.*

interface RecommendationService {
    @GET("recommendations")
    suspend fun getLatestSampleRecommendations(): SampleRecommendationsData

    @GET("wellness/sample/recommendations/food")
    suspend fun getLatestSampleFoodRecommendations(): List<FoodRecommendationData>

    @POST("recommendations/like")
    suspend fun sendLikeStatus(@Body likePayLoad: LikePayLoad): SurveyResponse

    @HTTP(method = "DELETE", path = "recommendations/like", hasBody = true)
    suspend fun deleteLike(@Body deleteLikePayLoad: DeleteLikePayLoad): SurveyResponse

    @POST("recommendations/review")
    suspend fun postReview(@Body request: ReviewRequest): SurveyResponse

    @GET("recommendations/reviews")
    suspend fun getReviews(@Query("record_id") id: Int?, @Query("category") category: String?): ReviewResponse

    @HTTP(method = "DELETE", path = "recommendations/review", hasBody = true)
    suspend fun deleteReview(@Body request: DeleteReviewRequest): SurveyResponse

    @GET("recommendations/food/reagent")
    suspend fun getFoodRecommendations(@Query("reagent_id") id: Int?): ReagentFoodRecommendationData
}
