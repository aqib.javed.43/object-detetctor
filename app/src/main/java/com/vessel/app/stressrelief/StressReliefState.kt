package com.vessel.app.stressrelief

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.checkscience.model.ScienceReference
import com.vessel.app.reagent.ui.V2ReagentFragmentArgs
import com.vessel.app.stressrelief.model.StressReliefDayItem
import com.vessel.app.stressrelief.model.StressReliefHeaderViewItem
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class StressReliefState @Inject constructor() {
    var isDailyView: Boolean = true
    val navigateTo = LiveEvent<NavDirections>()
    val navigateToReagent = LiveEvent<NavigateTo>()
    val stressReliefHeader = MutableLiveData<List<StressReliefHeaderViewItem>>()
    val stressReliefItems = MutableLiveData<Pair<List<StressReliefDayItem>, Boolean>>()
    var stressReliefDailyItems = mutableListOf<StressReliefDayItem>()
    var stressReliefInWeekly = mutableListOf<StressReliefDayItem>()
    val showStickyPlanView = MutableLiveData(false)

    class NavigateTo(val destinationId: Int, val args: V2ReagentFragmentArgs)

    fun getCheckOutReferences(): Array<ScienceReference> {
        return arrayOf(
            ScienceReference(R.string.stress_relief_reference_1, R.string.stress_relief_reference_title_1),
            ScienceReference(R.string.stress_relief_reference_2, R.string.stress_relief_reference_title_2),
            ScienceReference(R.string.stress_relief_reference_3, R.string.stress_relief_reference_title_3),
            ScienceReference(R.string.stress_relief_reference_4, R.string.stress_relief_reference_title_4),
            ScienceReference(R.string.stress_relief_reference_5, R.string.stress_relief_reference_title_5),
        )
    }

    operator fun invoke(block: StressReliefState.() -> Unit) = apply(block)
}
