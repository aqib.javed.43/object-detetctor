package com.vessel.app.ready.instructionstips

import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.ready.instructionstips.ui.InstrucationTabTipsUIModel
import com.vessel.app.ready.instructionstips.ui.InstrucationTabUIModel
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class InstructionsTipsState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val cupTipsList = listOf(
        InstrucationTabTipsUIModel(R.string.in_cup_pee_text_1, R.drawable.ic_1),
        InstrucationTabTipsUIModel(R.string.in_cup_pee_text_2, R.drawable.ic_2),
    )
    val cardTipsList = listOf(
        InstrucationTabTipsUIModel(R.string.direct_pee_text_1, R.drawable.ic_1),
        InstrucationTabTipsUIModel(R.string.direct_pee_text_2, R.drawable.ic_2),
        InstrucationTabTipsUIModel(R.string.direct_pee_text_3, R.drawable.ic_3),
    )
    val tabs = listOf(
        InstrucationTabUIModel(
            R.string.use_a_cup,
            R.string.in_cup_pee_title,
            R.drawable.in_cup_pee_period,
            R.drawable.cup_pee_tip,
            cupTipsList
        ),
        InstrucationTabUIModel(
            R.string.pee_on_card,
            R.string.direct_pee_title,
            R.drawable.how_to_use_card_period,
            R.drawable.how_to_use_card,
            cardTipsList
        )
    )

    operator fun invoke(block: InstructionsTipsState.() -> Unit) = apply(block)
}
