package com.vessel.app.common.model

import com.vessel.app.BaseFaker.Fake.FAKER

object ContactFaker {
    fun basic() = FAKER.run {
        Contact(
            id = number().randomDigit(),
            email = internet().emailAddress(),
            firstName = name().firstName(),
            lastName = name().lastName(),
            lastLogin = date().birthday(),
            gender = listOf(Contact.Gender.FEMALE, Contact.Gender.MALE, Contact.Gender.OTHER).random(),
            height = Contact.Height(number().randomDigit(), number().randomDigit()),
            weight = Contact.Weight(number().randomNumber().toFloat()),
            birthDate = date().birthday(),
            insertDate = date().birthday(),
            allergies = emptyList(),
            diets = emptyList(),
            goals = emptyList(),
            time_zone = "PST",
            occupation = null,
            imageUrl = null,
            isVerified = null,
            about = null,
            location = null,
            surveyStatus = null,
            testsTaken = null,
            programs = null,
            tips = null,
            mainGoalId = null
        )
    }
}
