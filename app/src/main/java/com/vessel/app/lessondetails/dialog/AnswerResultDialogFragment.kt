package com.vessel.app.lessondetails.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.view.Gravity
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AnswerResultDialogFragment : BaseDialogFragment<AnswerResultDialogViewModel>() {
    override val viewModel: AnswerResultDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_answer_result_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                0
            )
            setLayout(width, height)
            setGravity(Gravity.BOTTOM)
            setBackgroundDrawable(inset)
        }
        isCancelable = false

        observe(viewModel.dismissDialog) { dismiss() }
        observe(viewModel.navigateTo) {
            dismiss()
            findNavController().navigate(it)
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        val timer = object : CountDownTimer(2000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                dismiss()
            }
        }
        timer.start()
    }
}
