package com.vessel.app.today.programinfo

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Program
import kotlinx.android.synthetic.main.item_program_plan_information.view.*

class ProgramInfoAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<Program>() {
    override fun getLayout(position: Int) = R.layout.item_program_plan_information

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onGoalSelectSelected(item: Program)
    }
}
