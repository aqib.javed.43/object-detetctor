package com.vessel.app.vesselfuelcheckout.model.request

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VesselFuelItemRequest(
    val address_id: Int,
    val data: List<VesselFuelSupplementItemModel>
)
