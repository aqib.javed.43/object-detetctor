package com.vessel.app.scoredialog

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class ScoreDialogViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {
    val state = ScoreDialogState(savedStateHandle["wellness"] ?: false)

    fun onGotItClicked() {
        state.dismissDialog.call()
    }
}
