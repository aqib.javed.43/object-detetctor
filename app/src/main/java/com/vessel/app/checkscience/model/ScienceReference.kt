package com.vessel.app.checkscience.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScienceReference(
    @StringRes val link: Int,
    @StringRes val text: Int? = null,
    @DrawableRes val background: Int = R.color.linen
) : Parcelable
