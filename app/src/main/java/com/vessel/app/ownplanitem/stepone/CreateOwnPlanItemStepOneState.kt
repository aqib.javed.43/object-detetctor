package com.vessel.app.ownplanitem.stepone

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CreateOwnPlanItemStepOneState @Inject constructor() {
    var planItemTitle: String = ""
    val planItemTitleRemainingCharacters = MutableLiveData<String>()
    var planItemFrequency: String = ""
    val planItemFrequencyRemainingCharacters = MutableLiveData<String>()
    val navigateTo = LiveEvent<NavDirections>()
    val closeEvent = LiveEvent<Unit>()

    operator fun invoke(block: CreateOwnPlanItemStepOneState.() -> Unit) = apply(block)
}
