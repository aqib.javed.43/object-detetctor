package com.vessel.app.notification

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.vessel.app.common.repo.PreferencesRepository
import javax.inject.Inject

class VesselFirebaseMessagingService : FirebaseMessagingService() {
    @Inject
    protected lateinit var preferencesRepository: PreferencesRepository

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.e("Log message fcm", remoteMessage.toString())
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        if (::preferencesRepository.isInitialized) {
            preferencesRepository.fcmToken = token
        }
    }
}
