package com.vessel.app.wellness.model

data class WellnessHeader(
    val goals: List<GoalButton>,
    val selectedGoal: GoalButton
)
