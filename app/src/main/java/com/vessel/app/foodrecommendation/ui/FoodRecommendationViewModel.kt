package com.vessel.app.foodrecommendation.ui

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.plan.BuildPlanRequest
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.foodrecommendation.FoodRecommendationState
import com.vessel.app.foodrecommendation.model.FoodRecommendationSelectModel
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class FoodRecommendationViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    val state: FoodRecommendationState,
    val buildPlanManager: BuildPlanManager,
    val planManager: PlanManager,
    val reagentsManager: ReagentsManager,
    val foodManager: FoodManager,
    val trackingManager: TrackingManager,
    @Assisted private val savedStateHandle: SavedStateHandle,
    val recommendationsManager: RecommendationManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    FoodRecommendationSelectAdapter.OnActionHandler {
    val reagentId = savedStateHandle.get<Int>("reagentId")

    val foodIds = mutableListOf<Int>()

    init {
        val reagent = reagentsManager.fromId(reagentId)
        state.title.value = "Foods rich in ${reagent?.displayName}"
        loadFoodRecommendations()
    }

    private fun loadFoodRecommendations() {
        showLoading()
        viewModelScope.launch {
            reagentId?.let {
                recommendationsManager.getReagentFoodRecommendations(true, it)
                    .onSuccess {
                        hideLoading(false)
                        state.items.value = it.map { food ->
                            FoodRecommendationSelectModel(
                                food.foodId ?: 0,
                                food.name.orEmpty(),
                                food.imageUrl.orEmpty(),
                                false
                            )
                        }.sortedBy { food ->
                            food.id
                        }
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onHttpOrUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }
                    .onError {
                        hideLoading()
                    }
            }
        }
    }
    override fun onBackButtonClicked(view: View) {
        finishReagentFoodRecommendation()
    }

    private fun finishReagentFoodRecommendation() {
        reagentId?.let { id ->
            if (id != ReagentItem.VitaminC.id && id != ReagentItem.Magnesium.id)buildPlanManager.removeReagentRecommendation(id)
        }
        state.navigateTo.value = FoodRecommendationFragmentDirections.actionFoodRecommendationFragmentToReagentPersonalizingFragment(reagentId == ReagentItem.VitaminC.id || reagentId == ReagentItem.Magnesium.id)
    }

    fun onNextClicked() {
        if (foodIds.size > MAX_FOOD_COUNT) {
            showError(getResString(R.string.select_three_food_error))
            return
        }
        showLoading()
        val buildPlanRequest = BuildPlanRequest(
            food_ids = foodIds,
            reagent_lifestyle_recommendations_ids = listOf(),
            supplements_ids = listOf(),
            null
        )
        viewModelScope.launch {
            planManager.buildPlan(buildPlanRequest)
                .onSuccess {
                    finishReagentFoodRecommendation()
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    override fun onFoodSelectClicked(item: FoodRecommendationSelectModel) {
        if (foodIds.contains(item.id)) {
            foodIds.remove(item.id)
        } else {
            foodIds.add(item.id)
        }
        state {
            items.value =
                items.value.orEmpty().map { food ->
                    if (food.id == item.id) food.copy(checked = !item.checked)
                    else food
                }.toMutableList().sortedBy {
                    it.id
                }
            refreshItems.value = true
        }
    }
    companion object {
        const val MAX_FOOD_COUNT = 3
    }
}
