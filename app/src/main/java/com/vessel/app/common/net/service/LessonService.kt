package com.vessel.app.common.net.service

import com.vessel.app.common.model.CompleteAnswer
import com.vessel.app.common.model.LessonResponse
import retrofit2.http.*

interface LessonService {
    @GET("lesson")
    suspend fun fetchAllLessons(
        @Query("is_today") isToday: Boolean? = true,
        @Query("contact_id") contactId: Int,
        @Query("program_id") programId: Int,
        @Query("fetch_all") fetchAll: Boolean? = true,
        @Query("show_all") showAll: Boolean? = true,
        @Query("date") date: String? = null
    ): LessonResponse

    @POST("lesson/answer")
    suspend fun postAnswer(
        @Body answer: CompleteAnswer
    )
}
