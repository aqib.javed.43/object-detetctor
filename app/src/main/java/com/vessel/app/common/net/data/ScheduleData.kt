package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ScheduleData(
    val program_id: Int,
    val schedule: List<ScheduleItemData>
)
