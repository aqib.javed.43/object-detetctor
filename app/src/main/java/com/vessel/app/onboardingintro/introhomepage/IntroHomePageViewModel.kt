package com.vessel.app.onboardingintro.introhomepage

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository

class IntroHomePageViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider),
    ToolbarHandler {

    val state = IntroHomePageState()

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        state.navigateTo.value = IntroHomePageFragmentDirections.actionIntroHomePageFragmentToIntroResultFragment()
    }
}
