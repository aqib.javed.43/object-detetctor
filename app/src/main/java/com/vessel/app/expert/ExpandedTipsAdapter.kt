package com.vessel.app.expert

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.views.RecommendationListWidgetOnActionHandler
import com.vessel.app.wellness.ui.TipsAdapter

class ExpandedTipsAdapter(
    private val handler: RecommendationListWidgetOnActionHandler,
    private val collapsedTitleHandler: CollapsedTitleHandler
) :
    BaseAdapterWithDiffUtil<GoalWithRecommendations>() {

    override fun getLayout(position: Int) = R.layout.item_goal_with_tips

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(
        holder: BaseViewHolder<GoalWithRecommendations>,
        position: Int
    ) {
        super.onBindViewHolder(holder, position)
        val tips = holder.itemView.findViewById<RecyclerView>(R.id.tips)
        val tipsAdapter = TipsAdapter(handler)
        tips.adapter = tipsAdapter
        tipsAdapter.submitList(getItem(position).tips)
        val collapsedIcon = holder.itemView.findViewById<AppCompatImageView>(R.id.collapsedIcon)
        holder.itemView.findViewById<AppCompatTextView>(R.id.nameTv).text =
            holder.itemView.context.getString(R.string.tips_for, getItem(position).goalName)
        if (getItem(position).isCollapsed) {
            tips.isVisible = false
            collapsedIcon.setImageResource(R.drawable.ic_collapsed_tip)
        } else {
            tips.isVisible = true
            collapsedIcon.setImageResource(R.drawable.ic_expanded_tip)
        }
        holder.itemView.setOnClickListener {
            collapsedTitleHandler.onCollapsedTitleClicked(getItem(position))
        }
    }
}

interface CollapsedTitleHandler {
    fun onCollapsedTitleClicked(goal: GoalWithRecommendations)
}
