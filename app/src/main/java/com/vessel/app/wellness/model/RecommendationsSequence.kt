package com.vessel.app.wellness.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RecommendationsSequence(
    val id: Int,
    val type: RecommendationsSequenceType
) : Parcelable

enum class RecommendationsSequenceType(val value: String) {

    FOOD("food"), TIP("tip"), LIFESTYLE("lifestyle"), NOTHING("nothing");

    companion object {
        fun from(state: String) = values().firstOrNull { it.value == state } ?: NOTHING
    }
}
