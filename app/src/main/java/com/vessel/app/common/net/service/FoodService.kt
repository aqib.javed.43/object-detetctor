package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.SearchedFilteredFoodList
import com.vessel.app.common.net.payload.SearchFilterPayload
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface FoodService {
    @POST("food/filters/contact/filtered")
    suspend fun searchFilterFood(
        @Body payload: SearchFilterPayload,
        @Query("order_by") orderBy: String,
        @Query("ASC") ascending: Boolean
    ): SearchedFilteredFoodList

    @GET("food")
    suspend fun getFoodItems(
        @Query("page") page: Int = 1,
        @Query("per_page") per_page: Int = 1000
    ): SearchedFilteredFoodList
}
