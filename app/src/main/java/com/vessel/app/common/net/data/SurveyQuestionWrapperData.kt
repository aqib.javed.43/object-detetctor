package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SurveyQuestionWrapperData(
    val question_id: Int,
    val survey_id: Int,
    val question_sequence: Int,
    val question: SurveyQuestionData
)
