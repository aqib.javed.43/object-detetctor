package com.vessel.app.ready.instructionstips.ui

import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class InstrucationsTabPagerAdapter : BaseAdapterWithDiffUtil<InstrucationTabUIModel>() {

    override fun getLayout(position: Int) = R.layout.item_instructions_tab

    override fun onBindViewHolder(holder: BaseViewHolder<InstrucationTabUIModel>, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
        val tipAdapter = object : BaseAdapterWithDiffUtil<InstrucationTabTipsUIModel>() {
            override fun getLayout(position: Int) = R.layout.item_instructions_tab_tip
        }
        holder.itemView.findViewById<RecyclerView>(R.id.tips).adapter = tipAdapter
        tipAdapter.submitList(getItem(position).list)
    }
}
