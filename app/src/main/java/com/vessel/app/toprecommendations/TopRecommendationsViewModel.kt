package com.vessel.app.toprecommendations

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AssessmentManager
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.net.data.plan.CreatePlanRequest
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import com.vessel.app.wellness.model.Tip
import com.vessel.app.wellness.net.data.LikeCategory
import kotlinx.coroutines.launch

class TopRecommendationsViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    val resourceProvider: ResourceRepository,
    private val assessmentManager: AssessmentManager,
    private val planManager: PlanManager,
    val homeTabsManager: HomeTabsManager,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceProvider),
    RecommendationsSelectAdapter.OnActionHandler,
    ToolbarHandler {
    val state = TopRecommendationsState(savedStateHandle.get("goalSelected")!!, resourceProvider)

    init {
        loadRecommendations(state.goalSelected)
    }

    override fun onRecommendationSelectClicked(item: Tip) {
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, tip ->
                    if (tip.id == item.id) tip.copy(isChecked = item.isChecked.not())
                    else tip.copy(isChecked = false)
                }
        }
    }

    override fun onRecommendationClicked(item: Tip) {
        state.navigateTo.value =
            TopRecommendationsFragmentDirections.actionTopRecommendationsFragmentToTipDetailsDialogFragment(
                item
            )
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        val selectedItem = state.items.value?.firstOrNull { it.isChecked }
        if (selectedItem != null) {
            addTopPlan(selectedItem)
        }
    }

    private fun addTopPlan(tip: Tip) {
        showLoading()
        viewModelScope.launch {
            val createRequest = CreatePlanRequest(
                notification_enabled = true,
                tip_id = tip.id,
                day_of_week = listOf(0, 1, 2, 3, 4, 5, 6),
                multiple = 1,
            )
            planManager.addPlanItem(createRequest)
                .onSuccess {
                    planManager.clearCachedData()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_ADDED)
                            .addProperty(TrackingConstants.KEY_ID, tip.id.toString())
                            .addProperty(TrackingConstants.TYPE, LikeCategory.Tip.value)
                            .addProperty(
                                TrackingConstants.KEY_LOCATION,
                                "Top recommendation After Test"
                            )
                            .addProperty("has reminders", "true")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    hideLoading()
                    goToNextStep()
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun loadRecommendations(goal: GoalSelect) {
        showLoading()
        val pageSize = 6
        viewModelScope.launch {
            assessmentManager.getRecommendations(
                goal.id,
                tagIds = null,
                level = null,
                pageNumber = 1,
                pageSize = pageSize,
                sortingByLikes = true,
                sortingByNewest = false
            )
                .onSuccess { response ->
                    state.items.value = response.tips
                    hideLoading(false)
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    fun onSkipThisClicked() {
        goToNextStep()
    }

    private fun goToNextStep() {
        state.navigateTo.value =
            TopRecommendationsFragmentDirections.actionTopRecommendationsFragmentToCreatePlanRecommendationFragment(
                RecommendationType.Food
            )
    }
}
