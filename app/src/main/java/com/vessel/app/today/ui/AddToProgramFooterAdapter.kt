package com.vessel.app.today.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.plan.model.AddPlanFooter

class AddToProgramFooterAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        AddPlanFooter -> R.layout.item_add_to_program
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any = handler

    interface OnActionHandler {
        fun onAddPlanFooterClicked(item: Any, view: View)
    }
}
