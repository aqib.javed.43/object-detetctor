package com.vessel.app.checkscience.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.checkscience.model.CheckOutScienceHeaderItem

class CheckOutScienceHeaderAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<CheckOutScienceHeaderItem>() {

    override fun getLayout(position: Int) = R.layout.item_check_out_science_header

    override fun getHandler(position: Int): Any? = handler
}
