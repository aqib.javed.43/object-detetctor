package com.vessel.app.planrecommendation.ready

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PlanRecommendationReadyFragment : BaseFragment<PlanRecommendationReadyViewModel>() {
    override val viewModel: PlanRecommendationReadyViewModel by viewModels()
    override val layoutResId = R.layout.fragment_ready_plan_recommendations

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)
        viewModel.navigateToHome.observe(
            viewLifecycleOwner,
            {
                findNavController().navigate(PostTestNavGraphDirections.actionGoToHome())
            }
        )
    }
}
