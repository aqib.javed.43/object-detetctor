It has been estimated that ~75% of the sodium that we ingest comes from the salt that is added during food processing and/or manufacturing as opposed to the salt that we add during cooking or eating.

-   https://www.fda.gov/consumers/consumer-updates/lowering-salt-your-diet
-   https://www.ncbi.nlm.nih.gov/pubmed/26788299


Before beginning we should clarify that although salt and sodium are often used interchangeably they are not the same thing. Salt refers to a crystal compound composed of sodium chloride. Sodium is a  mineral and is one of the elements that make up salt.

Sodium is one of the body’s essential electrolytes. It helps maintain the balance of water inside and outside of your cells. It is a key player in muscle function and nerve conduction. And, sodium is also involved in maintaining blood pressure. Your body requires a small amount of sodium to maintain proper functioning, but too much sodium may be dangerous to your health. Multiple mechanisms act on the kidneys to ensure that the body maintains proper sodium balance.

High sodium in the blood (hypernatremia) can have adverse health outcomes. Hypernatremia may lead to high blood pressure, cardiovascular problems, osteoporosis, gastric cancer, and kidney stones.

-   https://lpi.oregonstate.edu/mic/minerals/sodium
-   https://www.fda.gov/food/nutrition-education-resources-materials/use-nutrition-facts-label-reduce-your-intake-sodium-your-diet


Most Americans eat too much sodium. Studies have shown that Americans eat ~3,400mg of sodium per day. But the Dietary Guidelines for Americans recommends that we eat < 2,300 mg per (i.e., 1 teaspoon of salt). Lowest sodium intake is associated with the consumption of unprocessed foods, especially fruits, vegetables and legumes.

-   https://www.fda.gov/food/nutrition-education-resources-materials/use-nutrition-facts-label-reduce-your-intake-sodium-your-diet


High sodium levels rank as one of the most dangerous factors in our nation’s physical health and morbidity. A vast majority of individuals do not heed the call to lower intake while there’s time to avoid irreversible cardiac impact. You can be one of the relatively few that takes charge of your long-term health. Showing love to your heart, kidneys, and overall body systems will pay big dividends. No one ever said, at the end, I wish I’d eaten more fast food. You can do this.
