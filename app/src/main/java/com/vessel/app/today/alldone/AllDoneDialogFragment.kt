package com.vessel.app.today.alldone

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AllDoneDialogFragment : BaseDialogFragment<AllDoneViewModel>() {
    override val viewModel: AllDoneViewModel by viewModels()
    override val layoutResId = R.layout.fragment_all_done_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                50
            )
            setLayout(width, height)
            setGravity(Gravity.BOTTOM)
            setBackgroundDrawable(inset)
        }
        isCancelable = false
        observe(viewModel.dismissDialog) { dismiss() }
    }
}
