package com.vessel.app.reagentpersonalizing.model

import android.os.Parcelable
import com.vessel.app.common.model.data.ReagentLevel
import com.vessel.app.common.model.data.UtiLevel
import com.vessel.app.wellness.RecommendationType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReagentRecommendationModel(
    var reagentId: Int,
    var recommendationType: RecommendationType = RecommendationType.Food,
    var level: ReagentLevel? = ReagentLevel.Good,
    val utiLevel: UtiLevel ? = UtiLevel.Normal,
    var lastTitle: String = "",
    var lastRecommendation: String = ""
) : Parcelable
