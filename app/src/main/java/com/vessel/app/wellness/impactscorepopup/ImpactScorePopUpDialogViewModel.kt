package com.vessel.app.wellness.impactscorepopup

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class ImpactScorePopUpDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()

    fun onCloseClicked() {
        dismissDialog.call()
    }
}
