package com.vessel.app.today.selectprogram

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.goalsselection.model.GoalSelect

class ProgramSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<GoalSelect>() {
    override fun getLayout(position: Int) = R.layout.item_program_plan_selection

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onGoalSelectClicked(item: GoalSelect)
        fun onGoalSelectSelected(item: GoalSelect)
    }
}
