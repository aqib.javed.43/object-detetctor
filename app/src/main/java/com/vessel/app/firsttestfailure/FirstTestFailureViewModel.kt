package com.vessel.app.firsttestfailure

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.views.retrytabstips.OnTabSelectedListener

class FirstTestFailureViewModel @ViewModelInject constructor(
    val state: FirstTestFailureState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider), ToolbarHandler, OnTabSelectedListener {
    var currentTabPosition = FIRST_TAB

    init {
        state.buttonName.value = getResString(R.string.next)
    }

    fun onTryToFixItClicked() {
        if (currentTabPosition == LAST_TAB) {
            state.navigateToCapture.call()
        } else {
            state.moveToTab.value = currentTabPosition.inc()
        }
    }

    override fun setCurrentTab(position: Int) {
        currentTabPosition = position
        state.buttonName.value =
            if (position == LAST_TAB)
                getResString(R.string.try_to_fix_it)
            else
                getResString(R.string.next)
    }

    fun onBackPressed() {
        if (currentTabPosition > FIRST_TAB)
            state.moveToTab.value = currentTabPosition.dec()
        else
            state.showBackWarning.call()
    }

    companion object {
        const val FIRST_TAB = 0
        const val LAST_TAB = 2
    }

    override fun onBackButtonClicked(view: View) {
        onBackPressed()
    }
}
