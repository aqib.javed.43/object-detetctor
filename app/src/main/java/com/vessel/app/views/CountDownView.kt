package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.DimenRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import com.vessel.app.common.binding.setTextSize
import kotlinx.android.synthetic.main.view_countdown.view.*

class CountDownView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        inflate(getContext(), R.layout.view_countdown, this)
        val res = intArrayOf(android.R.attr.textSize)
        val arr = context.obtainStyledAttributes(attrs, res)
        val resTextSize = arr.getResourceId(0, -1)
        if (resTextSize != -1)
            setTextSize(resTextSize)
    }

    fun setMinutes(text: String) {
        minutes.text = text
    }

    fun setSeconds(text: String) {
        seconds.text = text
    }

    fun setTextSize(@DimenRes textSizeRes: Int) {
        textSizeRes.let {
            minutes.setTextSize(it)
            seconds.setTextSize(it)
            colon.setTextSize(it)
        }
    }
}
