package com.vessel.app.createnewtpost

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CreateNewPostState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: CreateNewPostState.() -> Unit) = apply(block)
}
