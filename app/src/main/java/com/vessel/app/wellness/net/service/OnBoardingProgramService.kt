package com.vessel.app.wellness.net.service

import com.vessel.app.common.net.data.OnBoardingProgramResponseData
import retrofit2.http.GET
import retrofit2.http.Query

interface OnBoardingProgramService {
    @GET("program")
    suspend fun getOnBoardingProgram(
        @Query("goal_id") goalId: Int?
    ): OnBoardingProgramResponseData
}
