package com.vessel.app.nutritioncoach.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.nutritioncoach.NutritionCoachViewModel
import com.vessel.app.posttestnutritioncoach.ui.BulletStringAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_add_supplements.*

@AndroidEntryPoint
class NutritionCoachFragment : BaseFragment<NutritionCoachViewModel>() {
    override val viewModel: NutritionCoachViewModel by viewModels()
    override val layoutResId = R.layout.fragment_nutrition_coach
    val bulletStringAdapter by lazy { BulletStringAdapter() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        alignedList.adapter = bulletStringAdapter
    }
}
