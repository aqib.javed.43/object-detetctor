// TODO fix the unit test

// package com.vessel.app.wellness
//
// import com.vessel.app.BaseFaker
// import com.vessel.app.BaseFaker.Fake.FAKER
// import com.vessel.app.common.model.Reagent.*
// import com.vessel.app.common.model.ReagentEntry
// import com.vessel.app.wellness.model.Score
//
// object ScoreFaker {
//    fun basic() = FAKER.run {
//        Score(
//            wellness = WellnessEntryFaker.list(),
//            reagents = mapOf(
//                listOf(
//                    PH,
//                    Hydration,
//                    Ketones,
//                    VitaminC,
//                    Magnesium,
//                    B9,
//                    Cortisol,
//                    B7
//                ).random() to BaseFaker.list {
//                    ReagentEntry(
//                        x = number().randomNumber().toFloat(),
//                        y = number().randomNumber().toFloat(),
//                        date = date().birthday().time,
//                        score = number().randomNumber().toFloat()
//                    )
//                }
//            )
//        )
//    }
// }
