package com.vessel.app.views

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import com.vessel.app.R
import com.vessel.app.databinding.SwipeItemViewBinding

open class SwipeItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private lateinit var binding: SwipeItemViewBinding

    init {
        initView(attrs, context)
    }

    private fun initView(attrs: AttributeSet?, context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = SwipeItemViewBinding.inflate(layoutInflater, this, true)
        context.withStyledAttributes(attrs, R.styleable.SwipeItemView) {
            setText(getString(R.styleable.SwipeItemView_swipeText).orEmpty())
            setIcon(getResourceId(R.styleable.SwipeItemView_swipeIcon, 0))
            setBackgroundTint(getResourceId(R.styleable.SwipeItemView_swipeColor, 0))
        }
    }

    fun setText(text: String) {
        binding.text.text = text
    }

    fun setIcon(@DrawableRes drawable: Int) {
        binding.icon.setImageResource(drawable)
    }

    fun setBackgroundTint(@ColorRes colorRes: Int) {
        binding.container.backgroundTintList = ColorStateList.valueOf(context.getColor(colorRes))
    }
}
