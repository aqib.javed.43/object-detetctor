package com.vessel.app.selectcard.ui

import android.content.Intent
import android.content.Intent.ACTION_SENDTO
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.selectcard.SelectCardViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SelectCardFragment : BaseFragment<SelectCardViewModel>() {
    override val viewModel: SelectCardViewModel by viewModels()
    override val layoutResId = R.layout.fragment_select_card

    override fun onViewLoad(savedInstanceState: Bundle?) {
        view?.findViewById<AppCompatTextView>(R.id.noticeTextView)?.apply {
            val fullText = getString(R.string.were_adding_support_for_more_test_cards)
            val linkText = getString(R.string.wegotyou_email)
            text = SpannableStringBuilder(fullText)
                .apply {
                    setSpan(
                        weGotYouSpan,
                        fullText.indexOf(linkText),
                        fullText.indexOf(linkText) + linkText.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }
            movementMethod = LinkMovementMethod.getInstance()
        }
    }

    private val weGotYouSpan = object : ClickableSpan() {
        override fun onClick(widget: View) {
            startActivity(
                Intent(ACTION_SENDTO)
                    .apply {
                        data = Uri.parse("mailto:")
                        putExtra(Intent.EXTRA_EMAIL, arrayOf(getString(R.string.wegotyou_email)))
                        putExtra(Intent.EXTRA_SUBJECT, getString(R.string.test_card_recommendation))
                    }
            )
        }
    }
}
