package com.vessel.app.plan.ui

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.google.android.material.tabs.TabLayout
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.util.DisclaimerFooter
import com.vessel.app.common.util.DisclaimerFooterAdapter
import com.vessel.app.plan.PlanViewModel
import com.vessel.app.plan.model.AddPlanFooter
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.PlanDeleteDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_plan.*
import kotlinx.android.synthetic.main.item_plan_header.*
import kotlinx.android.synthetic.main.item_plan_header.buttonBadge
import kotlinx.android.synthetic.main.item_plan_header.filterTabLayout
import kotlinx.android.synthetic.main.item_plan_header.moreInfoButton
import kotlinx.android.synthetic.main.item_plan_header.planProgramCardWidget
import kotlinx.android.synthetic.main.item_today_header.*

@AndroidEntryPoint
class PlanFragment : BaseFragment<PlanViewModel>() {
    override val viewModel: PlanViewModel by viewModels()
    override val layoutResId = R.layout.fragment_plan

    private val planAdapter by lazy { DayPlanAdapter(viewModel) }
    private val emptyPlanAdapter by lazy { EmptyPlanAdapter(viewModel) }
    private val chatPlanFooterAdapter by lazy { PlanChatFooterAdapter(viewModel) }
    private val addPlanFooterAdapter by lazy { AddPlanFooterAdapter(viewModel) }
    private val disclaimerFooterAdapter by lazy { DisclaimerFooterAdapter() }
    private var updateSelectedTab = false

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
        setupFilterTab()
        setupPlanHeader()
        setupPrograms()
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadPrograms(true)
        viewModel.loadUserPlans(isFromUpdatedState = true)
    }

    private fun setupPrograms() {
        planProgramCardWidget.setHandler(viewModel)
    }

    private fun setupList() {
        planList.also {
            val adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                planAdapter,
                emptyPlanAdapter,
                addPlanFooterAdapter,
                chatPlanFooterAdapter,
                disclaimerFooterAdapter
            )
            it.itemAnimator = null
            it.adapter = adapter
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(addPlanChatFooterItem) {
                chatPlanFooterAdapter.submitList(listOf(it))
            }
            observe(planItems) { items ->
                planAdapter.submitList(items.first) {
                    planAdapter.notifyDataSetChanged()
                }
                filterTabLayout.isVisible = planItems.value?.first!!.isEmpty().not()
            }
            observe(deletePlanItem) { item ->
                planAdapter.notifyItemRemoved(item)
            }
            observe(planNotificationCount) {
                buttonBadge.text = it.toString()
            }
            observe(showPlanNotification) {
                buttonBadge.isVisible = it
            }
            observe(openMessaging) {
                findNavController().navigate(PostTestNavGraphDirections.globalActionToNutritionOptionsFragment())
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(showProgramLoading) {
                planProgramCardWidget.isLoadingVisible(it)
            }
            observe(programsItems) {
                planProgramCardWidget.setList(it)
            }
            observe(viewModel.state.removeReminder) {
                PlanDeleteDialog.showDialog(
                    requireActivity(),
                    it.first.title,
                    {
                        // Not used
                    },
                    {
                        viewModel.deletePlanItems(it.first, it.second, it.third)
                    },
                    {
                        // Not used
                    }
                )
            }
            observe(readyToPopulate) {
                if (filterTabLayout.selectedTabPosition == DAILY_TAB_POSITION) {
                    viewModel.onDailyFilterSelected(updateSelectedTab)
                } else if (filterTabLayout.selectedTabPosition == WEEKLY_TAB_POSITION) {
                    viewModel.onWeeklyFilterSelected(updateSelectedTab)
                }
            }
        }
        disclaimerFooterAdapter.submitList(listOf(DisclaimerFooter.WithBottomNavPadding))
        addPlanFooterAdapter.submitList(listOf(AddPlanFooter))
    }

    private fun setupPlanHeader() {
        moreInfoButton.isVisible = true
        moreInfoButton.setOnClickListener {
            viewModel.onPlanItemClicked(AddPlanFooter, it, 0)
        }
    }

    private fun setupFilterTab() {
        updateSelectedTab = false
        filterTabLayout.apply {
            addOnTabListener(this)
        }

        if (viewModel.isDailyPlanView()) {
            filterTabLayout.getTabAt(DAILY_TAB_POSITION)?.select()
        } else {
            filterTabLayout.getTabAt(WEEKLY_TAB_POSITION)?.select()
        }
    }

    private fun addOnTabListener(tabLayout: TabLayout) {
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == DAILY_TAB_POSITION) {
                    viewModel.onDailyFilterSelected(updateSelectedTab)
                } else if (tab.position == WEEKLY_TAB_POSITION) {
                    viewModel.onWeeklyFilterSelected(updateSelectedTab)
                }
                updateSelectedTab = true
                if (viewModel.isDailyPlanView()) {
                    filterTabLayout.getTabAt(DAILY_TAB_POSITION)?.select()
                } else {
                    filterTabLayout.getTabAt(WEEKLY_TAB_POSITION)?.select()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.daily_plan)))
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.weekly_plan)))
    }

    companion object {
        private const val DAILY_TAB_POSITION = 0
        private const val WEEKLY_TAB_POSITION = 1
    }
}
