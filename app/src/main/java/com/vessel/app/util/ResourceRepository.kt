package com.vessel.app.util

import android.content.ContentResolver
import android.content.Context
import android.content.res.AssetManager
import androidx.annotation.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.vessel.app.common.manager.LoggingManager
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResourceRepository @Inject constructor(
    @ApplicationContext private val context: Context,
    private val loggingManager: LoggingManager
) {

    fun getString(@StringRes resId: Int) = context.getString(resId)

    fun <T> getQuantityString(
        @PluralsRes resId: Int,
        quantity: Int,
        vararg formatArgs: T
    ) = context.resources.getQuantityString(resId, quantity, *formatArgs)

    fun <T> getString(@StringRes resId: Int, vararg formatArgs: T): String =
        context.getString(resId, *formatArgs)

    fun getColor(@ColorRes resId: Int) = ContextCompat.getColor(context, resId)

    fun getDimension(@DimenRes resId: Int) = context.resources.getDimension(resId)
    fun getFont(@FontRes resId: Int) = ResourcesCompat.getFont(context, resId)
    fun getAssets(): AssetManager = context.assets

    fun getDrawableIdentifier(drawableName: String): Int {
        return context.resources.getIdentifier(
            drawableName,
            "drawable",
            context.packageName
        )
    }

    fun readFile(filePath: String): String? {
        var file: String? = null
        try {
            file = getAssets().open(filePath).bufferedReader()
                .use { it.readText() }
        } catch (io: IOException) {
            loggingManager.logError(io)
        }
        return file
    }

    fun getDrawable(@DrawableRes resId: Int) = ContextCompat.getDrawable(context, resId)

    fun getContentResolver(): ContentResolver = context.contentResolver
}
