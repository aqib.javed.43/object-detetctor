package com.vessel.app.stressrelief.ui

import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.stressrelief.model.StressReliefDayItem

class StressReliefDayAdapter(private val handler: StressReliefAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<StressReliefDayItem>() {

    override fun getLayout(position: Int) = R.layout.item_day_stress_plan

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<StressReliefDayItem>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as StressReliefDayItem
        val planAdapter = StressReliefAdapter(handler, position)
        holder.itemView.findViewById<TextView>(R.id.title).apply {
            text = item.title
            setTextAppearance(if (item.isDailyItem) R.style.Title else R.style.NeoTextRegualr)
        }

        holder.itemView.findViewById<RecyclerView>(R.id.planList).apply {
            adapter = planAdapter
            itemAnimator = null
        }

        planAdapter.submitList(item.items)
    }
}
