package com.vessel.app.base.navigation

import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment

class BloomNavHostFragment : NavHostFragment() {

    override fun onCreateNavController(navController: NavController) {
        super.onCreateNavController(navController)

        context?.let {
            navController.navigatorProvider.addNavigator(ChromeCustomTabsNavigator(it))
        }
    }
}
