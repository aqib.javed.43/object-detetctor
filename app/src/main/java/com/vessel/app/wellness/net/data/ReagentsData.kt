package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass
import com.vessel.app.common.model.data.ReagentState
import com.vessel.app.common.net.data.ReagentResponseInfoData

@JsonClass(generateAdapter = true)
data class ReagentsData(
    val recommended_daily_allowance: Float? = null,
    val state: ReagentState,
    val type: ReagentType? = null,
    val id: Int,
    val unit: String,
    val consumption_unit: String? = null,
    val name: String,
    val supplement_id: Long? = null,
    val info: List<ReagentResponseInfoData>,
    val impact: Int,
    val reagent_type: String? = null
)
