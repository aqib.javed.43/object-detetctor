package com.vessel.app.programs.complete.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.weekdays.getDaysOfWeek
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_complete_program.*
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity
import java.util.*

@AndroidEntryPoint
class CompleteProgramFragment : BaseDialogFragment<CompleteProgramViewModel>() {
    override val viewModel: CompleteProgramViewModel by viewModels()
    override val layoutResId = R.layout.fragment_complete_program
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        val dayOfWeek = getDaysOfWeek(requireContext()).firstOrNull {
            it.calendarDay == Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        }?.planIndex ?: Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        weekDaysView.setSelectedDays(listOf(dayOfWeek))
    }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
        isCancelable = false
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(dismissDialog) {
                dismiss()
            }
            observe(openMessaging) {
                MessagingActivity.builder()
                    .withToolbarTitle(getString(R.string.chat_to_nutritionist))
                    .withEngines(ChatEngine.engine())
                    .show(requireContext(), viewModel.getChatConfiguration())
            }
        }
    }
}
