package com.vessel.app.goalsselection.posttest

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PostTestGoalsSelectState @Inject constructor() {
    val items = MutableLiveData<List<GoalSelect>>()
    val refreshItems = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: PostTestGoalsSelectState.() -> Unit) = apply(block)
}
