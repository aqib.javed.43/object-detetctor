package com.vessel.app.common.net.payload

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AddTipPayload(
    val title: String,
    val image_url: String?,
    val frequency: String?,
    val main_goal_id: Int?
)
