package com.vessel.app.programs.ui

import android.content.Context
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Program
import com.vessel.app.common.widget.loaderwidget.PlanProgramsCardWidgetOnActionHandler
import kotlinx.android.synthetic.main.item_plan_program.view.*

class PlanProgramAdapter(private val handler: PlanProgramsCardWidgetOnActionHandler) :
    BaseAdapterWithDiffUtil<Program>(
        areItemsTheSame = { oldItem, newItem -> oldItem.getCompletedItemPercent() == newItem.getCompletedItemPercent() }
    ) {
    override fun getLayout(position: Int) = R.layout.item_plan_program

    override fun onBindViewHolder(holder: BaseViewHolder<Program>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as Program
        holder.itemView.programLoaderWidget.apply {
            setProgressHint(getRemainingPeriodHint(item, holder.itemView.context))
            setProgress(item.getCompletedItemPercent())
        }
    }

    private fun getRemainingPeriodHint(item: Program, context: Context) =
        if (item.isEnded()) {
            context.getString(R.string.program_is_over)
        } else if (item.isInLastDay()) {
            context.getString(R.string.las_day)
        } else {
            context.resources.getQuantityString(R.plurals.remaining_days, item.getRemainingPeriod(), item.getRemainingPeriod())
        }

    override fun getHandler(position: Int) = handler
}
