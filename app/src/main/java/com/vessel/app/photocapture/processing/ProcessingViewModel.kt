package com.vessel.app.photocapture.processing

import android.os.CountDownTimer
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Result
import com.vessel.app.common.net.HttpStatusCode.BAD_REQUEST
import com.vessel.app.common.net.HttpStatusCode.NOT_FOUND
import com.vessel.app.common.net.HttpStatusCode.UNPROCESSABLE_ENTITY
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.SampleRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.taketest.model.UploadTestFileModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.ScoresData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.TimeUnit

class ProcessingViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val awsManager: AwsManager,
    private val sampleRepository: SampleRepository,
    private val scoreManager: ScoreManager,
    private val recommendationManager: RecommendationManager,
    private val planManager: PlanManager,
    private val foodManager: FoodManager,
    private val preferencesRepository: PreferencesRepository,
    private val fileManager: FileManager,
    private val trackingManager: TrackingManager,
    private val loggingManager: LoggingManager,
) : BaseViewModel(resourceProvider) {

    val state = ProcessingState()
    private var wellnessCardUuid: String? = null
    var latestSampleUuid: String? = null

    private val timer = object : CountDownTimer(TIME_TO_STOP_WHEN_NOT_COMPLETED, SECOND) {
        override fun onTick(millisUntilFinished: Long) {
            PROCESSING_EXPECTED_TIMER.minus(millisUntilFinished).toDouble()
                .div(PROCESSING_EXPECTED_TIMER).times(100.0)
        }

        override fun onFinish() {}
    }

    fun associateTestUuid(uploadTestFileModel: UploadTestFileModel) {
        wellnessCardUuid = uploadTestFileModel.wellnessCardUuid
        state.uuid = uploadTestFileModel.uuid
        state.operationTitle.value = R.string.uploading_image
        val wellnessCardUuid = uploadTestFileModel.wellnessCardUuid
        val firstFailedTestUUID = uploadTestFileModel.firstFailedTestUUID
        val autoScan = uploadTestFileModel.autoScan
        viewModelScope.launch(Dispatchers.IO) {
            sampleRepository.associateTestUuid(
                state.uuid!!,
                wellnessCardUuid,
                firstFailedTestUUID,
                autoScan
            )
                .onSuccess {
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.SAMPLE_ASSOCIATION_SUCCESS)
                            .addProperty(TrackingConstants.KEY_UUID, state.uuid!!)
                            .addProperty(TrackingConstants.KEY_WELLNESS_CARD_UUID, wellnessCardUuid)
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    uploadSampleImage(
                        uploadTestFileModel,
                        it.wellness_card_batch_id,
                        it.wellness_card_calibration_mode,
                        it.orca_sheet_name
                    )
                }
                .onError {
                    val serviceError = (it as Result.ServiceError)
                    when (serviceError.code) {
                        BAD_REQUEST -> {
                            if (serviceError.error.message == "Card already scanned successfully") {
                                val error = getResString(R.string.test_association_failed)
                                trackingManager.log(
                                    TrackedEvent.Builder(TrackingConstants.RESCAN_ERROR)
                                        .addProperty(TrackingConstants.KEY_UUID, state.uuid!!)
                                        .addProperty(TrackingConstants.KEY_ERROR, error)
                                        .withKlaviyo()
                                        .withFirebase()
                                        .withAmplitude()
                                        .create()
                                )
                                latestSampleUuid = serviceError.error.latest_sample_uuid
                                showError(ProcessingErrorUiModel.CARD_ASSOCIATION_ERROR)
                            } else {
                                showError(ProcessingErrorUiModel.SCANNING_ERROR)
                            }
                        }
                        NOT_FOUND -> {
                            showError(ProcessingErrorUiModel.SCANNING_ERROR)
                        }
                        UNPROCESSABLE_ENTITY -> {
                            showError(ProcessingErrorUiModel.SCANNING_ERROR)
                        }
                        else -> {
                            showError(ProcessingErrorUiModel.UPLOAD_ERROR)
                        }
                    }
                }
        }
    }

    private fun uploadSampleImage(
        uploadTestFileModel: UploadTestFileModel,
        wellnessCardBatchId: String?,
        wellnessCardCalibrationMode: String?,
        orcaSheetName: String?
    ) {
        val startUploadRequestInMillis = Calendar.getInstance().timeInMillis
        awsManager.uploadFileToS3(
            uuid = state.uuid!!,
            file = uploadTestFileModel.file,
            jpgFile = uploadTestFileModel.jpgFile,
            wellnessCardBatchId = wellnessCardBatchId,
            wellnessCardCalibrationMode = wellnessCardCalibrationMode,
            orcaSheetName = orcaSheetName,
            captureResult = uploadTestFileModel.captureResult,
            timerStartDate = uploadTestFileModel.timerStartDate,
            captureDate = uploadTestFileModel.captureDate,
            onUploadCompleted = {
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.SAMPLE_IMAGE_UPLOAD_SUCCESS)
                        .addProperty(TrackingConstants.KEY_UUID, state.uuid!!)
                        .addProperty(
                            TrackingConstants.KEY_WELLNESS_CARD_UUID,
                            uploadTestFileModel.wellnessCardUuid
                        )
                        .addProperty(
                            TrackingConstants.KEY_UPLOAD_TIME_MS,
                            (Calendar.getInstance().timeInMillis - startUploadRequestInMillis).toString()
                        )
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
                state.showUploadProgress.postValue(false)
                timer.start()
                pollForTestResults()
                fileManager.deleteTempFile()
            },
            onUploadFailed = {
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.SAMPLE_IMAGE_UPLOAD_FAILURE)
                        .addProperty(TrackingConstants.KEY_UUID, state.uuid!!)
                        .addProperty(
                            TrackingConstants.KEY_WELLNESS_CARD_UUID,
                            uploadTestFileModel.wellnessCardUuid
                        )
                        .addProperty(
                            TrackingConstants.KEY_UPLOAD_TIME_MS,
                            (Calendar.getInstance().timeInMillis - startUploadRequestInMillis).toString()
                        )
                        .addProperty(
                            TrackingConstants.KEY_ERROR,
                            getResString(R.string.upload_failed)
                        )
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
                showError(ProcessingErrorUiModel.UPLOAD_ERROR)
                fileManager.deleteTempFile()
            },
            onProgressChanged = { value ->
                state.uploadProgress.postValue(value)
                state.showUploadProgress.postValue(value > 0)
            },
            onUploadWaitingForNetwork = {
                showError(ProcessingErrorUiModel.TIMEOUT_ERROR)
                fileManager.deleteTempFile()
            }
        )
    }

    private fun pollForTestResults() {
        state.operationTitle.value = R.string.processing_subtitle
        if (state.getScoreTryCount >= GET_SCORE_MAX_TRY) {
            showErrorAndCancelTimer(ProcessingErrorUiModel.API_ERROR)
            return
        }

        state.getScoreTryCount++

        val processingStartTimeInMilli = Calendar.getInstance().timeInMillis
        viewModelScope.launch {
            delay(DELAY_BETWEEN_TRIES_IN_MS)
            state.uuid?.let { uuid ->
                sampleRepository.getScoreForId(uuid)
                    .onSuccess {
                        wellnessCardUuid?.let {
                            trackingManager.log(
                                TrackedEvent.Builder(TrackingConstants.SAMPLE_RESULTS_RETRIEVED_SUCCESS)
                                    .addProperty(TrackingConstants.KEY_UUID, uuid)
                                    .addProperty(TrackingConstants.KEY_WELLNESS_CARD_UUID, it)
                                    .addProperty(
                                        TrackingConstants.KEY_PROCESSING_TIME_MS,
                                        (Calendar.getInstance().timeInMillis - processingStartTimeInMilli).toString()
                                    )
                                    .withKlaviyo()
                                    .withFirebase()
                                    .withAmplitude()
                                    .create()
                            )
                        }

                        planManager.clearCachedData()
                        recommendationManager.clearCachedData()
                        if (preferencesRepository.testCompletedCount == 0) {
                            scoreManager.updateFirstTestState()
//                            devicePreferencesRepository.showPlanReadyHint = true
                        } else {
                            scoreManager.updateTestState()
                        }

                        // This to make sure the test completed count updated and it will be overrided by scoreManager.updateWellnessScoreCachedData
                        preferencesRepository.testCompletedCount += 1

                        scoreManager.run {
                            updatePlanRecommendationTestState()
                            clearCachedData()
                            updateWellnessScoreCachedData()
                            updateReagentDeficienciesCachedData()
                        }

                        foodManager.updateFoodCachedData()

                        recommendationManager.run {
                            clearCachedData()
                            updateLatestSampleRecommendationsCachedData()
                            updateLatestSampleFoodRecommendationsCachedData()
                        }

//                        todosManager.run {
//                            clearCachedData()
//                            updateRecurringTodosCachedData()
//                            updateTodayTodosCachedData()
//                        }

                        handleTestScoreResult(it)
                    }
                    .onServiceError {
                        when {
                            it.code == NOT_FOUND -> {
                                logErrorEvent(
                                    TrackingConstants.SAMPLE_RESULTS_RETRIEVED_FAILURE,
                                    processingStartTimeInMilli,
                                    it.error.message ?: ""
                                )
                                pollForTestResults()
                            }
                            it.error.errors.isNullOrEmpty().not() -> {
                                val errorList = it.error.errors
                                when {
                                    errorList?.firstOrNull { errorItem -> errorItem.code == BE_CHEMISTRY_ERROR_CODE } != null -> {
                                        logErrorEvent(
                                            TrackingConstants.BE_CHEMISTRY_ERROR,
                                            processingStartTimeInMilli
                                        )
                                        showError(ProcessingErrorUiModel.CHEMISTRY_ERROR)
                                    }
                                    errorList?.firstOrNull { errorItem -> errorItem.code == BE_CARD_ERROR_CODE_1 || errorItem.code == BE_CARD_ERROR_CODE_2 } != null -> {
                                        logErrorEvent(
                                            TrackingConstants.BE_CARD_ERROR,
                                            processingStartTimeInMilli
                                        )
                                        showError(ProcessingErrorUiModel.CARD_ERROR)
                                    }
                                    errorList?.firstOrNull { errorMessageItem -> errorMessageItem.code == BE_SHADOW_ERROR_CODE } != null -> {
                                        logErrorEvent(
                                            TrackingConstants.SHADOW_ERROR,
                                            processingStartTimeInMilli
                                        )
                                        showError(ProcessingErrorUiModel.SHADOW_ERROR)
                                    }
                                    else -> {
                                        logErrorEvent(
                                            TrackingConstants.BE_API_ERROR,
                                            processingStartTimeInMilli
                                        )
                                        showErrorAndCancelTimer(ProcessingErrorUiModel.API_ERROR)
                                    }
                                }
                            }
                            it.code == UNPROCESSABLE_ENTITY -> {
                                logErrorEvent(
                                    TrackingConstants.BE_CARD_ERROR,
                                    processingStartTimeInMilli
                                )
                                showErrorAndCancelTimer(ProcessingErrorUiModel.CARD_ERROR)
                            }
                            else -> {
                                logErrorEvent(
                                    TrackingConstants.BE_API_ERROR,
                                    processingStartTimeInMilli
                                )
                                showErrorAndCancelTimer(ProcessingErrorUiModel.API_ERROR)
                            }
                        }
                    }
                    .onNetworkIOError {
                        logErrorEvent(
                            TrackingConstants.BE_TIMEOUT_ERROR,
                            processingStartTimeInMilli
                        )
                        showErrorAndCancelTimer(ProcessingErrorUiModel.TIMEOUT_ERROR)
                    }
                    .onUnknownError {
                        logErrorEvent(
                            TrackingConstants.BE_TIMEOUT_ERROR,
                            processingStartTimeInMilli
                        )
                        showErrorAndCancelTimer(ProcessingErrorUiModel.TIMEOUT_ERROR)
                    }
            }
        }
    }

    private fun handleTestScoreResult(score: ScoresData) {
        timer.cancel()

        val reagentsErrors = score.reagents.orEmpty().filter { it.score == null }

        if (reagentsErrors.isNullOrEmpty()) {
            state.goToPostTest.call()
        } else {
            state.goToLFAErrorView.value = reagentsErrors
        }
    }

    private fun logErrorEvent(
        eventName: String,
        processingStartTimeInMilli: Long,
        message: String = ""
    ) {
        state.uuid?.let { uuid ->
            wellnessCardUuid?.let { wellnessCardUuid ->
                trackingManager.log(
                    TrackedEvent.Builder(eventName)
                        .addProperty(TrackingConstants.KEY_UUID, uuid)
                        .addProperty(TrackingConstants.KEY_WELLNESS_CARD_UUID, wellnessCardUuid)
                        .addProperty(
                            TrackingConstants.KEY_UPLOAD_TIME_MS,
                            (Calendar.getInstance().timeInMillis - processingStartTimeInMilli).toString()
                        )
                        .addProperty(TrackingConstants.KEY_ERROR, message)
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()

                )
            }
        }
    }

    private fun showErrorAndCancelTimer(error: ProcessingErrorUiModel) {
        timer.cancel()
        state.showErrorDialog.postValue(error)
    }

    private fun showError(error: ProcessingErrorUiModel) {
        state.showErrorDialog.postValue(error)
    }

    fun logNavigationPathIssue(ignored: Exception) {
        loggingManager.logError(ignored)
    }

    companion object {
        private const val GET_SCORE_MAX_TRY = 30
        private const val DELAY_BETWEEN_TRIES_IN_MS = 500L
        private val SECOND = TimeUnit.SECONDS.toSeconds(1)
        private val PROCESSING_EXPECTED_TIMER = TimeUnit.MINUTES.toMillis(1)
        private val TIME_TO_STOP_WHEN_NOT_COMPLETED = TimeUnit.SECONDS.toMillis(50)
        private const val BE_CHEMISTRY_ERROR_CODE = 20
        private const val BE_CARD_ERROR_CODE_1 = 19
        private const val BE_CARD_ERROR_CODE_2 = 2
        private const val BE_SHADOW_ERROR_CODE = 5
    }
}
