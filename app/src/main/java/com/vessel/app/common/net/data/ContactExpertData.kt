package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ContactExpertData(
    val id: Int,
    val first_name: String?,
    val last_name: String?,
    val email: String?,
    val occupation: String?,
    val image_url: String?,
    val is_verified: Boolean?,
    val about: String?
) : Parcelable
