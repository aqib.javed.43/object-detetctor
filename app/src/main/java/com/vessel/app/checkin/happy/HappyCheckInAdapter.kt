package com.vessel.app.checkin.happy

import android.text.InputType
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.plan.model.PlanItem
import kotlinx.android.synthetic.main.item_issue.view.*

class HappyCheckInAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<PlanItem>() {
    override fun getLayout(position: Int) = R.layout.item_activity_lesson

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onItemClicked(items: List<PlanItem>)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<PlanItem>, position: Int) {
        val item = getItem(position)
        if (item.type == PlanType.TestPlan) {
            holder.itemView.findViewById<View>(R.id.fadding).isVisible = false
            holder.itemView.findViewById<View>(R.id.improvesData).isVisible = false
            holder.itemView.findViewById<View>(R.id.goalsData).isVisible = false
        } else {
            val goals = item.currentPlan.getPlanGoals()
            val improves = item.currentPlan.getImproves()
            holder.itemView.findViewById<View>(R.id.fadding).isVisible =
                goals.trim().isNotEmpty() || improves.trim().isNotEmpty()
            holder.itemView.findViewById<TextView>(R.id.goalsData).apply {
                text = goals
                isVisible = goals.trim().isNotEmpty()
            }
            holder.itemView.findViewById<TextView>(R.id.improvesData).apply {
                text = improves
                isVisible = improves.trim().isNotEmpty()
            }

            if (improves.trim().isEmpty() || goals.trim().isEmpty()) {
                holder.itemView.findViewById<TextView>(R.id.goalsData).apply {
                    inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    isElegantTextHeight = true
                    isSingleLine = false
                }
                holder.itemView.findViewById<TextView>(R.id.improvesData).apply {
                    inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    isElegantTextHeight = true
                    isSingleLine = false
                }
            }
        }
        super.onBindViewHolder(holder, position)
    }
}
