package com.vessel.app.welcome

import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class WelcomeState @Inject constructor() {
    val showBecomeTester = LiveEvent<Unit>()
    operator fun invoke(block: WelcomeState.() -> Unit) = apply(block)
}
