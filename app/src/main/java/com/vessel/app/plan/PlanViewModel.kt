package com.vessel.app.plan

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.Constants
import com.vessel.app.Constants.DAY_DATE_FORMAT
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Program
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.PlanRecord
import com.vessel.app.common.net.data.plan.toPlanRequest
import com.vessel.app.common.net.mapper.ProgramMapper
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.loaderwidget.PlanProgramsCardWidgetOnActionHandler
import com.vessel.app.foodplan.model.FoodDetailsItemModel
import com.vessel.app.homescreen.HomeScreenFragmentDirections
import com.vessel.app.plan.model.*
import com.vessel.app.plan.ui.AddPlanFooterAdapter
import com.vessel.app.plan.ui.EmptyPlanAdapter
import com.vessel.app.plan.ui.PlanAdapter
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.stressreliefdetails.model.StressReliefDetailsItemModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import java.util.*
import kotlin.collections.LinkedHashMap

class PlanViewModel @ViewModelInject constructor(
    val state: PlanState,
    resourceProvider: ResourceRepository,
    private val planManager: PlanManager,
    private val buildPlanManager: BuildPlanManager,
    val trackingManager: TrackingManager,
    val tipMapper: TipMapper,
    private val programMapper: ProgramMapper,
    private val preferencesRepository: PreferencesRepository,
    private val reminderManager: ReminderManager,
    private val programManager: ProgramManager
) : BaseViewModel(resourceProvider),
    PlanAdapter.OnActionHandler,
    AddPlanFooterAdapter.OnActionHandler,
    EmptyPlanAdapter.OnActionHandler,
    PlanProgramsCardWidgetOnActionHandler {
    override val showPlanNotification = state.showPlanNotification
    override val planNotificationCount = state.planNotificationCount
    var isLoadingInProgress = false

    init {
        state.addPlanChatFooterItem.value = PlanChatFooter
        calculateNumberOfRecommendations()
        observeTodayTodoDataState()
        observeRemindersDataState()
        loadUserPlans()
    }

    fun loadPrograms(showLoader: Boolean = false) {
        if (showLoader)
            state.showProgramLoading.value = true
        viewModelScope.launch(Dispatchers.IO, CoroutineStart.ATOMIC) {
            programManager.getEnrolledPrograms()
                .onSuccess { enrolledPrograms ->
                    state.programsItems.postValue(enrolledPrograms.filterNot { it.isEnded() })
                    state.showProgramLoading.postValue(false)
                    showTheCompletedProgramsIfNeeded(enrolledPrograms)
                }
                .onServiceError {
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    state.showProgramLoading.postValue(false)
                }
        }
    }

    private fun showTheCompletedProgramsIfNeeded(enrolledPrograms: List<Program>) {
        if (preferencesRepository.checkCompletedProgram) {
            val endedProgram = enrolledPrograms.firstOrNull { it.isEnded() }
            if (endedProgram != null) {
            } else {
                preferencesRepository.checkCompletedProgram = false
            }
        }
    }

    fun loadUserPlans(forceUpdate: Boolean = false, isFromUpdatedState: Boolean = false) {
        if (isLoadingInProgress) return
        if (!isFromUpdatedState)
            showLoading()
        isLoadingInProgress = true
        viewModelScope.launch {
            planManager.getUserPlans(forceUpdate)
                .onSuccess { items ->
                    calculateNumberOfRecommendations()
                    val groupedFoodItems = items.filter {
                        it.food_id != null
                    }.groupBy {
                        it.food_id
                    }

                    val groupedLifeStyleItemsItems = items.filter {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null
                    }.groupBy {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                    }

                    val groupedSupplementItems = items.filter {
                        it.isSupplementPlan()
                    }

                    val groupedTips = items.filter {
                        it.isTip()
                    }.groupBy {
                        it.tip_id
                    }

                    setupWeeklyData(
                        groupedFoodItems,
                        groupedLifeStyleItemsItems,
                        groupedSupplementItems,
                        groupedTips
                    )
                    setupDailyData(
                        groupedFoodItems,
                        groupedLifeStyleItemsItems,
                        groupedSupplementItems,
                        groupedTips
                    )
                    state.readyToPopulate.call()
                    hideLoading(false)
                    isLoadingInProgress = false
                }
                .onError {
                    hideLoading(false)
                    isLoadingInProgress = false
                }
        }
    }

    private fun setupDailyData(
        groupedFoodItems: Map<Int?, List<PlanData>>,
        groupedLifeStyleItemsItems: Map<Int?, List<PlanData>>,
        groupedSupplementItems: List<PlanData>,
        groupedTips: Map<Int?, List<PlanData>>
    ) {
        val dailyItems = mutableListOf<PlanItem>()
        groupedFoodItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapToDoToPlanItem(
                        it,
                        true,
                        1,
                        grouped,
                        grouped
                    )
                )
            }
        }
        groupedTips.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapToDoToPlanItem(
                        it,
                        true,
                        1,
                        grouped,
                        grouped
                    )
                )
            }
        }
        groupedLifeStyleItemsItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapToDoToPlanItem(
                        it,
                        true,
                        1,
                        grouped,
                        grouped
                    )
                )
            }
        }
        groupedSupplementItems.map { item ->
            dailyItems.add(
                mapToDoToPlanItem(
                    item,
                    true,
                    1,
                    groupedSupplementItems,
                    groupedSupplementItems
                )
            )
        }

        val daysPlans = mutableMapOf<Date, MutableList<PlanItem>>()
        dailyItems.onEach { planItem ->
            planItem.getOccurrenceDates().onEach { day ->
                if (daysPlans.containsKey(day)) {
                    if (daysPlans[day]?.contains(planItem) == false)
                        daysPlans[day]?.add(planItem)
                } else {
                    daysPlans[day] = mutableListOf(planItem)
                }
            }
        }

        val sortedDaysPlans: MutableMap<Date, MutableList<PlanItem>> = LinkedHashMap()
        daysPlans.keys.sorted().forEach { date ->
            sortedDaysPlans[date] = daysPlans[date]!!
        }

        val dailyPlans = mutableListOf<DayPlanItem>()
        sortedDaysPlans.forEach { daysPlan ->
            val dayItems = daysPlan.value
            val date = daysPlan.key
            val planItems = togglePlanItems(dayItems, date)
            dailyPlans.add(
                DayPlanItem(
                    planItems.count { it.isCompleted },
                    planItems.size,
                    date.formatPlanDate(),
                    getSortedPlans(planItems),
                    date
                )
            )
        }

        dailyPlans.removeIf { it.items.isEmpty() }
        state.todayTodosData = dailyPlans
        /*if ((
            preferencesRepository.isDailyPlanView && state.planItems.value?.first.orEmpty()
                .isEmpty()
            ) || (forceUpdate && preferencesRepository.isDailyPlanView) && !isFromUpdatedState
        ) {
            loadPlan(state.todayTodosData, true)
        }*/
    }

    private fun togglePlanItems(
        dayItems: MutableList<PlanItem>,
        formatPlanDate: Date
    ): List<PlanItem> {
        return dayItems.map {
            it.copy(
                isCompleted = it.currentPlan.plan_records.orEmpty().firstOrNull {
                    it.completed == true && formatPlanDate.isSameDay(
                        Constants.DAY_DATE_FORMAT.parse(
                            it.date
                        )
                    )
                } != null
            )
        }
    }

    private fun getSortedPlans(planItems: List<PlanItem>): List<PlanItem> {
        return planItems.sortedBy {
            it.currentPlan.getTimeFormattedAsNumber()
        }
    }

    private fun setupWeeklyData(
        groupedFoodItems: Map<Int?, List<PlanData>>,
        groupedLifeStyleItemsItems: Map<Int?, List<PlanData>>,
        groupedSupplementItems: List<PlanData>,
        groupedTips: Map<Int?, List<PlanData>>
    ) {
        val weeklyPlans = mutableListOf<PlanItem>()

        groupedFoodItems.forEach { fullGroup ->
            fullGroup.value.groupBy { it.multiple }.forEach { internalItem ->
                weeklyPlans.add(
                    mapToDoToPlanItem(
                        internalItem.value.first(),
                        false,
                        internalItem.value.sumBy { it.getNonCompletedDays().size },
                        internalItem.value,
                        fullGroup.value
                    )
                )
            }
        }

        groupedTips.forEach { fullGroup ->
            fullGroup.value.groupBy { it.multiple }.forEach { internalItem ->
                weeklyPlans.add(
                    mapToDoToPlanItem(
                        internalItem.value.first(),
                        false,
                        internalItem.value.sumBy { it.getNonCompletedDays().size },
                        internalItem.value,
                        fullGroup.value
                    )
                )
            }
        }

        groupedLifeStyleItemsItems.forEach { fullGroup ->
            fullGroup.value.groupBy { it.multiple }.forEach { internalItem ->
                weeklyPlans.add(
                    mapToDoToPlanItem(
                        internalItem.value.first(),
                        false,
                        internalItem.value.sumBy { it.getNonCompletedDays().size },
                        internalItem.value,
                        fullGroup.value
                    )
                )
            }
        }

        if (groupedSupplementItems.isNotEmpty()) {
            weeklyPlans.add(
                mapToDoToPlanItem(
                    groupedSupplementItems.first(),
                    false,
                    groupedSupplementItems.sumBy { it.getNonCompletedDays().size },
                    groupedSupplementItems,
                    groupedSupplementItems,
                )
            )
        }

        val weekDates = getCurrentWeekDates()

        weeklyPlans.removeIf { it.todos.isEmpty() }

        if (weeklyPlans.isNotEmpty()) {
            state.weeklyTodosData = listOf(
                DayPlanItem(
                    getWeeklyCompletedItems(weeklyPlans),
                    getWeeklyItemsCount(weeklyPlans),
                    "${weekDates.first.formatPlanDate()} - ${weekDates.second.formatPlanDate()}",
                    weeklyPlans,
                    weekDates.first
                )
            )
        } else {
            state.weeklyTodosData = listOf()
        }

        /*if ((
            !preferencesRepository.isDailyPlanView && state.planItems.value?.first.orEmpty()
                .isEmpty()
            ) || (forceUpdate && !preferencesRepository.isDailyPlanView) && !isFromUpdatedState
        ) {
            loadPlan(state.weeklyTodosData, true)
        }*/
    }

    private fun calculateNumberOfRecommendations() {
        viewModelScope.launch(Dispatchers.IO) {
            buildPlanManager.loadPlanData()
            val count = buildPlanManager.countRedBadges()
            if (count > 0) state.planNotificationCount.postValue(count)
            state.showPlanNotification.postValue(count > 0)
        }
    }

    private fun observeTodayTodoDataState() {
        viewModelScope.launch {
            planManager.getPlanDataState().collect {
                if (it) {
                    planManager.clearPlanState()
                    loadUserPlans(true, true)
                }
            }
        }
    }

    private fun observeRemindersDataState() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    loadUserPlans(true)
                }
            }
        }
    }

    private fun loadPlan(items: List<DayPlanItem>, withUpdate: Boolean = false) {
        state.planItems.value = Pair(items, withUpdate)
    }

    override fun onPlanItemClicked(item: Any, view: View, parentPosition: Int) {
        if (item is PlanItem && item.type == PlanType.StressPlan) {
            view.findNavController()
                .navigate(
                    HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToStressReliefDetailsFragment(
                        StressReliefDetailsItemModel(
                            item.planName,
                            item.valueProp1.orEmpty(),
                            item.currentPlan.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                                ?: return,
                            true
                        )
                    )
                )
        } else if (item is PlanItem && item.type == PlanType.FoodPlan) {
            view.findNavController()
                .navigate(
                    HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToFoodPlanDetailsFragment(
                        FoodDetailsItemModel(
                            item.title,
                            item.valueProp1.orEmpty(),
                            item.plans.first().food_id ?: return,
                            true
                        )
                    )
                )
        } else if (item is PlanItem && item.type == PlanType.WaterPlan) {
            view.findNavController()
                .navigate(HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToHydrationPlanFragment())
        } else if (item is PlanItem && item.type == PlanType.TestPlan) {
            view.findNavController()
                .navigate(HomeScreenFragmentDirections.globalActionToTestingPlanFragment())
        } else if (item is PlanItem && item.type == PlanType.SupplementPlan) {
            view.findNavController()
                .navigate(HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToSupplementFragment())
        } else if (item is PlanItem && item.type == PlanType.TIP) {
            view.findNavController()
                .navigate(
                    HomeScreenFragmentDirections.globalActionToTipDetailsFragment(
                        tipMapper.mapTipData(
                            item.currentPlan.tip!!
                        )
                    )
                )
        } else if (item is AddPlanFooter) {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.ADD_TO_YOUR_PLAN_TAPPED)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
            view.findNavController()
                .navigate(HomeScreenFragmentDirections.globalActionToBuildPlan())
        } else if (item is PlanChatFooter) {
            state.openMessaging.call()
        }
    }

    override fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int) {
        if (item is PlanItem) {
            if (item.isGrouped) {
                updateWeeklyItemToggle(position, parentPosition)
            } else {
                updateDailyItemToggle(item, position, parentPosition)
            }
        }
    }

    private fun updateDailyItemToggle(
        item: PlanItem,
        position: Int,
        parentPosition: Int
    ) {
        val date = DAY_DATE_FORMAT.format(state.todayTodosData[parentPosition].date)
        val planRecords = item.currentPlan.plan_records.orEmpty().toMutableList()
        planRecords.add(0, createPlanRecord(date, item.isCompleted.not()))
        val updatedItem = item.currentPlan.copy(plan_records = planRecords)
        updatePlanItem(
            mapToDoToPlanItem(
                updatedItem,
                state.isDailyView,
                item.groupCount,
                item.todos,
                item.todos,
                date
            ),
            position,
            parentPosition
        )

        viewModelScope.launch {
            planManager.updatePlanToggle(item.id, date, item.isCompleted.not())
                .onSuccess {
                    planManager.updatePlanCachedData()
                    planManager.todayPlanItemCheckChange()
                }
                .onError {
                    // todo handle error case
//                hideLoading(false)
                }
            markPlanItemInAProgramAsCompleted(updatedItem)
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_CHECKED)
                .addProperty(TrackingConstants.KEY_ID, item.id.toString())
                .addProperty(TrackingConstants.TYPE, item.type.title)
                .addProperty(TrackingConstants.KEY_LOCATION, "Plan Page")
                .addProperty("program_id", item.currentPlan.program_id.toString())
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun createPlanRecord(date: String, completed: Boolean) = PlanRecord(
        date = date,
        completed = completed
    )

    private fun updateWeeklyItemToggle(
        position: Int,
        parentPosition: Int
    ) {
        val items = state.planItems.value?.first.orEmpty().toMutableList()
        val parentItem = items[parentPosition]
        val planItems = parentItem.items.toMutableList()
        val selectedPlan = planItems[position]
        val today = DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())!!
        selectedPlan.todos.forEachIndexed { index, todoItem ->
            val matchingRecords = todoItem.plan_records.orEmpty()
                .firstOrNull { DAY_DATE_FORMAT.parse(it.date).isSameDay(today) }
            if (matchingRecords == null || matchingRecords.completed != true) {
                updatePlanWeeklyItemToggle(
                    selectedPlan,
                    todoItem,
                    position,
                    parentPosition,
                    index,
                    today.formatDayDate()
                )
                planManager.todayPlanItemCheckChange()
                return
            }
        }
    }

    private fun updatePlanWeeklyItemToggle(
        selectedPlan: PlanItem,
        item: PlanData,
        position: Int,
        parentPosition: Int,
        internalIndex: Int,
        date: String
    ) {

        val items = state.planItems.value?.first.orEmpty().toMutableList()
        val parentItem = items[parentPosition]

        val planRecords = item.plan_records.orEmpty().toMutableList()
        planRecords.add(0, createPlanRecord(date, true))
        val updatedItem = item.copy(plan_records = planRecords)
        val todos = selectedPlan.todos.toMutableList()
        todos[internalIndex] = updatedItem
        val updatedPlan = selectedPlan.copy(
            todos = todos,
            isCompleted = todos.sumBy { it.getNonCompletedDays().size } == 0,
            groupCount = todos.sumBy { it.getNonCompletedDays().size }
        )

        val weeklyItems = parentItem.items.toMutableList()
        weeklyItems[position] = updatedPlan

        val updatedWeekItem = parentItem.copy(
            currentStep = getWeeklyCompletedItems(weeklyItems),
            stepsCount = getWeeklyItemsCount(weeklyItems),
            title = parentItem.title,
            items = weeklyItems
        )

        items[parentPosition] = updatedWeekItem

        loadPlan(items, false)

        viewModelScope.launch {
            planManager.updatePlanToggle(item.id ?: return@launch, date, true)
                .onSuccess {
                    planManager.updatePlanCachedData()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_CHECKED)
                            .addProperty(TrackingConstants.KEY_ID, item.id.toString())
                            .addProperty(TrackingConstants.TYPE, item.getPlanType().title)
                            .addProperty(TrackingConstants.KEY_LOCATION, "Plan Page")
                            .addProperty("program_id", item.program_id.toString())
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
//                hideLoading(false)
                }
                .onError {
//                hideLoading(false)
                    // todo handle error case
                }
            markPlanItemInAProgramAsCompleted(updatedItem)
        }
    }

    private fun getWeeklyItemsCount(weeklyItems: MutableList<PlanItem>): Int {
        var items = 0
        weeklyItems.forEach {
            items += it.todos.sumBy {
                it.getDaysOfWeek().size
            }
        }
        return items
    }

    private fun getWeeklyCompletedItems(weeklyItems: MutableList<PlanItem>): Int {
        var completedItems = 0

        weeklyItems.forEach {

            it.todos.forEach { plan ->
                val daysOfWeek = plan.getDaysOfWeek()
                daysOfWeek.forEach {
                    val day = DAY_DATE_FORMAT.format(it.toInt().toCurrentWeekDate())
                    val completedRecord =
                        plan.plan_records.orEmpty().firstOrNull { it.date == day }?.completed
                            ?: false
                    if (completedRecord) {
                        completedItems += 1
                    }
                }
            }
        }
        return completedItems
    }

    private fun updatePlanItem(planItem: PlanItem, position: Int, parentPosition: Int) {
        val items = if (state.isDailyView) {
            state.todayTodosData
        } else {
            state.weeklyTodosData
        }.toMutableList()

        val parentItem = items[parentPosition]
        val planItems = parentItem.items.toMutableList()
        planItems[position] = planItem
        val updatedParentItem = parentItem.copy(
            currentStep = planItems.count { it.isCompleted },
            stepsCount = planItems.size,
            title = parentItem.title,
            items = planItems
        )
        items[parentPosition] = updatedParentItem
        if (planItems.isEmpty()) {
            items.removeAt(parentPosition)
            state.deletePlanItem.value = parentPosition
        }

        if (state.isDailyView) {
            state.todayTodosData = items
        } else {
            state.weeklyTodosData = items
        }
//        loadPlan(items, false)
    }

    override fun onAddReminderClicked(item: Any, view: View, parentPosition: Int) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_PLAN_REMINDER_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        if (item is PlanItem) {
            openReminderDialog(view, item)
        }
    }

    override fun onReminderNotificationClicked(
        item: Any,
        view: View,
        position: Int,
        parentPosition: Int
    ) {
        if (item is PlanItem) {
            showLoading()
            viewModelScope.launch {
                val updatedPlan = item.currentPlan.copy(
                    notification_enabled = item.currentPlan.notification_enabled?.not() ?: false
                )
                planManager.updatePlanItem(
                    updatedPlan.id ?: return@launch,
                    updatedPlan.toPlanRequest()
                )
                updatePlanItem(item.copy(currentPlan = updatedPlan), position, parentPosition)
                loadUserPlans(true)
            }
        }
    }

    override fun onItemDeleteClicked(item: Any, view: View, position: Int, parentPosition: Int) {
        if (item is PlanItem) {
            if (item.isGrouped && !item.plans.isNullOrEmpty()) {
                state.removeReminder.value = Triple(item, parentPosition, position)
            } else {
                deletePlanItems(item, parentPosition, position)
            }
        }
    }

    fun deletePlanItems(item: Any, parentPosition: Int, position: Int) {
        showLoading()
        viewModelScope.launch {
            if (item is PlanItem) {
                val result = if (item.isGrouped) {
                    planManager.deletePlanItem(item.todos.map { it.id ?: -1 })
                } else {
                    planManager.deletePlanItem(listOf(item.currentPlan.id ?: -1))
                }
                result
                    .onSuccess {
                        planManager.todayPlanItemCheckChange()
                        val items = state.planItems.value?.first.orEmpty().toMutableList()
                        val parentItem = items[parentPosition]
                        val todos = parentItem.items.toMutableList()
                        todos.removeAt(position)

                        val updatedItem = if (item.isGrouped) {
                            parentItem.copy(
                                currentStep = getWeeklyCompletedItems(todos),
                                stepsCount = getWeeklyItemsCount(todos),
                                items = todos
                            )
                        } else {
                            parentItem.copy(
                                currentStep = parentItem.currentStep - 1,
                                stepsCount = parentItem.stepsCount - 1,
                                items = todos
                            )
                        }

                        items[parentPosition] = updatedItem
                        if (updatedItem.items.isEmpty()) {
                            items.removeAt(parentPosition)
                            state.deletePlanItem.value = parentPosition
                        }
                        loadPlan(items)
                        loadUserPlans(true)
                        trackingManager.log(
                            TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                                .addProperty(
                                    TrackingConstants.KEY_ID,
                                    item.todos.map { it.id ?: -1 }.toString()
                                )
                                .addProperty(
                                    TrackingConstants.TYPE,
                                    item.currentPlan.getPlanType().title
                                )
                                .addProperty(TrackingConstants.KEY_LOCATION, "Plan Page")
                                .withKlaviyo()
                                .withFirebase()
                                .withAmplitude()
                                .create()
                        )
                    }
                    .onError {
                        hideLoading(false)
                        showError(getResString(R.string.unknown_error))
                    }
                markPlanItemInAProgramAsCompleted(item.currentPlan)
            }
        }
    }

    private suspend fun markPlanItemInAProgramAsCompleted(updatedItem: PlanData) {
        programManager.markPlanRecordAsCompleted(updatedItem)
            .onSuccess {
                loadPrograms()
            }
            .onServiceError {
                showError(it.error.message ?: getResString(R.string.unknown_error))
            }
            .onNetworkIOError {
                showError(getResString(R.string.network_error))
            }
            .onUnknownError {
                showError(getResString(R.string.unknown_error))
            }
    }

    override fun onItemEditClicked(item: Any, view: View, parentPosition: Int) {
        if (item is PlanItem) {
            openReminderDialog(view, item)
        }
    }

    override fun onGroupedItemsClicked(item: Any, view: View, parentPosition: Int) {
    }

    private fun openReminderDialog(
        view: View,
        item: PlanItem
    ) {
        view.findNavController().navigate(
            HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToPlanReminderDialog(
                PlanReminderHeader(
                    title = item.title,
                    description = item.valueProp1.orEmpty(),
                    backgroundUrl = item.backgroundUrl,
                    background = item.background,
                    todos = item.plans,
                    isTestingPlan = item.type == PlanType.TestPlan
                )
            )
        )
    }

    private fun mapToDoToPlanItem(
        todo: PlanData,
        isDailyView: Boolean,
        groupedItemsCount: Int,
        todos: List<PlanData>,
        plans: List<PlanData>,
        date: String? = null
    ): PlanItem {
        val isCompleted = todo.plan_records?.firstOrNull { it.date == date }?.completed == true

        return PlanItem(
            todo.id ?: -1,
            todo.getDisplayName(),
            todo.getDescription(),
            getRemindersTitle(todo, isDailyView),
            todo.getToDoImageUrl(),
            todo.getToDoImageBackground(),
            todo.getDisplayName(),
            if (isDailyView) {
                isCompleted
            } else {
                groupedItemsCount == 0
            },
            !isDailyView,
            groupedItemsCount,
            todos,
            plans,
            todo.getPlanType(),
            todo.day_of_week,
            todo.program_dates?.map { DAY_DATE_FORMAT.parse(it) },
            program = todo.program?.let { programMapper.mapProgram(it) },
            todo
        )
    }

    override fun onClosePlanClicked(view: View) {
        preferencesRepository.emptyPlanRecommendationItemDismissed = true
    }

    fun onDailyFilterSelected(updateSelectedTab: Boolean) {
        if (updateSelectedTab) {
            state.isDailyView = true

            preferencesRepository.isDailyPlanView = true
            loadPlan(state.todayTodosData, true)
        } else if (preferencesRepository.isDailyPlanView) {
            loadPlan(state.todayTodosData, true)
        }
    }

    fun onWeeklyFilterSelected(updateSelectedTab: Boolean) {
        if (updateSelectedTab) {
            state.isDailyView = false

            preferencesRepository.isDailyPlanView = false
            loadPlan(state.weeklyTodosData, true)
        } else if (!preferencesRepository.isDailyPlanView) {
            loadPlan(state.weeklyTodosData, true)
        }
    }

    fun isDailyPlanView() = preferencesRepository.isDailyPlanView

    private fun getRemindersTitle(todo: PlanData?, isDailyView: Boolean): String {
        return if (isDailyView) {
            todo?.getTimeFormattedWithMeridian() ?: getResString(R.string.set_time)
        } else {
            if (todo?.day_of_week.isNullOrEmpty()) getResString(R.string.set_time) else getResString(
                R.string.reminders
            )
        }
    }

    override fun onAddPlanFooterClicked(item: Any, view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.ADD_TO_YOUR_PLAN_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        view.findNavController()
            .navigate(HomeScreenFragmentDirections.globalActionToBuildPlan())
    }

    override fun onAnimationFinished(item: Any, view: View, parentPosition: Int) {
        val items = if (state.isDailyView) {
            state.todayTodosData
        } else {
            state.weeklyTodosData
        }
        loadPlan(items, true)
    }

    override fun onProgramItemClicked(item: Program) {
    }

    override fun onProgramInfoClicked(item: Program) {
        state.navigateTo.value =
            HomeScreenFragmentDirections.actionHomeScreenFragmentToProgramInfoDialogFragment(
                item
            )
    }
}
