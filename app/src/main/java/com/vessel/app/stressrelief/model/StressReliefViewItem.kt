package com.vessel.app.stressrelief.model

import android.os.Parcelable
import androidx.annotation.*
import com.vessel.app.common.net.data.plan.PlanData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StressReliefViewItem(
    val title: String,
    val description: String,
    val planData: PlanData,
    val isGrouped: Boolean,
    val groupCount: Int,
    val items: List<PlanData>,
    val image: String,
    val reminderTime: String,
) : Parcelable
