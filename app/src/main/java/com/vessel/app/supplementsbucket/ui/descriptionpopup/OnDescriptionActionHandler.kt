package com.vessel.app.supplementsbucket.ui.descriptionpopup

import android.view.View

interface OnDescriptionActionHandler {
    fun onCloseDialogClicked(view: View)
}
