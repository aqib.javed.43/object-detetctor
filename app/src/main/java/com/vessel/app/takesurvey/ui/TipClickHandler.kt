package com.vessel.app.takesurvey.ui

interface TipClickHandler {
    fun onGotItClicked()
}
