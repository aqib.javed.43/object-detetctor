package com.vessel.app.base

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vessel.app.R
import com.vessel.app.model.AlertData
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.delegators.LiveEventProvider
import com.vessel.app.util.delegators.MutableLiveDataProvider

abstract class BaseViewModel(private val resourceRepository: ResourceRepository) : ViewModel() {

    private val _isLoading by MutableLiveDataProvider<Boolean>()
    val isLoading = _isLoading as LiveData<Boolean>

    private val _isProgressLoading by MutableLiveDataProvider<Boolean>()
    val isProgressLoading = _isProgressLoading as LiveData<Boolean>

    private val _showAlert by LiveEventProvider<AlertData>()
    val showAlert = _showAlert as LiveData<AlertData>

    val navigateBack = LiveEvent<Unit>()

    fun getResString(id: Int) = resourceRepository.getString(id)
    fun getFont(id: Int) = resourceRepository.getFont(id)
    fun <T> getResString(@StringRes resId: Int, vararg formatArgs: T) =
        resourceRepository.getString(resId, *formatArgs)
    fun getResColor(id: Int) = resourceRepository.getColor(id)
    fun getAssets() = resourceRepository.getAssets()
    fun getDrawable(@DrawableRes id: Int) = resourceRepository.getDrawable(id)

    override fun onCleared() {
        super.onCleared()

        _isLoading.postValue(false)
        _isProgressLoading.postValue(false)
    }

    protected fun showLoading(isCalledFromMainThread: Boolean = true) {
        safeCallIsLoading(_isLoading, true, isCalledFromMainThread)
    }

    protected fun hideLoading(isCalledFromMainThread: Boolean = true) {
        safeCallIsLoading(_isLoading, false, isCalledFromMainThread)
    }

    protected fun showProgressLoading(isCalledFromMainThread: Boolean = true) {
        safeCallIsLoading(_isProgressLoading, true, isCalledFromMainThread)
    }

    protected fun hideProgressLoading(isCalledFromMainThread: Boolean = true) {
        safeCallIsLoading(_isProgressLoading, false, isCalledFromMainThread)
    }

    private fun safeCallIsLoading(data: MutableLiveData<Boolean>, value: Boolean, isCalledFromMainThread: Boolean) {
        if (isCalledFromMainThread) {
            data.value = value
        } else {
            data.postValue(value)
        }
    }

    protected fun showAlert(title: String, message: String) {
        _showAlert.postValue(AlertData(title, message))
    }

    protected fun showError(message: String) {
        _showAlert.postValue(AlertData(getResString(R.string.error), message))
    }

    protected fun showError(exception: Exception?, defaultMessage: String = "") {
        if (exception == null) {
            showError(defaultMessage)
        } else {
            showError(exception.message.toString())
        }
    }

    fun navigateBack() {
        navigateBack.call()
    }
}
