package com.vessel.app.foodrecommendation.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.home.HomeActivity
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FoodRecommendationFragment : BaseFragment<FoodRecommendationViewModel>() {
    override val viewModel: FoodRecommendationViewModel by viewModels()
    override val layoutResId = R.layout.fragment_food_recommendation

    val foodSelectAdapter by lazy { FoodRecommendationSelectAdapter(viewModel) }
    private lateinit var backButton: View
    override fun onViewLoad(savedInstanceState: Bundle?) {
        backButton = requireView().findViewById(R.id.toolbar)
        val foodList = requireView().findViewById<RecyclerView>(R.id.foodList)
        foodList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        foodList.adapter = foodSelectAdapter
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(navigateToHome) {
                navigateToHomeActivity()
            }
            observe(items) {
                foodSelectAdapter.submitList(it)
            }
            observe(refreshItems) {
                foodSelectAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun navigateToHomeActivity() {
        val intent = Intent(requireContext(), HomeActivity::class.java)
        context?.startActivity(intent)
        activity?.finishAffinity()
    }
}
