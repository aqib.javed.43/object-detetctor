package com.vessel.app.common.binding

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import androidx.databinding.BindingAdapter

const val animationDuration = 4000L
const val animationTranslationY = 150f
const val animationAlpha = 1f

@BindingAdapter(value = ["animatedTestScore", "animatorListener"], requireAll = false)
fun View.setAnimatedTestScore(animation: Pair<Float, Boolean>?, animatorListener: Animator.AnimatorListener ?) {
    val fadeObjectAnimator = ObjectAnimator.ofFloat(this, "alpha", animationAlpha, animation?.first ?: animationAlpha).apply {
        duration = animationDuration
    }

    val moveUpObjectAnimator = ObjectAnimator.ofFloat(this, "translationY", -animationTranslationY).apply {
        duration = animationDuration
    }

    val anim = AnimatorSet().apply {
        if (animation?.second == true) {
            play(fadeObjectAnimator).with(moveUpObjectAnimator)
        } else {
            play(fadeObjectAnimator)
        }
    }

    anim.start()

    animatorListener?.let {
        anim.addListener(it)
    }
}
