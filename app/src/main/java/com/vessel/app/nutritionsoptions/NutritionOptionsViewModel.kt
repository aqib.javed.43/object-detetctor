package com.vessel.app.nutritionsoptions

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import kotlinx.coroutines.launch

class NutritionOptionsViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    val supplementsManager: SupplementsManager,
    val scoreManager: ScoreManager,
    val chatManager: ChatManager,
    val membershipManager: MembershipManager,
    val preferencesRepository: PreferencesRepository,
    val remoteConfiguration: RemoteConfiguration,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    val dismissScreen = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()
    val openMessaging = LiveEvent<Any>()

    init {
        checkMembershipStatus()
        remoteConfiguration.fetchConfigFromRemote()
    }

    private fun checkMembershipStatus() {
        viewModelScope.launch {
            membershipManager.checkMembershipStatus()
        }
    }

    fun onCloseClicked() {
        dismissScreen.call()
    }

    fun checkVesselFuelMemberships(nutritionistCommunications: NutritionistCommunications) {
        if (!preferencesRepository.isFirstTimeClickChat) {
            if (!membershipManager.hasMembership()) {
                navigateTo.value =
                    HomeNavGraphDirections.globalActionToManageMembershipsDialogFragment()
            } else {
                navigateTo.value =
                    HomeNavGraphDirections.globalActionToNotMemberDialogFragment()
            }
            preferencesRepository.isFirstTimeClickChat = true
        } else {
            if (nutritionistCommunications == NutritionistCommunications.CHAT_NUTRITIONIST) {
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.TALK_TO_NUTRITIONIST_TAPPED)
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
                openMessaging.call()
            } else {
                navigateTo.value = HomeNavGraphDirections.globalActionToNutritionCoachFragment()
            }
        }
        hideLoading(false)
    }

    enum class NutritionistCommunications {
        TALK_TO_NUTRITIONIST, CHAT_NUTRITIONIST
    }

    fun getChatConfiguration() =
        membershipManager.getChatConfiguration(MembershipManager.ZENDESK_NUTRITIONIST_DEPARTMENT)

    fun isLiveChat() = remoteConfiguration.getBoolean("use_livechat")
    fun getUserName() = preferencesRepository.userFirstName
    fun getUserEmail() = preferencesRepository.userEmail
}
