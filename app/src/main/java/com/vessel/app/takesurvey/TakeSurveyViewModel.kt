package com.vessel.app.takesurvey

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.activationtimer.ActivationTimerState
import com.vessel.app.activationtimer.util.TestSurveyStates
import com.vessel.app.activationtimer.util.TimerService
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.net.HttpStatusCode.SUCCESS
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.SurveyRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.lfaerrors.model.LFAErrorModel
import com.vessel.app.takesurvey.model.SurveyAnswer
import com.vessel.app.takesurvey.model.SurveyResponseAnswer
import com.vessel.app.takesurvey.ui.QuestionRecyclerViewAdapter
import com.vessel.app.takesurvey.ui.TakeSurveyFragmentDirections
import com.vessel.app.takesurvey.ui.TipClickHandler
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.*
import java.util.*

class TakeSurveyViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    val state: TakeSurveyState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val surveyRepository: SurveyRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider),
    TimerService.TimerHandler,
    ToolbarHandler,
    TipClickHandler,
    QuestionRecyclerViewAdapter.QuestionClickHandler {

    private val surveyId = savedStateHandle.get<String>("surveyId")!!
    val testErrors: List<LFAErrorModel> =
        savedStateHandle.get<Array<LFAErrorModel>>("errors")?.toList().orEmpty()
    lateinit var lastQuestionAnswer: SurveyAnswer

    init {
        state.questions = savedStateHandle.get<Array<SurveyQuestion>>("questions")!!
        logSurveyEvents(TrackingConstants.SURVEY_STARTED)
    }

    private val userAnswersList: MutableList<SurveyResponseAnswer> = mutableListOf()

    override fun onUpdateText(minutes: String, seconds: String) {
        state.updateTimerText(minutes, seconds)
    }

    override fun onUpdateProgress(percentComplete: Double) {
        state {
            if (timerStart == null) {
                timerStart = Constants.CAPTURE_DATE_FORMAT.format(Date())
            }
            progress.value = (percentComplete * ActivationTimerState.PROGRESS_MAX).toInt()
        }
    }

    override fun onFinish() {
        state {
            updateTimerText(ActivationTimerState.FINISH_VALUE, ActivationTimerState.FINISH_VALUE)
            progress.value = ActivationTimerState.PROGRESS_MAX
            viewModelScope.launch {
                delay(500)
                setCompleted()
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        submitAnswers(TestSurveyStates.SKIPPED)
    }

    override fun onGotItClicked() {
        if (lastQuestionAnswer.chainedSurveyId == null) {
            state.showTip.value = false
            state.showGoodJob.value = false
            navigateForward()
        } else {
            submitAnswers(TestSurveyStates.CHAINED, lastQuestionAnswer.chainedSurveyId!!.toLong())
        }
    }

    override fun onSkipClicked(question: SurveyQuestion) {
        navigateForward()
    }

    override fun onAnswerClicked(answer: SurveyAnswer) {
        lastQuestionAnswer = answer
        val previousResponse =
            userAnswersList.firstOrNull { it.questionId == lastQuestionAnswer.questionId }
        if (previousResponse != null) {
            previousResponse.answerId = lastQuestionAnswer.id
        } else {
            userAnswersList.add(
                SurveyResponseAnswer(
                    lastQuestionAnswer.questionId,
                    lastQuestionAnswer.id
                )
            )
        }
        state.currentQuestion.value = state.questions[state.currentQuestionIndex()]
        if (lastQuestionAnswer.incorrect.not()) {
            if (state.currentQuestion.value?.successHeading.isNullOrEmpty()
                .not() && state.currentQuestion.value?.successText.isNullOrEmpty().not()
            ) {
                state.showGoodJob.value = true
            } else {
                if (lastQuestionAnswer.chainedSurveyId == null) {
                    navigateForward()
                } else {
                    submitAnswers(
                        TestSurveyStates.CHAINED,
                        lastQuestionAnswer.chainedSurveyId!!.toLong()
                    )
                }
            }
        } else {
            state.showTip.value = true
        }
    }

    override fun answerChangedListenerForLayout4(item: SurveyQuestion, answer: String) {
        val previousResponses = userAnswersList.firstOrNull { it.questionId == item.id }
        if (previousResponses != null) {
            previousResponses.answerText = answer
        } else {
            userAnswersList.add(SurveyResponseAnswer(item.id, answerText = answer))
        }
    }

    private fun navigateForward() {
        if (state.currentQuestionIndex() == state.questions.size - 1) {
            submitAnswers(TestSurveyStates.COMPLETED)
        } else {
            state.navigateToIndex.value = state.nextQuestionIndex()
        }
    }

    fun submitAnswers(surveyStates: TestSurveyStates, surveyId: Long? = null) {
        showLoading()
        viewModelScope.launch {
            state.uuid?.let { uuid ->
                surveyRepository.submitSurveyResponse(uuid, userAnswersList)
                    .onSuccess {
                        if (it.code() == SUCCESS) {
                            preferencesRepository.setSurveyStates(surveyStates)
                            when (surveyStates) {
                                TestSurveyStates.COMPLETED -> {
                                    state.finishSurvey.call()
                                }
                                TestSurveyStates.SKIPPED -> {
                                    state.skipSurvey.call()
                                }
                                TestSurveyStates.CHAINED -> {
                                    getChainedSurvey(surveyId!!)
                                }
                                else -> {
                                    // TODO?
                                }
                            }
                        } else {
                            showError(getResString(R.string.unknown_error))
                        }
                        hideLoading(false)
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onHttpOrUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }
                    .onError {
                        hideLoading()
                    }
            }
        }
        logSurveyEvents(if (surveyStates == TestSurveyStates.COMPLETED || surveyStates == TestSurveyStates.CHAINED) TrackingConstants.SURVEY_SUBMITTED else TrackingConstants.SURVEY_SKIP)
    }

    private fun getChainedSurvey(surveyId: Long) {
        showLoading()
        viewModelScope.launch {
            surveyRepository.getSurveyById(surveyId)
                .onSuccess {
                    processSurveyList(it)
                    hideLoading(true)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }
    private fun processSurveyList(items: List<SurveyQuestion>) {
        showLoading(false)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val multipleIds = items.map {
                    it.id
                }
                val content = arrayListOf<SurveyQuestion>()
                coroutineScope {
                    multipleIds.forEach { id ->
                        launch {
                            surveyRepository.getAnswerById(id.toLong()).onSuccess { answerResponse ->
                                items.firstOrNull { it.id == id }?.let {
                                    content.add(it.copy(answers = answerResponse))
                                }
                            }
                        }
                    }
                }
                state.navigateTo.value =
                    TakeSurveyFragmentDirections.actionTakeSurveyToTakeSurvey(
                        content.toTypedArray(),
                        surveyId.toString(),
                        testErrors.toTypedArray()
                    )
                hideLoading(false)
            }
        }
    }

    private fun logSurveyEvents(eventName: String) {
        state.uuid?.let { uuid ->
            trackingManager.log(
                TrackedEvent.Builder(eventName)
                    .addProperty(TrackingConstants.KEY_SURVEY_ID, surveyId)
                    .addProperty(TrackingConstants.KEY_UUID, uuid)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }
}
