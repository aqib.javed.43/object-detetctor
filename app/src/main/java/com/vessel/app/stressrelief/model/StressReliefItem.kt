package com.vessel.app.stressrelief.model

enum class StressReliefItem(val apiValue: String) {
    MEDITATE("meditate"),
    CARDIO("cardio"),
    YOGA("yoga"),
    BREATH("breath"),
    NIGHT("night"),
    FRIEND("friend")
}
