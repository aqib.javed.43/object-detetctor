// package com.vessel.app.common.net.mapper
//
// import com.vessel.app.BaseTest
// import com.vessel.app.isEqualTo
// import com.vessel.app.recommendation.SampleRecommendationsFaker
// import com.vessel.app.sample.SampleFaker
// import io.mockk.every
// import io.mockk.mockk
// import org.junit.jupiter.api.BeforeEach
// import org.junit.jupiter.api.Test
//
// class RecommendationMapperTest : BaseTest() {
//    private lateinit var recommendationMapper: RecommendationMapper
//
//    private val sampleMapper = mockk<SampleMapper>()
//
//    @BeforeEach
//    fun setUp() {
//        recommendationMapper = RecommendationMapper(sampleMapper)
//    }
//
//    @Test
//    fun `it should map the sample recommendations data to the domain model`() {
//        val data = SampleRecommendationsFaker.data()
//        val supplements = SampleFaker.list()
//        val lifestyle = SampleFaker.list()
//        every { sampleMapper.map(data.supplement) } returns supplements
//        every { sampleMapper.map(data.lifestyle) } returns lifestyle
//
//        val sampleRecommendations = recommendationMapper.map(data)
//
//        sampleRecommendations.supplements isEqualTo supplements
//        sampleRecommendations.lifestyle isEqualTo lifestyle
//    }
// }
