package com.vessel.app.checkin.habitsselect

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.onboarding.badhabitsselection.BadHabitSelect
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.model.GoalButton
import javax.inject.Inject

class CheckInHabitsSelectState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val items = MutableLiveData<List<BadHabitSelect>>()
    val selectedGoal = MutableLiveData<GoalButton>()
    operator fun invoke(block: CheckInHabitsSelectState.() -> Unit) = apply(block)
}
