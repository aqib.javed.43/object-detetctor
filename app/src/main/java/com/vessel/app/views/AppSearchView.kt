package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import kotlinx.android.synthetic.main.app_search_view.view.*

class AppSearchView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.app_search_view, this)
    }

    fun setSearchQuery(text: String?) {
        text?.let {
            searchView.setQuery(text, false)
        }
    }

    fun setOnQueryTextListener(onQueryTextListener: SearchView.OnQueryTextListener?) {
        onQueryTextListener?.let {
            searchView.setOnQueryTextListener(onQueryTextListener)
        }
    }
}
