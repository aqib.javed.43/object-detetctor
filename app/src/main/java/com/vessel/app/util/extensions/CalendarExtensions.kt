package com.vessel.app.util.extensions

import androidx.annotation.IntRange
import java.util.Calendar

fun getDayFullName(@IntRange(from = 1, to = 7) dayOfWeek: Int): String {
    return when {
        Calendar.MONDAY == dayOfWeek -> "Monday"
        Calendar.TUESDAY == dayOfWeek -> "Tuesday"
        Calendar.WEDNESDAY == dayOfWeek -> "Wednesday"
        Calendar.THURSDAY == dayOfWeek -> "Thursday"
        Calendar.FRIDAY == dayOfWeek -> "Friday"
        Calendar.SATURDAY == dayOfWeek -> "Saturday"
        Calendar.SUNDAY == dayOfWeek -> "Sunday"
        else -> ""
    }
}

fun getDayShortName(@IntRange(from = 1, to = 7) dayOfWeek: Int): String {
    return when {
        Calendar.MONDAY == dayOfWeek -> "Mon"
        Calendar.TUESDAY == dayOfWeek -> "Tues"
        Calendar.WEDNESDAY == dayOfWeek -> "Wed"
        Calendar.THURSDAY == dayOfWeek -> "Thu"
        Calendar.FRIDAY == dayOfWeek -> "Fri"
        Calendar.SATURDAY == dayOfWeek -> "Sat"
        Calendar.SUNDAY == dayOfWeek -> "Sun"
        else -> ""
    }
}
