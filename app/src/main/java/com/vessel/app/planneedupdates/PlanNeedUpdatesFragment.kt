package com.vessel.app.planneedupdates

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.wellness.RecommendationType
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PlanNeedUpdatesFragment : BaseFragment<PlanNeedUpdatesViewModel>() {
    override val viewModel: PlanNeedUpdatesViewModel by viewModels()
    override val layoutResId = R.layout.fragment_plan_need_updates

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)
        viewModel.navigateToHome.observe(
            viewLifecycleOwner,
            {
                findNavController().navigate(PostTestNavGraphDirections.actionGoToHome())
            }
        )
        viewModel.navigateToPlanRecommendation.observe(
            viewLifecycleOwner,
            {
                findNavController().navigate(
                    PlanNeedUpdatesFragmentDirections.actionPlanNeedUpdatesFragmentToCreatePlanRecommendationFragment(
                        RecommendationType.Tip
                    )
                )
            }
        )
    }
}
