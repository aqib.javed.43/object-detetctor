package com.vessel.app.common.manager

import com.vessel.app.common.repo.RemindersRepository
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReminderManager @Inject constructor(
    private val remindersRepository: RemindersRepository
) {
    private var isReminderCleared = false
    private val remindersStateChannel = ConflatedBroadcastChannel(false)
    private val firstReminderStateChannel = BroadcastChannel<Boolean>(1)
    private val updateReminderState = BroadcastChannel<Boolean>(1)

    fun updateRemindersCachedData() {
        updateRemindersState()
    }

    private fun updateRemindersState() {
        remindersStateChannel.offer(true)
    }

    fun clearCachedData() {
        remindersRepository.clearCachedData()
    }

    fun getRemindersState() = remindersStateChannel.asFlow()

    fun updateFirstReminderState() {
        isReminderCleared = false
        firstReminderStateChannel.offer(true)
    }

    fun clearFirstReminderState() {
        isReminderCleared = true
        firstReminderStateChannel.offer(false)
    }

    fun getIsReminderClear() = isReminderCleared

    fun getFirstRemindersState() = firstReminderStateChannel.asFlow()

    fun updateUpdateReminderState() {
        updateReminderState.offer(true)
    }

    fun clearUpdateReminderState() {
        updateReminderState.offer(false)
    }

    fun getUpdateRemindersState() = updateReminderState.asFlow()
}
