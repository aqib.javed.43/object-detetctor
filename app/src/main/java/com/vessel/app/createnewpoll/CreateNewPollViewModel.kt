package com.vessel.app.createnewpoll

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.PollQuestions

class CreateNewPollViewModel@ViewModelInject constructor(
    val state: CreateNewPollState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler {
    override fun onBackButtonClicked(view: View) {
        view.findNavController().navigateUp()
    }
    fun gotoPollPreview(item: PollQuestions) {
        val nav = CreateNewPollFragmentDirections.actionCreateNewPollFragmentToPollPreviewFragment(item)
        state.navigateTo.value = nav
    }
    // Create Poll
    fun createPoll(question: String, items: List<String>) {
        val pollQuestions = PollQuestions()
        pollQuestions.contactID = preferencesRepository.contactId.toString()
        pollQuestions.question = question
        if (items.isNotEmpty()) {
            pollQuestions.response1 = items[0]
        }
        if (items.size > 1) {
            pollQuestions.response2 = items[1]
        }
        if (items.size > 2) {
            pollQuestions.response3 = items[2]
        }
        if (items.size > 3) {
            pollQuestions.response4 = items[3]
        }
        if (isValidatePoll(pollQuestions, items)) {
            gotoPollPreview(pollQuestions)
        }
    }

    private fun isValidatePoll(pollQuestions: PollQuestions, items: List<String>): Boolean {
        if (pollQuestions.question.isNullOrEmpty()) {
            showError("Poll question missing")
            return false
        }
        if (items.size < 2) {
            showError("At least 2 options required")
            return false
        }
        return true
    }
}
