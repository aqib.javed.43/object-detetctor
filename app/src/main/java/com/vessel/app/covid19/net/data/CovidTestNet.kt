package com.vessel.app.covid19.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CovidTestNet(
    val uuid: String,
    val status: Int,
    val insert_date: String
)
