Calcium is the most abundant mineral in our bodies. About 99% is stored in our bones and teeth, and the other 1% is in our blood and soft tissue. Calcium levels in the blood must be tightly regulated to maintain normal functioning in our bodies. Calcium plays a key role in bone health and strength, hormone secretion, constriction and relaxation of blood vessels, nerve impulse transmission, muscular contraction, and enzyme function.

Calcium absorption is dependent on adequate levels of Vitamin D. Absorption is also enhanced when calcium is ingested with food.  If we aren’t getting enough calcium in our diet, the body will reduce urinary excretion of calcium and start to dissolve our bones and teeth to release calcium stores.  If we’re getting enough or too much calcium in our diets, our body will increase urinary excretion of calcium, and reduce bone resorption. Also, dietary salt, protein, and caffeine all lead to increased calcium loss in the urine.

The majority of people (especially women) are not getting enough calcium in their diets. Optimizing calcium levels can help to prevent osteoporosis, lower blood pressure, and may confer some protection against colon cancer.


**references:**

-   Weaver CM. Calcium. In: Erdman JJ, Macdonald I, Zeisel S, eds. Present Knowledge in Nutrition. 10th ed: John Wiley & Sons, Inc.; 2012:434-446.
-   https://www.ncbi.nlm.nih.gov/pubmed/25062462
-   Nutrition in Clinical Practice: A Comprehensive, Evidence-Based Manual for the Practitioner (Nutrition in Clinical Practice), 2nd Edition by David Katz. 

