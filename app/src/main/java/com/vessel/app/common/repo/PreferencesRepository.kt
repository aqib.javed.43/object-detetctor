package com.vessel.app.common.repo

import android.content.Context
import com.bugsee.library.Bugsee
import com.chibatching.kotpref.KotprefModel
import com.chibatching.kotpref.enumpref.nullableEnumValuePref
import com.squareup.moshi.Moshi
import com.vessel.app.Constants
import com.vessel.app.activationtimer.util.TestSurveyStates
import com.vessel.app.common.model.Contact
import com.vessel.app.common.model.Diet
import com.vessel.app.common.net.data.GoalDataJsonAdapter
import com.vessel.app.common.net.data.UnlockedProgramData
import com.vessel.app.common.net.data.UnlockedProgramDataJsonAdapter
import com.vessel.app.common.net.mapper.GoalsMapper
import com.vessel.app.common.repo.ContactRepository.Companion.SERVER_LAST_LOGIN_FORMAT
import com.vessel.app.common.util.EncryptedPreferencesProvider
import com.vessel.app.onboarding.goals.model.GoalSample
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesRepository @Inject constructor(
    @ApplicationContext context: Context,
    preferencesProvider: EncryptedPreferencesProvider,
    val moshi: Moshi,
    val goalsMapper: GoalsMapper
) : KotprefModel(context, preferencesProvider) {
    var isDailyPlanView by booleanPref(true)
    var showTestingTimerInThePostFlow by booleanPref(true)
    var skipPreTestTips by booleanPref(false)
    var showTipVideoController by booleanPref(false)
    var checkCompletedProgram by booleanPref(false)
    var isAddedHydrationRecommendation by booleanPref(false)
    var isAddedSupplementRecommendation by booleanPref(false)
    var showChatNotification by booleanPref(true)
    var showKetonesPopup by booleanPref(true)
    var showNewMagnesiumPopup by booleanPref(true)
    var isFirstTimeClickChat by booleanPref(false)
    var enableActivationTimerNotification by booleanPref(true)
    var isFirstTimeClickTeam by booleanPref(true)
    private var surveyStates by intPref(TestSurveyStates.PRETAKE.ordinal)
    fun setSurveyStates(value: TestSurveyStates) {
        surveyStates = value.ordinal
    }

    fun getSurveyStates(): TestSurveyStates = TestSurveyStates.values()[surveyStates]

    // Auth
    var accessToken by nullableStringPref()
    var refreshToken by nullableStringPref()
    var inTestingMode by booleanPref()
    var testingProgramStartDate by nullableStringPref()
    var fcmToken by nullableStringPref()

    // AppsFlyer
    var appsFlyerAttributeDeepLink by nullableStringPref()
    var pushWooshAttributeDeepLink by nullableStringPref()
    var appsFlyerAttributeUrl by nullableStringPref()
    var appsFlyerAttributeLinkId by nullableStringPref()

    // Flags
    var onboardingCompleted by booleanPref()
    var dontShowPlanRemoveConfirmationDialog by booleanPref(false)
    var showAccurateResultDialogV2 by booleanPref(default = true)
    var testCompletedCount by intPref()
    var isExistingUserFirstTime by booleanPref(default = true)
    var lastCheckIn by longPref(0)

    // Contact
    var contactId by intPref()
    var userEmail by nullableStringPref()
    var userFirstName by nullableStringPref()
    var userTimezone by nullableStringPref()
    var userLastName by nullableStringPref()
    var userLastLogin by nullableStringPref()
    var userInsertDate by nullableStringPref()
    private val userOccupation by nullableStringPref()
    private val userProfileImageUrl by nullableStringPref()
    private val userIsVerified by booleanPref()
    private val userAbout by nullableStringPref()
    private val userLocation by nullableStringPref()
    private val userSurveyStatus by booleanPref()
    private val userTestsTaken by intPref()
    var userGender by nullableEnumValuePref<Contact.Gender>()
    var userHeight by floatPref()
    var userWeight by floatPref()
    var userBirthDate by nullableStringPref()
    var userGenderTemp by nullableEnumValuePref<Contact.Gender>()
    var userHeightTemp by floatPref()
    var userWeightTemp by floatPref()
    var userBirthDateTemp by nullableStringPref()
    val userAllergies by stringSetPref()
    val userDiets by stringSetPref()
    var freeTrialLocked by booleanPref(false)
    val sendDats by stringSetPref()
    val completedEssentials by stringSetPref()
    val completedLessons by stringSetPref()
    val likedPlans by stringSetPref()
    val completedPlans by stringSetPref()
    val disLikedPlans by stringSetPref()
    val unlockedLessons by stringSetPref()
    val unlockedPrograms by stringSetPref()
    val showCompleteDays by stringSetPref()

    private val userChosenGoals by stringSetPref()
    private var userSubGoals by stringPref()
    private var userMainChosenGoalId by intPref()
    var userHasAtLeastEnrolledProgram by booleanPref(false)
    val servedLessons by stringSetPref()
    val servedActivities by stringSetPref()
    val servedEssentials by stringSetPref()

    fun setUserTempData(
        gender: Contact.Gender, height: Contact.Height, weight: Contact.Weight, birthDate: String
    ) {
        userGenderTemp = gender
        userHeightTemp = height.toCentimeters().toFloat()
        userWeightTemp = weight.weight
        userBirthDateTemp = birthDate
    }


    fun setSendDate(date: String) {
        if (!sendDats.any { day -> day == date }) {
            sendDats.add(date)
        }
    }

    fun isTodayDateSent(date: String): Boolean {
        return sendDats.any { day -> day == date }
    }

    fun getUserChosenGoals() =
        goalsMapper.map(userChosenGoals.map { GoalDataJsonAdapter(moshi).fromJson(it)!! })

    fun getUserMainGoal() = getUserChosenGoals().firstOrNull { it.id == userMainChosenGoalId }
    fun getUserMainGoalId(): Int? =
        if (userMainChosenGoalId != 0) // fixing a crash, because zero is not a goal id
            userMainChosenGoalId
        else null

    fun getUserSubGoalId(): String? =
        if (userSubGoals.isNullOrEmpty()) // fixing a crash, because zero is not a goal id
            null
        else userSubGoals

    fun setUserMainGoal(mainGoalId: Int) {
        userMainChosenGoalId = mainGoalId
        setCompletedEssentials(arrayListOf())
    }

    fun setUserSubGoal(subGoals: String) {
        userSubGoals = subGoals
    }

    fun setUserChosenGoals(goals: List<GoalSample>) {
        clearUserChosenGoals()
        goalsMapper.mapToGoalData(goals).forEach {
            userChosenGoals.add(GoalDataJsonAdapter(moshi).toJson(it))
        }
    }

    fun getUserHasEnrolledProgram() = userHasAtLeastEnrolledProgram

    private fun clearUserChosenGoals() = userChosenGoals.clear()

    var goalsOrder by nullableStringPref()

    // empty plan recommendation
    var emptyPlanRecommendationItemDismissed by booleanPref()

    fun getContact() = if (contactId > 0) {
        Contact(
            id = contactId,
            email = userEmail.orEmpty(),
            firstName = userFirstName.orEmpty(),
            lastName = userLastName.orEmpty(),
            lastLogin = userLastLogin?.let { SERVER_LAST_LOGIN_FORMAT.parse(it) }
                ?: Date(Long.MIN_VALUE),
            insertDate = userInsertDate?.let { SERVER_LAST_LOGIN_FORMAT.parse(it) }
                ?: Date(Long.MIN_VALUE),
            gender = userGender,
            height = if (userHeight > 0f) Contact.Height.fromCentimeters(userHeight) else null,
            weight = if (userWeight > 0f) Contact.Weight(userWeight) else null,
            birthDate = userBirthDate?.let { ContactRepository.SERVER_DATE_FORMAT.parse(it) },
            allergies = userAllergies.toList(),
            diets = userDiets.map { Diet.fromApiName(it) },
            goals = getUserChosenGoals(),
            time_zone = userTimezone.orEmpty(),
            occupation = userOccupation.orEmpty(),
            imageUrl = userProfileImageUrl.orEmpty(),
            isVerified = userIsVerified,
            about = userAbout.orEmpty(),
            location = userLocation.orEmpty(),
            surveyStatus = userSurveyStatus,
            testsTaken = userTestsTaken,
            programs = null,
            mainGoalId = userMainChosenGoalId
        )
    } else null

    fun setContact(contact: Contact) {
        contactId = contact.id ?: 0
        userEmail = contact.email
        userFirstName = contact.firstName
        userLastName = contact.lastName
        userTimezone = contact.time_zone
        userLastLogin = SERVER_LAST_LOGIN_FORMAT.format(contact.lastLogin)
        userInsertDate = SERVER_LAST_LOGIN_FORMAT.format(contact.insertDate)
        userGender = contact.gender
        userHeight = contact.height?.toCentimeters()?.toFloat() ?: 0f
        userWeight = contact.weight?.weight ?: 0f
        userBirthDate = contact.birthDate?.let { ContactRepository.SERVER_DATE_FORMAT.format(it) }
        contact.allergies?.mapNotNull { it }?.toList()?.let {
            userAllergies.addAll(it)
        }
        contact.diets?.map { it.apiName }?.toList()?.let {
            userDiets.addAll(it)
        }
        contact.goals?.let {
            setUserChosenGoals(it)
        }
        if (contact.mainGoalId != null) userMainChosenGoalId = contact.mainGoalId
    }

    fun setTestingMode(enableTest: Boolean) {
        inTestingMode = enableTest
        testingProgramStartDate = if (enableTest) {
            Constants.CAPTURE_DATE_FORMAT.format(Calendar.getInstance(Locale.ENGLISH).time)
                .toString()
        } else {
            null
        }
    }

    fun isTestingPeriodExpired(): Boolean? {
        return testingProgramStartDate?.let {
            val oldDate = Constants.CAPTURE_DATE_FORMAT.parse(it)!!
            val formattedToday =
                Constants.CAPTURE_DATE_FORMAT.format(Calendar.getInstance(Locale.ENGLISH).time)
                    .toString()
            val today = Constants.CAPTURE_DATE_FORMAT.parse(formattedToday)!!
            val diff = (today.time - oldDate.time).div(TimeUnit.DAYS.toMillis(1))
            diff >= Constants.TESTING_PROGRAM_PERIOD
        }
    }

    fun addBugseeAttributes() {
        userEmail?.let {
            Bugsee.setEmail(it)
            Bugsee.setAttribute(Constants.CONTACT_ID, contactId)
        }
    }

    fun setContactDiets(contactDiets: List<String>) {
        userDiets.clear()
        userDiets.addAll(contactDiets)
    }

    fun setCompletedEssentials(ids: List<String>) {
        completedEssentials.clear()
        completedEssentials.addAll(ids)
    }

    fun updateCompletedEssentials(id: String) {
        if (completedEssentials.contains(id)) return
        completedEssentials.add(id)
    }

    fun updateCompletedLessons(id: String) {
        if (completedLessons.contains(id)) return
        completedLessons.add(id)
    }

    fun setContactAllergies(contactAllergies: List<String>) {
        userAllergies.clear()
        userAllergies.addAll(contactAllergies)
    }

    fun setLikedPlans(ids: List<String>) {
        likedPlans.clear()
        likedPlans.addAll(ids)
    }

    fun updateLikedPlans(id: String) {
        disLikedPlans.remove(id)
        if (likedPlans.contains(id)) return
        likedPlans.add(id)
    }

    fun updateUnlikedPlans(id: String) {
        likedPlans.remove(id)
        disLikedPlans.remove(id)
    }

    fun setDislikedPlans(ids: List<String>) {
        disLikedPlans.clear()
        disLikedPlans.addAll(ids)
    }

    fun updateDislikedPlans(id: String) {
        likedPlans.remove(id)
        if (disLikedPlans.contains(id)) return
        disLikedPlans.add(id)
    }

    fun updateUnlockedLesson(programId: String) {
        unlockedLessons.remove(programId)
        if (unlockedLessons.contains(programId)) return
        unlockedLessons.add(programId)
    }

    fun removeUnlockedLesson(programId: String) {
        unlockedLessons.remove(programId)
    }

    fun clearUnlockedLesson() {
        unlockedLessons.clear()
    }

    fun updateShowCompleteForDay(day: String) {
        showCompleteDays.clear()
        showCompleteDays.add(day)
    }

    fun updateCompletedPlans(isCompleted: Boolean?, id: Int?) {
        id?.let {
            if (isCompleted == true) {
                if (completedPlans.contains(it.toString())) return
                completedPlans.add(it.toString())
            } else {
                completedPlans.remove(it.toString())
            }
        }
    }

    fun afterFirstTest() = testCompletedCount >= 1
    fun updateServedLessons(id: String) {
        if (servedLessons.contains(id)) return
        servedLessons.add(id)
    }

    fun updateServedActivities(id: String) {
        if (servedActivities.contains(id)) return
        servedActivities.add(id)
    }

    fun updateServedEssentials(id: String) {
        if (servedEssentials.contains(id)) return
        servedEssentials.add(id)
    }

    fun getUnlockedPrograms() =
        unlockedPrograms.map { UnlockedProgramDataJsonAdapter(moshi).fromJson(it)!! }

    fun updateUnlockedProgram(programId: Int, count: Int) {
        var programs = getUnlockedPrograms() as MutableList
        if (programs.isEmpty() || programs.firstOrNull {
                it.id == programId
            } == null) programs.add(UnlockedProgramData(programId, count))
        else {
            programs = programs.map {
                if (it.id == programId) it.copy(count = count)
                else it.copy()
            } as MutableList<UnlockedProgramData>
        }
        unlockedPrograms.clear()
        programs.forEach {
            unlockedPrograms.add(UnlockedProgramDataJsonAdapter(moshi).toJson(it))
        }
    }

    fun removeUnlockedPrograms(programId: Int) {
        val programs = getUnlockedPrograms() as MutableList
        programs.firstOrNull {
            it.id == programId
        }?.let {
            programs.remove(it)
        }
        programs.forEach {
            unlockedPrograms.add(UnlockedProgramDataJsonAdapter(moshi).toJson(it))
        }
    }

    fun resetUnlockedPrograms() {
        unlockedPrograms.clear()
    }
}
