package com.vessel.app.more

import android.os.Build
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.BuildConfig
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.TrailLockItem
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.homescreen.HomeScreenFragmentDirections
import com.vessel.app.more.model.MoreItems
import com.vessel.app.more.model.MoreModel
import com.vessel.app.more.ui.MoreAdapter
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import kotlinx.coroutines.launch
import java.util.*

class MoreViewModel @ViewModelInject constructor(
    val state: MoreState,
    resourceRepository: ResourceRepository,
    private val authManager: AuthManager,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    val membershipManager: MembershipManager,
    val remoteConfiguration: RemoteConfiguration
) : BaseViewModel(resourceRepository),
    ToolbarHandler,
    MoreAdapter.OnActionHandler {
    init {
        state.version.value = resourceRepository.getString(
            R.string.version_number,
            "${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})"
        )
        state.moreItems.value = MoreItems.values().map { it.moreModel }.toList()
        checkMembershipStatus()
        remoteConfiguration.fetchConfigFromRemote()
    }

    private fun onMyAccountClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.MY_ACCOUNT_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value = HomeNavGraphDirections.globalActionToMyAccount()
    }

    private fun onTakeTestClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_TAPPED)
                .setScreenName(TrackingConstants.FROM_MENU)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        val isSupportedDevice =
            Constants.SUPPORTED_DEVICES_MODELS_PREFIX.any { Build.MODEL.startsWith(it, true) }
        if (isSupportedDevice) {
            onTestClicked()
        } else {
            state.navigateTo.value = HomeNavGraphDirections.globalActionToTestNotAccurate()
        }
    }

    private fun onTestClicked() {
        val now = Date()
        val fourThirtyAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 4)
                set(Calendar.MINUTE, 30)
                set(Calendar.SECOND, 0)
            }.time
        val nineAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 9)
                set(Calendar.MINUTE, 0)
                set(Calendar.SECOND, 0)
            }.time
        if (now.after(fourThirtyAm) && now.before(nineAm)) {
            state.navigateTo.value = if (preferencesRepository.skipPreTestTips) {
                HomeNavGraphDirections.globalActionToTakeTest()
            } else {
                HomeNavGraphDirections.globalActionToPreTest()
            }
        } else {
            state.navigateTo.value = HomeNavGraphDirections.globalActionToTimeOfDay()
        }
    }

    private fun onSupplementClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.BUILD_SUPPLEMENT_PLAN_TAPPED)
                .setScreenName(TrackingConstants.FROM_MENU)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value = HomeNavGraphDirections.globalActionToSupplements()
    }

    private fun onOrderCardsClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.ORDER_CARDS_TAPPED)
                .setScreenName(TrackingConstants.FROM_MENU)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        showLoading()
        viewModelScope.launch {
            authManager.multipass(Constants.PATH_ACCOUNT)
                .onSuccess {
                    hideLoading(false)
                    state.navigateTo.value =
                        HomeNavGraphDirections.globalActionToWeb(BuildConfig.BUY_CARD_URL)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onHttpOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun onBackedByScienceClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.BACKED_BY_SCIENCE_TAPPED)
                .setScreenName(TrackingConstants.FROM_MENU)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value =
            HomeNavGraphDirections.globalActionToWeb(BuildConfig.BACKED_BY_SCIENCE_URL)
    }

    private fun onSupportClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SUPPORT_TAPPED_MENU)
                .setScreenName(TrackingConstants.FROM_MENU)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value = HomeNavGraphDirections.globalActionToSupport()
    }

    override fun onBackButtonClicked(view: View) {
    }

    private fun onChatTonNtritionistClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TALK_TO_NUTRITIONIST_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.openMessaging.call()
    }

    private fun checkMembershipStatus() {
        viewModelScope.launch {
            membershipManager.checkMembershipStatus()
        }
    }

    override fun onMoreItemClicked(moreModel: MoreModel) {
        when (moreModel.itemName) {
            MoreItems.MY_ACCOUNT.moreModel.itemName -> onMyAccountClicked()
            MoreItems.TAKE_A_TEST.moreModel.itemName -> onTakeTestClicked()
            MoreItems.ORDER_CARDS.moreModel.itemName -> onOrderCardsClicked()
            MoreItems.BACKED_BY_SCIENCE.moreModel.itemName -> onBackedByScienceClicked()
            MoreItems.CUSTOM_SUPPLEMENT.moreModel.itemName -> onSupplementClicked()
            MoreItems.SUPPORT.moreModel.itemName -> onSupportClicked()
            MoreItems.CHAT_TO_NUTRITIONIST.moreModel.itemName -> onChatTonNtritionistClicked()
        }
    }
    private fun trailLocked(isTakeTest: Boolean): Boolean {
        if (membershipManager.hasTrailLocked()) {
            val trailLockItem = TrailLockItem()
            trailLockItem.isDisPlayIcon = isTakeTest
            trailLockItem.title = getResString(if (isTakeTest) R.string.take_a_test_locked else R.string.chat_lock_title)
            trailLockItem.message = getResString(R.string.membership_lock_description)
            trailLockItem.onDismiss = {
                state.isTrialLocked.value = false
            }
            trailLockItem.onActiveMember = {
                state.navigateTo.value = HomeScreenFragmentDirections.globalActionToWeb(BuildConfig.BUY_TEST_URL)
            }
            state.trialLockItem.value = trailLockItem
            state.isTrialLocked.value = true
            return true
        }
        return false
    }
    fun getChatConfiguration() =
        membershipManager.getChatConfiguration(MembershipManager.ZENDESK_NUTRITIONIST_DEPARTMENT)

    fun isLiveChat() = remoteConfiguration.getBoolean("use_livechat")
    fun getUserName() = preferencesRepository.userFirstName
    fun getUserEmail() = preferencesRepository.userEmail
}
