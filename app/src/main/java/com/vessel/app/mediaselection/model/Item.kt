package com.vessel.app.mediaselection.model

import com.vessel.app.helper.MediaItem

data class Item(
    val mediaItem: MediaItem,
    var isChecked: Boolean = false
)
