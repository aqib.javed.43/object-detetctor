package com.vessel.app.score.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.score.model.ScoreWellnessItem

class ScoreWellnessAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<ScoreWellnessItem>() {
    override fun getLayout(position: Int) = R.layout.item_score_wellness

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onWellnessScoreToolTipClicked(view: View)
    }
}
