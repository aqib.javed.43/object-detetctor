package com.vessel.app.views

import android.content.Context
import android.text.format.DateUtils
import android.util.AttributeSet
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import com.vessel.app.common.binding.setSrcResource
import com.vessel.app.common.binding.setVisible
import com.vessel.app.wellness.model.GoalRate
import java.util.*

class SmileFaceView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val goalAssessmentImageView by lazy { findViewById<ImageView>(R.id.goalAssessmentImageView) }
    private val rateIt by lazy { findViewById<AppCompatTextView>(R.id.rateIt) }
    var isFirstAppear: Boolean = true

    init {
        inflate(getContext(), R.layout.view_smile_face, this)
    }

    private fun startFadeAnimation() {
        val anim = AlphaAnimation(0f, 1f)
        anim.duration = HIGHLIGHT_FADE_ANIMATION_DURATION
        goalAssessmentImageView.startAnimation(anim)
    }

    fun startTransition(x: Float, y: Float, xValue: Int, date: Long?, showSmileFace: Boolean?) {
        if (isFirstAppear) {
            translationX = x - X_AXIS_SPACING
            isFirstAppear = false
            startFadeAnimation()

            goalAssessmentImageView.translationY = y
        } else {
            animate().translationX(x - X_AXIS_SPACING)
                .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
        }
        updateLabelValue(y, xValue, date, showSmileFace)
    }

    fun updateLabelValue(newPoint: Float, value: Int, date: Long?, showSmileFace: Boolean?) {
        if (showSmileFace == true) {
            val goalRate = GoalRate.valueOf(value)
            goalAssessmentImageView.setVisible(true)
            if (DateUtils.isToday(date ?: Calendar.getInstance().timeInMillis) && goalRate == GoalRate.UNKNOWN) {
                goalAssessmentImageView.setVisible(false)
                rateIt.setVisible(true)
            } else {
                rateIt.setVisible(false)
            }
            val imageRes = when (goalRate) {
                GoalRate.SAD -> R.drawable.sad_face_brown_without_background
                GoalRate.MEH -> R.drawable.normal_face_yellow_without_background
                GoalRate.HAPPY -> R.drawable.smile_face_green_without_background
                else -> android.R.color.transparent
            }

            goalAssessmentImageView.apply {
                animate()
                    .translationY(newPoint)
                    .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
                setSrcResource(imageRes)
            }
            rateIt.apply {
                animate()
                    .translationY(newPoint)
                    .translationX(X_AXIS_RATE_IT_SPACING)
                    .setInterpolator(AccelerateInterpolator()).duration = X_LABEL_ANIMATION_DURATION
            }
        } else {
            goalAssessmentImageView.setVisible(false)
            rateIt.setVisible(false)
        }
    }

    companion object {
        const val X_AXIS_SPACING = 7f
        const val X_AXIS_RATE_IT_SPACING = -25f
        const val X_LABEL_ANIMATION_DURATION = 500L
        const val HIGHLIGHT_FADE_ANIMATION_DURATION = 900L
    }
}
