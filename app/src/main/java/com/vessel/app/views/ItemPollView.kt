package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import kotlinx.android.synthetic.main.item_poll_view.view.*

open class ItemPollView@JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {
    var onSelected: ((View) -> Unit)? = null
    fun setQuestion(question: String?) {
        lblQuestion.text = question
    }

    init {
        inflate(context, R.layout.item_poll_view, this)
        initView()
    }

    private fun initView() {
        swSelected.setOnCheckedChangeListener { compoundButton, b ->
            onSelected?.invoke(this)
        }
    }
}
