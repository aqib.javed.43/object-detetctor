package com.vessel.app.testwinscore.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.testwinscore.model.TestWinScoreGoalItem

class TestWinScoreGoalAdapter : BaseAdapterWithDiffUtil<TestWinScoreGoalItem>() {
    override fun getLayout(position: Int) = R.layout.item_test_win_score_goal
}
