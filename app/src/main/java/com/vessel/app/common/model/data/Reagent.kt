package com.vessel.app.common.model.data

import android.os.Parcelable
import androidx.annotation.DrawableRes
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
class Reagent(
    val id: Int,
    val apiName: String,
    val displayName: String,
    val unit: String,
    val ranges: List<ReagentRange> = listOf(),
    val lowerBound: Float,
    val upperBound: Float,
    val minValue: Float,
    val maxValue: Float,
    @DrawableRes val headerImage: Int,
    val isBeta: Boolean,
    val isInverted: Boolean,
    val queryString: String?,
    val consumptionUnit: String?,
    val recommendedDailyAllowance: Float?,
    val info: List<ReagentInfo>?,
    val state: ReagentState,
    var impact: Int?
) : Parcelable

enum class ReagentState(val value: String) {
    ACTIVE("ACTIVE"), INACTIVE("INACTIVE"), COMING_SOON("COMING_SOON");

    companion object {
        fun from(state: String) = values().firstOrNull { it.value == state } ?: INACTIVE
    }
}
