// TODO fix the unit test

// package com.vessel.app.sample
//
// import com.vessel.app.BaseFaker
// import com.vessel.app.BaseFaker.Fake.FAKER
// import com.vessel.app.common.model.Reagent
// import com.vessel.app.common.model.Sample
// import com.vessel.app.common.net.data.SampleData
//
// object SampleFaker {
//    fun basic(reagent: Reagent = Reagent.Magnesium) = FAKER.run {
//        Sample(
//            reagent = reagent,
//            amount = number().randomDigit(),
//            apiName = ""
//        )
//    }
//
//    fun data(name: String = FAKER.dragonBall().character()) = SampleData(
//        name = name,
//        quantity = FAKER.number().randomDigit()
//    )
//
//    fun dataList() = BaseFaker.list { data() }
//
//    fun list() = BaseFaker.list { basic() }
// }
