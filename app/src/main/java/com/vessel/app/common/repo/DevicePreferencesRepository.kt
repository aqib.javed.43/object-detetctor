package com.vessel.app.common.repo

import android.content.Context
import com.chibatching.kotpref.KotprefModel
import com.squareup.moshi.Moshi
import com.vessel.app.common.net.mapper.GoalsMapper
import com.vessel.app.common.util.EncryptedPreferencesProvider
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DevicePreferencesRepository @Inject constructor(
    @ApplicationContext context: Context,
    preferencesProvider: EncryptedPreferencesProvider,
    val moshi: Moshi,
    val goalsMapper: GoalsMapper
) : KotprefModel(context, preferencesProvider) {
    var showPlanIntroduction by booleanPref(false)
    var showPlanUpdatedDialog by booleanPref(false)
    var showAppReviewPopup by booleanPref(true)
    var showPlanReadyHint by booleanPref(default = false)
    var showTipVideoAlertDialog by booleanPref(true)
}
