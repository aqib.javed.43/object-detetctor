package com.vessel.app.supplement.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AddSupplementIngredientIdRequest(
    val supplement_id: Int
)
