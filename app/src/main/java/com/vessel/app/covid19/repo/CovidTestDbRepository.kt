package com.vessel.app.covid19.repo

import com.vessel.app.covid19.db.CovidTestDao
import com.vessel.app.covid19.db.CovidTestDb
import com.vessel.app.covid19.db.CovidTestDbMapper
import com.vessel.app.covid19.model.CovidTest
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CovidTestDbRepository @Inject constructor(
    private val covidTestDao: CovidTestDao,
    private val covidTestDbMapper: CovidTestDbMapper
) {
    fun getCovidTests() = covidTestDao.getAllTests()
        .map {
            covidTestDbMapper.map(it)
                .sortedByDescending { covidTest: CovidTest -> covidTest.date }
        }

    suspend fun addTests(tests: List<CovidTestDb>) {
        covidTestDao.insertAll(tests)
    }
}
