package com.vessel.app.createpost

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CreatePostState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: CreatePostState.() -> Unit) = apply(block)
}
