package com.vessel.app.addsupplements.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.supplement.model.SupplementFormula

class AddSupplementsAdapter() : BaseAdapterWithDiffUtil<SupplementFormula>() {
    override fun getLayout(position: Int) = R.layout.item_supplement_goal
}
