package com.vessel.app.foodplan.model

data class SearchFilterItem(val searchQuery: String)
