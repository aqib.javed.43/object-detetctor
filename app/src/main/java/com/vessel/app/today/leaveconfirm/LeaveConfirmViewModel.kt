package com.vessel.app.today.leaveconfirm

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.Constants
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.ProgramAction

class LeaveConfirmViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    @Assisted savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()

    fun onCancel() {
        Constants.onCancel?.invoke()
        dismissDialog.call()
    }
    fun onConfirmWithClear() {
        Constants.onSelectConfirm?.invoke(ProgramAction.CLEAR)
        dismissDialog.call()
    }
    fun onConfirmWithKeep() {
        Constants.onSelectConfirm?.invoke(ProgramAction.KEEP)
        dismissDialog.call()
    }
}
