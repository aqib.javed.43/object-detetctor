package com.vessel.app.common.manager

import com.vessel.app.common.repo.OnBoardingProgramRepository
import javax.inject.Inject

class OnBoardingProgramManager @Inject constructor(
    private val onBoardingProgramRepository: OnBoardingProgramRepository
) {
    suspend fun onBoardingProgram(goalId: Int?) = onBoardingProgramRepository.getOnBoardingProgram(goalId)
}
