package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.net.payload.AddTipPayload
import com.vessel.app.common.net.service.TipService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TipRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val tipService: TipService,
    private val tipMapper: TipMapper
) : BaseNetworkRepository(moshi, loggingManager) {

    suspend fun getTipDetails(id: Int) =
        executeWithErrorMessage(tipMapper::mapTipData) {
            tipService.getTipDetails(
                id
            )
        }

    suspend fun addTip(
        title: String,
        imageUrl: String,
        frequency: String,
        mainGoalId: Int
    ) =
        executeWithErrorMessage(tipMapper::mapTipData) {
            tipService.addTip(
                AddTipPayload(
                    title,
                    imageUrl,
                    frequency,
                    mainGoalId
                )
            )
        }
    suspend fun getTipByTags(options: Map<String, String>) =
        executeWithErrorMessage(tipMapper::map) {
            tipService.getTipsByTags(
                options
            )
        }
}
