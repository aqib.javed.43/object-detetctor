package com.vessel.app.programs.details.adapters

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Essential
import kotlinx.android.synthetic.main.item_essential.view.*

class EssentialAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<Essential>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is Essential -> R.layout.item_essential
        else -> throw IllegalStateException(
            "Unexpected WellnessAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }
    interface OnActionHandler {
        fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int)
        fun showEssentialInformation()
    }
    override fun onBindViewHolder(holder: BaseViewHolder<Essential>, position: Int) {
        val item = getItem(position)
        holder.itemView.lblTitle.visibility = if (position == 0) View.VISIBLE else View.GONE
        holder.itemView.lblTitle.setOnClickListener {
            handler.showEssentialInformation()
        }
        item.days?.let {
            if (it.isNotEmpty()) {
                holder.itemView.valueDay.text = "Day ${it[0]}"
            }
        }
        super.onBindViewHolder(holder, position)
    }
}
