package com.vessel.app.views

import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.R

class ExpandableTextViewGroupSpanSizeLookup(
    private val adapter: ConcatAdapter
) : GridLayoutManager.SpanSizeLookup() {
    private val spanCount = 2

    override fun getSpanSize(position: Int): Int {
        return when (adapter.getItemViewType(position)) {
            R.layout.item_impact_goal -> 1
            else -> spanCount
        }
    }
}
