package com.vessel.app.onboarding.badhabitsselection

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class BadHabitAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<BadHabitSelect>() {
    override fun getLayout(position: Int) = R.layout.item_badhabit

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onHabitSelectClicked(item: BadHabitSelect)
    }
}
