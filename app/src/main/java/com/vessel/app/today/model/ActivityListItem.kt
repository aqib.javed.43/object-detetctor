package com.vessel.app.today.model

import com.vessel.app.plan.model.PlanItem
import java.util.*

data class ActivityListItem(
    var items: List<PlanItem>? = arrayListOf(),
    val date: Date,
    var programTitle: String? = null,
    var noMoreActivity: Boolean = true,
    var isPreviousDay: Boolean? = false
) {
    fun isFinishAll(): Boolean {
        if (items.isNullOrEmpty()) return true
        var isFinishAll = true
        for (item in items!!) {
            if (!item.isCompleted)isFinishAll = false
        }
        return isFinishAll
    }
    fun isFinishAllWithProgram(programId: Int): Boolean {
        if (items.isNullOrEmpty()) return false
        var isFinishAll = true
        val filters = items.orEmpty().filter { activity ->
            activity.currentPlan.program_id == programId
        }
        for (item in filters) {
            if (item.isCompleted.not())isFinishAll = false
        }
        return isFinishAll
    }
    fun getTitle(): String {
        return programTitle?.replace("Program", "") + " Activities"
    }
}
