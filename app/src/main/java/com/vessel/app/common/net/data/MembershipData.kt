package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MembershipData(
    val id: Int,
    val shopify_variant_id: Long,
    val product_title: String,
    val quantity: Int,
    val properties: List<MembershipPropertiesData>?,
    val status: String
)
