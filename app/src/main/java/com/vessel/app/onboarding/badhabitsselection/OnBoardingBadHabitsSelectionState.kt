package com.vessel.app.onboarding.badhabitsselection

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnBoardingBadHabitsSelectionState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val items = MutableLiveData<List<BadHabitSelect>>()
    val item_program = MutableLiveData<List<OnBoardingProgramSelect>>()
    operator fun invoke(block: OnBoardingBadHabitsSelectionState.() -> Unit) = apply(block)
}
