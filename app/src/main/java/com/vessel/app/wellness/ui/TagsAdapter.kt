package com.vessel.app.wellness.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.wellness.model.Tag

class TagsAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Tag>(
    { oldItem, newItem ->
        oldItem.name == newItem.name
    }
) {

    override fun getLayout(position: Int) = R.layout.item_goal_tag

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onTagSelected(tag: Tag)
    }
}
