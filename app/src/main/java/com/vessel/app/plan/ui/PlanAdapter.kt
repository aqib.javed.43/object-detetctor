package com.vessel.app.plan.ui

import android.animation.Animator
import android.annotation.SuppressLint
import android.text.InputType
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.MutableLiveData
import com.airbnb.lottie.LottieAnimationView
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.plan.model.PlanItem
import com.vessel.app.util.extensions.afterMeasured

class PlanAdapter(
    private val handler: OnActionHandler,
    private val parentPosition: Int,
    private val hideLocker: Boolean
) :
    BaseAdapterWithDiffUtil<PlanItem>(
        areItemsTheSame = { oldItem, newItem -> oldItem.id == newItem.id },
        areContentsTheSame = { oldItem, newItem -> oldItem.id == newItem.id }
    ) {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is PlanItem -> R.layout.item_plan
        else -> throw IllegalStateException(
            "Unexpected WellnessAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<PlanItem>,
        @SuppressLint("RecyclerView") position: Int
    ) {
        super.onBindViewHolder(holder, position)

        val item = getItem(position) as PlanItem

        holder.itemView.findViewById<SwipeLayout>(R.id.swipe)
            .addSwipeListener(object : SimpleSwipeListener() {
                override fun onStartOpen(layout: SwipeLayout?) {
                    super.onStartOpen(layout)
                    holder.itemView.findViewById<View>(R.id.dummyBackgroundView).isVisible = true
                }

                override fun onClose(layout: SwipeLayout?) {
                    super.onClose(layout)
                    holder.itemView.findViewById<View>(R.id.dummyBackgroundView).isVisible = false
                }
            })
        holder.itemView.findViewById<View>(R.id.blurBackground).alpha =
            if (item.isCompleted) 1f else 0f
        holder.itemView.findViewById<View>(R.id.bottomBlurBackground).alpha =
            if (item.isCompleted) 0f else 1f
        holder.itemView.findViewById<View>(R.id.reminder).alpha = if (item.isCompleted) 0f else 1f
        holder.itemView.findViewById<View>(R.id.reminderIcon).alpha =
            if (item.isCompleted) 0f else 1f
        holder.itemView.findViewById<View>(R.id.reminderTime).alpha =
            if (item.isCompleted) 0f else 1f
        holder.itemView.findViewById<View>(R.id.reminderButton).alpha =
            if (item.isCompleted) 0f else 1f

        holder.itemView.findViewById<View>(R.id.deleteSwipe).setOnClickListener {
            handler.onItemDeleteClicked(item, it, position, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.reminderTime).setOnClickListener {
            handler.onAddReminderClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.background).setOnClickListener {
            handler.onPlanItemClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.editSwipe).setOnClickListener {
            handler.onItemEditClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.swipeView).afterMeasured {
            this.apply {
                updateLayoutParams {
                    val findViewById = holder.itemView.findViewById<View>(R.id.dummyBackground)
                    height = findViewById.height
                    y = findViewById.y
                }
            }
        }
        val checkboxLocker = holder.itemView.findViewById<AppCompatImageView>(R.id.checkboxLocker)
        val checkbox = holder.itemView.findViewById<AppCompatCheckBox>(R.id.checkbox)
        val checkboxBg = holder.itemView.findViewById<View>(R.id.checkboxBg)
        val enableCheckBox = (item.isCompleted && item.isGrouped && item.groupCount <= 1).not()
        checkbox.apply {
            isEnabled = enableCheckBox
            isClickable = enableCheckBox
        }
        checkboxBg.apply {
            isEnabled = enableCheckBox
            isClickable = enableCheckBox
        }

        if (item.isGrouped && item.isAllTodayItemsChecked() && item.isCompleted.not()) {
            checkboxLocker.isVisible = true
        } else if (item.isGrouped.not() && hideLocker.not()) {
            checkboxLocker.isVisible = true
        } else {
            checkboxLocker.isVisible = false
            checkbox.setOnClickListener {
                handlePlanCheckClick(holder, position, it)
            }
            checkboxBg.setOnClickListener {
                handlePlanCheckClick(holder, position, it)
            }
        }

        holder.itemView.findViewById<AppCompatImageView>(R.id.reminderIcon).apply {
            setOnClickListener {
                this@PlanAdapter.handler.onReminderNotificationClicked(
                    item,
                    it,
                    position,
                    parentPosition
                )
            }

            setImageResource(
                if (item.currentPlan.notification_enabled == true) {
                    R.drawable.ic_reminders
                } else {
                    R.drawable.ic_reminders_off
                }

            )
        }

        holder.itemView.findViewById<View>(R.id.reminderButton).apply {
            setOnClickListener {
                this@PlanAdapter.handler.onReminderNotificationClicked(
                    item,
                    it,
                    position,
                    parentPosition
                )
            }
        }

        holder.itemView.findViewById<View>(R.id.fadding).setOnClickListener {
            val isExpanded = holder.itemView.findViewById<TextView>(R.id.goalsData).lineCount != 1
            if (isExpanded && item.currentPlan.isTip().not()) {
                holder.itemView.findViewById<TextView>(R.id.goalsData).apply {
                    setSingleLine()
                    isElegantTextHeight = true
                }
                holder.itemView.findViewById<TextView>(R.id.improvesData).apply {
                    setSingleLine()
                    isElegantTextHeight = true
                }
            } else {
                holder.itemView.findViewById<TextView>(R.id.goalsData).apply {
                    inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    isElegantTextHeight = true
                    isSingleLine = false
                }
                holder.itemView.findViewById<TextView>(R.id.improvesData).apply {
                    inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    isElegantTextHeight = true
                    isSingleLine = false
                }
            }
        }

        if (item.type == PlanType.TestPlan) {
            holder.itemView.findViewById<View>(R.id.fadding).isVisible = false
            holder.itemView.findViewById<View>(R.id.improvesData).isVisible = false
            holder.itemView.findViewById<View>(R.id.goalsData).isVisible = false
        } else {
            val goals = item.currentPlan.getPlanGoals()
            val improves = item.currentPlan.getImproves()
            holder.itemView.findViewById<View>(R.id.fadding).isVisible =
                goals.trim().isNotEmpty() || improves.trim().isNotEmpty()
            holder.itemView.findViewById<TextView>(R.id.goalsData).apply {
                text = goals
                isVisible = goals.trim().isNotEmpty()
            }
            holder.itemView.findViewById<TextView>(R.id.improvesData).apply {
                text = improves
                isVisible = improves.trim().isNotEmpty()
            }

            if (improves.trim().isEmpty() || goals.trim().isEmpty()) {
                holder.itemView.findViewById<TextView>(R.id.goalsData).apply {
                    inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    isElegantTextHeight = true
                    isSingleLine = false
                }
                holder.itemView.findViewById<TextView>(R.id.improvesData).apply {
                    inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    isElegantTextHeight = true
                    isSingleLine = false
                }
            }
        }
        holder.itemView.findViewById<View>(R.id.checkbox).isVisible = true

        holder.itemView.findViewById<LottieAnimationView>(R.id.planAddingAnimationView).apply {
            isVisible = false
            scale = ANIMATION_VIEW_SCALE_FACTOR
        }

        holder.itemView.findViewById<LottieAnimationView>(R.id.planSuccessAnimationView)
            .apply {
                setAnimation(R.raw.plan_success)
                scale = ANIMATION_VIEW_SCALE_FACTOR
                addAnimatorListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        this@PlanAdapter.handler.onItemSelected(
                            getItem(position),
                            this@apply,
                            position,
                            parentPosition
                        )
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        setAnimation(R.raw.plan_success)
                        this@PlanAdapter.handler.onAnimationFinished(
                            getItem(position),
                            this@apply,
                            position
                        )
                    }

                    override fun onAnimationCancel(animation: Animator) {
                    }

                    override fun onAnimationRepeat(animation: Animator) {
                    }
                })
            }

        holder.itemView.findViewById<ConstraintLayout>(R.id.groupBackground).apply {
            isVisible = item.isGrouped && item.groupCount > 1
            backgroundTintList =
                if (item.type == PlanType.SupplementPlan || item.type == PlanType.StressPlan) {
                    ContextCompat.getColorStateList(context, R.color.roseGoldBgAlpha50)
                } else {
                    ContextCompat.getColorStateList(context, R.color.pixiGreenAlpha50)
                }
            findViewById<TextView>(R.id.planItemsCount).apply {
                text = "+ ${item.groupCount - 1}"
                setOnClickListener {
                    this@PlanAdapter.handler.onGroupedItemsClicked(
                        item,
                        it,
                        this@PlanAdapter.parentPosition
                    )
                }
            }
        }
    }

    private fun handlePlanCheckClick(holder: BaseViewHolder<PlanItem>, position: Int, view: View) {
        val item = getItem(position)
        if ((item as PlanItem).isCompleted.not()) {
            holder.itemView.findViewById<LottieAnimationView>(R.id.planSuccessAnimationView)
                .playAnimation()
            holder.itemView.findViewById<LottieAnimationView>(R.id.planAddingAnimationView).apply {
                isVisible = true
                playAnimation()
            }
            holder.itemView.findViewById<View>(R.id.reminder).animate().alpha(0f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderButton).animate().alpha(0f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderTime).animate().alpha(0f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderIcon).animate().alpha(0f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.bottomBlurBackground).animate()
                .alpha(0f).duration = ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.blurBackground).animate().alpha(1f).duration =
                ANIMATION_DURATION
        } else {
            holder.itemView.findViewById<View>(R.id.reminder).animate().alpha(1f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderButton).animate().alpha(1f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderTime).animate().alpha(1f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderIcon).animate().alpha(1f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.bottomBlurBackground).animate()
                .alpha(1f).duration = ANIMATION_DURATION
            this@PlanAdapter.handler.onItemSelected(
                item,
                view,
                position,
                parentPosition
            )
            holder.itemView.findViewById<View>(R.id.blurBackground).animate().apply {
                setListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        this@PlanAdapter.handler.onAnimationFinished(
                            getItem(position),
                            view,
                            position
                        )
                    }

                    override fun onAnimationCancel(animation: Animator) {
                    }

                    override fun onAnimationRepeat(animation: Animator) {
                    }
                })
                alpha(0f).duration = ANIMATION_DURATION
            }
        }
    }

    override fun getHandler(position: Int): Any? = handler

    interface OnActionHandler {
        fun onPlanItemClicked(item: Any, view: View, parentPosition: Int)
        fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int)
        fun onAddReminderClicked(item: Any, view: View, parentPosition: Int)
        fun onReminderNotificationClicked(item: Any, view: View, position: Int, parentPosition: Int)
        fun onItemDeleteClicked(item: Any, view: View, position: Int, parentPosition: Int)
        fun onItemEditClicked(item: Any, view: View, parentPosition: Int)
        fun onGroupedItemsClicked(item: Any, view: View, parentPosition: Int)
        fun onAnimationFinished(item: Any, view: View, parentPosition: Int)
        val showPlanNotification: MutableLiveData<Boolean>
        val planNotificationCount: MutableLiveData<Int>
    }

    companion object {
        private const val ANIMATION_VIEW_SCALE_FACTOR = 0.4f
        private const val ANIMATION_DURATION: Long = 500
    }
}
