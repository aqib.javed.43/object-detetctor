package com.vessel.app.testscore

import androidx.annotation.ColorRes
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.ReagentsManager
import com.vessel.app.common.manager.ScoreManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.data.ReagentLevel
import com.vessel.app.common.model.data.ReagentState
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.reagentpersonalizing.model.ReagentRecommendationModel
import com.vessel.app.testscore.ui.ResultAdapter
import com.vessel.app.testscore.ui.TestScoreFragmentDirections
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import com.vessel.app.wellness.model.ReagentItem
import kotlinx.coroutines.launch
import kotlin.collections.ArrayList

class TestScoreViewModel @ViewModelInject constructor(
    val state: TestScoreState,
    resourceProvider: ResourceRepository,
    private val scoreManager: ScoreManager,
    val preferencesRepository: PreferencesRepository,
    private val reagentsManager: ReagentsManager,
    val homeTabsManager: HomeTabsManager,
    val buildPlanManager: BuildPlanManager,
    val trackingManager: TrackingManager,
    val goalsRepository: GoalsRepository
) : BaseViewModel(resourceProvider),
    ResultAdapter.OnActionHandler {

    private var neededItemsToImproveCount: Boolean? = null

    init {
        verifyReagents()
        loadScores()
    }

    private fun processNext(navigateDirectly: Boolean = false) {
        viewModelScope.launch {
            neededItemsToImproveCount = buildPlanManager.getReagentRecommendation().isNotEmpty()
            if (navigateDirectly) {
                state.navigateTo.value =
                    TestScoreFragmentDirections.actionTestScoreFragmentToPlanPersonalizingFragment()
            }
        }
    }

    private fun getNeededItemsToImproveCount(navigateDirectly: Boolean = false) {
        viewModelScope.launch {
            val recommendList: ArrayList<ReagentRecommendationModel> = arrayListOf()
            state.reagentItems.value?.let {
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.PH.id
                }?.let { item ->
                    if (item.lastReagentChartRange?.reagentType != ReagentLevel.Good)recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = if (item.lastReagentChartRange?.reagentType == ReagentLevel.Low) RecommendationType.Food else RecommendationType.Tip,
                            item.lastReagentChartRange?.reagentType
                        )
                    )
                }
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.B7.id
                }?.let { item ->
                    if (item.lastReagentChartRange?.reagentType == ReagentLevel.Low)recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = RecommendationType.Food,
                            item.lastReagentChartRange.reagentType
                        )
                    )
                }
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.VitaminC.id
                }?.let { item ->
                    if (item.lastReagentChartRange?.reagentType == ReagentLevel.Low)recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = RecommendationType.Food,
                            item.lastReagentChartRange.reagentType
                        )
                    )
                }
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.Magnesium.id
                }?.let { item ->
                    if (item.lastReagentChartRange?.reagentType == ReagentLevel.Low)recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = RecommendationType.Food,
                            item.lastReagentChartRange.reagentType
                        )
                    )
                }
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.Calcium.id
                }?.let { item ->
                    if (item.lastReagentChartRange?.reagentType == ReagentLevel.Low)recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = RecommendationType.Food,
                            item.lastReagentChartRange.reagentType
                        )
                    )
                }
                // Add Ketones
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.Ketones.id
                }?.let { item ->
                    if (item.lastReagentChartRange?.reagentType == ReagentLevel.Low)recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = RecommendationType.Food,
                            item.lastReagentChartRange.reagentType
                        )
                    )
                }
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.Hydration.id
                }?.let { item ->
                    if (item.lastReagentChartRange?.reagentType != ReagentLevel.Good)recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = RecommendationType.Hydration,
                            item.lastReagentChartRange?.reagentType
                        )
                    )
                }
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.Cortisol.id
                }?.let { item ->
                    if (buildPlanManager.hasStressRecommendations()) recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = RecommendationType.Lifestyle,
                            item.lastReagentChartRange?.reagentType
                        )
                    )
                }
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.Sodium.id && reagent.lastReagentChartRange?.reagentType == ReagentLevel.Low
                }?.let { item ->
                    recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = RecommendationType.Tip,
                            item.lastReagentChartRange?.reagentType
                        )
                    )
                }
                it.firstOrNull { reagent ->
                    reagent.id == com.vessel.app.common.model.data.ReagentItem.Nitrites.id && reagent.lastReagentChartRange?.reagentType == ReagentLevel.Low
                }?.let { item ->
                    recommendList.add(
                        ReagentRecommendationModel(
                            item.id,
                            recommendationType = RecommendationType.Tip,
                            item.lastReagentChartRange?.reagentType
                        )
                    )
                }
                buildPlanManager.updateReagentRecommendation(recommendList)
                processNext(navigateDirectly)
            }
        }
    }

    fun isFirstTest() = preferencesRepository.testCompletedCount == 1
    private fun loadScores() {
        showLoading()
        viewModelScope.launch {
            scoreManager.getLatestWellnessScores(forceUpdate = true)
                .onSuccess { score ->
                    hideLoading()
                    state {
                        val currentTestScore = score.wellness.last().y.toInt()
                        val oldResult = if (score.wellness.size > 1) {
                            score.wellness[score.wellness.size - 2].y.toInt()
                        } else {
                            0
                        }
                        val maxScore = score.wellness.take(score.wellness.size - 1).maxOfOrNull {
                            it.y.toInt()
                        } ?: 0
                        scoreItems.value = Triple(oldResult, currentTestScore, maxScore)

                        reagentItems.value =
                            score.reagents.filter {
                                it.key.state != ReagentState.COMING_SOON && it.value.firstOrNull()?.y != Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE
                            }.map {
                                ReagentItem(
                                    id = it.key.id,
                                    title = it.key.displayName,
                                    unit = it.key.unit,
                                    chartEntries = it.value,
                                    lowerBound = it.key.lowerBound,
                                    upperBound = it.key.upperBound,
                                    minY = it.key.minValue,
                                    maxY = it.key.maxValue,
                                    dimmed = false,
                                    ranges = it.key.ranges,
                                    isBetaTesting = it.key.isBeta,
                                    isInverted = it.key.isInverted,
                                    selectedChartEntityPosition = it.value.lastIndex,
                                    consumptionUnit = it.key.consumptionUnit,
                                    info = it.key.info,
                                    state = it.key.state,
                                    impact = null
                                )
                            }
                    }
                    getNeededItemsToImproveCount(false)
                }
                .onError {
                    hideLoading()
                    // todo handle error case
                }
        }
    }

    fun onBackClicked() {
        navigateBack()
    }

    fun onContinueClicked() {
        if (state.isFirstAnimationDone && state.isReagentAnimationDone) {
            gotToNextStep()
        } else if (state.isFirstAnimationDone) {
            if (preferencesRepository.testCompletedCount == 1) {
                state.startReagentAndHeaderScaleAnimation.call()
            } else {
                gotToNextStep()
            }
        }
    }

    private fun gotToNextStep() {
        when {
            preferencesRepository.testCompletedCount <= 1 -> {
                getNeededItemsToImproveCount(true)
            }
            neededItemsToImproveCount == null -> {
                getNeededItemsToImproveCount(true)
            }
            neededItemsToImproveCount == true -> {
                state.navigateTo.value =
                    TestScoreFragmentDirections.actionTestScoreFragmentToPlanPersonalizingFragment()
            }
            else -> {
                homeTabsManager.clear()
                state.navigateTo.value = TestScoreFragmentDirections.actionGoToHome()
            }
        }
    }

    enum class ScoreLevel(val level: String, @ColorRes val color: Int) {
        Poor("Poor", R.color.roseGoldBg),
        Fair("Fair", R.color.lightGrayishOrange),
        Good("Good", R.color.lightGrayishGreen),
        Great("Great", R.color.pixiGreen);

        companion object {
            fun fromScorePoints(score: Int) =
                when (score) {
                    in 0..25 -> Poor
                    in 26..50 -> Fair
                    in 51..75 -> Good
                    in 76..100 -> Great
                    else -> null
                }
        }
    }

    fun onToolTipClicked() {
        if (state.isFirstAnimationDone) {
            state.navigateTo.value =
                TestScoreFragmentDirections.actionTestScoreFragmentToToolTipScoreFragment()
        }
    }

    private fun verifyReagents() {
        viewModelScope.launch {
            reagentsManager.fetchReagent()
        }
    }

    override fun onReagentSelected(item: ReagentItem, position: Int) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.AFTER_TEST_FLOW_TAPPED)
                .addProperty(TrackingConstants.REAGENT_iD, item.id.toString())
                .addProperty(TrackingConstants.SEQUENCE, position.toString())
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value =
            TestScoreFragmentDirections.actionTestScoreFragmentToReagentGraphFragment(
                com.vessel.app.reagent.model.ReagentHeader(
                    id = item.id,
                    title = item.title,
                    unit = item.unit,
                    chartEntries = item.chartEntries.map {
                        DateEntry(
                            it.x,
                            it.y,
                            it.date,
                            it.reagentsCount
                        )
                    },
                    lowerBound = item.lowerBound,
                    upperBound = item.upperBound,
                    minY = item.minY,
                    maxY = item.maxY,
                    background = reagentsManager.fromId(item.id)!!.headerImage,
                    ranges = item.ranges,
                    errorDescription = if (item.id == com.vessel.app.common.model.data.ReagentItem.B9.id) getResString(
                        R.string.b9_not_available_message
                    ) else getResString(
                        R.string.reagent_test_results_unavailable,
                        item.title
                    ),
                    isBetaReagent = item.isBetaTesting,
                    betaHint = getResString(
                        R.string.reagent_beta_test_hint,
                        item.title
                    ),
                    consumptionUnit = item.consumptionUnit
                )
            )
    }
}
