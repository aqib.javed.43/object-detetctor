package com.vessel.app.supplement.model

data class MembershipPayment(
    val card_last4: Int,
    val card_exp_month: Int,
    val card_exp_year: Int,
    val paymentType: paymentType?
)

enum class paymentType(val payment: String) {
    PAYPAL("paypal"),
    GOOGLE("google"),
    APPLE("apple");

    companion object {
        fun from(paymentText: String) = values().firstOrNull { it.payment == paymentText }
    }
}
