package com.vessel.app.util

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.pushwoosh.firebase.PushwooshFcmHelper
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.notification.NotificationHelper
import zendesk.chat.Chat
import zendesk.chat.PushData
import javax.inject.Inject

class FirebaseMessagingRouterService : FirebaseMessagingService() {
    @Inject
    protected lateinit var preferencesRepository: PreferencesRepository

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        PushwooshFcmHelper.onTokenRefresh(token)
        if (::preferencesRepository.isInitialized) {
            preferencesRepository.fcmToken = token
            Chat.INSTANCE.providers()?.pushNotificationsProvider()?.registerPushToken(token)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (PushwooshFcmHelper.isPushwooshMessage(remoteMessage)) {
            // this is a Pushwoosh push, SDK will handle it automatically
            PushwooshFcmHelper.onMessageReceived(this, remoteMessage)
        } else {
            // this is not a Pushwoosh push, you should handle it by yourself
            dispatchNonPushwooshMessage(remoteMessage)
        }
    }

    private fun dispatchNonPushwooshMessage(remoteMessage: RemoteMessage) {
        // Implement your push handling logics here

        val pushData: PushData? = Chat.INSTANCE.providers()?.pushNotificationsProvider()
            ?.processPushNotification(remoteMessage.data)

        if (pushData != null) {
            val title = pushData.author
            val body = pushData.message

            NotificationHelper.displayNotification(
                applicationContext,
                title.orEmpty(),
                body.orEmpty()
            )
        }
    }
}
