package com.vessel.app.onboarding.goals

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.Constants.USER_ALLOWED_GOALS_SELECTION_COUNT
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.onboarding.goals.model.OnboardingGoalSelect
import com.vessel.app.onboarding.goals.ui.OnboardingGoalSelectAdapter
import com.vessel.app.onboarding.goals.ui.OnboardingGoalsFragmentDirections
import com.vessel.app.util.ResourceRepository

class OnboardingGoalsViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    private val goalsRepository: GoalsRepository
) : BaseViewModel(resourceProvider), ToolbarHandler, OnboardingGoalSelectAdapter.OnActionHandler {

    val state = OnboardingGoalsState()
    val bgColor = if (preferencesRepository.isExistingUserFirstTime) R.color.beige_color else R.color.primaryLight
    fun mapGoals() {
        state.items.value = goalsRepository.getGoals()
            .map { entry ->
                entry.key.let {
                    OnboardingGoalSelect(
                        id = it.id,
                        title = it.title,
                        checked = preferencesRepository.getUserChosenGoals().any { g -> g.id == it.id }
                    )
                }
            }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onNextClicked() {
        val selectedItems = state.items.value.orEmpty().filter { it.checked }

        if (selectedItems.size == USER_ALLOWED_GOALS_SELECTION_COUNT) {
            goalsRepository.setUserChosenGoals(
                selectedItems.map {
                    GoalSample(
                        it.id,
                        getResString(it.title)
                    )
                }
            )
            state.navigateTo.value =
                OnboardingGoalsFragmentDirections.actionOnboardingGoalsFragmentToOnBoardingOneGoalSelectionFragment()
        } else {
            showError(getResString(R.string.select_three_goals_to_proceed))
        }
    }

    override fun onGoalSelectClicked(item: OnboardingGoalSelect) {
        state {
            val selectedItems = items.value.orEmpty().filter { it.checked }

            if (selectedItems.size >= USER_ALLOWED_GOALS_SELECTION_COUNT && item.checked.not()) {
                refreshItems.value = true
                return@state
            }

            items.value =
                items.value.orEmpty().mapIndexed { _, goalSelect ->
                    if (goalSelect.id == item.id) goalSelect.copy(checked = !item.checked)
                    else goalSelect
                }.toMutableList()
            refreshItems.value = true
        }
    }
}
