package com.vessel.app.wellness.net.service

import com.vessel.app.wellness.net.data.ScoreData
import retrofit2.http.GET
import retrofit2.http.Query

interface ScoreService {
    @GET("score/average")
    suspend fun getAverageScores(
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String,
        @Query("show_all") showAll: Boolean = false,
        @Query("minimal") minimal: Boolean = true
    ): List<ScoreData>

    @GET("score/summary")
    suspend fun getScores(
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String,
        @Query("show_all") showAll: Boolean = false,
        @Query("minimal") minimal: Boolean = true
    ): List<ScoreData>

    @GET("score/summary")
    suspend fun getAllScores(
        @Query("show_all") showAll: Boolean = false,
        @Query("minimal") minimal: Boolean = true
    ): List<ScoreData>

    @GET("score/average")
    suspend fun getAllAverageScores(
        @Query("show_all") showAll: Boolean = false,
        @Query("minimal") minimal: Boolean = true
    ): List<ScoreData>
}
