package com.vessel.app.wellness.tipdeatails.ui

import android.os.Bundle
import android.widget.TextView
import androidx.core.view.isInvisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.activityreview.ui.ActivityReviewAdapter
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.common.adapter.planreminders.toReminderList
import com.vessel.app.common.model.Goal
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.BlackRoundedButtonGroup
import com.vessel.app.views.ExpandableTextViewGroup
import com.vessel.app.views.PlanDeleteDialog
import com.vessel.app.views.setImpactsGoals
import com.vessel.app.wellness.tipdeatails.TipDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TipDetailsFragment : BaseFragment<TipDetailsViewModel>() {
    override val viewModel: TipDetailsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_tip_details

    private val reminderRowAdapter by lazy { ReminderRowAdapter(viewModel) }
    private val description: TextView by lazy { requireView().findViewById<TextView>(R.id.description) }
    private val activityReviewAdapter by lazy { ActivityReviewAdapter(viewModel) }
    private val aboutValue: ExpandableTextViewGroup by lazy {
        requireView().findViewById<ExpandableTextViewGroup>(
            R.id.aboutValue
        )
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupScheduleList()
        setupReviewList()
        setupObservers()
    }

    private fun setupScheduleList() {
        requireView().findViewById<RecyclerView>(R.id.scheduleList).let {
            it.adapter = reminderRowAdapter
            it.itemAnimator = null
        }
    }
    private fun setupReviewList() {
        requireView().findViewById<RecyclerView>(R.id.reviewList).let {
            it.adapter = activityReviewAdapter
            it.itemAnimator = null
        }
        requireView().findViewById<BlackRoundedButtonGroup>(R.id.btnWriteReview).let {
            it.setOnClickListener {
                viewModel.gotoReview()
            }
        }
    }
    private fun setupObservers() {
        viewModel.state {
            observe(tip) { tip ->
                val goals = tip.impactsGoals.orEmpty().mapIndexed { index, goal ->
                    ImpactGoalModel(
                        goal.name,
                        goal.image_small_url,
                        Goal.alternateBackground(index),
                    )
                }
                if (ownItemTip.value == true) {
                    description.text = getString(R.string.activity_created_by_you)
                } else {
                    if (tip.contactFullName.isNullOrEmpty().not()) {
                        val text = getString(R.string.recommended_by_expert, tip.contactFullName) + " to help with ${Constants.GOALS.filter { it.id == tip.mainGoalId }.joinToString { it.name }}"
                        description.text = text
                    } else
                        description.isInvisible = true
                }
                aboutValue.setImpactsGoals(goals)
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(plans) {
                reminderRowAdapter.submitList(it.toReminderList())
            }
            observe(viewModel.state.removeReminder) { plans ->
                PlanDeleteDialog.showDialog(
                    requireActivity(),
                    viewModel.state.tip.value?.title.orEmpty(),
                    {
                        // Not used
                    },
                    {
                        viewModel.deletePlanItem(plans)
                    },
                    {
                        viewModel.onDontShowAgainClicked(it)
                    }
                )
            }
            observe(reviews) { reviews ->
                requireView().findViewById<TextView>(R.id.memberReviewTitle).let {
                    it.text = String.format(getString(R.string.member_review_count), reviews.size.toString())
                }
                activityReviewAdapter.submitList(reviews)
            }
        }
    }
}
