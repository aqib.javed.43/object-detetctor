package com.vessel.app.views

import android.app.DatePickerDialog
import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.DatePicker
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import com.vessel.app.R
import java.text.SimpleDateFormat
import java.util.*

class DatePickerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr),
    DatePickerDialog.OnDateSetListener {

    companion object {
        const val MODE_CALENDAR = "calendar"
        const val MODE_SPINNER = "spinner"
        private const val EIGHTEEN_YEARS_OLD = 18

        @JvmField
        val CURRENT_DATE: Date = Date()

        @JvmStatic
        fun dateForMinimumAgeAllowed() = Calendar.getInstance()
            .apply { add(Calendar.YEAR, -EIGHTEEN_YEARS_OLD) }
            .time
        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    }

    private val calendar = Calendar.getInstance()
    private val heightMin by lazy { resources.getDimensionPixelSize(R.dimen.date_picker_min_height) }
    private val font by lazy { ResourcesCompat.getFont(context, R.font.noetext_regular) }
    private val fontSize by lazy { resources.getDimension(R.dimen.font_small) }
    private val verticalPadding by lazy { resources.getDimensionPixelSize(R.dimen.spacing_large) }
    private val horizontalPadding by lazy { resources.getDimensionPixelSize(R.dimen.spacing_xl) }
    private var listener: OnDateChangedListener? = null

    private var mode: String = MODE_CALENDAR
    private var maxDate: Date? = null
    private var dateFormat = DATE_FORMAT

    init {
        typeface = font
        minHeight = heightMin
        gravity = Gravity.CENTER_VERTICAL
        setBackgroundResource(R.drawable.white_transparent_background)
        setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding)
        setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize)
        setText(R.string.date)
        setOnClickListener { showDatePickerDialog() }
    }

    fun setMode(mode: String) {
        this.mode = mode
    }

    fun setMaxDate(date: Date) {
        maxDate = date
    }

    fun setDateFormat(format: SimpleDateFormat) {
        dateFormat = format
    }

    private fun showDatePickerDialog() {
        DatePickerDialog(
            context,
            if (mode == MODE_SPINNER) R.style.SpinnerDatePickerDialogTheme else 0,
            this,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        ).apply {
            maxDate?.let { datePicker.maxDate = it.time }
        }.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        calendar.run {
            set(Calendar.YEAR, year)
            set(Calendar.MONTH, month)
            set(Calendar.DAY_OF_MONTH, dayOfMonth)
        }
        text = dateFormat.format(calendar.time)
        listener?.onDateChanged(text.toString())
    }

    fun setDate(date: String?) {
        if (date.isNullOrEmpty()) {
            setText(R.string.date)
        } else {
            text = date
        }
    }

    fun onDateChanged(listener: OnDateChangedListener) {
        this.listener = listener
    }

    interface OnDateChangedListener {
        fun onDateChanged(date: String)
    }
}
