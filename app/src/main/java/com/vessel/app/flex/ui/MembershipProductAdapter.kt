package com.vessel.app.flex.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.flex.model.MembershipOptionProduct
import kotlinx.android.synthetic.main.item_membership_option.view.*

class MembershipProductAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<MembershipOptionProduct>() {
    override fun getLayout(position: Int) = R.layout.item_membership_option

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onItemSelectClicked(item: MembershipOptionProduct)
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<MembershipOptionProduct>,
        position: Int
    ) {
        super.onBindViewHolder(holder, position)
        holder.itemView.btnBuy.setText(getItem(position).buttonTitle.toString())
    }
}
