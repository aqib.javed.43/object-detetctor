package com.vessel.app.common.model

import android.os.Parcelable
import com.vessel.app.R
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.programs.details.uimodels.ProgramScheduleRowUiModel
import com.vessel.app.programs.details.uimodels.ProgramScheduleUiModel
import com.vessel.app.util.extensions.toCalender
import com.vessel.app.wellness.model.ScienceSource
import com.vessel.app.wellness.net.data.DifficultyLevel
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*
import java.util.concurrent.TimeUnit

@Parcelize
data class Program(
    val id: Int,
    val title: String,
    var likeStatus: LikeStatus,
    val reviewedContactIds: List<Int>?,
    val difficulty: DifficultyLevel,
    val contactId: Int,
    var isEnrolled: Boolean,
    val imageUrl: String,
    val durationDays: Int,
    var totalLikes: Int,
    var totalDislikes: Int,
    val mainGoalId: Int,
    val description: String,
    val contact: Contact?,
    val createdAt: String,
    val goals: List<GoalRecord>?,
    val sources: List<ScienceSource>?,
    val planData: MutableList<PlanData>?,
    val enrolledDate: Date?,
    val frequency: Int,
    val schedule: List<ScheduleItem>?,
    val essentials: List<Essential>? = null,
    val image: Int? = null
) : Parcelable {
    @IgnoredOnParcel
    private var programScheduleUiModel: ProgramScheduleUiModel? = null

    fun getFormattedDuration() =
        if (durationDays % 7 == 0) {
            val durationWeeks = durationDays / 7
            if (durationWeeks == 1) {
                "$durationWeeks Week"
            } else {
                "$durationWeeks Weeks"
            }
        } else if (durationDays == 1) {
            "$durationDays Day"
        } else {
            "$durationDays Days"
        }

    private fun getEnrolledDays(): Int {
        return if (isEnrolled) {
            val diff = Calendar.getInstance().timeInMillis - (
                enrolledDate?.time ?: Calendar.getInstance().timeInMillis
                )
            TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toInt()
        } else 0
    }

    fun getRemainingPeriod() = durationDays - getEnrolledDays()

    fun getCompletedItemPercent(): Int {
        val totalCount = getItemCount()
        return if (totalCount == 0)
            100
        else getCompletedItems().times(100).div(totalCount)
    }

    fun isEnded() = getRemainingPeriod() <= 0

    fun isInLastDay() = getRemainingPeriod() == 1

    fun getItemCount() = getScheduleUiModel().totalItems

    fun getCompletedItems() = getScheduleUiModel().completedItems

    fun getCompletedItemsOverTheTotal() = "${getCompletedItems()}/${getItemCount()}"

    fun getAchievement() = getScheduleUiModel().maxStreaks

    fun getButtonResId() = if (isEnrolled)
        R.string.view_program
    else
        R.string.join_program

    fun difficultyBackground() = when (difficulty) {
        DifficultyLevel.HARD -> R.drawable.red_rose_background_with_12_radius
        DifficultyLevel.MEDIUM -> R.drawable.beige_background_with_12_radius
        else -> R.drawable.gray_background_with_12_radius
    }

    fun getLikeBackground() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.green_background_with_16_radius
    else
        R.drawable.gray_background_with_12_radius

    fun getDisLikeBackground() = if (likeStatus == LikeStatus.DISLIKE)
        R.drawable.red_rose_background
    else
        R.drawable.gray_background_with_16_radius

    fun getThumbShapeIcon() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.ic_filled_thumbs_up_green
    else
        R.drawable.ic_outline_thumb_up

    @IgnoredOnParcel
    val contactOccupation = contact?.occupation ?: ""

    @IgnoredOnParcel
    val contactFullName = contact?.fullName

    @IgnoredOnParcel
    val contactImageUrl = contact?.imageUrl

    fun getScheduleUiModel(): ProgramScheduleUiModel {
        if (programScheduleUiModel == null) {
            buildScheduleUiModelFromScheduleItems(
                schedule,
                showStreaks = false,
                changeBackgroundBasedOnTheDate = false
            )
        }
        return programScheduleUiModel!!
    }

    fun reBuildScheduleUiModel() {
        buildScheduleUiModelFromScheduleItems(
            schedule,
            showStreaks = false,
            changeBackgroundBasedOnTheDate = false
        )
    }

    fun buildScheduleUiModelFromScheduleItems(
        scheduleItems: List<ScheduleItem>?,
        showStreaks: Boolean = true,
        changeBackgroundBasedOnTheDate: Boolean = true,
    ) {
        if (scheduleItems.isNullOrEmpty()) {
            programScheduleUiModel = ProgramScheduleUiModel(0, 0, 0, listOf())
        } else {
            val completedItems = scheduleItems.sumBy { it.plans.count { it.completed } }
            val totalItems = scheduleItems.sumBy { it.plans.size }
            var maxStreaks = 0
            var streaksCount = 0
            val enrolledDays = getEnrolledDays() + 1
            scheduleItems.forEach {
                val doneItems = it.plans.count { it.completed }
                val isOffDay = it.plans.isNullOrEmpty()
                if ((it.day == enrolledDays && doneItems == it.plans.size) || // isToday and complete all the items
                    (isOffDay && it.day <= enrolledDays) || // previous free day
                    (doneItems > 0 && it.day != enrolledDays) // previous or future day with any item
                ) {
                    streaksCount += 1
                } else {
                    streaksCount = 0
                }
                if (streaksCount > maxStreaks)
                    maxStreaks = streaksCount
            }
            val startDate = enrolledDate?.toCalender() ?: Calendar.getInstance()
            startDate.add(Calendar.DATE, -1)
            val programScheduleList = scheduleItems.map {
                startDate.add(Calendar.DATE, 1)
                ProgramScheduleRowUiModel(
                    startDate.time,
                    it.day,
                    changeBackgroundBasedOnTheDate,
                    it.plans
                )
            }
            programScheduleUiModel = ProgramScheduleUiModel(
                completedItems,
                totalItems,
                maxStreaks,
                programScheduleList,
                showStreaks
            )
        }
    }
}
