// TODO fix the unit test

// package com.vessel.app.common.manager
//
// import com.vessel.app.BaseTest
// import com.vessel.app.common.model.*
// import com.vessel.app.common.repo.RecommendationRepository
// import com.vessel.app.isEqualTo
// import com.vessel.app.recommendation.SampleRecommendationsFaker
// import io.mockk.coEvery
// import io.mockk.mockk
// import org.junit.jupiter.api.BeforeEach
// import org.junit.jupiter.api.Test
//
// class RecommendationManagerTest : BaseTest() {
//    private lateinit var recommendationManager: RecommendationManager
//
//    private val recommendationRepository = mockk<RecommendationRepository>()
//
//    @BeforeEach
//    fun setUp() {
//        recommendationManager = RecommendationManager(recommendationRepository)
//    }
//
//    @Test
//    fun `it should get the sample recommendations from the repository`() = runBlockingTest {
//        val data = SampleRecommendationsFaker.basic()
//        coEvery { recommendationRepository.getLatestSampleRecommendations() }
//            .answers { Result.Success(data) }
//
//        (recommendationManager.getLatestSampleRecommendations() as Result.Success<SampleRecommendations>).value isEqualTo data
//    }
//
//    // TODO fix the unit test
//    @Test
//    fun `it should get the sample food recommendations from the repository`() = runBlockingTest {
//        val data = FoodRecommendationFaker.list()
//        coEvery { recommendationRepository.getLatestSampleFoodRecommendations() }
//            .answers { Result.Success(data) }
//
//        (recommendationManager.getLatestSampleFoodRecommendations() as Result.Success<List<FoodRecommendation>>).value isEqualTo data
//    }
// }
