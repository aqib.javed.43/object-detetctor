package com.vessel.app.common.net

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.core.content.ContextCompat.getSystemService

class ConnectionChangeReceiver : BroadcastReceiver() {
    private var isConnected = false
    private var connectionChangeReceiverListener: ConnectionChangeReceiverListener? = null

    override fun onReceive(context: Context, intent: Intent) {
        isConnected = checkConnection(context)
        connectionChangeReceiverListener?.onConnectionChangeReceiverListener(isConnected)
    }

    private fun checkConnection(context: Context): Boolean {
        var result = false
        val connectivityManager = getSystemService(context, ConnectivityManager::class.java)
        connectivityManager?.let { cm ->
            val networkCapabilities = cm.activeNetwork ?: return false
            val actNw = cm.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        }
        return result
    }

    fun setConnectionChangeReceiverListener(connectionChangeReceiverListener: ConnectionChangeReceiverListener) {
        this.connectionChangeReceiverListener = connectionChangeReceiverListener
    }

    fun isConnectionEnabled() = isConnected

    interface ConnectionChangeReceiverListener {
        fun onConnectionChangeReceiverListener(isConnected: Boolean)
    }
}
