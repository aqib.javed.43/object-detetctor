package com.vessel.app.common.model

sealed class Result<out T, out E> {
    data class Success<out T>(val value: T) : Result<T, Nothing>()

    data class ServiceError(val code: Int, val error: ErrorMessage) : Result<Nothing, Nothing>()

    data class HttpError(val code: Int) : Result<Nothing, Nothing>()

    data class NetworkIOError(val throwable: Throwable) : Result<Nothing, Nothing>()

    data class UnknownError(val throwable: Throwable) : Result<Nothing, Nothing>()

    inline fun onSuccess(block: (T) -> Unit) = apply {
        if (this is Success) {
            block(value)
        }
    }

    inline fun onServiceError(block: (ServiceError) -> Unit) = apply {
        if (this is ServiceError) {
            block(this)
        }
    }

    inline fun onHttpError(block: (HttpError) -> Unit) = apply {
        if (this is HttpError) {
            block(this)
        }
    }

    inline fun onNetworkIOError(block: (NetworkIOError) -> Unit) = apply {
        if (this is NetworkIOError) {
            block(this)
        }
    }

    inline fun onUnknownError(block: (UnknownError) -> Unit) = apply {
        if (this is UnknownError) {
            block(this)
        }
    }

    inline fun onHttpOrUnknownError(block: () -> Unit) = apply {
        if (this is HttpError ||
            this is UnknownError
        ) {
            block()
        }
    }

    inline fun onServiceOrUnknownError(block: () -> Unit) = apply {
        if (this is ServiceError ||
            this is UnknownError
        ) {
            block()
        }
    }

    inline fun onError(block: (Result<Nothing, E>) -> Unit): Result<T, E> = apply {
        when (this) {
            is HttpError -> block(this)
            is NetworkIOError -> block(this)
            is UnknownError -> block(this)
            is ServiceError -> block(this)
            is Success -> return@apply
        }
    }

    inline fun <R> then(apiCall: (Success<T>) -> Result<R, *>): Result<R, *> = when (this) {
        is Success -> apiCall(this)
        is ServiceError -> this
        is HttpError -> this
        is NetworkIOError -> this
        is UnknownError -> this
    }
}
