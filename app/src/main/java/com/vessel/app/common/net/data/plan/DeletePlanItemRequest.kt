package com.vessel.app.common.net.data.plan

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class DeletePlanItemRequest(
    val plans_ids: List<Int>
)
