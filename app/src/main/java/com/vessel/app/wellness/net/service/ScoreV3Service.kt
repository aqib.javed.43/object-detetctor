package com.vessel.app.wellness.net.service

import com.vessel.app.wellness.net.data.ScoreDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ScoreV3Service {
    @GET("score/summary")
    suspend fun getScores(
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String,
        @Query("show_all") showAll: Boolean = false,
        @Query("minimal") minimal: Boolean = true,
    ): ScoreDataResponse
}
