package com.vessel.app.wellness.lockpopup

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class LockPopUpState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: LockPopUpState.() -> Unit) = apply(block)
}
