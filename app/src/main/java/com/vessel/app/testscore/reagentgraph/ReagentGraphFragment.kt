package com.vessel.app.testscore.reagentgraph

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.testscore.ui.TestScoreFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReagentGraphFragment : BaseFragment<ReagentGraphViewModel>() {
    override val viewModel: ReagentGraphViewModel by viewModels()
    override val layoutResId = R.layout.fragment_reagent_graph

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }

    override fun beforeNavigateBack() {
        findNavController().previousBackStackEntry
            ?.savedStateHandle?.apply {
                set(
                    TestScoreFragment.PREVENT_ANIMATION,
                    true
                )
            }
    }
}
