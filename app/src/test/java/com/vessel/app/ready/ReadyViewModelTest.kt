package com.vessel.app.ready

import androidx.lifecycle.Observer
import com.vessel.app.*
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.ready.ui.ReadyFragmentDirections
import com.vessel.app.util.ResourceRepository
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ReadyViewModelTest : BaseTest() {
    private val resourceRepository = mockk<ResourceRepository>()
    private val preferencesRepository = mockk<PreferencesRepository>()
    private val devicePreferencesRepository = mockk<DevicePreferencesRepository>()
    private val trackingManager = mockk<TrackingManager>()
    private val eventObserver = mockk<Observer<Unit>>()
    private val resourceProvider = mockk<ResourceRepository>()

    private lateinit var state: ReadyState

    private lateinit var viewModel: ReadyViewModel

    @BeforeEach
    fun setUp() {
        every { eventObserver.onChanged(null) } just runs
        state = ReadyState(resourceProvider)
    }

    private fun initViewModel() {
        viewModel = ReadyViewModel(state, resourceRepository, preferencesRepository, devicePreferencesRepository, trackingManager)
    }

    @Test
    fun `onStartTheTimerClicked navigates to activation timer screen`() {
        initViewModel()
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onStartTheTimerClicked()

        verify { navController.navigate(ReadyFragmentDirections.actionReadyToActivationTimer()) }
    }

    @Test
    fun `onBackButtonClicked navigates back`() {
        initViewModel()
        state.closeEvent.observeForever(eventObserver)

        viewModel.onBackClicked()

        verify { eventObserver.onChanged(null) }
    }
}
