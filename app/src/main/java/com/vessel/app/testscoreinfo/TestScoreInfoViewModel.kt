package com.vessel.app.testscoreinfo

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class TestScoreInfoViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    val homeTabsManager: HomeTabsManager,
    val buildPlanManager: BuildPlanManager,
) : BaseViewModel(resourceProvider) {
    private var neededItemsToImproveCount: Boolean? = null
    val state = TestScoreInfoState(savedStateHandle["score"] ?: "")

    private fun getNeededItemsToImproveCount(isFromClick: Boolean = false) {
        viewModelScope.launch {
            neededItemsToImproveCount =
                buildPlanManager.hasFoodPlanRecommendations() || buildPlanManager.hasStressRecommendations() || buildPlanManager.hasHydrationRecommendation()
            if (isFromClick) {
                onGotItClicked()
            }
        }
    }

    fun onGotItClicked() {
        if (preferencesRepository.testCompletedCount <= 1) {
            state.navigateToPlan.call()
        } else if (neededItemsToImproveCount == null) {
            getNeededItemsToImproveCount(true)
        } else if (neededItemsToImproveCount == true) {
            state.navigateToUpdatePlan.call()
        } else {
            homeTabsManager.clear()
            state.continueClicked.call()
        }
    }
}
