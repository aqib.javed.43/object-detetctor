package com.vessel.app.activationtimer

import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.*
import com.vessel.app.BaseFaker.Fake.FAKER
import com.vessel.app.activationtimer.ActivationTimerState.Companion.FINISH_VALUE
import com.vessel.app.activationtimer.ActivationTimerState.Companion.PROGRESS_MAX
import com.vessel.app.activationtimer.ui.ActivationTabsManager
import com.vessel.app.activationtimer.ui.ActivationTimerFragmentDirections
import com.vessel.app.common.manager.ReminderManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.ResourceRepository
import io.mockk.*
import kotlinx.coroutines.delay
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ActivationTimerViewModelTest : BaseTest() {
    companion object {
        private val MINUTES = FAKER.date().birthday().toString()
        private val SECONDS = FAKER.date().birthday().toString()
        private val PERCENT = FAKER.number().randomDouble(2, 0, 1)
    }

    private val resourceRepository: ResourceRepository = mockk()
    private val eventObserver = mockk<Observer<Unit>>()
    private lateinit var state: ActivationTimerState
    private lateinit var viewModel: ActivationTimerViewModel
    private val savedStateHandle = mockk<SavedStateHandle>()
    private val trackingManager = mockk<TrackingManager>()
    private val activationTabsManager = mockk<ActivationTabsManager>()
    private val preferencesRepository = mockk<PreferencesRepository>()
    private val reminderManager = mockk<ReminderManager>()
    private val timerStart = FAKER.date().birthday().toString()

    @BeforeEach
    fun setUp() {
        every { eventObserver.onChanged(null) } just runs

        state = ActivationTimerState()
            .apply { timerStart = timerStart }

        viewModel = ActivationTimerViewModel(savedStateHandle, state, resourceRepository, preferencesRepository, trackingManager, activationTabsManager, reminderManager)
    }

    @Test
    fun `onContinueClicked navigates to the capture screen`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onContinueClicked()

        verify { navController.navigate(ActivationTimerFragmentDirections.actionActivationTimerToRemoveDropletsFragment()) }
    }

    @Test
    fun `onSkipClicked calls the showConfirmSkipAlertEvent`() {
        state.showConfirmSkipAlertEvent.observeForever(eventObserver)

        viewModel.onSkipClicked()

        verify { eventObserver.onChanged(null) }
    }

    @Test
    fun `onSkipConfirm sets the state to completed, calls the stopTimerEvent, and navigates to the capture screen`() {
        val (_, navController) = MockProvider.mockedNavigation()
        state.stopTimerEvent.observeForever(eventObserver)

        viewModel.onSkipConfirm()

        state {
            minutes.value isEqualTo FINISH_VALUE
            seconds.value isEqualTo FINISH_VALUE
            progress.value isEqualTo PROGRESS_MAX
            title.value isEqualTo R.string.activation_timer_complete_title
            subtitle.value isEqualTo R.string.activation_timer_complete_body
            countDownCompleted.value.isTrue()
            skipButtonVisibility.value isEqualTo View.GONE
            completed.isTrue()
        }
        verify { eventObserver.onChanged(null) }
        verify { navController.navigate(ActivationTimerFragmentDirections.actionActivationTimerToRemoveDropletsFragment()) }
    }

    @Test
    fun `onUpdateText sets the minutes and seconds`() {
        viewModel.onUpdateText(MINUTES, SECONDS)

        state {
            minutes.value isEqualTo MINUTES
            seconds.value isEqualTo SECONDS
        }
    }

    @Test
    fun `onUpdateProgress sets the minutes and seconds`() {
        viewModel.onUpdateProgress(PERCENT)

        state.progress.value isEqualTo (PERCENT * PROGRESS_MAX).toInt()
    }

    @Test
    fun `onFinish sets the state to completed and calls the stopTimerEvent`() = runBlockingTest {
        state.stopTimerEvent.observeForever(eventObserver)

        viewModel.onFinish()
        delay(500)

        state {
            minutes.value isEqualTo FINISH_VALUE
            seconds.value isEqualTo FINISH_VALUE
            progress.value isEqualTo PROGRESS_MAX
            title.value isEqualTo R.string.activation_timer_complete_title
            subtitle.value isEqualTo R.string.activation_timer_complete_body
            countDownCompleted.value.isTrue()
            skipButtonVisibility.value isEqualTo View.GONE
            completed.isTrue()
        }
        verify { eventObserver.onChanged(null) }
    }

    @Test
    fun `onBackButtonClicked calls the stopTimerEvent and calls navigateBack`() {
        state.stopTimerEvent.observeForever(eventObserver)
        viewModel.navigateBack.observeForever(eventObserver)

        viewModel.onBackButtonClicked(mockk())

        verify(exactly = 2) { eventObserver.onChanged(null) }
    }
}
