package com.vessel.app.wellness.tipdeatails

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.activityreview.model.ActivityReviewItem
import com.vessel.app.activityreview.model.RecommendationReview
import com.vessel.app.activityreview.ui.ActivityReviewAdapter
import com.vessel.app.base.BaseViewModel
import com.vessel.app.checkscience.ui.OnActionHandler
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.impactgoals.ImpactGoalsAdapter
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.model.Tip
import com.vessel.app.wellness.net.data.DeleteReviewRequest
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class TipDetailsViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val recommendationManager: RecommendationManager,
    private val planManager: PlanManager,
    private val reminderManager: ReminderManager,
    private val tipMapper: TipMapper,
    private val preferencesRepository: PreferencesRepository,
    private val tipManager: TipManager,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    OnActionHandler,
    ImpactGoalsAdapter.OnActionHandler,
    ActivityReviewAdapter.OnActionHandler,
    ReminderRowAdapter.OnActionHandler {

    val state = TipDetailsState(savedStateHandle.get<Tip>("tip")!!)
    private val contactId = preferencesRepository.contactId

    init {
        state.ownItemTip.value = state.tip.value?.contact?.id == contactId
        getTipDetails()
        getTipPlans()
        getReviews()
        observeAddRemoveToPlan()
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_VIEWED)
                .addProperty(TrackingConstants.KEY_ID, state.tip.value?.id.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Tip.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun getTipPlans() {
        viewModelScope.launch {
            planManager.getUserPlans()
                .onSuccess { planData ->
                    state.plans.value = planData.filter {
                        it.isTip()
                    }.filter {
                        it.tip_id == state.tip.value?.id
                    }
                    val isAddedToPlan = state.plans.value?.isNotEmpty() ?: false
                    state.tip.value = state.tip.value?.copy(isAddedToPlan = isAddedToPlan)
                }
        }
    }
    private fun getReviews() {
        state.tip.value?.let {
            viewModelScope.launch {
                recommendationManager.getReviews(
                    it.id,
                    LikeCategory.Tip.value
                )
                    .onSuccess { response ->
                        state.reviews.value = response.reviews?.map {
                            it.copy(myContact = preferencesRepository.contactId)
                        }
                    }
            }
        }
    }
    private fun observeAddRemoveToPlan() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect { reminder ->
                if (reminder) {
                    showLoading()
                    planManager.getUserPlans(true)
                        .onSuccess { planData ->
                            state.plans.value = planData.filter {
                                it.isTip()
                            }.filter {
                                it.tip_id == state.tip.value?.id
                            }

                            val isAddedToPlan = state.plans.value?.isNotEmpty() ?: false
                            state.tip.value = state.tip.value?.copy(isAddedToPlan = isAddedToPlan)
                            hideLoading()
                        }
                }
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    private fun getTipDetails() {
        showLoading()
        viewModelScope.launch {
            tipManager.getTipDetails(state.passedTip.id)
                .onSuccess {
                    val isAddedToPlan = state.plans.value?.isNotEmpty() ?: false
                    state.tip.value = it.copy(isAddedToPlan = isAddedToPlan)
                    state.ownItemTip.value = it.contact?.id == contactId
                    hideLoading(false)
                }
        }
    }

    fun onAddToPlanClicked(tip: Tip) {
        if (tip.isAddedToPlan.not()) {
            val plans = listOf(
                PlanData(
                    tip_id = tip.id,
                    tip = tipMapper.mapTip(tip),
                    multiple = 1
                )
            )
            navigeteToReminder(tip, plans)
        } else {
            val plans = state.plans.value!!
            if (preferencesRepository.dontShowPlanRemoveConfirmationDialog) {
                deletePlanItem(plans)
            } else {
                state.removeReminder.value = plans
            }
        }
    }

    private fun navigeteToReminder(
        tip: Tip,
        plans: List<PlanData>,
    ) {
        val planReminderHeader = PlanReminderHeader(
            tip.getDisplayedTitle(),
            tip.getDisplayedFrequency(),
            tip.getBackgroundImageUrl(),
            null,
            plans,
            tip.isAddedToPlan.not()
        )
        state.navigateTo.value =
            HomeNavGraphDirections.globalActionToPlanReminderDialog(planReminderHeader)
    }

    fun deletePlanItem(plans: List<PlanData>) {
        viewModelScope.launch {
            showLoading(false)
            planManager.deletePlanItem(plans.map { it.id ?: -1 })
                .onSuccess {
                    hideLoading(false)
                    planManager.updatePlanCachedData()
                    reminderManager.updateRemindersCachedData()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(
                                TrackingConstants.KEY_ID,
                                plans.map { it.id ?: -1 }.toString()
                            )
                            .addProperty(
                                TrackingConstants.TYPE,
                                LikeCategory.Tip.value
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Tip Details")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun onLikeClicked(tip: Tip) {
        val likesCount = tip.likesCount
        val dislikesCount = tip.dislikesCount
        val newLikesCount: Int
        var newDislikesCount: Int
        if (tip.likeStatus != LikeStatus.LIKE) {
            newLikesCount = likesCount?.plus(1) ?: 0
            newDislikesCount = tip.dislikesCount ?: 0
            if (tip.likeStatus == LikeStatus.DISLIKE) newDislikesCount = newDislikesCount.minus(1)
            sendLikeStatus(tip, LikeStatus.LIKE, newLikesCount, newDislikesCount)
        } else {
            newLikesCount = likesCount?.minus(1) ?: 0
            dislikesCount?.let { unLikeStatus(tip, LikeStatus.NO_ACTION, newLikesCount, it) }
        }
    }

    fun onDisLikeClicked(tip: Tip) {
        val likesCount = tip.likesCount
        val dislikesCount = tip.dislikesCount
        val newLikesCount: Int
        val newDislikesCount: Int
        if (tip.likeStatus != LikeStatus.DISLIKE) {
            newLikesCount =
                when (tip.likeStatus) {
                    LikeStatus.NO_ACTION -> likesCount ?: 0
                    LikeStatus.LIKE -> likesCount?.minus(1) ?: 0
                    else -> return
                }
            newDislikesCount = dislikesCount?.plus(1) ?: 0
            sendLikeStatus(tip, LikeStatus.DISLIKE, newLikesCount, newDislikesCount)
        } else {
            newLikesCount = dislikesCount?.minus(1) ?: 0
            likesCount?.let { unLikeStatus(tip, LikeStatus.NO_ACTION, it, newLikesCount) }
        }
    }

    private fun sendLikeStatus(tip: Tip, likeStatus: LikeStatus, likesCount: Int, dislikesCount: Int) {
        state.tip.value = state.tip.value?.copy(likeStatus = likeStatus, likesCount = likesCount, dislikesCount = dislikesCount)
        saveLikeState(likeStatus, likesCount)
        val programId = state.plans.value?.firstOrNull {
            it.isTip() && it.tip_id == tip.id
        }?.id
        viewModelScope.launch {
            recommendationManager.sendLikesStatus(
                tip.id,
                LikeCategory.Tip.value,
                likeStatus == LikeStatus.LIKE
            )
            if (likeStatus == LikeStatus.LIKE) preferencesRepository.updateLikedPlans(programId.toString())
            else preferencesRepository.updateDislikedPlans(programId.toString())
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CONTENT_LIKED)
                .addProperty(TrackingConstants.KEY_ID, tip.id.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Tip.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun unLikeStatus(tip: Tip, likeStatus: LikeStatus, likesCount: Int, dislikesCount: Int) {
        state.tip.value = state.tip.value?.copy(likeStatus = likeStatus, likesCount = likesCount, dislikesCount = dislikesCount)
        saveLikeState(likeStatus, likesCount)
        viewModelScope.launch {
            recommendationManager.unLikesStatus(
                tip.id,
                LikeCategory.Tip.value
            )
            val programId = state.plans.value?.firstOrNull {
                it.isTip() && it.tip_id == tip.id
            }?.id
            preferencesRepository.updateUnlikedPlans(programId.toString())
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CONTENT_UNLIKED)
                .addProperty(TrackingConstants.KEY_ID, tip.id.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Tip.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    override fun onLinkButtonClick(view: View, link: String) {
        state.navigateTo.value = HomeNavGraphDirections.globalActionToWeb(link)
    }

    override fun onGoalItemClick(item: ImpactGoalModel, view: View) {}

    override fun onReminderItemClick(item: ReminderRowUiModel, view: View) {
        navigeteToReminder(state.tip.value!!, state.plans.value!!)
    }

    fun onPlanEditItemClick() {
        navigeteToReminder(state.tip.value!!, state.plans.value!!)
    }

    fun onDontShowAgainClicked(isChecked: Boolean) {
        preferencesRepository.dontShowPlanRemoveConfirmationDialog = isChecked
    }

    fun onItemLikeClicked(tip: Tip) {
        onLikeClicked(tip)
    }
    fun onExpertClicked() {
        val expertContact = state.tip.value?.contact
        if (expertContact != null) {
            state.navigateTo.value =
                HomeNavGraphDirections.globalActionToExpertProfileFragment(expertContact)
        }
    }
    private fun saveLikeState(likeStatus: LikeStatus, likesCount: Int) {
        savedStateHandle.get<Tip>("tip")?.likesCount = likesCount
        savedStateHandle.get<Tip>("tip")?.likeStatus = likeStatus
    }

    fun gotoReview() {
        val programId = state.plans.value?.firstOrNull {
            it.isTip() && it.tip_id == state.tip.value?.id
        }?.id
        val reviewItem = ActivityReviewItem()
        reviewItem.category = LikeCategory.Tip.value
        reviewItem.record_id = state.tip.value?.id
        reviewItem.likeStatus = state.tip.value?.likeStatus
        reviewItem.image = state.tip.value?.imageUrl
        reviewItem.title = state.tip.value?.title
        reviewItem.programId = programId
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalActivityReviewFragment(reviewItem)
    }

    override fun onWriteNewReview() {
        gotoReview()
    }

    override fun onDeleteReview(item: RecommendationReview, position: Int) {
        deleteReview(position)
    }

    override fun onEditReview(item: RecommendationReview) {
        gotoReview()
    }

    override fun onFlagReview(item: RecommendationReview) {
    }
    fun deleteReview(position: Int) {
        val request = DeleteReviewRequest(record_id = state.tip.value?.id, category = LikeCategory.Tip.value)
        viewModelScope.launch {
            showLoading()
            recommendationManager.deleteReview(
                request
            ).onSuccess {
                hideLoading()
                state {
                    val items = reviews.value.orEmpty().toMutableList()
                    items.removeAt(position)
                    reviews.value = items
                }
            }.onError {
                hideLoading()
                showError(it.toString())
            }
        }
    }
}
