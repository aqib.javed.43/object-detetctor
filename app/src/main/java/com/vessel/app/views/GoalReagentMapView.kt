package com.vessel.app.views

import android.content.Context
import android.graphics.PointF
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.wellness.model.ReagentItem

class GoalReagentMapView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    var onReagentDetails: ((item: ReagentItem, view: View) -> Unit)? = null
    var onGoalDetails: ((item: GoalRecord, view: View) -> Unit)? = null
    var mapVr: HashMap<ReagentItem, View> = HashMap()
    var mapVg: HashMap<GoalRecord, View> = HashMap()

    fun updateGoals(goals: List<GoalRecord>?) {
        goals?.let { it ->
            goalListView.removeAllViews()
            mapVg.clear()
            for (element in it) {
                val goal = element
                val view = GoalReagentView(context)
                val layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                layoutParams.bottomMargin = BOTTOM_MARGIN
                view.layoutParams = layoutParams
                view.updateData(goal)
                view.lblViewDetails.setOnClickListener { v ->
                    onGoalDetails?.invoke(goal, v)
                }
                view.setOnClickListener {
                    collaspAll()
                    view.extend(true)
                    Handler(
                        Looper.getMainLooper()
                    ).postDelayed(
                        {
                            drawMapLine(goal, view)
                        },
                        DRAW_DELAY
                    )
                }
                goalListView.addView(view)
                mapVg[goal] = view
            }
        }
    }

    fun updateReagents(reagents: List<ReagentItem>?) {
        reagents?.let { it ->
            reagentListView.removeAllViews()
            mapVr.clear()
            for (reagent in it) {
                val view = GoalReagentView(context)
                val layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                layoutParams.bottomMargin = BOTTOM_MARGIN
                view.layoutParams = layoutParams
                view.updateData(reagent)
                view.lblViewDetails.setOnClickListener { v ->
                    onReagentDetails?.invoke(reagent, v)
                }
                view.setOnClickListener {
                    onDrawMap(view, reagent)
                }
                reagentListView.addView(view)
                mapVr[reagent] = view
                if (it.indexOf(reagent) == it.size - 1) {
                    onDrawMap(reagentListView.getChildAt(0) as GoalReagentView, it.first())
                }
            }
        }
    }

    private fun onDrawMap(view: GoalReagentView, reagent: ReagentItem) {
        collaspAll()
        view.extend(true)
        Handler(
            Looper.getMainLooper()
        ).postDelayed(
            {
                drawMapLine(reagent, view)
            },
            DRAW_DELAY
        )
    }

    // Todo draw line reagent to goal
    fun drawMapLine(reagent: ReagentItem, view: View) {
        bezierView.drawFromEnd = false
        bezierView.startPoint = PointF(view.x, view.y + view.height * 5 / 4)
        bezierView.endPointList = checkEndPoint(reagent)
        bezierView.invalidate()
    }

    private fun checkEndPoint(reagent: ReagentItem): List<PointF> {
        val endPs: ArrayList<PointF> = arrayListOf()
        for (key in mapVg.keys) {
            val reagentItem =
                key.reagents?.firstOrNull {
                    it.id == reagent.id
                }
            if (reagentItem != null && reagentItem.impact ?: 0 < 3) {
                val view = mapVg[key]
                val pointF = PointF(view!!.x, view.y + view.height * 3 / 2)
                endPs.add(pointF)
            }
        }
        return endPs
    }

    private fun checkStartPoint(goal: GoalRecord): List<PointF> {
        val endPs: ArrayList<PointF> = arrayListOf()
        for (key in mapVr.keys) {
            val reagentItem =
                goal.reagents?.firstOrNull {
                    it.id == key.id
                }
            if (reagentItem != null && reagentItem.impact ?: 0 > 2) {
                val view = mapVr[key]
                val pointF = PointF(view!!.x, view.y + view.height * 3 / 2)
                endPs.add(pointF)
            }
        }
        return endPs
    }

    fun drawMapLine(goal: GoalRecord, view: View) {
        bezierView.drawFromEnd = true
        bezierView.endPoint = PointF(view.x, view.y + view.height * 5 / 4)
        bezierView.startPointList = checkStartPoint(goal)
        bezierView.invalidate()
    }

    fun collaspAll() {
        collaspGoals()
        collaspRegents()
    }

    private fun collaspRegents() {
        for (i in 0 until reagentListView.childCount) {
            val view = reagentListView.getChildAt(i)
            if (view is GoalReagentView) view.extend(false)
        }
    }

    private fun collaspGoals() {
        for (i in 0 until goalListView.childCount) {
            val view = goalListView.getChildAt(i)
            if (view is GoalReagentView) view.extend(false)
        }
    }

    private val reagentListView by lazy { findViewById<LinearLayout>(R.id.reagentList) }
    private val goalListView by lazy { findViewById<LinearLayout>(R.id.goalListStack) }
    private val bezierView by lazy { findViewById<BezierView>(R.id.bezierView) }

    init {
        inflate(getContext(), R.layout.goal_map_view, this)
    }

    companion object {
        const val BOTTOM_MARGIN = 30
        const val DRAW_DELAY = 300L
    }
}
