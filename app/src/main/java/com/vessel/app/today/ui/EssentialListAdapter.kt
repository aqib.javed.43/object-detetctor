package com.vessel.app.today.ui

import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.today.model.EssentialListItem
import kotlinx.android.synthetic.main.item_activity_list.view.*
import kotlinx.android.synthetic.main.item_activity_list.view.title
import kotlinx.android.synthetic.main.item_lesson_list.view.*

class EssentialListAdapter(private val handler: EssentialAdapter.OnActionHandler) : BaseAdapterWithDiffUtil<EssentialListItem>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is EssentialListItem -> R.layout.item_essential_list
        else -> throw IllegalStateException(
            "Unexpected WellnessAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }
    override fun onBindViewHolder(holder: BaseViewHolder<EssentialListItem>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as EssentialListItem
        holder.itemView.title.text = item.getTitle()
        val essentialAdapter = EssentialAdapter(handler, position)
        holder.itemView.findViewById<RecyclerView>(R.id.essentialList).apply {
            adapter = essentialAdapter
            itemAnimator = null
            setHasFixedSize(true)
        }
        essentialAdapter.submitList(item.items)
        holder.itemView.title.setOnClickListener {
            handler.showEssentialInformation()
        }
    }
}
