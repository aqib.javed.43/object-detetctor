Vitamin C is an essential vitamin that we cannot make on our own and must get from our diet. It is involved in the biosynthesis of collagen, L-carnitine, and even neurotransmitters. Healthy collagen is important for healthy skin and wound healing. Vitamin C is also a powerful antioxidant and can even regenerate other antioxidants in the body like vitamin E. As an antioxidant, it fights off free radicals, which are a common pathway of cellular aging and chronic disease.

Several studies have shown surprisingly high rates of vitamin C deficiency in healthy individuals. Many studies have recommended that the recommended dietary allowance of vitamin C be significantly increased to around 200mg/day because of the multiple health benefits of this essential vitamin.

**References:**

-   [https://pubmed.ncbi.nlm.nih.gov/10217058/](https://pubmed.ncbi.nlm.nih.gov/10217058/)
-   [https://pubmed.ncbi.nlm.nih.gov/10453176/](https://pubmed.ncbi.nlm.nih.gov/10453176/)
-   [https://pubmed.ncbi.nlm.nih.gov/8623000/](https://pubmed.ncbi.nlm.nih.gov/8623000/)
-   [https://pubmed.ncbi.nlm.nih.gov/7495230/](https://pubmed.ncbi.nlm.nih.gov/7495230/)
-   [https://pubmed.ncbi.nlm.nih.gov/11504949/](https://pubmed.ncbi.nlm.nih.gov/11504949/)
-   [https://pubmed.ncbi.nlm.nih.gov/9710847/](https://pubmed.ncbi.nlm.nih.gov/9710847/)
