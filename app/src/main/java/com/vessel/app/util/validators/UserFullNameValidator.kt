package com.vessel.app.util.validators

import androidx.lifecycle.LiveData
import com.wajahatkarim3.easyvalidation.core.view_ktx.maxLength
import com.wajahatkarim3.easyvalidation.core.view_ktx.minLength

class InvalidNameException(type: NameType) : ValidatorException("Please enter a valid ${type.value} name.")

class NameValidator(private val type: NameType) : BaseValidator<String>() {

    override fun error(): ValidatorException = InvalidNameException(type)

    override fun validate(value: LiveData<String>): Boolean {
        val name = value.value ?: throw error()
        return validate(name.trim())
    }

    override fun validate(value: String): Boolean {
        return if (
            value.minLength(2) &&
            value.maxLength(120)
        ) {
            true
        } else {
            throw error()
        }
    }
}

sealed class NameType(val value: String) {
    object First : NameType("first")
    object Last : NameType("last")
}
