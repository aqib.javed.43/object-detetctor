package com.vessel.app.onboarding.ratemaingoal

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class RateMainGoalSelectionState @Inject constructor() {
    val goal = MutableLiveData<GoalSample>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: RateMainGoalSelectionState.() -> Unit) = apply(block)
}
