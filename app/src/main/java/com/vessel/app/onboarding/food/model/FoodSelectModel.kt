package com.vessel.app.onboarding.food.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FoodSelectModel(
    val id: Int,
    val title: String,
    val image: String,
    val checked: Boolean
) : Parcelable
