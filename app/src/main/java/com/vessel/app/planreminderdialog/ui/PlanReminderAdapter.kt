package com.vessel.app.planreminderdialog.ui

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.planreminderdialog.model.ReminderItemUiModel
import com.vessel.app.views.QuantityWidget
import com.vessel.app.views.ReminderWidget
import com.vessel.app.views.weekdays.DayOfWeekUiModel
import com.vessel.app.views.weekdays.WeekDaysClickListener
import com.vessel.app.views.weekdays.WeekDaysWidget

class PlanReminderAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<ReminderItemUiModel>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is ReminderItemUiModel -> R.layout.reminder_item
        else -> throw IllegalStateException("Unexpected GoalHeaderAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any? = when (getItem(position)) {
        else -> handler
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ReminderItemUiModel>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position)
        holder.itemView.findViewById<ReminderWidget>(R.id.reminder_widget).apply {
            setHasAlarm(item.todo.notification_enabled ?: false)
            setReminderClick {
                this@PlanReminderAdapter.handler.onAlarmItemClicked(item)
            }
            setReminderTimeClick {
                this@PlanReminderAdapter.handler.onAlarmTimeClicked(item)
            }
            setDescription(item.getTimeFormattedWithMeridian().orEmpty())
        }

        holder.itemView.findViewById<QuantityWidget>(R.id.quantity_widget).apply {
            setMinQuantity(item.todo.getQuantityScale().toFloat())
            setMaxQuantity(
                item.todo.getQuantityScale().toFloat() * item.todo.getQuantityMaxMultiple()
            )
            setQuantityUnit(item.todo.getQuantityScale().toFloat())
            setQuantityDescription(item.todo.getQuantityUnit())
            setQuantity(item.todo.getQuantityScale().toFloat() * (item.todo.multiple ?: 1))
            setSubtractClickListener {
                this@PlanReminderAdapter.handler.onSubtractQuantityClicked(
                    item,
                    (it / item.todo.getQuantityScale().toFloat()).toInt()
                )
            }
            setAddClickListener {
                this@PlanReminderAdapter.handler.onAddQuantityClicked(
                    item,
                    (it / item.todo.getQuantityScale().toFloat()).toInt()
                )
            }
            isVisible = item.todo.isSupplementPlan().not() && item.isTestingPlan.not() && item.todo.isTip().not()
        }

        val weekDaysView = holder.itemView.findViewById<WeekDaysWidget>(R.id.weekDaysView)
        val unselectedDaysWarning = holder.itemView.findViewById<View>(R.id.reminderWarning)
        val programsTitle = holder.itemView.findViewById<AppCompatTextView>(R.id.programsTitle)
        if (item.todo.program != null) {
            weekDaysView.isVisible = false
            unselectedDaysWarning.isVisible = false
            programsTitle.isVisible = true
            programsTitle.text = item.todo.program.title
        } else {
            weekDaysView.isVisible = true
            programsTitle.isVisible = false
            weekDaysView.apply {
                setSelectedDays(item.dayOfWeek.orEmpty())
                unselectedDaysWarning.isVisible = item.dayOfWeek.orEmpty().isEmpty()
                onWeekDaysClickListener = object : WeekDaysClickListener {
                    override fun isNoneWeekDaysSelectedListener(isAnyWeekDaysSelected: Boolean) {
                        unselectedDaysWarning.isVisible = isAnyWeekDaysSelected
                    }

                    override fun onWeekDaysSelectedListener(days: List<DayOfWeekUiModel>) {
                        this@PlanReminderAdapter.handler.onReminderDaysSelectedClicked(item, days)
                    }
                }
            }
        }
    }

    interface OnActionHandler {
        fun onAlarmItemClicked(item: ReminderItemUiModel)
        fun onAlarmTimeClicked(item: ReminderItemUiModel)
        fun onReminderDaysSelectedClicked(item: ReminderItemUiModel, days: List<DayOfWeekUiModel>)
        fun onAddQuantityClicked(item: ReminderItemUiModel, multiple: Int)
        fun onSubtractQuantityClicked(item: ReminderItemUiModel, multiple: Int)
        fun onRemoveClicked(item: ReminderItemUiModel)
    }
}
