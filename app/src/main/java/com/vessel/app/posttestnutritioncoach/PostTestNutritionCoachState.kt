package com.vessel.app.posttestnutritioncoach

import com.vessel.app.R
import javax.inject.Inject

class PostTestNutritionCoachState @Inject constructor() {
    val infoText = listOf<Int>(
        R.string.review_your_results,
        R.string.build_your_plan,
        R.string.answer_your_questions
    )
    operator fun invoke(block: PostTestNutritionCoachState.() -> Unit) = apply(block)
}
