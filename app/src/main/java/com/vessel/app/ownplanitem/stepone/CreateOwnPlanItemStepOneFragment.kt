package com.vessel.app.ownplanitem.stepone

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateOwnPlanItemStepOneFragment : BaseFragment<CreateOwnPlanItemStepOneViewModel>() {
    override val viewModel: CreateOwnPlanItemStepOneViewModel by viewModels()
    override val layoutResId = R.layout.fragment_create_own_plan_item_step_one
    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(closeEvent) {
                activity?.finish()
            }
        }
    }
}
