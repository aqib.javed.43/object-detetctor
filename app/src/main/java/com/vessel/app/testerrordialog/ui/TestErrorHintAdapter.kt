package com.vessel.app.testerrordialog.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.testerrordialog.model.TestErrorHintModel

class TestErrorHintAdapter : BaseAdapterWithDiffUtil<TestErrorHintModel>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is TestErrorHintModel -> R.layout.item_test_error_hint
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }
}
