package com.vessel.app.onboarding.diets.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.onboarding.diets.model.OnboardingDietSelect

class OnboardingDietSelectAdapter(
    private val handler: OnActionHandler
) : BaseAdapterWithDiffUtil<OnboardingDietSelect>({ oldItem, newItem ->
    oldItem.title == newItem.title
}) {
    override fun getLayout(position: Int) = R.layout.item_onboarding_diet_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onDietSelectClicked(item: OnboardingDietSelect)
    }
}
