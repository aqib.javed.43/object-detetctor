package com.vessel.app.foodplan.ui

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R

const val FOOD_RECOMMENDATIONS_POSITION = 1

class FoodPlanItemDecoration(context: Context) : RecyclerView.ItemDecoration() {
    private val spacing3Xl by lazy { context.resources.getDimensionPixelSize(R.dimen.spacing_3xl) }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        if (parent.getChildAdapterPosition(view) == RecyclerView.NO_POSITION) return

        // reset all properties adjusted below
        outRect.left = 0
        outRect.top = 0
        outRect.right = 0
        outRect.bottom = 0

        when (position) {
            FOOD_RECOMMENDATIONS_POSITION -> outRect.top = spacing3Xl
        }
    }
}
