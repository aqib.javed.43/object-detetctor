package com.vessel.app.helper

class CaptureStrategy {
    val isPublic: Boolean = true
    val authority: String = "com.vessel.app.fileprovider"
    val directory: String? = null
}
