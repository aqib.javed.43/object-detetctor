package com.vessel.app.onboarding.lifestyle.ui

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.RecommendationManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.plan.BuildPlanRequest
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.onboarding.lifestyle.OnboardingLifestyleState
import com.vessel.app.onboarding.lifestyle.model.LifestyleSelectModel
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class OnboardingLifestyleViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    val planManager: PlanManager,
    val buildPlanManager: BuildPlanManager,
    val trackingManager: TrackingManager,
    val preferencesRepository: PreferencesRepository,
    val devicePreferencesRepository: DevicePreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,
    val recommendationsManager: RecommendationManager,
    private val homeTabsManager: HomeTabsManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    LifestyleSelectAdapter.OnActionHandler {
    val state = OnboardingLifestyleState(savedStateHandle["isLastStep"]!!)
    val isBackVisible = savedStateHandle.get<Boolean>("showBackButton")

    private val lifestyleIds = mutableListOf<Int>()

    init {
        loadLifestyleRecommendations()
    }

    private fun loadLifestyleRecommendations() {
        showLoading()
        viewModelScope.launch {
            recommendationsManager.getLatestSampleRecommendations()
                .onSuccess { sampleRecommendations ->
                    hideLoading(false)
                    state.items.value =
                        sampleRecommendations.lifestyle.filter {
                            it.lifestyle_recommendation_id != Constants.TEST_PLAN_ID &&
                                it.lifestyle_recommendation_id != Constants.HYDRATION_PLAN_ID
                        }.map { lifestyle ->
                            LifestyleSelectModel(
                                lifestyle.id ?: 0,
                                lifestyle.apiName,
                                lifestyle.image_url.orEmpty(),
                                false
                            )
                        }
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onHttpOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onNextClicked() {
        if (lifestyleIds.size != 1) {
            showError(getResString(R.string.pick_one_stress_activity))
        } else {
            showLoading()
            val buildPlanRequest = BuildPlanRequest(
                food_ids = listOf(),
                reagent_lifestyle_recommendations_ids = listOf(lifestyleIds.first()),
                supplements_ids = listOf(),
                null
            )
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.PLAN_BUILD_SUBMITTED)
                    .addProperty("data", buildPlanRequest.toString())
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
            viewModelScope.launch {
                planManager.buildPlan(buildPlanRequest)
                    .onSuccess {
                        hideLoading(false)
                        devicePreferencesRepository.showPlanUpdatedDialog = true
                        if (state.isLastStep) {
                            planManager.clearCachedData()
                            recommendationsManager.updateLatestSampleFoodRecommendationsCachedData()
                            recommendationsManager.updateLatestSampleRecommendationsCachedData()
                            homeTabsManager.clear()
                            state.navigateToHome.call()
                        } else {
                            buildPlanManager.removeReagentRecommendation(ReagentItem.Cortisol.id)
                            state.navigateTo.value =
                                OnboardingLifestyleFragmentDirections.actionOnboardingLifestyleFragmentToReagentPersonalizingFragment(false)
                        }
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onServiceOrUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }
                    .onError {
                        hideLoading()
                    }
            }
        }
    }

    override fun onLifestyleSelectClicked(item: LifestyleSelectModel) {
        if (lifestyleIds.contains(item.id)) {
            lifestyleIds.remove(item.id)
        } else {
            lifestyleIds.add(item.id)
        }
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, diet ->
                    if (diet.id == item.id) diet.copy(checked = !item.checked)
                    else diet
                }.toMutableList()
            refreshItems.value = true
        }
    }
}
