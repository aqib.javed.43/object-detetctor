package com.vessel.app.buildplan.model

// TODO handle different plans data
sealed class PlanType(val title: String) {
    object FoodPlan : PlanType("Food")
    object TIP : PlanType("Tip")
    object StressPlan : PlanType("Stress Relief")
    object SupplementPlan : PlanType("Supplement")
    object TestPlan : PlanType("Test")
    object WaterPlan : PlanType("Hydration")
    object OwnPlan : PlanType("Own")
    object MainPlan : PlanType("MainPlan")
}
