package com.vessel.app.supplementsbucket.model

import android.content.Context
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.util.extensions.formatDigits

data class SupplementHeader(
    val supplements: List<Supplement>,
    val totalPrice: Double
) {
    fun pricePerDay(context: Context): String {
        val pricePerDay = totalPrice / Constants.DAYS_IN_MONTH
        return context.getString(
            R.string.supplement_price_breakdown,
            pricePerDay.formatDigits(2)
        )
    }
}
