package com.vessel.app.common.manager

import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Todo
import com.vessel.app.common.repo.TodosRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TodosManager @Inject constructor(
    private val todosRepository: TodosRepository
) {
    private val todayTodosStateChannel = ConflatedBroadcastChannel(false)

    suspend fun getRecurringTodos(forceUpdate: Boolean = false): Result<List<Todo>, Nothing> =
        todosRepository.getRecurringTodos(forceUpdate)

    suspend fun getTodayRecurringTodos(forceUpdate: Boolean = false) =
        todosRepository.getTodayRecurringTodos(forceUpdate)

    suspend fun addRecurringTodo(
        todoApiName: String,
        todoAmount: Float = 1f,
        todoInsertDate: Date = Date(),
        todoUsdaNdbNumber: Int = 0,
        foodId: Int? = null
    ) = todosRepository.addRecurringTodo(
        Todo(
            id = -1,
            name = todoApiName,
            quantity = todoAmount,
            insertDate = todoInsertDate,
            usdaNdbNumber = todoUsdaNdbNumber,
            food_id = foodId
        )
    )

    suspend fun addRecurringTodo(todo: Todo) = todosRepository.addRecurringTodo(todo)

    suspend fun updateRecurringTodo(todo: Todo) = todosRepository.updateRecurringTodo(todo)

    suspend fun deleteRecurringTodo(todoId: Int) = todosRepository.deleteRecurringTodo(todoId)

    suspend fun updateRecurringTodoToggle(todoId: Int) =
        todosRepository.updateRecurringTodoToggle(todoId)

    fun updateRecurringTodosCachedData() {
        CoroutineScope(SupervisorJob()).launch {
            getRecurringTodos(true)
        }
    }

    fun updateTodayTodosCachedData() {
        CoroutineScope(SupervisorJob()).launch {
            if (getTodayRecurringTodos(true) is Result.Success) {
                updateTodayTodosState()
            }
        }
    }

    fun clearCachedData() {
        todosRepository.clearCachedData()
    }

    fun getTodayTodosState() = todayTodosStateChannel.asFlow()

    private fun updateTodayTodosState() {
        todayTodosStateChannel.offer(true)
    }

    fun clearTodayTodosState() {
        todayTodosStateChannel.offer(false)
    }
}
