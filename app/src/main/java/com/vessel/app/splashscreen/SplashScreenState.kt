package com.vessel.app.splashscreen

import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class SplashScreenState @Inject constructor() {
    val navigateTo = LiveEvent<NavigationDestination>()
    var appsFlyerData: MutableMap<String, String>? = null
    enum class NavigationDestination {
        WELCOME, ONBOARDING, HOME
    }
}
