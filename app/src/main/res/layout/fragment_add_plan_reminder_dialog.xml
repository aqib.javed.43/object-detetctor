<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewModel"
            type="com.vessel.app.planreminderdialog.PlanReminderViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:id="@+id/parentContainer"
        android:layout_width="match_parent"
        android:layout_height="match_parent">


        <androidx.core.widget.NestedScrollView
            android:id="@+id/nestedScrollView"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:fillViewport="true">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/container"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:background="@color/pixiGreen">

                <com.google.android.material.imageview.ShapeableImageView
                    android:id="@+id/planHeader"
                    placeholder="@{viewModel.state.planHeaderItem.background}"
                    srcFromUrl="@{viewModel.state.planHeaderItem.backgroundUrl}"
                    android:layout_width="match_parent"
                    android:layout_height="0dp"
                    android:minHeight="152dp"
                    android:scaleType="centerCrop"
                    app:layout_constraintBottom_toBottomOf="@id/planDescription"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    app:shapeAppearanceOverlay="@style/TopRoundedCornerShapeAppearance"
                    tools:src="@drawable/stress_relief_plan_cardio" />

                <androidx.appcompat.widget.AppCompatImageView
                    android:id="@+id/closeDialogButton"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/spacing_large"
                    android:layout_marginTop="@dimen/spacing_4xl"
                    android:onClick="@{() -> viewModel.onCloseDialogClicked()}"
                    android:src="@drawable/ic_back_button"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/planTitle"
                    style="@style/Title.XL"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/spacing_xl"
                    android:layout_marginTop="@dimen/spacing_4xl"
                    android:layout_marginEnd="@dimen/spacing_2xs"
                    android:text="@{viewModel.state.planHeaderItem.title}"
                    android:textColor="@color/white"
                    android:textStyle="bold"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/closeDialogButton"
                    app:layout_constraintTop_toTopOf="parent" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/planDescription"
                    style="@style/Link.small"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/spacing_small"
                    android:layout_marginEnd="@dimen/spacing_xl"
                    android:paddingBottom="@dimen/spacing_large"
                    android:text="@{viewModel.state.planHeaderItem.description}"
                    android:textAllCaps="false"
                    android:textColor="@color/white"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="@id/planTitle"
                    app:layout_constraintTop_toBottomOf="@id/planTitle" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/remindersTitle"
                    style="@style/Title.XL"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:padding="@dimen/spacing_normal"
                    android:text="@string/reminders"
                    android:textAllCaps="false"
                    android:textColor="@color/black"
                    android:textStyle="bold"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/planHeader"
                    app:visible="@{!viewModel.state.planHeaderItem.isTestingPlan}" />

                <androidx.recyclerview.widget.RecyclerView
                    android:id="@+id/reminders"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:paddingVertical="@dimen/spacing_xs"
                    android:visibility="gone"
                    app:layoutManager="androidx.recyclerview.widget.LinearLayoutManager"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/remindersTitle"
                    tools:itemCount="1"
                    tools:listitem="@layout/reminder_item"
                    tools:visibility="visible" />

                <View
                    android:id="@+id/dummyBackground"
                    android:layout_width="match_parent"
                    android:layout_height="0dp"
                    app:layout_constraintBottom_toTopOf="@id/dummyBackground"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/reminders" />

                <androidx.constraintlayout.widget.Group
                    android:id="@+id/addReminderGroup"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:visibility="gone"
                    app:constraint_referenced_ids="weekTimePicker,timeDescription,quantity_widget" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/addToPlanDescription"
                    style="@style/Title.XL"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/spacing_large"
                    android:layout_marginTop="@dimen/spacing_xl"
                    android:layout_marginEnd="@dimen/spacing_xl"
                    android:text="@string/add_another_reminder"
                    android:textAllCaps="false"
                    android:textColor="@color/black"
                    android:textSize="@dimen/font_title_l"
                    android:textStyle="bold"
                    android:visibility="gone"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/dummyBackground" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/addToPlanQuantityDescription"
                    style="@style/Title.SemiBold.XL"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/spacing_large"
                    android:layout_marginTop="@dimen/spacing_xl"
                    android:layout_marginEnd="@dimen/spacing_xl"
                    android:text="@string/how_much"
                    android:textAllCaps="false"
                    android:textColor="@color/black"
                    android:textStyle="bold"
                    android:visibility="gone"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/addToPlanDescription" />

                <com.vessel.app.views.QuantityWidget
                    android:id="@+id/quantity_widget"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_margin="@dimen/spacing_large"
                    android:visibility="gone"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/addToPlanQuantityDescription" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/timeDescription"
                    style="@style/Title.SemiBold.XL"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/spacing_large"
                    android:layout_marginTop="@dimen/spacing_large"
                    android:layout_marginEnd="@dimen/spacing_large"
                    android:text="@string/what_time"
                    android:textAllCaps="false"
                    android:textColor="@color/black"
                    android:textStyle="bold"
                    android:visibility="gone"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/quantity_widget" />

                <com.vessel.app.views.WeekTimePickerView
                    android:id="@+id/weekTimePicker"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/spacing_large"
                    android:paddingBottom="@dimen/spacing_2xl"
                    android:visibility="gone"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/timeDescription"
                    app:setOnWeekDaysClickListener="@{viewModel}"
                    app:setSelectedWeekDays="@{viewModel.state.updateReminder.dayOfWeek}"
                    app:setTime="@{viewModel.state.updateReminder.getDate()}" />

                <View
                    android:id="@+id/reminderWarningBackground"
                    android:layout_width="match_parent"
                    android:layout_height="0dp"
                    android:layout_marginHorizontal="@dimen/spacing_large"
                    android:background="@drawable/rose_background"
                    app:layout_constraintBottom_toBottomOf="@+id/reminderWarning"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="@+id/reminderWarning"
                    app:visible="@{viewModel.state.showReminderWarning}" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/reminderWarning"
                    style="@style/Filter"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/spacing_large"
                    android:layout_marginBottom="@dimen/spacing_large"
                    android:drawableStart="@drawable/ic_info_black"
                    android:drawablePadding="@dimen/spacing_2xs"
                    android:paddingVertical="@dimen/spacing_small"
                    android:text="@string/reminder_warning"
                    app:layout_constraintBottom_toTopOf="@+id/addPlanButton"
                    app:layout_constraintEnd_toEndOf="@id/reminderWarningBackground"
                    app:layout_constraintStart_toStartOf="@id/reminderWarningBackground"
                    app:layout_constraintTop_toBottomOf="@id/weekTimePicker"
                    app:visible="@{viewModel.state.showReminderWarning}" />

                <com.vessel.app.views.BlackRoundedButtonGroup
                    android:id="@+id/addPlanButton"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/spacing_large"
                    android:layout_marginTop="@dimen/spacing_2xl"
                    android:onClick="@{() -> viewModel.onSetRemindersClicked()}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/reminderWarning"
                    app:visible="@{!viewModel.state.showReminderWarning}"
                    tools:background="@color/blackAlpha70" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/skipThis"
                    style="@style/Link.Large"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/spacing_normal"
                    android:layout_marginBottom="@dimen/spacing_normal"
                    android:clickable="true"
                    android:focusable="true"
                    android:onClick="@{() -> viewModel.onSkipThisClicked()}"
                    android:text="@string/skip_this"
                    android:textColor="@color/greenishGray"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintEnd_toEndOf="@+id/addPlanButton"
                    app:layout_constraintStart_toStartOf="@+id/addPlanButton"
                    app:layout_constraintTop_toBottomOf="@+id/addPlanButton"
                    tools:text="Skip this" />
            </androidx.constraintlayout.widget.ConstraintLayout>
        </androidx.core.widget.NestedScrollView>

        <include
            android:id="@+id/loadingLayout"
            layout="@layout/layout_full_screen_loading_transparent"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:visible="@{viewModel.isLoading}"
            tools:visibility="visible" />

    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
