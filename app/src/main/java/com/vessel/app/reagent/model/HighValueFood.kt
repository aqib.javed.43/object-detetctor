package com.vessel.app.reagent.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HighValueFood(
    @Json(name = "ndB_No")
    val ndBNo: String? = null,
    val b7: Double? = null,
    val b9: Long? = null,
    val vitaminC: Double? = null,
    val calcium: Double? = null,
    val magnesium: Long? = null,
    @Json(name = "imageUrl")
    val imageURL: String? = null,
    val reagentName: String? = null,
    val amount: String? = null,
    val name: String? = null
) {
    fun getFoodNutrientsText(): String {
        return "• $amount mg of\n$reagentName"
    }
}
