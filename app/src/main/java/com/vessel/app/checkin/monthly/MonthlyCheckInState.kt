package com.vessel.app.checkin.monthly

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class MonthlyCheckInState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val progress = LiveEvent<Int>()
    val status = LiveEvent<String>()
    operator fun invoke(block: MonthlyCheckInState.() -> Unit) = apply(block)
}
