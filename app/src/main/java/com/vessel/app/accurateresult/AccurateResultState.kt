package com.vessel.app.accurateresult

import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class AccurateResultState @Inject constructor() {
    val navigateToCapture = LiveEvent<Unit>()

    operator fun invoke(block: AccurateResultState.() -> Unit) = apply(block)
}
