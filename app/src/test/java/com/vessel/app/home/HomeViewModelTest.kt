package com.vessel.app.home

import androidx.lifecycle.Observer
import com.vessel.app.BaseFaker.Fake.FAKER
import com.vessel.app.BaseTest
import com.vessel.app.R
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.ReminderManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import org.junit.jupiter.api.BeforeEach

class HomeViewModelTest : BaseTest() {

    private lateinit var viewModel: HomeViewModel

    private val state = HomeState()

    private val resourceRepository = mockk<ResourceRepository>()
    private val preferencesRepository = mockk<PreferencesRepository>()
    private val remoteConfiguration = mockk<RemoteConfiguration>()
    private val trackingManager = mockk<TrackingManager>()
    private val reminderManager = mockk<ReminderManager>()
    private val eventObserver = mockk<Observer<Unit>>()

    private val authManager = mockk<AuthManager>()
    private val versionNumber = FAKER.chuckNorris().fact()

    @BeforeEach
    fun setUp() {
        every { eventObserver.onChanged(null) } just runs
        every { resourceRepository.getString(eq(R.string.version_number), any<String>()) }
            .answers { versionNumber }

        viewModel = HomeViewModel(state, remoteConfiguration, resourceRepository, preferencesRepository, reminderManager, trackingManager)
    }

    // removed from homeViewModel with the new navigation feature to Morefragmnet
}
