package com.vessel.app.ownplanitem

import android.os.Bundle
import androidx.activity.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseActivity
import com.vessel.app.util.extensions.applyFullscreenFlags
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateOwnPlanItemActivity : BaseActivity<CreateOwnPlanItemViewModel>() {
    override val viewModel: CreateOwnPlanItemViewModel by viewModels()
    override val layoutResId = R.layout.activity_create_own_plan_item

    override fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        applyFullscreenFlags()
    }
}
