package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ReviewRequest(
    var category: String?,
    var record_id: Int?,
    var review: String?
)

@JsonClass(generateAdapter = true)
data class DeleteReviewRequest(
    var category: String?,
    var record_id: Int?
)
