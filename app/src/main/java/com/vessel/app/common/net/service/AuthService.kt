package com.vessel.app.common.net.service

import com.vessel.app.common.net.TokenAuthenticator.Companion.AUTHORIZATION_HEADER
import com.vessel.app.common.net.data.AuthData
import com.vessel.app.common.net.data.MultiPassData
import com.vessel.app.common.net.payload.ForgotPasswordPayload
import com.vessel.app.common.net.payload.LoginPayload
import com.vessel.app.common.net.payload.MultiPassPayload
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthService {
    @POST("auth/login")
    suspend fun login(@Body payload: LoginPayload): AuthData

    @POST("auth/refresh-token")
    suspend fun refreshToken(@Header(AUTHORIZATION_HEADER) refreshToken: String): AuthData

    @POST("auth/forgot-password")
    suspend fun forgotPassword(@Body payload: ForgotPasswordPayload)

    @POST("auth/multipass")
    suspend fun multipass(@Header(AUTHORIZATION_HEADER) refreshToken: String, @Body payload: MultiPassPayload): MultiPassData
}
