package com.vessel.app.privacy

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class PrivacyViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {
    fun onContinueClicked(view: View) {
        view.findNavController()
            .navigate(R.id.onboardingFragment)
    }
}
