package com.vessel.app.today.notice

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class NoticeViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    @Assisted savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()

    fun onGotIt() {
        dismissDialog.call()
    }
}
