package com.vessel.app.util

import android.app.Application
import com.bugsee.library.Bugsee
import com.bugsee.library.LaunchOptions
import com.vessel.app.BuildConfig

object BugseeUtils {

    fun launch(application: Application) {
        val options = LaunchOptions()
        options.General.isCrashReport = false
        Bugsee.launch(application, BuildConfig.BUGSEE_APP_TOKEN, options)
    }
}
