package com.vessel.app.tooltipscore.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.wellness.model.ReagentItem

class ReagentScoreAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<ReagentItem>(
    { oldItem, newItem ->
        oldItem.id == newItem.id
    }
) {
    override fun getLayout(position: Int) = R.layout.item_reagent_tooltip
    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onReagentSelected(item: ReagentItem)
    }
}
