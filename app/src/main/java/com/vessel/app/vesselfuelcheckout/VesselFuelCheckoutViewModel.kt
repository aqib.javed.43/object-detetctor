package com.vessel.app.vesselfuelcheckout

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.SupplementsManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.supplement.model.MembershipAddress
import com.vessel.app.supplement.model.paymentType
import com.vessel.app.util.ResourceRepository
import com.vessel.app.vesselfuelcheckout.model.VesselFuelItemUiModel
import com.vessel.app.vesselfuelcheckout.model.request.toVesselFuelModel
import kotlinx.coroutines.launch

class VesselFuelCheckoutViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceRepository: ResourceRepository,
    private val supplementsManager: SupplementsManager,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository) {
    val state = VesselFuelCheckoutState(savedStateHandle["items"]!!)
    private lateinit var addressInformation: MembershipAddress

    init {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.APP_SUPPLEMENTS_CHECKOUT_SHOWN)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        loadMembershipData()
    }

    private fun loadMembershipData() {
        showLoading()
        viewModelScope.launch {
            val addressResponse = supplementsManager.getMembershipsAddresses()
            val paymentResponse = supplementsManager.getMembershipsPayment()
            hideLoading(false)
            if (addressResponse is Result.Success && paymentResponse is Result.Success) {
                val paymentData = paymentResponse.value.first()
                val addressData = addressResponse.value.first()
                addressInformation = addressData

                state.addressData.postValue(addressData.getAddressInformation())
                if (paymentData.paymentType != null) {
                    state.paymentOptionTitle.value = when (paymentData.paymentType) {
                        paymentType.PAYPAL -> getResString(R.string.paypal)
                        paymentType.GOOGLE -> getResString(R.string.google_pay)
                        paymentType.APPLE -> getResString(R.string.apple_pay)
                    }
                    state.paymentOptionIcon.value = when (paymentData.paymentType) {
                        paymentType.PAYPAL -> R.drawable.ic_paypal
                        paymentType.GOOGLE -> R.drawable.ic_google_pay
                        paymentType.APPLE -> R.drawable.ic_apple_pay
                    }
                }
                state.paymentItems.postValue(
                    listOf(
                        VesselFuelItemUiModel(
                            getResString(R.string.visa_ending_in),
                            paymentData.card_last4.toString(),
                            0.0
                        ),
                        VesselFuelItemUiModel(
                            getResString(R.string.expiration_date),
                            "${paymentData.card_exp_month}/${paymentData.card_exp_year}",
                            0.0
                        )
                    )
                )
            }
        }
    }

    fun onEditSupplementClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.APP_SUPPLEMENTS_CHECKOUT_EDIT)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        navigateBack()
    }

    fun onEditAddressClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.APP_SUPPLEMENTS_CHECKOUT_EDIT_ADDERSS)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateToWeb.value = state.checkoutUrl
    }

    fun onEditPaymentClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.APP_SUPPLEMENTS_CHECKOUT_EDIT_PAYMENT)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateToWeb.value = state.checkoutUrl
    }

    fun onCheckoutClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.APP_SUPPLEMENTS_CHECKOUT_ORDER)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        val supplementItems = state.fuelItems.items.map { it.toVesselFuelModel() }
        val addressId = addressInformation.id
        showLoading()
        viewModelScope.launch {
            supplementsManager.checkoutVesselFuel(addressId, supplementItems)
                .onSuccess {
                    hideLoading()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.APP_SUPPLEMENTS_CHECKOUT_SUCCESS)
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    supplementsManager.updateVesselFuelCheckoutSuccess()
                    state.checkoutSuccessEvent.call()
                }
                .onServiceError {
                    val errorMessage = it.error.message ?: getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage, it.code.toString())
                    showError(errorMessage)
                }
                .onNetworkIOError {
                    val errorMessage = getResString(R.string.network_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onUnknownError {
                    val errorMessage = getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun logFailureEvent(errorMessage: String, errorCode: String = "") {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.APP_SUPPLEMENTS_CHECKOUT_FAILED)
                .addProperty(TrackingConstants.KEY_ERROR_CODE, errorCode)
                .addProperty(TrackingConstants.KEY_ERROR, errorMessage)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun onCancelClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.APP_SUPPLEMENTS_CHECKOUT_CANCEL)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.cancelCheckoutClicked.call()
    }
}
