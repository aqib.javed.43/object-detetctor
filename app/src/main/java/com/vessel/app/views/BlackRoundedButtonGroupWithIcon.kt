package com.vessel.app.views

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import com.vessel.app.common.binding.setTextSize
import kotlinx.android.synthetic.main.rounded_button_layout_with_icon.view.*

open class BlackRoundedButtonGroupWithIcon @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.rounded_button_layout_with_icon, this)
    }

    fun setText(text: String) {
        button.text = text
    }

    fun setImageResource(@DrawableRes iconRes: Int?) {
        iconRes?.let { icon.setImageResource(it) } ?: run { icon.setImageResource(R.drawable.ic_3dot_arrow_black) }
    }
    fun setImageResource(iconRes: Drawable?) {
        icon.setImageDrawable(iconRes)
    }
    fun setSmallText(smallText: Boolean) {
        if (smallText) {
            button.setTextSize(R.dimen.font_xs)
        }
    }

    fun setText(@StringRes stringResId: Int?) {
        stringResId?.let { button.setText(it) } ?: run { button.text = null }
    }

    fun setHint(hint: Int?) {
        hint?.let { button.text = hint.toString() } ?: run { button.text = null }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        buttonBg.isEnabled = enabled
    }

    override fun setOnClickListener(l: OnClickListener?) {
        buttonBg.setOnClickListener(l)
    }
}
