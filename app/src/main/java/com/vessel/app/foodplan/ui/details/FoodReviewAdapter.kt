package com.vessel.app.foodplan.ui.details

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.activityreview.ui.ActivityReviewAdapter
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodplan.model.FoodReviewItem
import kotlinx.android.synthetic.main.item_activity_review.view.*

class FoodReviewAdapter(private val handler: ActivityReviewAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<FoodReviewItem>() {
    override fun getLayout(position: Int) = R.layout.layout_review

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<FoodReviewItem>, position: Int) {
        val item = getItem(position)
        holder.itemView.findViewById<View>(R.id.btnWriteReview).setOnClickListener {
            handler.onWriteNewReview()
        }
        holder.itemView.findViewById<TextView>(R.id.memberReviewTitle).let {
            it.text = item.title
        }
        val activityReviewAdapter = ActivityReviewAdapter(handler)
        holder.itemView.findViewById<RecyclerView>(R.id.reviewList).apply {
            adapter = activityReviewAdapter
            setHasFixedSize(true)
        }
        activityReviewAdapter.submitList(item.items)
        super.onBindViewHolder(holder, position)
    }
}
