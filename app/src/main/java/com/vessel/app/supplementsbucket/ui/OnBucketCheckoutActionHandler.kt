package com.vessel.app.supplementsbucket.ui

import android.view.View

interface OnBucketCheckoutActionHandler {
    fun onCheckoutClicked(view: View)
}
