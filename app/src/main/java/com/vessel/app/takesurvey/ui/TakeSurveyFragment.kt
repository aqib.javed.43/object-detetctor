package com.vessel.app.takesurvey.ui

import android.os.Bundle
import android.widget.SeekBar
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.vessel.app.R
import com.vessel.app.activationtimer.util.TestSurveyStates
import com.vessel.app.activationtimer.util.TimerServiceManager
import com.vessel.app.base.BaseFragment
import com.vessel.app.takesurvey.TakeSurveyViewModel
import com.vessel.app.taketest.TakeTestViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TakeSurveyFragment : BaseFragment<TakeSurveyViewModel>() {

    override val viewModel: TakeSurveyViewModel by viewModels()
    override val layoutResId = R.layout.fragment_take_survey
    private val takeTestViewModel by lazy { parentViewModel<TakeTestViewModel>() }

    private val questionRecyclerViewAdapter by lazy { QuestionRecyclerViewAdapter(viewModel) }
    lateinit var questionsPager: ViewPager2
    override fun onViewLoad(savedInstanceState: Bundle?) {
        requireView().findViewById<SeekBar>(R.id.progressBar).isEnabled = false
        setupSurveyQuestions()
        setupObservers()
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            viewModel.submitAnswers(TestSurveyStates.SKIPPED)
        }
        viewModel.state.uuid = takeTestViewModel.uuid
    }

    private fun setupSurveyQuestions() {
        questionsPager = requireView().findViewById<ViewPager2>(R.id.questions)
        questionsPager.let {
            it.adapter = questionRecyclerViewAdapter
        }
        questionsPager.isUserInputEnabled = false
        questionRecyclerViewAdapter.submitList(viewModel.state.questions.toList())
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateToIndex) {
                questionsPager.currentItem = it
            }
            observe(finishSurvey) {
                navigation()
            }
            observe(skipSurvey) {
                navigation()
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }

    private fun navigation() {
        if (viewModel.testErrors.isNullOrEmpty()) {
            findNavController().navigate(TakeSurveyFragmentDirections.actionTakeSurveyToNewTestTimerFragment())
        } else {
            findNavController().navigate(TakeSurveyFragmentDirections.actionTakeSurveyToLFAErrorFragment(viewModel.testErrors.toTypedArray()))
        }
    }

    override fun onResume() {
        super.onResume()
        setupTimerHelper()
    }

    private fun setupTimerHelper() {
        TimerServiceManager.setHandler(viewModel)
        TimerServiceManager.stopForeground()
    }
}
