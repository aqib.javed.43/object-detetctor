package com.vessel.app.mediapostpreview

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.teampage.model.PostType
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_create_new_post.*

@AndroidEntryPoint
class MediaPostPreviewFragment : BaseFragment<MediaPostPreviewViewModel>() {
    override val viewModel: MediaPostPreviewViewModel by viewModels()
    override val layoutResId = R.layout.fragment_preview_media_post

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        observeValues()
        setupBackPressedDispatcher()
    }

    private fun setupViews() {
        val postType = arguments?.getInt(Constants.POST_TYPE)
        val title = when (postType) {
            PostType.PHOTO.ordinal -> "Post your Photo?"
            PostType.VIDEO.ordinal -> "Post your Video?"
            else -> "Create a text post?"
        }
        lblTitle.text = title
    }

    private fun observeValues() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }

    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.let { viewModel.onBackButtonClicked(it) }
        }
    }
}
