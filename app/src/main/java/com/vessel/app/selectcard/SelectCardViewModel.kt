package com.vessel.app.selectcard

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.selectcard.ui.SelectCardFragmentDirections
import com.vessel.app.util.ResourceRepository

class SelectCardViewModel @ViewModelInject constructor(
    val state: SelectCardState,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {
    fun continueClicked(view: View) {
        view.findNavController()
            .navigate(SelectCardFragmentDirections.actionSelectCardToEducation())
    }
}
