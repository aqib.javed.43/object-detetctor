package com.vessel.app.onboarding.finalonboarding.stepone

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository

class OnBoardingFinalStepOneViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider),
    ToolbarHandler {
    val bgColor = if (preferencesRepository.isExistingUserFirstTime) R.color.beige_color else R.color.primaryLight
    val state = OnBoardingFinalStepOneState()
    var title = ""
    init {
        val mainGoal = preferencesRepository.getUserMainGoal()
        mainGoal?.let {
            title = String.format(getResString(R.string.step_one_notice), it.name)
        }
    }
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        state.navigateTo.value = OnBoardingFinalStepOneFragmentDirections.actionOnBoardingFinalStepOneFragmentToOnBoardingFinalStepTwoFragment()
    }
}
