package com.vessel.app.common.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.vessel.app.R
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
data class LessonResponse(
    val pagination: Pagination? = null,
    val lessons: List<Lesson>? = null
)
@JsonClass(generateAdapter = true)
@Parcelize
data class Lesson(
    val title: String? = null,

    @Json(name = "internal_title")
    val internalTitle: String? = null,

    @Json(name = "question_count")
    val questionCount: Long? = null,

    val curriculums: List<Curriculum>? = null,

    @Json(name = "updated_at")
    val updatedAt: String? = null,

    val description: String? = null,
    val day: Long? = null,

    @Json(name = "updated_by")
    val updatedBy: Long? = null,

    val duration: Long? = null,

    @Json(name = "created_at")
    val createdAt: String? = null,

    @Json(name = "question_ids")
    val questionIDS: List<Long>? = null,

    val id: Long? = null,
    var completed: Boolean = false,
    val questions: List<Question>? = null,

    @Json(name = "insert_date")
    val insertDate: String? = null,

    @Json(name = "image_url")
    val imageURL: String? = null,
    var programId: Int? = null,
    var isUnlockedLesson: Boolean? = false,
    var programName: String? = null
) : Parcelable {
    fun getDurationTime(): String {
        return "~ $duration mins"
    }
}

@JsonClass(generateAdapter = true)
@Parcelize
data class Curriculum(
    @Json(name = "curriculum_id")
    val curriculumID: Long? = null,

    @Json(name = "lesson_id")
    val lessonID: Long? = null,

    @Json(name = "updated_at")
    val updatedAt: String? = null,

    val day: Long? = null,

    @Json(name = "updated_by")
    val updatedBy: Long? = null,

    @Json(name = "created_at")
    val createdAt: String? = null,

    @Json(name = "curriculum_lesson_id")
    val curriculumLessonID: Long? = null,

    @Json(name = "lesson_sequence")
    val lessonSequence: Long? = null
) : Parcelable
@JsonClass(generateAdapter = true)
@Parcelize
data class Question(
    val answers: List<Answer>? = null,

    @Json(name = "tip_text")
    val tipText: String? = null,

    @Json(name = "internal_title")
    val internalTitle: String? = null,

    @Json(name = "success_text")
    val successText: String? = null,

    @Json(name = "updated_at")
    val updatedAt: String? = null,

    @Json(name = "placeholder_text")
    val placeholderText: String? = null,

    val image: String? = null,

    @Json(name = "tip_image")
    val tipImage: String? = null,

    @Json(name = "updated_by")
    val updatedBy: Long? = null,

    @Json(name = "success_heading")
    val successHeading: String? = null,

    @Json(name = "created_at")
    val createdAt: String? = null,

    val id: Long? = null,

    val text: String? = null,

    @Json(name = "is_skippable")
    val isSkippable: Boolean? = null,

    val type: String?
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
data class AnswerModel(
    var chained_survey_id: Int? = null,
    var updated_by: Int? = null,
    var is_incorrect: Boolean? = null,
    var created_at: String? = null,
    var primary_text: String? = null,
    var id: Int? = null,
    var secondary_text: String? = null,
    var image: String? = null,
    var updated_at: String? = null
) : Parcelable
@JsonClass(generateAdapter = true)
@Parcelize
data class Answer(
    var updated_by: Int? = null,
    var answer_id: Int? = null,
    var created_at: String? = null,
    var answer_sequence: Int? = null,
    var updated_at: String? = null,
    var isSelected: Boolean? = false,
    var isChecked: Boolean? = false,
    var answer: AnswerModel?
) : Parcelable {
    @DrawableRes
    val background: Int = if (isSelected == false) {
        R.drawable.white_rounded_background
    } else {
        when {
            answer?.is_incorrect == false -> {
                R.drawable.green_rounded_background
            }
            isChecked == false -> {
                R.drawable.white_rounded_background
            }
            isChecked == true && answer?.is_incorrect != false -> {
                R.drawable.red_rounded_background
            }
            else -> {
                R.drawable.green_rounded_background
            }
        }
    }
}

@JsonClass(generateAdapter = true)
@Parcelize
data class Pagination(
    val page: Long? = null,
    val total: Long? = null,
    val pages: Long? = null,
    val perPage: Long? = null
) : Parcelable

@JsonClass(generateAdapter = true)
data class CompleteAnswer(
    var lesson_id: Int? = null,
    var program_id: Int? = null,
    var questions: List<AnswerParams> = arrayListOf()
)
@JsonClass(generateAdapter = true)
data class AnswerParams(
    var answer_text: String? = null,
    var question_id: Int? = null,
    var answer_id: Int? = null,
    var day: Int? = null,
    var question_read: Boolean? = null,
)
