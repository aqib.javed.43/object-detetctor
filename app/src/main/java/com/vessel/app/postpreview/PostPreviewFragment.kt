package com.vessel.app.postpreview

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostPreviewFragment : BaseFragment<PostPreviewViewModel>() {
    override val viewModel: PostPreviewViewModel by viewModels()
    override val layoutResId = R.layout.fragment_preview_post

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observeValues()
        setupBackPressedDispatcher()
    }

    private fun observeValues() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }

    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.let { viewModel.onBackButtonClicked(it) }
        }
    }
}
