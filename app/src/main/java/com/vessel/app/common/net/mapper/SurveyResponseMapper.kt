package com.vessel.app.common.net.mapper

import com.vessel.app.common.net.data.*
import com.vessel.app.takesurvey.model.SurveyResponseAnswer
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SurveyResponseMapper @Inject constructor() {
    fun map(data: List<SurveyResponseAnswer>) = SurveyResponseData(data.map { map(it) })

    private fun map(data: SurveyResponseAnswer) = SurveyResponseAnswerData(
        question_id = data.questionId,
        answer_id = data.answerId,
        answer_text = data.answerText
    )
}
