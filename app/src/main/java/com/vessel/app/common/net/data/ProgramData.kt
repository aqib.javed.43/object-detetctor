package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import com.vessel.app.common.model.Essential
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.common.net.data.plan.PlanData
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ProgramData(
    val id: Int?,
    val title: String?,
    val like_status: String?,
    val reviewed_contact_ids: List<Int>?,
    val difficulty: String?,
    val contact_id: Int?,
    val is_enrolled: Boolean?,
    val image_url: String?,
    val duration_days: Int?,
    val total_likes: Int?,
    val dislikes_count: Int? = null,
    val main_goal_id: Int?,
    val description: String?,
    val contact: ContactData?,
    val created_at: String?,
    val impactsGoals: List<GoalRecord>?,
    val sources: List<ScienceSourceData>?,
    val plans: List<PlanData>?,
    val enrolled_date: String?,
    val frequency: Int?,
    val schedule: List<ScheduleProgramData>?,
    val essentials: List<Essential>? = null
) : Parcelable {
    fun getEssentialList(): List<Essential>? {
        essentials?.let {
            for (es in it) {
                es.programTitle = "${title?.replace("Program","")} Essentials"
            }
        }
        return essentials
    }
}
