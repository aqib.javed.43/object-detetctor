package com.vessel.app.common.manager

import com.pushwoosh.Pushwoosh
import com.vessel.app.common.database.VesselDatabase
import com.vessel.app.common.model.AuthToken
import com.vessel.app.common.model.Contact
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Result.*
import com.vessel.app.common.net.HttpException
import com.vessel.app.common.net.HttpStatusCode
import com.vessel.app.common.net.IncorrectAuthException
import com.vessel.app.common.repo.AuthRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.usecase.PrefetchUseCase
import com.vessel.app.homescreen.HomeTabsManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.withContext
import zendesk.core.JwtIdentity
import zendesk.core.Zendesk

class AuthManager(
    private val authRepository: AuthRepository,
    private val preferencesRepository: PreferencesRepository,
    private val contactManager: ContactManager,
    private val vesselDatabase: VesselDatabase,
    private val prefetchUseCase: PrefetchUseCase,
    private val homeTabsManager: HomeTabsManager,
    private val programManager: ProgramManager
) {
    private var authToken: AuthToken? = null

    private val authStateChannel = BroadcastChannel<Boolean>(1)

    suspend fun onAppleGoogleAuth(authToken: AuthToken): Result<Contact, Nothing> {
        saveAuthToken(authToken)
        return contactManager.getContact()
    }

    suspend fun login(email: String, password: String) = authRepository.login(email, password).let {
        when (it) {
            is Success -> {
                onAppleGoogleAuth(it.value)
            }
            else -> it
        }
    }

    suspend fun refreshToken(): Result<Any, Nothing> {
        if (getAuthToken().accessToken == null || getAuthToken().refreshToken == null) {
            logout()
            return UnknownError(IncorrectAuthException())
        }

        return authRepository.refreshToken(getAuthToken().refreshToken!!).let {
            when (it) {
                is Success -> {
                    // refresh token api call does not return a new refresh token so use existing one
                    val token = if (it.value.refreshToken == null) {
                        it.value.copy(refreshToken = preferencesRepository.refreshToken)
                    } else {
                        it.value
                    }

                    saveAuthToken(token)
                    Success(Unit)
                }
                is HttpError -> when (it.code) {
                    HttpStatusCode.UNAUTHORIZED,
                    HttpStatusCode.UNPROCESSABLE_ENTITY -> {
                        logout()
                        UnknownError(IncorrectAuthException())
                    }
                    else -> UnknownError(HttpException(it.code))
                }
                else -> it
            }
        }
    }

    suspend fun forgotPassword(email: String) = authRepository.forgotPassword(email)

    suspend fun multipass(body: String) =
        authRepository.multipass(getAuthToken().accessToken!!, body)

    fun isAuthenticated() = getAuthToken().run {
        !accessToken.isNullOrEmpty() && !refreshToken.isNullOrEmpty()
    }

    fun getAuthToken() = authToken ?: run {
        AuthToken(
            accessToken = preferencesRepository.accessToken,
            refreshToken = preferencesRepository.refreshToken
        )
    }.also { authToken = it }

    fun saveAuthToken(authToken: AuthToken) {
        this.authToken = authToken
        preferencesRepository.accessToken = authToken.accessToken
        preferencesRepository.refreshToken = authToken.refreshToken
        updateAuthState()
    }

    fun getAuthState() = authStateChannel.asFlow().distinctUntilChanged()

    private fun updateAuthState() {
        authStateChannel.offer(isAuthenticated())
    }

    suspend fun logout() {
        authToken = null

        Zendesk.INSTANCE.setIdentity(JwtIdentity(null))
        homeTabsManager.clear()
        programManager.clear()
        preferencesRepository.clear()
        prefetchUseCase.clearCachedData()
        Pushwoosh.getInstance().unregisterForPushNotifications()
        withContext(Dispatchers.IO) {
            vesselDatabase.clearAllTables()
            updateAuthState()
        }
    }
}
