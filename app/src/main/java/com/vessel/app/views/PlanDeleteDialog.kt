package com.vessel.app.views

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import com.vessel.app.R
import com.vessel.app.databinding.PlanReminderRemoveDialogBinding

class PlanDeleteDialog(
    private val activity: FragmentActivity,
    private val title: String,
    private val cancelActionCallback: (() -> Unit)?,
    private val confirmActionCallback: (() -> Unit)?,
    private val dontShowAgainActionCallback: ((Boolean) -> Unit)?,
) {
    private val dialog: AlertDialog = AlertDialog.Builder(activity).create()
    private lateinit var binding: PlanReminderRemoveDialogBinding

    init {
        setupDialogView()
    }

    private fun setupDialogView() {
        val layoutInflater = LayoutInflater.from(activity)
        binding = PlanReminderRemoveDialogBinding.inflate(layoutInflater, null, false)
        dialog.setView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.BOTTOM)
        dialog.setCancelable(true)
        binding.description.text = activity.getString(R.string.remove_all_plans_confirmation, title)
        setupActionClick()
    }

    private fun setupActionClick() {
        binding.closeDialogButton.setOnClickListener {
            hideDialog()
        }
        binding.checkbox.setOnCheckedChangeListener { _, isChecked ->
            dontShowAgainActionCallback?.invoke(isChecked)
        }

        binding.removeButton.setOnClickListener {
            confirmActionCallback?.invoke()
            hideDialog()
        }
        binding.keepButton.setOnClickListener {
            cancelActionCallback?.invoke()
            hideDialog()
        }
    }

    fun showDialog() {
        dialog.show()
    }

    private fun hideDialog() {
        dialog.dismiss()
    }

    companion object {
        fun showDialog(
            activity: FragmentActivity,
            title: String,
            cancelActionCallback: (() -> Unit)?,
            confirmActionCallback: (() -> Unit)?,
            dontShowAgainActionCallback: ((Boolean) -> Unit)?,
        ) {
            PlanDeleteDialog(
                activity,
                title,
                cancelActionCallback,
                confirmActionCallback,
                dontShowAgainActionCallback,
            ).showDialog()
        }
    }
}
