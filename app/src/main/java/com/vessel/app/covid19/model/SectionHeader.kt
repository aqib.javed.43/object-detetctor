package com.vessel.app.covid19.model

import androidx.annotation.StringRes

data class SectionHeader(
    @StringRes val text: Int
)
