package com.vessel.app.supplement

import android.text.SpannableString
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.BuildConfig
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.common.adapter.planreminders.toReminderList
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Result
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.supplement.model.Membership
import com.vessel.app.supplement.model.MembershipProperties
import com.vessel.app.supplement.model.SupplementFormula
import com.vessel.app.supplement.ui.SupplementAdapter
import com.vessel.app.supplement.ui.SupplementFragmentDirections
import com.vessel.app.supplementsbucket.ui.descriptionpopup.OnDescriptionActionHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.makeClickableSpan
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ViewModel @ViewModelInject constructor(
    val state: SupplementState,
    resourceProvider: ResourceRepository,
    private val recommendationManager: RecommendationManager,
    private val planManager: PlanManager,
    private val authManager: AuthManager,
    private val supplementsManager: SupplementsManager,
    private val reminderManager: ReminderManager,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceProvider),
    SupplementAdapter.OnActionHandler,
    ToolbarHandler,
    OnDescriptionActionHandler,
    ReminderRowAdapter.OnActionHandler {
    private var itemTobeAdded: SupplementFormula? = null
    private var todos = mutableListOf<PlanData>()
    private var hasNewMemberships = false
    val descriptionPopupBottomMargin =
        resourceProvider.getDimension(R.dimen.supplement_description_bottom_margin).toInt()

    init {
        observeVesselFuelSuccessState()
        observeRemindersState()
        loadSupplements()
    }
    private fun observeVesselFuelSuccessState() {
        viewModelScope.launch {
            supplementsManager.getVesselFuelCheckoutSuccess().collect {
                if (it) {
                    hasNewMemberships = true
                    supplementsManager.clearVesselFuelCheckoutSuccess()
                    state.vesselFuelOrderConfirmationEvent.call()
                }
            }
        }
    }

    private fun observeRemindersState() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    planManager.clearCachedData()
                    loadSupplements(true)
                    if (itemTobeAdded != null) {
                        onAddToPlanClick(itemTobeAdded!!)
                        itemTobeAdded = null
                    }
                }
            }
        }
    }

    private fun loadSupplements(forceUpdate: Boolean = false) {
        showLoading()
        viewModelScope.launch {
            val recs = recommendationManager.getLatestSampleRecommendations(forceUpdate)
            val plansResult = planManager.getUserPlans(forceUpdate)
            if (recs is Result.Success && plansResult is Result.Success) {
                val supplementPlans = plansResult.value.filter { it.isSupplementPlan() }
                todos = supplementPlans.toMutableList()
                val reminders = supplementPlans.toReminderList()
                state.reminderItems.value = reminders
                state.showSupplementRecommendations.value = true

                // create supplement view items
                val recommendationsSupplement =
                    recs.value.supplements.filter { it.reagent != null && it.amount > 0 }
                        .map { sample ->
                            val isInPlan =
                                supplementPlans.any { it.supplements?.any { it.id == sample.id } == true } || sample.in_plan == true
                            SupplementFormula(
                                sample.reagent?.displayName,
                                sample.reagent?.apiName,
                                sample.reagent?.unit,
                                isInPlan = isInPlan,
                                reagent = sample.reagent,
                                quantity = sample.amount,
                                supplementId = sample.supplementId ?: sample.id ?: 0,
                                description = sample.description,
                                consumptionUnit = sample.reagent?.consumptionUnit,
                                goals = sample.goals
                            )
                        }

                state.supplementNutrients.value = recommendationsSupplement.filter {
                    it.nutrient.isNullOrEmpty().not() && it.quantity > 0
                }
                hideLoading(false)
            } else {
                state.showSupplementRecommendations.value = false
            }
            checkVesselFuelMemberships()
        }
    }

    fun onVesselFuelClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SUPPLEMENT_CART_OPEN)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        val nutrients = state.supplementNutrients.value ?: arrayListOf()
        view.findNavController().navigate(
            SupplementFragmentDirections.actionSupplementFragmentToSupplementsBucketFragment(nutrients.toTypedArray())
        )
    }

    override fun onAddToPlanClick(item: SupplementFormula) {
        if (todos.isEmpty() && itemTobeAdded == null) {
            itemTobeAdded = item
            navigateToReminders()
        } else {
            showLoading()
            viewModelScope.launch {
                val result =
                    if (item.isInPlan) {
                        supplementsManager.deleteIngredientsFromSupplements(item.supplementId)
                    } else {
                        supplementsManager.addIngredientsToSupplements(item.supplementId)
                    }
                result
                    .onSuccess {
                        state.supplementNutrients.value =
                            state.supplementNutrients.value.orEmpty().map {
                                if (it.supplementId == item.supplementId) {
                                    it.copy(isInPlan = item.isInPlan.not())
                                } else {
                                    it
                                }
                            }
                        planManager.updatePlanCachedData()
                        recommendationManager.updateLatestSampleRecommendationsCachedData()
                        hideLoading(false)
                    }
                    .onServiceError {
                        showError(it.error.message ?: getResString(R.string.unknown_error))
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }
                    .onError {
                        hideLoading(false)
                    }
            }
        }
    }

    fun onViewMembershipClicked(view: View) {
        showLoading()
        viewModelScope.launch {
            authManager.multipass(Constants.PATH_ACCOUNT)
                .onSuccess {
                    hideLoading(false)
                    view.findNavController()
                        .navigate(HomeNavGraphDirections.globalActionToWeb(it.multipass_url))
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onHttpOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    fun getCheckOutScienceHeaderContent(): SpannableString {
        return getResString(R.string.supplement_science_description)
            .makeClickableSpan(
                Pair(
                    getResString(R.string.references),
                    View.OnClickListener {
                        onLinkButtonClick(it, getResString(R.string.references))
                    }
                )
            )
    }

    fun onLinkButtonClick(view: View, link: String) {
        val navigateTo = when (link) {
            getResString(R.string.references) ->
                HomeNavGraphDirections.globalActionToScienceReferencesDialogFragment(state.getCheckOutReferences())
            else ->
                HomeNavGraphDirections.globalActionToWeb(link)
        }
        view.findNavController().navigate(navigateTo)
    }

    fun onVitaminAngelsLearnMoreClicked(view: View) {
        view.findNavController()
            .navigate(HomeNavGraphDirections.globalActionToWeb(BuildConfig.VITAMIN_URL))
    }

    override fun onInfoIconClicked(view: View, item: SupplementFormula) {
        val positionOnScreen = IntArray(2)
        view.getLocationInWindow(positionOnScreen)
        state.descriptionPopupText.value = item.description
        state.descriptionPopupPosition.value = positionOnScreen[1] + descriptionPopupBottomMargin
    }

    override fun onCloseDialogClicked(view: View) {
        state.descriptionPopupCloseAction.call()
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun forceUpdateVesselFuelMemberships() {
        if (hasNewMemberships) {
            hasNewMemberships = false
            checkVesselFuelMemberships(true)
        }
    }

    private fun checkVesselFuelMemberships(forceUpdate: Boolean = false) {
        showLoading()
        viewModelScope.launch {
            supplementsManager.getMemberships(forceUpdate)
                .onSuccess { memberships ->
                    val membership = memberships.find {
                        it.productTitle == BuildPlanManager.TEST_VESSEL_FUEL_API_NAME && it.shopifyVariantId.toString() == BuildConfig.SHOPIFY_VARIANT_ID && it.status == STATUS_ACTIVE
                    }
                    membership?.let {
                        addVesselFuelAsSupplement(it)
                    }
                    hideLoading(false)
                }
                .onError {
                    state.hasVesselFuel.value = false
                    hideLoading()
                }
        }
    }

    private fun addVesselFuelAsSupplement(membership: Membership) {
        viewModelScope.launch {
            supplementsManager.getSupplements()
                .onSuccess { supplements ->
                    val fuelSupplement = supplements.firstOrNull {
                        it.name == "Vessel Fuel"
                    }
                    val goals: ArrayList<GoalRecord> = arrayListOf()
                    fuelSupplement?.supplementAssociatedGoals.orEmpty().mapIndexed { index, supplementAssociatedGoalSample ->
                        goals.add(GoalRecord(id = supplementAssociatedGoalSample.id, name = supplementAssociatedGoalSample.name, impact = supplementAssociatedGoalSample.impact))
                    }
                    val supplementFormulaList =
                        state.supplementNutrients.value?.toMutableList() ?: mutableListOf()
                    val supplementFormulaMembership =
                        supplementFormulaList.find { it.apiName == membership.productTitle }
                    if (supplementFormulaMembership == null && fuelSupplement != null) {
                        supplementFormulaList.add(
                            0,
                            SupplementFormula(
                                nutrient = membership.productTitle,
                                apiName = membership.productTitle,
                                unit = fuelSupplement.dosageUnit,
                                quantity = fuelSupplement.dosage.toFloat(),
                                supplementId = fuelSupplement.id,
                                description = getResString(R.string.vessel_fuel_desc),
                                isInPlan = false,
                                reagent = null,
                                goals = goals
                            )
                        )
                        state.supplementNutrients.value = supplementFormulaList
                        state.hasVesselFuel.value = true
                        hideLoading(false)
                    }
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun formatDescription(properties: List<MembershipProperties>?): String? {
        val filteredProperties = properties?.filterNot {
            it.name.contentEquals(IGNORED_KEY1) || it.name.contentEquals(IGNORED_KEY2) || it.name.contentEquals(
                IGNORED_KEY3
            )
        }
        if (filteredProperties != null) {
            return buildString {
                append(getResString(R.string.your_formula))
                append(" ")
                append(
                    filteredProperties.map { it.name + " " + it.value }.joinToString(", ")
                )
            }
        }
        return null
    }

    fun onPlanEditItemClick() {
        navigateToReminders()
    }

    override fun onReminderItemClick(item: ReminderRowUiModel, view: View) {
        navigateToReminders()
    }

    fun navigateToReminders() {
        state.navigateTo.value =
            SupplementFragmentDirections.actionSupplementFragmentToPlanReminderDialog(
                PlanReminderHeader(
                    getResString(R.string.supplements),
                    "Daily",
                    "",
                    R.drawable.ic_supplement_red,
                    todos.ifEmpty {
                        listOf(
                            PlanData(
                                is_supplements = true,
                                multiple = 1
                            )
                        )
                    },
                    todos.isEmpty()
                )
            )
    }

    companion object {
        const val VESSEL_FUEL_FIXED_QUANTITY = 4f
        const val STATUS_ACTIVE = "ACTIVE"
        const val IGNORED_KEY1 = "charge_interval_frequency"
        const val IGNORED_KEY2 = "shipping_interval_frequency"
        const val IGNORED_KEY3 = "shipping_interval_unit_type"
    }
}
