package com.vessel.app.wellness.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.repo.BaseNetworkRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.extensions.isSameDay
import com.vessel.app.wellness.model.Score
import com.vessel.app.wellness.net.data.ScoreData
import com.vessel.app.wellness.net.mapper.ScoreMapper
import com.vessel.app.wellness.net.service.ScoreService
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.HashMap

@Singleton
class ScoreRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val scoreService: ScoreService,
    private val scoreMapper: ScoreMapper,
    private val preferencesRepository: PreferencesRepository
) : BaseNetworkRepository(moshi, loggingManager) {
    private var loadedScoreData: Score? = null
    private var loadedAverageScoreData: Score? = null
    private var loadedLatestScoreData: Score? = null

    suspend fun getWellnessScores(forceUpdate: Boolean = false): Result<Score, Nothing> {
        return if (forceUpdate || loadedScoreData == null) {
            val endDate = Calendar.getInstance().apply {
                time = Date()
                add(Calendar.HOUR_OF_DAY, RESULT_UPCOMING_PERIOD_IN_HOURS)
            }

            val calendar = Calendar.getInstance().apply {
                time = endDate.time
                add(Calendar.MONTH, RESULT_PREVIOUS_PERIOD_IN_MONTHS)
            }

            execute(
                scoreMapper::map,
                {
                    scoreService.getScores(
                        DATE_ONLY_FORMAT.format(calendar.time),
                        DATE_ONLY_FORMAT.format(endDate.time)
                    ).apply {
                        preferencesRepository.testCompletedCount = this.size
                    }
                },
                {
                    loadedScoreData = it
                }
            )
        } else {
            Result.Success(loadedScoreData!!)
        }
    }
    suspend fun getLatestWellnessScores(forceUpdate: Boolean = false): Result<Score, Nothing> {
        return if (forceUpdate || loadedLatestScoreData == null) {
            val endDate = Calendar.getInstance().apply {
                time = Date()
                add(Calendar.HOUR_OF_DAY, RESULT_UPCOMING_PERIOD_IN_HOURS)
            }

            val calendar = Calendar.getInstance().apply {
                time = endDate.time
                add(Calendar.MONTH, RESULT_PREVIOUS_PERIOD_IN_MONTHS)
            }

            execute(
                scoreMapper::mapLatest,
                {
                    scoreService.getScores(
                        DATE_ONLY_FORMAT.format(calendar.time),
                        DATE_ONLY_FORMAT.format(endDate.time)
                    ).apply {
                        preferencesRepository.testCompletedCount = this.size
                    }
                },
                {
                    loadedLatestScoreData = it
                }
            )
        } else {
            Result.Success(loadedLatestScoreData!!)
        }
    }

    suspend fun getAverageScores(forceUpdate: Boolean = false): Result<Score, Nothing> {
        return if (forceUpdate || loadedAverageScoreData == null) {
            val endDate = Calendar.getInstance().apply {
                time = Date()
                add(Calendar.HOUR_OF_DAY, RESULT_UPCOMING_PERIOD_IN_HOURS)
            }

            val calendar = Calendar.getInstance().apply {
                time = endDate.time
                add(Calendar.MONTH, RESULT_PREVIOUS_PERIOD_IN_MONTHS)
            }

            execute(
                scoreMapper::map,
                {
                    scoreService.getAverageScores(
                        DATE_ONLY_FORMAT.format(calendar.time),
                        DATE_ONLY_FORMAT.format(endDate.time)
                    )
                },
                {
                    loadedAverageScoreData = it
                }
            )
        } else {
            Result.Success(loadedAverageScoreData!!)
        }
    }

    suspend fun getWellnessScoresWithAverage(forceUpdate: Boolean = false): Result<Pair<Score, Score>, Nothing> {
        return if (forceUpdate || loadedAverageScoreData == null || loadedScoreData == null) {
            val endDate = Calendar.getInstance().apply {
                time = Date()
                add(Calendar.HOUR_OF_DAY, RESULT_UPCOMING_PERIOD_IN_HOURS)
            }

            val calendar = Calendar.getInstance().apply {
                time = endDate.time
                add(Calendar.MONTH, RESULT_PREVIOUS_PERIOD_IN_MONTHS)
            }
            executeBlock {
                val scoreData = scoreService.getAllScores().sortedBy { it.insertDate }

                val averageScoreData = scoreService.getAllAverageScores().sortedBy { it.insertDate }

                val averageItems = HashMap<Int, List<ScoreData>>()

                scoreData.forEachIndexed { index, scoreItem ->
                    val items = mutableListOf<ScoreData>()
                    val averageScore = averageScoreData.firstOrNull { averageScore ->
                        averageScore.insertDate.isSameDay(scoreItem.insertDate)
                    }

                    if (averageScore != null) {
                        items.add(averageScore)
                    } else {
                        items.add(ScoreData(insert_date = scoreItem.insert_date, scores = scoreItem.scores, sample_id = null, sample_uuid = null))
                    }

                    // get the average between current data and the one after index+1
                    if (index + 1 < scoreData.size) {
                        val averageDateRange = averageScoreData.filter {
                            it.insertDate.after(scoreItem.insertDate) &&
                                it.insertDate.before(scoreData[index + 1].insertDate) &&
                                !it.insertDate.isSameDay(scoreItem.insertDate) &&
                                !it.insertDate.isSameDay(scoreData[index + 1].insertDate)
                        }
                        if (averageDateRange.isNotEmpty()) {
                            // get the average from start index  0
                            val start = 0
                            val end = averageDateRange.size - 1
                            items.add(averageDateRange[start + (end - start) / 2])
                        }
                    }
                    averageItems[index] = items
                }

                loadedScoreData = scoreMapper.map(scoreData)
                loadedAverageScoreData = scoreMapper.mapAverageScore(averageItems)
                Result.Success(Pair(loadedScoreData!!, loadedAverageScoreData!!))
            }
        } else {
            return Result.Success(Pair(loadedScoreData!!, loadedAverageScoreData!!))
        }
    }

    fun clearCachedData() {
        loadedScoreData = null
        loadedAverageScoreData = null
    }

    companion object {
        private const val RESULT_PREVIOUS_PERIOD_IN_MONTHS = -6
        private const val RESULT_UPCOMING_PERIOD_IN_HOURS = 48
        private val DATE_ONLY_FORMAT =
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ", Locale("en_US_POSIX"))
    }
}
