package com.vessel.app.onboarding.goals.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GoalSample(
    val id: Int,
    val name: String
) : Parcelable
