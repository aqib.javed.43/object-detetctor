package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ErrorData(
    val latest_sample_uuid: String?,
    val message: String,
    val errors: List<ErrorMessageItemData>?
)
