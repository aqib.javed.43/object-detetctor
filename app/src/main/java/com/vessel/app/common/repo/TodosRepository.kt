package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Todo
import com.vessel.app.common.net.mapper.TodosMapper
import com.vessel.app.common.net.service.TodosService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TodosRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val todosMapper: TodosMapper,
    private val todosService: TodosService
) : BaseNetworkRepository(moshi, loggingManager) {

    private var recurringTodosData: List<Todo>? = null
    private var todayRecurringTodosData: List<Todo>? = null

    suspend fun getRecurringTodos(forceUpdate: Boolean = false): Result<List<Todo>, Nothing> {
        return if (forceUpdate || recurringTodosData == null) {
            execute(
                todosMapper::map,
                {
                    todosService.getRecurringTodos()
                },
                {
                    recurringTodosData = it
                }
            )
        } else {
            Result.Success(recurringTodosData!!)
        }
    }

    suspend fun getTodayRecurringTodos(forceUpdate: Boolean = false): Result<List<Todo>, Nothing> {
        return if (forceUpdate || todayRecurringTodosData == null) {
            execute(
                todosMapper::map,
                {
                    todosService.getTodayRecurringTodos()
                },
                {
                    todayRecurringTodosData = it
                }
            )
        } else {
            Result.Success(todayRecurringTodosData!!)
        }
    }

    suspend fun addRecurringTodo(todo: Todo) = executeWithErrorMessage(todosMapper::map) {
        todosService.addRecurringTodo(todosMapper.mapToData(todo))
    }

    suspend fun updateRecurringTodo(todo: Todo) = executeWithErrorMessage(todosMapper::map) {
        todosService.updateRecurringTodo(todo.id, todosMapper.mapToData(todo))
    }

    suspend fun deleteRecurringTodo(todoId: Int) = executeWithErrorMessage {
        todosService.deleteRecurringTodo(todoId)
    }

    suspend fun updateRecurringTodoToggle(todoId: Int) = executeWithErrorMessage(todosMapper::map) {
        todosService.updateRecurringTodoToggle(todoId)
    }

    fun clearCachedData() {
        recurringTodosData = null
        todayRecurringTodosData = null
    }
}
