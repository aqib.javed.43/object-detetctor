package com.vessel.app.splashscreen

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_splash_screen.*

@AndroidEntryPoint
class SplashScreenFragment : BaseFragment<SplashScreenViewModel>() {
    override val viewModel: SplashScreenViewModel by viewModels()
    override val layoutResId = R.layout.fragment_splash_screen

    private val args: SplashScreenFragmentArgs by lazy {
        SplashScreenFragmentArgs.fromBundle(requireActivity().intent.extras ?: arguments ?: Bundle.EMPTY)
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observeNavigationChanges()
        viewModel.init()
        setupLoadingView()
        setupAppsFlyer()
    }

    private fun setupLoadingView() {
        lottieAnimationView.apply {
            setAnimation(LOADER_FILE_NAME)
            scale = LOADING_VIEW_SCALE_FACTOR
        }
    }

    fun setupAppsFlyer() {
        val conversionDataListener = object : AppsFlyerConversionListener {
            override fun onConversionDataSuccess(data: MutableMap<String, Any>?) {
            }
            override fun onConversionDataFail(error: String?) {
            }
            override fun onAppOpenAttribution(data: MutableMap<String, String>?) {
                viewModel.onAppOpenAttribution(data)
            }
            override fun onAttributionFailure(error: String?) {
            }
        }
        AppsFlyerLib.getInstance().registerConversionListener(requireContext(), conversionDataListener)
        AppsFlyerLib.getInstance().start(requireContext())
    }

    private fun observeNavigationChanges() {
        observe(viewModel.state.navigateTo) { destination ->
            AppsFlyerLib.getInstance().unregisterConversionListener()
            when (destination) {
                SplashScreenState.NavigationDestination.WELCOME -> {
                    findNavController().navigate(SplashScreenFragmentDirections.actionSplashScreenFragmentToWelcome())
                }
                SplashScreenState.NavigationDestination.ONBOARDING -> {
                    findNavController().navigate(SplashScreenFragmentDirections.actionSplashScreenFragmentToOnboarding())
                }
                SplashScreenState.NavigationDestination.HOME -> {
                    findNavController().navigate(SplashScreenFragmentDirections.actionSplashScreenFragmentToHome())
                    activity?.finish()
                }
                else -> throw IllegalArgumentException("Unexpected Navigation destination: $destination")
            }
        }
    }

    companion object {
        const val LOADER_FILE_NAME = "loader.json"
        const val LOADING_VIEW_SCALE_FACTOR = 0.4f
    }
}
