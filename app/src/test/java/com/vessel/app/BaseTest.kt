package com.vessel.app

import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.RegisterExtension

@Suppress("EXPERIMENTAL_API_USAGE")
abstract class BaseTest {
    @JvmField
    @RegisterExtension
    val instantExecutorExtension = InstantExecutorExtension()

    @JvmField
    @RegisterExtension
    val coroutinesExecutorExtension = CoroutinesExecutorExtension()

    @BeforeEach
    fun initMockk() {
        MockKAnnotations.init(this)
    }

    @AfterEach
    open fun tearDown() {
        clearAllMocks()
    }

    fun runBlockingTest(block: suspend TestCoroutineScope.() -> Unit) =
        coroutinesExecutorExtension.testDispatcher.runBlockingTest { block() }
}
