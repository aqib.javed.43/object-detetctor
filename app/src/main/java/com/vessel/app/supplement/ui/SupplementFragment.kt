package com.vessel.app.supplement.ui

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.vessel.app.BR
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.supplement.ViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SupplementFragment : BaseFragment<ViewModel>() {
    override val viewModel: ViewModel by viewModels()
    override val layoutResId = R.layout.fragment_supplement
    private lateinit var popupWindow: PopupWindow
    private val reminderRowAdapter by lazy { ReminderRowAdapter(viewModel) }

    private val supplementAdapter by lazy { SupplementAdapter(viewModel) }
    override fun onViewLoad(savedInstanceState: Bundle?) {
        view?.findViewById<RecyclerView>(R.id.formulaItems)?.adapter = supplementAdapter
        view?.findViewById<RecyclerView>(R.id.reminders)?.adapter = reminderRowAdapter
        setupPopup()
        setupObservers()
    }

    fun setupObservers() {
        viewModel.state {
            observe(descriptionPopupPosition) {
                popupWindow.showAtLocation(requireView(), Gravity.NO_GRAVITY, 0, it)
            }
            observe(descriptionPopupCloseAction) {
                popupWindow.dismiss()
            }
            observe(vesselFuelOrderConfirmationEvent) {
                showVesselFuelConfirmation()
            }
            observe(supplementNutrients) {
                supplementAdapter.submitList(it)
                view?.findViewById<View>(R.id.vesselFuelRecommendation)?.isVisible = it.isEmpty()
                view?.findViewById<View>(R.id.yourFormula)?.isVisible = it.isNotEmpty()
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(vesselFuelOrderConfirmationEvent) {
                showVesselFuelConfirmation()
            }
            observe(reminderItems) {
                view?.findViewById<View>(R.id.planTitle)?.isVisible = it.isNotEmpty()
                view?.findViewById<View>(R.id.editPlan)?.isVisible = it.isNotEmpty()
                reminderRowAdapter.submitList(it)
                reminderRowAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun showVesselFuelConfirmation() {
        val snackbar = Snackbar.make(requireView(), "", Snackbar.LENGTH_SHORT)
        val layout = snackbar.view as Snackbar.SnackbarLayout
        layout.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.notification_snackbar_background_color
            )
        )
        val snackView: View = LayoutInflater.from(requireContext())
            .inflate(R.layout.notification_snakbar_layout, null, false)
        snackView.findViewById<TextView>(R.id.snakebarText)
            .setText(R.string.vessel_fuel_order_confirmed)
        layout.setPadding(0, 0, 0, 0)
        layout.addView(snackView, 0)
        snackbar.show()
    }

    fun setupPopup() {
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        val viewDataBinding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.supplement_description_popup,
            null,
            false
        )
            .apply {
                setVariable(BR.item, viewModel.state.descriptionPopupText)
                setVariable(BR.handler, viewModel)
                lifecycleOwner = viewLifecycleOwner
            }
        popupWindow = PopupWindow(viewDataBinding.root, width, height, true)
    }

    override fun onResume() {
        super.onResume()
        viewModel.forceUpdateVesselFuelMemberships()
    }
}
