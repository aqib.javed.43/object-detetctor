package com.vessel.app.cardchoices.gifted

import android.text.SpannedString
import android.view.View
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavDirections
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.util.CustomTypefaceSpan
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.makeClickableSpan

class GiftedCardChoiceViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    private val trackingManager: TrackingManager,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceRepository) {
    val navigateTo = LiveEvent<NavDirections>()

    fun subTitleTextFormat(): SpannedString {
        val prefix = getResString(R.string.gifted_card_choice_subtitle_text_prefix)
        val boldPart = getResString(R.string.gifted_card_choice_subtitle_text_bold_part)
        val vesselSite = getResString(R.string.vessel_site)
        return buildSpannedString {
            append(prefix)
            append(" ")
            inSpans(CustomTypefaceSpan("", getFont(R.font.bananagrotesk_bold)!!)) {
                append(boldPart)
                append(" ")
                append(
                    vesselSite.makeClickableSpan(
                        Pair(
                            vesselSite,
                            View.OnClickListener {
                                logContinueEvent(TrackingConstants.SIGN_UP_GIFTED_BECOME_MEMBER_LINK)
                                navigateTo.value =
                                    GiftedCardChoiceFragmentDirections.actionGiftedCardChoiceFragmentToWeb(
                                        BuildConfig.BUY_TEST_URL
                                    )
                            }
                        )
                    )
                )
            }
        }
    }

    fun continueClicked() {
        logContinueEvent(TrackingConstants.SIGN_UP_GIFTED_CONTINUE)
        navigateTo.value =
            GiftedCardChoiceFragmentDirections.actionGiftedCardChoiceFragmentToCreateAccount(email, fromAppleGoogleSSOAuth)
    }

    fun onBackButtonClicked() {
        navigateBack()
    }

    private val email = savedStateHandle.get<String>("email")!!
    private val fromAppleGoogleSSOAuth = savedStateHandle.get<Boolean>("fromAppleGoogleSSOAuth")!!

    private fun logContinueEvent(trackingConstants: String) {
        trackingManager.log(
            TrackedEvent.Builder(trackingConstants)
                .setEmail(email)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
