package com.vessel.app.views.dailyview

import com.vessel.app.R

data class DailyStepperUiModel(
    val stepNum: String,
    var isCompleted: Boolean = false,
    val inActiveIcon: Int = R.drawable.ic_step_inactive
) {
    val backgroundResId
        get() = if (isCompleted) {
            R.drawable.ic_step_active
        } else {
            inActiveIcon
        }

    val lineColorResId
        get() = if (isCompleted) {
            R.color.smokey_black
        } else {
            R.color.swirl
        }
}
