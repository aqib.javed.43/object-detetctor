package com.vessel.app.supplementsbucket.ui

import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.BR
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.base.BaseFragment
import com.vessel.app.supplementsbucket.SupplementsBucketViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SupplementsBucketFragment : BaseFragment<SupplementsBucketViewModel>() {
    override val viewModel: SupplementsBucketViewModel by viewModels()
    override val layoutResId = R.layout.fragment_supplement_bucket

    private val supplementHeaderAdapter by lazy { SupplementHeaderAdapter(viewModel) }
    private val freeTitleAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_supplement_bucket_free_title
    }
    private val goalBasedSupplementAdapter by lazy { GoalBasedSupplementAdapter(viewModel) }
    private lateinit var popupWindow: PopupWindow

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupRecyclerView()
        setupPopup()
        setupObservers()
    }

    private fun setupRecyclerView() {
        requireView().findViewById<RecyclerView>(R.id.contentRecyclerView).adapter = ConcatAdapter(
            ConcatAdapter.Config.Builder()
                .setIsolateViewTypes(false)
                .build(),
            supplementHeaderAdapter,
            freeTitleAdapter,
            goalBasedSupplementAdapter
        )
    }

    fun setupObservers() {
        viewModel.state {
            observe(goalBasedSupplement) {
                goalBasedSupplementAdapter.submitList(it.toList())
            }
            observe(supplementHeader) {
                freeTitleAdapter.submitList(listOf(Unit))
                supplementHeaderAdapter.submitList(listOf(it))
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(descriptionPopupPosition) {
                popupWindow.showAtLocation(requireView(), Gravity.NO_GRAVITY, 0, it)
            }
            observe(descriptionPopupCloseAction) {
                popupWindow.dismiss()
            }
            observe(navigateToWeb) {
                findNavController().navigateUp()
                findNavController().navigate(HomeNavGraphDirections.globalActionToWeb(it))
            }
            observe(navigateToCheckout) {
                findNavController().navigate(SupplementsBucketFragmentDirections.actionSupplementsBucketFragmentToVesselFuelCheckoutFragment(it))
            }
        }
    }

    fun setupPopup() {
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        val viewDataBinding = DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, R.layout.supplement_description_popup, null, false)
            .apply {
                setVariable(BR.item, viewModel.state.descriptionPopupText)
                setVariable(BR.handler, viewModel)
                lifecycleOwner = viewLifecycleOwner
            }
        popupWindow = PopupWindow(viewDataBinding.root, width, height, true)
    }
}
