package com.vessel.app.reagent.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.data.ReagentButtonAction
import com.vessel.app.reagent.model.ReagentHeader
import kotlinx.android.synthetic.main.item_reagent_header_fragment.view.*

class ReagentHeaderAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is ReagentHeader -> R.layout.item_reagent_header_fragment
        else -> throw IllegalStateException("Unexpected ReagentHeaderAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any = when (getItem(position)) {
        else -> handler
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        if (holder.itemView.chart.isAlreadyDrawChart) return
        super.onBindViewHolder(holder, position)
        holder.itemView.chart.reDrawRanges((getItem(position) as ReagentHeader).ranges)
    }
    interface OnActionHandler {
        fun onInfoButtonClick()
        fun onCardErrorActionClicked()
        fun onBetaTestingInfoClicked()
        fun onPlanActionClicked(action: ReagentButtonAction)
        fun onSupplementPlanActionClicked()
    }
}
