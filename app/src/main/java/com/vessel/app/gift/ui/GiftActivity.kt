package com.vessel.app.gift.ui

import android.os.Bundle
import androidx.activity.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseActivity
import com.vessel.app.gift.GiftViewModel
import com.vessel.app.util.extensions.applyFullscreenFlags
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GiftActivity : BaseActivity<GiftViewModel>() {
    override val viewModel: GiftViewModel by viewModels()
    override val layoutResId = R.layout.activity_gift

    override fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        applyFullscreenFlags()
    }
}
