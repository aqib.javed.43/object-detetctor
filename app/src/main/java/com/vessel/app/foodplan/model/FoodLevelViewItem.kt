package com.vessel.app.foodplan.model

import androidx.annotation.DrawableRes
import com.vessel.app.R

data class FoodLevelViewItem(
    val name: String,
    val needed: String,
    val progress: Int
) {
    @DrawableRes
    val progressEndDrawable = if (progress == 100) {
        R.drawable.ic_progress_bar_end_black
    } else {
        R.drawable.ic_progress_bar_end
    }
}
