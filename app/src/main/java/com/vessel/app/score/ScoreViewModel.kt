package com.vessel.app.score

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.ScoreManager
import com.vessel.app.common.model.Goal.Companion.alternateBackground
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.score.model.ScoreGoalItem
import com.vessel.app.score.model.ScoreWellnessItem
import com.vessel.app.score.ui.ScoreFragmentDirections
import com.vessel.app.score.ui.ScoreGoalHeaderAdapter
import com.vessel.app.score.ui.ScoreWellnessAdapter
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import kotlinx.coroutines.launch

class ScoreViewModel @ViewModelInject constructor(
    val state: ScoreState,
    resourceProvider: ResourceRepository,
    private val scoreManager: ScoreManager,
    private val goalsRepository: GoalsRepository,
    val preferencesRepository: PreferencesRepository,
    val homeTabsManager: HomeTabsManager,
    val buildPlanManager: BuildPlanManager,
) : BaseViewModel(resourceProvider),
    ScoreWellnessAdapter.OnActionHandler,
    ScoreGoalHeaderAdapter.OnActionHandler {
    private var neededItemsToImproveCount: Boolean? = null

    companion object {
        private const val GOAL_ITEM_COUNT = 3
    }

    init {
        loadScores()
    }

    private fun loadScores() {
        showLoading()
        viewModelScope.launch {
            scoreManager.getWellnessScores(true)
                .onSuccess { score ->
                    hideLoading()
                    var position = 0
                    state {
                        scoreWellnessItems.value =
                            listOf(ScoreWellnessItem(score.wellness.last().y.toInt()))
                        scoreGoalItems.value = goalsRepository.getGoals()
                            .filter { it.value }
                            .map {
                                ScoreGoalItem(
                                    scoreValue = score.goals[it.key]?.lastOrNull()?.y?.toInt() ?: 0,
                                    title = it.key.title,
                                    background = alternateBackground(position++),
                                    image = it.key.icon
                                )
                            }
                            .toList()
                            .take(GOAL_ITEM_COUNT)
                    }
                }
                .onError {
                    hideLoading()
                    // todo handle error case
                }
        }
    }

    override fun onWellnessScoreToolTipClicked(view: View) {
        view.findNavController()
            .navigate(ScoreFragmentDirections.actionScoreToScoreDialog(true))
    }

    override fun onGoalScoreHeaderToolTipClicked(view: View) {
        view.findNavController()
            .navigate(ScoreFragmentDirections.actionScoreToScoreDialog(false))
    }

    fun onBackClicked() {
        navigateBack()
    }

    private fun getNeededItemsToImproveCount(isFromClick: Boolean = false, view: View? = null) {
        viewModelScope.launch {
            neededItemsToImproveCount =
                buildPlanManager.hasFoodPlanRecommendations() || buildPlanManager.hasStressRecommendations() || buildPlanManager.hasHydrationRecommendation()
            if (isFromClick) {
                view?.let { onContinueClicked(view) }
            }
        }
    }

    fun onContinueClicked(view: View) {
        if (preferencesRepository.testCompletedCount <= 1) {
            view.findNavController()
                .navigate(
                    ScoreFragmentDirections.actionScoreFragmentToCreatePlanRecommendationFragment(
                        RecommendationType.Tip
                    )
                )
        } else if (neededItemsToImproveCount == null) {
            getNeededItemsToImproveCount(true, view)
        } else if (neededItemsToImproveCount == true) {
            view.findNavController()
                .navigate(ScoreFragmentDirections.actionScoreFragmentToPlanNeedUpdatesFragment())
        } else {
            homeTabsManager.clear()
            view.findNavController().navigate(PostTestNavGraphDirections.actionGoToHome())
        }
    }
}
