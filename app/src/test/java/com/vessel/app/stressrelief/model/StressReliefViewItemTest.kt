// package com.vessel.app.stressrelief.model
//
// import com.vessel.app.*
// import com.vessel.app.recommendation.StressReliefFaker
// import org.junit.jupiter.api.Test
//
// class StressReliefViewItemTest : BaseTest() {
//
//    @Test
//    fun `it should resolve the drawable res ID for meditate`() {
//        val item = StressReliefFaker.viewItem(StressReliefItem.MEDITATE)
//
//        item.imageResId isEqualTo R.drawable.stress_relief_plan_meditation
//    }
//
//    @Test
//    fun `it should resolve the drawable res ID for breath`() {
//        val item = StressReliefFaker.viewItem(StressReliefItem.BREATH)
//
//        item.imageResId isEqualTo R.drawable.stress_relief_plan_breathwork
//    }
//
//    @Test
//    fun `it should resolve the drawable res ID for yoga`() {
//        val item = StressReliefFaker.viewItem(StressReliefItem.YOGA)
//
//        item.imageResId isEqualTo R.drawable.stress_relief_plan_yoga
//    }
//
//    @Test
//    fun `it should resolve the drawable res ID for cardio`() {
//        val item = StressReliefFaker.viewItem(StressReliefItem.CARDIO)
//
//        item.imageResId isEqualTo R.drawable.stress_relief_plan_cardio
//    }
//
//    @Test
//    fun `it should resolve the drawable res ID for night`() {
//        val item = StressReliefFaker.viewItem(StressReliefItem.NIGHT)
//
//        item.imageResId isEqualTo R.drawable.stress_relief_plan_nighttime_routine
//    }
//
//    @Test
//    fun `it should resolve the drawable res ID for friend`() {
//        val item = StressReliefFaker.viewItem(StressReliefItem.FRIEND)
//
//        item.imageResId isEqualTo R.drawable.stress_relief_plan_hangout
//    }
//
//    @Test
//    fun `it should return the "Remove" button res ID for an item in plan`() {
//        val item = StressReliefFaker.viewItem(isInPlan = true)
//
//        item.buttonResId isEqualTo R.string.remove
//    }
//
//    @Test
//    fun `it should return the "Add to plan" button res ID for an item not in plan`() {
//        val item = StressReliefFaker.viewItem(isInPlan = false)
//
//        item.buttonResId isEqualTo R.string.add_to_plan
//    }
//
//    @Test
//    fun `it should return the white background res ID for an item in plan`() {
//        val item = StressReliefFaker.viewItem(isInPlan = true)
//
//        item.backgroundColorResId isEqualTo R.color.white
//    }
//
//    @Test
//    fun `it should return the grey background res ID for an item not in plan`() {
//        val item = StressReliefFaker.viewItem(isInPlan = false)
//
//        item.backgroundColorResId isEqualTo R.color.grayBg
//    }
// }
