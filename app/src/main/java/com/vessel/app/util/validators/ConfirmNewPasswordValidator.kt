package com.vessel.app.util.validators

import androidx.lifecycle.LiveData

class InvalidConfirmNewPasswordException : ValidatorException("Confirm new password does not match.")

class ConfirmNewPasswordValidator : PasswordValidator(PasswordType.New) {
    fun validate(new: LiveData<String>, confirm: LiveData<String>): Boolean {
        return validate(new.value.orEmpty(), confirm.value.orEmpty())
    }

    fun validate(new: String, confirm: String): Boolean {
        return if (validate(new) && new == confirm) {
            true
        } else {
            throw InvalidConfirmNewPasswordException()
        }
    }
}
