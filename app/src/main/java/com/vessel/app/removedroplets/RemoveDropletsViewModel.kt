package com.vessel.app.removedroplets

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.removedroplets.ui.RemoveDropletsFragmentDirections
import com.vessel.app.util.ResourceRepository

class RemoveDropletsViewModel @ViewModelInject constructor(
    val state: RemoveDropletsState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider), ToolbarHandler {

    fun onContinueClicked() {
        if (preferencesRepository.showAccurateResultDialogV2) {
            state.navigateTo.value =
                RemoveDropletsFragmentDirections.actionRemoveDropletsFragmentToAccurateResultFragment()
        } else {
            state.navigateToCapture.call()
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
}
