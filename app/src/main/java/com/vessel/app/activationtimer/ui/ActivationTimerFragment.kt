package com.vessel.app.activationtimer.ui

import android.os.Bundle
import android.widget.SeekBar
import androidx.activity.addCallback
import androidx.appcompat.widget.AppCompatSeekBar
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.tapadoo.alerter.Alerter
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.activationtimer.ActivationTimerViewModel
import com.vessel.app.activationtimer.ui.ActivationTabsManager.Companion.GOALS_TAB
import com.vessel.app.activationtimer.ui.ActivationTabsManager.Companion.VIDEO_2_TAB
import com.vessel.app.activationtimer.util.TimerServiceManager
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.ext.showMessageSnackbar
import com.vessel.app.common.ext.showNotificationSettingsSnackbar
import com.vessel.app.homescreen.HomeScreenPagerAdapter
import com.vessel.app.taketest.TakeTestViewModel
import com.vessel.app.util.extensions.afterMeasured
import com.vessel.app.util.extensions.observe
import com.vessel.app.util.extensions.openSystemSettings
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActivationTimerFragment : BaseFragment<ActivationTimerViewModel>() {
    override val viewModel: ActivationTimerViewModel by viewModels()
    override val layoutResId = R.layout.fragment_activation_timer

    private var pagerAdapter: HomeScreenPagerAdapter? = null
    private lateinit var viewPager: ViewPager2
    private lateinit var tabLayout: TabLayout
    private lateinit var progressBar: AppCompatSeekBar
    private val minProgressBarYPosition = 250
    val lockTabSelectedIcon by lazy {
        ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_lock_white
        )
    }
    val lockTabUnselectedIcon by lazy {
        ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_lock_black
        )
    }
    private val wholeScreenScrollListener =
        NestedScrollView.OnScrollChangeListener { _, _, _, _, _ ->
            val location = IntArray(2)
            progressBar.getLocationOnScreen(location)
            viewModel.state.showStickyProgress.value = location[1] < minProgressBarYPosition
        }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewPager = requireView().findViewById(R.id.viewPager)
        tabLayout = requireView().findViewById(R.id.tabLayout)
        requireView().findViewById<NestedScrollView>(R.id.nestedScrollView)
            .setOnScrollChangeListener(wholeScreenScrollListener)
        requireView().findViewById<SeekBar>(R.id.timer_progressBar).isEnabled = false
        progressBar = requireView().findViewById(R.id.progressBar)
        progressBar.isEnabled = false
        setupViewPager()
        setupObservers()
        setupBackPressedDispatcher()
    }

    override fun onDestroyView() {
        viewPager.adapter = null
        pagerAdapter = null
        viewModel.activationTabsManager.clear()
        super.onDestroyView()
    }

    private fun setupViewPager() {
        pagerAdapter = HomeScreenPagerAdapter(this)
        viewPager.isUserInputEnabled = false
        pagerAdapter?.addFragments(viewModel.activationTabsManager.getCurrentTabs())
        viewPager.adapter = pagerAdapter

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = getString(viewModel.activationTabsManager.pages[position])
            if (viewModel.afterFirstTest.not() && (position == VIDEO_2_TAB || position == GOALS_TAB))
                tab.icon = lockTabUnselectedIcon
        }.attach()

        if (viewModel.afterFirstTest.not()) {
            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    if (tab.position == VIDEO_2_TAB || tab.position == GOALS_TAB)
                        tab.icon = lockTabSelectedIcon
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {
                    if (tab.position == VIDEO_2_TAB || tab.position == GOALS_TAB)
                        tab.icon = lockTabUnselectedIcon
                }

                override fun onTabReselected(tab: TabLayout.Tab) {
                }
            })
        }
        viewPager.afterMeasured {
            viewModel.getDefaultTabNumber()
        }
    }

    private fun checkCameraPermissions() {
        if (Constants.SCAN_CARD_PERMISSIONS.any { shouldShowRequestPermissionRationale(it) }) {
            Alerter.create(requireActivity())
                .setTitle(R.string.request_permission_no_camera_access_title)
                .setText(R.string.allow_camera_permission_from_settings_message)
                .setBackgroundColorRes(R.color.colorAccent)
                .enableSound(true)
                .enableInfiniteDuration(true)
                .setDismissable(false)
                .addButton(
                    text = getString(R.string.allow_camera_permission_from_settings_go_to_settings_title),
                    onClick = {
                        Alerter.hide()
                        requireActivity().openSystemSettings()
                    }
                ).show()
        } else {
            showConfirmSkipAlert()
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(showConfirmSkipAlertEvent) { checkCameraPermissions() }
            observe(stopTimerEvent) { TimerServiceManager.stopTimerCountDown() }
            observe(forceCompleteTimerEvent) { TimerServiceManager.forceCompleteTimerCountDown() }
            observe(showNotificationSettingsAlert) { showNotificationSettingsSnackbar(it) }
            observe(stopForegroundTimer) { TimerServiceManager.stopForeground() }
            observe(navigateToCapture) {
                parentViewModel<TakeTestViewModel>().testStartDate = viewModel.getStartDate()
                findNavController().navigate(
                    ActivationTimerFragmentDirections.actionActivationTimerToRemoveDropletsFragment()
                )
            }
            observe(defaultTab) {
                tabLayout.getTabAt(it)?.select()
            }
            observe(showFirstReminderHint) {
                showMessageSnackbar(getString(R.string.added_to_plan))
            }
        }
    }

    private fun showConfirmSkipAlert() {
        Alerter.create(requireActivity())
            .setTitle(R.string.warning)
            .setText(R.string.skip_confirm_message)
            .setBackgroundColorRes(R.color.colorAccent)
            .enableSound(true)
            .enableInfiniteDuration(true)
            .addButton(
                text = getString(R.string.btn_cancel),
                onClick = { Alerter.hide() }
            )
            .addButton(
                text = getString(R.string.btn_continue),
                onClick = {
                    viewModel.logScanWarningContinueTapped()
                    viewModel.onSkipConfirm()
                    Alerter.hide()
                }
            )
            .show()
    }

    override fun onResume() {
        super.onResume()
        setupTimerHelper()
    }

    private fun setupTimerHelper() {
        TimerServiceManager.setHandler(viewModel)
        TimerServiceManager.stopForeground()
    }

    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.let { viewModel.onBackButtonClicked(it) }
        }
    }
}
