package com.vessel.app.wellness.model

import com.vessel.app.wellness.RecommendationType
import com.vessel.app.wellness.net.data.LikeStatus

interface IRecommendation {
    fun getBackgroundImageUrl(): String?
    fun getDisplayedTitle(): String?
    fun getDisplayedFrequency(): String?
    fun getDisplayedDescription(): String?
    fun hasSimilar(): Boolean?
    fun getLikesTotalCount(): String?
    fun getLikeBackground(): Int?
    fun getButtonResId(): Int?
    fun getType(): RecommendationType?
    fun getAddedToPlan(): Boolean?
    fun getThumbShapeIcon(): Int?
    fun getLikedStatus(): LikeStatus
    fun getLikesCount(): Int
    fun getDislikesCount(): Int
    fun getDislikesTotalCount(): String?
    fun setLikesCount(count: Int)
    fun setLikedStatus(status: LikeStatus)
}
