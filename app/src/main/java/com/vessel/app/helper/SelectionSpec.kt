
package com.vessel.app.helper

import android.content.pm.ActivityInfo
import android.net.Uri
import androidx.annotation.StyleRes
import com.vessel.app.R
import com.vessel.app.util.GlideEngine
import com.vessel.app.util.ImageEngine

class SelectionSpec private constructor() {
    var mimeTypeSet: Set<MimeType>? = null
    var mediaTypeExclusive = false
    var showSingleMediaType = false

    @StyleRes
    var themeId = 0
    var orientation = 0
    var countable = false
    var maxSelectable = 0
    var maxImageSelectable = 0
    var maxVideoSelectable = 0
    var filters: List<Filter>? = null
    var capture = true
    var captureStrategy: CaptureStrategy? = null
    var spanCount = 0
    var gridExpectedSize = 0
    var thumbnailScale = 0f
    var imageEngine: ImageEngine? = GlideEngine()
    var hasInited = false
    var onSelectedListener: OnSelectedListener? = null
    var originalable = false
    var autoHideToobar = false
    var originalMaxSize = 0
    var onCheckedListener: OnCheckedListener? = null
    var showPreview = false
    private fun reset() {
        mimeTypeSet = null
        mediaTypeExclusive = true
        showSingleMediaType = false
        themeId = R.style.selectionSpec
        orientation = 0
        countable = false
        maxSelectable = 1
        maxImageSelectable = 0
        maxVideoSelectable = 0
        filters = null
        capture = false
        captureStrategy = null
        spanCount = 3
        gridExpectedSize = 0
        thumbnailScale = 0.5f
        imageEngine = GlideEngine()
        hasInited = true
        originalable = false
        autoHideToobar = false
        originalMaxSize = Int.MAX_VALUE
        showPreview = true
    }

    fun singleSelectionModeEnabled(): Boolean {
        return !countable && (maxSelectable == 1 || maxImageSelectable == 1 && maxVideoSelectable == 1)
    }

    fun needOrientationRestriction(): Boolean {
        return orientation != ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    }

    fun onlyShowImages(): Boolean {
        return showSingleMediaType && MimeType.ofImage().containsAll(mimeTypeSet!!)
    }

    fun onlyShowVideos(): Boolean {
        return showSingleMediaType && MimeType.ofVideo().containsAll(mimeTypeSet!!)
    }

    fun onlyShowGif(): Boolean {
        return showSingleMediaType && MimeType.ofGif().equals(mimeTypeSet)
    }

    companion object {
        fun getInstance(): SelectionSpec {
            return InstanceHolder.instance
        }
        fun getCleanInstance(): SelectionSpec {
            val selectionSpec: SelectionSpec = getInstance()
            selectionSpec.reset()
            return selectionSpec
        }
        private object InstanceHolder {
            val instance = SelectionSpec()
        }
    }
}

interface OnSelectedListener {
    fun onSelected(uriList: List<Uri?>, pathList: List<String?>)
}
interface OnCheckedListener {
    fun onCheck(isChecked: Boolean)
}
