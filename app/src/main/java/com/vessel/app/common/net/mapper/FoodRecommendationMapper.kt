package com.vessel.app.common.net.mapper

import com.vessel.app.common.manager.ReagentsManager
import com.vessel.app.common.model.*
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.FoodRecommendationData
import com.vessel.app.common.net.data.ReagentFoodRecommendationData
import com.vessel.app.wellness.net.data.LikeStatus
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FoodRecommendationMapper @Inject constructor(private val reagentsManager: ReagentsManager) {
    fun map(foodRecommendationsData: List<FoodRecommendationData>) = foodRecommendationsData.map {
        FoodRecommendation(
            usdaNdbNumber = it.usda_ndb_number ?: 0,
            foodId = it.id,
            name = it.name.orEmpty(),
            nutrients = extractNutrients(it),
            servingSize = FoodRecommendationServingSize(
                quantity = it.serving_size?.quantity,
                display_quantity = it.serving_size?.display_quantity,
                unit = it.serving_size?.unit
            ),
            imageUrl = it.image_url.orEmpty(),
            goals = listOf(),
            likeStatus = LikeStatus.from(it.like_status),
            totalLikes = it.total_likes,
            dislikes_count = it.dislikes_count
        )
    }
    fun map(recommendationData: ReagentFoodRecommendationData) = recommendationData.foodResponse.map {
        FoodRecommendation(
            foodId = it.id,
            name = it.foodTitle.orEmpty(),
            imageUrl = it.imageURL.orEmpty(),
            goals = listOf(),
            likeStatus = LikeStatus.NO_ACTION,
            nutrients = arrayListOf(),
            servingSize = null,
            usdaNdbNumber = 0
        )
    }
    private fun extractNutrients(foodRecommendationData: FoodRecommendationData) =
        mutableListOf<Nutrient>().apply {
            if (foodRecommendationData.b7 != null && foodRecommendationData.b7 > 0) {
                add(
                    Nutrient(
                        reagentsManager.fromId(ReagentItem.B7.id) ?: return@apply,
                        foodRecommendationData.b7
                    )
                )
            }
            if (foodRecommendationData.b9 != null && foodRecommendationData.b9 > 0) {
                add(
                    Nutrient(
                        reagentsManager.fromId(ReagentItem.B9.id) ?: return@apply,
                        foodRecommendationData.b9
                    )
                )
            }
            if (foodRecommendationData.magnesium != null && foodRecommendationData.magnesium > 0) {
                add(
                    Nutrient(
                        reagentsManager.fromId(ReagentItem.Magnesium.id) ?: return@apply,
                        foodRecommendationData.magnesium
                    )
                )
            }
            if (foodRecommendationData.vitamin_c != null && foodRecommendationData.vitamin_c > 0) {
                add(
                    Nutrient(
                        reagentsManager.fromId(ReagentItem.VitaminC.id) ?: return@apply,
                        foodRecommendationData.vitamin_c
                    )
                )
            }
            sortByDescending { it.amount }
        }
}
