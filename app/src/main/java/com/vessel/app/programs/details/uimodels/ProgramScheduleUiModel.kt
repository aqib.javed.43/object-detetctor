package com.vessel.app.programs.details.uimodels

import android.os.Parcelable
import android.text.format.DateUtils
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProgramScheduleUiModel(
    val completedItems: Int,
    val totalItems: Int,
    val maxStreaks: Int,
    val programScheduleList: List<ProgramScheduleRowUiModel>,
    val showStreaks: Boolean = false
) : Parcelable {

    fun getFocusedList(): List<ProgramScheduleRowUiModel> {
        val todayIndex = programScheduleList.indexOfFirst { DateUtils.isToday(it.date.time) }
        val focusedList = mutableListOf<ProgramScheduleRowUiModel>()
        if (todayIndex > 0 && todayIndex < programScheduleList.lastIndex) {
            focusedList.add(programScheduleList[todayIndex - 1])
            focusedList.add(programScheduleList[todayIndex])

            if (todayIndex + 1 < programScheduleList.size)
                focusedList.add(programScheduleList[todayIndex + 1])
        } else {
            if (programScheduleList.isNotEmpty())
                focusedList.add(programScheduleList.first())
            if (programScheduleList.size > 1)
                focusedList.add(programScheduleList[1])
            if (programScheduleList.size > 2)
                focusedList.add(programScheduleList[2])
        }
        return focusedList
    }
}
