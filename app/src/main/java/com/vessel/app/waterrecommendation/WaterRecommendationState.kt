package com.vessel.app.waterrecommendation

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import com.vessel.app.waterrecommendation.model.WaterRecommendationSelectModel
import javax.inject.Inject

class WaterRecommendationState @Inject constructor() {
    val items = MutableLiveData<List<WaterRecommendationSelectModel>>()
    val navigateTo = LiveEvent<NavDirections>()
    val navigateToHome = LiveEvent<Unit>()
    operator fun invoke(block: WaterRecommendationState.() -> Unit) = apply(block)
}
