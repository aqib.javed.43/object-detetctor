package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ContactData(
    val id: Int? = null,
    val email: String? = null,
    val first_name: String?,
    val last_name: String?,
    val last_login: String? = null,
    val insert_date: String? = null,
    val gender: Char?,
    val height: Float?,
    val weight: Float?,
    val birth_date: String?,
    val allergies: List<Allergy>?,
    val diets: List<Diet>?,
    val goals: List<GoalData>?,
    val time_zone: String?,
    var tips: List<TipData>?,
    val occupation: String?,
    val image_url: String?,
    val is_verified: Boolean?,
    val description: String?,
    val location_description: String?,
    val survey_status: Boolean?,
    val tests_taken: Int?,
    val programs: List<ProgramData>?,
    val main_goal_id: Int?
) : Parcelable
