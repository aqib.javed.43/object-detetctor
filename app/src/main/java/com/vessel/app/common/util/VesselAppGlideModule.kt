package com.vessel.app.common.util

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class VesselAppGlideModule : AppGlideModule()
