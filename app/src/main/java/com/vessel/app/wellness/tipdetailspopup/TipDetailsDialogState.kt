package com.vessel.app.wellness.tipdetailspopup

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.model.Tip
import javax.inject.Inject

class TipDetailsDialogState @Inject constructor(private val passedTip: Tip) {

    val tip = MutableLiveData<Tip>(passedTip)
    val navigateTo = LiveEvent<NavDirections>()
    val dismiss = LiveEvent<Unit>()

    operator fun invoke(block: TipDetailsDialogState.() -> Unit) = apply(block)
}
