package com.vessel.app.util.validators

import androidx.lifecycle.LiveData
import com.wajahatkarim3.easyvalidation.core.view_ktx.maxLength
import com.wajahatkarim3.easyvalidation.core.view_ktx.minLength

class InvalidPasswordException(type: PasswordType) : ValidatorException(type.error)

open class PasswordValidator(private val type: PasswordType = PasswordType.Default) : BaseValidator<String>() {

    override fun error(): ValidatorException = InvalidPasswordException(type)

    override fun validate(value: LiveData<String>): Boolean {
        val password = value.value ?: throw error()
        return validate(password)
    }

    override fun validate(value: String): Boolean {
        return if (value.minLength(6) && value.maxLength(25)) {
            true
        } else {
            throw error()
        }
    }
}

sealed class PasswordType(val error: String) {
    object Default : PasswordType("Please enter a password between 6 and 25 characters.")
    object Current : PasswordType("Current password must be between 6 and 25 characters.")
    object New : PasswordType("New password must be between 6 and 25 characters.")
}
