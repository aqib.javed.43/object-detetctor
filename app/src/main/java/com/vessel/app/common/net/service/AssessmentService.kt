package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.AssessmentData
import com.vessel.app.common.net.data.GoalAssessmentResponse
import com.vessel.app.common.net.data.TagsResponse
import com.vessel.app.common.net.data.TipsResponse
import com.vessel.app.common.net.payload.GoalAssessmentPayload
import retrofit2.http.*

interface AssessmentService {
    @GET("goal_assessment")
    suspend fun getGoalAssessments(
        @Query("start_date") startDate: String?,
        @Query("end_date") endDate: String?
    ): GoalAssessmentResponse

    @POST("goal_assessment")
    suspend fun putGoalAssessments(@Body goalAssessmentPayload: GoalAssessmentPayload): AssessmentData

    @GET("tag")
    suspend fun getTags(
        @Query("goal_id") goal_id: Int?,
        @Query("level") level: String?
    ): TagsResponse

    @GET("recommendations/goal/{goalId}")
    suspend fun getTips(
        @Path("goalId") goalId: Int?,
        @Query("page") page: Int?,
        @Query("per_page") pageSize: Int?,
        @Query("likes") sortingByLikes: Boolean?,
        @Query("created_date") sortingByNewest: Boolean?,
        @Query("level") level: String?,
        @Query("tag_ids") tag_ids: List<Int>?
    ): TipsResponse
    @GET("recommendations/goal/{goalId}")
    suspend fun getTipsWithHabits(
        @Path("goalId") goalId: Int?,
        @Query("page") page: Int?,
        @Query("per_page") pageSize: Int?,
        @Query("likes") sortingByLikes: Boolean?,
        @Query("created_date") sortingByNewest: Boolean?,
        @Query("level") level: String?,
        @Query("tag_ids") tag_ids: List<Int>?,
        @Query("bad_habit_ids") bad_habit_ids: List<Int>?
    ): TipsResponse
}
