package com.vessel.app.onboarding.problemselection

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.SubGoalManager
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.onboarding.problemselection.model.ProblemSelect
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class ProblemSelectionViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    private val subGoalsManager: SubGoalManager,
    private val preferencesRepository: PreferencesRepository,
) : BaseViewModel(resourceProvider),
    ProblemSelectAdapter.OnActionHandler,
    ToolbarHandler {

    var subGoalList: List<ProblemSelect>? = null
    val state = ProblemSelectState()
    var goalRepo: String = ""
    val bgColor = if (preferencesRepository.isExistingUserFirstTime) R.color.beige_color else R.color.primaryLight

    init {
        val goalSelected = goalsRepository.getUserMainGoal()
        goalSelected?.let {
            setGoal(it)
        }
    }
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    private fun fetchAllSubGoals(goalId: Int?) {
        viewModelScope.launch {
            showLoading()
            subGoalsManager.fetchAllSubGoals(goalId)
                .onSuccess { subGoalData ->
                    state {
                        items.value = subGoalData.map {
                            ProblemSelect(
                                it.id,
                                it.name,
                                false
                            )
                        }
                    }
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    fun onDoneClicked() {
        subGoalList = state.items.value.orEmpty()
            .filter { it.checked }
        if (subGoalList!!.isNotEmpty()) {
            var data = ""
            subGoalList!!.forEach {
                data = if (data.isEmpty()) {
                    it.id.toString()
                } else {
                    data + "," + it.id.toString()
                }
            }
            goalsRepository.setUserSubGoal(data)
            state.navigateTo.value = ProblemSelectionFragmentDirections.actionProblemSelectionFragmentToOnBoardingBadHabitsSelectionFragment()
        }
    }
    fun setGoal(goal: GoalSample) {
        goalRepo = "What problems are you having with your " + goal.name + "?"
        fetchAllSubGoals(goal.id)
    }
    override fun onGoalSelectClicked(items: List<ProblemSelect>) {
        subGoalList = items
    }
}
