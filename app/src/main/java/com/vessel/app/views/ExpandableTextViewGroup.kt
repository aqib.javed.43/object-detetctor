package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.withStyledAttributes
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.checkscience.model.CheckOutScienceHeaderItem
import com.vessel.app.checkscience.model.CheckOutScienceViewItem
import com.vessel.app.checkscience.ui.CheckOutScienceHeaderAdapter
import com.vessel.app.checkscience.ui.CheckOutScienceItemsAdapter
import com.vessel.app.checkscience.ui.OnActionHandler
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.impactgoals.ImpactGoalsAdapter
import com.vessel.app.util.extensions.afterMeasured
import com.vessel.app.util.extensions.makeClickableSpan
import com.vessel.app.wellness.model.ScienceSource
import kotlinx.android.synthetic.main.expandable_text_view_group.view.*

class ExpandableTextViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {

    var text: CharSequence
        get() = textView.text
        set(value) {
            textView.text = value
            updateButton()
        }
    private var hideMoreLessButtons = false
    private var lessButtonPoistion = 0
    private var lessButtonText = R.string.less
    private var moreButtonText = R.string.more
    private val backedByScience by lazy { findViewById<RecyclerView>(R.id.backedByScience) }
    var entrySelectionListener: ((View, String) -> Unit)? = null
    var impactsGoalItemClickListener: ((View, ImpactGoalModel) -> Unit)? = null

    private val linkButtonClick = object : OnActionHandler {
        override fun onLinkButtonClick(view: View, link: String) {
            entrySelectionListener?.invoke(view, link)
        }
    }
    private val impactsGoalItemClick = object : ImpactGoalsAdapter.OnActionHandler {
        override fun onGoalItemClick(item: ImpactGoalModel, view: View) {
            impactsGoalItemClickListener?.invoke(view, item)
        }
    }
    val checkOutScienceHeaderAdapter by lazy { CheckOutScienceHeaderAdapter(linkButtonClick) }
    val checkOutScienceItemsAdapter by lazy { CheckOutScienceItemsAdapter(linkButtonClick) }
    val goalsRecordAdapter by lazy { ImpactGoalsAdapter(impactsGoalItemClick) }
    val impactGoalsHeaderAdapter = object : BaseAdapterWithDiffUtil<String>() {
        override fun getLayout(position: Int) = R.layout.item_text_header
    }

    fun setChartEntrySelectionListener(listener: (View, String) -> Unit) {
        this.entrySelectionListener = listener
    }

    fun setImpactsGoalItemClick(listener: (View, ImpactGoalModel) -> Unit) {
        this.impactsGoalItemClickListener = listener
    }

    init {
        inflate(context, R.layout.expandable_text_view_group, this)
        attrs?.let {
            context.withStyledAttributes(attrs, R.styleable.ExpandableTextViewGroup, 0, 0) {
                textView.collapsedLineCount =
                    getInt(R.styleable.ExpandableTextViewGroup_collapsedLines, 0)
                hideMoreLessButtons =
                    getBoolean(R.styleable.ExpandableTextViewGroup_hideMoreLessButtons, false)
                lessButtonPoistion =
                    getInt(R.styleable.ExpandableTextViewGroup_lessPosition, 0)
                lessButtonText =
                    getResourceId(R.styleable.ExpandableTextViewGroup_lessButtonText, R.string.less)
                moreButtonText =
                    getResourceId(R.styleable.ExpandableTextViewGroup_moreButtonText, R.string.more)
            }
        }
        backedByScience.apply {
            val concatAdapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                impactGoalsHeaderAdapter,
                goalsRecordAdapter,
                checkOutScienceHeaderAdapter,
                checkOutScienceItemsAdapter
            )
            val filterSpanSizeLookup = ExpandableTextViewGroupSpanSizeLookup(concatAdapter)
            adapter = concatAdapter
            itemAnimator = null
            layoutManager = GridLayoutManager(context, 2).apply {
                spanSizeLookup = filterSpanSizeLookup
            }
        }
        afterMeasured {
            if (hideMoreLessButtons) {
                button.isVisible = false
                textView.toggleExpanded()
                updateButton()
            } else {
                button.isVisible = true
            }
        }
        button.setOnClickListener {
            if (!hideMoreLessButtons) {
                textView.toggleExpanded()
                updateButton()
            }
        }
        orientation = VERTICAL
    }

    private fun updateButton() {
        backedByScience.isVisible = textView.isExpanded
        button.setText(if (textView.isExpanded) lessButtonText else moreButtonText)
        button.gravity =
            if (textView.isExpanded && lessButtonPoistion == 1) Gravity.LEFT else Gravity.RIGHT
    }
}

@BindingAdapter("checkOutScienceSourcesClickListener")
fun ExpandableTextViewGroup.setCheckOutScienceSourcesClickListener(handler: OnActionHandler?) {
    this.setChartEntrySelectionListener { view, link ->
        handler?.onLinkButtonClick(view, link)
    }
}

@BindingAdapter("checkOutScienceSources")
fun ExpandableTextViewGroup.setCheckOutScienceSources(sources: List<ScienceSource>?) {
    val entries = sources?.map { scienceSource ->
        CheckOutScienceViewItem(
            scienceSource.title.plus("\n\n")
                .plus(context.getString(R.string.read_more))
                .makeClickableSpan(
                    Pair(
                        context.getString(R.string.read_more),
                        View.OnClickListener {
                            entrySelectionListener?.invoke(it, scienceSource.sourceUrl)
                        }
                    )
                ),
            background = android.R.color.transparent
        )
    }
    if (entries.isNullOrEmpty().not()) {
        checkOutScienceItemsAdapter.submitList(entries)
        checkOutScienceHeaderAdapter.submitList(
            listOf(
                CheckOutScienceHeaderItem(
                    null,
                    android.R.color.transparent,
                    title = R.string.backed_by_science
                )
            )
        )
    }
}

@BindingAdapter("impactsGoals")
fun ExpandableTextViewGroup.setImpactsGoals(impactsGoal: List<ImpactGoalModel>?) {
    if (impactsGoal?.isNullOrEmpty() == false) {
        goalsRecordAdapter.submitList(impactsGoal)
        impactGoalsHeaderAdapter.submitList(listOf(context.getString(R.string.helps_with_these_goals)))
    }
}

@BindingAdapter("impactsGoalsClickListener")
fun ExpandableTextViewGroup.setImpactsGoalsClickListener(handler: ImpactGoalsAdapter.OnActionHandler?) {
    this.setImpactsGoalItemClick { view, item ->
        handler?.onGoalItemClick(item, view)
    }
}
