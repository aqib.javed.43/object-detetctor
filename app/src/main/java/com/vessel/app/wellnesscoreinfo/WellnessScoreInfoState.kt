package com.vessel.app.wellnesscoreinfo

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.model.ReagentItem
import javax.inject.Inject

class WellnessScoreInfoState @Inject constructor() {
    val scoreItems = MutableLiveData<String>()
    val reagentItems = MutableLiveData<List<ReagentItem>>()
    val scoreLevel = MutableLiveData<String>()
    val scoreLevelColor = MutableLiveData<Int>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: WellnessScoreInfoState.() -> Unit) = apply(block)
}
