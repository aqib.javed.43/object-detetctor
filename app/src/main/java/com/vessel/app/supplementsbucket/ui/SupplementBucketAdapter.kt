package com.vessel.app.supplementsbucket.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.supplementsbucket.model.Supplement

class SupplementBucketAdapter(
    private val handler: OnActionHandler
) : BaseAdapterWithDiffUtil<Supplement>() {

    override fun getLayout(position: Int) = R.layout.item_supplement_bucket_formula

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onAddItClick(item: Supplement)
        fun onInfoIconClicked(view: View, item: Supplement)
    }
}
