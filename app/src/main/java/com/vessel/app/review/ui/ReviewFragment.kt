package com.vessel.app.review.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.review.ReviewViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReviewFragment : BaseFragment<ReviewViewModel>() {
    override val viewModel: ReviewViewModel by viewModels()
    override val layoutResId = R.layout.fragment_review

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(closeEvent) { getBaseActivity().navigateUp() }
        }
    }
}
