package com.vessel.app.pretimer

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.activationtimer.ActivationTimerState
import com.vessel.app.activationtimer.util.TestSurveyStates
import com.vessel.app.activationtimer.util.TimerService
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.SurveyRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.lfaerrors.model.LFAErrorModel
import com.vessel.app.takesurvey.SurveyQuestion
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import kotlinx.coroutines.*
import java.util.*

class PreActivationTimerViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    val state: PreActivationTimerState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val surveyRepository: SurveyRepository,
    private val trackingManager: TrackingManager,
    private val remoteConfiguration: RemoteConfiguration
) : BaseViewModel(resourceProvider),
    TimerService.TimerHandler,
    ToolbarHandler {
    private val surveyId = remoteConfiguration.getLong(Constants.REMOTE_CONFIG_TIMER_SURVEY_ID)
    val testErrors: List<LFAErrorModel> =
        savedStateHandle.get<Array<LFAErrorModel>>("errors")?.toList().orEmpty()

    override fun onUpdateText(minutes: String, seconds: String) {
        state.updateTimerText(minutes, seconds)
    }

    override fun onUpdateProgress(percentComplete: Double) {
        state {
            if (timerStart == null) {
                timerStart = Constants.CAPTURE_DATE_FORMAT.format(Date())
            }
            progress.value = (percentComplete * ActivationTimerState.PROGRESS_MAX).toInt()
        }
    }

    override fun onFinish() {
        state {
            updateTimerText(ActivationTimerState.FINISH_VALUE, ActivationTimerState.FINISH_VALUE)
            progress.value = ActivationTimerState.PROGRESS_MAX
            viewModelScope.launch {
                delay(500)
                setCompleted()
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        onBackPressed()
    }

    fun onBackPressed() {
        state.showBackWarning.call()
    }

    fun onLetsDoItClicked() {
        showLoading()
        viewModelScope.launch {
            surveyRepository.getSurveyById(surveyId)
                .onSuccess {
                    processSurveyList(it)
                    hideLoading(true)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }
    private fun processSurveyList(items: List<SurveyQuestion>) {
        showLoading(false)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val multipleIds = items.map {
                    it.id
                }
                val content = arrayListOf<SurveyQuestion>()
                coroutineScope {
                    multipleIds.forEach { id ->
                        launch {
                            surveyRepository.getAnswerById(id.toLong()).onSuccess { answerResponse ->
                                items.firstOrNull { it.id == id }?.let {
                                    content.add(it.copy(answers = answerResponse))
                                }
                            }
                        }
                    }
                }
                navigateToTakeSurvey(content)
                hideLoading(false)
            }
        }
    }

    fun onNotPreferClicked() {
        logSurveyEvents(TrackingConstants.SURVEY_SKIP)
        preferencesRepository.setSurveyStates(TestSurveyStates.NOTTAKE)
        state.navigateTo.value = if (testErrors.isNullOrEmpty())
            PreActivationTimerFragmentDirections.actionPreActivationTimerToNewTestTimerFragment()
        else
            PreActivationTimerFragmentDirections.actionPreActivationTimerFragmentToLFAErrorFragment(
                testErrors.toTypedArray()
            )
    }

    private fun logSurveyEvents(eventName: String) {
        trackingManager.log(
            TrackedEvent.Builder(eventName)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun navigateToTakeSurvey(questions: List<SurveyQuestion>) {
        state.navigateTo.value =
            PreActivationTimerFragmentDirections.actionPreActivationTimerToTakeSurvey(
                questions.toTypedArray(),
                surveyId.toString(),
                testErrors.toTypedArray()
            )
    }
}
