package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import com.vessel.app.R
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.button
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.dotsImageView
import kotlinx.android.synthetic.main.black_circle_button_view_group.view.*

open class BlackCircleButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.black_circle_button_view_group, this)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        button.setOnClickListener(l)
    }
}

class BlackCircleWithCrossButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : BlackCircleButtonViewGroup(context, attrs, defStyle) {

    init {
        dotsImageView.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_white_cross_dots
            )
        )
    }
}

class BlackCircleWithPlusButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : BlackCircleButtonViewGroup(context, attrs, defStyle) {

    init {
        dotsImageView.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                R.drawable.white_plus
            )
        )
    }
}

class BlackCircleWithVesselButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : BlackCircleButtonViewGroup(context, attrs, defStyle) {

    private val imageViewContainerPadding by lazy { resources.getDimensionPixelSize(R.dimen.spacing_xs) }

    init {
        imageViewContainer.setPadding(imageViewContainerPadding)
        dotsImageView.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_vessel_white
            )
        )
    }
}

class BlackCircleWithBackButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : BlackCircleButtonViewGroup(context, attrs, defStyle) {

    init {
        dotsImageView.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_white_back_dots
            )
        )
    }
}

class BlackCircleWithDotsLineButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : BlackCircleButtonViewGroup(context, attrs, defStyle) {

    init {
        dotsImageView.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                R.drawable.three_dots_line
            )
        )
    }
}
