package com.vessel.app.vesselfuelcheckout.model.request

import com.squareup.moshi.JsonClass
import com.vessel.app.supplementsbucket.model.Supplement

@JsonClass(generateAdapter = true)
data class VesselFuelSupplementItemModel(
    val id: Int,
    val name: String,
    val dosage_unit: String,
    val dosage: Double,
    val price: Double,
    val is_multivitamin: Boolean,
)

fun Supplement.toVesselFuelModel() = VesselFuelSupplementItemModel(
    id,
    name,
    dosageUnit,
    dosage,
    price,
    isMultivitamin
)
