package com.vessel.app.accurateresult

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.taketest.TakeTestViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AccurateResultFragment : BaseFragment<AccurateResultViewModel>() {
    override val viewModel: AccurateResultViewModel by viewModels()
    override val layoutResId = R.layout.fragment_accurate_result
    private val takeTestViewModel by lazy { parentViewModel<TakeTestViewModel>() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewModel.state {
            observe(navigateToCapture) {
                takeTestViewModel.generateSampleUUID()
                findNavController().navigate(R.id.action_accurateResultFragment_to_capture)
            }
        }
    }
}
