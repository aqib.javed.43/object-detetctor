package com.vessel.app.recommendations.ui

import android.text.SpannableStringBuilder
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import com.vessel.app.R
import com.vessel.app.recommendations.ui.RecommendationTab.*

data class RecommendationsPage(
    val recommendationTab: RecommendationTab,
    val recommendationsPageMessage: SpannableStringBuilder? = null,
    val recommendationsPageGoal: SpannableStringBuilder? = null,
    val isReagentDeficient: Boolean = true,
    val hasPlan: Boolean = false,
    val hasSupplementPlan: Boolean = false,
    val showRedAlert: Boolean = false,
    val hasAddedAllSupplementToPlan: Boolean = false,
    val showActionButton: Boolean = true,
    val hasItemsInPlan: Boolean = false,
) {
    @StringRes
    val title = when (recommendationTab) {
        NUTRIENTS -> R.string.nutrients
        CORTISOL -> R.string.cortisol
        HYDRATION -> R.string.hydration
        TESTING -> R.string.testing
    }

    @DrawableRes
    val recommendationsPageMessageIcon = when (recommendationTab) {
        NUTRIENTS -> R.drawable.ic_add_supplements_colorful_icon
        CORTISOL -> R.drawable.cortisol_recommendations
        HYDRATION -> R.drawable.recommendation_hydration
        TESTING -> R.drawable.test_recommendation_icon
    }

    @DrawableRes
    val recommendationsDurationIcon = when (recommendationTab) {
        NUTRIENTS -> R.drawable.recommendation_food_plan
        CORTISOL -> R.drawable.recommendation_cortisol_plan
        HYDRATION -> R.drawable.recommendation_hydration_plan
        TESTING -> R.drawable.test_recommendation_icon
    }

    @DrawableRes
    val recommendationsGoalsIcon = when (recommendationTab) {
        NUTRIENTS -> R.drawable.ic_add_supplements_girl_image
        CORTISOL -> R.drawable.recommendation_cortisol_goal_icon
        HYDRATION -> R.drawable.recommendation_hydration_goal_icon
        TESTING -> R.drawable.test_recommendation_icon
    }

    @LayoutRes
    val layoutResId: Int = when {
        recommendationTab == TESTING -> R.layout.test_page_recommendations
        isReagentDeficient -> R.layout.deficient_page_recommendations
        else -> R.layout.normal_level_page_recommendations
    }

    @StringRes
    val planActionTitle = if (isReagentDeficient) {
        when (recommendationTab) {
            NUTRIENTS -> if (hasPlan || hasItemsInPlan) R.string.edit_food_plan else R.string.build_a_food_plan
            CORTISOL -> if (hasPlan || hasItemsInPlan) R.string.edit_stress_relief_plan else R.string.build_stress_relief_plan
            HYDRATION -> if (hasPlan || hasItemsInPlan) R.string.edit_hydration_plan else R.string.build_hydration_plan
            TESTING -> if (hasPlan || hasItemsInPlan) R.string.remove_from_plan else R.string.add_to_plan
        }
    } else {
        when (recommendationTab) {
            NUTRIENTS -> R.string.maintenance_food_plan
            CORTISOL -> R.string.maintenance_plan
            HYDRATION -> R.string.maintenance_plan
            TESTING -> if (hasPlan || hasItemsInPlan) R.string.remove_from_plan else R.string.add_to_plan
        }
    }

    @StringRes
    val actionTitle =
        when (recommendationTab) {
            NUTRIENTS -> if (hasPlan && hasSupplementPlan) R.string.nutrients_has_plan_action_text else R.string.nutrients_has_no_plan_action_text
            CORTISOL -> if (hasPlan) R.string.cortisol_has_plan_action_text else R.string.cortisol_has_no_plan_action_text
            HYDRATION ->
                if (!showActionButton) R.string.hydration_over_hydrate_text
                else if (hasPlan) R.string.hydration_has_plan_action_text
                else R.string.hydration_has_no_plan_action_text
            TESTING -> if (hasPlan) R.string.testing_has_plan_action_text else R.string.testing_has_no_plan_action_text
        }

    val canAddPlanReminder = when (recommendationTab) {
        NUTRIENTS -> hasPlan && hasSupplementPlan
        CORTISOL -> hasPlan
        HYDRATION -> if (!showActionButton) false else hasPlan
        TESTING -> false
    }

    @StringRes
    val supplementActionActionTitle = if (!isReagentDeficient) {
        R.string.maintenance_supplement_plan
    } else if (hasSupplementPlan) {
        R.string.edit_supplement_plan
    } else {
        R.string.build_supplement_plan
    }

    @StringRes
    val recommendationsPageDurationMessage: Int =
        when (recommendationTab) {
            NUTRIENTS -> R.string.build_your_nutrients_plan_duration
            CORTISOL -> R.string.build_your_cortisol_plan_duration
            HYDRATION -> R.string.build_your_hydration_plan_duration
            TESTING -> R.string.testing_has_no_plan_action_text
        }

    val shouldShowSupplementRedAlert = recommendationTab == NUTRIENTS && !hasAddedAllSupplementToPlan
    val shouldShowSupplementAction = recommendationTab == NUTRIENTS
    fun calculateTotalBadgeNumber(): Int {
        var count = 0
        if (showRedAlert) {
            count++
        }
        if (shouldShowSupplementRedAlert) {
            count++
        }
        return count
    }
}

enum class RecommendationTab {
    NUTRIENTS,
    CORTISOL,
    HYDRATION,
    TESTING
}
