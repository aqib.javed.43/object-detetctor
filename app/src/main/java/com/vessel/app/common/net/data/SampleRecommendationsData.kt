package com.vessel.app.common.net.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SampleRecommendationsData(
    val supplement: List<SampleData>,
    val food: List<SampleFoodData>,
    val lifestyle: List<SampleData>
)
@JsonClass(generateAdapter = true)
data class ReagentFoodRecommendationData(
    @Json(name = "food_response")
    val foodResponse: List<FoodRecommendation> = arrayListOf()
)

@JsonClass(generateAdapter = true)
data class FoodRecommendation(
    @Json(name = "reagent_id")
    val reagentID: Int? = null,
    @Json(name = "food_title")
    val foodTitle: String? = null,
    val id: Int? = null,
    @Json(name = "image_url")
    val imageURL: String? = null
)
