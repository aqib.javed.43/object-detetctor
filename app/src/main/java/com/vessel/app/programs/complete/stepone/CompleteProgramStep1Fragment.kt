package com.vessel.app.programs.complete.stepone

import android.os.Bundle
import android.widget.TextView
import androidx.activity.addCallback
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import com.vessel.app.wellness.net.data.LikeStatus
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CompleteProgramStep1Fragment : BaseFragment<CompleteProgramStep1ViewModel>() {
    override val viewModel: CompleteProgramStep1ViewModel by viewModels()
    override val layoutResId = R.layout.fragment_complete_program_step_1
    private val recommendedByExpertName: TextView by lazy { requireView().findViewById<TextView>(R.id.recommendedBy) }
    private var feedbackEt: AppCompatEditText? = null

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
        feedbackEt = requireView().findViewById(R.id.feedbackEt)
        activity?.onBackPressedDispatcher?.addCallback(this) {
            viewModel.preventCheckingCompletedProgramAndNavigateBack()
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(program) { program ->
                if (program.contactFullName.isNullOrEmpty().not()) {
                    val byText =
                        getString(R.string.by, program.contactFullName)
                    recommendedByExpertName.text = byText
                } else
                    recommendedByExpertName.isInvisible = true

                if (program.likeStatus == LikeStatus.DISLIKE) {
                    feedbackEt?.isVisible = true
                    feedbackEt?.requestFocus()
                } else {
                    feedbackEt?.isVisible = false
                }
            }
        }
    }
}
