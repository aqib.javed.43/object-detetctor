package com.vessel.app.reagentpersonalizing

import androidx.navigation.NavDirections
import com.vessel.app.reagentpersonalizing.model.ReagentRecommendationModel
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ReagentRecommendationState @Inject constructor() {
    val reagent = LiveEvent<ReagentRecommendationModel>()
    val navigateTo = LiveEvent<NavDirections>()
    val navigate = LiveEvent<Any>()
    val title = LiveEvent<String>()
    val icon = LiveEvent<Int>()
    val description = LiveEvent<String>()
    operator fun invoke(block: ReagentRecommendationState.() -> Unit) = apply(block)
}
