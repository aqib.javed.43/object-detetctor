package com.vessel.app.today.finishreview

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.RecommendationManager
import com.vessel.app.util.ResourceRepository

class FinishReviewViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val recommendationManager: RecommendationManager,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    fun onFinish(v: View) {
        v.findNavController().popBackStack(R.id.homeScreenFragment, false)
    }
}
