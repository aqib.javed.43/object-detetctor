package com.vessel.app.wellness.model

import android.os.Parcelable
import com.vessel.app.R
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.util.extensions.divideToPercent
import com.vessel.app.wellness.RecommendationType
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Lifestyle(
    val id: Int,
    val activityName: String,
    val quantity: Int?,
    val extraImages: List<String>?,
    val description: String?,
    val frequency: String?,
    val imageUrl: String?,
    val totalLikes: Int?,
    val totalDislikes: Int?,
    var likeStatus: LikeStatus,
    val reagentBucketId: Int?,
    val unit: String?,
    val lifestyleRecommendationId: Int,
    val isAddedToPlan: Boolean,
    val impactsGoals: List<GoalRecord>?,
) : Parcelable, IRecommendation {

    override fun getBackgroundImageUrl() = imageUrl

    override fun getDisplayedTitle() = activityName

    override fun getDisplayedFrequency() = frequency

    override fun getDisplayedDescription() = getReason()

    override fun hasSimilar() = false

    override fun getLikeBackground() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.green_background_with_16_radius
    else
        R.drawable.gray_background_with_16_radius

    override fun getButtonResId() = if (isAddedToPlan)
        R.string.remove_from_plan
    else
        R.string.add_to_plan

    private fun getReason() = "Improves: Cortisol \n" + "Goals: ${getPlanGoals().joinToString { it.name }}"

    private fun getPlanGoals(): List<GoalRecord> {
        return impactsGoals.orEmpty()
    }

    override fun getType(): RecommendationType = RecommendationType.Lifestyle

    override fun getAddedToPlan() = isAddedToPlan
    override fun getThumbShapeIcon(): Int? {
        return null
    }

    override fun getLikedStatus(): LikeStatus {
        return likeStatus
    }

    override fun getLikesCount(): Int {
        return totalLikes ?: 0
    }
    override fun getDislikesCount(): Int {
        return totalDislikes ?: 0
    }
    override fun getLikesTotalCount(): String {
        val percent = getLikesCount().divideToPercent(getLikesCount() + getDislikesCount())
        return "$percent %"
    }
    override fun getDislikesTotalCount(): String {
        val percent = getDislikesCount().divideToPercent(getLikesCount() + getDislikesCount())
        return "$percent %"
    }

    override fun setLikesCount(count: Int) {
    }

    override fun setLikedStatus(status: LikeStatus) {
        likeStatus = status
    }
}
