package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass
import com.vessel.app.Constants.SERVER_INSERT_DATE_FORMAT
import java.util.*

@JsonClass(generateAdapter = true)
data class ScoreData(
    val sample_id: Int?,
    val sample_uuid: String?,
    val insert_date: String,
    val scores: ScoresData
) {
    @Transient
    val insertDate = SERVER_INSERT_DATE_FORMAT.parse(insert_date) ?: Date()
}

@JsonClass(generateAdapter = true)
data class ScoreDataResponse(val data: List<ScoreData>)
