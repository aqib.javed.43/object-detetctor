package com.vessel.app.testwinscore

import androidx.lifecycle.MutableLiveData
import com.vessel.app.testwinscore.model.TestWinScoreGoalItem
import com.vessel.app.testwinscore.model.TestWinScoreWellnessItem
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class TestWinScoreState @Inject constructor() {
    val scoreWellnessItems = MutableLiveData<List<TestWinScoreWellnessItem>>()
    val scoreGoalItems = MutableLiveData<List<TestWinScoreGoalItem>>()
    val emptyWinScoreItems = LiveEvent<Unit>()

    operator fun invoke(block: TestWinScoreState.() -> Unit) = apply(block)
}
