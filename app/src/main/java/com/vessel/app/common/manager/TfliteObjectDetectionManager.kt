package com.vessel.app.common.manager

import android.content.Context
import android.util.Log
import com.vessel.app.photocapture.ObjectDetectionHelper
import dagger.hilt.android.qualifiers.ApplicationContext
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.nnapi.NnApiDelegate
import org.tensorflow.lite.support.common.FileUtil
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.ops.ResizeOp
import org.tensorflow.lite.support.image.ops.Rot90Op
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TfliteObjectDetectionManager @Inject constructor(@ApplicationContext val context: Context) {

    data class TfliteDetectionBundle(
        val detector: ObjectDetectionHelper,
        val processor: ImageProcessor
    )

    val bundle: TfliteDetectionBundle

    init {
        bundle = createTfliteDetectionBundle()
    }

    private fun createTfliteDetectionBundle(): TfliteDetectionBundle {
        // val currentContext = requireContext()
        Log.d("tflite_startup", "Creating TfliteDetectionBundle with lazy Items")
        val MODEL_PATH = "model.tflite"
        val LABELS_PATH = "labelmap_10000.txt"
        val imageRotationDegrees: Int = 0
        val tfliteModelFile by lazy {
            Log.d("tflite_startup", "Loading Model " + MODEL_PATH)
            FileUtil.loadMappedFile(context, MODEL_PATH)
        }
        val tflite: Interpreter by lazy {
            try {
                Log.d("tflite_startup", "Initializing Tflite Interpreter")
                Log.d("tflite_startup", "Attempting Interpreter initialization with no delegate specified")
                Interpreter(
                    tfliteModelFile,
                    Interpreter.Options()
                )
            } catch (e: Exception) {
                Log.d("tflite_startup", "Attempting Interpreter initialization Using NNAPI")
                Interpreter(
                    tfliteModelFile,
                    Interpreter.Options().addDelegate(NnApiDelegate())
                )
            }
        }
        val tfInputSize by lazy {
            /** The input image size to the model, derived from the model itself.
             */
            val inputIndex = 0
            val inputShape = tflite.getInputTensor(inputIndex).shape()
            android.util.Size(inputShape[2], inputShape[1]) // Order of axis is: {1, height, width, 3}
        }
        val detector by lazy {
            Log.d("tflite_startup", "Instantiating Tflite ObjectDetectionHelper")
            ObjectDetectionHelper(
                tflite,
                FileUtil.loadLabels(context, LABELS_PATH)
            )
        }
        val processor by lazy {
            Log.d("tflite_startup", "Building TfImageProcessor Pipeline")
            /** The tensorflow pipeline object including resizing.
             */
            ImageProcessor.Builder()
                .add(
                    ResizeOp(
                        tfInputSize.height, tfInputSize.width, ResizeOp.ResizeMethod.NEAREST_NEIGHBOR
                    )
                )
                .add(Rot90Op(-imageRotationDegrees / 90))
                .add(NormalizeOp(0f, 1f))
                .build()
        }
        Log.d("tflite_startup", "Returning TfliteDetectionBundle with lazy Items")
        return TfliteDetectionBundle(detector, processor)
    }
}
