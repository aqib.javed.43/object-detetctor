package com.vessel.app.onboarding.height

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.model.Contact
import com.vessel.app.onboarding.height.HeightDialogFragment.Companion.HEIGHT_KEY
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class HeightDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    @Assisted savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    var height = savedStateHandle[HEIGHT_KEY] ?: Contact.Height.DEFAULT_HEIGHT
    val dismissDialog = LiveEvent<Unit>()
    val heightErrorEvent = LiveEvent<Any>()
    val heightSet = LiveEvent<Contact.Height>()

    fun onCancelClicked() {
        dismissDialog.call()
    }

    fun onSetClicked() {
        if (height.feet == 0 && height.inches == 0) {
            heightErrorEvent.call()
        } else {
            heightSet.value = height
            dismissDialog.call()
        }
    }

    fun onFeetSelected(feetValue: Int) {
        height = Contact.Height(feetValue, height.inches)
    }

    fun onInchesSelected(inchesValue: Int) {
        height = Contact.Height(height.feet, inchesValue)
    }
}
