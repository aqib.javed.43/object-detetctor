package com.vessel.app.common.net.payload

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GoalAssessmentPayload(
    val goal_id: Int,
    val value: Int,
    val created_at: String
)
