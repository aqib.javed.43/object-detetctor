package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.vessel.app.R
import kotlinx.android.synthetic.main.widget_reminder.view.*

class ReminderWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.widget_reminder, this)
    }

    fun setReminderClick(click: () -> Unit) {
        reminderIcon.setOnClickListener {
            click.invoke()
        }
    }

    fun setReminderTimeClick(click: () -> Unit) {
        description.setOnClickListener {
            click.invoke()
        }
    }

    fun setHasAlarm(hasAlarm: Boolean) {
        if (hasAlarm) {
            reminderIcon.setImageResource(R.drawable.ic_reminders)
            reminderButton.backgroundTintList =
                ContextCompat.getColorStateList(context, R.color.black)
        } else {
            reminderIcon.setImageResource(R.drawable.ic_reminders_off)
            reminderButton.backgroundTintList =
                ContextCompat.getColorStateList(context, R.color.whiteAlpha30)
        }
    }

    fun setDescription(hint: String) {
        description.text = hint
    }
}
