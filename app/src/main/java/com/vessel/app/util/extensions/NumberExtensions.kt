package com.vessel.app.util.extensions

import java.text.NumberFormat
import kotlin.math.roundToInt

fun List<Float>.closestValue(value: Float) = minByOrNull { kotlin.math.abs(value - it) }

fun Float.formatToString(): String = NumberFormat.getInstance().format(this)

fun Double.formatDigits(digit: Int) = String.format("%.${digit}f", this)
fun Int.divideToPercent(divideTo: Int): Int {
    return if (divideTo == 0) 0
    else (this.toDouble() / divideTo.toDouble() * 100.0).roundToInt()
}
