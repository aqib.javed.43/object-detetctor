package com.vessel.app.helper

import android.content.Context

abstract class Filter {
    /**
     * Against what mime types this filter applies.
     */
    protected abstract fun constraintTypes(): Set<MimeType>

    /**
     * Invoked for filtering each item.
     *
     * @return null if selectable, [IncapableCause] if not selectable.
     */
    abstract fun filter(context: Context?, mediaItem: MediaItem?): IncapableCause?

    /**
     * Whether an [MediaItem] need filtering.
     */
    protected fun needFiltering(context: Context, mediaItem: MediaItem): Boolean {
        for (type in constraintTypes()) {
            if (type.checkType(context.contentResolver, mediaItem.contentUri)) {
                return true
            }
        }
        return false
    }

    companion object {
        /**
         * Convenient constant for a minimum value.
         */
        const val MIN = 0

        /**
         * Convenient constant for a maximum value.
         */
        const val MAX = Int.MAX_VALUE

        /**
         * Convenient constant for 1024.
         */
        const val K = 1024
    }
}
