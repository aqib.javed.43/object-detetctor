package com.vessel.app.pretest.directpee.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.pretest.directpee.DirectPeeViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DirectPeeFragment : BaseFragment<DirectPeeViewModel>() {
    override val viewModel: DirectPeeViewModel by viewModels()
    override val layoutResId = R.layout.fragment_direct_pee

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
                requireActivity().finish()
            }
        }
    }
}
