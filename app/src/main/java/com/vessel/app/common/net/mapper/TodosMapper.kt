package com.vessel.app.common.net.mapper

import com.vessel.app.Constants
import com.vessel.app.common.model.Todo
import com.vessel.app.common.net.data.TodoData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TodosMapper @Inject constructor() {
    fun map(data: List<TodoData>) = data.map { map(it) }

    fun map(data: TodoData) = Todo(
        id = data.id,
        name = data.name,
        quantity = data.quantity,
        insertDate = data.insert_date?.let { Constants.SERVER_INSERT_DATE_FORMAT.parse(it) },
        usdaNdbNumber = data.usda_ndb_number ?: 0,
        sampleId = data.sample_id,
        completedDate = data.completed_date,
        dueDate = data.due_date,
        dueDateTime = data.due_date_dt,
        recurringTodoId = data.recurring_todo_id,
        food_id = data.food_id,
        food = data.food,
        has_reminder = data.has_reminder,
        day_of_week = data.day_of_week,
        reminder_time_of_day = data.reminder_time_of_day
    )

    fun mapToData(todo: Todo) = TodoData(
        id = todo.id,
        name = todo.name,
        quantity = todo.quantity,
        insert_date = todo.formattedDate,
        usda_ndb_number = todo.usdaNdbNumber,
        sample_id = todo.sampleId,
        completed_date = todo.completedDate,
        due_date = todo.dueDate,
        due_date_dt = todo.dueDateTime,
        recurring_todo_id = todo.recurringTodoId,
        food_id = todo.food_id,
        food = todo.food,
        has_reminder = todo.has_reminder,
        day_of_week = todo.day_of_week,
        reminder_time_of_day = todo.reminder_time_of_day,
    )
}
