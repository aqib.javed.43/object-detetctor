package com.vessel.app.reagent.ui

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.reagent.ReagentViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_reagent.*

@AndroidEntryPoint
class ReagentFragment : BaseFragment<ReagentViewModel>() {
    override val viewModel: ReagentViewModel by viewModels()
    override val layoutResId = R.layout.fragment_reagent
    private val reagentHeaderAdapter by lazy { ReagentHeaderAdapter(viewModel) }
    private val reagentScienceAdapter by lazy { ReagentScienceAdapter() }
    private val reagentEnergyAdapter by lazy { ReagentEnergyAdapter(viewModel) }
    private val reagentReachInfoAdapter by lazy { ReagentReachInfoAdapter() }
    private val reagentFooterAdapter by lazy { ReagentFooterAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        reagentList.also {
            val adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                reagentHeaderAdapter,
                reagentScienceAdapter,
                reagentEnergyAdapter,
                reagentReachInfoAdapter,
                reagentFooterAdapter
            )

            it.adapter = adapter
            it.layoutManager = GridLayoutManager(requireContext(), 1)
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(reagentHeaderItems) { reagentHeaderAdapter.submitList(it) }
            observe(navigateTo) { findNavController().navigate(it) }
            observe(reachInfo) { reagentReachInfoAdapter.submitList(it) }
            observe(reagentFooterItems) { reagentFooterAdapter.submitList(it) }
            observe(talkToNutritionist) {
                requireActivity().supportFragmentManager.setFragmentResult(REQUEST_KEY, bundleOf())
                viewModel.navigateBack()
            }
            observe(reagentDetails) {
                it?.let {
                    reagentEnergyAdapter.submitList(it.benefitsOfBeingOptimal)
                    reagentScienceAdapter.submitList(listOf(viewModel.reagentHeader))
                }
            }
        }
    }
    companion object {
        const val REQUEST_KEY = "ReagentFragmentRequestkey"
    }
}
