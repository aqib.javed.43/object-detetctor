package com.vessel.app.lfaerrors

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ReagentsManager
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.lfaerrors.model.LFAErrorModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.makeClickableSpan

class LFAErrorViewModel @ViewModelInject constructor(
    val state: LFAErrorState,
    private val resourceProvider: ResourceRepository,
    private val reagentsManager: ReagentsManager,
    @Assisted private val savedStateHandle: SavedStateHandle,
) : BaseViewModel(resourceProvider) {

    val hideTryAgain = savedStateHandle.get<Boolean>("isFirstTest")?.not() ?: true

    init {
        createLFAErrorHint()
    }

    fun onGoResultClicked() {
        state.goToPostScreen.call()
    }

    private fun createLFAErrorHint() {
        val testErrors: List<LFAErrorModel> =
            savedStateHandle.get<Array<LFAErrorModel>>("errors")?.toList().orEmpty()
        val reagentsErrors: List<LFAErrorModel> = testErrors.filter { it.reagentId != null }

        val reagentsHint = reagentsErrors.map {
            reagentsManager.fromId(it.reagentId ?: 0)?.displayName.orEmpty()
        }.filterNot { it.isEmpty() }.joinToString()

        val text =
            getResString(R.string.lfa_test_result_error_hint, reagentsHint)
        val hintString = text.makeClickableSpan(
            Pair(
                resourceProvider.getString(R.string.lfa_test_result_error_hint_few_things),
                View.OnClickListener {
                    state.goToErrorHintDialog.call()
                }
            )
        )

        state.reagents.value = hintString
        failedReagentsImageInit(reagentsErrors)
        titleInit(reagentsErrors)
    }

    private fun failedReagentsImageInit(errors: List<LFAErrorModel>) {
        val reagentsIds = errors.map { it.reagentId }
        val imageName = buildString {
            append("lfa")
            if (reagentsIds.contains(ReagentItem.B7.id)) {
                append("_b7")
            }
            if (reagentsIds.contains(ReagentItem.B9.id)) {
                append("_b9")
            }
            if (reagentsIds.contains(ReagentItem.Cortisol.id)) {
                append("_cortisol")
            }
        }

        state.failedReagentsImage.value = resourceProvider.getDrawableIdentifier(imageName)
    }

    private fun titleInit(errors: List<LFAErrorModel>) {
        val reagentsIds = errors.map { it.reagentId }
        val result = buildString {
            var didAddB7 = false
            if (reagentsIds.contains(ReagentItem.B7.id)) {
                append(resourceProvider.getString(R.string.b7_biotin))
                didAddB7 = true
            }
            if (reagentsIds.contains(ReagentItem.Cortisol.id)) {
                if (didAddB7) {
                    append(" ")
                    append(resourceProvider.getString(R.string.and))
                    append(" ")
                }
                append(resourceProvider.getString(R.string.cortisol))
            }
        }

        state.title.value = resourceProvider.getString(R.string.title_we_couldnt_read, result)
    }

    fun onTryAgainClicked() {
        state.navigateToCapture.call()
    }
}
