package com.vessel.app.supplement.model

data class Membership(
    val id: Int,
    val shopifyVariantId: Long,
    val productTitle: String,
    val quantity: Int,
    val properties: List<MembershipProperties>?,
    val status: String
)
