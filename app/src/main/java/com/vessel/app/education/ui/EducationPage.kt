package com.vessel.app.education.ui

import androidx.annotation.StringRes
import com.vessel.app.R
import com.vessel.app.covid19.model.FAQ

enum class EducationPage(
    val number: Int,
    @StringRes val title: Int,
    val videoUrl: String,
    val stepA: EducationStep,
    val stepB: EducationStep,
    val stepC: EducationStep,
    val faqs: List<FAQ>
) {
    Step1(
        1,
        R.string.take_test_page1_title,
        "https://d32fui7xaadmx6.cloudfront.net/diagnostic/video/1_prep_space.mp4",
        EducationStep(
            R.drawable.step_1_a,
            R.string.take_test_page1_step_a
        ),
        EducationStep(
            R.drawable.step_1_b,
            R.string.take_test_page1_step_b
        ),
        EducationStep(
            R.drawable.step_1_c,
            R.string.take_test_page1_step_c
        ),
        listOf(
            FAQ(R.string.take_test_page1_faq1_title, R.string.take_test_page1_faq1_body),
            FAQ(R.string.take_test_page1_faq2_title, R.string.take_test_page1_faq2_body)
        )
    ),
    Step2(
        2,
        R.string.take_test_page2_title,
        "https://d32fui7xaadmx6.cloudfront.net/diagnostic/video/2_wash_hands.mp4",
        EducationStep(
            R.drawable.step_2_a,
            R.string.take_test_page2_step_a
        ),
        EducationStep(
            R.drawable.step_2_b,
            R.string.take_test_page2_step_b
        ),
        EducationStep(
            R.drawable.step_2_c,
            R.string.take_test_page2_step_c
        ),
        listOf(
            FAQ(R.string.take_test_page2_faq1_title, R.string.take_test_page2_faq1_body),
            FAQ(R.string.take_test_page2_faq2_title, R.string.take_test_page2_faq2_body)
        )
    ),
    Step3(
        3,
        R.string.take_test_page3_title,
        "https://d32fui7xaadmx6.cloudfront.net/diagnostic/video/3_prick_finger.mp4",
        EducationStep(
            R.drawable.step_3_a,
            R.string.take_test_page3_step_a
        ),
        EducationStep(
            R.drawable.step_3_b,
            R.string.take_test_page3_step_b
        ),
        EducationStep(
            R.drawable.step_3_c,
            R.string.take_test_page3_step_c
        ),
        listOf(
            FAQ(R.string.take_test_page3_faq1_title, R.string.take_test_page3_faq1_body),
            FAQ(R.string.take_test_page3_faq2_title, R.string.take_test_page3_faq2_body),
            FAQ(R.string.take_test_page3_faq3_title, R.string.take_test_page3_faq3_body),
            FAQ(R.string.take_test_page3_faq4_title, R.string.take_test_page3_faq4_body),
            FAQ(R.string.take_test_page3_faq5_title, R.string.take_test_page3_faq5_body),
            FAQ(R.string.take_test_page3_faq6_title, R.string.take_test_page3_faq6_body)
        )
    ),
    Step4(
        4,
        R.string.take_test_page4_title,
        "https://d32fui7xaadmx6.cloudfront.net/diagnostic/video/4_collect_blood.mp4",
        EducationStep(
            R.drawable.step_4_a,
            R.string.take_test_page4_step_a
        ),
        EducationStep(
            R.drawable.step_4_b,
            R.string.take_test_page4_step_b
        ),
        EducationStep(
            R.drawable.step_4_c,
            R.string.take_test_page4_step_c
        ),
        listOf(
            FAQ(R.string.take_test_page4_faq1_title, R.string.take_test_page4_faq1_body),
            FAQ(R.string.take_test_page4_faq2_title, R.string.take_test_page4_faq2_body)
        )
    ),
    Step5(
        5,
        R.string.take_test_page5_title,
        "https://d32fui7xaadmx6.cloudfront.net/diagnostic/video/5_deposit_blood.mp4",
        EducationStep(
            R.drawable.step_5_a,
            R.string.take_test_page5_step_a
        ),
        EducationStep(
            R.drawable.step_5_b,
            R.string.take_test_page5_step_b
        ),
        EducationStep(
            R.drawable.step_5_c,
            R.string.take_test_page5_step_c
        ),
        listOf(
            FAQ(R.string.take_test_page5_faq1_title, R.string.take_test_page5_faq1_body),
            FAQ(R.string.take_test_page5_faq2_title, R.string.take_test_page5_faq2_body)
        )
    ),
    Step6(
        6,
        R.string.take_test_page6_title,
        "https://d32fui7xaadmx6.cloudfront.net/diagnostic/video/6_add_buffer.mp4",
        EducationStep(
            R.drawable.step_6_a,
            R.string.take_test_page6_step_a
        ),
        EducationStep(
            R.drawable.step_6_b,
            R.string.take_test_page6_step_b
        ),
        EducationStep(
            R.drawable.step_6_c,
            R.string.take_test_page6_step_c
        ),
        listOf(
            FAQ(R.string.take_test_page6_faq1_title, R.string.take_test_page6_faq1_body),
            FAQ(R.string.take_test_page6_faq2_title, R.string.take_test_page6_faq2_body)
        )
    )
}
