package com.vessel.app.util

import android.content.ContentResolver
import android.database.ContentObserver
import android.database.Cursor
import android.net.Uri
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.provider.MediaStore

class ScreenshotDetectionDelegate(contentResolver: ContentResolver, listener: Listener) {
    private val mHandlerThread: HandlerThread = HandlerThread("ScreenshotDetectionDelegate")
    private val mHandler: Handler
    private val mContentResolver: ContentResolver
    private val mContentObserver: ContentObserver
    private val mListener: Listener
    fun register() {
        mContentResolver.registerContentObserver(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            true,
            mContentObserver
        )
    }

    fun unregister() {
        mContentResolver.unregisterContentObserver(mContentObserver)
    }

    interface Listener {
        fun onScreenShotTaken()
    }

    init {
        mHandlerThread.start()
        mHandler = Handler(mHandlerThread.looper)
        mContentResolver = contentResolver
        mContentObserver = ScreenShotObserver(mHandler, contentResolver, listener)
        mListener = listener
    }
}
class ScreenShotObserver(
    handler: Handler?,
    private val mContentResolver: ContentResolver,
    private val mListener: ScreenshotDetectionDelegate.Listener
) :
    ContentObserver(handler) {
    private val PROJECTION = arrayOf(
        MediaStore.Images.Media._ID,
        MediaStore.Images.Media.DISPLAY_NAME,
        MediaStore.Images.Media.DATA
    )
    private val MEDIA_EXTERNAL_URI_STRING = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString()
    private val FILE_NAME_PREFIX = "screenshot"
    private val PATH_SCREENSHOT = "screenshots/"
    override fun deliverSelfNotifications(): Boolean {
        return super.deliverSelfNotifications()
    }

    override fun onChange(selfChange: Boolean) {
        super.onChange(selfChange)
    }

    override fun onChange(selfChange: Boolean, uri: Uri?) {
        super.onChange(selfChange, uri)
        if (isSingleImageFile(uri)) {
            handleItem(uri)
        }
    }

    private fun isSingleImageFile(uri: Uri?): Boolean {
        return uri.toString().matches(("$MEDIA_EXTERNAL_URI_STRING/[0-9]+").toRegex())
    }

    private fun handleItem(uri: Uri?) {
        var cursor: Cursor? = null
        try {
            cursor = mContentResolver.query(uri!!, PROJECTION, null, null, null)
            if (cursor != null) {
                Handler(Looper.getMainLooper()).post {
                    mListener.onScreenShotTaken()
                }
            }
        } finally {
            cursor?.close()
        }
    }
}
