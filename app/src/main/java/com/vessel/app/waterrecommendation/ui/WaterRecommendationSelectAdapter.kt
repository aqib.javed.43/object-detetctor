package com.vessel.app.waterrecommendation.ui

import android.widget.ImageView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.waterrecommendation.model.WaterRecommendationSelectModel
import kotlinx.android.synthetic.main.item_water_recommendation_select.view.*

class WaterRecommendationSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<WaterRecommendationSelectModel>() {
    override fun getLayout(position: Int) = R.layout.item_water_recommendation_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onItemSelectClicked(item: WaterRecommendationSelectModel)
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<WaterRecommendationSelectModel>,
        position: Int
    ) {
        super.onBindViewHolder(holder, position)
        holder.itemView.waterCount.removeAllViews()
        for (i in 0 until getItem(position).count) {
            val imageView = ImageView(holder.itemView.context)
            imageView.setImageResource(R.drawable.ic_water_bottle)
            imageView.adjustViewBounds = true
            holder.itemView.waterCount.addView(imageView)
        }
    }
}
