package com.vessel.app.homescreen

import com.vessel.app.base.BaseFragment
import com.vessel.app.coach.CoachFragment
import com.vessel.app.more.ui.MoreFragment
import com.vessel.app.today.ui.TodayFragment
import com.vessel.app.wellness.ui.WellnessFragment
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HomeTabsManager @Inject constructor() {

    lateinit var tabs: List<BaseFragment<*>>

    init {
        createTabs()
    }

    private fun createTabs() {
        tabs = listOf(
            TodayFragment(),
            WellnessFragment(),
            CoachFragment(),
            MoreFragment()
        )
    }

    fun getCurrentTabs(): List<BaseFragment<*>> {
        if (::tabs.isInitialized.not() || (::tabs.isInitialized && tabs.isEmpty()) || isAnyTabAdded()) {
            createTabs()
        }
        return tabs
    }

    fun isAnyTabAdded(): Boolean {
        return ::tabs.isInitialized && tabs.any { it.isAdded }
    }

    fun clear() {
        tabs = emptyList()
    }
}
