
Magnesium is an essential mineral that is required for the normal functioning of over 300 enzymes in the body, impacting virtually all aspects of metabolism.

The majority of US adults do not consume enough magnesium in their diet and have at least a nominal magnesium deficiency. Furthermore, conditions such as alcohol abuse, diabetes, diseases of the digestive tract, and use of acid-blocking medications such as omeprazole or pantoprazole all lead to increased magnesium loss in the urine creating even more deficiency.

What does magnesium do? Inadequate intake of magnesium may modestly elevate blood pressure and increase the risk of osteoporosis and fractures. In addition, supplementing with magnesium may be helpful for conditions such as migraines and menstrual pain, and it's an effective laxative and antacid. It may also improve glucose status in people with prediabetes -- particularly if they are low in magnesium. It also helps control levels of vitamin D and can boost low levels. Maintaining adequate magnesium intake also has cardiovascular benefits.

**References:**

-   Nutrition in Clinical Practice: A Comprehensive, Evidence-Based Manual for the Practitioner (Nutrition in Clinical Practice), 2nd Edition by David Katz. (many additional references listed in appendix of book)
