We use a urine marker called specific gravity (SG) to track your hydration status.

The lower your specific gravity, the more hydrated you are, and the higher your specific gravity, the more dehydrated you are.

When you look at the graph, a low level suggests dehydration, and a high level suggests overhydration.

If you are overhydrated (top zone on the graph), your urine is very diluted with water. Try consuming more electrolytes. The best way to do this is by eating more balanced meals that include plenty of fresh fruits and vegetables. Check out your Vessel food recommendations for personalized food recommendations.

If you’re dehydrated (low zone on the graph), we will suggest how many more 8oz glasses of water you need each day to get into the sweet spot.

**references:**

-   https://pubmed.ncbi.nlm.nih.gov/25564016/

-   This paper reviews how urine markers of hydration are sensitive measures to evaluate changes in total water intake.
- https://www.healthline.com/health/urine-specific-gravity#results

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5451964/

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4207053/

-   https://labtestsonline.org/tests/urinalysis

-   https://pubmed.ncbi.nlm.nih.gov/30959872/

-   https://pubmed.ncbi.nlm.nih.gov/16028571/

-   [Perrier ET](https://www.ncbi.nlm.nih.gov/pubmed/?term=Perrier%20ET%5BAuthor%5D&cauthor=true&cauthor_uid=28145416)1, [Bottin JH](https://www.ncbi.nlm.nih.gov/pubmed/?term=Bottin%20JH%5BAuthor%5D&cauthor=true&cauthor_uid=28145416)1, [Vecchio M](https://www.ncbi.nlm.nih.gov/pubmed/?term=Vecchio%20M%5BAuthor%5D&cauthor=true&cauthor_uid=28145416)1, [Lemetais G](https://www.ncbi.nlm.nih.gov/pubmed/?term=Lemetais%20G%5BAuthor%5D&cauthor=true&cauthor_uid=28145416)1. Criterion values for urine-specific gravity and urine color representing adequate water intake in healthy adults. [Eur J Clin Nutr.](https://www.ncbi.nlm.nih.gov/pubmed/28145416#) 2017 Apr;71(4):561-563. doi: 10.1038/ejcn.2016.269. Epub 2017 Feb 1.

-   [Godevithanage S](https://www.ncbi.nlm.nih.gov/pubmed/?term=Godevithanage%20S%5BAuthor%5D&cauthor=true&cauthor_uid=20616562)1, [Kanankearachchi PP](https://www.ncbi.nlm.nih.gov/pubmed/?term=Kanankearachchi%20PP%5BAuthor%5D&cauthor=true&cauthor_uid=20616562), [Dissanayake MP](https://www.ncbi.nlm.nih.gov/pubmed/?term=Dissanayake%20MP%5BAuthor%5D&cauthor=true&cauthor_uid=20616562), [Jayalath TA](https://www.ncbi.nlm.nih.gov/pubmed/?term=Jayalath%20TA%5BAuthor%5D&cauthor=true&cauthor_uid=20616562), [Chandrasiri N](https://www.ncbi.nlm.nih.gov/pubmed/?term=Chandrasiri%20N%5BAuthor%5D&cauthor=true&cauthor_uid=20616562), [Jinasena RP](https://www.ncbi.nlm.nih.gov/pubmed/?term=Jinasena%20RP%5BAuthor%5D&cauthor=true&cauthor_uid=20616562), [Kumarasiri RP](https://www.ncbi.nlm.nih.gov/pubmed/?term=Kumarasiri%20RP%5BAuthor%5D&cauthor=true&cauthor_uid=20616562), [Goonasekera CD](https://www.ncbi.nlm.nih.gov/pubmed/?term=Goonasekera%20CD%5BAuthor%5D&cauthor=true&cauthor_uid=20616562). Spot urine osmolality/creatinine ratio in healthy humans.[Kidney Blood Press Res.](https://www.ncbi.nlm.nih.gov/pubmed/20616562#) 2010;33(4):291-6. doi: 10.1159/000316509. Epub 2010 Jul 10.

-   Armstrong, Lawrence E.; Herrera Soto, Jorge A.; Hacker, Frank T. Jr.; Casa, Douglas J.; Kavouras, Stavros A.; Maresh, Carl M. (December 1998). Urinary indices during dehydration, exercise, and rehydration. Int. J. Sport Nutr. 8 (4): 345–355. doi:10.1123/ijsn.8.4.345. PMID 9841955.

-   [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5451964/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5451964/)

-   [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5451964/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5451964/)

-   Cooper GM. The Cell: A Molecular Approach. 2nd edition. Sunderland (MA): Sinauer Associates; 2000. The Molecular Composition of Cells. Available from: https://www.ncbi.nlm.nih.gov/books/NBK9879/

-   Flasar, C. (2008). What is urine specific gravity? Nursing, 38(7), 14. [https://doi.org/10.1097/01.nurse.0000325315.41513.a0](https://doi.org/10.1097/01.nurse.0000325315.41513.a0)

-   Pediatric Reference Ranges: Age 2 - 17 References:

-   [https://www.childrensmn.org/references/lab/urinestool/specific-gravity-urine.pdf](https://www.childrensmn.org/references/lab/urinestool/specific-gravity-urine.pdf)

-   [https://www.researchgate.net/publication/314857680_Urine_specific_gravity_as_a_diagnostic_tool_for_dehydration_in_children](https://www.researchgate.net/publication/314857680_Urine_specific_gravity_as_a_diagnostic_tool_for_dehydration_in_children)

-   [https://www.ncbi.nlm.nih.gov/pubmed/9241476](https://www.ncbi.nlm.nih.gov/pubmed/9241476)

-   [https://www.ucsfbenioffchildrens.org/tests/003587.html](https://www.ucsfbenioffchildrens.org/tests/003587.html)
