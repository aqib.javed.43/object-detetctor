package com.vessel.app.wellness.lockpopup

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.Constants
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class LockPopUpViewModel @ViewModelInject constructor(
    val state: LockPopUpState,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    @Assisted savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()

    fun onTakeTest(view: View) {
        Constants.onTakeTest?.invoke()
    }
    fun onGoBack(view: View) {
        dismissDialog.call()
        Constants.onGotoPreviousScreen?.invoke()
    }
}
