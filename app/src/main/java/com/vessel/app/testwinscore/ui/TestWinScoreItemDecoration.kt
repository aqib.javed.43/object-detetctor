package com.vessel.app.testwinscore.ui

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import com.vessel.app.R

class TestWinScoreItemDecoration(context: Context, val spanSizeLookup: TestWinScoreSpanLookup) : RecyclerView.ItemDecoration() {
    private val spanCount = context.resources.getInteger(R.integer.test_win_score_list_span_count)
    private val spacingSmall = context.resources.getDimensionPixelSize(R.dimen.spacing_small)
    private val spacingLarge = context.resources.getDimensionPixelSize(R.dimen.spacing_large)
    private val spacing2xs = context.resources.getDimensionPixelSize(R.dimen.spacing_2xs)

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        if (parent.getChildAdapterPosition(view) == NO_POSITION) return

        // reset all properties adjusted below
        outRect.left = 0
        outRect.top = 0
        outRect.right = 0
        outRect.bottom = 0
        view.translationY = 0f

        val spanIndex = spanSizeLookup.getSpanIndex(position, spanCount)

        when (parent.adapter?.getItemViewType(position)) {
            R.layout.item_test_win_score_goal -> {
                outRect.left = spacing2xs
                outRect.top = spacingSmall
                outRect.right = spacing2xs
                outRect.left = spacingSmall

                if (spanIndex == 0) outRect.left = spacingLarge
                else if (spanIndex + 1 == spanCount) outRect.right = spacingLarge
            }
        }
    }
}
