package com.vessel.app.createnewpoll

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CreateNewPollState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: CreateNewPollState.() -> Unit) = apply(block)
}
