package com.vessel.app.addsupplements

import android.text.SpannedString
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.R
import com.vessel.app.addsupplements.ui.AddSupplementsFragmentDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.RecommendationManager
import com.vessel.app.common.manager.TodosManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Todo
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.supplement.model.SupplementFormula
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class AddSupplementsViewModel @ViewModelInject constructor(
    val state: AddSupplementsState,
    val resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val goalsRepository: GoalsRepository,
    private val recommendationManager: RecommendationManager,
    private val todosManager: TodosManager,
    private var authManager: AuthManager
) : BaseViewModel(resourceProvider) {

    private val todos = mutableListOf<Todo>()

    init {
        prepareGoals()
    }

    private fun prepareGoals() {
        viewModelScope.launch {
            val recs = recommendationManager.getLatestSampleRecommendations()
            val todosResult = todosManager.getRecurringTodos()

            if (recs is Result.Success) {
                if (todosResult is Result.Success) {
                    todos.apply {
                        clear()
                        addAll(todosResult.value)
                    }
                }
                state.showSupplementRecommendations.value = true
                // create supplement view items
                state.supplementNutrients.value = recs.value.supplements.map { sample ->
                    val isSupplementTodo = todos.firstOrNull { todo ->
                        todo.name == sample.reagent?.apiName
                    }
                    SupplementFormula(
                        sample.reagent?.displayName,
                        sample.reagent?.apiName,
                        sample.reagent?.unit,
                        isInPlan = isSupplementTodo != null,
                        reagent = sample.reagent,
                        quantity = sample.amount,
                        consumptionUnit = sample.reagent?.consumptionUnit
                    )
                }
            } else {
                state.showSupplementRecommendations.value = false
            }
        }
    }

    fun formatGoals(): SpannedString {
        val goalsList = goalsRepository.getGoals().keys.toList()
        val prefixString =
            resourceProvider.getString(R.string.add_supplements_to_your_plan_improve_nutrient_title)
        val names = goalsList.take(GOALS_COUNT).map { getResString(it.title) }.joinToString(", ")
        return buildSpannedString {
            append(prefixString)
            append(" ")
            bold {
                append(names)
                append(".")
            }
        }
    }

    fun onBuyNowClicked() {
        showLoading()
        viewModelScope.launch {
            authManager.multipass(Constants.PATH_SUPPLEMENTS)
                .onSuccess {
                    hideLoading(false)
                    state.navigateTo.value =
                        PostTestNavGraphDirections.globalActionToWeb(it.multipass_url)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onHttpOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun onLaterClicked() {
        state.navigateTo.value =
            AddSupplementsFragmentDirections.actionAddSupplementsFragmentToLastStepFragmentFragment()
    }

    companion object {
        const val GOALS_COUNT = 4
    }
}
