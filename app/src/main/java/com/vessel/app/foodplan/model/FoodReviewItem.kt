package com.vessel.app.foodplan.model

import com.vessel.app.activityreview.model.RecommendationReview

data class FoodReviewItem(
    val title: String,
    val items: List<RecommendationReview>
)
