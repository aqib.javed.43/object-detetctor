package com.vessel.app.onboarding.terms

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.terms.ui.OnboardingTermsFragmentDirections
import com.vessel.app.util.ResourceRepository

class OnboardingTermsViewModel @ViewModelInject constructor(
    @Assisted val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val contactManager: ContactManager,
    private val preferencesRepository: PreferencesRepository,
) : BaseViewModel(resourceProvider), ToolbarHandler {

    val state = OnboardingTermsState()
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onNextClicked() {
        preferencesRepository.isExistingUserFirstTime = false
        val isFirstTestFlow = savedStateHandle.get<Boolean>("isFirstTestFlow")
        isFirstTestFlow?.let {
            state.navigateTo.value =
                OnboardingTermsFragmentDirections.actionOnboardingTermsFragmentToOnboardingGoalsFragment(
                    it
                )
        }
    }
}
