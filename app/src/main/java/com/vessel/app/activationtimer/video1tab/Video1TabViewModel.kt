package com.vessel.app.activationtimer.video1tab

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.ResourceRepository

class Video1TabViewModel @ViewModelInject constructor(
    val state: Video1TabState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider)
