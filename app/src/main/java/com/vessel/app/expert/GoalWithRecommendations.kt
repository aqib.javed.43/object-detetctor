package com.vessel.app.expert

import com.vessel.app.wellness.model.RecommendationItem

data class GoalWithRecommendations(
    val goalName: String,
    val tips: List<RecommendationItem>,
    var isCollapsed: Boolean = true
)
