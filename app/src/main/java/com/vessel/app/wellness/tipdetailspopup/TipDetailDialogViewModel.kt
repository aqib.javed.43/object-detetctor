package com.vessel.app.wellness.tipdetailspopup

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.MainNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.checkscience.ui.OnActionHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.model.Tip

class TipDetailDialogViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider),
    OnActionHandler {

    val state = TipDetailsDialogState(savedStateHandle.get<Tip>("tip")!!)

    fun onCloseDialogClicked() {
        state.dismiss.call()
    }

    override fun onLinkButtonClick(view: View, link: String) {
        state.navigateTo.value = MainNavGraphDirections.globalActionToWeb(link)
    }
}
