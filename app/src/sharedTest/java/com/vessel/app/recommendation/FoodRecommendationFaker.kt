// TODO fix the unit test

// package com.vessel.app.recommendation
//
// import com.vessel.app.BaseFaker
// import com.vessel.app.BaseFaker.Fake.FAKER
// import com.vessel.app.common.model.FoodRecommendation
// import com.vessel.app.common.model.Nutrient
// import com.vessel.app.common.model.Reagent
// import com.vessel.app.common.net.data.FoodRecommendationData
// import com.vessel.app.foodplan.model.FoodRecommendationViewItem
//
// object FoodRecommendationFaker {
//    fun basic(
//        nutrients: List<Nutrient> = listOf(
//            Nutrient(Reagent.B7, FAKER.number().numberBetween(1, 100).toFloat()),
//            Nutrient(Reagent.B9, FAKER.number().numberBetween(1, 100).toFloat()),
//            Nutrient(Reagent.Magnesium, FAKER.number().numberBetween(1, 100).toFloat())
//        )
//    ) = FAKER.run {
//        FoodRecommendation(
//            usdaNdbNumber = number().randomDigit(),
//            name = dragonBall().character(),
//            nutrients = nutrients,
//            servingSize = null
//        )
//    }
//
//    fun data(
//        b7: Float = FAKER.number().numberBetween(1, 100).toFloat(),
//        b9: Float = FAKER.number().numberBetween(1, 100).toFloat(),
//        magnesium: Float = FAKER.number().numberBetween(1, 100).toFloat(),
//        vitaminC: Float = FAKER.number().numberBetween(1, 100).toFloat()
//    ) = FAKER.run {
//        FoodRecommendationData(
//            usda_ndb_number = number().randomDigit(),
//            name = dragonBall().character(),
//            b7 = b7,
//            b9 = b9,
//            magnesium = magnesium,
//            vitamin_c = vitaminC
//        )
//    }
//
//    fun viewItem(
//        name: String = FAKER.dragonBall().character(),
//        isInPlan: Boolean = FAKER.bool().bool()
//    ) = FAKER.run {
//        FoodRecommendationViewItem(
//            name = name,
//            description = name,
//            foodNutrientsText = chuckNorris().fact(),
//            isInPlan = isInPlan,
//            usdaNdbNumber = number().randomDigit()
//        )
//    }
//
//    fun list() = BaseFaker.list { basic() }
//
//    fun dataList() = BaseFaker.list { data() }
// }
