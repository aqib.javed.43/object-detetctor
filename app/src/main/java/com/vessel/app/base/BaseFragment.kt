package com.vessel.app.base

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.*
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.tapadoo.alerter.Alerter
import com.vessel.app.BR
import com.vessel.app.R
import com.vessel.app.util.extensions.hideKeyboard
import com.vessel.app.util.extensions.observe

abstract class BaseFragment<VM : BaseViewModel> : Fragment() {
    protected abstract val viewModel: VM
    protected abstract val layoutResId: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        onBeforeViewLoad(savedInstanceState)

        val viewDataBinding =
            DataBindingUtil.inflate<ViewDataBinding>(inflater, layoutResId, container, false)

        return viewDataBinding?.apply {
            setVariable(BR.viewModel, viewModel)
            lifecycleOwner = this@BaseFragment
        }?.root ?: inflater.inflate(layoutResId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupObservers()

        onViewLoad(savedInstanceState)

        observe(viewModel.showAlert) {
            Alerter.create(requireActivity())
                .setTitle(it.title)
                .setText(it.body)
                .setBackgroundColorRes(R.color.colorAccent)
                .enableSound(true)
                .show()
        }
    }

    private fun setupObservers() {
        observe(viewModel.navigateBack) {
            beforeNavigateBack()
            findNavController().popBackStack()
            hideKeyboard()
        }
    }

    open fun beforeNavigateBack() {
        // Intentionally empty so that subclasses can override if necessary
    }

    open fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        // Intentionally empty so that subclasses can override if necessary
    }

    open fun onViewLoad(savedInstanceState: Bundle?) {
        // Intentionally empty so that subclasses can override if necessary
    }

    protected fun getRuntimePermissions(permissions: List<String>, permissionRequestCode: Int) {
        val allNeededPermissions = mutableListOf<String>()
        if (!allPermissionsGranted(permissions)) {
            allNeededPermissions.addAll(
                permissions.filter {
                    ContextCompat.checkSelfPermission(
                        requireContext(), it
                    ) != PackageManager.PERMISSION_GRANTED
                }
            )
        }

        if (allNeededPermissions.isNotEmpty()) {
            requestPermissions(allNeededPermissions.toTypedArray(), permissionRequestCode)
        }
    }

    protected fun allPermissionsGranted(permissions: List<String>) =
        permissions.all {
            ContextCompat.checkSelfPermission(
                requireContext(),
                it
            ) == PackageManager.PERMISSION_GRANTED
        }

    protected inline fun <reified PVM : BaseViewModel> parentViewModel(): PVM {
        return when {
            parentFragment != null ->
                if (parentFragment is NavHostFragment) {
                    if (requireParentFragment().parentFragment == null) {
                        ViewModelProvider(requireParentFragment().requireActivity())
                            .get(PVM::class.java)
                    } else {
                        ViewModelProvider(requireParentFragment().requireParentFragment())
                            .get(PVM::class.java)
                    }
                } else {
                    ViewModelProvider(requireParentFragment()).get(PVM::class.java)
                }
            activity != null -> activity?.run {
                ViewModelProvider(this)[PVM::class.java]
            } ?: throw Exception("Invalid Activity")
            else -> throw RuntimeException("This fragment does not have any parent!")
        }
    }

    fun getBaseActivity() = requireActivity() as BaseActivity<*>

    protected fun showBackWarning(
        @StringRes messageBody: Int,
        okClickedAction: (() -> Unit)? = null
    ) {
        if (isAdded) {
            Alerter.create(requireActivity())
                .setTitle(R.string.warning)
                .setText(messageBody)
                .setBackgroundColorRes(R.color.colorAccent)
                .enableSound(true)
                .enableInfiniteDuration(true)
                .addButton(
                    text = getString(R.string.ok),
                    onClick = {
                        Alerter.hide()
                        okClickedAction?.invoke()
                        requireActivity().finish()
                    }
                )
                .addButton(
                    text = getString(R.string.btn_cancel),
                    onClick = {
                        Alerter.hide()
                    }
                )
                .show()
        }
    }
}
