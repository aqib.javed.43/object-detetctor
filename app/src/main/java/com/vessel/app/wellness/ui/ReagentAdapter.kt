package com.vessel.app.wellness.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.wellness.model.ReagentItem

class ReagentAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<ReagentItem>(
    areItemsTheSame = { oldItem, newItem -> oldItem.id == newItem.id }
) {
    override fun getLayout(position: Int) = R.layout.item_reagent

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onReagentItemClick(item: ReagentItem, view: View)
    }
}
