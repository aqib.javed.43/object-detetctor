package com.vessel.app.flex.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.flex.model.MembershipOptionProduct
import kotlinx.android.synthetic.main.item_membership_option.view.*

class ShippingProductAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<MembershipOptionProduct>() {
    override fun getLayout(position: Int) = R.layout.item_shipping_option

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onItemSelectClicked(item: MembershipOptionProduct)
    }
}
