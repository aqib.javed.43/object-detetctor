package com.vessel.app.review

import androidx.lifecycle.Observer
import com.vessel.app.BaseTest
import com.vessel.app.util.ResourceRepository
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ReviewViewModelTest : BaseTest() {
    private val resourceRepository = mockk<ResourceRepository>()
    private val eventObserver = mockk<Observer<Unit>>()
    private lateinit var state: ReviewState
    private lateinit var viewModel: ReviewViewModel

    @BeforeEach
    fun setUp() {
        every { eventObserver.onChanged(null) } just runs

        state = ReviewState()

        viewModel = ReviewViewModel(state, resourceRepository)
    }

    @Test
    fun `onContinueClicked calls the close event`() {
        state.closeEvent.observeForever(eventObserver)

        viewModel.onContinueClicked()

        verify { eventObserver.onChanged(null) }
    }
}
