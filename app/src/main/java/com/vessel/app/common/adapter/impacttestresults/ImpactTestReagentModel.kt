package com.vessel.app.common.adapter.impacttestresults

import androidx.annotation.DrawableRes

class ImpactTestReagentModel(
    val title: String,
    @DrawableRes val icon: Int,
    val quantity: String
) {
    val showQuantity = quantity.isNotEmpty()
}
