package com.vessel.app.wellnesscoreinfo

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.testscore.TestScoreViewModel
import com.vessel.app.tooltipscore.ui.ReagentScoreAdapter
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_test_score.*
import kotlinx.android.synthetic.main.fragment_test_score.resultsList
import kotlinx.android.synthetic.main.fragment_tool_tip_score.*

@AndroidEntryPoint
class WellnessScoreInfoFragment : BaseFragment<WellnessScoreInfoViewModel>() {
    override val viewModel: WellnessScoreInfoViewModel by viewModels()
    override val layoutResId = R.layout.fragment_wellness_score_info
    private val resultAdapter by lazy { ReagentScoreAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupAdapters()
        setupObservers()
    }
    private fun setupAdapters() {
        resultsList.apply {
            adapter = resultAdapter
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(reagentItems) {
                resultAdapter.submitList(it)
            }
            observe(scoreLevelColor) {
                val color = ContextCompat.getColor(
                    requireContext(),
                    it ?: TestScoreViewModel.ScoreLevel.Poor.color
                )
                wellnessScoreContainerCard.setCardBackgroundColor(color)
            }
        }
    }
}
