package com.vessel.app.programs.programselection

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Program
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ProgramCardWidgetOnActionHandler
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import com.vessel.app.wellness.net.data.ProgramAction
import kotlinx.coroutines.launch

class ProgramSelectionViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val programManager: ProgramManager,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider),
    ProgramCardWidgetOnActionHandler,
    ToolbarHandler {

    val state = ProgramSelectionState(savedStateHandle.get("goalIdSelected")!!)
    val isBackVisible = savedStateHandle.get<Boolean>("showBackButton")
    init {
        loadPrograms(state.goalIdSelected)
    }

    private fun loadPrograms(goalIdSelected: MutableLiveData<Int>) {
        showLoading()
        viewModelScope.launch {
            goalIdSelected.value?.let {
                programManager.getProgramsByGoal(goalId = it, true).onSuccess { programModel ->
                    state.programsItems.value = programModel.program
                    hideLoading(false)
                }.onError {
                    hideLoading(false)
                }
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onMaybeLaterClicked() {
        navigateToCreatePlan()
    }

    override fun onProgramItemClicked(program: Program) {
        state.navigateTo.value =
            ProgramSelectionFragmentDirections.actionProgramSelectionFragmentToProgramInfoDialogFragment(
                program
            )
    }

    override fun onProgramInfoClicked(program: Program) {
        state.navigateTo.value =
            ProgramSelectionFragmentDirections.actionProgramSelectionFragmentToProgramInfoDialogFragment(
                program
            )
    }

    override fun onJoinProgramClicked(program: Program) {
        enrollProgram(program)
    }

    private fun enrollProgram(program: Program) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PROGRAM_ENROLLED)
                .addProperty(TrackingConstants.KEY_ID, program.id.toString())
                .addProperty(TrackingConstants.ENROLL_TYPE, ProgramAction.KEEP.value)
                .addProperty(TrackingConstants.KEY_LOCATION, Constants.AUTO_BUILD_PLAN)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state {
            viewModelScope.launch {
                showLoading()
                programManager.joinProgram(program, ProgramAction.KEEP).onSuccess {
                    hideLoading()
                    navigateToCreatePlan()
                }.onServiceError {
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }.onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }.onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }.onError { hideLoading() }
            }
        }
    }

    private fun navigateToCreatePlan() {
        state.navigateTo.value =
            ProgramSelectionFragmentDirections.actionProgramSelectionFragmentToCreatePlanRecommendationFragment(
                RecommendationType.Food
            )
    }
}
