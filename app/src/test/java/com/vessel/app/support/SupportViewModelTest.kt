package com.vessel.app.support

import androidx.lifecycle.Observer
import com.vessel.app.*
import com.vessel.app.common.manager.MembershipManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class SupportViewModelTest : BaseTest() {
    private val resourceRepository = mockk<ResourceRepository>()
    private val preferencesRepository = mockk<PreferencesRepository>()
    private val remoteConfiguration = mockk<RemoteConfiguration>()
    private val membershipManager = mockk<MembershipManager>()
    private val trackingManager = mockk<TrackingManager>()
    private val eventObserver = mockk<Observer<Unit>>(relaxed = true)

    private lateinit var viewModel: SupportViewModel

    @BeforeEach
    fun setUp() {
        viewModel = SupportViewModel(resourceRepository, preferencesRepository, remoteConfiguration, membershipManager, trackingManager)
    }

    @Test
    fun `it should open the feedback URL when clicking the share idea button`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onShareAnIdeaClicked(view)

        verify {
            navController.navigate(SupportFragmentDirections.actionSupportToWeb(BuildConfig.FEEDBACK_URL))
        }
    }

    @Test
    fun `it should open the report bug URL when clicking the report bug button`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onReportBugClicked(view)

        verify {
            navController.navigate(SupportFragmentDirections.actionSupportToWeb(BuildConfig.REPORT_BUG_URL))
        }
    }

    @Test
    fun `it should open the support URL when clicking the contact support button`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onContactSupportClicked()

        verify {
            navController.navigate(SupportFragmentDirections.actionSupportToWeb(BuildConfig.HELP_CENTER_URL))
        }
    }

    @Test
    fun `it should navigate back when onBackButtonClicked is called`() {
        viewModel.navigateBack.observeForever(eventObserver)

        viewModel.onBackButtonClicked(mockk())

        verify { eventObserver.onChanged(null) }
    }
}
