package com.vessel.app.forgotpassword.ui

import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.forgotpassword.ForgotPasswordViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPasswordFragment : BaseFragment<ForgotPasswordViewModel>() {
    override val viewModel: ForgotPasswordViewModel by viewModels()
    override val layoutResId = R.layout.fragment_forgot_password
}
