package com.vessel.app.wellness.net.service

import com.vessel.app.common.net.data.BadHabitResponseData
import retrofit2.http.GET
import retrofit2.http.Query

interface BadHabitService {
    @GET("bad-habit")
    suspend fun getAllBadHabits(
        @Query("subgoal_ids") subGoalId: String?
    ): BadHabitResponseData
}
