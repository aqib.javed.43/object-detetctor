package com.vessel.app.filterfoodplandialog.model

import androidx.annotation.StringRes
import com.vessel.app.R

enum class SortOptions(@StringRes val title: Int, var isAscending: Boolean, val apiName: String) {
    RECOMMENDED(R.string.recommended, false, "popularity"),
    A_Z(R.string.a_z, true, "alpha")
}
