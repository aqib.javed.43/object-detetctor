package com.vessel.app.goalpage.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.goalpage.GoalPageViewModel
import com.vessel.app.util.extensions.observe
import com.vessel.app.wellness.filterpopup.FilterDialogFragment
import com.vessel.app.wellness.filterpopup.ItemFilterCategory
import com.vessel.app.wellness.model.Tag
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_goal_page.*

@AndroidEntryPoint
class GoalPageFragment : BaseFragment<GoalPageViewModel>() {
    override val viewModel: GoalPageViewModel by viewModels()
    override val layoutResId = R.layout.fragment_goal_page
    private val goalPageRecommendsAdapter by lazy { GoalPageRecommendsAdapter(viewModel) }
    private val goalPageFooterAdapter by lazy { GoalPageFooterAdapter(viewModel) }
    private val reagentAdapter by lazy { GoalPageReagentAdapter(viewModel) }
    private val goalPageTodayAdapter by lazy { GoalPageTodayAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
        findNavController().currentBackStackEntry
            ?.savedStateHandle?.apply {
                getLiveData<Pair<List<Tag>?, List<ItemFilterCategory>>>(FilterDialogFragment.SELECTED_FILTER_ITEMS)
                    .observe(viewLifecycleOwner) { value ->
                        if (viewModel.state.selectedTagsList != value.first || viewModel.state.filterCategories != value.second) {
                            viewModel.state.selectedTagsList = value.first
                            viewModel.state.filterCategories = value.second
                            viewModel.updateFilterOptions()
                        }
                    }
            }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(goalHeaderItems) {
                chart.setEntries(it.chartEntries, it.chartEntries)
            }
            observe(navigateTo) { findNavController().navigate(it) }
            observe(reagentItems) {
                reagentAdapter.submitList(it) {
                    reagentAdapter.notifyDataSetChanged()
                }
            }
            observe(todayActivitiesItems) {
                goalPageTodayAdapter.submitList(it) {
                    goalPageTodayAdapter.notifyDataSetChanged()
                }
            }
            observe(recommendations) {
                goalPageRecommendsAdapter.submitList(it) {
                    goalPageRecommendsAdapter.notifyDataSetChanged()
                }
            }
            observe(footerItems) { goalPageFooterAdapter.submitList(it) }
        }
    }
    private fun setupList() {
        rcvPlans.also {
            val adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                goalPageRecommendsAdapter,
                goalPageTodayAdapter,
                reagentAdapter,
                goalPageFooterAdapter
            )
            it.itemAnimator = null
            it.adapter = adapter
        }
    }
}
