package com.vessel.app.wellness.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.databinding.WidgetGoalsButtonsBinding
import com.vessel.app.wellness.model.GoalButton
import java.util.*

class GoalsListWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var goalsList: MutableList<GoalButton> = mutableListOf()
    private lateinit var binding: WidgetGoalsButtonsBinding
    private lateinit var adapter: GoalButtonAdapter

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetGoalsButtonsBinding.inflate(layoutInflater, this, true)
    }

    fun setHandler(goalHandler: GoalButtonAdapter.OnActionHandler) {
        if (::adapter.isInitialized.not()) {
            adapter = GoalButtonAdapter(goalHandler)
            binding.goalsList.adapter = adapter
            enableDragAndDrop(goalHandler)
        }
    }

    fun setList(goals: List<GoalButton>) {
        goalsList = goals.toMutableList()
        adapter.submitList(goalsList)
    }

    private fun enableDragAndDrop(goalHandler: GoalButtonAdapter.OnActionHandler) {
        val dragDirections = ItemTouchHelper.START or ItemTouchHelper.END
        val swipeDirections = ItemTouchHelper.ACTION_STATE_IDLE
        val simpleDragDrop =
            object : ItemTouchHelper.SimpleCallback(dragDirections, swipeDirections) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {

                    val fromPosition = viewHolder.bindingAdapterPosition
                    val toPosition = target.bindingAdapterPosition

                    Collections.swap(goalsList, fromPosition, toPosition)
                    adapter.notifyItemMoved(fromPosition, toPosition)
                    goalHandler.onGoalButtonMoved(fromPosition, toPosition)
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                }
            }
        val touchHelper = ItemTouchHelper(simpleDragDrop)
        touchHelper.attachToRecyclerView(binding.goalsList)
    }
}
