package com.vessel.app.mediaselection

import android.database.Cursor
import android.util.Log
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.helper.*
import com.vessel.app.mediaselection.model.Item
import com.vessel.app.teampage.model.PostType
import com.vessel.app.util.ResourceRepository

class MediaSelectionViewModel@ViewModelInject constructor(
    val state: MediaSelectionState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,

) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    MediaAdapter.OnActionHandler,
    AlbumCollection.AlbumCallbacks,
    AlbumMediaCollection.AlbumMediaCallbacks {
    private var newCursor: Cursor? = null
    private lateinit var albumMediaCollection: AlbumMediaCollection
    private lateinit var albumCollection: AlbumCollection
    var postType: Int? = PostType.PHOTO.ordinal
    fun createAlbumCollection(activity: FragmentActivity, mimeType: Set<MimeType>) {
        SelectionSpec.getInstance().mimeTypeSet = mimeType
        albumCollection = AlbumCollection()
        albumMediaCollection = AlbumMediaCollection()
        albumCollection.onCreate(activity, this)
        albumMediaCollection.onCreate(activity, this)
    }
    override fun onBackButtonClicked(view: View) {
        Log.d(TAG, "onBackButtonClicked: ")
        view.findNavController().navigateUp()
    }

    fun fetchAllMedias() {
        albumCollection.loadAlbums()
    }

    companion object {
        const val TAG = "MediaSelectionViewModel"
    }

    override fun onAlbumLoad(cursor: Cursor?) {
        // Todo load all albums
        val albumList: ArrayList<Album> = arrayListOf()
        cursor?.let {
            for (i in 0..it.count) {
                it.moveToPosition(i)
                val album = Album.valueOf(it)
                if (album.isAll && SelectionSpec.getInstance().capture) {
                    album.addCaptureCount()
                }
                albumList.add(album)
                if (i == 0) {
                    onAlbumSelected(album)
                }
            }
        }
        processAlbums(albumList)
    }

    private fun processAlbums(albumList: ArrayList<Album>) {
        // Todo display on spinner
        state.albums.value = albumList
    }

    private fun onAlbumSelected(album: Album) {
        Log.d(TAG, "onAlbumSelected: $album")
        if (album.isAll && album.isEmpty) {
            // Todo nothing
        } else {
            val selectionSpec = SelectionSpec.getInstance()
            albumMediaCollection.load(album, selectionSpec.capture)
        }
    }

    override fun onAlbumReset() {
    }

    override fun onAlbumMediaLoad(mCursor: Cursor?) {
        val items: ArrayList<Item> = arrayListOf()
        if (newCursor == mCursor) {
            return
        }
        if (mCursor != null) {
            // Todo load media file and display
            newCursor = mCursor
            Log.d(TAG, "onAlbumMediaLoad: $mCursor")
            for (i in 0 until newCursor!!.count) {
                newCursor!!.moveToPosition(i)
                val mediaItem = MediaItem.valueOf(cursor = newCursor!!)
                items.add(Item(mediaItem, false))
            }
        }
        state.medias.value = items
    }

    override fun onAlbumMediaReset() {
    }
    fun destroyLoader() {
        albumCollection.onDestroy()
    }
    fun onNextClicked() {
        state.navigateTo.value = MediaSelectionFragmentDirections.actionMediaSelectionFragmentToCreateNewPostFragment(postType!!)
    }

    override fun onSelect(
        item: Any,
        parentPosition: Int,
        isSelected: Boolean,
        selectedItems: ArrayList<Item>
    ) {
        Log.d(TAG, "onSelect: ${selectedItems.size}")
        state.nextButtonEnabled.value = selectedItems.isNotEmpty()
    }

    override fun onCapture() {
        // Todo open camera with capture
    }
}
