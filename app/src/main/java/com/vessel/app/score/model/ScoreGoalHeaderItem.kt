package com.vessel.app.score.model

import androidx.annotation.StringRes

data class ScoreGoalHeaderItem(@StringRes val title: Int)
