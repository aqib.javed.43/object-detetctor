package com.vessel.app.pretest.preteststep1

import android.os.CountDownTimer
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.NavDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.pretest.preteststep1.ui.PreTestStep1FragmentDirections
import com.vessel.app.util.ResourceRepository

class PreTestStep1ViewModel @ViewModelInject constructor(
    val state: PreTestStep1State,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider), ToolbarHandler {
    fun peeOnACupCheckedListener() {
        navigateTo(PreTestStep1FragmentDirections.actionPreTestStep1FragmentToInCupPeeFragment())
    }

    fun directPeeCheckedListener() {
        navigateTo(PreTestStep1FragmentDirections.actionPreTestStep1FragmentToDirectPeeFragment())
    }

    private fun navigateTo(direction: NavDirections) {
        object : CountDownTimer(DURATION, DURATION) {
            override fun onTick(millisUntilFinished: Long) {
                state.enableCheckBoxes.value = false
            }

            override fun onFinish() {
                state.navigateTo.value = direction
                state.enableCheckBoxes.value = true
                state.peeOnACardChecked.value = false
                state.directPeeChecked.value = false
            }
        }.start()
    }

    override fun onBackButtonClicked(view: View) {
        state.finish.call()
    }

    companion object {
        private const val DURATION = 500L
    }
}
