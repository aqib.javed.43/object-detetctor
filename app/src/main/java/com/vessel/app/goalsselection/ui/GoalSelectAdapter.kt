package com.vessel.app.goalsselection.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.goalsselection.model.GoalSelect

class GoalSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<GoalSelect>({ oldItem, newItem ->
        oldItem.checked == newItem.checked
    }) {
    override fun getLayout(position: Int) = R.layout.item_goal_setting_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onGoalSelectClicked(item: GoalSelect)
    }
}
