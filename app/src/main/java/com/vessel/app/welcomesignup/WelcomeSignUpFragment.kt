package com.vessel.app.welcomesignup

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WelcomeSignUpFragment : BaseFragment<WelcomeSignUpViewModel>() {
    override val viewModel: WelcomeSignUpViewModel by viewModels()
    override val layoutResId = R.layout.fragment_sign_up_welcome
    override fun onViewLoad(savedInstanceState: Bundle?) {
    }
}
