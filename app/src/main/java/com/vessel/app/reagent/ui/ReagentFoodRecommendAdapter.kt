package com.vessel.app.reagent.ui

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.reagent.model.ReagentFoodRecommendation

class ReagentFoodRecommendAdapter(private val handler: HighValueFoodAdapter.OnActionHandler) : BaseAdapterWithDiffUtil<ReagentFoodRecommendation>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is ReagentFoodRecommendation -> R.layout.item_reagent_food_recommendation
        else -> throw IllegalStateException(
            "Unexpected WellnessFooterAdapter type at position $position for item ${getItem(position)}"
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ReagentFoodRecommendation>, position: Int) {
        super.onBindViewHolder(holder, position)
        val highValueFoodAdapter = HighValueFoodAdapter(handler)
        holder.itemView.findViewById<RecyclerView>(R.id.recommendList).apply {
            layoutManager = GridLayoutManager(holder.itemView.context, 2)
            adapter = highValueFoodAdapter
            itemAnimator = null
        }
        val details = getItem(position).reagentDetails
        details?.mapHighValueFoods()?.let {
            highValueFoodAdapter.submitList(it)
        }
        holder.itemView.findViewById<RecyclerView>(R.id.recommendList).visibility = if (details?.mapHighValueFoods().isNullOrEmpty()) View.GONE else View.VISIBLE
        holder.itemView.visibility = if (details?.mapHighValueFoods() == null) View.GONE else View.VISIBLE
        holder.itemView.findViewById<View>(R.id.btnViewFoodPlan).setOnClickListener {
            handler.onViewFoodPlan()
        }
    }

    override fun getHandler(position: Int) = handler
}
