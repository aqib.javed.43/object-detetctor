package com.vessel.app.stressrelief.ui

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.animation.AlphaAnimation
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.util.DisclaimerFooter
import com.vessel.app.common.util.DisclaimerFooterAdapter
import com.vessel.app.checkscience.ui.CheckOutScienceHeaderAdapter
import com.vessel.app.checkscience.model.CheckOutScienceHeaderItem
import com.vessel.app.plan.model.AddPlanFooter
import com.vessel.app.plan.ui.AddPlanFooterAdapter
import com.vessel.app.stressrelief.StressReliefViewModel
import com.vessel.app.util.extensions.addImage
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_food_plan.*
import kotlinx.android.synthetic.main.fragment_stress_relief.*

@AndroidEntryPoint
class StressReliefFragment : BaseFragment<StressReliefViewModel>() {
    override val viewModel: StressReliefViewModel by viewModels()
    override val layoutResId = R.layout.fragment_stress_relief

    private val headerAdapter by lazy { StressReliefHeaderAdapter(viewModel) }
    private val stressReliefDayPlanAdapter by lazy { StressReliefDayAdapter(viewModel) }
    private val footerAdapter = DisclaimerFooterAdapter()
    private val checkOutScienceHeaderAdapter by lazy { CheckOutScienceHeaderAdapter(viewModel) }
    private val addPlanFooterAdapter by lazy {
        AddPlanFooterAdapter(
            viewModel,
            R.string.add_to_your_stress_relief_plan
        )
    }

    private val plansScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            (stressReliefRecyclerView.layoutManager as? LinearLayoutManager)?.findFirstVisibleItemPosition()
                ?.let {
                    if (viewModel.state.showStickyPlanView.value != (it > STRESS_PLAN_LEVEL_POSITION)) {
                        viewModel.state.showStickyPlanView.value = it > STRESS_PLAN_LEVEL_POSITION
                    }
                }
        }
    }

    private val emptyViewAdapter = object : BaseAdapterWithDiffUtil<Spanned>() {
        override fun getLayout(position: Int) = R.layout.item_stress_relief_empty_view
        override fun onBindViewHolder(holder: BaseViewHolder<Spanned>, position: Int) {
            super.onBindViewHolder(holder, position)
            holder.itemView.findViewById<TextView>(R.id.description).text = getItem(position)
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observe(viewModel.state.navigateToReagent) {
            findNavController().navigate(it.destinationId, it.args.toBundle())
        }
        observe(viewModel.state.navigateTo) {
            findNavController().navigate(it)
        }

        setupAdapter()
    }

    private fun setupAdapter() {
        view?.findViewById<RecyclerView>(R.id.stressReliefRecyclerView)?.apply {
            adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                headerAdapter,
                addPlanFooterAdapter,
                stressReliefDayPlanAdapter,
                checkOutScienceHeaderAdapter,
                footerAdapter
            )
            itemAnimator = null
        }

        view?.findViewById<RecyclerView>(R.id.stressReliefRecyclerView)
            ?.addOnScrollListener(plansScrollListener)

        observe(viewModel.state.stressReliefHeader) {
            headerAdapter.submitList(it)
            headerAdapter.notifyDataSetChanged()
        }
        observe(viewModel.state.stressReliefItems) {
            if (it.first.isEmpty()) {
                addPlanFooterAdapter.submitList((listOf(AddPlanFooter)))
                stressReliefDayPlanAdapter.submitList(listOf())
            } else {
                stressReliefDayPlanAdapter.submitList(it.first) {
                    if (it.second) {
                        view?.findViewById<RecyclerView>(R.id.stressReliefRecyclerView)
                            ?.scrollToPosition(1)
                    }
                }
                addPlanFooterAdapter.submitList(listOf())
            }
        }
        footerAdapter.submitList(listOf(DisclaimerFooter.Default)) {
            view?.findViewById<RecyclerView>(R.id.stressReliefRecyclerView)?.scrollToPosition(0)
        }
        checkOutScienceHeaderAdapter.submitList(listOf(CheckOutScienceHeaderItem(viewModel.getCheckOutScienceHeaderContent()))) {
            view?.findViewById<RecyclerView>(R.id.stressReliefRecyclerView)?.scrollToPosition(0)
        }
    }

    private fun getEmptyViewText(): SpannableStringBuilder {
        return requireContext().addImage(
            getString(R.string.choose_stress_relief_plan),
            "[empty-image]",
            R.drawable.ic_add_rounded_button,
            80,
            80
        )
    }

    override fun onDestroyView() {
        view?.findViewById<RecyclerView>(R.id.stressReliefRecyclerView)
            ?.removeOnScrollListener(plansScrollListener)
        super.onDestroyView()
    }

    companion object {
        const val STRESS_PLAN_LEVEL_POSITION = 0
        private const val ANIMATION_DURATION = 1000L
        val fadingInAnimation = AlphaAnimation(0.0f, 1.0f).apply {
            duration = ANIMATION_DURATION
        }
        val fadingOutAnimation = AlphaAnimation(1.0f, 0.0f).apply {
            duration = ANIMATION_DURATION
        }
    }
}
