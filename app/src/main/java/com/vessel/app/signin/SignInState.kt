package com.vessel.app.signin

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class SignInState @Inject constructor(val email: String) {
    val userEmail = MutableLiveData(email)
    val userPassword = MutableLiveData("")
    val closeEvent = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: SignInState.() -> Unit) = apply(block)
}
