
The best way to improve your magnesium levels is with fresh whole foods. Some people may benefit from additional magnesium in the form of supplementation with a bioavailable form of magnesium.

Here’s how we make your food and supplement suggestions. Every 5 years the US government has a team of scientific experts reviewing the literature on nutrition and health to determine the recommended daily intake (RDI) of many different micronutrients and [publish this information](https://health.gov/sites/default/files/2019-09/2015-2020_Dietary_Guidelines.pdf) for free to the public. We start here as the foundation for our recommendations. We then build upon this to customize recommendations for each user based on their Vessel test results. To determine this formula we take into account the expert opinion of our medical and scientific board of doctors, scientists, and registered dietitians looking at the medical evidence for the risks and benefits of being high vs low in a certain nutrient. For example, if a nutrient could be dangerous at high levels (such as potassium), we would never suggest higher than the US recommended daily intake (RDI).

**references:**

-   [https://health.gov/sites/default/files/2019-09/2015-2020_Dietary_Guidelines.pdf](https://health.gov/sites/default/files/2019-09/2015-2020_Dietary_Guidelines.pdf)
