package com.vessel.app.overview.ui

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.overview.OverviewViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OverviewFragment : BaseFragment<OverviewViewModel>() {
    override val viewModel: OverviewViewModel by viewModels()
    override val layoutResId = R.layout.fragment_overview

    override fun onViewLoad(savedInstanceState: Bundle?) {
        requireActivity().onBackPressedDispatcher.addCallback(this) { viewModel.onBackPressed() }

        viewModel.state {
            observe(backToHome) {
                findNavController().navigateUp()
            }
            observe(showForceUpdate) {
                if (it) {
                    findNavController().navigate(R.id.action_overviewFragment_to_forceUpdateDialog)
                }
            }
        }
    }
}
