package com.vessel.app.flex.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.flex.model.MembershipOptionProduct

class MembershipCardAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<MembershipOptionProduct>() {
    override fun getLayout(position: Int) = R.layout.item_membership_card

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onItemSelectClicked(item: MembershipOptionProduct)
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<MembershipOptionProduct>,
        position: Int
    ) {
        super.onBindViewHolder(holder, position)
    }
}
