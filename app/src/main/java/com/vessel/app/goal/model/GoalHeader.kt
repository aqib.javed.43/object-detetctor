package com.vessel.app.goal.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.common.model.DateEntry
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GoalHeader(
    @StringRes val title: Int,
    val chartEntries: List<DateEntry>,
    @DrawableRes val background: Int
) : Parcelable
