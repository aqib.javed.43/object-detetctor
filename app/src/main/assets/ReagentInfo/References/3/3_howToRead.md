Here are the reference ranges we use for urine ketones:


Ketone A (Acetoacetate):

No Ketosis: 0 - 0.75 mmol/L

Low Ketosis: 0.75 - 2 mmol/L

Moderate Ketosis: 2 - 7 mmol/L

High Ketosis: >7 mmol/L


Ketone B (Betahydroxy butryrate)

No Ketosis: 0 - 5 mmol/L

Low Ketosis: 5 - 19 mmol/L

Moderate Ketosis: 19 - 80 mmol/L

High Ketosis: >80 mmol/L


Ketone A and Ketone B combined:

No Ketosis: 0 - 0.75 mmol/L

Low Ketosis: 0.75 - 2 mmol/L

Moderate Ketosis: 2 - 7 mmol/L

High Ketosis: >7 mmol/L


This graph shows you your test dates and where your urine ketone levels fall in our defined ranges of low, good, and high. If you click on the individual test circles, you can see the actual value of urine ketones in mmol/L.

The toggle switch at the top allows you to see Ketone A (Acetoacetate), Ketone B (Beta-hydroxybutyrate), or the sum of both ketones. Read more about the difference in the “why it matters” section below.

If you are not following a ketogenic diet, a normal and healthy value would be low ketones.

Because these values are most relevant for people who are interested in adhering to a ketogenic diet who want to track their progress, we have this section greyed out for those people not interested in following a ketogenic diet at this time.

If you want to try a ketogenic diet, read on:

Nutritional ketosis starts around 0.5 mmol/L, called light nutritional ketosis, and is a good starting point for beginners.

From there, aim for “optimal nutritional ketosis,” which is when your ketone levels are between 1.0 mmol/L-3.0 mmol/L, which is the goal for most people.

Sometimes people choose a ketogenic diet for therapeutic benefits for conditions such as epilepsy, cancer, and metabolic disorders. The goals in these cases are often higher, in the range of 3.0 mmol/L to 5.0 mmol/L called “heavy nutritional ketosis”. Please only attempt this under a doctor’s supervision.

Levels over 5.0mmol/L are higher than what you need to get the benefits of nutritional ketosis, and levels over 10.0 mmol/L can be a sign of something dangerous called ketoacidosis, which is usually seen in people with Type I diabetes or severe alcoholism.

**references:**

-   [https://www.ncbi.nlm.nih.gov/pubmed?term=11344564](https://www.ncbi.nlm.nih.gov/pubmed?term=11344564)

-   [https://www.ncbi.nlm.nih.gov/pubmed/759825](https://www.ncbi.nlm.nih.gov/pubmed/759825)

-   Wu AHB. Tietz clinical guide to laboratory tests. 4th ed. St. Louis, Mo: Saunders/Elsevier; 2006.

-   Williamson MA, Snyder LM, Wallach JB. Wallach's interpretation of diagnostic tests. 9th ed. Wolters Kluwer/Lippincott Williams & Wilkins Health: Philadelphia; 2011.

-   [https://www.ncbi.nlm.nih.gov/pubmed?term=23670253](https://www.ncbi.nlm.nih.gov/pubmed?term=23670253)

Pediatric Reference Range Sources:

-   [https://www.ucsfbenioffchildrens.org/tests/003585.html](https://www.ucsfbenioffchildrens.org/tests/003585.html)

-   [https://sites.magellanhealth.com/library/HIE%20Multimedia/1/003585.htm](https://sites.magellanhealth.com/library/HIE%20Multimedia/1/003585.htm)
