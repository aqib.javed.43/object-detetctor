package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ProgramPayLoadNew(
    val notes: String,
    val contact_id: Int,
    val date: String,
    val subgoals_ids: Array<Int>,
    val bad_habits_ids: Array<Int>

)
