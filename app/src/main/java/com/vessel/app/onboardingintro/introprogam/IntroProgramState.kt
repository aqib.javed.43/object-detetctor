package com.vessel.app.onboardingintro.introprogam

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class IntroProgramState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val isJoinedProgram = LiveEvent<Boolean>()
    operator fun invoke(block: IntroProgramState.() -> Unit) = apply(block)
}
