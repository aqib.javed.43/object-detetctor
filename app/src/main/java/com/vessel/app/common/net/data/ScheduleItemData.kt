package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ScheduleItemData(
    val day: Int,
    val plans: List<SchedulePlanItemData>
)
