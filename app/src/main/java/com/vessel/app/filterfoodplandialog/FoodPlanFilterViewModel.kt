package com.vessel.app.filterfoodplandialog

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.filterfoodplandialog.model.*
import com.vessel.app.filterfoodplandialog.ui.FilterFooterAdapter
import com.vessel.app.filterfoodplandialog.ui.FilterSelectAdapter
import com.vessel.app.filterfoodplandialog.ui.FoodPlanFilterDialog
import com.vessel.app.filterfoodplandialog.ui.FoodPlanFilterDialogDirections
import com.vessel.app.util.ResourceRepository

class FoodPlanFilterViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    val contactManager: ContactManager,
) : BaseViewModel(resourceProvider),
    FilterSelectAdapter.OnActionHandler,
    FilterFooterAdapter.OnActionHandler {
    val state =
        FoodPlanFilterState(savedStateHandle[FoodPlanFilterDialog.SELECTED_FILTER_OPTIONS]!!)

    init {
        loadFoodCategoryList()
    }

    private fun loadFoodCategoryList() {

        state.sortHeaderItem.value = FilterHeaderItem(
            getResString(FoodFilterAndSortOptions.SORT.title)
        )

        state.sortOptionsList.value = SortOptions.values().map {
            FilterSelect(
                it.title,
                state.selectedFilterAndSortOptions.sortOptions.title == it.title,
                FoodFilterAndSortOptions.SORT
            )
        }

        state.foodCategoriesHeader.value = FilterHeaderItem(
            getResString(FoodFilterAndSortOptions.CATEGORY.title)
        )

        state.foodCategoriesList.value = FoodCategory.values().map {
            FilterSelect(
                it.title,
                state.selectedFilterAndSortOptions.categories.contains(it),
                FoodFilterAndSortOptions.CATEGORY
            )
        }

        state.nutrientHeaderItem.value = FilterHeaderItem(
            getResString(FoodFilterAndSortOptions.NUTRIENT.title)
        )

        state.nutrientOptionsList.value = FilterNutrient.values().map {
            FilterSelect(
                it.title,
                state.selectedFilterAndSortOptions.nutrients.contains(it),
                FoodFilterAndSortOptions.NUTRIENT
            )
        }
    }

    fun onCloseDialogClicked() {
        state.dismissDialog.call()
    }

    override fun onManageDietAllergiesClicked() {
        state.navigateTo.postValue(
            FoodPlanFilterDialogDirections.actionFoodPlanFilterDialogToFoodPreferencesFragment(
                getResString(
                    R.string.manage_diet_allergies
                )
            )
        )
    }

    override fun onFilterSelectChecked(item: FilterSelect, checked: Boolean) {
        state {
            when (item.type) {
                FoodFilterAndSortOptions.CATEGORY ->
                    onFoodCategorySelected(selectedFilterAndSortOptions, item, checked)
                FoodFilterAndSortOptions.SORT ->
                    onSortOptionSelected(selectedFilterAndSortOptions, item, checked)
                FoodFilterAndSortOptions.NUTRIENT ->
                    onNutrientSelected(selectedFilterAndSortOptions, item, checked)
            }
        }
    }

    private fun onFoodCategorySelected(
        selectedFilterAndSortOptions: FilterAndSortDialogOptions,
        item: FilterSelect,
        checked: Boolean
    ) {
        FoodCategory.values().firstOrNull { it.title == item.title }?.let {
            if (checked) {
                selectedFilterAndSortOptions.categories.add(it)
            } else {
                selectedFilterAndSortOptions.categories.remove(it)
            }
        }
        state.foodCategoriesList.value = state.foodCategoriesList.value.orEmpty().mapIndexed { index, selectedFilterItem ->
            if (index == state.foodCategoriesList.value?.indexOf(item)) {
                selectedFilterItem.copy(checked = checked)
            } else {
                selectedFilterItem
            }
        }
    }

    private fun onSortOptionSelected(
        selectedFilterAndSortOptions: FilterAndSortDialogOptions,
        item: FilterSelect,
        checked: Boolean
    ) {
        when (item.title) {
            SortOptions.RECOMMENDED.title ->
                selectedFilterAndSortOptions.sortOptions = if (checked) SortOptions.RECOMMENDED else SortOptions.A_Z

            SortOptions.A_Z.title ->
                selectedFilterAndSortOptions.sortOptions = if (checked) SortOptions.A_Z else SortOptions.RECOMMENDED
        }

        state.sortOptionsList.value = state.sortOptionsList.value.orEmpty().mapIndexed { index, selectedFilterItem ->
            if (index == state.sortOptionsList.value?.indexOf(item)) {
                selectedFilterItem.copy(checked = checked)
            } else {
                selectedFilterItem.copy(checked = !checked)
            }
        }
    }

    private fun onNutrientSelected(
        selectedFilterAndSortOptions: FilterAndSortDialogOptions,
        item: FilterSelect,
        checked: Boolean
    ) {
        FilterNutrient.values().firstOrNull { it.title == item.title }?.let {
            if (checked) {
                selectedFilterAndSortOptions.nutrients.clear()
                selectedFilterAndSortOptions.nutrients.add(it)
            } else {
                selectedFilterAndSortOptions.nutrients.clear()
            }
        }

        state.nutrientOptionsList.value = state.nutrientOptionsList.value.orEmpty().mapIndexed { index, selectedFilterItem ->
            if (index == state.nutrientOptionsList.value?.indexOf(item)) {
                selectedFilterItem.copy(checked = checked)
            } else {
                selectedFilterItem.copy(checked = false)
            }
        }
    }
}
