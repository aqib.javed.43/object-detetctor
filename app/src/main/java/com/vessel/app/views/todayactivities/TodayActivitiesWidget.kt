package com.vessel.app.views.todayactivities

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.common.binding.setVisible
import com.vessel.app.databinding.WidgetTodayActivitiesBinding
import com.vessel.app.plan.model.PlanItem
import com.vessel.app.util.extensions.formatDayMonthName
import java.util.*

class TodayActivitiesWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var planItems: List<PlanItem> = listOf()
    private lateinit var binding: WidgetTodayActivitiesBinding
    private var handler: TodayActivitiesWidgetOnActionHandler? = null
    private lateinit var todayActivitiesAdapter: TodayActivitiesAdapter

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetTodayActivitiesBinding.inflate(layoutInflater, this, true)
    }

    fun setHandler(mHandler: TodayActivitiesWidgetOnActionHandler) {
        handler = mHandler
        binding.addActivity.setOnClickListener { handler?.onActivitiesClick() }
        if (::todayActivitiesAdapter.isInitialized.not()) {
            todayActivitiesAdapter = TodayActivitiesAdapter(mHandler)
            binding.plans.apply {
                adapter = todayActivitiesAdapter
                itemAnimator = null
            }
        }
    }

    fun setList(items: List<PlanItem>) {
        planItems = items
        binding.addActivity.setVisible(planItems.isNullOrEmpty())
        todayActivitiesAdapter.submitList(planItems)
        todayActivitiesAdapter.notifyDataSetChanged()
        val activeCount = planItems.count { it.isCompleted }
        val hint = Calendar.getInstance().time.formatDayMonthName()
        binding.stepperView.apply {
            setStepsCount(planItems.size, activeCount)
            setActiveStep(activeCount)
            setProgressHint(hint)
        }
    }

    fun loading(enableLoading: Boolean) {
        binding.loader.setVisible(enableLoading)
    }
}

interface TodayActivitiesWidgetOnActionHandler {
    fun onActivitiesClick()
    fun onPlanItemClicked(item: Any, view: View)
    fun onItemSelected(item: Any, position: Int)
    fun onAddReminderClicked(item: Any, view: View)
    fun onAnimationFinished(item: Any)
}
