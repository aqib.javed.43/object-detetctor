package com.vessel.app.util.validators

import android.util.Patterns
import androidx.lifecycle.LiveData
import com.wajahatkarim3.easyvalidation.core.view_ktx.maxLength
import com.wajahatkarim3.easyvalidation.core.view_ktx.minLength

class InvalidEmailException : ValidatorException("Please enter a valid email address.")

class EmailValidator : BaseValidator<String>() {

    override fun error(): ValidatorException = InvalidEmailException()

    override fun validate(value: LiveData<String>): Boolean {
        val email = value.value ?: throw error()
        return validate(email)
    }

    override fun validate(value: String): Boolean {
        return if (betterValidate(value) && value.minLength(5) && value.maxLength(240)) {
            true
        } else {
            throw error()
        }
    }

    private fun betterValidate(text: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(text).matches()
    }
}
