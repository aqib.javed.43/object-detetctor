package com.vessel.app.wellness.filterpopup

import android.os.Parcelable
import androidx.annotation.DrawableRes
import com.vessel.app.R
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemFilterCategory(
    val title: String?,
    val choiceItems: List<ItemFilterChoice>,
    val singleSelection: Boolean = false,
    val visibleInHome: Boolean = true
) : Parcelable

@Parcelize
data class ItemFilterChoice(val title: String, val isChecked: Boolean, val itemFilterChoiceType: ItemFilterChoiceType? = null) :
    Parcelable {
    @DrawableRes
    @IgnoredOnParcel
    val selectItemBackground: Int = if (isChecked) {
        R.drawable.white_transparent70_background
    } else {
        R.drawable.rounded_white_alpha
    }

    @DrawableRes
    @IgnoredOnParcel
    val selectItemImage: Int = if (isChecked) {
        R.drawable.ic_radio_selected
    } else {
        R.drawable.ic_radio_normal
    }
}

enum class ItemFilterChoiceType {
    NEWEST, HIGHEST_RATE, RECOMMENDATION
}
