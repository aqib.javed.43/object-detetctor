package com.vessel.app.createpost

import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_teams.*

@AndroidEntryPoint
class CreatePostFragment : BaseFragment<CreatePostViewModel>() {
    override val viewModel: CreatePostViewModel by viewModels()
    override val layoutResId: Int = R.layout.fragment_create_post
    private lateinit var myTeamView: ConstraintLayout
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
