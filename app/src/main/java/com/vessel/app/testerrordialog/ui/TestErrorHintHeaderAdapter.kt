package com.vessel.app.testerrordialog.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.testerrordialog.model.TestErrorHintHeader

class TestErrorHintHeaderAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<TestErrorHintHeader>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is TestErrorHintHeader -> R.layout.item_test_hint_header
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any? = handler

    interface OnActionHandler {
        fun onDoneClicked()
    }
}
