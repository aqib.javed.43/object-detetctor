package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AssessmentData(
    val value: Int,
    val goal: GoalData,
    val created_at: String
)
