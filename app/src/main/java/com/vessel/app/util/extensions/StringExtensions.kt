package com.vessel.app.util.extensions

import android.text.*
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView

fun String.makeClickableSpan(vararg links: Pair<String, View.OnClickListener>): SpannableString {
    val spannableString = SpannableString(this)
    for (link in links) {
        val clickableSpan = object : ClickableSpan() {

            override fun updateDrawState(textPaint: TextPaint) {
                textPaint.isUnderlineText = true
            }

            override fun onClick(view: View) {
                Selection.setSelection((view as TextView).text as Spannable, 0)
                view.invalidate()
                link.second.onClick(view)
            }
        }
        val startIndexOfLink = this.indexOf(link.first)
        spannableString.setSpan(
            clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    return spannableString
}
