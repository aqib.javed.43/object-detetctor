package com.vessel.app.foodplan.model

data class FoodDailyItem(
    val title: String,
    val items: List<FoodPlanItem>,
    val isDailyItem: Boolean
)
