// TODO fix the unit test

// package com.vessel.app.common.manager
//
// import com.vessel.app.BaseFaker.Fake.FAKER
// import com.vessel.app.BaseTest
// import com.vessel.app.common.model.*
// import com.vessel.app.isEqualTo
// import com.vessel.app.wellness.ScoreFaker
// import com.vessel.app.wellness.WellnessEntryFaker
// import com.vessel.app.wellness.model.Score
// import com.vessel.app.wellness.repo.ScoreRepository
// import io.mockk.coEvery
// import io.mockk.mockk
// import org.assertj.core.api.Assertions.assertThat
// import org.assertj.core.api.Assertions.entry
// import org.junit.jupiter.api.BeforeEach
// import org.junit.jupiter.api.Test
//
// class ScoreManagerTest : BaseTest() {
//    private lateinit var scoreManager: ScoreManager
//
//    private val scoreRepository = mockk<ScoreRepository>()
//
//    @BeforeEach
//    fun setUp() {
//        scoreManager = ScoreManager(scoreRepository)
//    }
//
//    @Test
//    fun `it should return the repository wellness scores`() = runBlockingTest {
//        val score = ScoreFaker.basic()
//        coEvery { scoreRepository.getWellnessScores() } returns Result.Success(score)
//
//        (scoreManager.getWellnessScores() as Result.Success).value.run {
//            wellness isEqualTo score.wellness
//            reagents isEqualTo score.reagents
//            assertThat(goals).containsKeys(
//                Goal.Beauty,
//                Goal.Body,
//                Goal.Calm,
//                Goal.Digestion,
//                Goal.Energy,
//                Goal.Endurance,
//                Goal.Focus,
//                Goal.Immunity,
//                Goal.Mood,
//                Goal.Sleep
//            )
//        }
//    }
//
//    @Test
//    fun `it should map the reagents deficiencies`() = runBlockingTest {
//        val reagents = mapOf(
//            Reagent.VitaminC to entries(50f),
//            Reagent.B7 to entries(25f),
//            Reagent.B9 to entries(10f),
//            Reagent.Magnesium to entries(50f),
//        )
//        val score = Score(WellnessEntryFaker.list(), reagents)
//        coEvery { scoreRepository.getWellnessScores() } returns Result.Success(score)
//
//        val deficiencies = scoreManager.reagentDeficiencies()
//
//        assertThat((deficiencies as Result.Success).value).containsExactly(
//            entry(Reagent.VitaminC, 360f),
//            entry(Reagent.B7, 90f),
//            entry(Reagent.B9, 800f),
//            entry(Reagent.Magnesium, 800f)
//        )
//    }
//
//    private fun entries(y: Float) = listOf(
//        ReagentEntry(
//            x = FAKER.number().randomNumber().toFloat(),
//            y = y,
//            score = FAKER.number().randomNumber().toFloat(),
//            date = null
//        )
//    )
// }
