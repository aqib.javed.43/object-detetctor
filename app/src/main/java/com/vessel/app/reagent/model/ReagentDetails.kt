package com.vessel.app.reagent.model

import com.squareup.moshi.JsonClass
import com.vessel.app.common.model.data.ReagentItem

@JsonClass(generateAdapter = true)
data class ReagentDetails(
    val reagentID: Int,
    val name: String,
    val benefitsOfBeingOptimal: List<BenefitsOfBeingOptimal>,
    val highValueFoods: List<HighValueFood>? = null,
    val lowRecommendations: ReagentScreens? = null,
    val highRecommendations: ReagentScreens? = null
) {
    fun getFoodRecommendationTitle(): String {
        var title = ""
        highValueFoods?.let {
            title = "$name Boosting Foods"
        }
        return title
    }
    fun mapHighValueFoods(): List<HighValueFood>? {
        if (highValueFoods == null) return null
        val filter = when (reagentID) {
            ReagentItem.Calcium.id -> highValueFoods.filter {
                it.calcium != null
            }.map {
                it.copy(reagentName = name, amount = it.calcium.toString())
            }
            ReagentItem.VitaminC.id -> highValueFoods.filter {
                it.vitaminC != null
            }.map {
                it.copy(reagentName = name, amount = it.vitaminC.toString())
            }
            ReagentItem.Magnesium.id -> highValueFoods.filter {
                it.magnesium != null
            }.map {
                it.copy(reagentName = name, amount = it.magnesium.toString())
            }
            ReagentItem.B7.id -> highValueFoods.filter {
                it.b7 != null
            }.map {
                it.copy(reagentName = name, amount = it.b7.toString())
            }
            ReagentItem.B9.id -> highValueFoods.filter {
                it.b9 != null
            }.map {
                it.copy(reagentName = name, amount = it.b9.toString())
            }
            else -> null
        }
        return filter
    }
}

@JsonClass(generateAdapter = true)
data class ReagentScreens(
    val screens: List<ReagentScreen>? = null,
    val final: ReagentScreen? = null
)

@JsonClass(generateAdapter = true)
data class ReagentScreen(
    val title: String?,
    val subTitle: String?
)
data class ReagentFoodRecommendation(
    val reagentDetails: ReagentDetails? = null
)
