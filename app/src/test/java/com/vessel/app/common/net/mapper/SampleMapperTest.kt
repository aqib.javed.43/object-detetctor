// TODO fix the unit test

// package com.vessel.app.common.net.mapper
//
// import com.vessel.app.BaseTest
// import com.vessel.app.common.model.Reagent
// import com.vessel.app.isEqualTo
// import com.vessel.app.sample.SampleFaker
// import org.assertj.core.api.Assertions.assertThat
// import org.junit.jupiter.api.BeforeEach
// import org.junit.jupiter.api.Test
//
// class SampleMapperTest : BaseTest() {
//    private lateinit var sampleMapper: SampleMapper
//
//    @BeforeEach
//    fun setUp() {
//        sampleMapper = SampleMapper()
//    }
//
//    @Test
//    fun `it should map the sample data to the domain model`() {
//        val data = SampleFaker.data(Reagent.Magnesium.apiName)
//
//        val sample = sampleMapper.map(data)
//
//        assertThat(sample!!.reagent).isEqualTo(Reagent.Magnesium)
//        assertThat(sample.amount).isEqualTo(data.quantity)
//    }
//
//    @Test
//    fun `it should map the sample data list to the domain model`() {
//        val data = listOf(
//            SampleFaker.data(Reagent.Magnesium.apiName),
//            SampleFaker.data(Reagent.VitaminC.apiName),
//            SampleFaker.data(Reagent.B7.apiName),
//            SampleFaker.data(Reagent.B9.apiName)
//        )
//
//        val samples = sampleMapper.map(data)
//
//        samples[0].reagent isEqualTo Reagent.Magnesium
//        samples[0].amount isEqualTo data[0].quantity
//        samples[1].reagent isEqualTo Reagent.VitaminC
//        samples[1].amount isEqualTo data[1].quantity
//        samples[2].reagent isEqualTo Reagent.B7
//        samples[2].amount isEqualTo data[2].quantity
//        samples[3].reagent isEqualTo Reagent.B9
//        samples[3].amount isEqualTo data[3].quantity
//    }
// }
