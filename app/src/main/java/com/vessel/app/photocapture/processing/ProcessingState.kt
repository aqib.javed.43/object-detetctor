package com.vessel.app.photocapture.processing

import androidx.lifecycle.MutableLiveData
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.net.data.ReagentData
import javax.inject.Inject

class ProcessingState @Inject constructor() {
    var uuid: String? = null
    var getScoreTryCount = 0
    val goToPostTest = LiveEvent<Unit>()
    val goToLFAErrorView = LiveEvent<List<ReagentData>>()
    val showErrorDialog = MutableLiveData<ProcessingErrorUiModel>()
    val uploadProgress = MutableLiveData(0)
    val showUploadProgress = MutableLiveData(false)
    val operationTitle = MutableLiveData<Int>()

    operator fun invoke(block: ProcessingState.() -> Unit) = apply(block)
}
