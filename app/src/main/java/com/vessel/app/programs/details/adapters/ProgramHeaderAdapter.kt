package com.vessel.app.programs.details.adapters

import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Program

class ProgramHeaderAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<Program>() {

    override fun getLayout(position: Int) = R.layout.item_program_header
    override fun getHandler(position: Int): Any = handler
    override fun onBindViewHolder(holder: BaseViewHolder<Program>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position)
        val remainingLengthLabel =
            holder.itemView.findViewById<AppCompatTextView>(R.id.remainingLengthLabel)
        val remainingLengthValue =
            holder.itemView.findViewById<AppCompatTextView>(R.id.remainingLengthValue)
        if (item.isEnrolled) {
            if (item.isEnded()) {
                remainingLengthLabel.text =
                    holder.itemView.context.getString(R.string.program_is_over)
                remainingLengthValue.isVisible = false
            } else if (item.isInLastDay()) {
                remainingLengthLabel.text = holder.itemView.context.getString(R.string.las_day)
                remainingLengthValue.isVisible = false
            } else {
                remainingLengthValue.text = holder.itemView.context.resources.getQuantityString(
                    R.plurals.remaining_days,
                    item.getRemainingPeriod(),
                    item.getRemainingPeriod()
                )
                remainingLengthValue.isVisible = true
            }
        }
        holder.itemView.findViewById<AppCompatTextView>(R.id.description).text =
            holder.itemView.context.getString(R.string.by, getItem(position).contactFullName)
    }

    interface OnActionHandler {
        fun onJoinedTheProgramClicked(item: Program)
        fun onViewYourPlanClicked(item: Program)
        fun onItemLikeClicked(item: Program)
        fun onExpertClicked()
    }
}
