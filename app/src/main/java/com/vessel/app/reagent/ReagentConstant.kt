package com.vessel.app.reagent

object ReagentConstant {
    private const val FILE_TEMPLATE_ROOT = "ReagentInfo/References"
    const val FILE_TEMPLATE_JSON = "$FILE_TEMPLATE_ROOT/%s/%s.json"
    const val FILE_TEMPLATE_HOW_TO_READ = "$FILE_TEMPLATE_ROOT/%s/%s_howToRead.md"
    const val FILE_TEMPLATE_INTERACTIONS_WITH_MEDICATIONS =
        "$FILE_TEMPLATE_ROOT/%s/%s_interactionsWithMedications.md"
    const val FILE_TEMPLATE_TIPS_TO_IMPROVE = "$FILE_TEMPLATE_ROOT/%s/%s_tipsToImprove.md"
    const val FILE_TEMPLATE_WHY_IT_MATTERS = "$FILE_TEMPLATE_ROOT/%s/%s_whyItMatters.md"
    const val FILE_TEMPLATE_BACKEND_BY_SCIENCE = "$FILE_TEMPLATE_ROOT/%s/%s_backedByScience.md"

    // Ids for the reagents that have medications information
    val SUPPORTED_MEDICATIONS_REAGENTS = listOf(3, 4, 5, 6, 8)
}
