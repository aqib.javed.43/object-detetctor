package com.vessel.app.views.weekdays

import android.content.Context
import com.vessel.app.R
import java.util.*

data class DayOfWeekUiModel(val day: String, var calendarDay: Int, val planIndex: Int, var isSelected: Boolean = true)

fun getDaysOfWeek(context: Context) = listOf(
    DayOfWeekUiModel(context.getString(R.string.m), Calendar.MONDAY, 0),
    DayOfWeekUiModel(context.getString(R.string.t), Calendar.TUESDAY, 1),
    DayOfWeekUiModel(context.getString(R.string.w), Calendar.WEDNESDAY, 2),
    DayOfWeekUiModel(context.getString(R.string.t), Calendar.THURSDAY, 3),
    DayOfWeekUiModel(context.getString(R.string.f), Calendar.FRIDAY, 4),
    DayOfWeekUiModel(context.getString(R.string.s), Calendar.SATURDAY, 5),
    DayOfWeekUiModel(context.getString(R.string.s), Calendar.SUNDAY, 6)
)

enum class AppDayOfWeek(val value: Int) {
    MONDAY(0), TUESDAY(1), WEDNESDAY(2), THURSDAY(3), FRIDAY(4), SATURDAY(5), SUNDAY(6);

    fun next() = when (value) {
        MONDAY.value -> TUESDAY
        TUESDAY.value -> WEDNESDAY
        WEDNESDAY.value -> THURSDAY
        THURSDAY.value -> FRIDAY
        FRIDAY.value -> SATURDAY
        SATURDAY.value -> SUNDAY
        SUNDAY.value -> MONDAY
        else -> MONDAY
    }

    companion object {
        fun getAppDayOfWeekByCalenderDayOfWeek(calenderDayOfWeek: Int) =
            when (calenderDayOfWeek) {
                Calendar.MONDAY -> MONDAY
                Calendar.TUESDAY -> TUESDAY
                Calendar.WEDNESDAY -> WEDNESDAY
                Calendar.THURSDAY -> THURSDAY
                Calendar.FRIDAY -> FRIDAY
                Calendar.SATURDAY -> SATURDAY
                Calendar.SUNDAY -> SUNDAY
                else -> MONDAY
            }
    }
}
