package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class LikePayLoad(
    val record_id: Int,
    val category: String,
    val status: String
)
