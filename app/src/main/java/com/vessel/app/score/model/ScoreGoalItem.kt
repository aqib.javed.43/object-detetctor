package com.vessel.app.score.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class ScoreGoalItem(
    val scoreValue: Int,
    @StringRes val title: Int,
    @DrawableRes val background: Int,
    @DrawableRes val image: Int
) {
    val score = scoreValue.toString()
}
