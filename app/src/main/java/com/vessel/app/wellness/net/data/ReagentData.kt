package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ReagentData(
    val reagent_id: Int,
    val score: Float? = null,
    val value: Float? = null
)
