package com.vessel.app.teampage.model

import android.util.Log
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.vessel.app.wellness.net.data.FeedData
import com.vessel.app.wellness.net.data.PollQuestions
import java.io.Serializable

data class TeamPostItem(
    var id: String? = null,
    var title: String = "",
    var actor: String = "",
    var image: ArrayList<String> = arrayListOf(),
    var isLiked: Boolean = false,
    var likeCounts: Int = 26,
    var type: PostType = PostType.TEXT,
    var isMine: Boolean = false,
    var time: String = "",
    var videoUrl: String = "",
    var pollQuestions: PollQuestions? = null,
    var custompr: String = "",
    var isFollowing: Boolean = false,
    var pollResponse: Boolean = false,
) : Serializable {
    fun valueOf(feedData: FeedData): TeamPostItem {
        val teamPostItem = TeamPostItem()
        teamPostItem.id = feedData.id
        feedData.actor?.let {
            teamPostItem.actor = it
        }

        teamPostItem.type = when (feedData.verb) {
            "poll" -> PostType.POLL
            "post" -> PostType.TEXT
            else -> PostType.TEXT
        }
        feedData.text?.let {
            teamPostItem.title = it
            teamPostItem.type = PostType.TEXT
        }

        if (teamPostItem.type == PostType.POLL) {
            feedData.pollQuestions?.let {
                val moshi: Moshi = Moshi.Builder().build()
                val adapter: JsonAdapter<PollQuestions> = moshi.adapter(PollQuestions::class.java)
                if (it is String) {
                    teamPostItem.pollQuestions = adapter.fromJson(it)
                } else {
                    try {
                        teamPostItem.pollQuestions = adapter.fromJsonValue(it)
                    } catch (e: Exception) {
                        Log.e("TAG", "valueOf: $e")
                    }
                }
            }
        } else {
            feedData.imgURL1?.let {
                teamPostItem.image.add(it)
            }
            feedData.imgURL2?.let {
                teamPostItem.image.add(it)
            }
            feedData.imgURL3?.let {
                teamPostItem.image.add(it)
            }
        }
        if (teamPostItem.image.isNotEmpty()) {
            teamPostItem.type = PostType.PHOTO
        }
        feedData.videoURL?.let {
            teamPostItem.type = PostType.VIDEO
            teamPostItem.videoUrl = it
        }
        feedData.custompr?.let {
            teamPostItem.custompr = it
        }
        teamPostItem.time = feedData.time.toString()
        return teamPostItem
    }
}
enum class PostType(type: Int) {
    TEXT(0), PHOTO(1), VIDEO(2), POLL(3)
}
