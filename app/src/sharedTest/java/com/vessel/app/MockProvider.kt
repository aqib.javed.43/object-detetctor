package com.vessel.app

import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic

object MockProvider {
    fun mockedNavigation(): Pair<View, NavController> {
        val view = mockk<View>()
        val navController = mockk<NavController>(relaxed = true)
        mockkStatic(Navigation::class)
        every { Navigation.findNavController(view) } returns navController

        return view to navController
    }
}
