package com.vessel.app.common.net.mapper

import com.vessel.app.*
import com.vessel.app.todo.TodoFaker
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TodosMapperTest : BaseTest() {
    private lateinit var mapper: TodosMapper

    @BeforeEach
    fun setUp() {
        mapper = TodosMapper()
    }

    @Test
    fun map() {
        val data = TodoFaker.dataList()

        mapper.map(data).forEachIndexed { index, todo ->
            todo.id isEqualTo data[index].id
            todo.name isEqualTo data[index].name
            todo.quantity isEqualTo data[index].quantity
            Constants.SERVER_INSERT_DATE_FORMAT.format(todo.insertDate!!) isEqualTo data[index].insert_date
            todo.usdaNdbNumber isEqualTo data[index].usda_ndb_number
        }
    }

    @Test
    fun mapToData() {
        val value = TodoFaker.basic()

        mapper.mapToData(value).apply {
            this.id isEqualTo value.id
            this.name isEqualTo value.name
            this.quantity isEqualTo value.quantity
            this.insert_date!! isEqualTo Constants.SERVER_INSERT_DATE_FORMAT.format(value.insertDate!!)
            this.usda_ndb_number isEqualTo value.usdaNdbNumber
        }
    }
}
