package com.vessel.app.today.selectprogram

import androidx.navigation.NavDirections
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ProgramSelectState @Inject constructor() {
    val items = LiveEvent<List<GoalSelect>>()
    val refreshItems = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()
    val backToHome = LiveEvent<Unit>()
    operator fun invoke(block: ProgramSelectState.() -> Unit) = apply(block)
}
