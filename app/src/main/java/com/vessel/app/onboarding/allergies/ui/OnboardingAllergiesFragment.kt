package com.vessel.app.onboarding.allergies.ui

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.onboarding.allergies.OnboardingAllergiesViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingAllergiesFragment : BaseFragment<OnboardingAllergiesViewModel>() {
    override val viewModel: OnboardingAllergiesViewModel by viewModels()
    override val layoutResId = R.layout.fragment_allergies_onboarding

    val onboardingAllergiesSelectAdapter by lazy { OnboardingAllergiesSelectAdapter(viewModel) }
    private lateinit var backButton: View
    override fun onViewLoad(savedInstanceState: Bundle?) {
        backButton = requireView().findViewById(R.id.toolbar)
        if (viewModel.isBackVisible == true) {
            backButton.isVisible = false
            requireActivity().onBackPressedDispatcher.addCallback(this) {}
        }
        view?.findViewById<ConstraintLayout>(R.id.parentView)?.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                if (viewModel.state.isFirstTestFlow) {
                    R.color.yellow_light
                } else {
                    R.color.colorPrimary
                }
            )
        )

//        view?.findViewById<View>(R.id.backgroundImage)?.isVisible = viewModel.state.isFirstTestFlow.not()

        val allergies = requireView().findViewById<RecyclerView>(R.id.allergies)
        allergies.layoutManager = GridLayoutManager(context, Constants.SPAN_COUNT_GOAL_GRID)
        allergies.adapter = onboardingAllergiesSelectAdapter
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(items) {
                onboardingAllergiesSelectAdapter.submitList(it)
            }
            observe(refreshItems) {
                onboardingAllergiesSelectAdapter.notifyDataSetChanged()
            }
        }
    }
}
