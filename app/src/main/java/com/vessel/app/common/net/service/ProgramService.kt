package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.*
import retrofit2.http.*

interface ProgramService {
    @GET("program")
    suspend fun getProgramsByGoal(
        @Query("goal_id") goalId: Int,
        @Query("per_page") perPage: Int?,
        @Query("page") pageNumber: Int?
    ): ProgramResponseData

    @POST("program/{programId}/enroll")
    suspend fun joinProgram(
        @Path("programId") programId: Int,
        @Body payload: ProgramPayload
    )

    @POST("program/{programId}/enroll")
    suspend fun joinProgramString(
        @Path("programId") programId: Int,
        @Body payload: ProgramPayLoadNew
    )

    @GET("program/{programId}")
    suspend fun getProgramDetails(
        @Path("programId") programId: Int
    ): ProgramData

    @GET("program")
    suspend fun getPrograms(): ProgramResponseData

    @GET("program/schedule")
    suspend fun getProgramSchedule(): ScheduleResponseData

    @POST("program/{programId}/complete")
    suspend fun completeProgram(
        @Path("programId") programId: Int,
        @Body payload: CompleteProgramPayload
    )
    @POST("program/{programId}/summary")
    suspend fun getProgramSummary(
        @Path("programId") programId: Int
    ): ProgramSummaryData
}
