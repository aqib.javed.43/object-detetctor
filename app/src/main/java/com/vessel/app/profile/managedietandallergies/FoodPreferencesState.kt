package com.vessel.app.profile.managedietandallergies

import androidx.lifecycle.MutableLiveData
import com.vessel.app.profile.managedietandallergies.model.FoodPreferencesPage
import com.vessel.app.profile.managedietandallergies.model.FoodPreferencesSelect
import javax.inject.Inject

class FoodPreferencesState @Inject constructor(pageTitle: String) {
    val currentTitle = MutableLiveData(pageTitle)
    val pages = MutableLiveData<MutableList<FoodPreferencesPage>>()
    val tabList = MutableLiveData<List<FoodPreferencesSelect>>()
    val position = MutableLiveData(0)
    operator fun invoke(block: FoodPreferencesState.() -> Unit) = apply(block)
}
