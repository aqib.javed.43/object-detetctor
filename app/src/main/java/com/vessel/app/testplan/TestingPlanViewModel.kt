package com.vessel.app.testplan

import android.os.Build
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.adapter.planedit.PlanEditAdapter
import com.vessel.app.common.adapter.planedit.PlanEditItem
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.common.adapter.planreminders.toReminderList
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Sample
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.ReagentLifestyleRecommendation
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.testplan.model.TestingPlanHeader
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*

class TestingPlanViewModel @ViewModelInject constructor(
    val state: TestingPlanState,
    val resourceProvider: ResourceRepository,
    private val planManager: PlanManager,
    private val preferencesRepository: PreferencesRepository,
    private val reminderManager: ReminderManager,
    private val trackingManager: TrackingManager,
    private val recommendationManager: RecommendationManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    TestingPlanHeaderAdapter.TestingPlanHeaderHandler,
    ReminderRowAdapter.OnActionHandler,
    PlanEditAdapter.OnActionHandler {

    private lateinit var testingPlan: Sample
    private var userPlans = listOf<PlanData>()

    init {
        observeRemindersDataState()
        loadHeaderData()
    }

    private fun observeRemindersDataState() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    planManager.clearCachedData()
                    loadHeaderData(true)
                }
            }
        }
    }

    private fun loadHeaderData(forceUpdate: Boolean = false) {
        showLoading()
        viewModelScope.launch {
            val plansResponse = planManager.getUserPlans(forceUpdate)
            val recommendationResponse = recommendationManager.getLatestSampleRecommendations()
            if (plansResponse is Result.Success && recommendationResponse is Result.Success) {
                hideLoading(false)
                val recommendations = recommendationResponse.value.lifestyle
                testingPlan =
                    recommendations.firstOrNull { it.lifestyle_recommendation_id == Constants.TEST_PLAN_ID }
                    ?: return@launch
                val testPlans = plansResponse.value.filter { it.isTestingPlan() }
                setupTestingPlans(testPlans)
                state.headerItems.value = TestingPlanHeader(testPlans.isNotEmpty())
            }
        }
    }

    private fun setupTestingPlans(testingPlans: List<PlanData>) {
        val plans = testingPlans.toReminderList()
        userPlans = testingPlans
        state.reminderItems.value = plans
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onTestingActionButtonClick() {
        openTestingReminders()
    }

    override fun onTakeTestingActionButtonClick() {
        onTakeTestClicked()
    }

    private fun onTakeTestClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_TAPPED)
                .setScreenName(TrackingConstants.TESTING_PLAN)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        val isSupportedDevice =
            Constants.SUPPORTED_DEVICES_MODELS_PREFIX.any { Build.MODEL.startsWith(it, true) }
        if (isSupportedDevice) {
            onTestClicked()
        } else {
            state.navigateTo.value = HomeNavGraphDirections.globalActionToTestNotAccurate()
        }
    }

    private fun onTestClicked() {
        val now = Date()
        val fourThirtyAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 4)
                set(Calendar.MINUTE, 30)
                set(Calendar.SECOND, 0)
            }.time
        val nineAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 9)
                set(Calendar.MINUTE, 0)
                set(Calendar.SECOND, 0)
            }.time
        if (now.after(fourThirtyAm) && now.before(nineAm)) {
            state.navigateTo.value = if (preferencesRepository.skipPreTestTips)
                HomeNavGraphDirections.globalActionToTakeTest()
            else
                HomeNavGraphDirections.globalActionToPreTest()
        } else {
            state.navigateTo.value = HomeNavGraphDirections.globalActionToTimeOfDay()
        }
    }

    fun openTestingReminders() {
        if (userPlans.isEmpty()) {
            if (::testingPlan.isInitialized.not()) {
                loadHeaderData()
                return
            }
            state.navigateTo.value =
                TestingPlanFragmentDirections.actionTestingPlanFragmentToPlanReminderDialog(
                    PlanReminderHeader(
                        getResString(R.string.wellness_testing),
                        "",
                        "",
                        R.drawable.ic_take_test,
                        listOf(getTestingPlanData()),
                        isEmptyPlan = true,
                        isTestingPlan = true
                    )
                )
        } else {
            state.navigateTo.value =
                TestingPlanFragmentDirections.actionTestingPlanFragmentToPlanReminderDialog(
                    PlanReminderHeader(
                        state.reminderItems.value.orEmpty().first().plan.getDisplayName(),
                        state.reminderItems.value.orEmpty().first().plan.getDescription(),
                        state.reminderItems.value.orEmpty().first().plan.getToDoImageUrl(),
                        0,
                        userPlans,
                        isEmptyPlan = false,
                        isTestingPlan = true
                    )
                )
        }
    }

    override fun onReminderItemClick(item: ReminderRowUiModel, view: View) {
        openTestingReminders()
    }

    override fun onPlanEditItemClick(item: PlanEditItem, view: View) {
        openTestingReminders()
    }

    private fun getTestingPlanData() = PlanData(
        multiple = 1,
        reagent_lifestyle_recommendation_id = testingPlan.id ?: Constants.TEST_PLAN_ID,
        reagent_lifestyle_recommendation = ReagentLifestyleRecommendation(
            id = -1,
            lifestyle_recommendation_id = Constants.TEST_PLAN_ID,
            quantity = testingPlan.amount,
            unit = testingPlan.unit,
            frequency = testingPlan.frequency
        )
    )
}
