package com.vessel.app.common.net.mapper

import com.vessel.app.common.model.Assessment
import com.vessel.app.common.model.GoalAssessment
import com.vessel.app.common.net.data.AssessmentData
import com.vessel.app.common.net.data.GoalAssessmentData
import com.vessel.app.common.net.data.GoalAssessmentResponse
import com.vessel.app.common.repo.ContactRepository.Companion.SERVER_DATE_FORMAT
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GoalAssessmentMapper @Inject constructor() {
    fun map(data: GoalAssessmentResponse) = data.data.map { map(it) }

    fun map(data: GoalAssessmentData) = GoalAssessment(
        createdAt = SERVER_DATE_FORMAT.parse(data.created_at)!!,
        assessments = data.assessments.map { mapAssessments(it) }
    )

    fun mapAssessments(data: AssessmentData) = Assessment(
        goalId = data.goal.id,
        value = data.value,
        goalName = data.goal.name,
        createdAt = SERVER_DATE_FORMAT.parse(data.created_at)!!
    )
}
