package com.vessel.app.appreview.firststep

import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class AppReviewFirstStepDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()
    var selectedFeelings = FEELING_NOTHING

    fun onSplitTypeChanged(id: Int) {
        selectedFeelings = when (id) {
            R.id.sadFaceRadioButton -> FEELING_UNHAPPY
            R.id.normalFaceRadioButton -> FEELING_MEH
            R.id.happyFaceRadioButton -> FEELING_HAPPY
            else -> FEELING_NOTHING
        }
    }

    fun dismiss() {
        devicePreferencesRepository.showAppReviewPopup = false
        dismissDialog.call()
    }

    fun submit() {
        if (selectedFeelings == FEELING_MEH || selectedFeelings == FEELING_UNHAPPY) {
            logUserFeeling()
            navigateTo.value = AppReviewFirstStepDialogFragmentDirections.actionAppReviewFirstStepDialogFragmentToAppReviewDialogFragment()
        } else if (selectedFeelings == FEELING_HAPPY) {
            logUserFeeling()
            navigateTo.value = AppReviewFirstStepDialogFragmentDirections.actionAppReviewFirstStepDialogFragmentToAppReviewSecondStepDialogFragment()
        }
    }

    private fun logUserFeeling() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.APP_REVIEW_FEEDBACK)
                .addProperty(TrackingConstants.KEY_FEEDBACK, selectedFeelings)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    companion object {
        const val FEELING_NOTHING = ""
        const val FEELING_UNHAPPY = "unhappy"
        const val FEELING_MEH = "meh"
        const val FEELING_HAPPY = "happy"
    }
}
