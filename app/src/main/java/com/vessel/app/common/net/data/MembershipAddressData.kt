package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MembershipAddressData(
    val id: Int,
    val customer_id: Int,
    val address1: String,
    val address2: String?,
    val city: String,
    val country: String,
    val created_at: String,
    val first_name: String,
    val last_name: String,
    val province: String,
    val zip: String,
)

@JsonClass(generateAdapter = true)
data class MembershipAddressDataResponse(
    val addresses: List<MembershipAddressData>
)
