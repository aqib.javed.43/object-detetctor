package com.vessel.app.today.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.util.DisclaimerFooter
import com.vessel.app.common.util.DisclaimerFooterAdapter
import com.vessel.app.plan.model.AddPlanFooter
import com.vessel.app.plan.model.DayPlanProgressItem
import com.vessel.app.today.TodayViewModel
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.PlanDeleteDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_today.*
import kotlinx.android.synthetic.main.item_today_header.*

@AndroidEntryPoint
class TodayFragment : BaseFragment<TodayViewModel>() {
    override val viewModel: TodayViewModel by viewModels()
    override val layoutResId = R.layout.fragment_today

    private val activityAdapter by lazy { ActivityListAdapter(viewModel) }
    private val addProgramFooterAdapter by lazy { AddToProgramFooterAdapter(viewModel) }
    private val disclaimerFooterAdapter by lazy { DisclaimerFooterAdapter() }
    private val essentialAdapter by lazy { EssentialListAdapter(viewModel) }
    private val lessonListAdapter by lazy { LessonListAdapter(viewModel) }
    private val programActivityAdapter by lazy { ProgramPlanListAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupList()
        setupObservers()
        setupPlanHeader()
    }

    private fun setupViews() {
        dailyWidget.onDateChange = {
            viewModel.setDate(it)
        }
        swipeRefresh?.setOnRefreshListener {
            swipeRefresh.isRefreshing = true
            viewModel.reloadData()
        }
        welcomeInformation.setOnClickListener {
            viewModel.navigateToProgramInformation()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.checkTrialLock()
        viewModel.loadPrograms(true)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.resetDateList()
        Log.i("Destroy", "today fragment")
    }

    private fun setupList() {
        planList.also {
            val adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                lessonListAdapter,
                programActivityAdapter,
                activityAdapter,
                essentialAdapter,
                addProgramFooterAdapter,
                disclaimerFooterAdapter
            )
            it.itemAnimator = null
            it.adapter = adapter
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(dateList) { progressItems ->
                updateDailyWidgetView(progressItems)
            }
            observe(isTakeFirstTest) {
                dailyWidget.showOrHideProgress(false)
            }
            observe(dayPlanItems) {
                activityAdapter.submitList(it) {
                    activityAdapter.notifyDataSetChanged()
                }
            }
            observe(essentialItems) {
                essentialAdapter.submitList(it) {
                    essentialAdapter.notifyDataSetChanged()
                }
            }
            observe(deletePlanItem) { item ->
                activityAdapter.notifyItemRemoved(item)
            }
            observe(openMessaging) {
                findNavController().navigate(PostTestNavGraphDirections.globalActionToNutritionOptionsFragment())
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(viewModel.state.removeReminder) {
                PlanDeleteDialog.showDialog(
                    requireActivity(),
                    it.first.title,
                    {
                        // Not used (do nothing when cancel dialog)
                    },
                    {
                        viewModel.deletePlanItems(it.first, it.second, it.third)
                    },
                    {
                        // Not used (do nothing when show again dialog)
                    }
                )
            }
            observe(readyToPopulate) {
                swipeRefresh.isRefreshing = false
            }
            observe(lessonItems) {
                lessonListAdapter.submitList(it) {
                    lessonListAdapter.notifyDataSetChanged()
                }
            }
            observe(todayActivitiesItems) {
                programActivityAdapter.submitList(it) {
                    programActivityAdapter.notifyDataSetChanged()
                }
            }
            observe(openEssentialLink) {
                openBrowser(it)
            }
            observe(currentDate) {
                dailyWidget.setDate(it)
            }
        }
        disclaimerFooterAdapter.submitList(listOf(DisclaimerFooter.WithBottomNavPadding))
        addProgramFooterAdapter.submitList(listOf(AddPlanFooter))
    }

    private fun updateDailyWidgetView(items: List<DayPlanProgressItem>) {
        dailyWidget.setData(items)
    }

    private fun setupPlanHeader() {
        titleLabel.text = viewModel.getTitle()
    }

    private fun openBrowser(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }
}
