package com.vessel.app.onboarding.pickothergoal

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PickAnotherGoalState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: PickAnotherGoalState.() -> Unit) = apply(block)
}
