package com.vessel.app.onboardingintro.introprogam

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_onboarding_intro_program.*

@AndroidEntryPoint
class IntroProgramFragment : BaseFragment<IntroProgramViewModel>() {
    override val viewModel: IntroProgramViewModel by viewModels()
    override val layoutResId = R.layout.fragment_onboarding_intro_program
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }
    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(isJoinedProgram) { joined ->
                lblMessage.text = if (joined)"Let’s further customize your program with a simple check-in." else "Let’s build your new personalized program. \n" +
                    "It starts with a simple check-in."
            }
        }
    }
}
