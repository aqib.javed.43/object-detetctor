package com.vessel.app.taketest

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TfliteObjectDetectionManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.taketest.model.UploadTestFileModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import java.util.*

class TakeTestViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    remoteConfiguration: RemoteConfiguration,
    val tfliteObjectDetectionManager: TfliteObjectDetectionManager
) : BaseViewModel(resourceRepository) {
    var failureTestCounter = 0
        private set
    var firstFailedTestUUID: String? = null
    lateinit var testStartDate: String
    var uuid: String? = null

    val tfliteDetectionBundle = tfliteObjectDetectionManager.bundle

    var uploadTestFileModel: UploadTestFileModel? = null

    init {
        remoteConfiguration.fetchConfigFromRemote()
    }

    fun incrementFailureTestCounter() {
        failureTestCounter += 1
    }

    fun generateSampleUUID() {
        uuid = UUID.randomUUID().toString().toUpperCase(Locale.ENGLISH)
    }

    companion object {
        const val FIRST_FAILURE_TEST = 1
    }
}
