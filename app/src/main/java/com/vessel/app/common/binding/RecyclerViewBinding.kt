package com.vessel.app.common.binding

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

@Suppress("UNCHECKED_CAST")
@BindingAdapter("adapterItems")
fun <T> RecyclerView.submitAdapterItems(list: List<T>?) {
    if (adapter == null) {
        Log.d("submitAdapterItems", "Trying to set items for a recyclerview that doesn't have an adapter.")
    }

    if (adapter is ListAdapter<*, *>) {
        (adapter as ListAdapter<T, RecyclerView.ViewHolder>).submitList(list)
    } else {
        throw RuntimeException("You should use your RecyclerView with a ListAdapter before calling this binding.")
    }
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("refreshItems")
fun <T> RecyclerView.refreshItems(refreshItems: Boolean) {
    if (adapter != null && adapter is ListAdapter<*, *> && refreshItems) {
        (adapter as ListAdapter<T, RecyclerView.ViewHolder>).notifyDataSetChanged()
    }
}
