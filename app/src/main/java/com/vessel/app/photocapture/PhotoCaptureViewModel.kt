package com.vessel.app.photocapture

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.google.zxing.Result
import com.google.zxing.ResultPoint
import com.vessel.app.BuildConfig
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.FileManager
import com.vessel.app.common.manager.TestCardQrCodeMetadataManager
import com.vessel.app.common.manager.TfliteObjectDetectionManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.photocapture.model.PhotoCaptureHintModel
import com.vessel.app.photocapture.previewcaptureimage.ui.PreviewCaptureImageFragment
import com.vessel.app.taketest.model.UploadTestFileModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import com.wajahatkarim3.easyvalidation.core.view_ktx.contains
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.opencv.core.Rect
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

class PhotoCaptureViewModel @ViewModelInject constructor(
    val state: PhotoCaptureState,
    private val remoteConfiguration: RemoteConfiguration,
    private val resourceRepository: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val trackingManager: TrackingManager,
    private val fileManager: FileManager,
    private val testCardQrCodeMetadataManager: TestCardQrCodeMetadataManager,
    val tfliteObjectDetectionManager: TfliteObjectDetectionManager
) : BaseViewModel(resourceRepository), ToolbarHandler {
    var autoScan: Boolean = true
    var isCameraMovedAfterScan = savedStateHandle.get<Boolean>("isCameraMovedAfterScan") ?: false
    var startTestTimeInMilli = if (savedStateHandle.get<Long>("startTestTimeInMilli") != 0L) {
        savedStateHandle.get<Long>("startTestTimeInMilli")!!
    } else {
        Calendar.getInstance().timeInMillis
    }

    companion object {
        // This should match whatever the argument is called in take_test_nav_graph.xml
        private const val CAMERA_VISION_TESTERS = "camera_vision_tester"
        private const val ERROR_DELAY_TIME = 500L
        private const val SHOW_TAKE_PHOTO_BUTTON_DELAY_TIME = 10000L
    }

    override fun onBackButtonClicked(view: View) {
        state.onBackAction.call()
    }

    fun recordStartCaptureTimer() {
        startTestTimeInMilli = Calendar.getInstance().timeInMillis
    }

    fun onPermissionsDenied() {
        logImageCaptureAbandonedAndNavigateBack()
    }

    fun copyImageFromUri(
        selectedImageUri: Uri,
        captureDate: String,
        firstFailedTestUUID: String?,
        testStartDate: String
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val mediaFile = fileManager.getMediaFile(selectedImageUri)
                mediaFile?.let {
                    if (it.second.exists()) {
                        val rotatedImage = rotatedImage(it.second)
                        state.uploadImageFile.postValue(
                            UploadTestFileModel(
                                uuid = state.uuid!!,
                                file = it.second,
                                jpgFile = it.second,
                                firstFailedTestUUID = firstFailedTestUUID,
                                autoScan = autoScan,
                                captureResult = null,
                                timerStartDate = testStartDate,
                                captureDate = captureDate,
                                wellnessCardUuid = it.first,
                                rotatedImage
                            )
                        )
                    }
                }
            }
        }
    }

    fun logSampleImageCaptured(wellnessCardUuid: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SAMPLE_IMAGE_CAPTURED)
                .addProperty(TrackingConstants.KEY_UUID, state.uuid!!)
                .addProperty(TrackingConstants.KEY_WELLNESS_CARD_UUID, wellnessCardUuid)
                .addProperty(
                    TrackingConstants.KEY_ATTEMPT_TIME_MS,
                    (Calendar.getInstance().timeInMillis - startTestTimeInMilli).toString()
                )
                .addProperty(TrackingConstants.KEY_AUTO_SCAN, autoScan.toString())
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        if (Calendar.getInstance().timeInMillis - startTestTimeInMilli > TimeUnit.SECONDS.toMillis(
                20
            )
        ) {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.SAMPLE_IMAGE_CAPTURED_DELAYED)
                    .addProperty(TrackingConstants.KEY_UUID, state.uuid!!)
                    .addProperty(TrackingConstants.KEY_WELLNESS_CARD_UUID, wellnessCardUuid)
                    .addProperty(
                        TrackingConstants.KEY_ATTEMPT_TIME_MS,
                        (Calendar.getInstance().timeInMillis - startTestTimeInMilli).toString()
                    )
                    .addProperty(TrackingConstants.KEY_AUTO_SCAN, autoScan.toString())
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }

    fun rotatedImage(file: File): Bitmap? {
        val matrix = Matrix().apply {
            postRotate(PreviewCaptureImageFragment.ROTATE_DEGREES)
        }
        val bitmap = BitmapFactory.decodeFile(file.path)
        return bitmap?.let {
            Bitmap.createBitmap(
                bitmap,
                0,
                0,
                bitmap.width,
                bitmap.height,
                matrix,
                true
            )
        }
    }

    fun onLightingDetected() {
        state.uuid?.let {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.SHOW_LIGHTING_ERROR)
                    .addProperty(
                        TrackingConstants.KEY_ATTEMPT_TIME_MS,
                        (Calendar.getInstance().timeInMillis - startTestTimeInMilli).toString()
                    )
                    .addProperty(TrackingConstants.KEY_UUID, it)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()

            )
        }
        onCardIssueDetected(R.string.find_better_lighting, false)
    }

    fun onGlareDetected() {
        state.uuid?.let {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.SHOW_GLARE_ERROR)
                    .addProperty(
                        TrackingConstants.KEY_ATTEMPT_TIME_MS,
                        (Calendar.getInstance().timeInMillis - startTestTimeInMilli).toString()
                    )
                    .addProperty(TrackingConstants.KEY_UUID, it)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()

            )
        }
        onCardIssueDetected(R.string.too_much_glare, false)
    }

    fun onShadowDetected() {
        state.uuid?.let {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.SHOW_SHADOW_ERROR)
                    .addProperty(
                        TrackingConstants.KEY_ATTEMPT_TIME_MS,
                        (Calendar.getInstance().timeInMillis - startTestTimeInMilli).toString()
                    )
                    .addProperty(TrackingConstants.KEY_UUID, it)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()

            )
        }
        onCardIssueDetected(R.string.move_to_avoid_shadow, true)
    }

    fun onCardIssueDetected(@StringRes issueMessage: Int, isShadowDetected: Boolean) {
        viewModelScope.launch {
            delay(ERROR_DELAY_TIME)
            state.shadowOverlayVisibility.postValue(isShadowDetected)
            updatePhotoCaptureHint(
                issueMessage, R.color.white, R.drawable.frosted_border_red
            )
        }
    }

    fun onFiducialsDetected(items: List<UpdatedImageProcessor.MatchTemplateLocation>) {
        logCardAcquiredEvent(items)

        if (items.size == Constants.FIDUCIALS_COUNT) {
            updatePhotoCaptureHint(
                R.string.card_detected, R.color.pixiGreen, R.drawable.frosted_border_green
            )
        } else if (items.size < Constants.FIDUCIALS_COUNT) {
            updatePhotoCaptureHint(
                R.string.align_test_card, R.color.white, R.drawable.frosted_border_red
            )
        }
    }

    fun onCardDetected(
        bitmap: Bitmap,
        qrCodeMetadataResult: Result?,
        foundTemplateMatches: MutableList<Rect>,
        resizeScale: Double,
        resultPoints: List<ResultPoint>
    ) {
        testCardQrCodeMetadataManager.setQrCodeMetadataResult(
            bitmap,
            qrCodeMetadataResult,
            foundTemplateMatches,
            resizeScale,
            resultPoints
        )
        autoScan = true
        validCardDetected(R.string.card_detected)
    }

    fun validCardDetected(@StringRes message: Int) {
        state.shadowOverlayVisibility.postValue(false)
        updatePhotoCaptureHint(
            message, R.color.pixiGreen, R.drawable.frosted_border_green
        )
    }

    fun onQrDataDetected(
        bitmap: Bitmap,
        qrCodeMetadataResult: Result?,
        resizeScale: Double,
        resultPoints: List<ResultPoint>
    ) {
        testCardQrCodeMetadataManager.setQrCodeMetadataResult(
            bitmap,
            qrCodeMetadataResult,
            mutableListOf(),
            resizeScale,
            resultPoints
        )
    }

    fun onCameraDisconnected() {
        state.retakePhoto.postValue(R.string.camera_disconnected)
    }

    fun onCameraNotSupportedButtonClicked() {
        logImageCaptureAbandonedAndNavigateBack()
    }

    fun isCameraVisionTester(): Boolean {
        return if (preferencesRepository.userEmail.isNullOrEmpty()) {
            false
        } else {
            remoteConfiguration.getString(CAMERA_VISION_TESTERS)
                .contains(preferencesRepository.userEmail!!)
        }
    }

    fun logImageCaptureAbandoned() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SAMPLE_IMAGE_CAPTURE_ABANDONED)
                .addProperty(
                    TrackingConstants.KEY_ATTEMPT_TIME_MS,
                    (Calendar.getInstance().timeInMillis - startTestTimeInMilli).toString()
                )
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun logCameraOpenEvent() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CAMERA_OPENED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun logImageFailure(error: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SAMPLE_IMAGE_VALIDATION_FAILURE)
                .setError(error)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun logImageCaptureAbandonedAndNavigateBack() {
        logImageCaptureAbandoned()
        navigateBack()
    }

    fun getFileDescription(): String {
        return testCardQrCodeMetadataManager.getExifMetaData()
    }

    fun fromActivationTimerFragment() =
        savedStateHandle.get<Boolean>("fromActivationTimerFragment")!!

    fun playScanAnimation() {
        state.playScanAnimation.postValue(true)
    }

    fun onMoveCameraErrorAfterScan() {
        stopScanAnimation()
        updatePhotoCaptureHint(
            R.string.align_test_card,
            R.color.white,
            R.drawable.frosted_border_red
        )
    }

    private val takePhotoManuallyDelayTimer =
        object : CountDownTimer(SHOW_TAKE_PHOTO_BUTTON_DELAY_TIME, TimeUnit.SECONDS.toSeconds(1)) {
            override fun onTick(millisUntilFinished: Long) {
                // Not needed
            }

            override fun onFinish() {
                state.takePhotoManuallyButtonDelayTimerStates.postValue(
                    TakePhotoManuallyButtonDelayTimerStates.ENDED
                )
            }
        }

    private fun updatePhotoCaptureHint(
        @StringRes hintMessage: Int,
        @ColorRes backgroundColor: Int,
        @DrawableRes frostedBorderDrawable: Int
    ) {
        state.photoCaptureHint.postValue(
            PhotoCaptureHintModel(
                hintMessage,
                resourceRepository.getColor(backgroundColor)
            )
        )
        state.frostedBorder.postValue(frostedBorderDrawable)
    }

    private fun stopScanAnimation() {
        state.playScanAnimation.postValue(false)
    }

    private fun logCardAcquiredEvent(items: List<UpdatedImageProcessor.MatchTemplateLocation>) {
        if (areAllFiducialsDetected(items)) {
            state.uuid?.let {
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.CARD_ACQUIRED)
                        .addProperty(TrackingConstants.KEY_UUID, it)
                        .addProperty(
                            TrackingConstants.KEY_ATTEMPT_TIME_MS,
                            (Calendar.getInstance().timeInMillis - startTestTimeInMilli).toString()
                        )
                        .withKlaviyo()
                        .withAmplitude()
                        .withFirebase()
                        .create()
                )
            }
        }
    }

    private fun areAllFiducialsDetected(items: List<UpdatedImageProcessor.MatchTemplateLocation>): Boolean {
        return items.contains(UpdatedImageProcessor.MatchTemplateLocation.BOTTOM_LEFT) &&
            items.contains(UpdatedImageProcessor.MatchTemplateLocation.BOTTOM_RIGHT) &&
            items.contains(UpdatedImageProcessor.MatchTemplateLocation.TOP_LEFT) &&
            items.contains(UpdatedImageProcessor.MatchTemplateLocation.TOP_RIGHT)
    }

    init {
        Log.d("view_model_init", "Initializing camera view.")
        state.showChoosePhotoButton.postValue(BuildConfig.FLAVOR == "dev")

        takePhotoManuallyDelayTimer.start()

        if (isCameraMovedAfterScan) {
            Log.d("view_model_init", "Camera was moved after scan")
            showError(getResString(R.string.align_test_card))
        }
        updatePhotoCaptureHint(
            R.string.align_test_card,
            R.color.white,
            R.drawable.frosted_border_red
        )
    }
}
