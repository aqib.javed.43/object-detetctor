package com.vessel.app.onboarding.badhabitsselection

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OnBoardingProgramSelect(
    val id: Int
) : Parcelable
