package com.vessel.app.common.model

class ReagentEntry(
    x: Float,
    y: Float?,
    val score: Float? = null,
    date: Long?
) : DateEntry(x, y, date)
