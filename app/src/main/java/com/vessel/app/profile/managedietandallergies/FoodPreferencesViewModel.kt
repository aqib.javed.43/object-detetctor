package com.vessel.app.profile.managedietandallergies

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.FoodManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Allergies
import com.vessel.app.common.model.Diet
import com.vessel.app.common.model.Result
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.profile.managedietandallergies.model.FoodPreferencesSelect
import com.vessel.app.profile.managedietandallergies.model.FoodPreferencesTab
import com.vessel.app.profile.managedietandallergies.ui.FoodPreferencesAdapter
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class FoodPreferencesViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    val contactManager: ContactManager,
    val foodManager: FoodManager,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider),
    FoodPreferencesAdapter.OnActionHandler,
    ToolbarHandler {

    val state = FoodPreferencesState(savedStateHandle["title"]!!)

    var diets: List<FoodPreferencesSelect> = emptyList()
    var allergies: List<FoodPreferencesSelect> = emptyList()

    init {
        loadRecommendationsPlans()
    }

    private fun loadRecommendationsPlans() {
        showLoading()
        viewModelScope.launch {
            contactManager.getContact()
                .onSuccess { contact ->
                    hideLoading()
                    state {
                        diets = Diet.values()
                            .map { diet ->
                                FoodPreferencesSelect(
                                    diet.title,
                                    contact.diets?.map { it.apiName }?.contains(diet.apiName) ?: false, FoodPreferencesTab.DIETS
                                )
                            }

                        allergies = Allergies.values()
                            .map {
                                FoodPreferencesSelect(
                                    it.title,
                                    contact.allergies?.contains(it.apiName) ?: false, FoodPreferencesTab.ALLERGIES
                                )
                            }

                        state.tabList.value = diets
                    }
                }
                .onError {
                    hideLoading()
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        view.findNavController().popBackStack()
    }

    fun onSaveClicked() {
        showLoading()
        state {
            viewModelScope.launch {
                val addDietResponse = contactManager.addDiet(getSelectedDiets())
                val addAllergiesResponse = contactManager.addAllergies(getSelectedAllergies())
                val removeUnSelectedDietResponse = contactManager.deleteDiet(getUnSelectedDiets())
                val removeUnSelectedAllergiesResponse = contactManager.deleteAllergies(getUnSelectedAllergies())

                if (addDietResponse is Result.Success<*> &&
                    addAllergiesResponse is Result.Success &&
                    removeUnSelectedDietResponse is Result.Success &&
                    removeUnSelectedAllergiesResponse is Result.Success
                ) {
                    preferencesRepository.setContactDiets(getSelectedDiets())
                    preferencesRepository.setContactAllergies(getSelectedAllergies())
                    foodManager.updateFoodCachedData()
                    hideLoading()
                    logEvent(TrackingConstants.ALLERGIES_SUBMITTED)
                    logEvent(TrackingConstants.DIET_SUBMITTED)
                    navigateBack()
                } else {
                    val errorMessage = getResString(R.string.unknown_error)
                    showError(errorMessage)
                    hideLoading()
                }
            }
        }
    }

    private fun getSelectedDiets() = diets
        .filter { it.checked }
        .mapNotNull { selectedItem ->
            Diet.values().firstOrNull {
                it.title == selectedItem.title
            }?.apiName
        }

    private fun getUnSelectedDiets() = diets
        .filter { !it.checked }
        .mapNotNull { unSelectedItem ->
            Diet.values().firstOrNull {
                it.title == unSelectedItem.title
            }?.apiName
        }.filter { preferencesRepository.userDiets.contains(it) }

    private fun getSelectedAllergies() = allergies
        .filter { it.checked }
        .mapNotNull { selectedItem ->
            Allergies.values().firstOrNull {
                it.title == selectedItem.title
            }?.apiName
        }

    private fun getUnSelectedAllergies() = allergies
        .filter { !it.checked }
        .mapNotNull { unSelectedItem ->
            Allergies.values().firstOrNull {
                it.title == unSelectedItem.title
            }?.apiName
        }.filter { preferencesRepository.userAllergies.contains(it) }

    override fun onFilterSelectChecked(item: FoodPreferencesSelect, view: View) {
        state {
            when (item.type) {
                FoodPreferencesTab.DIETS ->
                    diets = diets.mapIndexed { index, foodPreferencesSelect ->
                        if (index == diets.indexOf(item)) {
                            foodPreferencesSelect.copy(checked = item.checked.not())
                        } else {
                            foodPreferencesSelect
                        }
                    }

                FoodPreferencesTab.ALLERGIES ->
                    allergies = allergies.mapIndexed { index, foodPreferencesSelect ->
                        if (index == allergies.indexOf(item)) {
                            foodPreferencesSelect.copy(checked = item.checked.not())
                        } else {
                            foodPreferencesSelect
                        }
                    }
            }
            onTabSelected(item.type.position)
        }
    }

    fun onTabSelected(position: Int) {
        state {
            when (position) {
                FoodPreferencesTab.DIETS.position ->
                    tabList.value = diets
                FoodPreferencesTab.ALLERGIES.position ->
                    tabList.value = allergies
            }
        }
    }

    fun onScreenClosed() {
        contactManager.updateDietAllergiesState()
    }

    private fun logEvent(event: String) {
        trackingManager.log(
            TrackedEvent.Builder(event)
                .setScreenName(TrackingConstants.FROM_PLAN_BUILDER)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
