package com.vessel.app.score.model

data class ScoreWellnessItem(
    val scoreValue: Int
) {
    val score = scoreValue.toString()
}
