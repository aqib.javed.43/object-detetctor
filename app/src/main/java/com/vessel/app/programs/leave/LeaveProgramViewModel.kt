package com.vessel.app.programs.leave

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Program
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.ProgramAction
import kotlinx.coroutines.launch

class LeaveProgramViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val programManager: ProgramManager,
    private val planManager: PlanManager,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {

    val state = LeaveProgramState(savedStateHandle.get<Program>("program")!!)

    fun onLeaveAndKeepPlanItemsClicked() {
        completeProgram(ProgramAction.KEEP)
    }

    fun onLeaveAndRemovePlanItemsClicked() {
        completeProgram(ProgramAction.CLEAR)
    }

    private fun completeProgram(action: ProgramAction) {
        showLoading()
        viewModelScope.launch {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.PROGRAM_LEAVE)
                    .addProperty(TrackingConstants.KEY_ID, state.program.id.toString())
                    .addProperty(TrackingConstants.LEAVE_TYPE, action.value)
                    .addProperty(
                        TrackingConstants.KEY_LOCATION,
                        Constants.PROGRAM_DETAILS_PAGE
                    )
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
            programManager.completeProgram(state.program, action)
                .onSuccess {
                    if (action == ProgramAction.KEEP) {
                        state.dismissDialog.value = true
                        hideLoading(false)
                    } else {
                        viewModelScope.launch {
                            planManager.getUserPlans(true)
                                .onSuccess {
                                    state.dismissDialog.value = true
                                    hideLoading(false)
                                    navigateToSuccessfullyLeave()
                                }
                                .onError {
                                    showError(getResString(R.string.unknown_error))
                                    hideLoading(false)
                                }
                        }
                    }
                }
                .onServiceError {
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }
    private fun navigateToSuccessfullyLeave() {
        state.navigateTo.value =
            LeaveProgramDialogFragmentDirections.actionLeaveProgramFragmentToSuccessfullyLeaveDialogFragment(
                state.program.title
            )
    }
    fun onCloseClicked() {
        state.dismissDialog.value = false
    }
}
