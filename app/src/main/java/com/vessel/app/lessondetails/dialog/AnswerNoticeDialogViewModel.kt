package com.vessel.app.lessondetails.dialog

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class AnswerNoticeDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    var notice: String = savedStateHandle.get<String>("notice")!!
    val dismissDialog = LiveEvent<Unit>()
    var navigateTo = LiveEvent<NavDirections>()
    fun dismiss() {
        dismissDialog.call()
    }
}
