package com.vessel.app.takesurvey

import android.os.Parcelable
import com.vessel.app.takesurvey.model.SurveyAnswer
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SurveyQuestion(
    val id: Int,
    val text: String,
    val imageUrl: String?,
    val layoutType: LayoutType,
    val answers: List<SurveyAnswer>?,
    val tipText: String?,
    val tipImageUrl: String?,
    val tipImageAlignment: ImageAlignment,
    val imageAlignment: ImageAlignment,
    val successHeading: String?,
    val successText: String?,
    val isSkippable: Boolean
) : Parcelable

@Parcelize
enum class LayoutType : Parcelable {
    LAYOUT1, LAYOUT2, LAYOUT3, LAYOUT4
}

@Parcelize
enum class ImageAlignment : Parcelable {
    CENTER, LEFT, RIGHT
}
