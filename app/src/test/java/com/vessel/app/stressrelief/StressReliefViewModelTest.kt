// TODO fix the unit test

// package com.vessel.app.stressrelief
//
// import androidx.lifecycle.Observer
// import com.vessel.app.*
// import com.vessel.app.BaseFaker.Fake.FAKER
// import com.vessel.app.common.manager.RecommendationManager
// import com.vessel.app.common.manager.ScoreManager
// import com.vessel.app.common.manager.TodosManager
// import com.vessel.app.common.model.*
// import com.vessel.app.recommendation.StressReliefFaker
// import com.vessel.app.stressrelief.model.StressReliefItem
// import com.vessel.app.stressrelief.model.StressReliefViewItem
// import com.vessel.app.todo.TodoFaker
// import com.vessel.app.util.ResourceRepository
// import com.vessel.app.wellness.WellnessEntryFaker
// import com.vessel.app.wellness.model.Score
// import io.mockk.*
// import org.assertj.core.api.Assertions.assertThat
// import org.junit.jupiter.api.BeforeEach
// import org.junit.jupiter.api.Test
//
// class StressReliefViewModelTest : BaseTest() {
//    private val state = StressReliefState()
//    private val resourceProvider = mockk<ResourceRepository>(relaxed = true)
//    private val todosManager = mockk<TodosManager>()
//    private val recommendationManager = mockk<RecommendationManager>()
//    private val scoreManager = mockk<ScoreManager>()
//
//    private val eventObserver = mockk<Observer<Unit>>(relaxed = true)
//    private val todos = listOf(TodoFaker.basic(name = "meditate"))
//
//    private lateinit var viewModel: StressReliefViewModel
//
//    @BeforeEach
//    fun setUp() {
//        coEvery { todosManager.getRecurringTodos() } returns Result.Success(todos)
//
//        viewModel = StressReliefViewModel(state, resourceProvider, todosManager, recommendationManager, scoreManager)
//    }
//
//    @Test
//    fun `it should set stress relief items`() {
//        assertThat(state.stressReliefInPlan.value!!).containsOnly(
//            StressReliefViewItem(StressReliefItem.MEDITATE, true, "")
//        )
//        assertThat(state.stressReliefNotInPlan.value!!).containsOnly(
//            StressReliefViewItem(StressReliefItem.BREATH, false, ""),
//            StressReliefViewItem(StressReliefItem.YOGA, false, ""),
//            StressReliefViewItem(StressReliefItem.CARDIO, false, ""),
//            StressReliefViewItem(StressReliefItem.NIGHT, false, ""),
//            StressReliefViewItem(StressReliefItem.HANGING_OUT, false, "")
//        )
//    }
//
//    @Test
//    fun `it should open the reagent page when clicking to see cortisol levels`() = runBlockingTest {
//        val score = Score(
//            wellness = WellnessEntryFaker.list(),
//            reagents = mapOf(
//                Reagent.Cortisol to BaseFaker.list {
//                    ReagentEntry(
//                        x = FAKER.number().randomNumber().toFloat(),
//                        y = FAKER.number().randomNumber().toFloat(),
//                        date = FAKER.date().birthday().time,
//                        score = FAKER.number().randomNumber().toFloat()
//                    )
//                }
//            )
//        )
//        coEvery { scoreManager.getWellnessScores() } returns Result.Success(score)
//
//        viewModel.onSeeCortisolLevelsClicked()
//
//        val navigateTo = state.navigateTo.value!!
//        assertThat(navigateTo.destinationId).isEqualTo(R.id.action_stress_relief_to_reagent)
//        (navigateTo.args.reagentHeader).run {
//            assertThat(id).isEqualTo(Reagent.Cortisol.id)
//            assertThat(title).isEqualTo(Reagent.Cortisol.displayName)
//            assertThat(unit).isEqualTo(Reagent.Cortisol.unit)
//            assertThat(chartEntries).isEqualTo(score.reagents.getValue(Reagent.Cortisol))
//            assertThat(lowerBound).isEqualTo(Reagent.Cortisol.lowerBound)
//            assertThat(upperBound).isEqualTo(Reagent.Cortisol.upperBound)
//            assertThat(minY).isEqualTo(Reagent.Cortisol.min)
//            assertThat(maxY).isEqualTo(Reagent.Cortisol.max)
//            assertThat(background).isEqualTo(Reagent.fromId(Reagent.Cortisol.id)!!.headerImage)
//        }
//    }
//
//    @Test
//    fun `it should delete the todo on the server if the item clicked is already in the plan and then update the cell`() =
//        runBlockingTest {
//            val item1 = StressReliefFaker.viewItem(StressReliefItem.MEDITATE, isInPlan = true)
//            val item2 = StressReliefFaker.viewItem(StressReliefItem.YOGA, isInPlan = true)
//            state.stressReliefInPlan.value = listOf(item1, item2)
//            coEvery { todosManager.deleteRecurringTodo(todos[0].id) }
//                .answers { Result.Success(Unit) }
//
//            viewModel.onAddToPlanClick(item1)
//
//            state.stressReliefInPlan.value!![0] isEqualTo item2
//            assertThat(state.stressReliefInPlan.value).hasSize(1)
//            state.stressReliefNotInPlan.value!!.last().run {
//                isInPlan.isFalse()
//                stressReliefItem isEqualTo StressReliefItem.MEDITATE
//            }
//        }
//
//    @Test
//    fun `it should do nothing when it fails to delete the todo on the server with generic error`() =
//        runBlockingTest {
//            val item1 = StressReliefFaker.viewItem(StressReliefItem.MEDITATE, isInPlan = true)
//            val item2 = StressReliefFaker.viewItem(StressReliefItem.YOGA, isInPlan = true)
//            state.stressReliefInPlan.value = listOf(item1, item2)
//            val error = ErrorMessage(FAKER.chuckNorris().fact())
//            coEvery { todosManager.deleteRecurringTodo(todos[0].id) }
//                .answers { Result.GenericError(FAKER.number().randomDigit(), error) }
//
//            viewModel.onAddToPlanClick(item1)
//
//            state.stressReliefInPlan.value!![0] isEqualTo item1
//            state.stressReliefInPlan.value!![1] isEqualTo item2
//            assertThat(state.stressReliefInPlan.value).hasSize(2)
//            viewModel.showAlert.value!!.body isEqualTo error.message
//        }
//
//    @Test
//    fun `it should do nothing when it fails to delete the todo on the server with network error`() =
//        runBlockingTest {
//            val item1 = StressReliefFaker.viewItem(StressReliefItem.MEDITATE, isInPlan = true)
//            val item2 = StressReliefFaker.viewItem(StressReliefItem.YOGA, isInPlan = true)
//            state.stressReliefInPlan.value = listOf(item1, item2)
//            coEvery { todosManager.deleteRecurringTodo(todos[0].id) }
//                .answers { Result.NetworkError(Throwable()) }
//            val errorMessage = FAKER.chuckNorris().fact()
//            every { resourceProvider.getString(R.string.network_error) } returns errorMessage
//
//            viewModel.onAddToPlanClick(item1)
//
//            state.stressReliefInPlan.value!![0] isEqualTo item1
//            state.stressReliefInPlan.value!![1] isEqualTo item2
//            assertThat(state.stressReliefInPlan.value).hasSize(2)
//            viewModel.showAlert.value!!.body isEqualTo errorMessage
//        }
//
//    @Test
//    fun `it should do nothing when it fails to delete the todo on the server with unknown error`() =
//        runBlockingTest {
//            val item1 = StressReliefFaker.viewItem(StressReliefItem.MEDITATE, isInPlan = true)
//            val item2 = StressReliefFaker.viewItem(StressReliefItem.YOGA, isInPlan = true)
//            state.stressReliefInPlan.value = listOf(item1, item2)
//            coEvery { todosManager.deleteRecurringTodo(todos[0].id) }
//                .answers { Result.UnknownError(Throwable()) }
//            val errorMessage = FAKER.chuckNorris().fact()
//            every { resourceProvider.getString(R.string.unknown_error) } returns errorMessage
//
//            viewModel.onAddToPlanClick(item1)
//
//            state.stressReliefInPlan.value!![0] isEqualTo item1
//            state.stressReliefInPlan.value!![1] isEqualTo item2
//            assertThat(state.stressReliefInPlan.value).hasSize(2)
//            viewModel.showAlert.value!!.body isEqualTo errorMessage
//        }
//
//    @Test
//    fun `it should add the todo on the server if the item clicked is not in the plan and then update the cell`() =
//        runBlockingTest {
//            val item1 = StressReliefFaker.viewItem(StressReliefItem.BREATH, isInPlan = false)
//            val item2 = StressReliefFaker.viewItem(StressReliefItem.YOGA, isInPlan = false)
//            state.stressReliefNotInPlan.value = listOf(item1, item2)
//            val todo = TodoFaker.basic()
//            coEvery { todosManager.addRecurringTodo(any(), any(), any(), any()) }
//                .answers { Result.Success(todo) }
//
//            viewModel.onAddToPlanClick(item2)
//
//            state.stressReliefInPlan.value!!.last().stressReliefItem isEqualTo StressReliefItem.YOGA
//            state.stressReliefNotInPlan.value!![0] isEqualTo item1
//            assertThat(state.stressReliefNotInPlan.value).hasSize(1)
//        }
//
//    @Test
//    fun `it should do nothing when it fails to add the todo on the server with generic error`() =
//        runBlockingTest {
//            val item1 = StressReliefFaker.viewItem(StressReliefItem.BREATH, isInPlan = false)
//            val item2 = StressReliefFaker.viewItem(StressReliefItem.YOGA, isInPlan = false)
//            state.stressReliefNotInPlan.value = listOf(item1, item2)
//            val error = ErrorMessage(FAKER.chuckNorris().fact())
//            coEvery {
//                todosManager.addRecurringTodo(any(), any(), any(), any())
//            }.answers { Result.GenericError(FAKER.number().randomDigit(), error) }
//
//            viewModel.onAddToPlanClick(item2)
//
//            state.stressReliefNotInPlan.value!![0] isEqualTo item1
//            state.stressReliefNotInPlan.value!![1] isEqualTo item2
//            assertThat(state.stressReliefNotInPlan.value).hasSize(2)
//            viewModel.showAlert.value!!.body isEqualTo error.message
//        }
//
//    @Test
//    fun `it should do nothing when it fails to add the todo on the server with network error`() =
//        runBlockingTest {
//            val item1 = StressReliefFaker.viewItem(StressReliefItem.BREATH, isInPlan = false)
//            val item2 = StressReliefFaker.viewItem(StressReliefItem.YOGA, isInPlan = false)
//            state.stressReliefNotInPlan.value = listOf(item1, item2)
//            coEvery { todosManager.addRecurringTodo(any(), any(), any(), any()) }
//                .answers { Result.NetworkError(Throwable()) }
//            val errorMessage = FAKER.chuckNorris().fact()
//            every { resourceProvider.getString(R.string.network_error) } returns errorMessage
//
//            viewModel.onAddToPlanClick(item2)
//
//            state.stressReliefNotInPlan.value!![0] isEqualTo item1
//            state.stressReliefNotInPlan.value!![1] isEqualTo item2
//            assertThat(state.stressReliefNotInPlan.value).hasSize(2)
//            viewModel.showAlert.value!!.body isEqualTo errorMessage
//        }
//
//    @Test
//    fun `it should do nothing when it fails to add the todo on the server with unknown error`() =
//        runBlockingTest {
//            val item1 = StressReliefFaker.viewItem(StressReliefItem.BREATH, isInPlan = false)
//            val item2 = StressReliefFaker.viewItem(StressReliefItem.YOGA, isInPlan = false)
//            state.stressReliefNotInPlan.value = listOf(item1, item2)
//            coEvery { todosManager.addRecurringTodo(any(), any(), any(), any()) }
//                .answers { Result.UnknownError(Throwable()) }
//            val errorMessage = FAKER.chuckNorris().fact()
//            every { resourceProvider.getString(R.string.unknown_error) } returns errorMessage
//
//            viewModel.onAddToPlanClick(item2)
//
//            state.stressReliefNotInPlan.value!![0] isEqualTo item1
//            state.stressReliefNotInPlan.value!![1] isEqualTo item2
//            assertThat(state.stressReliefNotInPlan.value).hasSize(2)
//            viewModel.showAlert.value!!.body isEqualTo errorMessage
//        }
//
//    @Test
//    fun `it should navigate to the previous fragment when clicking the back button`() {
//        viewModel.navigateBack.observeForever(eventObserver)
//
//        viewModel.onBackButtonClicked(mockk())
//
//        verify { eventObserver.onChanged(null) }
//    }
// }
