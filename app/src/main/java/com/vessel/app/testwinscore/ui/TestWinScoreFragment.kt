package com.vessel.app.testwinscore.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.score.model.ScoreGoalHeaderItem
import com.vessel.app.score.ui.ScoreGoalHeaderAdapter
import com.vessel.app.testwinscore.TestWinScoreViewModel
import com.vessel.app.testwinscore.model.TestWinScoreEmptyModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_score.*

@AndroidEntryPoint
class TestWinScoreFragment : BaseFragment<TestWinScoreViewModel>() {
    override val viewModel: TestWinScoreViewModel by viewModels()
    override val layoutResId = R.layout.fragment_test_win_score

    private val scoreWellnessAdapter by lazy { TestWinScoreWellnessAdapter(viewModel) }
    private val scoreGoalHeaderAdapter by lazy { ScoreGoalHeaderAdapter(viewModel) }
    private val scoreGoalAdapter = TestWinScoreGoalAdapter()
    private val testWinEmptyAdapter = TestWinEmptyAdapter()

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        scoreList.also {

            val adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                scoreWellnessAdapter,
                scoreGoalHeaderAdapter,
                scoreGoalAdapter,
                testWinEmptyAdapter
            )

            val testWinScoreSpanLookup = TestWinScoreSpanLookup(requireContext(), adapter)

            it.adapter = adapter
            it.layoutManager = GridLayoutManager(requireContext(), 2).apply {
                spanSizeLookup = testWinScoreSpanLookup
            }
            it.addItemDecoration(TestWinScoreItemDecoration(requireContext(), testWinScoreSpanLookup))
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(scoreWellnessItems) {
                scoreWellnessAdapter.submitList(it)
                scoreList.scrollToPosition(0)
            }
            observe(scoreGoalItems) { scoreGoalAdapter.submitList(it) }

            observe(emptyWinScoreItems) { testWinEmptyAdapter.submitList(listOf(TestWinScoreEmptyModel)) }
        }

        scoreGoalHeaderAdapter.submitList(listOf(ScoreGoalHeaderItem(R.string.win_goals)))
    }
}
