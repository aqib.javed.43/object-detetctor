package com.vessel.app.expert

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Contact
import com.vessel.app.common.model.Program
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ExpertProfileState @Inject constructor() {
    val contactExpert = MutableLiveData<Contact>()
    val goalWithTips = MutableLiveData<List<GoalWithRecommendations>>()
    val programsItem = MutableLiveData<List<Program>>()
    val isProgramCollapsed = MutableLiveData<Boolean>(false)
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: ExpertProfileState.() -> Unit) = apply(block)
}
