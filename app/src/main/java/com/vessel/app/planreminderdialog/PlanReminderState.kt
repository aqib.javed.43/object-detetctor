package com.vessel.app.planreminderdialog

import androidx.lifecycle.MutableLiveData
import com.vessel.app.common.net.data.plan.toReminderUiModel
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.planreminderdialog.model.ReminderItemUiModel
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PlanReminderState @Inject constructor(val planHeader: PlanReminderHeader) {
    val planHeaderItem = MutableLiveData(planHeader)
    val reminderList = MutableLiveData(
        if (planHeader.isEmptyPlan) listOf() else {
            planHeader.todos.map { it.toReminderUiModel() }
        }
    )
    val updateReminder = MutableLiveData<ReminderItemUiModel?>()
    val dismissDialog = LiveEvent<Unit>()
    val removeReminder = LiveEvent<ReminderItemUiModel>()
    val updateReminderTime = LiveEvent<ReminderItemUiModel>()
    val setRemindersEvent = LiveEvent<Unit>()
    val skipRemindersEvent = LiveEvent<Unit>()
    val planItem = MutableLiveData(planHeader.todos.first())
    val showReminderWarning = MutableLiveData<Boolean>(false)
    operator fun invoke(block: PlanReminderState.() -> Unit) = apply(block)
}
