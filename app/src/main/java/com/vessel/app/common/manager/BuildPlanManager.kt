package com.vessel.app.common.manager

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.buildplan.model.MakePlanItem
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.common.model.*
import com.vessel.app.common.model.data.Reagent
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.model.data.ReagentLevel
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.reagentpersonalizing.model.ReagentRecommendationModel
import com.vessel.app.recommendations.ui.RecommendationTab
import com.vessel.app.recommendations.ui.RecommendationsPage
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.model.Score
import com.vessel.app.wellness.net.data.LikeCategory
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList

@Singleton
class BuildPlanManager @Inject constructor(
    private val resourceProvider: ResourceRepository,
    private val planManager: PlanManager,
    private val recommendationManager: RecommendationManager,
    private val scoreManager: ScoreManager,
    private val foodManager: FoodManager,
    private val reagentsManager: ReagentsManager,
    private val goalsRepository: GoalsRepository,
    private val trackingManager: TrackingManager,
) {
    private var lastReagent: ReagentRecommendationModel? = null
    private var recommendList: ArrayList<ReagentRecommendationModel> = arrayListOf()
    private var todayIncludedPlans: List<PlanType>? = null
    private var foodPlanRecommendations: List<Pair<String, Int>>? = null
    private var supplementsPlanRecommendations: List<Sample>? = null
    private var planResponseData: Result<List<PlanData>, Nothing>? = null
    private var recommendationResponseData: Result<SampleRecommendations, Nothing>? = null

    private val viewPlanStateChannel = ConflatedBroadcastChannel(false)

    suspend fun loadPlans(forceUpdate: Boolean = false): List<MakePlanItem> {
        val plansResponse = planManager.getUserPlans(forceUpdate)
        val recommendationsResponse =
            recommendationManager.getLatestSampleRecommendations(forceUpdate)
        val reagentDeficienciesResponse = scoreManager.reagentDeficiencies(forceUpdate)
        val foodRecommendationResponse = foodManager.getRecommendedFoodItems(forceUpdate)
        val scoresResponse = scoreManager.getWellnessScores()

        if (recommendationsResponse is Result.Success &&
            reagentDeficienciesResponse is Result.Success &&
            foodRecommendationResponse is Result.Success &&
            scoresResponse is Result.Success &&
            plansResponse is Result.Success
        ) {
            val scoreData = scoresResponse.value
            planResponseData = plansResponse
            recommendationResponseData = recommendationsResponse
            val todayIncludedPlans = getIncludedPlans(plansResponse.value)

            return mutableListOf(
                MakePlanItem(
                    title = getDescription(todayIncludedPlans, PlanType.SupplementPlan, scoreData),
                    buttonText = getButtonText(
                        todayIncludedPlans,
                        PlanType.SupplementPlan,
                        scoreData
                    ),
                    background = getBackground(
                        todayIncludedPlans,
                        PlanType.SupplementPlan,
                        scoreData
                    ),
                    type = PlanType.SupplementPlan,
                    image = R.drawable.ic_plan_supp
                ),
                MakePlanItem(
                    title = getDescription(todayIncludedPlans, PlanType.StressPlan, scoreData),
                    buttonText = getButtonText(todayIncludedPlans, PlanType.StressPlan, scoreData),
                    background = getBackground(todayIncludedPlans, PlanType.StressPlan, scoreData),
                    type = PlanType.StressPlan,
                    image = R.drawable.ic_plan_stress
                ),
                MakePlanItem(
                    title = getDescription(todayIncludedPlans, PlanType.FoodPlan, scoreData),
                    buttonText = getButtonText(todayIncludedPlans, PlanType.FoodPlan, scoreData),
                    background = getBackground(todayIncludedPlans, PlanType.FoodPlan, scoreData),
                    type = PlanType.FoodPlan,
                    image = R.drawable.ic_plan_food
                ),
                MakePlanItem(
                    title = getDescription(
                        todayIncludedPlans,
                        PlanType.WaterPlan,
                        scoreData
                    ),
                    buttonText = getButtonText(
                        todayIncludedPlans,
                        PlanType.WaterPlan,
                        scoreData
                    ),
                    background = getBackground(
                        todayIncludedPlans,
                        PlanType.WaterPlan,
                        scoreData
                    ),
                    type = PlanType.WaterPlan,
                    image = R.drawable.ic_plan_water
                ),
                MakePlanItem(
                    title = getDescription(todayIncludedPlans, PlanType.TestPlan, scoreData),
                    buttonText = getButtonText(todayIncludedPlans, PlanType.TestPlan, scoreData),
                    background = getBackground(todayIncludedPlans, PlanType.TestPlan, scoreData),
                    type = PlanType.TestPlan,
                    image = R.drawable.ic_plan_test
                ),
                MakePlanItem(
                    title = resourceProvider.getString(R.string.create_your_own_plan_item),
                    subTitle = resourceProvider.getString(R.string.what_do_you_what_to_track),
                    buttonText = resourceProvider.getString(R.string.create_a_plan_item),
                    background = R.drawable.green_background,
                    type = PlanType.OwnPlan,
                    image = R.drawable.ic_make_plan_item
                )
            )
        } else {
            return emptyList()
        }
    }

    suspend fun loadPlansForAdding(forceUpdate: Boolean = false): List<MakePlanItem> {
        val plansResponse = planManager.getUserPlans(forceUpdate)
        val recommendationsResponse =
            recommendationManager.getLatestSampleRecommendations(forceUpdate)
        val reagentDeficienciesResponse = scoreManager.reagentDeficiencies(forceUpdate)
        val foodRecommendationResponse = foodManager.getRecommendedFoodItems(forceUpdate)
        val scoresResponse = scoreManager.getWellnessScores()

        if (recommendationsResponse is Result.Success &&
            reagentDeficienciesResponse is Result.Success &&
            foodRecommendationResponse is Result.Success &&
            scoresResponse is Result.Success &&
            plansResponse is Result.Success
        ) {
            val scoreData = scoresResponse.value
            planResponseData = plansResponse
            recommendationResponseData = recommendationsResponse
            val todayIncludedPlans = getIncludedPlans(plansResponse.value)

            return mutableListOf(
                MakePlanItem(
                    title = PlanType.SupplementPlan.title,
                    buttonText = String.format(resourceProvider.getString(R.string.add_activity_to_program), PlanType.SupplementPlan.title),
                    background = getBackground(
                        todayIncludedPlans,
                        PlanType.SupplementPlan,
                        scoreData
                    ),
                    type = PlanType.SupplementPlan,
                    image = R.drawable.ic_plan_supp
                ),
                MakePlanItem(
                    title = PlanType.StressPlan.title,
                    buttonText = String.format(resourceProvider.getString(R.string.add_activity_to_program), PlanType.StressPlan.title),
                    background = getBackground(todayIncludedPlans, PlanType.StressPlan, scoreData),
                    type = PlanType.StressPlan,
                    image = R.drawable.ic_plan_stress
                ),
                MakePlanItem(
                    title = PlanType.FoodPlan.title,
                    buttonText = String.format(resourceProvider.getString(R.string.add_activity_to_program), PlanType.FoodPlan.title),
                    background = getBackground(todayIncludedPlans, PlanType.FoodPlan, scoreData),
                    type = PlanType.FoodPlan,
                    image = R.drawable.ic_plan_food
                ),
                MakePlanItem(
                    title =
                    PlanType.WaterPlan.title,
                    buttonText = String.format(resourceProvider.getString(R.string.add_activity_to_program), PlanType.WaterPlan.title),
                    background = getBackground(
                        todayIncludedPlans,
                        PlanType.WaterPlan,
                        scoreData
                    ),
                    type = PlanType.WaterPlan,
                    image = R.drawable.ic_plan_water
                ),
                MakePlanItem(
                    title = resourceProvider.getString(R.string.create_your_own_plan_item),
                    buttonText = resourceProvider.getString(R.string.create_own_plan),
                    background = R.drawable.green_background,
                    type = PlanType.OwnPlan,
                    image = 0
                )
            )
        } else {
            return emptyList()
        }
    }

    suspend fun loadPlanData() {
        val planResponse = planManager.getUserPlans(true)
        val recommendationsResponse = recommendationManager.getLatestSampleRecommendations(true)
        val reagentDeficienciesResponse = scoreManager.reagentDeficiencies(true)
        val foodRecommendationResponse = foodManager.getRecommendedFoodItems(true)

        if (recommendationsResponse is Result.Success &&
            reagentDeficienciesResponse is Result.Success &&
            foodRecommendationResponse is Result.Success &&
            planResponse is Result.Success
        ) {
            todayIncludedPlans = getIncludedPlans((planResponse).value)

            foodPlanRecommendations = getFoodPlanRecommendations(
                (foodRecommendationResponse).value,
                (reagentDeficienciesResponse).value,
                (planResponse.value as List<PlanData>?).orEmpty()
            )

            supplementsPlanRecommendations =
                (recommendationsResponse).value.supplements
                    .filter { it.reagent != null && it.amount > 0 }
        }
    }

    suspend fun loadRecommendationsPlans(): List<RecommendationsPage> {
        loadPlanData()

        val scoresResponse = scoreManager.getWellnessScores()

        if (scoresResponse is Result.Success) {

            val score = scoresResponse.value as? Score

            val cortisolReagentRangeType = getReagentLevel(score, ReagentItem.Cortisol)

            val hydrationReagentRangeType = getReagentLevel(score, ReagentItem.Hydration)

            return mutableListOf(
                createNutrientsRecommendations()
            ).apply {
                add(
                    mapCortisolReagentRangeToPlan(
                        cortisolReagentRangeType,
                        todayIncludedPlans.orEmpty()
                    )
                )
                add(
                    mapHydrationReagentRangeToPlan(
                        hydrationReagentRangeType,
                        todayIncludedPlans.orEmpty()
                    )
                )
                add(createTestingRecommendationsPage(todayIncludedPlans.orEmpty()))
            }
        } else return emptyList()
    }

    private fun mapCortisolReagentRangeToPlan(
        cortisolReagentRangeType: ReagentLevel?,
        todayIncludedPlans: List<PlanType>
    ): RecommendationsPage {
        val hasStressPlan = hasPlan(todayIncludedPlans, PlanType.StressPlan)
        return when (cortisolReagentRangeType) {
            ReagentLevel.High -> {
                // cortisol -> only show a notification when a user is high in cortisol
                // and has not added anything to their stress relief plan
                createCortisolRecommendationsPage(
                    recommendationsPageMessage = getFormattedBoldText(
                        resourceProvider.getString(R.string.build_your_cortisol_plan_body),
                        resourceProvider.getString(R.string.cortisol).capitalize(Locale.ENGLISH)
                    ),
                    recommendationsPageGoal = getCortisolGoals(),
                    hasPlan = hasStressPlan,
                    showRedAlert = !hasStressPlan
                )
            }
            null -> {
                createCortisolRecommendationsPage(
                    recommendationsPageMessage = getFormattedBoldText(
                        resourceProvider.getString(R.string.recommendation_reagent_missing),
                        resourceProvider.getString(R.string.cortisol).capitalize(Locale.ENGLISH)
                    ),
                    recommendationsPageGoal = getCortisolGoals(),
                    hasPlan = hasStressPlan
                )
            }
            else -> {
                createCortisolRecommendationsPage(
                    isReagentDeficient = false,
                    hasPlan = hasStressPlan
                )
            }
        }
    }

    suspend fun hasHydrationRecommendation(): Boolean {
        val planResponse = planManager.getUserPlans()
        if (planResponse is Result.Success) {
            return planResponse.value.firstOrNull { it.isHydrationPlan() } == null
        }
        return false
    }

    private fun mapHydrationReagentRangeToPlan(
        hydrationReagentRangeType: ReagentLevel?,
        todayIncludedPlans: List<PlanType>
    ): RecommendationsPage {
        val hasHydrationPlan = hasPlan(todayIncludedPlans, PlanType.WaterPlan)
        return when (hydrationReagentRangeType) {
            ReagentLevel.Low -> {
                // hydration -> only show a notification when a user is dehydrated
                // and has not created a plan
                createHydrationRecommendationsPage(
                    recommendationsPageMessage = getFormattedBoldText(
                        resourceProvider.getString(R.string.build_your_hydration_plan_body),
                        resourceProvider.getString(R.string.dehydrated).capitalize(Locale.ENGLISH)
                    ),
                    recommendationsPageGoal = getHydrationGoals(),
                    isReagentDeficient = true,
                    hasPlan = hasHydrationPlan,
                    showRedAlert = !hasHydrationPlan
                )
            }
            ReagentLevel.High -> {
                createHydrationRecommendationsPage(
                    recommendationsPageMessage = getFormattedBoldText(
                        resourceProvider.getString(R.string.build_your_hydration_plan_body),
                        resourceProvider.getString(R.string.overhydrated).capitalize(Locale.ENGLISH)
                    ),
                    recommendationsPageGoal = getHydrationGoals(),
                    isReagentDeficient = true,
                    hasPlan = hasHydrationPlan,
                    showActionButton = false
                )
            }
            else -> {
                createHydrationRecommendationsPage(
                    isReagentDeficient = false,
                    hasPlan = hasHydrationPlan
                )
            }
        }
    }

    private fun createTestingRecommendationsPage(
        todayIncludedPlans: List<PlanType>
    ): RecommendationsPage {
        return RecommendationsPage(
            recommendationTab = RecommendationTab.TESTING,
            isReagentDeficient = false,
            hasPlan = hasPlan(todayIncludedPlans, PlanType.TestPlan),
            showRedAlert = !hasPlan(todayIncludedPlans, PlanType.TestPlan)
        )
    }

    private suspend fun createNutrientsRecommendations(): RecommendationsPage {
        val nutrientsRecommendations = mutableListOf<String>().apply {
            foodPlanRecommendations?.let {
                addAll(it.map { it.first.capitalize(Locale.ENGLISH) })
            }
            supplementsPlanRecommendations?.let {
                addAll(
                    it.map { it.reagent!!.displayName.capitalize(Locale.ENGLISH) }
                )
            }
        }.distinct()

        return if (nutrientsRecommendations.isNullOrEmpty()) {
            RecommendationsPage(
                recommendationTab = RecommendationTab.NUTRIENTS,
                isReagentDeficient = false,
                hasPlan = hasPlan(
                    todayIncludedPlans,
                    PlanType.FoodPlan
                ) && !hasFoodPlanRecommendations(),
                hasItemsInPlan = hasPlan(todayIncludedPlans, PlanType.FoodPlan),
                hasSupplementPlan = hasPlan(todayIncludedPlans, PlanType.SupplementPlan),
                showRedAlert = hasFoodPlanRecommendations(),
                hasAddedAllSupplementToPlan = hasPlan(todayIncludedPlans, PlanType.SupplementPlan)
            )
        } else {
            RecommendationsPage(
                recommendationTab = RecommendationTab.NUTRIENTS,
                recommendationsPageMessage = getFormattedBoldText(
                    resourceProvider.getString(R.string.build_your_nutrients_plan_body),
                    nutrientsRecommendations.joinToString(", ")
                ),
                recommendationsPageGoal = getNutrientsGoals(),
                hasPlan = hasPlan(
                    todayIncludedPlans,
                    PlanType.FoodPlan
                ) && !hasFoodPlanRecommendations(),
                hasItemsInPlan = hasPlan(todayIncludedPlans, PlanType.FoodPlan),
                hasSupplementPlan = hasPlan(todayIncludedPlans, PlanType.SupplementPlan),
                showRedAlert = hasFoodPlanRecommendations(),
                hasAddedAllSupplementToPlan = hasPlan(todayIncludedPlans, PlanType.SupplementPlan)
            )
        }
    }

    suspend fun hasStressRecommendations(): Boolean {
        val plansResponse = planManager.getUserPlans()
        val scoresResponse = scoreManager.getWellnessScores()
        return if (plansResponse is Result.Success && scoresResponse is Result.Success) {

            val score = scoresResponse.value as? Score

            val cortisolReagentRangeType = getReagentLevel(score, ReagentItem.Cortisol)
            val todayIncludedPlans = getIncludedPlans(plansResponse.value)

            (
                hasPlan(
                    todayIncludedPlans,
                    PlanType.StressPlan
                ) || cortisolReagentRangeType != ReagentLevel.High
                ).not()
        } else false
    }
    suspend fun getSupplementRecommendations(): List<ReagentItem> {
        val plansResponse = planManager.getUserPlans()
        val scoresResponse = scoreManager.getWellnessScores()
        val supplementRecommendations = ArrayList<ReagentItem>()
        if (plansResponse is Result.Success && scoresResponse is Result.Success) {
            val score = scoresResponse.value as? Score
            val magnesiumRangeType = getReagentLevel(score, ReagentItem.Magnesium)
            val b7ReagentRangeType = getReagentLevel(score, ReagentItem.B7)
            val b9ReagentRangeType = getReagentLevel(score, ReagentItem.B9)
            if (magnesiumRangeType == ReagentLevel.Low) supplementRecommendations.add(ReagentItem.Magnesium)
            if (b7ReagentRangeType == ReagentLevel.Low) supplementRecommendations.add(ReagentItem.B7)
            if (b9ReagentRangeType == ReagentLevel.Low) supplementRecommendations.add(ReagentItem.B9)
        }
        return supplementRecommendations
    }

    suspend fun hasFoodPlanRecommendations(): Boolean {

        val plansReponse = planManager.getUserPlans()
        val recommendationResponse = foodManager.getRecommendedFoodItems()
        val reagentDeficienciesResponse = scoreManager.reagentDeficiencies()

        if (reagentDeficienciesResponse is Result.Success && plansReponse is Result.Success && recommendationResponse is Result.Success) {
            val addedFoodPlans = plansReponse.value.filter { it.isFoodPlan() }
            var foodPlanMagnesium = 0f
            var foodPlanVitaminC = 0f
            var foodPlanB7 = 0f
            var foodPlanB9 = 0f
            for (recommendation in addedFoodPlans) {
                foodPlanMagnesium += (
                    recommendation.food?.nutrient_Magnesium
                        ?: 0f
                    ).toFloat() * (recommendation.multiple ?: 1).toFloat()

                foodPlanVitaminC += (
                    recommendation.food?.nutrient_VitaminC
                        ?: 0f
                    ).toFloat() * (recommendation.multiple ?: 1).toFloat()

                foodPlanB7 += (
                    recommendation.food?.nutrient_VitaminB7
                        ?: 0f
                    ).toFloat() * (recommendation.multiple ?: 1).toFloat()

                foodPlanB9 += (
                    recommendation.food?.nutrient_VitaminB9
                        ?: 0f
                    ).toFloat() * (recommendation.multiple ?: 1).toFloat()
            }
            var recommendationCount = 0

            for (reagent in reagentDeficienciesResponse.value) {
                if (reagent.key.id == ReagentItem.B7.id && foodPlanB7 < reagent.value) {
                    recommendationCount++
                }
//              if (reagent.key.id == ReagentItem.B9.id && foodPlanB9 < reagent.value) {
//                    recommendationCount++
//              }
                if (reagent.key.id == ReagentItem.VitaminC.id && foodPlanVitaminC < reagent.value) {
                    recommendationCount++
                }
                if (reagent.key.id == ReagentItem.Magnesium.id && foodPlanMagnesium < reagent.value) {
                    recommendationCount++
                }
            }

            return recommendationCount > 0
        }
        return false
    }
    suspend fun hasFoodPlanRecommendations(reagentId: Int): Boolean {

        val plansReponse = planManager.getUserPlans()
        val recommendationResponse = foodManager.getRecommendedFoodItems()
        val reagentDeficienciesResponse = scoreManager.reagentDeficiencies()

        if (reagentDeficienciesResponse is Result.Success && plansReponse is Result.Success && recommendationResponse is Result.Success) {
            val addedFoodPlans = plansReponse.value.filter { it.isFoodPlan() }
            var foodPlanMagnesium = 0f
            var foodPlanVitaminC = 0f
            var foodPlanB7 = 0f
            var foodPlanCalcium = 0f
            for (recommendation in addedFoodPlans) {
                foodPlanMagnesium += (
                    recommendation.food?.nutrient_Magnesium
                        ?: 0f
                    ).toFloat() * (recommendation.multiple ?: 1).toFloat()

                foodPlanVitaminC += (
                    recommendation.food?.nutrient_VitaminC
                        ?: 0f
                    ).toFloat() * (recommendation.multiple ?: 1).toFloat()

                foodPlanB7 += (
                    recommendation.food?.nutrient_VitaminB7
                        ?: 0f
                    ).toFloat() * (recommendation.multiple ?: 1).toFloat()

                foodPlanCalcium += (
                    recommendation.food?.nutrient_Calcium
                        ?: 0f
                    ).toFloat() * (recommendation.multiple ?: 1).toFloat()
            }
            var recommendationCount = 0
            val reagentValue = when (reagentId) {
                ReagentItem.Magnesium.id -> foodPlanMagnesium
                ReagentItem.VitaminC.id -> foodPlanVitaminC
                ReagentItem.B7.id -> foodPlanB7
                ReagentItem.Calcium.id -> foodPlanCalcium
                else -> 0f
            }
            for (reagent in reagentDeficienciesResponse.value) {
                if (reagent.key.id == reagentId && reagentValue < reagent.value) {
                    recommendationCount++
                }
            }

            return recommendationCount > 0
        }
        return false
    }

    private fun mapFoodRecommendations(
        plans: List<PlanData>,
        foodRecs: List<FoodRecommendation>
    ): LinkedList<Pair<FoodRecommendation, PlanData>> {
        val planNumbers = plans.filter { it.food != null }.map { it.food?.id }
        val recommendationsSelected = LinkedList<Pair<FoodRecommendation, PlanData>>()

        foodRecs.filter { it.foodId != null }.forEach { foodItem ->
            if (planNumbers.contains(foodItem.foodId)) {
                recommendationsSelected.add(
                    Pair(
                        foodItem,
                        plans.first { it.food?.id == foodItem.foodId }
                    )
                )
            }
        }
        return recommendationsSelected
    }

    private fun checkIfAllSupplementRecommendationsAddedToPlan(): Boolean {
        if (planResponseData == null || recommendationResponseData == null) return false
        return (planResponseData as Result.Success<List<PlanData>>).value.firstOrNull { plan ->
            plan.isSupplementPlan()
        } != null || (recommendationResponseData as Result.Success<SampleRecommendations>).value.supplements.isEmpty()
    }

    private fun createCortisolRecommendationsPage(
        recommendationsPageMessage: SpannableStringBuilder? = null,
        recommendationsPageGoal: SpannableStringBuilder? = null,
        isReagentDeficient: Boolean = true,
        hasPlan: Boolean,
        showRedAlert: Boolean = false
    ): RecommendationsPage {
        return RecommendationsPage(
            recommendationTab = RecommendationTab.CORTISOL,
            recommendationsPageMessage = recommendationsPageMessage,
            recommendationsPageGoal = recommendationsPageGoal,
            isReagentDeficient = isReagentDeficient,
            hasItemsInPlan = hasPlan,
            hasPlan = hasPlan,
            showRedAlert = showRedAlert
        )
    }

    private fun createHydrationRecommendationsPage(
        recommendationsPageMessage: SpannableStringBuilder? = null,
        recommendationsPageGoal: SpannableStringBuilder? = null,
        isReagentDeficient: Boolean,
        hasPlan: Boolean,
        showRedAlert: Boolean = false,
        showActionButton: Boolean = true,
    ): RecommendationsPage {
        return RecommendationsPage(
            recommendationTab = RecommendationTab.HYDRATION,
            recommendationsPageMessage = recommendationsPageMessage,
            recommendationsPageGoal = recommendationsPageGoal,
            isReagentDeficient = isReagentDeficient,
            hasPlan = hasPlan,
            hasItemsInPlan = hasPlan,
            showRedAlert = showRedAlert,
            showActionButton = showActionButton
        )
    }

    private fun getReagentLevel(score: Score?, reagentItem: ReagentItem): ReagentLevel? {
        val reagent = reagentsManager.fromId(reagentItem.id)
        return score?.reagents?.get(score.reagents.keys.firstOrNull { it.id == reagent?.id })
            ?.lastOrNull { it.y != Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE }
            ?.y?.let { lastChartValue ->
                reagent?.ranges.orEmpty().firstOrNull {
                    if (reagent?.id == ReagentItem.Hydration.id) {
                        lastChartValue <= it.from && lastChartValue >= it.to
                    } else {
                        lastChartValue >= it.from && lastChartValue <= it.to
                    }
                }
            }?.reagentType
    }

    private fun getNutrientsGoals(): SpannableStringBuilder {
        return getFormattedGoals(
            R.string.nutrients,
            sortRecommendationGoals(
                arrayListOf(R.string.energy, R.string.immunity, R.string.mood)
            )
        )
    }

    private fun getHydrationGoals(): SpannableStringBuilder {
        return getFormattedGoals(
            R.string.hydration,
            sortRecommendationGoals(
                arrayListOf(R.string.energy, R.string.beauty, R.string.digestion)
            )
        )
    }

    private fun getCortisolGoals(): SpannableStringBuilder {
        return getFormattedGoals(
            R.string.cortisol,
            sortRecommendationGoals(
                arrayListOf(R.string.calm, R.string.focus, R.string.digestion)
            )
        )
    }

    private fun sortRecommendationGoals(goals: ArrayList<Int>): String {
        return goals.sortedByDescending {
            getUserSelectedGoals().indexOf(it)
        }.joinToString(", ") {
            resourceProvider.getString(it).capitalize(Locale.ENGLISH)
        }
    }

    private fun getUserSelectedGoals(): List<Int> {
        return goalsRepository.getGoals().filter {
            it.value
        }.keys.map {
            it.title
        }.toList()
    }

    private fun getFormattedGoals(title: Int, goals: String): SpannableStringBuilder {
        return getFormattedBoldText(
            resourceProvider.getString(
                R.string.build_food_plan_goals,
                resourceProvider.getString(title).decapitalize(Locale.ENGLISH)
            ),
            "$goals"
        )
    }

    private fun getFormattedBoldText(normalText: String, boldText: String): SpannableStringBuilder {

        val builder = SpannableStringBuilder()
        val spanFlag = Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        builder.append(normalText)
        builder.append(boldText, StyleSpan(Typeface.BOLD), spanFlag)
        builder.append(".")

        return builder
    }

    private suspend fun getButtonText(
        plans: List<PlanType>,
        plan: PlanType,
        score: Score
    ): String {
        val cortisolReagentRangeLevel = getReagentLevel(score, ReagentItem.Cortisol)
        val hydrationReagentRangeLevel = getReagentLevel(score, ReagentItem.Hydration)

        when (plan) {
            PlanType.TestPlan -> {
                return if (plans.contains(plan)) resourceProvider.getString(R.string.manage_reminder) else resourceProvider.getString(
                    R.string.create_a_reminder
                )
            }
            PlanType.WaterPlan -> {
                return if (plans.contains(plan) || hydrationReagentRangeLevel != ReagentLevel.Low) resourceProvider.getString(
                    R.string.edit_your_plan,
                    plan.title
                ) else resourceProvider.getString(R.string.complete_your_plan, plan.title)
            }
            PlanType.FoodPlan -> {
                return if (hasFoodPlanRecommendations()) resourceProvider.getString(
                    R.string.complete_your_plan,
                    plan.title
                ) else resourceProvider.getString(R.string.edit_your_plan, plan.title)
            }
            PlanType.SupplementPlan -> {
                return if (checkIfAllSupplementRecommendationsAddedToPlan()) resourceProvider.getString(
                    R.string.edit_your_plan,
                    plan.title
                ) else resourceProvider.getString(R.string.complete_your_plan, plan.title)
            }
            PlanType.StressPlan -> {
                return if (hasPlan(
                        plans,
                        PlanType.StressPlan
                    ) || cortisolReagentRangeLevel != ReagentLevel.High
                ) resourceProvider.getString(
                    R.string.edit_your_plan,
                    plan.title
                ) else resourceProvider.getString(R.string.complete_your_plan, plan.title)
            }
            else -> return ""
        }
    }

    private suspend fun getDescription(
        plans: List<PlanType>,
        plan: PlanType,
        score: Score
    ): String {
        val cortisolReagentRangeLevel = getReagentLevel(score, ReagentItem.Cortisol)
        val hydrationReagentRangeLevel = getReagentLevel(score, ReagentItem.Hydration)

        when (plan) {
            PlanType.TestPlan -> {
                return if (plans.contains(plan)) resourceProvider.getString(R.string.test_cover_needs) else resourceProvider.getString(
                    R.string.need_more_test
                )
            }
            PlanType.WaterPlan -> {
                return if (plans.contains(plan) || hydrationReagentRangeLevel != ReagentLevel.Low) resourceProvider.getString(
                    R.string.hydration_cover_needs
                ) else resourceProvider.getString(
                    R.string.need_more_hydration
                )
            }
            PlanType.FoodPlan -> {
                return if (hasFoodPlanRecommendations()) resourceProvider.getString(R.string.need_more_food) else resourceProvider.getString(
                    R.string.food_cover_needs
                )
            }
            PlanType.SupplementPlan -> {
                return if (checkIfAllSupplementRecommendationsAddedToPlan()) resourceProvider.getString(
                    R.string.supplement_cover_needs
                ) else resourceProvider.getString(
                    R.string.need_more_supplement
                )
            }
            PlanType.StressPlan -> {
                return if (hasPlan(
                        plans,
                        PlanType.StressPlan
                    ) || cortisolReagentRangeLevel != ReagentLevel.High
                ) resourceProvider.getString(
                    R.string.stress_cover_needs
                ) else resourceProvider.getString(R.string.need_more_stress)
            }
            else -> return ""
        }
    }

    private suspend fun getBackground(
        plans: List<PlanType>,
        plan: PlanType,
        score: Score
    ): Int {
        val cortisolReagentRangeLevel = getReagentLevel(score, ReagentItem.Cortisol)
        val hydrationReagentRangeLevel = getReagentLevel(score, ReagentItem.Hydration)

        when (plan) {
            PlanType.TestPlan -> {
                return if (plans.contains(plan)) R.drawable.green_background else R.drawable.rose_background
            }
            PlanType.WaterPlan -> {
                return if (plans.contains(plan) || hydrationReagentRangeLevel != ReagentLevel.Low) R.drawable.green_background else R.drawable.rose_background
            }
            PlanType.FoodPlan -> {
                return if (hasFoodPlanRecommendations()) R.drawable.rose_background else R.drawable.green_background
            }
            PlanType.SupplementPlan -> {
                return if (checkIfAllSupplementRecommendationsAddedToPlan()) R.drawable.green_background else R.drawable.rose_background
            }
            PlanType.StressPlan -> {
                return if (hasPlan(
                        plans,
                        PlanType.StressPlan
                    ) || cortisolReagentRangeLevel != ReagentLevel.High
                ) R.drawable.green_background else R.drawable.rose_background
            }
            else -> return 0
        }
    }

    private fun hasPlan(plans: List<PlanType>?, plan: PlanType) = (plans?.contains(plan)) ?: false

    fun checkIfStillHasPlan(recommendationTab: RecommendationTab): Boolean =
        when (recommendationTab) {
            RecommendationTab.NUTRIENTS ->
                hasPlan(todayIncludedPlans, PlanType.FoodPlan)
            RecommendationTab.CORTISOL -> hasPlan(todayIncludedPlans, PlanType.StressPlan)
            RecommendationTab.HYDRATION -> hasPlan(todayIncludedPlans, PlanType.WaterPlan)
            RecommendationTab.TESTING -> hasPlan(todayIncludedPlans, PlanType.TestPlan)
        }

    fun checkHasSupplementPlan(recommendationTab: RecommendationTab): Boolean =
        if (recommendationTab == RecommendationTab.NUTRIENTS) {
            hasPlan(todayIncludedPlans, PlanType.SupplementPlan)
        } else {
            false
        }

    suspend fun showRedAlertPlanBadge(recommendationTab: RecommendationTab) =
        when (recommendationTab) {
            RecommendationTab.NUTRIENTS -> hasFoodPlanRecommendations()
            RecommendationTab.CORTISOL -> !hasPlan(todayIncludedPlans, PlanType.StressPlan)
            RecommendationTab.HYDRATION -> !hasPlan(todayIncludedPlans, PlanType.WaterPlan)
            RecommendationTab.TESTING -> !hasPlan(todayIncludedPlans, PlanType.TestPlan)
        }

    fun showRedAlertSupplementPlanBadge(recommendationTab: RecommendationTab) =
        if (recommendationTab == RecommendationTab.NUTRIENTS) {
            checkHasSupplementPlan(RecommendationTab.NUTRIENTS)
        } else {
            false
        }

    suspend fun countRedBadges(): Int {
        var count = 0
        val scoresResponse = scoreManager.getWellnessScores(true)
        val recommendationResponse = recommendationManager.getLatestSampleRecommendations(true)
        val reagentDeficienciesResponse = scoreManager.reagentDeficiencies()
        val plansResponse = planManager.getUserPlans(true)
        if (scoresResponse is Result.Success && reagentDeficienciesResponse is Result.Success && plansResponse is Result.Success && recommendationResponse is Result.Success) {
            val score = scoresResponse.value as? Score
            planResponseData = plansResponse
            recommendationResponseData = recommendationResponse
            val todayIncludedPlans = getIncludedPlans(plansResponse.value)
            // cortisol -> only show a notification when a user is high in cortisol
            // and has not added anything to their stress relief plan
            val cortisolReagentRangeLevel = getReagentLevel(score, ReagentItem.Cortisol)
            if ((
                hasPlan(
                        todayIncludedPlans,
                        PlanType.StressPlan
                    ) || cortisolReagentRangeLevel != ReagentLevel.High
                ).not()
            ) {
                count++
            }

            // hydration -> only show a notification when a user is dehydrated
            // and has not created a plan
            val hydrationReagentRangeLevel = getReagentLevel(score, ReagentItem.Hydration)
            if ((
                hasPlan(
                        todayIncludedPlans,
                        PlanType.WaterPlan
                    ) || hydrationReagentRangeLevel == ReagentLevel.Low
                ).not()
            ) {
                count++
            }

            if (todayIncludedPlans.contains(PlanType.TestPlan).not()) {
                count++
            }

            if (hasFoodPlanRecommendations())
                count++
            if (checkIfAllSupplementRecommendationsAddedToPlan().not())
                count++
            return count
        }
        return count
    }

    private fun getIncludedPlans(plans: List<PlanData>): List<PlanType> {
        val includedPlans: MutableList<PlanType> = mutableListOf()
        plans.forEach { plan ->
            when {
                plan.isStressPlan() -> {
                    includedPlans.add(PlanType.StressPlan)
                }
                plan.isFoodPlan() -> {
                    includedPlans.add(PlanType.FoodPlan)
                }
                plan.isHydrationPlan() -> {
                    includedPlans.add(PlanType.WaterPlan)
                }
                plan.isTestingPlan() -> {
                    includedPlans.add(PlanType.TestPlan)
                }
                else -> {
                    includedPlans.add(PlanType.SupplementPlan)
                }
            }
        }
        return includedPlans
    }

    private fun getFoodPlanRecommendations(
        foodRecommendations: List<FoodRecommendation>,
        reagentDeficiencies: Map<Reagent, Float>,
        plans: List<PlanData>
    ): List<Pair<String, Int>> {
        val selectedFoodRecommendation = mapFoodRecommendations(foodRecommendations, plans)

        return reagentDeficiencies.map { entry ->
            val (reagentNeeded, _) = calculateReagentInFoodPlan(
                selectedFoodRecommendation.first,
                entry
            )

            if (reagentNeeded > 0) {
                Pair(
                    entry.key.displayName,
                    reagentNeeded
                )
            } else {
                null
            }
        }.filterNotNull()
    }

    private fun mapFoodRecommendations(
        foodRecs: List<FoodRecommendation>,
        plans: List<PlanData>
    ): Pair<LinkedList<FoodRecommendation>, LinkedList<FoodRecommendation>> {
        val planIds = plans.map { it.food_id }
        val recommendationsSelected = LinkedList<FoodRecommendation>()
        val recommendationsNotSelected = LinkedList<FoodRecommendation>()
        foodRecs.forEach {
            if (planIds.contains(it.foodId)) {
                recommendationsSelected.add(it)
            } else {
                recommendationsNotSelected.add(it)
            }
        }
        return recommendationsSelected to recommendationsNotSelected
    }

    private fun calculateReagentInFoodPlan(
        recommendationsSelected: List<FoodRecommendation>,
        deficiency: Map.Entry<Reagent, Float>
    ): Pair<Int, Int> {
        val reagentInFoodPlan = recommendationsSelected.sumOf { foodRec ->
            val multiple = ((planResponseData as? Result.Success<List<PlanData>>)?.value).orEmpty()
                .firstOrNull { it.food?.usda_ndb_number == foodRec.usdaNdbNumber.toString() }?.multiple
                ?: 1
            foodRec.nutrients.sumOf {
                if (it.reagent.id == deficiency.key.id) it.amount.times(multiple).toInt()
                else 0
            }
        }

        return (deficiency.value).toInt() to reagentInFoodPlan
    }

    suspend fun onTestingPlanClicked(): Result<Any, ErrorMessage> {
        val testPlan =
            ((planResponseData as? Result.Success<List<PlanData>>)?.value).orEmpty().firstOrNull {
                it.isTestingPlan()
            }
        val id =
            testPlan?.food_id ?: (
                testPlan?.tip_id
                    ?: testPlan?.reagent_lifestyle_recommendation_id
                )
        val type = when {
            testPlan?.food_id != null -> LikeCategory.Food.value
            testPlan?.tip_id != null -> LikeCategory.Tip.value
            else -> LikeCategory.Lifestyle.value
        }
        if (testPlan == null) {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_ADDED)
                    .addProperty(TrackingConstants.KEY_ID, id.toString())
                    .addProperty(TrackingConstants.TYPE, type)
                    .addProperty(TrackingConstants.KEY_LOCATION, "Recommendations Page")
                    .addProperty("has reminders", "true")
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
            return planManager.addPlanItem(lifeStyleId = Constants.TEST_PLAN_ID)
        } else {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                    .addProperty(TrackingConstants.KEY_ID, id.toString())
                    .addProperty(TrackingConstants.TYPE, type)
                    .addProperty(TrackingConstants.KEY_LOCATION, "Recommendations Page")
                    .addProperty("has reminders", "true")
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
            return planManager.deletePlanItem(listOf(testPlan.id ?: Constants.TEST_PLAN_ID))
        }
    }

    fun getViewPlanState() = viewPlanStateChannel.asFlow()

    fun updateViewPlanState() {
        viewPlanStateChannel.offer(true)
    }

    fun clearViewPlanState() {
        viewPlanStateChannel.offer(false)
    }

    fun updateReagentRecommendation(recommendList: ArrayList<ReagentRecommendationModel>) {
        this.recommendList = recommendList
    }
    fun getReagentRecommendation(): List<ReagentRecommendationModel> {
        return recommendList
    }
    fun removeReagentRecommendation(reagentId: Int) {
        this.recommendList.removeIf {
            it.reagentId == reagentId
        }
    }

    fun setLastReagent(lastReagent: ReagentRecommendationModel) {
        this.lastReagent = lastReagent
    }

    fun getLastReagent(): ReagentRecommendationModel ? {
        return lastReagent
    }

    companion object {
        const val WATER_PLAN_API_NAME = "water"
        const val TEST_PLAN_API_NAME = "Take a Test"
        const val TEST_VESSEL_FUEL_API_NAME = "Vessel Fuel"
    }
}
