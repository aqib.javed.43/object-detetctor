package com.vessel.app.photocapture

import androidx.lifecycle.MutableLiveData
import com.vessel.app.R
import com.vessel.app.photocapture.model.PhotoCaptureHintModel
import com.vessel.app.taketest.model.UploadTestFileModel
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PhotoCaptureState @Inject constructor() {
    var uuid: String? = null
    val retakePhoto = LiveEvent<Int>()
    val shadowOverlayVisibility = MutableLiveData(false)
    val frostedBorder = MutableLiveData(R.drawable.frosted_border_red)
    val onBackAction = LiveEvent<Unit>()
    val uploadImageFile = MutableLiveData<UploadTestFileModel>()
    val photoCaptureHint = MutableLiveData(PhotoCaptureHintModel(R.string.align_test_card, R.color.white))
    val takePhotoManuallyButtonDelayTimerStates = MutableLiveData(TakePhotoManuallyButtonDelayTimerStates.NOT_STARTED)
    val showTakePhotoButton = MutableLiveData(false)
    val showChoosePhotoButton = MutableLiveData(false)
    val playScanAnimation = MutableLiveData(false)
    operator fun invoke(block: PhotoCaptureState.() -> Unit) = apply(block)
}

enum class TakePhotoManuallyButtonDelayTimerStates {
    NOT_STARTED, CANCELED, ENDED
}
