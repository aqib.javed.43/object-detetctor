{
    "reagentID": 11,
    "name": "B7",
    "howToRead" : [{ "information": "When you’re in the good zone, your body is likely getting enough biotin to support your wellness.\n\nToo high means you may be getting more than you need. Although there isn’t much evidence of danger from high levels of biotin, you may be wasting money if you’re taking a supplement.\n\nToo low means you likely are not getting enough biotin, and you might want to up your intake. Check out the recommendations in the app for how to do that."}],
    "whyItMatters" : [{ "information": "Biotin, also called vitamin B7, is an essential vitamin that helps your body turn the carbohydrates, fats, and proteins in the food you eat into the energy you need. It also helps to regulate gene expression."}],
    "tipsToImprove" : [{ "information": "The best way to improve your biotin stores is with whole fresh foods. Some people may need additional biotin in the form of supplementation."}],
    "benefitsOfBeingOptimal": [
        {
            "informationTypeID": 2,
            "informationType": "Benefits of being optimal",
            "information": "Energy",
            "sources": [
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/15628671/",
                    "sourceTitle": "Biotin deficiency is associated with depression and fatigue.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/23010431/",
                    "sourceTitle": "Biotin is important for generation of energy on a cellular level.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/15992683",
                    "sourceTitle": "Biotin likely helps to develop glycogen, a form of energy the body uses in between meals and during exercise.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/16765926/",
                    "sourceTitle": "Biotin is important for turning food into energy, and for providing fuel for the body during times of fasting.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/24801390/",
                    "sourceTitle": "Biotin deficiency led to a substantial reduction in ATP, the main energy source for the body and mind in this animal study.",
                    "sourceAuthor": ""
                }
            ],
            "insertDate": null,
            "reagentContentID": 934
        },
        {
            "informationTypeID": 2,
            "informationType": "Benefits of being optimal",
            "information": "Mood",
            "sources": [
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/15628671/",
                    "sourceTitle": "Biotin deficiency is associated with depression and fatigue.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/27011198/",
                    "sourceTitle": "Biotin deficiency is associated with depressive symptoms in children.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/6406708/",
                    "sourceTitle": "Biotin deficiency was associated with severe depression and delirium in this case report.",
                    "sourceAuthor": ""
                },
                {
                    "source": "http://www.sciencedirect.com/science/article/pii/S2211034815000061",
                    "sourceTitle": "Biotin activates enzymes that improve nerve conduction which is essential for a healthy brain.",
                    "sourceAuthor": ""
                }
            ],
            "insertDate": null,
            "reagentContentID": 824
        },
        {
            "informationTypeID": 2,
            "informationType": "Benefits of being optimal",
            "information": "Body",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/17506119",
                    "sourceTitle": "Good biotin levels are associated with improved glycemic control in obese people, and blood sugar control is critically important for fat loss.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/26168302/",
                    "sourceTitle": "Good biotin levels help with burning fat",
                    "sourceAuthor": ""
                },
               
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/26158509/",
                    "sourceTitle": "Biotin deficiency is associated with slower metabolism",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/12055344/",
                    "sourceTitle": "Good biotin levels are important for healthy metabolism",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/15992683",
                    "sourceTitle": "Biotin likely helps to develop glycogen, a form of energy the body uses in between meals and during exercise.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/25997216/",
                    "sourceTitle": "Biotin likely helps to develop glycogen, a form of energy the body uses in between meals and during exercise.",
                    "sourceAuthor": ""
                }
            ],
            "insertDate": null,
            "reagentContentID": 824
        },
        {
            "informationTypeID": 2,
            "informationType": "Benefits of being optimal",
            "information": "Immunity",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/books/NBK547751/#:~:text=These%20include%20hair%20loss%20(alopecia,develop%20conjunctivitis%20and%20skin%20infections.",
                    "sourceTitle": "Biotin deficiency may be associated with defects in B-cell and T-cell immunity, and natural killer cell function.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5129763/",
                    "sourceTitle": "Biotin deficiency is associated with potentially dangerous over-activation of the inflammatory response of our immune system.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/26168302/",
                    "sourceTitle": "Good biotin levels may be associated with maintaining a healthy functioning immune system.",
                    "sourceAuthor": ""
                }
            ],
            "insertDate": null,
            "reagentContentID": 934
        },
        {
            "informationTypeID": 2,
            "informationType": "Benefits of being optimal",
            "information": "Beauty",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/2273113?dopt=Abstract",
                    "sourceTitle": "Biotin treatment led to a 25% increase in nail thickness in people with brittle nails.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/2648686?dopt=Abstract",
                    "sourceTitle": "Biotin is an effective treatment for thin nails",
                    "sourceAuthor": ""
                },
                
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/3923177?dopt=Abstract",
                    "sourceTitle": "Biotin deficiency is associated with hair loss and skin rashes on the lips, cheeks, and scalp.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/6406708/",
                    "sourceTitle": "Biotin deficiency is associated with hair loss and skin rashes on the lips, cheeks, and scalp.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/15863846?dopt=Abstract",
                    "sourceTitle": "Biotin deficiency is characterized by hair loss and a red scaly rash around body orifices.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/31638351/",
                    "sourceTitle": "Biotin is helpful in the treatment of acne and dandruff (seborrheic dermatitis)",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://pubmed.ncbi.nlm.nih.gov/31022908/",
                    "sourceTitle": "Biotin deficiency can also lead to zinc deficiency which is another cause of a severe type of skin rash.",
                    "sourceAuthor": ""
                },
                {
                    "source": "https://jhu.pure.elsevier.com/en/publications/modern-nutrition-in-health-and-disease-eleventh-edition",
                    "sourceTitle": "Low biotin levels are associated with bloodshot eyes.",
                    "sourceAuthor": ""
                }
            ],
            "insertDate": null,
            "reagentContentID": 824
        }
    ],"benefitsOfBeingOptimal_": [
        {
            "informationTypeID": 2,
            "informationType": "Benefits of being optimal",
            "information": "Healthy hair, skin and nails",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/nlmcatalog/101591510",
                    "sourceTitle": "Biotin. In: Modern Nutrition in Health and Disease. 11th ed. Baltimore, MD: Lippincott Williams & Wilkins; 2014:390-8.",
                    "sourceAuthor": "Mock DM. Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds."
                }
            ],
            "insertDate": null,
            "reagentContentID": 824
        }
    ],
    "risksOfHavingLowLevels": [
        {
            "informationTypeID": 3,
            "informationType": "Risks of having low levels",
            "information": "Thinning hair or hair loss",
            "sources": [
                {
                    "source": "https://the-eye.eu/public/Books/BioMed/Encyclopedia%20of%20Dietary%20Supplements%202nd%20ed%20-%20P.%20Coates%2C%20et%20al.%2C%20%28Informa%2C%202010%29%20WW.pdf",
                    "sourceTitle": "Encyclopedia of Dietary Supplements. 2nd ed. London and New York: Informa Healthcare; 2010:43-51.",
                    "sourceAuthor": "Mock DM. Biotin. In: Coates PM, Betz JM, Blackman MR, et al., eds."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/28879195",
                    "sourceTitle": "A Review of the Use of Biotin for Hair Loss. Skin Appendage Disord 2017;3:166–169",
                    "sourceAuthor": "Deepa P. Patel, Shane M. Swink, Leslie Castelo-Soccio"
                }
            ],
            "insertDate": null,
            "reagentContentID": 838
        },
        {
            "informationTypeID": 3,
            "informationType": "Risks of having low levels",
            "information": "Scaly, red rash around body openings",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/nlmcatalog/101591510",
                    "sourceTitle": "Biotin. In: Modern Nutrition in Health and Disease. 11th ed. Baltimore, MD: Lippincott Williams & Wilkins; 2014:390-8.",
                    "sourceAuthor": "Mock DM. Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds."
                },
                {
                    "source": "https://journals.lww.com/jcge/Citation/2013/04000/Present_Knowledge_in_Nutrition,_10th_Edition.18.aspx",
                    "sourceTitle": "Present Knowledge in Nutrition. 10th ed. Washington, DC: Wiley-Blackwell; 2012:359-74.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Kuroishi T. Biotin. In: Erdman JW, Macdonald IA, Zeisel SH, eds."
                }
            ],
            "insertDate": null,
            "reagentContentID": 839
        },
        {
            "informationTypeID": 3,
            "informationType": "Risks of having low levels",
            "information": "Conjunctivitis",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/nlmcatalog/101591510",
                    "sourceTitle": "Biotin. In: Modern Nutrition in Health and Disease. 11th ed. Baltimore, MD: Lippincott Williams & Wilkins; 2014:390-8.",
                    "sourceAuthor": "Mock DM. Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds."
                },
                {
                    "source": "https://journals.lww.com/jcge/Citation/2013/04000/Present_Knowledge_in_Nutrition,_10th_Edition.18.aspx",
                    "sourceTitle": "Present Knowledge in Nutrition. 10th ed. Washington, DC: Wiley-Blackwell; 2012:359-74.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Kuroishi T. Biotin. In: Erdman JW, Macdonald IA, Zeisel SH, eds."
                }
            ],
            "insertDate": null,
            "reagentContentID": 840
        },
        {
            "informationTypeID": 3,
            "informationType": "Risks of having low levels",
            "information": "Ketolactic acidosis",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/nlmcatalog/101591510",
                    "sourceTitle": "Biotin. In: Modern Nutrition in Health and Disease. 11th ed. Baltimore, MD: Lippincott Williams & Wilkins; 2014:390-8.",
                    "sourceAuthor": "Mock DM. Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds."
                },
                {
                    "source": "https://journals.lww.com/jcge/Citation/2013/04000/Present_Knowledge_in_Nutrition,_10th_Edition.18.aspx",
                    "sourceTitle": "Present Knowledge in Nutrition. 10th ed. Washington, DC: Wiley-Blackwell; 2012:359-74.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Kuroishi T. Biotin. In: Erdman JW, Macdonald IA, Zeisel SH, eds."
                }
            ],
            "insertDate": null,
            "reagentContentID": 841
        },
        {
            "informationTypeID": 3,
            "informationType": "Risks of having low levels",
            "information": "Aciduria",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/nlmcatalog/101591510",
                    "sourceTitle": "Biotin. In: Modern Nutrition in Health and Disease. 11th ed. Baltimore, MD: Lippincott Williams & Wilkins; 2014:390-8.",
                    "sourceAuthor": "Mock DM. Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds."
                },
                {
                    "source": "https://journals.lww.com/jcge/Citation/2013/04000/Present_Knowledge_in_Nutrition,_10th_Edition.18.aspx",
                    "sourceTitle": "Present Knowledge in Nutrition. 10th ed. Washington, DC: Wiley-Blackwell; 2012:359-74.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Kuroishi T. Biotin. In: Erdman JW, Macdonald IA, Zeisel SH, eds."
                }
            ],
            "insertDate": null,
            "reagentContentID": 842
        },
        {
            "informationTypeID": 3,
            "informationType": "Risks of having low levels",
            "information": "Seizures",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/nlmcatalog/101591510",
                    "sourceTitle": "Biotin. In: Modern Nutrition in Health and Disease. 11th ed. Baltimore, MD: Lippincott Williams & Wilkins; 2014:390-8.",
                    "sourceAuthor": "Mock DM. Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds."
                },
                {
                    "source": "https://journals.lww.com/jcge/Citation/2013/04000/Present_Knowledge_in_Nutrition,_10th_Edition.18.aspx",
                    "sourceTitle": "Present Knowledge in Nutrition. 10th ed. Washington, DC: Wiley-Blackwell; 2012:359-74.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Kuroishi T. Biotin. In: Erdman JW, Macdonald IA, Zeisel SH, eds."
                }
            ],
            "insertDate": null,
            "reagentContentID": 843
        },
        {
            "informationTypeID": 3,
            "informationType": "Risks of having low levels",
            "information": "Skin infection",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/nlmcatalog/101591510",
                    "sourceTitle": "Biotin. In: Modern Nutrition in Health and Disease. 11th ed. Baltimore, MD: Lippincott Williams & Wilkins; 2014:390-8.",
                    "sourceAuthor": "Mock DM. Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds."
                },
                {
                    "source": "https://journals.lww.com/jcge/Citation/2013/04000/Present_Knowledge_in_Nutrition,_10th_Edition.18.aspx",
                    "sourceTitle": "Present Knowledge in Nutrition. 10th ed. Washington, DC: Wiley-Blackwell; 2012:359-74.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Kuroishi T. Biotin. In: Erdman JW, Macdonald IA, Zeisel SH, eds."
                }
            ],
            "insertDate": null,
            "reagentContentID": 844
        },
        {
            "informationTypeID": 3,
            "informationType": "Risks of having low levels",
            "information": "Brittle nails",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/17763607",
                    "sourceTitle": "Vitamins and minerals: their role in nail health and disease. J Drugs Dermatol. 2007 Aug;6(8):782-7.",
                    "sourceAuthor": "Scheinfeld N, Dahdah MJ, Scher R."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/2273113",
                    "sourceTitle": "Treatment of brittle fingernails and onychoschizia with biotin: scanning electron microscopy. J Am Acad Dermatol 1990;23:1127-32.",
                    "sourceAuthor": "Colombo VE, Gerber F, Bronhofer M, Floersheim GL."
                }
            ],
            "insertDate": null,
            "reagentContentID": 845
        },
        {
            "informationTypeID": 3,
            "informationType": "Risks of having low levels",
            "information": "depression, lethargy, hallucinations",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/nlmcatalog/101591510",
                    "sourceTitle": "Biotin. In: Modern Nutrition in Health and Disease. 11th ed. Baltimore, MD: Lippincott Williams & Wilkins; 2014:390-8.",
                    "sourceAuthor": "Mock DM. Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds."
                },
                {
                    "source": "https://journals.lww.com/jcge/Citation/2013/04000/Present_Knowledge_in_Nutrition,_10th_Edition.18.aspx",
                    "sourceTitle": "Present Knowledge in Nutrition. 10th ed. Washington, DC: Wiley-Blackwell; 2012:359-74.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Kuroishi T. Biotin. In: Erdman JW, Macdonald IA, Zeisel SH, eds."
                }
            ],
            "insertDate": null,
            "reagentContentID": 846
        }
    ],
    "risksOfHavingHighLevels": [
        {
            "informationTypeID": 4,
            "informationType": "Risks of having high levels",
            "information": "Bad taste in your mouth",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/19319844",
                    "sourceTitle": "Biotin. Biofactors 2009;35:36-46.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Hassan YI."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/10075337",
                    "sourceTitle": "Bioavailability of biotin given orally to humans in pharmacologic doses. Am J Clin Nutr. 1999 Mar; 69(3):504-8.",
                    "sourceAuthor": "Zempleni J, Mock DM."
                }
            ],
            "insertDate": null,
            "reagentContentID": 830
        },
        {
            "informationTypeID": 4,
            "informationType": "Risks of having high levels",
            "information": "Nausea",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/19319844",
                    "sourceTitle": "Biotin. Biofactors 2009;35:36-46.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Hassan YI."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/10075337",
                    "sourceTitle": "Bioavailability of biotin given orally to humans in pharmacologic doses. Am J Clin Nutr. 1999 Mar; 69(3):504-8.",
                    "sourceAuthor": "Zempleni J, Mock DM."
                }
            ],
            "insertDate": null,
            "reagentContentID": 831
        },
        {
            "informationTypeID": 4,
            "informationType": "Risks of having high levels",
            "information": "Loss of appetite",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/19319844",
                    "sourceTitle": "Biotin. Biofactors 2009;35:36-46.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Hassan YI."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/10075337",
                    "sourceTitle": "Bioavailability of biotin given orally to humans in pharmacologic doses. Am J Clin Nutr. 1999 Mar; 69(3):504-8.",
                    "sourceAuthor": "Zempleni J, Mock DM."
                }
            ],
            "insertDate": null,
            "reagentContentID": 832
        },
        {
            "informationTypeID": 4,
            "informationType": "Risks of having high levels",
            "information": "Confusion",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/19319844",
                    "sourceTitle": "Biotin. Biofactors 2009;35:36-46.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Hassan YI."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/10075337",
                    "sourceTitle": "Bioavailability of biotin given orally to humans in pharmacologic doses. Am J Clin Nutr. 1999 Mar; 69(3):504-8.",
                    "sourceAuthor": "Zempleni J, Mock DM."
                }
            ],
            "insertDate": null,
            "reagentContentID": 833
        },
        {
            "informationTypeID": 4,
            "informationType": "Risks of having high levels",
            "information": "Irritability",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/19319844",
                    "sourceTitle": "Biotin. Biofactors 2009;35:36-46.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Hassan YI."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/10075337",
                    "sourceTitle": "Bioavailability of biotin given orally to humans in pharmacologic doses. Am J Clin Nutr. 1999 Mar; 69(3):504-8.",
                    "sourceAuthor": "Zempleni J, Mock DM."
                }
            ],
            "insertDate": null,
            "reagentContentID": 834
        },
        {
            "informationTypeID": 4,
            "informationType": "Risks of having high levels",
            "information": "Sleep pattern disturbance",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/19319844",
                    "sourceTitle": "Biotin. Biofactors 2009;35:36-46.",
                    "sourceAuthor": "Zempleni J, Wijeratne SSK, Hassan YI."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/10075337",
                    "sourceTitle": "Bioavailability of biotin given orally to humans in pharmacologic doses. Am J Clin Nutr. 1999 Mar; 69(3):504-8.",
                    "sourceAuthor": "Zempleni J, Mock DM."
                }
            ],
            "insertDate": null,
            "reagentContentID": 835
        },
        {
            "informationTypeID": 4,
            "informationType": "Risks of having high levels",
            "information": "masks vitamin B-12 deficiency",
            "sources": [
                {
                    "source": "https://ods.od.nih.gov/factsheets/Folate-HealthProfessional/",
                    "sourceTitle": "Folate: Fact sheet for health professionals.",
                    "sourceAuthor": "National Institutes of Health Office of Dietary Supplements."
                },
                {
                    "source": "https://www.mayoclinic.org/drugs-supplements-folate/art-20364625",
                    "sourceTitle": "Folate (folic acid).",
                    "sourceAuthor": "Mayo Clinic"
                }
            ],
            "insertDate": null,
            "reagentContentID": 836
        },
        {
            "informationTypeID": 4,
            "informationType": "Risks of having high levels",
            "information": "risk of colorectal cancer",
            "sources": [
                {
                    "source": "https://ods.od.nih.gov/factsheets/Folate-HealthProfessional/",
                    "sourceTitle": "Folate: Fact sheet for health professionals.",
                    "sourceAuthor": "National Institutes of Health Office of Dietary Supplements."
                },
                {
                    "source": "https://www.mayoclinic.org/drugs-supplements-folate/art-20364625",
                    "sourceTitle": "Folate (folic acid).",
                    "sourceAuthor": "Mayo Clinic"
                }
            ],
            "insertDate": null,
            "reagentContentID": 837
        }
    ],
    "possibleInteractionsWithCommonMedications": [
        {
            "informationTypeID": 5,
            "informationType": "Possible interactions with common medications",
            "information": "Biotin can interact with certain medications, most notably anticonvulsants.",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/7181453",
                    "sourceTitle": "Impaired biotin status in anticonvulsant therapy. Ann Neurol 1982;12:485-6.",
                    "sourceAuthor": "Krause KH, Berlit P, Bonjour JP."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/9371938",
                    "sourceTitle": "Biotin catabolism is accelerated in adults receiving long-term therapy with anticonvulsants. Neurology 1997;49:1444-7.",
                    "sourceAuthor": "Mock DM, Dyken ME."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/9523856",
                    "sourceTitle": "Disturbances in biotin metabolism in children undergoing long-term anticonvulsant therapy. J Pediatr Gastroenterol Nutr 1998;26:245-50. Said HM, Redha R, Nylander W. Biotin transport in the human intestine: inhibition by anticonvulsant drugs. Am J Clin Nutr 1989;49:127-31.",
                    "sourceAuthor": "Mock DM, Mock NI, Nelson RP, Lombard KA."
                }
            ],
            "insertDate": null,
            "reagentContentID": 828
        },
        {
            "informationTypeID": 5,
            "informationType": "Possible interactions with common medications",
            "information": "Individuals taking medications on a regular basis should discuss their biotin status with their healthcare providers",
            "sources": [
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/7181453",
                    "sourceTitle": "Impaired biotin status in anticonvulsant therapy. Ann Neurol 1982;12:485-6.",
                    "sourceAuthor": "Krause KH, Berlit P, Bonjour JP."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/9371938",
                    "sourceTitle": "Biotin catabolism is accelerated in adults receiving long-term therapy with anticonvulsants. Neurology 1997;49:1444-7.",
                    "sourceAuthor": "Mock DM, Dyken ME."
                },
                {
                    "source": "https://www.ncbi.nlm.nih.gov/pubmed/9523856",
                    "sourceTitle": "Disturbances in biotin metabolism in children undergoing long-term anticonvulsant therapy. J Pediatr Gastroenterol Nutr 1998;26:245-50. Said HM, Redha R, Nylander W. Biotin transport in the human intestine: inhibition by anticonvulsant drugs. Am J Clin Nutr 1989;49:127-31.",
                    "sourceAuthor": "Mock DM, Mock NI, Nelson RP, Lombard KA."
                }
            ],
            "insertDate": null,
            "reagentContentID": 829
        }
    ],
    "whatCanAffectBioAvailability": null,
    "thingsThatCanImproveYourLevels": null,
    "thingsThatCanLowerYourLevels": null,
    "whatItDoes": [
        {
            "informationTypeID": 9,
            "informationType": "What it does (FDA)",
            "information": "Protein, carbohydrate, and fat metabolism",
            "sources": [],
            "insertDate": null,
            "reagentContentID": 848
        },
        {
            "informationTypeID": 9,
            "informationType": "What it does (FDA)",
            "information": "Energy storage",
            "sources": [],
            "insertDate": null,
            "reagentContentID": 847
        }
    ],
    "helpsWith": [
        {
            "informationTypeID": 10,
            "informationType": "Helps with",
            "information": "Energy",
            "sources": [],
            "insertDate": null,
            "reagentContentID": 825
        },
        {
            "informationTypeID": 10,
            "informationType": "Helps with",
            "information": "Muscles",
            "sources": [],
            "insertDate": null,
            "reagentContentID": 826
        },
        {
            "informationTypeID": 10,
            "informationType": "Helps with",
            "information": "Beauty",
            "sources": [],
            "insertDate": null,
            "reagentContentID": 827
        }
    ],
    "highValueFoods": [
        {
            "ndB_No": "20060",
            "b7": 66.0,
            "b9": 63.0,
            "vitaminC": 0.0,
            "magnesium": 781.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/20060.jpg",
            "name": "Rice Bran"
        },
        {
            "ndB_No": "12036",
            "b7": 66.0,
            "b9": 227.0,
            "vitaminC": 1.3999999761581421,
            "magnesium": 325.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/12036.jpg",
            "name": "Sunflower Seeds"
        },
        {
            "ndB_No": "12061",
            "b7": 45.292308807373047,
            "b9": 44.0,
            "vitaminC": 0.0,
            "magnesium": 270.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/12061.jpg",
            "name": "Almonds"
        },
        {
            "ndB_No": "16069",
            "b7": 40.0,
            "b9": 479.0,
            "vitaminC": 4.5,
            "magnesium": 47.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/16069.jpg",
            "name": "Lentils"
        },
        {
            "ndB_No": "11304",
            "b7": 40.0,
            "b9": 65.0,
            "vitaminC": 40.0,
            "magnesium": 33.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/11304.jpg",
            "name": "Green Peas"
        },
        {
            "ndB_No": "12154",
            "b7": 37.0,
            "b9": 31.0,
            "vitaminC": 1.7000000476837158,
            "magnesium": 201.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/12154.jpg",
            "name": "Walnuts"
        },
        {
            "ndB_No": "16087",
            "b7": 37.0,
            "b9": 240.0,
            "vitaminC": 0.0,
            "magnesium": 168.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/16087.jpg",
            "name": "Peanuts"
        },
        {
            "ndB_No": "12142",
            "b7": 27.75,
            "b9": 22.0,
            "vitaminC": 1.1000000238418579,
            "magnesium": 121.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/12142.jpg",
            "name": "Pecans"
        },
        {
            "ndB_No": "11124",
            "b7": 25.0,
            "b9": 19.0,
            "vitaminC": 5.9000000953674316,
            "magnesium": 12.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/11124.jpg",
            "name": "Carrots"
        },
        {
            "ndB_No": "08640",
            "b7": 24.0,
            "b9": 32.0,
            "vitaminC": 0.0,
            "magnesium": 148.0,
            "imageUrl": "https://d32fui7xaadmx6.cloudfront.net/wellness/food/08640.jpg",
            "name": "Oatmeal"
        }
    ],
    "lowRecommendations": {
        "screens": [
            {
                "title": "Your biotin levels are Low",
                "subTitle": " Studies show biotin is important for improved energy, glowing skin, and balanced digestion due to its role in converting your food into the energy you need as well as regulating gene expression. "
            },
            {
                "title": "Reasons your Biotin is Low",
                "subTitle": "The most common cause of low biotin is not eating enough B7 in your diet. Other causes may include alcohol consumption and long-term use of certain medications. With proper food and supplementation, you should see an improvement in biotin levels quickly!"
            }
        ],
        "final": {
            "title": "",
            "subTitle": ""
        }
    },
    "highRecommendations": {
        "screens": [
            {
                "title": "Your biotin levels are Low",
                "subTitle": " Studies show biotin is important for improved energy, glowing skin, and balanced digestion due to its role in converting your food into the energy you need as well as regulating gene expression. "
            },
            {
                "title": "Reasons your Biotin is Low",
                "subTitle": "The most common cause of low biotin is not eating enough B7 in your diet. Other causes may include alcohol consumption and long-term use of certain medications. With proper food and supplementation, you should see an improvement in biotin levels quickly!"
            }
        ],
        "final": {
            "title": "",
            "subTitle": ""
        }
    }
}
