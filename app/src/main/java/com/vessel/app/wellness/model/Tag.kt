package com.vessel.app.wellness.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tag(val name: String, val isActive: Boolean, val id: Int, val isSelected: Boolean) :
    Parcelable
