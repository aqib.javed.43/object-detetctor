package com.vessel.app.testplan.model

import com.vessel.app.R
import kotlinx.android.parcel.IgnoredOnParcel

data class TestingPlanHeader(val isAdded: Boolean) {
    @IgnoredOnParcel
    val buttonResId = if (isAdded) R.string.remove_from_plan else R.string.add_to_plan
}
