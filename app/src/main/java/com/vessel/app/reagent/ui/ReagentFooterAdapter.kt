package com.vessel.app.reagent.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.reagent.model.ReagentFooter

class ReagentFooterAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        ReagentFooter -> R.layout.item_reagent_footer
        else -> throw IllegalStateException(
            "Unexpected WellnessFooterAdapter type at position $position for item ${getItem(position)}"
        )
    }
    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onTalkToNutritionistClick(view: View)
        fun onCustomerSupportClick()
    }
}
