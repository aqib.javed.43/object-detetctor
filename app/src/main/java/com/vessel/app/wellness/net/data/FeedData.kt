package com.vessel.app.wellness.net.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class FeedData(
    val actor: String? = null,

    @Json(name = "foreign_id")
    val foreignID: String? = null,

    val id: String? = null,

    @Json(name = "object")
    val resultObject: String? = null,

    val origin: Any? = null,

    @Json(name = "poll_questions")
    val pollQuestions: Any? = null,

    val target: String? = null,
    val time: String? = null,
    val verb: String? = null,
    val custompr: String? = null,

    @Json(name = "img_url_1")
    val imgURL1: String? = null,

    @Json(name = "img_url_2")
    val imgURL2: String? = null,

    @Json(name = "img_url_3")
    val imgURL3: String? = null,

    val text: String? = null,

    @Json(name = "video_url")
    val videoURL: String? = null
)
@JsonClass(generateAdapter = true)
data class PollQuestions(
    @Json(name = "response_2")
    var response2: String? = null,
    var question: String? = null,
    @Json(name = "contact_id")
    var contactID: String? = null,
    @Json(name = "response_1")
    var response1: String? = null,
    @Json(name = "response_3")
    var response3: String? = null,
    @Json(name = "response_4")
    var response4: String? = null
) : Serializable
@JsonClass(generateAdapter = true)
data class FeedReaction(
    @Json(name = "user_id")
    var userId: String? = null,
    var kind: String? = null,
    @Json(name = "activity_id")
    var activityId: String? = null,
    var data: ResponseData? = null
)
@JsonClass(generateAdapter = true)
data class ResponseData(
    var response: Int? = null,
)
enum class Action(val kind: String) {
    LIKE("like"),
    UNLIKE("unlike"),
    REPORT("report"),
    POLL_RESPONSE("poll_response_1")
}
@JsonClass(generateAdapter = true)
data class FeedFlag(
    var actor: String? = null,
    @Json(name = "object")
    var reason: String? = null,
    var verb: String? = null,
    @Json(name = "activity_id")
    var activityId: String? = null
)
@JsonClass(generateAdapter = true)
data class CreatePollRequest(
    var actor: String? = null,
    @Json(name = "object")
    var poll: String? = null,
    var verb: String = "poll",
    @Json(name = "poll_questions")
    var pollSchema: String? = null

)
