package com.vessel.app.common.model.data

import com.vessel.app.common.net.data.plan.SubGoalRecord
import com.vessel.app.wellness.net.data.SubGoalData

object SubGoalMapper {
    fun map(item: SubGoalData) = SubGoalRecord(
        item.id,
        item.rank.orEmpty(),
        item.name,
        item.goal_id,
        item.description.orEmpty(),
        item.symptom_name,
        item.image_small_url.orEmpty(),
        item.image_medium_url.orEmpty(),
        item.image_large_url.orEmpty(),
    )
}
