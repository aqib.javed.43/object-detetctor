package com.vessel.app.emailcheck

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseAppleGoogleAuthFragment
import com.vessel.app.common.model.AuthToken
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EmailCheckFragment : BaseAppleGoogleAuthFragment<EmailCheckViewModel>() {
    override val viewModel: EmailCheckViewModel by viewModels()
    override val layoutResId = R.layout.fragment_email_check

    override fun onAuthTokenReceived(authToken: AuthToken) {
        viewModel.onAuthTokenReceived(authToken)
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)

        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
