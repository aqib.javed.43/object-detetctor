package com.vessel.app.planready

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class PlanReadyHintViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
) : BaseViewModel(resourceProvider) {
    val currentHint = MutableLiveData(R.drawable.plan_tutorial_1)
    private var currentHintIndex = 0
    val navigate = LiveEvent<Any>()

    private val hints = if (preferencesRepository.isExistingUserFirstTime) listOf(
        R.drawable.plan_tutorial_1,
        R.drawable.plan_tutorial_2,
        R.drawable.plan_tutorial_3,
        R.drawable.plan_tutorial_4,
        R.drawable.plan_tutorial_6,
        R.drawable.plan_tutorial_5,
    ) else listOf(
        R.drawable.plan_tutorial_2,
        R.drawable.plan_tutorial_3,
        R.drawable.plan_tutorial_4,
        R.drawable.plan_tutorial_6,
        R.drawable.plan_tutorial_5,
    )

    fun onHintClicked() {
        if (hints.lastIndex == currentHintIndex) {
            devicePreferencesRepository.showPlanReadyHint = false
            navigate.call()
        } else {
            currentHintIndex++
            currentHint.value = hints.get(currentHintIndex)
        }
    }
}
