package com.vessel.app.today.ui

import android.animation.Animator
import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.core.view.isVisible
import com.airbnb.lottie.LottieAnimationView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Lesson
import com.vessel.app.views.ExpandableWithMoreTextView
import kotlinx.android.synthetic.main.item_lesson.view.*
import java.util.*

class LessonAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Lesson>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is Lesson -> R.layout.item_lesson
        else -> throw IllegalStateException(
            "Unexpected WellnessAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }

    var onLessonUpdate: (() -> Unit)? = null

    override fun getHandler(position: Int): Any = handler

    interface OnActionHandler {
        fun onLessonCompleted(item: Any, view: View)
        fun onLessonClicked(item: Any, view: View)
        fun onAnimationFinished(item: Any, view: View, parentPosition: Int)
        fun onLessonInformationClicked()
        fun onAddOtherGoal()
        fun onUnlockLesson(item: Any, programId: Int?, date: Date)
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<Lesson>,
        @SuppressLint("RecyclerView") position: Int
    ) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position)

        val checkbox = holder.itemView.findViewById<AppCompatCheckBox>(R.id.checkbox)
        val bottomBlur = holder.itemView.findViewById<View>(R.id.bottomBlurBackground)
        holder.itemView.findViewById<ExpandableWithMoreTextView>(R.id.description).apply {
            collapsedLineCount = 2
            text = item.description
        }
        val faddingView = holder.itemView.findViewById<View>(R.id.fadding)
        val enableCheckBox = (item.completed).not()
        holder.itemView.itemView.apply {
            if (!enableCheckBox) {
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                layoutParams.height = resources.getDimension(R.dimen.plan_item_height).toInt()
            }
        }
        holder.itemView.swipe.apply {
            if (!enableCheckBox) {
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                layoutParams.height = resources.getDimension(R.dimen.max_expert_name_width).toInt()
            }
        }
        checkbox.apply {
            isEnabled = false
            isClickable = false
        }
        bottomBlur.apply {
            visibility = if (enableCheckBox) View.VISIBLE else View.GONE
        }
        faddingView.apply {
            visibility = if (enableCheckBox) View.VISIBLE else View.GONE
        }
        holder.itemView.findViewById<ExpandableWithMoreTextView>(R.id.description).apply {
            visibility = if (enableCheckBox) View.VISIBLE else View.GONE
        }
        checkbox.setOnClickListener {
            if (item.completed) return@setOnClickListener
            item.completed = false
        }
        if (item.completed)handleLessonCheckClick(item, holder, position)
    }

    private fun handleLessonCheckClick(
        item: Lesson,
        holder: BaseViewHolder<Lesson>,
        position: Int
    ) {
        if (item.completed.not()) {
            holder.itemView.findViewById<LottieAnimationView>(R.id.planSuccessAnimationView)
                .playAnimation()
            holder.itemView.findViewById<LottieAnimationView>(R.id.planAddingAnimationView).apply {
                isVisible = true
                visibility = View.VISIBLE
                playAnimation()
            }
            holder.itemView.findViewById<View>(R.id.blurBackground).animate().alpha(1f).duration =
                ANIMATION_DURATION
            this@LessonAdapter.handler.onLessonCompleted(
                item,
                holder.itemView,
            )
        } else {
            holder.itemView.findViewById<View>(R.id.blurBackground).animate().apply {
                setListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        this@LessonAdapter.handler.onAnimationFinished(
                            getItem(position),
                            holder.itemView,
                            holder.bindingAdapterPosition
                        )
                    }

                    override fun onAnimationCancel(animation: Animator) {
                    }

                    override fun onAnimationRepeat(animation: Animator) {
                    }
                })
                alpha(0f).duration = ANIMATION_DURATION
            }
        }
    }

    companion object {
        private const val ANIMATION_VIEW_SCALE_FACTOR = 0.4f
        private const val ANIMATION_DURATION: Long = 500
        private const val DEFAULT_PADDING: Int = 100
    }
}
