package com.vessel.app.auth

import android.os.Bundle
import androidx.activity.viewModels
import com.pushwoosh.Pushwoosh
import com.vessel.app.R
import com.vessel.app.base.BaseActivity
import com.vessel.app.util.extensions.applyFullscreenFlags
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthActivity : BaseActivity<AuthViewModel>() {
    override val viewModel: AuthViewModel by viewModels()
    override val layoutResId = R.layout.activity_auth

    override fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        applyFullscreenFlags()

        if (intent.hasExtra(Pushwoosh.PUSH_RECEIVE_EVENT)) {
            viewModel.onReceiveNotification()
        }
    }
}
