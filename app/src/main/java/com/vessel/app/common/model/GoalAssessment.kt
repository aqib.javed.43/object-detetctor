package com.vessel.app.common.model

import java.util.*

data class GoalAssessment(
    val createdAt: Date,
    val assessments: List<Assessment>
)
