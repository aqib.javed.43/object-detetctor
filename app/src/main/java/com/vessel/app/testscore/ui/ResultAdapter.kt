package com.vessel.app.testscore.ui

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.animation.doOnStart
import androidx.core.content.ContextCompat
import com.google.android.material.card.MaterialCardView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.wellness.model.ReagentItem
import kotlinx.android.synthetic.main.item_reagent_result.view.*

class ResultAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<ReagentItem>(
    { oldItem, newItem ->
        oldItem.id == newItem.id
    }
) {

    var playStartingAnimation = false
    var fadingStartingAnimation = false
    var fadingAndColorScalingStartingAnimation = false

    override fun getLayout(position: Int) = R.layout.item_reagent_result

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<ReagentItem>, position: Int) {
        super.onBindViewHolder(holder, position)
        if (position % 2 == 1) {
            holder.itemView.translationY = 50.0f
        }
        val item = getItem(position)
        val backgroundColorInitialValue = ContextCompat.getColor(
            holder.itemView.context,
            item.preLastBackgroundColor
        )
        val endValue = ContextCompat.getColor(
            holder.itemView.context,
            item.backgroundColor
        )
        val cardContainer = holder.itemView.findViewById<MaterialCardView>(R.id.cardContainer)
        val status = holder.itemView.findViewById<AppCompatTextView>(R.id.status)
        cardContainer.setCardBackgroundColor(endValue)
        cardContainer.setOnClickListener {
            handler.onReagentSelected(item, position)
        }
        if (playStartingAnimation) {
            if (fadingStartingAnimation) {
                ObjectAnimator.ofFloat(holder.itemView.cardContainer, "alpha", 1f).apply {
                    duration = 1
                    startDelay = position.toLong() * 500
                    start()
                }
                status.text = item.status
            } else if (fadingAndColorScalingStartingAnimation) {
                cardContainer.setCardBackgroundColor(backgroundColorInitialValue)
                val alpha = ObjectAnimator.ofFloat(holder.itemView.cardContainer, "alpha", 1f).apply {
                    duration = 1
                    doOnStart {
                        status.text = item.preStatus
                    }
                }
                val backgroundColorValueAnimator =
                    ValueAnimator.ofArgb(backgroundColorInitialValue, endValue).apply {
                        duration = 1000
                        addUpdateListener {
                            cardContainer.setCardBackgroundColor(animatedValue as Int)
                        }
                        doOnStart {
                            if (item.preStatus != item.status) {
                                val fadeOut = ObjectAnimator.ofFloat(status, "alpha", 0f).apply {
                                    duration = 500
                                }
                                val fadeIn = ObjectAnimator.ofFloat(status, "alpha", 1f).apply {
                                    duration = 500
                                    doOnStart {
                                        status.text = item.status
                                    }
                                }
                                AnimatorSet().apply {
                                    playSequentially(fadeOut, fadeIn)
                                    start()
                                }
                            }
                        }
                    }
                AnimatorSet().apply {
                    startDelay = position.toLong() * 1500
                    playSequentially(alpha, backgroundColorValueAnimator)
                    start()
                }
            }
        } else {
            status.text = item.status
            cardContainer.alpha = 1f
        }
    }

    interface OnActionHandler {
        fun onReagentSelected(item: ReagentItem, position: Int)
    }
}
