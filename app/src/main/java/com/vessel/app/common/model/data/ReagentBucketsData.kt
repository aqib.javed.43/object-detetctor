package com.vessel.app.common.model.data

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.vessel.app.common.net.data.ReagentResponseInfoData
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ReagentBucketsData(
    @Json(name = "reagents")
    val reagents: List<ReagentDataItem?>?,
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class ReagentDataItem(
    @Json(name = "reagent_id")
    val reagentId: Int?,
    @Json(name = "reagent_name")
    val reagentName: String?,
    @Json(name = "display_name")
    val displayName: String?,
    @Json(name = "query_string")
    val queryString: String?,
    @Json(name = "is_beta")
    val isBeta: Boolean?,
    @Json(name = "is_inverted")
    val isInverted: Boolean?,
    @Json(name = "unit")
    val unit: String?,
    @Json(name = "min_value")
    val minValue: Float?,
    @Json(name = "max_value")
    val maxValue: Float?,
    @Json(name = "lower_bound")
    val lowerBound: Float?,
    @Json(name = "upper_bound")
    val upperBound: Float?,
    @Json(name = "recommended_daily_allowance")
    val recommendedDailyAllowance: Float?,
    @Json(name = "consumption_unit")
    val consumptionUnit: String?,
    @Json(name = "details_suggestions")
    val detailsSuggestions: RecommendationSuggestions?,
    @Json(name = "buckets")
    val buckets: List<Bucket?>?,
    val info: List<ReagentResponseInfoData>?,
    val state: String?,
    @Json(name = "impact")
    val impact: Int?
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class Bucket(
    @Json(name = "high")
    val high: Float?,
    @Json(name = "low")
    val low: Float?,
    @Json(name = "reported_value")
    val reportedValue: Float?,
    @Json(name = "score")
    val score: Float?,
    @Json(name = "evaluation")
    val evaluation: String?,
    @Json(name = "hint")
    val hint: BucketHint?,
    @Json(name = "recommendations")
    val recommendations: BucketRecommendation?,
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class BucketHint(
    @Json(name = "title")
    val title: String?,
    @Json(name = "description")
    val description: String?,
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class RecommendationSuggestions(
    @Json(name = "low")
    val low: String?,
    @Json(name = "good")
    val good: String?,
    @Json(name = "high")
    val high: String?
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class BucketRecommendation(
    @Json(name = "lifestyle")
    val lifestyle: List<BucketLifestyleRecommendation>?,
    @Json(name = "food")
    val food: BucketRDARecommendation?,
    @Json(name = "supplement")
    val supplement: BucketRDARecommendation?,
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class BucketLifestyleRecommendation(
    @Json(name = "name")
    val name: String?,
    @Json(name = "quantity")
    val quantity: Float?,
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class BucketRDARecommendation(
    @Json(name = "rda_multiplier")
    val rda_multiplier: Float?,
) : Parcelable
