package com.vessel.app.goalpage.model

import com.vessel.app.plan.model.PlanItem
import com.vessel.app.wellness.model.Paging
import com.vessel.app.wellness.model.ReagentItem
import com.vessel.app.wellness.model.RecommendationItem

data class GoalPageReagent(
    val title: String,
    val reagents: List<ReagentItem>? = arrayListOf()
)
data class GoalPageTodayActivity(
    val items: List<PlanItem>? = arrayListOf()
)
data class GoalPageRecommends(
    val title: String,
    val items: List<RecommendationItem>? = arrayListOf(),
    var recommendationsPaging: Paging = Paging(currentPage = 1, pageSize = 10)
)
