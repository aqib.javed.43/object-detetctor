package com.vessel.app.testerrordialog.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.testerrordialog.model.TestErrorHintFooter

class TestErrorHintFooterAdapter(private val handler: TestErrorHintHeaderAdapter.OnActionHandler) : BaseAdapterWithDiffUtil<TestErrorHintFooter>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is TestErrorHintFooter -> R.layout.item_test_hint_footer
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any? = handler
}
