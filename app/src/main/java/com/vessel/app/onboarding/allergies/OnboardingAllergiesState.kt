package com.vessel.app.onboarding.allergies

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.onboarding.allergies.model.OnboardingAllergySelect
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnboardingAllergiesState @Inject constructor(
    val isFirstTestFlow: Boolean,
    val isLastStep: Boolean,
) {
    val items = MutableLiveData<List<OnboardingAllergySelect>>()
    val refreshItems = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: OnboardingAllergiesState.() -> Unit) = apply(block)
}
