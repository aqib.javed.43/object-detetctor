package com.vessel.app.common.widget.loaderwidget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.vessel.app.common.model.Program
import com.vessel.app.databinding.WidgetPlanProgramsCardBinding
import com.vessel.app.programs.ui.PlanProgramAdapter

class PlanProgramsCardWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private lateinit var binding: WidgetPlanProgramsCardBinding
    private lateinit var programAdapter: PlanProgramAdapter
    private var programs: List<Program> = mutableListOf()
    private var handler: PlanProgramsCardWidgetOnActionHandler? = null

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetPlanProgramsCardBinding.inflate(layoutInflater, this, true)
        binding.programsList.apply {
            layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            itemAnimator = null
            PagerSnapHelper().attachToRecyclerView(this)
            binding.listIndicator.attachToRecyclerView(this)
        }
    }

    fun setHandler(programsHandler: PlanProgramsCardWidgetOnActionHandler) {
        handler = programsHandler
        if (::programAdapter.isInitialized.not()) {
            programAdapter = PlanProgramAdapter(programsHandler)
            binding.programsList.adapter = programAdapter
        }
    }

    fun setList(programList: List<Program>?) {
        binding.loadingProgram.isVisible = false
        this.programs = programList ?: listOf()
        binding.listIndicator.isVisible = programs.size > 1
        programAdapter.submitList(programList)
        programAdapter.notifyDataSetChanged()
    }

    fun isLoadingVisible(visible: Boolean) {
        binding.loadingProgram.isVisible = visible
    }
}

interface PlanProgramsCardWidgetOnActionHandler {
    fun onProgramItemClicked(item: Program)
    fun onProgramInfoClicked(item: Program)
}
