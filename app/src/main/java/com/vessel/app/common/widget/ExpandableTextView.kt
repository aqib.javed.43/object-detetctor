package com.vessel.app.common.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.vessel.app.R
import kotlinx.android.synthetic.main.view_expandable_text_view.view.*

class ExpandableTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr),
    View.OnLayoutChangeListener {

    companion object {
        private const val EXPANDED_ROTATION = 270f
        private const val COLLAPSED_ROTATION = 90f
    }

    private val durationLong: Long by lazy {
        context.resources
            .getInteger(android.R.integer.config_longAnimTime)
            .toLong()
    }

    private var bodyHeightChangeListener: BodyHeightChangeListener? = null
    private var expandedChangeListener: ExpandedChangeListener? = null
    private var bodyHeight = -1
    private var isExpanded = false
    private var isExpanding = false

    init {
        val view = inflate(context, R.layout.view_expandable_text_view, this)
        view.bodyTextView.addOnLayoutChangeListener(this)
        view.headerTextView.setOnClickListener {
            if (!isExpanding) {
                isExpanding = true
                view.expandImageView.animate()
                    .rotationBy(180f)
                    .setDuration(durationLong)
                    .setInterpolator(DecelerateInterpolator())
                    .withEndAction {
                        isExpanded = !isExpanded
                        isExpanding = false
                        expandedChangeListener?.onExpandedChanged(isExpanded)
                    }
                    .setUpdateListener { onRotationUpdated(it.animatedFraction, view.bodyTextView) }
                    .start()
            }
        }
    }

    private fun onRotationUpdated(
        animationFraction: Float,
        bodyTextView: AppCompatTextView
    ) {
        val fraction = if (isExpanded) {
            1 - animationFraction
        } else {
            animationFraction
        }
        bodyTextView.height = (bodyHeight * fraction).toInt()
    }

    override fun onLayoutChange(
        v: View?,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int,
        oldLeft: Int,
        oldTop: Int,
        oldRight: Int,
        oldBottom: Int
    ) {
        if (bodyHeight == -1) {
            bodyHeight = bodyTextView.measuredHeight
            bodyTextView.post { bodyTextView.height = 0 }
            bodyHeightChangeListener?.onBodyHeightChanged(bodyHeight)
        }
        bodyTextView.removeOnLayoutChangeListener(this)
    }

    fun setHeaderText(text: String) {
        headerTextView.text = text
    }

    fun setHeaderText(@StringRes textRes: Int) {
        headerTextView.setText(textRes)
    }

    fun setBodyText(text: String) {
        bodyTextView.text = text
    }

    fun setBodyText(@StringRes textRes: Int) {
        bodyTextView.setText(textRes)
    }

    fun setBodyHeight(height: Int) {
        bodyHeight = height
    }

    fun getBodyHeight() = bodyHeight

    fun setExpanded(expanded: Boolean) {
        isExpanded = expanded
        if (bodyHeight != -1) bodyTextView.height = if (isExpanded) bodyHeight else 0
        expandImageView.rotation = if (isExpanded) EXPANDED_ROTATION else COLLAPSED_ROTATION
    }

    fun getExpanded() = isExpanded

    fun setOnBodyHeightChangeListener(listener: BodyHeightChangeListener) {
        bodyHeightChangeListener = listener
    }

    fun setOnExpandedChangeListener(listener: ExpandedChangeListener) {
        expandedChangeListener = listener
    }

    interface BodyHeightChangeListener {
        fun onBodyHeightChanged(height: Int)
    }

    interface ExpandedChangeListener {
        fun onExpandedChanged(expanded: Boolean)
    }
}

@BindingAdapter("bodyHeightAttrChanged")
fun ExpandableTextView.setBodyHeightListener(listener: InverseBindingListener?) {
    if (listener != null) {
        setOnBodyHeightChangeListener(object : ExpandableTextView.BodyHeightChangeListener {
            override fun onBodyHeightChanged(height: Int) {
                listener.onChange()
            }
        })
    }
}

@BindingAdapter("bodyHeight")
fun ExpandableTextView.setBodyHeightValue(height: Int?) {
    if (height != null && getBodyHeight() != height) {
        setBodyHeight(height)
    }
}

@InverseBindingAdapter(attribute = "bodyHeight")
fun ExpandableTextView.getBodyHeightValue() = getBodyHeight()

@BindingAdapter("expandedAttrChanged")
fun ExpandableTextView.setExpandedListener(listener: InverseBindingListener?) {
    if (listener != null) {
        setOnExpandedChangeListener(object : ExpandableTextView.ExpandedChangeListener {
            override fun onExpandedChanged(expanded: Boolean) {
                listener.onChange()
            }
        })
    }
}

@BindingAdapter("expanded")
fun ExpandableTextView.setExpandedValue(expanded: Boolean?) {
    if (expanded != null && getExpanded() != expanded) {
        setExpanded(expanded)
    }
}

@InverseBindingAdapter(attribute = "expanded")
fun ExpandableTextView.getExpandedValue() = getExpanded()
