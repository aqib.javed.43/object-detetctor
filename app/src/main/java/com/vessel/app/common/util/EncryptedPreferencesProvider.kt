package com.vessel.app.common.util

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.chibatching.kotpref.PreferencesProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EncryptedPreferencesProvider @Inject constructor() : PreferencesProvider {
    override fun get(context: Context, name: String, mode: Int): SharedPreferences {
        val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
        return EncryptedSharedPreferences.create(
            name,
            masterKeyAlias,
            context,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }
}
