package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.SampleAssociationData
import com.vessel.app.common.net.payload.AssociateTestPayload
import retrofit2.http.Body
import retrofit2.http.POST

interface UploadSampleService {
    @POST("sample")
    suspend fun associateTestUuid(@Body payload: AssociateTestPayload): SampleAssociationData
}
