package com.vessel.app.onboarding.badhabitsselection

import android.os.Bundle
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_on_boarding_bad_habits_select.*

@AndroidEntryPoint
class OnBoardingBadHabitsSelectionFragment : BaseFragment<OnBoardingBadHabitsSelectionViewModel>() {
    override val viewModel: OnBoardingBadHabitsSelectionViewModel by viewModels()
    override val layoutResId = R.layout.fragment_on_boarding_bad_habits_select
    private var badHabitList: List<BadHabitSelect>? = null
    private var badHabitListFull: List<BadHabitSelect>? = null
    private val badHabitAdapter by lazy { BadHabitAdapter(viewModel) }
    private var tagSearchQuery: String = ""

    override fun onViewLoad(savedInstanceState: Bundle?) {
        val badHabitList = requireView().findViewById<RecyclerView>(R.id.badHabitList)
        badHabitList.layoutManager = GridLayoutManager(context, 3, RecyclerView.HORIZONTAL, false)
        badHabitList.adapter = badHabitAdapter
        setupObservers()
        setSearch()
    }

    fun setSearch() {
        tagsSearchView.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return onSearchQuery(query)
                }

                override fun onQueryTextChange(query: String?): Boolean {
                    return onSearchQuery(query)
                }
            })
    }

    fun setList(badHabitList: List<BadHabitSelect>?) {
        this.badHabitList = badHabitList
        badHabitAdapter.submitList(
            this.badHabitList?.filter { badHabit ->
                badHabit.title.contains(tagSearchQuery, false)
            }
        )
    }

    private fun onSearchQuery(query: String?): Boolean {
        tagSearchQuery = query ?: ""
        return if (query != null) {
            setList(badHabitList)
            if (query.isEmpty())
                badHabitAdapter.submitList(
                    badHabitListFull
                )
            true
        } else
            false
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(items) {
                badHabitList = it
                badHabitListFull = it
                badHabitAdapter.submitList(it)
            }
        }
    }
}
