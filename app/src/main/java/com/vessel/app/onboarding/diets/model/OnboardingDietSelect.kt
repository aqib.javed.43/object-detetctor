package com.vessel.app.onboarding.diets.model

import android.os.Parcelable
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OnboardingDietSelect(
    @StringRes val title: Int,
    var checked: Boolean,
    val isNone: Boolean,
) : Parcelable {
    @ColorRes
    @IgnoredOnParcel
    val background = if (checked)
        R.color.whiteAlpha70
    else
        R.color.whiteAlpha40
}
