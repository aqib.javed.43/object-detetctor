package com.vessel.app.newtesttimer.lightingtab

import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LightingTabFragment : BaseFragment<LightingTabViewModel>() {
    override val viewModel: LightingTabViewModel by viewModels()
    override val layoutResId = R.layout.fragment_lighting_tab
}
