package com.vessel.app.newtesttimer.surveytab

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SurveyTabFragment : BaseFragment<SurveyViewModel>() {
    override val viewModel: SurveyViewModel by viewModels()
    override val layoutResId = R.layout.fragment_survey_tab

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
