package com.vessel.app.nutritioncoach

import com.vessel.app.R
import javax.inject.Inject

class NutritionCoachState @Inject constructor() {
    val infoText = listOf<Int>(
        R.string.review_your_results,
        R.string.build_your_plan,
        R.string.answer_your_questions
    )
    operator fun invoke(block: NutritionCoachState.() -> Unit) = apply(block)
}
