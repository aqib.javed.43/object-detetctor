package com.vessel.app.stressrelief.ui

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.stressrelief.model.StressReliefViewItem
import com.vessel.app.util.extensions.afterMeasured

class StressReliefAdapter(private val handler: OnActionHandler, private val parentPosition: Int) :
    BaseAdapterWithDiffUtil<StressReliefViewItem>() {

    override fun getLayout(position: Int) = when (getItem(position)) {
        is StressReliefViewItem -> R.layout.item_stress_relief
        else -> throw IllegalStateException(
            "Unexpected SupplementAdapter type at position $position for item ${getItem(position)}"
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<StressReliefViewItem>, position: Int) {
        super.onBindViewHolder(holder, position)

        val item = getItem(position) as StressReliefViewItem

        holder.itemView.findViewById<SwipeLayout>(R.id.swipe)
            .addSwipeListener(object : SimpleSwipeListener() {
                override fun onStartOpen(layout: SwipeLayout?) {
                    super.onStartOpen(layout)
                    holder.itemView.findViewById<View>(R.id.dummyBackgroundView).isVisible = true
                }

                override fun onClose(layout: SwipeLayout?) {
                    super.onClose(layout)
                    holder.itemView.findViewById<View>(R.id.dummyBackgroundView).isVisible = false
                }
            })

        holder.itemView.findViewById<View>(R.id.deleteSwipe).setOnClickListener {
            handler.onItemDeleteClicked(item, it, position, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.reminder).setOnClickListener {
            handler.onAddReminderClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.background).setOnClickListener {
            handler.onPlanItemClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.editSwipe).setOnClickListener {
            handler.onItemEditClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.swipeView).afterMeasured {
            this.apply {
                updateLayoutParams {
                    val findViewById = holder.itemView.findViewById<View>(R.id.dummyBackground)
                    height = findViewById.height
                    y = findViewById.y
                }
            }
        }

        holder.itemView.findViewById<AppCompatImageView>(R.id.reminderIcon).apply {
            setOnClickListener {
                this@StressReliefAdapter.handler.onReminderNotificationClicked(
                    item,
                    it,
                    position,
                    parentPosition
                )
            }

            setImageResource(
                if (item.planData.notification_enabled == true) {
                    R.drawable.ic_reminders
                } else {
                    R.drawable.ic_reminders_off
                }

            )
        }

        holder.itemView.findViewById<ConstraintLayout>(R.id.groupBackground).apply {
            isVisible = item.isGrouped && item.groupCount > 1
            findViewById<TextView>(R.id.planItemsCount).apply {
                text = "+ ${item.groupCount - 1}"
                setOnClickListener {
                    this@StressReliefAdapter.handler.onGroupedItemsClicked(
                        item,
                        it,
                        this@StressReliefAdapter.parentPosition
                    )
                }
            }
        }
    }

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onPlanItemClicked(item: StressReliefViewItem, view: View, parentPosition: Int)
        fun onItemSelected(
            item: StressReliefViewItem,
            view: View,
            position: Int,
            parentPosition: Int
        )

        fun onAddReminderClicked(item: StressReliefViewItem, view: View, parentPosition: Int)
        fun onReminderNotificationClicked(
            item: StressReliefViewItem,
            view: View,
            position: Int,
            parentPosition: Int
        )

        fun onItemDeleteClicked(
            item: StressReliefViewItem,
            view: View,
            position: Int,
            parentPosition: Int
        )

        fun onItemEditClicked(item: StressReliefViewItem, view: View, parentPosition: Int)
        fun onGroupedItemsClicked(item: StressReliefViewItem, view: View, parentPosition: Int)
    }
}
