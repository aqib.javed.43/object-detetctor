package com.vessel.app.today.ui

import android.animation.Animator
import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.MutableLiveData
import com.airbnb.lottie.LottieAnimationView
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.plan.model.PlanItem
import com.vessel.app.util.extensions.afterMeasured
import com.vessel.app.views.ExpandableWithMoreTextView
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.android.synthetic.main.bottom_nav.view.*
import kotlinx.android.synthetic.main.item_activity.view.*
import java.util.*

class ActivityAdapter(
    private val handler: OnActionHandler,
    private val parentPosition: Int,
    private val hideLocker: Boolean
) :
    BaseAdapterWithDiffUtil<PlanItem>(
        areItemsTheSame = { oldItem, newItem -> oldItem.id == newItem.id },
        areContentsTheSame = { oldItem, newItem -> oldItem.id == newItem.id }
    ) {
    var onActivityUpdate: (() -> Unit)? = null

    override fun getLayout(position: Int) = when (getItem(position)) {
        is PlanItem -> R.layout.item_activity
        else -> throw IllegalStateException(
            "Unexpected WellnessAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<PlanItem>,
        @SuppressLint("RecyclerView") position: Int
    ) {
        super.onBindViewHolder(holder, position)

        val item = getItem(position) as PlanItem

        holder.itemView.findViewById<SwipeLayout>(R.id.swipe)
            .addSwipeListener(object : SimpleSwipeListener() {
                override fun onStartOpen(layout: SwipeLayout?) {
                    super.onStartOpen(layout)
                    holder.itemView.findViewById<View>(R.id.dummyBackgroundView).isVisible = true
                }

                override fun onClose(layout: SwipeLayout?) {
                    super.onClose(layout)
                    holder.itemView.findViewById<View>(R.id.dummyBackgroundView).isVisible = false
                }
            })
        holder.itemView.findViewById<View>(R.id.blurBackground).alpha =
            if (item.isCompleted) 1f else 0f
        holder.itemView.findViewById<View>(R.id.bottomBlurBackground).alpha =
            if (item.isCompleted) 0f else 1f
        holder.itemView.findViewById<View>(R.id.reminder).alpha = if (item.isCompleted) 0f else 1f
        holder.itemView.findViewById<View>(R.id.reminderIcon).alpha =
            if (item.isCompleted) 0f else 1f
        holder.itemView.findViewById<View>(R.id.reminderTime).alpha =
            if (item.isCompleted) 0f else 1f
        holder.itemView.findViewById<View>(R.id.reminderButton).alpha =
            if (item.isCompleted) 0f else 1f

        holder.itemView.findViewById<View>(R.id.deleteSwipe).setOnClickListener {
            handler.onItemDeleteClicked(item, it, position, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.reminderTime).setOnClickListener {
            handler.onAddReminderClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.background).setOnClickListener {
            handler.onPlanItemClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.editSwipe).setOnClickListener {
            handler.onItemEditClicked(item, it, parentPosition)
        }

        holder.itemView.findViewById<View>(R.id.swipeView).afterMeasured {
            this.apply {
                updateLayoutParams {
                    val findViewById = holder.itemView.findViewById<View>(R.id.dummyBackground)
                    height = findViewById.height
                    y = findViewById.y
                }
            }
        }
        val checkboxLocker = holder.itemView.findViewById<AppCompatImageView>(R.id.checkboxLocker)
        val checkbox = holder.itemView.findViewById<AppCompatCheckBox>(R.id.checkbox)
        val checkboxBg = holder.itemView.findViewById<View>(R.id.checkboxBg)
        val enableCheckBox = (item.isCompleted && item.isGrouped && item.groupCount <= 1).not()
        checkbox.apply {
            isEnabled = enableCheckBox
            isClickable = enableCheckBox
        }
        checkboxBg.apply {
            isEnabled = enableCheckBox
            isClickable = enableCheckBox
        }

        if (item.isGrouped && item.isAllTodayItemsChecked() && item.isCompleted.not()) {
            checkboxLocker.isVisible = true
        } else if (item.isGrouped.not() && hideLocker.not()) {
            checkboxLocker.isVisible = item.isCompleted.not()
        } else {
            checkboxLocker.isVisible = false
            checkbox.setOnClickListener {
                handlePlanCheckClick(holder, position, it)
            }
            checkboxBg.setOnClickListener {
                handlePlanCheckClick(holder, position, it)
            }
        }

        holder.itemView.findViewById<AppCompatImageView>(R.id.reminderIcon).apply {
            setOnClickListener {
                this@ActivityAdapter.handler.onReminderNotificationClicked(
                    item,
                    it,
                    position,
                    parentPosition
                )
            }

            setImageResource(
                if (item.currentPlan.notification_enabled == true) {
                    R.drawable.ic_reminders
                } else {
                    R.drawable.ic_reminders_off
                }

            )
        }
        holder.itemView.findViewById<View>(R.id.reminderButton).apply {
            setOnClickListener {
                this@ActivityAdapter.handler.onReminderNotificationClicked(
                    item,
                    it,
                    position,
                    parentPosition
                )
            }
        }

        holder.itemView.findViewById<View>(R.id.fadding).setOnClickListener {
            handler.onPlanItemClicked(item, it, parentPosition)
        }

        if (item.type == PlanType.TestPlan) {
            holder.itemView.findViewById<View>(R.id.fadding).isVisible = false
            holder.itemView.findViewById<View>(R.id.improvesData).isVisible = false
        } else {
            val goals = item.currentPlan.getPlanGoals()
            val improves = item.currentPlan.getPlanDescription()
            holder.itemView.findViewById<View>(R.id.fadding).isVisible =
                goals.trim().isNotEmpty() || improves.trim().isNotEmpty()
            holder.itemView.findViewById<ExpandableWithMoreTextView>(R.id.improvesData).apply {
                if (item.isCompleted.not()) {
                    collapsedLineCount = 2
                    text = improves
                    isVisible = improves.trim().isNotEmpty()
                }
            }
        }
        holder.itemView.findViewById<View>(R.id.checkbox).isVisible = true
        holder.itemView.findViewById<LottieAnimationView>(R.id.planSuccessAnimationView)
            .apply {
                setAnimation(R.raw.plan_success)
                scale = ANIMATION_VIEW_SCALE_FACTOR
                addAnimatorListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        this@ActivityAdapter.handler.onItemSelected(
                            getItem(position),
                            this@apply,
                            position,
                            parentPosition
                        )
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        setAnimation(R.raw.plan_success)
                        this@ActivityAdapter.handler.onAnimationFinished(
                            getItem(position),
                            this@apply,
                            position
                        )
                    }

                    override fun onAnimationCancel(animation: Animator) {
                    }

                    override fun onAnimationRepeat(animation: Animator) {
                    }
                })
            }

        holder.itemView.findViewById<ConstraintLayout>(R.id.groupBackground).apply {
            isVisible = item.isGrouped && item.groupCount > 1
            backgroundTintList =
                if (item.type == PlanType.SupplementPlan || item.type == PlanType.StressPlan) {
                    ContextCompat.getColorStateList(context, R.color.roseGoldBgAlpha50)
                } else {
                    ContextCompat.getColorStateList(context, R.color.pixiGreenAlpha50)
                }
            findViewById<TextView>(R.id.planItemsCount).apply {
                text = "+ ${item.groupCount - 1}"
                setOnClickListener {
                    this@ActivityAdapter.handler.onGroupedItemsClicked(
                        item,
                        it,
                        this@ActivityAdapter.parentPosition
                    )
                }
            }
        }
        holder.itemView.findViewById<View>(R.id.reactionLayout).setOnClickListener {
            handler.onReactItemClicked(
                getItem(position).copy(likeStatus = LikeStatus.NO_ACTION),
                it,
                parentPosition
            )
        }
        holder.itemView.findViewById<View>(R.id.likeButton).setOnClickListener {
            handler.onReactItemClicked(
                getItem(position).copy(likeStatus = LikeStatus.LIKE),
                it,
                parentPosition
            )
        }
        holder.itemView.findViewById<View>(R.id.dislikeButton).setOnClickListener {
            handler.onReactItemClicked(
                getItem(position).copy(likeStatus = LikeStatus.DISLIKE),
                it,
                parentPosition
            )
        }
        updateItem(isCollapse = item.isCompleted, holder, position)
    }

    private fun updateItem(isCollapse: Boolean, holder: BaseViewHolder<PlanItem>, position: Int) {
        Log.d("TAG", "updateItem: $isCollapse")
        val reactionLayout = holder.itemView.findViewById<View>(R.id.reactionLayout)
        val bottomBlur = holder.itemView.findViewById<View>(R.id.bottomBlurBackground)
        val faddingView = holder.itemView.findViewById<View>(R.id.fadding)
        reactionLayout.apply {
            visibility = if (!isCollapse || getItem(position).type == PlanType.TestPlan) View.GONE else View.VISIBLE
        }
        faddingView.apply {
            visibility = if (!isCollapse) View.VISIBLE else View.GONE
        }
        bottomBlur.apply {
            visibility = if (!isCollapse && getItem(position).type != PlanType.TestPlan) View.VISIBLE else View.GONE
        }
        holder.itemView.findViewById<LottieAnimationView>(R.id.planSuccessAnimationView)
            .apply {
                visibility = if (!isCollapse) View.VISIBLE else View.GONE
            }
        holder.itemView.dummyBackgroundView.apply {
            if (isCollapse) {
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                layoutParams.height = resources.getDimension(R.dimen.plan_item_height).toInt()
            }
        }
        holder.itemView.dummyBackground.apply {
            if (isCollapse) {
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                layoutParams.height = resources.getDimension(R.dimen.plan_item_height).toInt()
            }
        }
        holder.itemView.findViewById<ExpandableWithMoreTextView>(R.id.improvesData).apply {
            visibility = if (!isCollapse && getItem(position).type != PlanType.TestPlan) View.VISIBLE else View.GONE
        }
        holder.itemView.itemView.apply {
            if (isCollapse) {
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                layoutParams.height = resources.getDimension(R.dimen.plan_item_height).toInt()
            }
        }
        holder.itemView.swipe.apply {
            if (isCollapse) {
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                layoutParams.height = resources.getDimension(R.dimen.max_expert_name_width).toInt()
            }
        }
    }
    private fun handlePlanCheckClick(holder: BaseViewHolder<PlanItem>, position: Int, view: View) {
        val item = getItem(position)
        Log.d("TAG", "handlePlanCheckClick: ${item.isCompleted}")
        if ((item as PlanItem).isCompleted.not()) {
            holder.itemView.findViewById<LottieAnimationView>(R.id.planSuccessAnimationView)
                .playAnimation()
            holder.itemView.findViewById<LottieAnimationView>(R.id.planAddingAnimationView).apply {
                isVisible = true
                playAnimation()
            }
            holder.itemView.findViewById<View>(R.id.reminder).animate().alpha(0f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderButton).animate().alpha(0f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderTime).animate().alpha(0f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderIcon).animate().alpha(0f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.bottomBlurBackground).animate()
                .alpha(0f).duration = ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.blurBackground).animate().alpha(1f).duration =
                ANIMATION_DURATION
        } else {
            holder.itemView.findViewById<View>(R.id.reminder).animate().alpha(1f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderButton).animate().alpha(1f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderTime).animate().alpha(1f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.reminderIcon).animate().alpha(1f).duration =
                ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.bottomBlurBackground).animate()
                .alpha(1f).duration = ANIMATION_DURATION
            holder.itemView.findViewById<View>(R.id.blurBackground).animate().apply {
                setListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        this@ActivityAdapter.handler.onItemSelected(
                            getItem(position),
                            view,
                            position,
                            parentPosition
                        )
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        this@ActivityAdapter.handler.onAnimationFinished(
                            getItem(position),
                            view,
                            position
                        )
                    }

                    override fun onAnimationCancel(animation: Animator) {
                    }

                    override fun onAnimationRepeat(animation: Animator) {
                    }
                })
                alpha(0f).duration = ANIMATION_DURATION
            }
        }
    }

    override fun getHandler(position: Int): Any? = handler

    interface OnActionHandler {
        fun onPlanItemClicked(item: Any, view: View, parentPosition: Int)
        fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int)
        fun onAddReminderClicked(item: Any, view: View, parentPosition: Int)
        fun onReminderNotificationClicked(item: Any, view: View, position: Int, parentPosition: Int)
        fun onItemDeleteClicked(item: Any, view: View, position: Int, parentPosition: Int)
        fun onItemEditClicked(item: Any, view: View, parentPosition: Int)
        fun onGroupedItemsClicked(item: Any, view: View, parentPosition: Int)
        fun onAnimationFinished(item: Any, view: View, parentPosition: Int)
        val showPlanNotification: MutableLiveData<Boolean>
        val planNotificationCount: MutableLiveData<Int>
        fun onWellnessActivityInformationClicked()
        fun onReactItemClicked(item: Any, view: View, parentPosition: Int)
        fun onProgramInformationClicked()
        fun onGetNextPlanClicked(item: PlanItem?, date: Date)
        fun onTakeATest()
        fun onProgramActivityCompleted(item: PlanItem?, view: View)
    }

    companion object {
        private const val ANIMATION_VIEW_SCALE_FACTOR = 0.4f
        private const val ANIMATION_DURATION: Long = 500
    }
}
