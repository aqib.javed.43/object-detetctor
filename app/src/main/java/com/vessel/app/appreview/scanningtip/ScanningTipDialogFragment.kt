package com.vessel.app.appreview.scanningtip

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.net.Uri
import android.view.Gravity
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_scanning_feedback_dialog.*
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity

@AndroidEntryPoint
class ScanningTipDialogFragment : BaseDialogFragment<ScanningTipDialogViewModel>() {
    override val viewModel: ScanningTipDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_scanning_feedback_dialog
    private val exoPlayer by lazy { SimpleExoPlayer.Builder(requireContext()).build() }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                0
            )
            setLayout(width, height)
            setGravity(Gravity.BOTTOM)
            setBackgroundDrawable(inset)
        }
        isCancelable = false

        observe(viewModel.dismissDialog) { dismiss() }
        observe(viewModel.navigateTo) {
            dismiss()
            findNavController().navigate(it)
        }
        viewModel.state.apply {
            observe(openMessaging) {
                MessagingActivity.builder()
                    .withToolbarTitle(getString(R.string.customer_support))
                    .withEngines(ChatEngine.engine())
                    .show(requireContext(), viewModel.getChatConfiguration())
            }
            observe(videoUrl) {
                setupVideo(it)
            }
            observe(isPlayingVideo) { isPlaying ->
                btnHowto.isVisible = !isPlaying
                btnPlay.isVisible = !isPlaying
                imgHowto.isVisible = !isPlaying
                video.isVisible = isPlaying
            }
            observe(openTips) {
                openBrowser(it)
            }
            observe(onCalling) {
                openPhoneCall(it)
            }
        }
    }

    private fun openPhoneCall(tel: String) {
        val dialIntent = Intent(Intent.ACTION_DIAL)
        dialIntent.data = Uri.parse("tel:$tel")
        startActivity(dialIntent)
    }

    private fun setupVideo(url: String) {
        requireView().findViewById<PlayerView>(R.id.video).player = exoPlayer
        exoPlayer.run {
            val defaultSourceFactory =
                DefaultHttpDataSourceFactory()
            val mediaItem = MediaItem.fromUri(Uri.parse(url))
            val assetVideoSource = ProgressiveMediaSource.Factory(defaultSourceFactory)
                .createMediaSource(mediaItem)
            addMediaSource(assetVideoSource)
            prepare()
        }
    }

    override fun onResume() {
        super.onResume()
        exoPlayer.playWhenReady = false
    }

    override fun onPause() {
        super.onPause()
        exoPlayer.playWhenReady = false
    }

    override fun onStop() {
        super.onStop()
        exoPlayer.playWhenReady = false
        exoPlayer.stop()
    }
    fun openBrowser(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }
}
