package com.vessel.app.util.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

fun <T> MutableLiveData<T>.liveData(): LiveData<T> = Transformations.map(this) { it }

fun <T, R> LiveData<T>.map(block: (T) -> R): LiveData<R> = Transformations.map(this) {
    block(it)
}

fun <T, Y, Z> zip(first: LiveData<T>, second: LiveData<Y>, zipFunction: (T, Y) -> Z): LiveData<Z> {
    val finalLiveData: MediatorLiveData<Z> = MediatorLiveData()

    var firstEmitted = false
    var firstValue: T? = null

    var secondEmitted = false
    var secondValue: Y? = null
    finalLiveData.addSource(first) { value ->
        firstEmitted = true
        firstValue = value
        if (firstEmitted && secondEmitted) {
            finalLiveData.value = zipFunction(firstValue!!, secondValue!!)
            firstEmitted = false
            secondEmitted = false
        }
    }
    finalLiveData.addSource(second) { value ->
        secondEmitted = true
        secondValue = value
        if (firstEmitted && secondEmitted) {
            finalLiveData.value = zipFunction(firstValue!!, secondValue!!)
            firstEmitted = false
            secondEmitted = false
        }
    }
    return finalLiveData
}

fun <T, Y, Z> combineLatest(
    first: LiveData<T>,
    second: LiveData<Y>,
    zipFunction: (T, Y) -> Z
): LiveData<Z> {
    val finalLiveData: MediatorLiveData<Z> = MediatorLiveData()

    var firstValue: T? = null
    var secondValue: Y? = null

    finalLiveData.addSource(first) { value ->
        firstValue = value
        if (firstValue != null && secondValue != null) {
            finalLiveData.value = zipFunction(firstValue!!, secondValue!!)
        }
    }

    finalLiveData.addSource(second) { value ->
        secondValue = value
        if (firstValue != null && secondValue != null) {
            finalLiveData.value = zipFunction(firstValue!!, secondValue!!)
        }
    }

    return finalLiveData
}

fun <T> LiveData<T>.distinctUntilChanged(): LiveData<T> = Transformations.distinctUntilChanged(this)
