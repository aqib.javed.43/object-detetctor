package com.vessel.app.more

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.TrailLockItem
import com.vessel.app.more.model.MoreModel
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class MoreState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val openMessaging = LiveEvent<Any>()
    val version = MutableLiveData("")
    val moreItems = MutableLiveData<List<MoreModel>>()
    val isTrialLocked = MutableLiveData<Boolean>()
    val trialLockItem = MutableLiveData<TrailLockItem>()
    operator fun invoke(block: MoreState.() -> Unit) = apply(block)
}
