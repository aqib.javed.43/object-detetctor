package com.vessel.app.cardchoices.gifted

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GiftedCardChoiceFragment : BaseFragment<GiftedCardChoiceViewModel>() {
    override val viewModel: GiftedCardChoiceViewModel by viewModels()
    override val layoutResId = R.layout.fragment_gifted_card_choice

    override fun onViewLoad(savedInstanceState: Bundle?) {
        requireView().findViewById<TextView>(R.id.subtitle).movementMethod = LinkMovementMethod.getInstance()
        observe(viewModel.navigateTo) {
            findNavController().navigate(it)
        }
    }
}
