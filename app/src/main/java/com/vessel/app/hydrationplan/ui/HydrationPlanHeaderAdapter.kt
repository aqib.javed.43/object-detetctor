package com.vessel.app.hydrationplan.ui

import android.widget.TextView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.hydrationplan.model.HydrationPlanHeaderModel

class HydrationPlanHeaderAdapter(
    val handler: HydrationPlanHeaderHandler
) : BaseAdapterWithDiffUtil<HydrationPlanHeaderModel>() {

    override fun getLayout(position: Int) = R.layout.item_hydration_plan_header

    override fun getHandler(position: Int): Any? = handler

    override fun onBindViewHolder(holder: BaseViewHolder<HydrationPlanHeaderModel>, position: Int) {
        super.onBindViewHolder(holder, position)

        val item = getItem(position) as HydrationPlanHeaderModel
        holder.itemView.findViewById<TextView>(R.id.hydrationPlanHint).text = item.title
    }

    interface HydrationPlanHeaderHandler {
        fun onHydrationActionButtonClick()
        fun onCheckHydrationActionButtonClick()
    }
}
