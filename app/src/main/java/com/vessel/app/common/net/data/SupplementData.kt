package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class SupplementData(
    val id: Int,
    val warning_description: String?,
    val description: String?,
    val dosage_unit: String,
    val name: String,
    val dosage: Double?,
    val volume: Double?,
    val price: Double?,
    val goals_supplement: List<SupplementAssociatedGoalData>? = arrayListOf(),
    val is_multivitamin: Boolean?,
    val supplement_association: List<SupplementAssociationData>?
) : Parcelable
