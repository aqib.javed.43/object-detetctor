package com.vessel.app.home

import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.Navigation
import com.vessel.app.MainApplication
import com.vessel.app.R
import com.vessel.app.base.BaseActivity
import com.vessel.app.common.ext.showMessageSnackbar
import com.vessel.app.util.ScreenshotDetectionDelegate
import com.vessel.app.util.extensions.applyFullscreenFlags
import dagger.hilt.android.AndroidEntryPoint
import io.cobrowse.CobrowseIO

@AndroidEntryPoint
class HomeActivity : BaseActivity<HomeViewModel>() {
    override val viewModel: HomeViewModel by viewModels()
    override val layoutResId = R.layout.activity_home
    private var currentScreen = "HomeScreen"
    private var screenshotDetectionDelegate: ScreenshotDetectionDelegate? = null
    override fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        applyFullscreenFlags()
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observeData()
        registerForPushNotifications()
        setupCoBrowse()
        Navigation.findNavController(this, R.id.fragmentContainerView)
            .addOnDestinationChangedListener { _, des, _ ->
                if (des.label != null)currentScreen = des.label.toString()
                viewModel.onNewDestinationReached()
            }
        registerScreenShotDetection()
    }

    private fun registerScreenShotDetection() {
        screenshotDetectionDelegate = ScreenshotDetectionDelegate(
            contentResolver,
            object : ScreenshotDetectionDelegate.Listener {
                override fun onScreenShotTaken() {
                    viewModel.trackingScreenShot(currentScreen)
                }
            }
        )
    }
    override fun onResume() {
        screenshotDetectionDelegate?.register()
        super.onResume()
    }

    override fun onPause() {
        screenshotDetectionDelegate?.unregister()
        super.onPause()
    }
    private fun registerForPushNotifications() {
        if ((application as MainApplication).isConnectionEnabled())
            viewModel.registerForPushNotifications()
    }

    private fun observeData() {
        viewModel.state.apply {
            observe(navigateTo) {
                Navigation.findNavController(this@HomeActivity, R.id.fragmentContainerView)
                    .navigate(it)
            }
            observe(showFirstReminderHint) {
                showMessageSnackbar(getString(R.string.added_to_plan))
                viewModel.clearFirstReminderState()
            }
            observe(showUpdateReminderHint) {
                showMessageSnackbar(getString(R.string.reminders_updated))
            }
        }
    }

    fun setupCoBrowse() {
        observe(viewModel.state.setupCoBrowseData) {
            CobrowseIO.instance().start(this)
            CobrowseIO.instance().customData(it)
        }
    }

    override fun onDetachedFromWindow() {
        viewModel.trackingLatestScreen(currentScreen)
        super.onDetachedFromWindow()
    }
}
