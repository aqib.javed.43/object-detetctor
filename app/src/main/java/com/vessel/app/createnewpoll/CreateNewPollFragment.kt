package com.vessel.app.createnewpoll

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_create_new_poll.*
import kotlinx.android.synthetic.main.fragment_create_new_poll.option1
import kotlinx.android.synthetic.main.fragment_create_new_poll.option2
import kotlinx.android.synthetic.main.fragment_create_new_poll.option3

@AndroidEntryPoint
class CreateNewPollFragment : BaseFragment<CreateNewPollViewModel>() {
    override val viewModel: CreateNewPollViewModel by viewModels()
    override val layoutResId: Int = R.layout.fragment_create_new_poll
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        btnContinue.setOnClickListener {
            val questions: ArrayList<String> = arrayListOf()
            if (!option1.text.isNullOrEmpty())questions.add(option1.text.toString())
            if (!option2.text.isNullOrEmpty())questions.add(option2.text.toString())
            if (!option3.text.isNullOrEmpty())questions.add(option3.text.toString())
            viewModel.createPoll(postContent.text.toString(), questions)
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
