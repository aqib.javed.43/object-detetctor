package com.vessel.app.supplementsbucket.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SupplementAssociation(
    val id: Int,
    val type: String
) : Parcelable
