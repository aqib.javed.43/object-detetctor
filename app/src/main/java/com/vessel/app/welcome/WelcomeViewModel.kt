package com.vessel.app.welcome

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.ResourceRepository

class WelcomeViewModel @ViewModelInject constructor(
    val state: WelcomeState,
    resourceRepository: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository) {
    var logoPressCounter = 0

    init {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SIGN_UP_WALL_SHOWN)
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun onCreateAccountClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CREATE_AN_ACCOUNT_TAPPED)
                .withFirebase()
                .withAmplitude()
                .create()
        )
        view.findNavController()
            .navigate(R.id.action_welcomeFragment_to_emailCheckFragment)
    }

    fun onSignInClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SIGN_IN_TAPPED)
                .withFirebase()
                .withAmplitude()
                .create()
        )
        view.findNavController()
            .navigate(R.id.action_welcomeFragment_to_signInFragment)
    }

    fun onLogoClicked() {
        if (!preferencesRepository.inTestingMode) {
            logoPressCounter += 1
            if (logoPressCounter >= 15) {
                preferencesRepository.setTestingMode(true)
                state.showBecomeTester.call()
            }
        }
    }
}
