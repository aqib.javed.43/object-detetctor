package com.vessel.app.util

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager

object WindowUtils {
    fun getDisplayMetrics(context: Context) =
        (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).run {
            DisplayMetrics().also { defaultDisplay.getMetrics(it) }
        }
}
