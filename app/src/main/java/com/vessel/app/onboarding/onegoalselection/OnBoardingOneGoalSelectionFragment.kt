package com.vessel.app.onboarding.onegoalselection

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.goalsselection.ui.GoalSelectAdapter
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_goal.*

@AndroidEntryPoint
class OnBoardingOneGoalSelectionFragment : BaseFragment<OnBoardingOneGoalSelectionViewModel>() {
    override val viewModel: OnBoardingOneGoalSelectionViewModel by viewModels()
    override val layoutResId = R.layout.fragment_on_boarding_one_goal_selection

    val goalSelectAdapter by lazy { GoalSelectAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        goalList.layoutManager = GridLayoutManager(context, Constants.SPAN_COUNT_GOAL_GRID)
        goalList.adapter = goalSelectAdapter
        goalList.itemAnimator = null
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(items) {
                goalSelectAdapter.submitList(it)
            }
        }
    }
}
