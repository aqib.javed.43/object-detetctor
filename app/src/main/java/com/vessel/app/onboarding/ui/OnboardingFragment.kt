package com.vessel.app.onboarding.ui

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.model.Contact
import com.vessel.app.onboarding.OnboardingViewModel
import com.vessel.app.onboarding.height.HeightDialogFragment.Companion.HEIGHT_KEY
import com.vessel.app.onboarding.weight.WeightDialogFragment.Companion.WEIGHT_KEY
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_onboarding.onboardingViewPager

@AndroidEntryPoint
class OnboardingFragment : BaseFragment<OnboardingViewModel>() {
    override val viewModel: OnboardingViewModel by viewModels()
    override val layoutResId = R.layout.fragment_onboarding

    private val onPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            viewModel.onPageChanged(position)
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        onboardingViewPager.apply {
            adapter = OnboardingAdapter(viewModel.state.pages, viewLifecycleOwner)
            isUserInputEnabled = false
            registerOnPageChangeCallback(onPageChangeCallback)
        }

        observeValues()
        setupBackPressedDispatcher()
    }

    private fun observeValues() {
        viewModel.state {
            observe(goToNextPage) { onboardingViewPager.currentItem += 1 }
            observe(goToPreviousPage) { onboardingViewPager.currentItem -= 1 }
            observe(closeEvent) { activity?.finish() }
            observe(navigateTo) { findNavController().navigate(it) }
        }

        findNavController().currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData<Contact.Height>(HEIGHT_KEY)
            ?.observe(viewLifecycleOwner) { value ->
                viewModel.onHeightSet(value)
            }

        findNavController().currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData<Float>(WEIGHT_KEY)
            ?.observe(viewLifecycleOwner) { value ->
                viewModel.onWeightSet(value)
            }
    }

    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.let { viewModel.onBackButtonClicked(it) }
        }
    }

    override fun onDestroyView() {
        onboardingViewPager.unregisterOnPageChangeCallback(onPageChangeCallback)
        super.onDestroyView()
    }
}
