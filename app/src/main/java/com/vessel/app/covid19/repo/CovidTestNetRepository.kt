package com.vessel.app.covid19.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.repo.BaseNetworkRepository
import com.vessel.app.covid19.net.mapper.CovidTestNetMapper
import com.vessel.app.covid19.net.service.CovidTestService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CovidTestNetRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val covidTestService: CovidTestService,
    private val covidTestMapper: CovidTestNetMapper
) : BaseNetworkRepository(moshi, loggingManager) {

    suspend fun getCovidTests() = execute(covidTestMapper::map) {
        covidTestService.getCovidTests()
    }

    suspend fun isTestProcessed(testId: String) = executeWithErrorMessage {
        covidTestService.isTestProcessed(testId)
    }
}
