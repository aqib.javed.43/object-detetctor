package com.vessel.app.triallockerpopup

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.model.TrailLockItem
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.homescreen.HomeScreenFragmentDirections
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class TrailLockerPopUpViewModel @ViewModelInject constructor(
    val state: TrialLockerPopUpState,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    @Assisted savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()

    init {
        val trailLockItem = TrailLockItem()
        trailLockItem.isDisPlayIcon = true
        trailLockItem.title = getResString(R.string.take_a_test_locked)
        trailLockItem.message = getResString(R.string.membership_lock_description)
        trailLockItem.onDismiss = {
            dismissDialog.call()
        }
        trailLockItem.onActiveMember = {
            state.navigateTo.value = HomeScreenFragmentDirections.globalActionToWeb(BuildConfig.BUY_TEST_URL)
        }
        state.trialLockItem.value = trailLockItem
        state.isTrialLocked.value = true
    }
}
