Normally, healthy urinary pH is slightly acidic, hovering around 6. However, it can range between 4.5-8, depending on the acid/ base balance in the body. The kidneys, along with the lungs, are involved in managing the body’s pH balance. This range fluctuates depending on many variables, including hydration status and consuming highly acidic foods (meats, alcohol, sugar, or dairy) vs. basic/alkaline foods (vegetables, sea salt, and citrus). Other imbalances, including lung disease, diabetic ketoacidosis, urinary tract infections, or dehydration, may lead to unhealthy changes in blood and urine pH.

The most common cause of lower (acidic) pH is eating a standard American diet of processed foods, alcohol, sugar, and animal products with few vegetables. It can also be a result of metabolic or respiratory disease.

Foods that increase urinary pH and reduce acidity are those that are high in calcium, magnesium, and potassium like green vegetables, fruits, nuts, lentils, citrus, and more vegetables. Some bacterial infections can also increase alkalinity and urine pH.

An important concept to remember: A food’s acidic or alkaline effect on the body is not necessarily related to it’s predigested pH. Scientists grade the acidity of foods based on their potential renal acid load (PRAL). The PRAL of a food is the amount of acid that is expected to reach the kidneys once it has been metabolized. Foods such as meats and grains are high in acidic nutrients, such as protein and phosphorous, so they increase the amount of acid the kidneys have to filter out, and consequently, they have a positive PRAL score. Alkaline foods, such as fruits and vegetables, on the other hand, are high in alkaline nutrients like magnesium and calcium, so they reduce the amount of acid the kidneys filter out and have a lower PRAL score. Now let us apply this to citrus, lemon juice has an acidic predigested pH, but once it is digested, it produces alkaline byproducts and therefore alkalizes the body.

-   **references:**

-   https://www.webmd.com/diet/a-z/alkaline-diets

-   https://draxe.com/alkaline-diet/

-   https://www.healthline.com/nutrition/the-alkaline-diet-myth
