package com.vessel.app.homescreen

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class HomeScreenState @Inject constructor() {
    val showPlanNotification = LiveEvent<Boolean>()
    val showChatNotification = LiveEvent<Boolean>()
    val planNotificationCount = MutableLiveData<Int>()
    val showPlanRecommendation = LiveEvent<Unit>()
    val showPlanComingDialog = LiveEvent<Int>()
    val tabNavigate = LiveEvent<MainPagerFragmentTab>()
    val navigateTo = LiveEvent<NavDirections>()
    val resultsSelected = MutableLiveData(false)
    val planSelected = MutableLiveData(true)
    val moreSelected = MutableLiveData(false)
    val chatSelected = MutableLiveData(false)
    val showIntroForCheckIn = LiveEvent<Unit>()
    val showMonthlyCheckIn = LiveEvent<Unit>()
    val showWeeklyCheckIn = LiveEvent<Unit>()
    val navigateToPopupLock = LiveEvent<Unit>()
    val showEndTrial = LiveEvent<Unit>()
    var showPlanHint = false
    operator fun invoke(block: HomeScreenState.() -> Unit) = apply(block)
}
