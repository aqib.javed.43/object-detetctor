package com.vessel.app.onboarding.problemselection.model

import android.os.Parcelable
import androidx.annotation.ColorRes
import com.vessel.app.R
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProblemSelect(
    val id: Int,
    val name: String,
    var checked: Boolean,
) : Parcelable {
    @ColorRes
    @IgnoredOnParcel
    val background = if (checked)
        R.color.whiteAlpha70
    else
        R.color.whiteAlpha40
}
