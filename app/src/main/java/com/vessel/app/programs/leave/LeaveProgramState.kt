package com.vessel.app.programs.leave

import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class LeaveProgramState @Inject constructor(val program: Program) {

    val dismissDialog = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: LeaveProgramState.() -> Unit) = apply(block)
}
