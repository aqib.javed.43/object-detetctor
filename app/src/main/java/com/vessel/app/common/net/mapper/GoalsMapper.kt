package com.vessel.app.common.net.mapper

import com.vessel.app.common.net.data.GoalData
import com.vessel.app.onboarding.goals.model.GoalSample
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GoalsMapper @Inject constructor() {
    fun map(data: List<GoalData>) = data.map { map(it) }

    fun mapToGoalData(data: List<GoalSample>) = data.map { map(it) }

    fun map(data: GoalData) = GoalSample(
        id = data.id,
        name = data.name
    )

    fun map(data: GoalSample) = GoalData(
        id = data.id,
        name = data.name
    )
}
