package com.vessel.app.supplementsbucket

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.BuildConfig
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.SupplementsManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.supplement.ViewModel
import com.vessel.app.supplementsbucket.model.GoalViewItem
import com.vessel.app.supplementsbucket.model.Supplement
import com.vessel.app.supplementsbucket.model.SupplementHeader
import com.vessel.app.supplementsbucket.ui.OnBucketCheckoutActionHandler
import com.vessel.app.supplementsbucket.ui.SupplementBucketAdapter
import com.vessel.app.supplementsbucket.ui.SupplementsBucketFragmentDirections
import com.vessel.app.supplementsbucket.ui.descriptionpopup.OnDescriptionActionHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDigits
import com.vessel.app.vesselfuelcheckout.model.VesselFuelItem
import kotlinx.coroutines.launch
import kotlin.math.min

class SupplementsBucketViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val supplementsManager: SupplementsManager,
    private val preferencesRepository: PreferencesRepository,
    private val goalsRepository: GoalsRepository,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    SupplementBucketAdapter.OnActionHandler,
    OnBucketCheckoutActionHandler,
    OnDescriptionActionHandler {

    val descriptionPopupBottomMargin =
        resourceProvider.getDimension(R.dimen.supplement_description_bottom_margin).toInt()

    private var hasActiveMembership: Boolean = false
    val state = SupplementsBucketState(savedStateHandle["supplements"]!!)
    private val _addedSupplements = mutableListOf<Supplement>()
    private var _supplements = listOf<Supplement>()
    private var _staticSupplements = listOf<Supplement>()
    private var _totalPrice = SUPPLEMENT_PRICE_BASE_RATE
    private val sortedGoals =
        goalsRepository.getSortedGoals().map { GoalViewItem(it.id, it.title, it.formulaIcon) }
    private val multiVitaminGoal = GoalViewItem(
        title = R.string.multivitamin,
        formulaIcon = R.drawable.ic_formula_goal_vitamin
    )

    init {
        showLoading()
        checkVesselFuelMemberships()
        viewModelScope.launch {
            supplementsManager.getSupplements()
                .onSuccess { supplements ->
                    val calculatedStaticSupplements = mutableListOf<Supplement>()
                    supplements.onEach { supplement ->
                        state.recommendedSupplements.onEach { recommendedSupplement ->
                            if (recommendedSupplement.supplementId == supplement.id) {
                                supplement.isAdded = true
                                supplement.price =
                                    0.0 // override the recommended supplement price to 0.0 and put them as free items
                                supplement.dosage = recommendedSupplement.quantity.toDouble()
                                supplement.dosageUnit =
                                    recommendedSupplement.consumptionUnit.orEmpty()
                                calculatedStaticSupplements.add(supplement)
                            }
                        }
                    }
                    _staticSupplements = calculatedStaticSupplements
                    _supplements = supplements.minus(_staticSupplements)
                    state.goalBasedSupplement.value = groupSupplementsByGoal(_supplements)
                    state.supplementHeader.value = SupplementHeader(_staticSupplements, _totalPrice)
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun checkVesselFuelMemberships() {
        viewModelScope.launch {
            supplementsManager.getMemberships()
                .onSuccess { memberships ->
                    val membership = memberships.firstOrNull {
                        it.productTitle.equals(
                            BuildPlanManager.TEST_VESSEL_FUEL_API_NAME,
                            true
                        ).not() && it.status.equals(ViewModel.STATUS_ACTIVE, true)
                    }
                    hasActiveMembership = membership != null
                }
                .onError {
                    hasActiveMembership = false
                }
        }
    }

    private fun groupSupplementsByGoal(supplements: List<Supplement>): Map<GoalViewItem, List<Supplement>> {
        val multiVitamins = supplements.filter { it.isMultivitamin }
        val supplementsWithoutMultivitamins = supplements.minus(multiVitamins)
        return sortedGoals.associateWith { goal ->
            supplementsWithoutMultivitamins.filter { supplement -> supplement.supplementAssociatedGoals.any { it.id == goal.id } }
        }.toMutableMap().apply {
            put(multiVitaminGoal, multiVitamins)
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onAddItClick(item: Supplement) {
        item.supplementAssociation?.forEach { supplementA ->
            _addedSupplements.firstOrNull { supplementB -> supplementA.id == supplementB.id }?.let {
                state.navigateTo.value =
                    SupplementsBucketFragmentDirections.actionSupplementsBucketFragmentToIncompatibityDialogFragment(
                        item.name,
                        it.name
                    )
                return
            }
        }
        val staticItem = _staticSupplements.find { it.id == item.id }
        if (staticItem != null) {
            logAddRemoveSupplementEvent(item.isAdded, item.id.toString(), item.name, true)
            if (item.isAdded.not()) {
                val totalVolume = item.volume + _staticSupplements.filter { it.isAdded }
                    .sumByDouble { it.volume } + _addedSupplements.sumByDouble { it.volume }
                if (totalVolume >= MAX_ALLOWED_SUPPLEMENT_VOLUME) {
                    state.navigateTo.value =
                        SupplementsBucketFragmentDirections.actionSupplementsBucketFragmentToRunOutOfRoomDialogFragment()
                    return
                }
            }
            _staticSupplements = _staticSupplements.map { supplementItem ->
                if (staticItem.id == supplementItem.id) {
                    supplementItem.copy(isAdded = !item.isAdded)
                } else {
                    supplementItem
                }
            }
        } else {
            logAddRemoveSupplementEvent(item.isAdded, item.id.toString(), item.name, false)
            if (item.isAdded) {
                _addedSupplements.removeIf { it.id == item.id }
                _supplements.find { it.id == item.id }?.isAdded = false
            } else {
                val totalVolume = item.volume + _staticSupplements.filter { it.isAdded }
                    .sumByDouble { it.volume } + _addedSupplements.sumByDouble { it.volume }
                if (totalVolume >= MAX_ALLOWED_SUPPLEMENT_VOLUME) {
                    state.navigateTo.value =
                        SupplementsBucketFragmentDirections.actionSupplementsBucketFragmentToRunOutOfRoomDialogFragment()
                    return
                } else {
                    item.isAdded = true
                    _supplements.find { it.id == item.id }?.isAdded = true
                    _addedSupplements.add(item)
                }
            }
        }
        _supplements.forEach {
            it.canAddForFree = _addedSupplements.size < MAX_FREE_SUPPLEMENTS
        }
        _totalPrice = SUPPLEMENT_PRICE_BASE_RATE

        if (_addedSupplements.size > MAX_FREE_SUPPLEMENTS) {
            _totalPrice += _addedSupplements.subList(MAX_FREE_SUPPLEMENTS, _addedSupplements.size)
                .sumByDouble { it.price }
        }
        val headerList = mutableListOf<Supplement>().apply {
            addAll(_staticSupplements)
            addAll(_addedSupplements)
        }
        state.goalBasedSupplement.value = groupSupplementsByGoal(_supplements)
        state.supplementHeader.value = SupplementHeader(headerList, _totalPrice)
    }

    private fun logAddRemoveSupplementEvent(
        logRemove: Boolean,
        id: String,
        name: String,
        isRecommended: Boolean
    ) {
        trackingManager.log(
            TrackedEvent.Builder(if (logRemove) TrackingConstants.SUPPLEMENT_REMOVE else TrackingConstants.SUPPLEMENT_ADD)
                .addProperty(TrackingConstants.KEY_NAME, name)
                .addProperty(TrackingConstants.KEY_ID, id)
                .addProperty(TrackingConstants.KEY_IS_RECOMMENDED, isRecommended.toString())
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    override fun onInfoIconClicked(view: View, item: Supplement) {
        val positionOnScreen = IntArray(2)
        view.getLocationInWindow(positionOnScreen)
        state.descriptionPopupText.value = item.description
        state.descriptionPopupPosition.value = positionOnScreen[1] + descriptionPopupBottomMargin
    }

    override fun onCheckoutClicked(view: View) {
        val addedSupplements = mutableListOf<Supplement>().apply { addAll(_addedSupplements) }
        for (i in 0 until min(addedSupplements.size, MAX_FREE_SUPPLEMENTS)) {
            addedSupplements[i].price = 0.0
        }
        val payList = mutableListOf<Supplement>().apply {
            addAll(_staticSupplements.filter { it.isAdded })
            addAll(addedSupplements)
        }
        sendPayList(payList)
    }

    private fun sendPayList(payList: List<Supplement>) {
        val encodeSupplements = supplementsManager.encodeSupplements(payList)
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SUPPLEMENT_CHECKOUT_STARTED)
                .addProperty(TrackingConstants.KEY_DATA, encodeSupplements)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )

        val storeUrl =
            BuildConfig.STORE_URL + SupplementsManager.VESSEL_FUEL_PREFIX + encodeSupplements
        val pricePerDay = _totalPrice / Constants.DAYS_IN_MONTH
        val dailyPrice = getResString(
            R.string.supplement_price_breakdown,
            pricePerDay.formatDigits(2)
        )

        if (hasActiveMembership) {
            state.navigateToCheckout.value = VesselFuelItem(
                _totalPrice,
                dailyPrice,
                storeUrl,
                payList
            )
        } else {
            state.navigateToWeb.value = storeUrl
        }
    }

    override fun onCloseDialogClicked(view: View) {
        state.descriptionPopupCloseAction.call()
    }

    companion object {
        const val MAX_FREE_SUPPLEMENTS = 2
        const val MAX_ALLOWED_SUPPLEMENT_VOLUME = 3.9
        const val SUPPLEMENT_PRICE_BASE_RATE = 50.0
    }
}
