package com.vessel.app.ownplanitem.steptwo

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TipManager
import com.vessel.app.common.model.Goal
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.FirebaseRemoteConfiguration
import com.vessel.app.wellness.model.Tip
import kotlinx.coroutines.launch

class CreateOwnPlanItemStepTwoViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val resourceProvider: ResourceRepository,
    private val tipManager: TipManager,
    private val tipMapper: TipMapper,
    val remoteConfiguration: FirebaseRemoteConfiguration
) : BaseViewModel(resourceProvider) {

    val state = CreateOwnPlanItemStepTwoState(
        savedStateHandle.get<String>("planItemTitle")!!,
        savedStateHandle.get<String>("planItemFrequency")!!,
        remoteConfiguration.getString(Constants.REMOTE_CONFIG_CUSTOM_TIP_IMAGE_URLS)
    )

    fun onCloseClicked() {
        navigateBack()
    }

    fun onContinueClicked() {
        showLoading()
        viewModelScope.launch {
            tipManager.addTip(
                title = state.planItemTitle,
                frequency = state.planItemFrequency,
                imageUrl = state.currentSelectedImageUrl,
                mainGoalId = Goal.Wellness.id
            )
                .onSuccess {
                    addToPlan(it)
                    hideLoading()
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun addToPlan(tip: Tip) {
        val plans = listOf(
            PlanData(
                tip_id = tip.id,
                tip = tipMapper.mapTip(tip)
            )
        )
        val planReminderHeader = PlanReminderHeader(
            tip.getDisplayedTitle(),
            tip.getDisplayedFrequency(),
            tip.getBackgroundImageUrl(),
            null,
            plans,
            tip.isAddedToPlan.not(),
            finishTheActivity = true,
            skippable = false
        )
        state.navigateTo.value =
            CreateOwnPlanItemStepTwoFragmentDirections.actionCreateOwnPlanItemStepTwoFragmentToPlanReminderDialog(
                planReminderHeader
            )
    }
}
