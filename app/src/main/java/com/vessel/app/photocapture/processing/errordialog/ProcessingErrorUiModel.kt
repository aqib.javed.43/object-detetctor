package com.vessel.app.photocapture.processing

import android.os.Parcelable
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class ProcessingErrorUiModel(
    @StringRes val title: Int,
    @StringRes val description: Int,
    val processingErrorDialogActionType: ProcessingErrorDialogActionType
) : Parcelable {
    CHEMISTRY_ERROR(R.string.processing_chat_title, R.string.processing_chat_description, ProcessingErrorDialogActionType.CHAT),
    API_ERROR(R.string.backend_api_error_title, R.string.processing_error_dialog_description, ProcessingErrorDialogActionType.TRY_AGAIN),
    CARD_ERROR(R.string.backend_didnt_detect_title, R.string.processing_error_dialog_description, ProcessingErrorDialogActionType.TRY_AGAIN),
    SCANNING_ERROR(R.string.card_detected_issue, R.string.processing_error_scanning_description, ProcessingErrorDialogActionType.TRY_AGAIN),
    UPLOAD_ERROR(R.string.upload_failure_title, R.string.upload_failure_description, ProcessingErrorDialogActionType.TRY_AGAIN),
    TIMEOUT_ERROR(R.string.backend_time_out_title, R.string.processing_error_dialog_description, ProcessingErrorDialogActionType.TRY_AGAIN),
    CARD_ASSOCIATION_ERROR(R.string.rescanned_api_error_title, R.string.test_association_failed, ProcessingErrorDialogActionType.VIEW_RESULT),
    SHADOW_ERROR(R.string.shadow_error_title, R.string.shadow_error_dialog_description, ProcessingErrorDialogActionType.TRY_AGAIN);

    @StringRes
    fun buttonText() = when (processingErrorDialogActionType) {
        ProcessingErrorDialogActionType.TRY_AGAIN -> R.string.try_again
        ProcessingErrorDialogActionType.CHAT -> R.string.customer_support
        ProcessingErrorDialogActionType.VIEW_RESULT -> R.string.view_result
    }
}

enum class ProcessingErrorDialogActionType {
    TRY_AGAIN, CHAT, VIEW_RESULT
}
