package com.vessel.app.covid19.net.mapper

import android.content.Context
import com.vessel.app.Constants.SERVER_INSERT_DATE_FORMAT
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.covid19.model.CovidTest
import com.vessel.app.covid19.net.data.CovidTestNet
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CovidTestNetMapper @Inject constructor(
    @ApplicationContext private val context: Context,
    private val preferencesRepository: PreferencesRepository
) {
    fun map(data: List<CovidTestNet>) = data.map {
        CovidTest(
            context = context,
            uuid = it.uuid,
            date = SERVER_INSERT_DATE_FORMAT.parse(it.insert_date) ?: Date(),
            status = CovidTest.Status.valueOf(it.status),
            email = preferencesRepository.userEmail.orEmpty()
        )
    }
}
