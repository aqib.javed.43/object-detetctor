package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import com.vessel.app.common.binding.setVisible
import com.vessel.app.databinding.WidgetRecommendationsWithPagingBinding
import com.vessel.app.wellness.model.Paging
import com.vessel.app.wellness.model.RecommendationItem
import com.vessel.app.wellness.ui.TipsAdapter

class RecommendationListWithPagingWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val emptyListPlaceholder by lazy { findViewById<ConstraintLayout>(R.id.emptyListPlaceholder) }
    private val loadMore by lazy { findViewById<TextView>(R.id.loadMore) }
    private val loader by lazy { findViewById<ProgressBar>(R.id.loader) }

    private var recommendations: MutableList<RecommendationItem> = mutableListOf()
    private lateinit var binding: WidgetRecommendationsWithPagingBinding
    private lateinit var tipsAdapter: TipsAdapter
    private var handler: RecommendationListWidgetOnActionHandler? = null
    private var paging: Paging? = null

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetRecommendationsWithPagingBinding.inflate(layoutInflater, this, true)
        loadMore.setOnClickListener { handler?.onLoadMoreClicked(paging!!.copy(currentPage = paging!!.currentPage + 1)) }
    }

    fun setHandler(recommendationsHandler: RecommendationListWidgetOnActionHandler) {
        handler = recommendationsHandler
        if (::tipsAdapter.isInitialized.not()) {
            tipsAdapter = TipsAdapter(recommendationsHandler)
            binding.recommendations.apply {
                adapter = tipsAdapter
                itemAnimator = null
            }
        }
    }

    fun setList(reco: List<RecommendationItem>, paging: Paging? = null) {
        this.paging = paging
        emptyListPlaceholder.setVisible(reco.isNullOrEmpty())
        recommendations = reco.toMutableList()
        tipsAdapter.submitList(recommendations)
        val totalPages = paging?.totalPages ?: 0
        val currentPage = paging?.currentPage ?: 0
        loadMore.setVisible(paging?.showLoadMore ?: (currentPage < totalPages))
    }

    fun loading(enableLoading: Boolean) {
        loader.setVisible(enableLoading)
        if (enableLoading) {
            emptyListPlaceholder.setVisible(false)
            loadMore.setVisible(false)
        }
    }
}

interface RecommendationListWidgetOnActionHandler {
    fun onExpandViewClicked(recommendation: RecommendationItem)
    fun onRecommendationItemClicked(recommendation: RecommendationItem)
    fun onRecommendationItemLikeClicked(recommendation: RecommendationItem)
    fun onRecommendationAddToPlanClicked(recommendation: RecommendationItem)
    fun onLoadMoreClicked(paging: Paging)
}
