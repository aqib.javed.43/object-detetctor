package com.vessel.app.views

import android.content.Context
import android.graphics.Color
import android.icu.text.DecimalFormat
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.marginBottom
import androidx.core.view.updateLayoutParams
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.ChartTouchListener
import com.github.mikephil.charting.listener.OnChartGestureListener
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.Utils
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.common.binding.setTextSize
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.model.data.ReagentLevel
import com.vessel.app.common.model.data.ReagentRange
import com.vessel.app.util.extensions.*
import kotlinx.android.synthetic.main.view_header_chart.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

class ReagentHeaderChartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {
    companion object {
        private const val MAX_X_AXIS_LABELS = 6
        private const val MAX_SHOWN_ITEMS_IN_CHART = 5f
        private const val CHART_LABEL = "Data"
        private val DATE_FORMAT = SimpleDateFormat("MMM\nd", Locale.ENGLISH)
        private const val SELECT_VIEW_ANIMATION_TIME = 900L
        private const val DRAG_CHART_ANIMATION_TIME = 200L
        private const val NITRITES_HIGH_VALUE = 3
        private const val Y_AXIS_SPACING = 130f
        private const val NITRITES_SCALE_VALUE = 8f
        private const val VALUE_LABEL_SPACING_MARGIN = 16
        private const val NITRITES_HIGHEST_VALUE = 5
        private const val MARGIN_X_BOTTOM: Int = 50
    }

    private var highestValue: Float = 0f
    var isAlreadyDrawChart: Boolean = false
    private lateinit var topHintTextView: TextView
    private var unitValue: String = ""
    var disableHighRange: Boolean = false
    private val monthTextSize by lazy {
        context.resources.getDimensionPixelSize(R.dimen.font_2xs).toFloat()
    }
    private val monthMarginTop by lazy { context.resources.getDimensionPixelSize(R.dimen.spacing_large) }
    private val dayTextSize by lazy {
        context.resources.getDimensionPixelSize(R.dimen.font_title).toFloat()
    }
    private val dayMarginTop by lazy { context.resources.getDimensionPixelSize(R.dimen.spacing_3xs) }

    private val boundedChartView by lazy { findViewById<ConstraintLayout>(R.id.boundedChartView) }
    private val chartView by lazy { findViewById<LineChart>(R.id.chartView) }
    private val selectView by lazy { findViewById<ReagentChartCurveSelectView>(R.id.chart_curve_select_view) }

    private val boundRangeBackground by lazy { findViewById<View>(R.id.boundRangeBackground) }
    private val allRangeBackground by lazy { findViewById<View>(R.id.allRangeBackground) }

    private val selectedValueHighlightColor by lazy {
        ContextCompat.getColor(
            context,
            R.color.whiteAlpha60
        )
    }

    private var reagentId: Int = -1
    private var ranges: List<ReagentRange> = emptyList()
    private var selectedEntryPosition: Int = 0
    private var isReagentDataAvailable: Boolean = true
    private var entries: MutableList<DateEntry> = mutableListOf()
    private var calculatePosition: MutableList<Int> = mutableListOf()

    init {
        inflate(getContext(), R.layout.view_reagent_header_chart, this)
        clipChildren = false

        chartView.apply {
            xAxis.apply {
                setDrawGridLines(false)
                setDrawAxisLine(false)
                position = XAxis.XAxisPosition.BOTTOM
                valueFormatter = object : ValueFormatter() {
                    override fun getFormattedValue(value: Float): String {
                        return if (value.toInt() in entries.indices) {
                            entries[value.toInt()].date?.let {
                                DATE_FORMAT.format(
                                    Calendar.getInstance().apply { timeInMillis = it }.time
                                )
                            }.orEmpty()
                        } else {
                            ""
                        }
                    }
                }
            }
            axisLeft.initAxis()
            axisRight.initAxis()

            setDrawGridBackground(false)
            setDrawBorders(false)
            setPinchZoom(false)
            setScaleEnabled(false)
            isHighlightPerDragEnabled = true

            isDoubleTapToZoomEnabled = false
            description.isEnabled = false
            legend.isEnabled = false

            extraBottomOffset =
                Utils.convertPixelsToDp(monthTextSize + monthMarginTop + dayTextSize + dayMarginTop + 5)
            setXAxisRenderer(
                DateXAxisRenderer(
                    context,
                    viewPortHandler,
                    xAxis,
                    getTransformer(YAxis.AxisDependency.LEFT)
                )
            )

            setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
                override fun onNothingSelected() {
                    // Not used
                }

                override fun onValueSelected(e: Entry, h: Highlight?) {
                    val closestEntryPosition =
                        this@ReagentHeaderChartView.entries.toList().indexOfFirst { e.x == it.x }
                    setEntrySelected(closestEntryPosition)
                }
            })

            onChartGestureListener = object : OnChartGestureListener {
                override fun onChartGestureEnd(
                    me: MotionEvent,
                    lastPerformedGesture: ChartTouchListener.ChartGesture?,
                ) {
                    if (lastPerformedGesture == ChartTouchListener.ChartGesture.DRAG) {
                        Handler(Looper.getMainLooper()).postDelayed(
                            {
                                val middleShownValue =
                                    (chartView.lowestVisibleX + chartView.visibleXRange / 2).toInt()
                                val closestEntryPosition =
                                    this@ReagentHeaderChartView.entries.toList()
                                        .indexOfFirst { middleShownValue == it.x.toInt() }
                                setEntrySelected(closestEntryPosition)
                            },
                            DRAG_CHART_ANIMATION_TIME
                        )
                    }
                }

                override fun onChartFling(
                    me1: MotionEvent?,
                    me2: MotionEvent?,
                    velocityX: Float,
                    velocityY: Float,
                ) {
                    // Not used
                }

                override fun onChartSingleTapped(me: MotionEvent?) {
                    // Not used
                }

                override fun onChartGestureStart(
                    me: MotionEvent?,
                    lastPerformedGesture: ChartTouchListener.ChartGesture?,
                ) {
                    // Not used
                }

                override fun onChartScale(me: MotionEvent?, scaleX: Float, scaleY: Float) {
                    // Not used
                }

                override fun onChartLongPressed(me: MotionEvent?) {
                    // Not used
                }

                override fun onChartDoubleTapped(me: MotionEvent?) {
                    // Not used
                }

                override fun onChartTranslate(me: MotionEvent?, dX: Float, dY: Float) {
                    // Not used
                }
            }

            invalidate()
        }
    }

    private fun YAxis.initAxis() {
        setDrawGridLines(false)
        setDrawAxisLine(false)
        setDrawLabels(false)
    }

    fun setEntries(entries: List<DateEntry>) {
        this.entries.clear()
        if (reagentId == ReagentItem.Nitrites.id) {
            this.entries.addAll(processEntries(entries))
        } else {
            this.entries.addAll(processValidEntries(entries))
        }
        Log.d("TAG", "setEntries: ${this.entries}")
        this.entries.add(0, DateEntry(0f, entries.first().y, null))

        selectedEntryPosition = this.entries.size - 1
        updateChartEntriesData()
    }

    private fun processEntries(entries: List<DateEntry>): List<DateEntry> {
        val entriesProcessed = entries.filter {
            it.y != Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE
        }
        val newEntries = ArrayList<DateEntry>()
        entriesProcessed.mapIndexed { index, dateEntry ->
            when {
                dateEntry.y == NITRITES_SCALE_VALUE -> {
                    dateEntry.y = NITRITES_HIGHEST_VALUE.toFloat()
                    dateEntry.x = (index + 1).toFloat()
                    newEntries.add(dateEntry)
                }
                dateEntry.y > NITRITES_HIGH_VALUE.toFloat() -> {
                    dateEntry.y = NITRITES_HIGH_VALUE.toFloat() + (dateEntry.y - NITRITES_HIGH_VALUE.toFloat() - 1f) * NITRITES_HIGHEST_VALUE / NITRITES_SCALE_VALUE
                    dateEntry.x = (index + 1).toFloat()
                    newEntries.add(dateEntry)
                }
                else -> {
                    dateEntry.x = (index + 1).toFloat()
                    newEntries.add(dateEntry)
                }
            }
        }
        return newEntries
    }
    private fun processValidEntries(entries: List<DateEntry>): List<DateEntry> {
        val entriesProcessed = entries.filter {
            it.y != Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE
        }
        val newEntries = ArrayList<DateEntry>()
        entriesProcessed.mapIndexed { index, dateEntry ->

            dateEntry.x = (index + 1).toFloat()
            newEntries.add(dateEntry)
        }

        return newEntries
    }

    private fun updateChartEntriesData(moveToEnd: Boolean = true) {
        val isNitrites = reagentId == ReagentItem.Nitrites.id
        chartView.apply {
            xAxis.setLabelCount(
                if (this@ReagentHeaderChartView.entries.size > MAX_X_AXIS_LABELS) {
                    MAX_X_AXIS_LABELS
                } else {
                    this@ReagentHeaderChartView.entries.size
                },
                true
            )

            val transparentDataSet =
                LineDataSet(
                    this@ReagentHeaderChartView.entries.subList(0, 1).toList(),
                    CHART_LABEL
                ).apply {
                    color = Color.TRANSPARENT
                    lineWidth = 0f
                    setDrawValues(false)
                    mode = LineDataSet.Mode.HORIZONTAL_BEZIER

                    setDrawHorizontalHighlightIndicator(false)
                    setDrawVerticalHighlightIndicator(false)
                }
            val scoreDataSet = LineDataSet(
                this@ReagentHeaderChartView.entries.subList(
                    1,
                    this@ReagentHeaderChartView.entries.size
                ).toList(),
                CHART_LABEL
            ).apply {
                color = Color.BLACK
                circleHoleColor = Color.BLACK
                circleHoleRadius = 3f
                circleRadius = 6f
                lineWidth = 2f
                setDrawValues(false)
                mode = LineDataSet.Mode.HORIZONTAL_BEZIER

                setDrawHorizontalHighlightIndicator(false)
                setDrawVerticalHighlightIndicator(false)

                val circleColors = mutableListOf<Int>()

                for (index in 1..this@ReagentHeaderChartView.entries.size) {
                    if (index == selectedEntryPosition) {
                        circleColors.add(selectedValueHighlightColor)
                    } else {
                        circleColors.add(Color.TRANSPARENT)
                    }
                }

                setCircleColors(circleColors)
            }
            data = LineData(transparentDataSet).apply {
                addDataSet(scoreDataSet)
            }

            if (moveToEnd) {
                setVisibleXRangeMaximum(MAX_SHOWN_ITEMS_IN_CHART)

                chartView.moveViewToX(entries[selectedEntryPosition].x)

                animateX(SELECT_VIEW_ANIMATION_TIME.toInt(), Easing.EaseInOutQuad)
            }
        }
    }

    private fun setEntrySelected(entryPosition: Int) {
        this.selectedEntryPosition = entryPosition
        moveHighlightToEntryPosition(topHintTextView, entryPosition)
        updateChartEntriesData(false)
    }

    fun setEntriesRanges(values: List<ReagentRange>) {
        this.highestValue = values.last().to
        val isHydrationPlan = reagentId == ReagentItem.Hydration.id
        val isNitrites = reagentId == ReagentItem.Nitrites.id

        val ranges = if (isHydrationPlan) {
            values.reversed()
        } else if (isNitrites) {
            processRanges(values)
        } else {
            values
        }

        chartView.apply {
            if (isHydrationPlan) {
                axisLeft.apply {
                    axisMinimum = ranges.last().to
                    axisMaximum = ranges.first().from
                }
                axisLeft.isInverted = true
            } else {
                axisLeft.apply {
                    axisMinimum = ranges.first().from
                    axisMaximum = ranges.last().to
                }
            }
        }

        setNonAvailableData(ranges)
        drawRanges(ranges)
    }

    private fun setNonAvailableData(ranges: List<ReagentRange>) {
        val nextEntryIndexWithValue = findNextEntryIndexWithValue()
        if (nextEntryIndexWithValue == this.entries.lastIndex) {
            updateChartStartValues()
            return
        }
        if (nextEntryIndexWithValue != -1) {
            for (index in this.entries.size - 1 downTo 0) {
                if (this.entries[index].y == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE) {
                    if (index < nextEntryIndexWithValue) {
                        break
                    } else {
                        this.entries[index].y = this.entries[nextEntryIndexWithValue].y
                    }
                }
            }
            isReagentDataAvailable = false
            updateChartStartValues()
            updateChartEntriesData()
        } else {
            val startValue = if (chartView.axisLeft.isInverted) {
                ranges.last().to
            } else {
                ranges.first().from
            }
            val scoreValue = startValue + abs(ranges.last().to - ranges.first().from) / 2
            this.entries.forEach {
                it.y = scoreValue
            }
            isReagentDataAvailable = false
            updateChartStartValues()
            updateChartEntriesData()
        }
    }

    private fun updateChartStartValues() {
        for (index in 0 until this.entries.size) {
            if (this.entries[index].y == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE) {
                this.entries[index].y = 0f // zero point in the chart to not shown as cutted off
            } else {
                break
            }
        }
    }

    private fun findNextEntryIndexWithValue(): Int {
        return this.entries.indexOfLast { it.y != Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE }
    }

    private fun moveHighlightToEntryPosition(topTextView: TextView, entryPosition: Int) {
        val chartEntry = this.entries[entryPosition]

        val entryPoint = chartView.getTransformer(YAxis.AxisDependency.LEFT).getPixelForValues(
            chartEntry.x,
            chartEntry.y
        )
        val transitionYPoint = entryPoint.y.toFloat() - Y_AXIS_SPACING

        val chartValue = getChartTextValue(chartEntry)

        selectView.startTransition(
            entryPoint.x.toFloat() - context.resources.getDimensionPixelSize(R.dimen.spacing_2xl),
            if (transitionYPoint < topTextView.y) (entryPoint.y + Y_AXIS_SPACING / 4).toFloat() else transitionYPoint,
            chartValue,
            unitValue,
            chartEntry.showNumber
        )
    }

    private fun getChartTextValue(chartEntry: DateEntry) =
        if (isReagentDataAvailable && chartEntry.y != Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE) {
            val fromRangeValue: Float?
            val toRangeValue: Float?
            when (reagentId) {
                ReagentItem.Hydration.id -> {
                    toRangeValue =
                        ranges.firstOrNull { chartEntry.y <= it.from && chartEntry.y >= it.to }?.from
                    fromRangeValue =
                        ranges.firstOrNull { chartEntry.y <= it.from && chartEntry.y >= it.to }?.to
                }
                ReagentItem.Nitrites.id -> {
                    toRangeValue =
                        ranges.firstOrNull { chartEntry.y >= it.from && chartEntry.y <= it.to }?.to
                    fromRangeValue =
                        ranges.firstOrNull { chartEntry.y >= it.from && chartEntry.y <= it.to }?.from
                }
                else -> {
                    toRangeValue =
                        ranges.firstOrNull { chartEntry.y >= it.from && chartEntry.y <= it.to }?.to
                    fromRangeValue =
                        ranges.firstOrNull { chartEntry.y >= it.from && chartEntry.y <= it.to }?.from
                }
            }
            if (reagentId == ReagentItem.Nitrites.id) {
                val detectValue = when (ranges.firstOrNull { chartEntry.y >= it.from && chartEntry.y <= it.to }?.reagentType) {
                    ReagentLevel.Good -> {
                        "Not Detected"
                    }
                    ReagentLevel.High -> {
                        if ((toRangeValue ?: 0).toInt() <= NITRITES_HIGH_VALUE) "Detected Low" else "Detected High"
                    }
                    else -> {
                        context.getString(R.string.na)
                    }
                }
                detectValue
            } else {
                val dec = DecimalFormat("#,###.###")
                "${fromRangeValue?.formatToString()}-${dec.format(toRangeValue)}"
            }
        } else {
            context.getString(R.string.na)
        }

    fun setReagentId(id: Int) {
        this.reagentId = id
        this.disableHighRange = id == ReagentItem.VitaminC.id
        selectView.setReagentId(reagentId)
    }

    private fun drawRanges(ranges: List<ReagentRange>) {
        afterMeasured {
            isAlreadyDrawChart = true
            if (reagentId == ReagentItem.Nitrites.id) {
                this@ReagentHeaderChartView.ranges = processRanges(ranges)
            } else {
                this@ReagentHeaderChartView.ranges = ranges
            }
            Log.d("TAG", "drawRanges: ${this@ReagentHeaderChartView.ranges}")
            val topTextView = TextView(context)
            val bottomTextView = TextView(context)

            topTextView.id = View.generateViewId()
            bottomTextView.id = View.generateViewId()

            bottomTextView.text = this@ReagentHeaderChartView.ranges.first().from.formatToString()
            topTextView.text = if (reagentId == ReagentItem.Nitrites.id)highestValue.toInt().toString() else this@ReagentHeaderChartView.ranges.last().to.formatToString()

            drawValueView(chartView.y, topTextView)
            drawValueView(
                chartView.height - chartView.extraBottomOffset - chartView.marginBottom - chartView.axisLeft.spaceTop - chartView.axisLeft.spaceBottom - bottomTextView.height / 2 - MARGIN_X_BOTTOM,
                bottomTextView
            )
            setBoundedBiases(topTextView, bottomTextView, this@ReagentHeaderChartView.ranges)
            this@ReagentHeaderChartView.topHintTextView = topTextView
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    chartView.moveViewToX(entries[selectedEntryPosition].x)
                    moveHighlightToEntryPosition(topTextView, selectedEntryPosition)
                    selectView.isVisible = true
                },
                SELECT_VIEW_ANIMATION_TIME + 100
            )
        }
    }

    private fun drawTopBottomRangePoint(pointY: Float) {
        drawRangePointView(0f)
        drawRangePointView(pointY + topHintTextView.height - MARGIN_X_BOTTOM)
    }

    private fun processRanges(ranges: List<ReagentRange>): List<ReagentRange> {
        val newRanges = ArrayList<ReagentRange>()
        ranges.map {
            if (it.to > NITRITES_HIGHEST_VALUE) {
                val newValue = it.copy()
                newValue.to = NITRITES_HIGHEST_VALUE.toFloat()
                newRanges.add(newValue)
            } else {
                newRanges.add(it.copy())
            }
        }
        return newRanges
    }

    fun reDrawRanges(ranges: List<ReagentRange>) {
        if (isAlreadyDrawChart) return
        if (reagentId == ReagentItem.Nitrites.id) {
            this@ReagentHeaderChartView.ranges = processRanges(ranges)
        } else {
            this@ReagentHeaderChartView.ranges = ranges
        }
        val topTextView = TextView(context)
        val bottomTextView = TextView(context)

        topTextView.id = View.generateViewId()
        bottomTextView.id = View.generateViewId()

        bottomTextView.text = this@ReagentHeaderChartView.ranges.first().from.formatToString()
        topTextView.text = if (reagentId == ReagentItem.Nitrites.id)highestValue.toInt().toString() else this@ReagentHeaderChartView.ranges.last().to.formatToString()

        reSetBoundedBiases(topTextView, bottomTextView, this@ReagentHeaderChartView.ranges)
        this@ReagentHeaderChartView.topHintTextView = topTextView
        Handler(Looper.getMainLooper()).postDelayed(
            {
                chartView.moveViewToX(entries[selectedEntryPosition].x)
                moveHighlightToEntryPosition(topTextView, selectedEntryPosition)
                selectView.isVisible = true
            },
            SELECT_VIEW_ANIMATION_TIME + 100
        )
    }

    private fun drawValueView(yPoint: Float, view: TextView) {
        Log.d("boundedChartView", "drawValueView: ${view.text}, $yPoint")
        view.setTextColor(ContextCompat.getColor(context, R.color.darkText))
        view.setTextSize(R.dimen.font_2xs)
        boundedChartView.addView(view)
        ConstraintSet().apply {
            clone(boundedChartView)
            connect(
                view.id,
                ConstraintSet.END,
                boundedChartView.id,
                ConstraintSet.END,
                VALUE_LABEL_SPACING_MARGIN
            )
            applyTo(boundedChartView)
        }
        view.translationY = yPoint
    }
    private fun drawRangeValueView(yPoint: Float, view: TextView) {
        Log.d("boundedChartView", "drawRangeValueView: ${view.text}, $yPoint")
        view.setTextColor(ContextCompat.getColor(context, R.color.darkText))
        view.setTextSize(R.dimen.font_2xs)
        boundedChartView.addView(view)
        ConstraintSet().apply {
            clone(boundedChartView)
            connect(
                view.id,
                ConstraintSet.END,
                boundedChartView.id,
                ConstraintSet.END,
                VALUE_LABEL_SPACING_MARGIN
            )
            applyTo(boundedChartView)
        }
        drawRangePointView(yPoint + topHintTextView.height / 2)
        view.translationY = yPoint
    }
    private fun drawRangePointView(yPoint: Float) {
        Log.d("boundedChartView", "drawRangePointView: $yPoint")
        val rangeValueView = View(context)
        rangeValueView.layoutParams = LayoutParams(15, 3)
        rangeValueView.setBackgroundColor(Color.BLACK)
        rangeValueView.id = View.generateViewId()
        boundedChartView.addView(rangeValueView)
        ConstraintSet().apply {
            clone(boundedChartView)
            connect(
                rangeValueView.id,
                ConstraintSet.END,
                boundedChartView.id,
                ConstraintSet.END,
                0
            )
            applyTo(boundedChartView)
        }
        rangeValueView.translationY = yPoint
    }

    private fun setBoundedBiases(
        topTextView: TextView,
        bottomTextView: TextView,
        ranges: List<ReagentRange>,
    ) {
        val axisMaximum = chartView.axisLeft.mAxisMaximum
        val axisMinimum = chartView.axisLeft.axisMinimum
        val axisRange = abs(axisMaximum - axisMinimum)

        afterMeasured {
            val minLabelPoint = bottomTextView.getPointLocationOnScreen()
            val maxLabelPoint = topTextView.getPointLocationOnScreen()
            val chartHeight = minLabelPoint.y - maxLabelPoint.y
            val rangeItemValue = chartHeight / axisRange

            for (i in 1 until ranges.size) {
                val rangeView = TextView(context)
                rangeView.id = View.generateViewId()

                rangeView.text = ranges[i].from.formatToString()

                val yPoint = if (chartView.axisLeft.isInverted) {
                    (bottomTextView.y - abs(axisMaximum - ranges[i].from) * rangeItemValue)
                } else {
                    (bottomTextView.y - abs(ranges[i].from - axisMinimum) * rangeItemValue)
                }
                drawRangeValueView(
                    yPoint,
                    rangeView
                )
            }
            drawTopBottomRangePoint(chartView.height - chartView.extraBottomOffset - chartView.marginBottom - chartView.axisLeft.spaceTop - chartView.axisLeft.spaceBottom - bottomTextView.height / 2)
            drawBoundedBackground(bottomTextView, ranges, rangeItemValue)
            drawLevelValues(bottomTextView, ranges, rangeItemValue)
        }
    }
    private fun reSetBoundedBiases(
        topTextView: TextView,
        bottomTextView: TextView,
        ranges: List<ReagentRange>,
    ) {
        val axisMaximum = chartView.axisLeft.mAxisMaximum
        val axisMinimum = chartView.axisLeft.axisMinimum
        val axisRange = abs(axisMaximum - axisMinimum)

        val minLabelPoint = bottomTextView.getPointLocationOnScreen()
        val maxLabelPoint = topTextView.getPointLocationOnScreen()
        val chartHeight = minLabelPoint.y - maxLabelPoint.y
        val rangeItemValue = chartHeight / axisRange

        for (i in 1 until ranges.size) {
            val rangeView = TextView(context)
            rangeView.id = View.generateViewId()

            rangeView.text = ranges[i].from.formatToString()

            val yPoint = if (chartView.axisLeft.isInverted) {
                (bottomTextView.y - abs(axisMaximum - ranges[i].from) * rangeItemValue)
            } else {
                (bottomTextView.y - abs(ranges[i].from - axisMinimum) * rangeItemValue)
            }
            drawBoundedBackground(bottomTextView, ranges, rangeItemValue)
        }
    }
    private fun drawLevelValues(
        bottomTextView: TextView,
        ranges: List<ReagentRange>,
        rangeItemValue: Float,
    ) {
        val lowRange = ranges.filter { it.reagentType == ReagentLevel.Low }
        val goodRange = ranges.filter { it.reagentType == ReagentLevel.Good }
        val highRange = ranges.filter { it.reagentType == ReagentLevel.High }

        drawHintValue(
            bottomTextView,
            lowRange,
            rangeItemValue
        )
        drawHintValue(
            bottomTextView,
            goodRange,
            rangeItemValue
        )
        if (!disableHighRange)drawHintValue(
            bottomTextView,
            highRange,
            rangeItemValue
        )
    }

    private fun drawHintValue(
        bottomTextView: TextView,
        ranges: List<ReagentRange>,
        rangeItemValue: Float
    ) {
        if (ranges.isEmpty()) {
            return
        }
        var label = ranges.first().label
        if (reagentId == ReagentItem.Nitrites.id) {
            for (range in ranges) {
                label = when (range.label.toLowerCase()) {
                    "good" -> "Not\nDetected"
                    "high" -> {
                        if (range.to <= NITRITES_HIGH_VALUE) "Detected\nLow" else "Detected\nHigh"
                    }
                    else -> "Detected\nHigh"
                }
                drawHintValue(
                    bottomTextView,
                    range.from,
                    range.to,
                    rangeItemValue,
                    label
                )
            }
        } else {
            drawHintValue(
                bottomTextView,
                ranges.first().from,
                ranges.last().to,
                rangeItemValue,
                label
            )
        }
    }

    private fun drawHintValue(
        bottomTextView: TextView,
        fromValue: Float,
        toValue: Float,
        rangeItemValue: Float,
        hint: String,
    ) {
        val hintViewRotationDegree = -90f
        val hintView = TextView(context)
        hintView.id = View.generateViewId()
        hintView.pivotX = 0f
        hintView.pivotY = 0f
        hintView.text = hint
        hintView.rotation = hintViewRotationDegree

        val lowerBoundAxisPosition = if (chartView.axisLeft.isInverted) {
            (bottomTextView.y - abs(chartView.axisLeft.mAxisMaximum - toValue) * rangeItemValue)
        } else {
            (bottomTextView.y - abs(fromValue - chartView.axisLeft.mAxisMinimum) * rangeItemValue)
        }

        val upperBoundAxisPosition = if (chartView.axisLeft.isInverted) {
            (bottomTextView.y - abs(chartView.axisLeft.mAxisMaximum - fromValue) * rangeItemValue)
        } else {
            (bottomTextView.y - abs(toValue - chartView.axisLeft.mAxisMinimum) * rangeItemValue)
        }

        hintView.setTextColor(ContextCompat.getColor(context, R.color.darkText))
        hintView.setTextSize(R.dimen.font_2xs)
        hintView.isSingleLine = reagentId != ReagentItem.Nitrites.id
        if (reagentId == ReagentItem.Nitrites.id) {
            hintView.maxLines = 2
            hintView.setTextSize(R.dimen.font_chart_size)
        }
        boundedChartView.addView(hintView)

        hintView.translationY = if (chartView.axisLeft.isInverted) {
            upperBoundAxisPosition
        } else {
            lowerBoundAxisPosition
        }

        hintView.ellipsize = TextUtils.TruncateAt.END
        hintView.gravity = Gravity.CENTER_HORIZONTAL

        hintView.width = if (chartView.axisLeft.isInverted) {
            ((upperBoundAxisPosition - lowerBoundAxisPosition).toInt() * 0.8).toInt()
        } else {
            ((lowerBoundAxisPosition - upperBoundAxisPosition).toInt() * 0.8).toInt()
        }

        hintView.translationX = context.getDimenInCalculatedPixels(R.dimen.spacing_3xs).toFloat()
    }

    private fun drawBoundedBackground(
        bottomTextView: TextView,
        ranges: List<ReagentRange>,
        rangeItemValue: Float,
    ) {
        val boundedRanges = ranges.filter { it.reagentType == ReagentLevel.Good }

        val fromValue = if (chartView.axisLeft.isInverted) {
            boundedRanges.last().to
        } else {
            boundedRanges.first().from
        }

        val toValue = if (chartView.axisLeft.isInverted) {
            boundedRanges.first().from
        } else {
            boundedRanges.last().to
        }

        val lowerBoundAxisPosition = if (chartView.axisLeft.isInverted) {
            (bottomTextView.y - abs(chartView.axisLeft.mAxisMaximum - toValue) * rangeItemValue)
        } else {
            (bottomTextView.y - abs(fromValue - chartView.axisLeft.mAxisMinimum) * rangeItemValue)
        }

        val upperBoundAxisPosition = if (chartView.axisLeft.isInverted) {
            (bottomTextView.y - abs(chartView.axisLeft.mAxisMaximum - fromValue) * rangeItemValue)
        } else {
            (bottomTextView.y - abs(toValue - chartView.axisLeft.mAxisMinimum) * rangeItemValue)
        }

        boundRangeBackground.updateLayoutParams {
            height = (lowerBoundAxisPosition - upperBoundAxisPosition).toInt()
            if (disableHighRange) height = (height - bottomTextView.height / 2) + MARGIN_X_BOTTOM
        }

        boundRangeBackground.y = if (disableHighRange) upperBoundAxisPosition + bottomTextView.height / 2 - MARGIN_X_BOTTOM / 2 else upperBoundAxisPosition + bottomTextView.height / 2

        boundRangeBackground.setBackgroundResource(R.color.primaryLight)

        allRangeBackground.updateLayoutParams {
            height = (bottomTextView.y - chartView.y).toInt() + bottomTextView.height / 2
        }

        allRangeBackground.y = if (disableHighRange) boundRangeBackground.y else chartView.y
        allRangeBackground.setBackgroundResource(if (reagentId == ReagentItem.Calcium.id) R.color.beige_color else R.color.chart_warning)
    }

    fun setUnit(text: String?) {
        this.unitValue = text.orEmpty()
    }
}
