package com.vessel.app.photoupload

import android.util.Log
import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository

class PhotoUploadViewModel@ViewModelInject constructor(
    val state: PhotoUploadState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler {
    init {
    }
    override fun onBackButtonClicked(view: View) {
        Log.d(TAG, "onBackButtonClicked: ")
        view.findNavController().navigateUp()
    }
    fun onUploadClick() {
        Log.d(TAG, "onUploadClick: ")
        // Todo process upload photo
    }
    fun onLaterClicked(v: View) {
        state.navigateTo.value = PhotoUploadFragmentDirections.actionGlobalHomeTeamFragment()
    }
    companion object {
        const val TAG = "PhotoUploadViewModel"
    }
}
