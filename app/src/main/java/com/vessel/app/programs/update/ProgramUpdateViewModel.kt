package com.vessel.app.programs.update

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.util.ResourceRepository

class ProgramUpdateViewModel @ViewModelInject constructor(
    val state: ProgramUpdateState,
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val programManager: ProgramManager,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    fun onUpdateClicked(view: View) {
        state.navigateTo.value = ProgramUpdateFragmentDirections.actionProgramUpdateFragmentToGoalsSelectFragment(true)
    }

    fun onLaterClicked(view: View) {
        navigateBack()
    }
}
