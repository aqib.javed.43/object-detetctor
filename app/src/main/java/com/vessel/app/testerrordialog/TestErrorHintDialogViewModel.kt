package com.vessel.app.testerrordialog

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.testerrordialog.model.TestErrorHintModel
import com.vessel.app.testerrordialog.ui.TestErrorHintHeaderAdapter
import com.vessel.app.util.ResourceRepository

class TestErrorHintDialogViewModel @ViewModelInject constructor(
    val state: TestErrorHintState,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider), TestErrorHintHeaderAdapter.OnActionHandler {

    init {
        state.errorHints.value = listOf(
            TestErrorHintModel(
                R.string.hint_one,
                R.string.dont_get_enough_urine,
                R.string.test_error_hint_first,
                R.drawable.test_error_hint_first,
                null
            ),
            TestErrorHintModel(
                R.string.hint_second,
                R.string.over_saturated_test,
                R.string.test_error_hint_second,
                R.drawable.test_error_hint_second,
                null
            ),
            TestErrorHintModel(
                R.string.hint_third,
                R.string.faulty_test_card,
                R.string.test_error_hint_third,
                null,
                R.string.test_error_hint_email
            )
        )
    }

    override fun onDoneClicked() {
        state.dismissDialog.call()
    }
}
