package com.vessel.app.wellness.homeprogromselection

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Program
import com.vessel.app.common.net.data.PaginationData
import com.vessel.app.common.tracking.TrackingConstants.FROM_HOME
import com.vessel.app.common.widget.ProgramCardWidgetOnActionHandler
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class HomeProgramSelectionViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val programManager: ProgramManager,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider),
    ProgramCardWidgetOnActionHandler,
    ToolbarHandler {

    val state = HomeProgramSelectionState(savedStateHandle.get("goalIdSelected")!!)

    init {
        showLoading()
        loadPrograms(state.goalIdSelected)
    }

    fun reloadPrograms() {
        state.programPagination.value =
            PaginationData(page = 0, per_page = 3, pages = null, total = null)
        loadPrograms(state.goalIdSelected)
    }

    private fun loadPrograms(goalIdSelected: MutableLiveData<Int>) {
        viewModelScope.launch {
            goalIdSelected.value?.let {
                programManager.getProgramsByGoal(
                    goalId = it,
                    true,
                    state.programPagination.value?.per_page,
                    (state.programPagination.value?.page ?: 0) + 1
                ).onSuccess { programModel ->
                    if (programModel.pagination.page == 1) {
                        state.programsItems.value = programModel.program.toMutableList()
                    } else {
                        val prePrograms = state.programsItems.value ?: mutableListOf()
                        prePrograms.addAll(programModel.program)
                        state.programsItems.value = prePrograms
                    }
                    state.programPagination.value = programModel.pagination
                    hideLoading(false)
                    state.isLoadingMore.postValue(false)
                }.onServiceError {
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }.onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }.onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }.onError {
                    hideLoading(false)
                    state.isLoadingMore.postValue(false)
                }
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onLoadMoreClicked() {
        state.isLoadingMore.value = true
        loadPrograms(state.goalIdSelected)
    }

    override fun onProgramItemClicked(program: Program) {
        state.navigateTo.value =
            HomeProgramSelectionFragmentDirections.actionProgramSelectionFragmentToProgramInfoDialogFragment(
                program
            )
    }

    override fun onProgramInfoClicked(program: Program) {
        state.navigateTo.value =
            HomeProgramSelectionFragmentDirections.actionProgramSelectionFragmentToProgramInfoDialogFragment(
                program
            )
    }

    override fun onJoinProgramClicked(program: Program) {
        if (program.isEnrolled.not()) {
            state.navigateTo.value =
                HomeProgramSelectionFragmentDirections.actionProgramSelectionFragmentToJoinProgramFragment(
                    program,
                    FROM_HOME
                )
        } else {
            state.navigateTo.value =
                HomeProgramSelectionFragmentDirections.actionProgramSelectionFragmentToProgramDetailsFragment(
                    program
                )
        }
    }
}
