package com.vessel.app.profile.editprofile

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.model.Contact
import com.vessel.app.onboarding.height.HeightDialogFragment
import com.vessel.app.onboarding.weight.WeightDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_edit_profile.*

@AndroidEntryPoint
class EditProfileFragment : BaseFragment<EditProfileViewModel>() {
    override val viewModel: EditProfileViewModel by viewModels()
    override val layoutResId = R.layout.fragment_edit_profile

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observe(viewModel.state.dismissView) {
            activity?.finish()
        }

        findNavController().currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData<Contact.Height>(HeightDialogFragment.HEIGHT_KEY)
            ?.observe(viewLifecycleOwner) { value ->
                viewModel.onHeightSet(value)
            }

        findNavController().currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData<Float>(WeightDialogFragment.WEIGHT_KEY)
            ?.observe(viewLifecycleOwner) { value ->
                viewModel.onWeightSet(value)
            }

        btnUploadPhoto.setOnClickListener {
            if (allPermissionsGranted(Constants.SCAN_CARD_PERMISSIONS)) {
                viewModel.onUploadPhotoClick(it)
            } else {
                getRuntimePermissions(
                    Constants.SCAN_CARD_PERMISSIONS,
                    Constants.SCAN_CARD_PERMISSIONS_REQUEST_CODE
                )
            }
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (!allPermissionsGranted(Constants.SCAN_CARD_PERMISSIONS)) {
            Toast.makeText(requireContext(), R.string.permissions_required, Toast.LENGTH_LONG)
                .show()
        } else {
            viewModel.onUploadPhotoClick(btnUploadPhoto)
        }
    }
}
