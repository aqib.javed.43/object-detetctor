package com.vessel.app.goal

import androidx.lifecycle.MutableLiveData
import com.vessel.app.goal.model.GoalHeader
import javax.inject.Inject

class GoalState @Inject constructor(goalItem: GoalHeader) {
    val goalHeaderItems = MutableLiveData<List<Any>>(listOf(goalItem))

    operator fun invoke(block: GoalState.() -> Unit) = apply(block)
}
