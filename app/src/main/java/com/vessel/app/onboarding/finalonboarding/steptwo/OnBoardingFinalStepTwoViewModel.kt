package com.vessel.app.onboarding.finalonboarding.steptwo

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository

class OnBoardingFinalStepTwoViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
) : BaseViewModel(resourceProvider),
    ToolbarHandler {

    val state = OnBoardingFinalStepTwoState()
    val bgColor = if (preferencesRepository.isExistingUserFirstTime) R.color.beige_color else R.color.primaryLight

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        state.navigateTo.value = OnBoardingFinalStepTwoFragmentDirections.actionOnBoardingFinalStepTwoFragmentToOnBoardingFinalStepThreeFragment()
    }
}
