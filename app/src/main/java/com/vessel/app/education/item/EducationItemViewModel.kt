package com.vessel.app.education.item

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.MembershipManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration

class EducationItemViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    @Assisted savedStateHandle: SavedStateHandle,
    val preferencesRepository: PreferencesRepository,
    val membershipManager: MembershipManager,
    val remoteConfiguration: RemoteConfiguration,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    val state = EducationItemState(savedStateHandle[EducationItemFragment.ARG_PAGE]!!)

    init {
        remoteConfiguration.fetchConfigFromRemote()
    }
    fun onBackClicked() {
        state.backEvent.call()
    }

    fun onContinueClicked() {
        state.continueEvent.call()
    }

    fun onLiveChatClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CHAT_WITH_SUPPORT)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.launchLiveChatEvent.call()
    }

    fun isLiveChat() = remoteConfiguration.getBoolean("use_livechat")
    fun getUserName() = preferencesRepository.userFirstName
    fun getUserEmail() = preferencesRepository.userEmail
}
