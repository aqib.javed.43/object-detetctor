package com.vessel.app.expert

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.widget.ProgramCardWidget
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.ExpandableTextViewGroup
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ExpertProfileFragment : BaseFragment<ExpertProfileViewModel>() {
    override val viewModel: ExpertProfileViewModel by viewModels()
    override val layoutResId = R.layout.fragment_expert_profile

    private val expandedTipsAdapter: ExpandedTipsAdapter by lazy { ExpandedTipsAdapter(viewModel, viewModel) }
    private val collapsedIcon by lazy { requireView().findViewById<AppCompatImageView>(R.id.collapsedIcon) }
    private lateinit var programList: ProgramCardWidget
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupTipList()
        setupProgramList()
        setupObservers()
    }

    private fun setupProgramList() {
        programList = requireView().findViewById(R.id.programs)
        programList.setHandler(viewModel)
        requireView().findViewById<ConstraintLayout>(R.id.programContainer).setOnClickListener {
            viewModel.state.isProgramCollapsed.value = viewModel.state.isProgramCollapsed.value?.not()
        }
    }

    private fun setupTipList() {
        requireView().findViewById<RecyclerView>(R.id.expendableTipRecyclerView).apply {
            adapter = expandedTipsAdapter
            itemAnimator = null
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(contactExpert) {
                requireView().findViewById<ConstraintLayout>(R.id.testTakenContainer).isVisible =
                    it.testsCount.isNotEmpty()
                requireView().findViewById<ConstraintLayout>(R.id.joinedContainer).isVisible =
                    it.joinedDate.isNotEmpty()
                requireView().findViewById<View>(R.id.basicInfoBackground).isVisible =
                    it.joinedDate.isNotEmpty() || it.testsCount.isNotEmpty()
                requireView().findViewById<View>(R.id.dividerView).isInvisible =
                    it.joinedDate.isEmpty() || it.testsCount.isEmpty()
                requireView().findViewById<View>(R.id.aboutBackground).isVisible =
                    it.formattedAbout.isNotEmpty()
                requireView().findViewById<AppCompatTextView>(R.id.aboutTitle).isVisible =
                    it.formattedAbout.isNotEmpty()
                requireView().findViewById<TextView>(R.id.jobTitleTv).text = it.occupationAndLocation()
                requireView().findViewById<ImageView>(R.id.verifyIv).isVisible =
                    (it.isVerified == true)
                requireView().findViewById<ExpandableTextViewGroup>(R.id.aboutValue).apply {
                    isVisible = it.formattedAbout.isNotEmpty()
                    text = it.formattedAbout
                }
            }
            observe(isProgramCollapsed) { isCollapsed ->
                if (isCollapsed.not()) {
                    programList.isVisible = false
                    collapsedIcon.setImageResource(R.drawable.ic_collapsed_tip)
                } else {
                    programList.isVisible = true
                    collapsedIcon.setImageResource(R.drawable.ic_expanded_tip)
                }
            }
            observe(goalWithTips) {
                expandedTipsAdapter.submitList(it)
            }
            observe(programsItem) {
                programList.setList(it)
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
