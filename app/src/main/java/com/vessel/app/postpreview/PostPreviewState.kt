package com.vessel.app.postpreview

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PostPreviewState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: PostPreviewState.() -> Unit) = apply(block)
}
