package com.vessel.app.common.manager

import com.vessel.app.common.repo.SubGoalRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SubGoalManager @Inject constructor(
    private val subGoalRepository: SubGoalRepository
) {
    suspend fun fetchAllSubGoals(goalId: Int?) = subGoalRepository.getAllSubGoals(goalId)
}
