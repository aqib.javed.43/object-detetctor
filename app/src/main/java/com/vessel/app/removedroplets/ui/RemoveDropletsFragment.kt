package com.vessel.app.removedroplets.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.removedroplets.RemoveDropletsViewModel
import com.vessel.app.taketest.TakeTestViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RemoveDropletsFragment : BaseFragment<RemoveDropletsViewModel>() {
    override val viewModel: RemoveDropletsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_remove_droplets
    private val takeTestViewModel by lazy { parentViewModel<TakeTestViewModel>() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(navigateToCapture) {
                takeTestViewModel.generateSampleUUID()
                findNavController().navigate(RemoveDropletsFragmentDirections.actionRemoveDropletsFragmentToCapture(true))
            }
        }
    }
}
