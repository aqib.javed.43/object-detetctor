package com.vessel.app.checkin.weekly

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.PlanRecord
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.util.extensions.getDateBefore
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*
import kotlin.collections.ArrayList

class WeeklyCheckInViewModel @ViewModelInject constructor(
    val state: WeeklyCheckInState,
    @Assisted savedStateHandle: SavedStateHandle,
    private val planManager: PlanManager,
    private val resourceProvider: ResourceRepository,
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    val today: Date = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
    fun dismiss() {
        navigateBack()
    }

    init {
        getWeeklyPlans()
    }
    fun takeTest() {
        state.navigateTo.value = HomeNavGraphDirections.globalActionToPreTest()
    }

    private fun getWeeklyPlans() {
        viewModelScope.launch() {
            showLoading()
            planManager.getPlansByDateRange(today.getDateBefore(Constants.WEEKLY).formatDayDate(), today.formatDayDate())
                .onSuccess { items ->
                    processPlans(items)
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun processPlans(items: List<PlanData>) {
        Log.d(TAG, "processPlans: $items")
        val plansList: ArrayList<PlanRecord> = arrayListOf()
        for (item in items) {
            item.plan_records?.let {
                plansList.addAll(it)
            }
        }
        val activityCount = items.count {
            it.program_id != null || it.goals?.firstOrNull { record ->
                record.impact ?: 0 >= 2
            } != null
        }
        val completeCount = plansList.count {
            it.completed == true
        }
        if (activityCount != 0) {
            state.progress.value = (
                (
                    BigDecimal(completeCount).divide(BigDecimal(activityCount), 2).setScale(2)
                        .multiply(
                            BigDecimal(100)
                        )
                    )

                ).toInt()
        }
        state.status.value = "$completeCount/$activityCount"
    }
    companion object {
        const val TAG = "WeeklyCheckInViewModel"
    }
}
