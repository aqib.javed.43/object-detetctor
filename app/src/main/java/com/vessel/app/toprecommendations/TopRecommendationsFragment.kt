package com.vessel.app.toprecommendations

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_top_recommendation.*

@AndroidEntryPoint
class TopRecommendationsFragment : BaseFragment<TopRecommendationsViewModel>() {
    override val viewModel: TopRecommendationsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_top_recommendation

    private val recommendationsSelectAdapter by lazy { RecommendationsSelectAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        recommendationList.layoutManager =
            GridLayoutManager(context, Constants.SPAN_COUNT_GOAL_GRID)
        recommendationList.adapter = recommendationsSelectAdapter
        recommendationList.itemAnimator = null
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(items) {
                recommendationsSelectAdapter.submitList(it)
            }
        }
    }
}
