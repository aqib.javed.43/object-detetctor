package com.vessel.app.views.dailyview

import android.content.Context
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.IntRange
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import androidx.core.view.isVisible
import com.vessel.app.R
import com.vessel.app.databinding.WidgetDailyStepperBinding
import com.vessel.app.plan.model.DayPlanProgressItem
import com.vessel.app.util.extensions.afterMeasured
import kotlinx.android.synthetic.main.widget_daily_stepper.view.*
import java.util.*

class DailyStepperWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var onDateChange: ((date: Date) -> Unit)? = null
    private var items: List<DayPlanProgressItem> = arrayListOf()
    private var index: Int = 0
    private lateinit var stepsList: MutableList<DailyStepperUiModel>
    private lateinit var binding: WidgetDailyStepperBinding
    private lateinit var adapter: DailyStepperAdapter
    private var stepsCount = 0
    private var activePosition = 0
    private var inActiveIcon = R.drawable.ic_step_inactive

    init {
        initView(context)
        obtainStyledAttributes(context, attrs, defStyleAttr)
    }

    private fun obtainStyledAttributes(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        context.withStyledAttributes(
            set = attrs,
            attrs = R.styleable.StepperView,
            defStyleAttr = defStyleAttr
        ) {
            getString(R.styleable.StepperView_progressHint)?.let { hint ->
                setProgressHint(hint)
            }
            inActiveIcon =
                getResourceId(R.styleable.StepperView_inActiveIcon, R.drawable.ic_step_inactive)
        }
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetDailyStepperBinding.inflate(layoutInflater, this, true)

        adapter = DailyStepperAdapter()
        binding.recyclerView.adapter = adapter
        binding.ivNext.setOnClickListener {
            index -= 1
            updateData()
        }
        binding.ivPrevious.setOnClickListener {
            index += 1
            updateData()
        }
    }

    private fun calculateItemWidth(screenWidth: Int): Int {
        val stepsCount = if (stepsList.size == 0) {
            1
        } else {
            stepsList.size
        }

        return screenWidth / stepsCount
    }

    fun setStepperAsCompleted() {
        stepsList.forEach {
            it.isCompleted = true
        }
        adapter.notifyDataSetChanged()
        setCompleteHint()
    }

    fun getStepCount() = stepsCount
    fun setStepsCount(@IntRange(from = 1) stepsCount: Int, activePosition: Int) {
        stepsList = mutableListOf()
        this.stepsCount = stepsCount
        createStepsList(stepsCount, activePosition)
    }

    fun getActiveStep() = activePosition
    fun setActiveStep(activePosition: Int) {
        this.activePosition = activePosition
        stepsList.forEachIndexed { index, item ->
            item.isCompleted = index <= activePosition - 1
        }
        adapter.notifyDataSetChanged()
        setCompleteHint()
        setProgress(adapter.itemWidth.times(activePosition))
    }

    private fun setProgress(progress: Int) {
        android.os.Handler(Looper.getMainLooper()).post {
            binding.progress.setProgress(progress, true)
        }
    }

    fun getProgressHint() = binding.stepperHintLabel.text.toString()

    fun setProgressHint(hint: String) {
        binding.stepperHintLabel.text = hint
    }

    fun setScheduleCompleteHint() {
        binding.stepperCompleteHintLabel.text = context.getString(
            R.string.plan_schedule_complete_hint,
            stepsList.filter { it.isCompleted }.size,
            stepsList.size
        )
    }

    private fun setCompleteHint() {
        if (stepsList.isEmpty()) {
            binding.stepperCompleteHintLabel.text = context.getString(R.string.no_activities_today)
        } else {
            binding.stepperCompleteHintLabel.text = context.getString(
                R.string.plan_complete_hint,
                stepsList.filter { it.isCompleted }.size,
                stepsList.size
            )
        }
    }

    private fun createStepsList(stepsCount: Int, activePosition: Int) {
        stepsList = mutableListOf()

        for (position in 0 until stepsCount) {
            stepsList.add(
                DailyStepperUiModel(
                    stepNum = (position).toString(),
                    isCompleted = activePosition == position,
                    inActiveIcon = inActiveIcon
                )
            )
        }

        afterMeasured {
            val screenWidth = binding.recyclerView.measuredWidth
            binding.progress.max = screenWidth
            adapter = DailyStepperAdapter()
            binding.recyclerView.adapter = adapter

            val itemWidth = calculateItemWidth(screenWidth)
            adapter.setItemWidth(itemWidth, stepsList.size)
            adapter.submitList(stepsList)
            adapter.notifyDataSetChanged()
            binding.recyclerView.isVisible = stepsList.size <= MAX_STEPS_COUNT
            setProgress(itemWidth.times(activePosition))
        }
    }

    fun setData(items: List<DayPlanProgressItem>) {
        index = 0
        this.items = items
        updateData()
    }

    fun updateData() {
        if (items.isNullOrEmpty()) return
        val item = items[index]
        setStepsCount(item.stepsCount, item.currentStep)

        setActiveStep(item.currentStep)
        if (item.title != getProgressHint()) {
            setProgressHint(item.title)
        }
        updateView()
        onDateChange?.invoke(item.date)
    }
    fun setDate(date: Date) {
        if (items.isNullOrEmpty()) return
        items.mapIndexed { index, dayPlanProgressItem ->
            if (dayPlanProgressItem.date == date) this.index = index
        }
        val item = items[index]
        setStepsCount(item.stepsCount, item.currentStep)

        setActiveStep(item.currentStep)
        if (item.title != getProgressHint()) {
            setProgressHint(item.title)
        }
        updateView()
    }
    private fun updateView() {
        ivNext.isEnabled = index > 0
        ivPrevious.isEnabled = index < items.size - 1
    }

    fun showOrHideProgress(isShow: Boolean) {
        binding.progressLayout.visibility = if (isShow) View.VISIBLE else View.GONE
    }

    companion object {
        private const val MAX_STEPS_COUNT = 10
    }
}
