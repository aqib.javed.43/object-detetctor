package com.vessel.app.filterfoodplandialog.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FilterAndSortDialogOptions(
    val categories: MutableList<FoodCategory> = mutableListOf(),
    var sortOptions: SortOptions = SortOptions.RECOMMENDED,
    val nutrients: MutableList<FilterNutrient> = mutableListOf()
) : Parcelable
