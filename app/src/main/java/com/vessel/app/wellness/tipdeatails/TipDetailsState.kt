package com.vessel.app.wellness.tipdeatails

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.activityreview.model.RecommendationReview
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.model.Tip
import javax.inject.Inject

class TipDetailsState @Inject constructor(val passedTip: Tip) {

    val tip = MutableLiveData<Tip>(passedTip)
    val ownItemTip = MutableLiveData(false)
    val plans = MutableLiveData<List<PlanData>>()
    val removeReminder = LiveEvent<List<PlanData>>()
    val navigateTo = LiveEvent<NavDirections>()
    val reviews = MutableLiveData<List<RecommendationReview>>()
    operator fun invoke(block: TipDetailsState.() -> Unit) = apply(block)
}
