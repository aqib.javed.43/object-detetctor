package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.vessel.app.R
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.*

open class AuthAppButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {
    init {
        inflate(context, R.layout.auth_app_button_view_group, this)
    }

    fun setText(text: String) {
        button.text = text
    }

    fun setText(@StringRes stringResId: Int?) {
        stringResId?.let { button.setText(it) } ?: run { button.text = null }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        button.isEnabled = enabled
    }

    override fun setOnClickListener(l: OnClickListener?) {
        button.setOnClickListener(l)
    }
}

class WhiteAuthAppButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AuthAppButtonViewGroup(context, attrs, defStyle) {
    init {
        dotsImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_black_button_dots))
        button.setTextColor(ContextCompat.getColor(context, R.color.darkText))
        blurView.setOverlayColor(ContextCompat.getColor(context, R.color.whiteAlpha40))
    }
}

class WhiteTakePhotoManuallyAuthAppButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AuthAppButtonViewGroup(context, attrs, defStyle) {
    init {
        dotsImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_black_button_dots_take_photo))
        button.setTextColor(ContextCompat.getColor(context, R.color.black))
        blurView.setOverlayColor(ContextCompat.getColor(context, R.color.whiteAlpha70))
    }
}

class BlackAuthAppButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AuthAppButtonViewGroup(context, attrs, defStyle) {
    private val enabledColor by lazy { ContextCompat.getColor(context, R.color.blackAlpha70) }
    private val disabledColor by lazy { ContextCompat.getColor(context, R.color.gray) }

    init {
        dotsImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_white_button_dots))
        button.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        blurView.setOverlayColor(enabledColor)
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        blurView.setOverlayColor(if (enabled) enabledColor else disabledColor)
    }
}

class BlackWithoutOverlayAppButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AuthAppButtonViewGroup(context, attrs, defStyle) {
    private val enabledColor by lazy { ContextCompat.getColor(context, R.color.black) }

    init {
        dotsImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_white_button_dots))
        button.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        blurView.setOverlayColor(enabledColor)
    }
}
