package com.vessel.app.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View

class BezierView(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {
    companion object {
        const val LINE_WIDTH: Float = 3F
        val TAG = BezierView::class.java.simpleName
    }

    var drawFromEnd: Boolean = false
    private var canvas: Canvas? = null

    // paints
    private val fillPaint = Paint()
        .apply {
            strokeWidth = LINE_WIDTH
            style = Paint.Style.FILL
            isAntiAlias = true
        }
    private val strokePaint = Paint()
        .apply {
            color = Color.GREEN
            strokeWidth = LINE_WIDTH
            style = Paint.Style.STROKE
            isAntiAlias = true
        }
    private val dashPaint = Paint()
        .apply {
            color = Color.GRAY
            strokeWidth = LINE_WIDTH
            style = Paint.Style.STROKE
            pathEffect = DashPathEffect(floatArrayOf(10f, 10f), 0f)
            isAntiAlias = true
        }

    // Initially allocate Points and Paths to prevent heavy allocations inside onDraw
    var startPoint: PointF? = null // fixed
    var endPointList: List<PointF>? = null
    var startPointList: List<PointF>? = null
    var endPoint: PointF? = null
    var middlePoint: PointF = PointF(0f, 0f)
    private val bezierPath = Path()

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        this.canvas = canvas
        if (drawFromEnd) drawLinesWithEndPoint()
        else drawLinesWithStartPoint()
    }

    fun drawLinesWithEndPoint() {
        if (endPoint == null || this.startPointList == null) return
        middlePoint = findMiddlePoint(endPoint!!, startPointList!!)

        for (startP in this.startPointList!!) {

            canvas?.let {
                drawBezier(it, startP, endPoint!!)
            }
        }
    }

    fun drawLinesWithStartPoint() {
        if (startPoint == null || this.endPointList == null) return
        middlePoint = findMiddlePoint(startPoint!!, endPointList!!)
        for (endP in this.endPointList!!) {
            canvas?.let {
                drawBezier(it, startPoint!!, endP)
            }
        }
    }

    private fun findMiddlePoint(startPoint: PointF, endPointList: List<PointF>): PointF {
        Log.d(TAG, "findMiddlePoint: $startPoint -- $endPointList")
        val highPoint: PointF = PointF(0f, 0f)
        val totalPoint: ArrayList<PointF> = arrayListOf()
        totalPoint.add(startPoint)
        totalPoint.addAll(endPointList)
        totalPoint.minByOrNull {
            it.y
        }?.let {
            highPoint.y = if (highPoint.y - width / 2 > 10) highPoint.y - width / 2 else highPoint.y - 10
        }

        return highPoint
    }

    /** draw the line with bezier coordinates **/
    private fun drawBezier(canvas: Canvas, p1: PointF, p2: PointF) {
        bezierPath.apply {
            reset()
            moveTo(p1.x, p1.y)
            cubicTo(
                p1.x,
                p1.y + width - p2.x,
                p1.x,
                middlePoint.y - width + p2.x,
                p2.x,
                p2.y - width + p2.x
            )
            if (p2.x < width) { // set end points
                postInvalidateDelayed(15)
                p2.x += 20
            }
            canvas.drawPath(bezierPath, strokePaint)
        }
    }
}
