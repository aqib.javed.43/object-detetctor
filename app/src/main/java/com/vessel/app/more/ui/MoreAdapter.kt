package com.vessel.app.more.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.more.model.MoreModel

class MoreAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<MoreModel>() {
    override fun getLayout(position: Int) = R.layout.item_more

    override fun getHandler(position: Int): OnActionHandler = handler

    interface OnActionHandler {
        fun onMoreItemClicked(moreModel: MoreModel)
    }
}
