package com.vessel.app.foodrecommendation.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodrecommendation.model.FoodRecommendationSelectModel

class FoodRecommendationSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<FoodRecommendationSelectModel>({ oldItem, newItem ->
        oldItem.id == newItem.id
    }) {
    override fun getLayout(position: Int) = R.layout.item_food_recommendation_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onFoodSelectClicked(item: FoodRecommendationSelectModel)
    }
}
