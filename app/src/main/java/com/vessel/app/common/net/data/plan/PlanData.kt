package com.vessel.app.common.net.data.plan

import android.icu.text.NumberFormat
import android.os.Parcelable
import com.squareup.moshi.JsonClass
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.common.model.data.Reagent
import com.vessel.app.common.net.data.Food
import com.vessel.app.common.net.data.ProgramData
import com.vessel.app.common.net.data.SupplementData
import com.vessel.app.common.net.data.TipData
import com.vessel.app.planreminderdialog.model.ReminderItemUiModel
import com.vessel.app.util.extensions.toCalender
import com.vessel.app.util.extensions.toCurrentWeekDate
import com.vessel.app.views.weekdays.AppDayOfWeek
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
@JsonClass(generateAdapter = true)
data class PlanData(
    val id: Int? = null,
    val food_id: Int? = null,
    val food: Food? = null,
    val notification_enabled: Boolean? = null,
    val is_supplements: Boolean? = null,
    val tip_id: Int? = null,
    val tip: TipData? = null,
    val plan_records: List<PlanRecord>? = null,
    val supplements: List<SupplementData>? = null,
    val goals: List<GoalRecord>? = null,
    val subGoals: List<SubGoalRecord>? = null,
    val badHabits: List<BadHabitRecord>? = null,
    val time_of_day: String? = null,
    val day_of_week: List<Int>? = null,
    val program_dates: List<String>? = null,
    val program: ProgramData? = null,
    val multiple: Int? = null,
    val contact_id: Int? = null,
    val program_id: Int? = null,
    val program_days: List<Int>? = null,
    val reagent_lifestyle_recommendation_id: Int? = null,
    val reagent_lifestyle_recommendation: ReagentLifestyleRecommendation? = null,
) : Parcelable {
    @IgnoredOnParcel
    var isPlanChecked = false

    fun isHydrationPlan(): Boolean {
        return reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null && reagent_lifestyle_recommendation.lifestyle_recommendation_id == Constants.HYDRATION_PLAN_ID
    }

    fun isTestingPlan(): Boolean {
        return reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null && reagent_lifestyle_recommendation.lifestyle_recommendation_id == Constants.TEST_PLAN_ID
    }

    fun isStressPlan(): Boolean {
        return reagent_lifestyle_recommendation?.id != null && !isTestingPlan() && !isHydrationPlan()
    }

    fun isTip(): Boolean {
        return tip_id != null && tip != null
    }

    fun isSupplementPlan(): Boolean = is_supplements == true

    fun isFoodPlan(): Boolean = food != null && food_id != null

    fun getPlanGoals(): String {
        return if (isTip().not() && goals.isNullOrEmpty().not()) {
            "Goals: ${goals.orEmpty().joinToString { it.name }}"
        } else
            ""
    }

    fun getDisplayName(): String {
        return when {
            isFoodPlan() -> {
                food?.food_title.orEmpty()
            }
            isStressPlan() || isTestingPlan() || isHydrationPlan() -> {
                reagent_lifestyle_recommendation?.activity_name.orEmpty()
            }
            isSupplementPlan() -> {
                "Supplements"
            }
            isTip() -> {
                tip?.title.orEmpty()
            }
            else -> {
                ""
            }
        }
    }

    fun getDescription(): String {
        return when {
            isFoodPlan() -> {
                "${
                NumberFormat.getInstance().format(getQuantityScale() * (multiple ?: 1))
                } ${food?.serving_unit}"
            }
            isStressPlan() || isTestingPlan() || isHydrationPlan() -> {
                listOfNotNull(
                    "${
                    NumberFormat.getInstance().format(getQuantityScale() * (multiple ?: 1))
                    } ${reagent_lifestyle_recommendation?.unit}",
                    if (reagent_lifestyle_recommendation?.frequency.isNullOrEmpty()) null else reagent_lifestyle_recommendation?.frequency
                ).joinToString()
            }
            isSupplementPlan() -> {
                "Daily"
            }
            isTip() -> {
                tip?.frequency ?: ""
            }
            else -> {
                ""
            }
        }
    }

    fun getPlanQuantityWithUnit(): String {
        return when {
            isFoodPlan() -> {
                "${
                NumberFormat.getInstance().format(getQuantityScale() * (multiple ?: 1))
                } ${food?.serving_unit}"
            }
            isStressPlan() || isTestingPlan() || isHydrationPlan() -> {
                "${
                NumberFormat.getInstance().format(getQuantityScale() * (multiple ?: 1))
                } ${reagent_lifestyle_recommendation?.unit}"
            }
            isSupplementPlan() -> {
                "Daily"
            }
            else -> {
                ""
            }
        }
    }

    fun getDate() = if (time_of_day?.isNotEmpty() == true) {
        Constants.SIMPLE_TIME_24_HOUR_FORMAT.parse(time_of_day)
    } else {
        null
    }

    fun getTimeFormattedWithMeridian(): String? {
        return getDate()?.let {
            Constants.SIMPLE_TIME_12_HOUR_FORMAT.format(it).orEmpty()
        }
    }

    fun getTimeFormattedAsNumber(): Int {
        val date = getDate()?.let {
            SimpleDateFormat("HHmm", Locale.US).format(it).orEmpty()
        }.orEmpty()

        return if (date.isEmpty()) 0 else date.toInt()
    }

    fun getToDoImageUrl(): String {
        return when {
            isFoodPlan() -> {
                food?.image_url.orEmpty()
            }
            isStressPlan() || isTestingPlan() || isHydrationPlan() -> {
                reagent_lifestyle_recommendation?.image_url.orEmpty()
            }
            isTip() -> {
                tip?.image_url.orEmpty()
            }
            else -> ""
        }
    }

    fun getToDoImageBackground(): Int {
        return if (isSupplementPlan()) R.drawable.ic_supplement_red
        else 0
    }

    fun getPlanType(): PlanType {
        return when {
            isSupplementPlan() -> PlanType.SupplementPlan
            isHydrationPlan() -> PlanType.WaterPlan
            isTestingPlan() -> PlanType.TestPlan
            isFoodPlan() -> PlanType.FoodPlan
            isTip() -> PlanType.TIP
            else -> PlanType.StressPlan
        }
    }

    fun getQuantityUnit(): String {
        return if (isFoodPlan()) {
            food?.serving_unit.orEmpty()
        } else if (isStressPlan() || isTestingPlan() || isHydrationPlan()) {
            reagent_lifestyle_recommendation?.unit.orEmpty()
        } else ""
    }

    fun getQuantityScale(): Double {
        return if (isFoodPlan()) {
            food?.serving_quantity ?: 0.toDouble()
        } else if (isStressPlan() || isTestingPlan() || isHydrationPlan()) {
            reagent_lifestyle_recommendation?.quantity?.toDouble() ?: 0.toDouble()
        } else 0.toDouble()
    }

    fun getQuantityMaxMultiple(): Int {
        return if (isSupplementPlan() || isTip()) 1 else 3
    }

    fun getDaysOfWeek(): List<Int> {
        var splitDays = mutableListOf<Int>()
        if (!program_dates.isNullOrEmpty()) {
            program_dates.onEach {
                val programDay = Constants.DAY_DATE_FORMAT.parse(it)!!.toCalender()
                val dayOfWeek =
                    AppDayOfWeek.getAppDayOfWeekByCalenderDayOfWeek(programDay.get(Calendar.DAY_OF_WEEK))
                splitDays.add(dayOfWeek.value)
            }
        } else {
            splitDays = day_of_week.orEmpty().toMutableList()
            if (splitDays.isNullOrEmpty()) {
                splitDays = Constants.WEEK_DAYS.toMutableList()
            }
        }
        return splitDays
    }

    fun getImproves(): String {
        if (isTip()) {
            return tip?.getDisplayedDescription() ?: ""
        } else if (isHydrationPlan()) {
            return "Improves: Hydration"
        } else if (isStressPlan()) {
            return "Improves: Cortisol"
        } else if (isFoodPlan()) {
            if (food?.getFoodImproves().isNullOrEmpty().not()) {
                return "Improves: ${food?.getFoodImproves()}"
            }
        } else if (isSupplementPlan()) {
            if (supplements.orEmpty().joinToString { it.name }.isEmpty().not()) {
                return "Improves: ${supplements.orEmpty().joinToString { it.name }}"
            }
        }
        return ""
    }
    fun getPlanDescription(): String {
        if (isTip()) {
            return tip?.description.toString()
        } else if (isHydrationPlan()) {
            return "Improves: Hydration"
        } else if (isStressPlan()) {
            return "Improves: Cortisol"
        } else if (isFoodPlan()) {
            if (food?.getFoodImproves().isNullOrEmpty().not()) {
                return "Improves: ${food?.getFoodImproves()}"
            }
        } else if (isSupplementPlan()) {
            if (supplements.orEmpty().joinToString { it.name }.isEmpty().not()) {
                return "Improves: ${supplements.orEmpty().joinToString { it.name }}"
            }
        }
        return ""
    }
    fun getNonCompletedDays(): MutableList<Int> {
        val nonCompletedDays = mutableListOf<Int>()
        getDaysOfWeek().forEach { day ->
            val dayDate = Constants.DAY_DATE_FORMAT.format(day.toCurrentWeekDate())
            val completedRecord = plan_records.orEmpty().firstOrNull { it.date == dayDate }
            if (completedRecord == null || completedRecord.completed == false) {
                nonCompletedDays.add(day)
            }
        }
        return nonCompletedDays
    }

    fun hasReminderHint() = (
        (isHydrationPlan() || isStressPlan() || isTestingPlan()) &&
            reagent_lifestyle_recommendation?.frequency.isNullOrEmpty().not()
        ) || isSupplementPlan()

    fun getReminderHint() =
        if ((isHydrationPlan() || isStressPlan() || isTestingPlan()) && reagent_lifestyle_recommendation?.frequency.isNullOrEmpty()
            .not()
        ) {
            "Recommended frequency: ${reagent_lifestyle_recommendation?.frequency}"
        } else if (isSupplementPlan()) {
            "Recommended frequency: Daily"
        } else if (isTip()) {
            tip?.frequency.orEmpty()
        } else ""
}

@JsonClass(generateAdapter = true)
@Parcelize
data class ReagentLifestyleRecommendation(
    val id: Int,
    val lifestyle_recommendation_id: Int,
    val quantity: Float,
    val reagent_bucket_id: Int? = null,
    val activity_name: String? = null,
    val frequency: String? = null,
    val image_url: String? = null,
    val unit: String? = null,
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
data class PlanRecord(
    val id: Int? = null,
    val contact_id: Int? = null,
    val date: String? = null,
    val completed: Boolean? = null,
    val is_deleted: Boolean? = null
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
data class GoalRecord(
    val id: Int,
    val name: String,
    val image_small_url: String? = null,
    val image_medium_url: String? = null,
    val image_large_url: String? = null,
    val reagents: List<Reagent>? = null,
    val tip_count: Int = 0,
    val impact: Int? = null

) : Parcelable

fun PlanData.toReminderUiModel() = ReminderItemUiModel(
    time_of_day,
    getDaysOfWeek().toMutableList(),
    this,
    isTestingPlan = isTestingPlan()
)
@JsonClass(generateAdapter = true)
@Parcelize
data class SubGoalRecord(
    val id: Int,
    val rank: String,
    val name: String,
    val goal_id: Int,
    val description: String?,
    val symptom_name: String?,
    val image_small_url: String? = null,
    val image_medium_url: String? = null,
    val image_large_url: String? = null,
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
data class BadHabitRecord(
    val id: Int,
    val title: String,
    val is_active: Boolean
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
data class OnBoardingProgramRecord(
    val id: Int
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
data class EssentialRecord(
    val id: Int? = null,
    val contact_id: Int? = null,
    val date: String? = null,
    val completed: Boolean? = null,
    val program_day: Int,
    val essential_id: Int
) : Parcelable
