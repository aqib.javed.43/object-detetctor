package com.vessel.app.programs.update

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ProgramUpdateState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: ProgramUpdateState.() -> Unit) = apply(block)
}
