package com.vessel.app.activityreview.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.vessel.app.R
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
data class ReviewResponse(
    val reviews: List<RecommendationReview>? = arrayListOf()
)
@JsonClass(generateAdapter = true)
data class RecommendationReview(
    var updated_by: Int? = null,
    var created_at: String? = null,
    var food_id: Int? = null,
    var updated_at: String? = null,
    @Json(name = "like_status")
    val likeStatus: Boolean? = null,
    var review: String? = null,
    @Json(name = "tip_id")
    val tipID: Long? = null,
    var myContact: Int? = null,
    var contact: ReviewContact? = null,
) {
    fun isMyReview(): Boolean {
        return contact?.id == myContact
    }

    fun getIconBackground() = if (likeStatus == true)
        R.drawable.ic_like_review
    else
        R.drawable.ic_dislike_review
}
@JsonClass(generateAdapter = true)
data class ReviewContact(
    var id: Int? = null,
    var last_name: String? = null,
    var location_description: String? = null,
    var first_name: String? = null
)

@Parcelize
data class ActivityReviewItem(
    var likeStatus: LikeStatus? = LikeStatus.NO_ACTION,
    var image: String? = null,
    var title: String? = null,
    var record_id: Int? = 0,
    var category: String? = null,
    var programId: Int? = 0
) : Parcelable
