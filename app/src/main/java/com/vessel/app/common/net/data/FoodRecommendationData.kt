package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FoodRecommendationData(
    val usda_ndb_number: Int? = 0,
    val name: String? = "",
    val image_url: String? = "",
    val b9: Float? = 0f,
    val id: Int?,
    val b7: Float? = 0f,
    val magnesium: Float? = 0f,
    val vitamin_c: Float? = 0f,
    val serving_size: FoodRecommendationServingSizeData? = null,
    val like_status: String? = null,
    val total_likes: Int? = null,
    val dislikes_count: Int? = null
)

@JsonClass(generateAdapter = true)
data class FoodRecommendationServingSizeData(
    val quantity: Float? = 0f,
    val display_quantity: String? = "",
    val unit: String? = ""
)
