package com.vessel.app.ownplanitem.steptwo

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import org.json.JSONArray
import javax.inject.Inject

class CreateOwnPlanItemStepTwoState @Inject constructor(
    val planItemTitle: String,
    val planItemFrequency: String,
    private val remoteImagesUrls: String
) {
    val imagesUrls: List<String> = buildImageUrls(remoteImagesUrls)
    var currentSelectedImageUrl: String = imagesUrls.first()
    val navigateTo = LiveEvent<NavDirections>()

    private fun buildImageUrls(remoteImagesUrls: String): List<String> {
        val remoteImagesArray = JSONArray(remoteImagesUrls)
        val imageUrls = mutableListOf<String>()
        for (i in 0 until remoteImagesArray.length()) {
            imageUrls.add(remoteImagesArray.getString(i))
        }
        return imageUrls.toList()
    }

    fun setCurrentSelectedImageUrl(index: Int) {
        if (index >= 0 && index < imagesUrls.size)
            currentSelectedImageUrl = imagesUrls[index]
    }

    operator fun invoke(block: CreateOwnPlanItemStepTwoState.() -> Unit) = apply(block)
}
