package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isInvisible
import com.vessel.app.R
import com.vessel.app.util.extensions.formatToString
import kotlinx.android.synthetic.main.widget_quantity.view.*

class QuantityWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private var minQuantity: Float = 0f
    private var maxQuantity: Float = 0f
    private var quantity: Float = 0f
    private var quantityUnit: Float = 1f
    private var quantityDescription: String = ""
    private var subtractClickListener: ((Float) -> Unit)? = null
    private var addClickListener: ((Float) -> Unit)? = null

    init {
        inflate(context, R.layout.widget_quantity, this)
        initView()
    }

    private fun initView() {
        plusButton.setOnClickListener {
            quantity += quantityUnit
            addClickListener?.invoke(quantity)
            updateDescription()
        }

        subtractButton.setOnClickListener {
            if (quantity <= minQuantity) return@setOnClickListener
            quantity -= quantityUnit
            subtractClickListener?.invoke(quantity)
            updateDescription()
        }

        updateDescription()
    }

    private fun updateDescription() {
        description.text = "${quantity.formatToString()} $quantityDescription"
        subtractButton.isInvisible = (quantity > minQuantity).not()
        plusButton.isInvisible = (quantity < maxQuantity).not()
    }

    fun setMaxQuantity(quantity: Float) {
        maxQuantity = quantity
        updateDescription()
    }

    fun setMinQuantity(quantity: Float) {
        minQuantity = quantity
        updateDescription()
    }

    fun setQuantityDescription(text: String) {
        quantityDescription = text
        updateDescription()
    }

    fun setQuantityUnit(unit: Float) {
        quantityUnit = unit
        updateDescription()
    }

    fun setQuantity(currentQuantity: Float) {
        quantity = currentQuantity
        updateDescription()
    }

    fun getQuantity() = quantity

    fun setSubtractClickListener(listener: (Float) -> Unit) {
        subtractClickListener = listener
    }

    fun setAddClickListener(listener: (Float) -> Unit) {
        addClickListener = listener
    }
}
