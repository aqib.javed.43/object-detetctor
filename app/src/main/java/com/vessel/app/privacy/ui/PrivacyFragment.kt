package com.vessel.app.privacy.ui

import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.privacy.PrivacyViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PrivacyFragment : BaseFragment<PrivacyViewModel>() {
    override val viewModel: PrivacyViewModel by viewModels()
    override val layoutResId = R.layout.fragment_privacy
}
