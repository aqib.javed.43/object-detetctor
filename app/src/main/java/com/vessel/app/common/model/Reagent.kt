// TODO remove this file after verify the bucket changes

// package com.vessel.app.common.model
//
// import android.os.Parcelable
// import androidx.annotation.DrawableRes
// import androidx.annotation.StringRes
// import com.vessel.app.R
// import kotlinx.android.parcel.Parcelize
//
// sealed class Reagent(
//    val id: Int,
//    val apiName: String,
//    @StringRes val displayName: Int,
//    @StringRes val unit: Int,
//    @DrawableRes val headerImage: Int,
//    val queryString: String,
//    val ranges: List<ReagentRange> = listOf(),
// ) {
//    val lowerBound: Float = ranges.first { it.reagentType == ReagentLevel.Good }.from
//    val upperBound: Float = ranges.last { it.reagentType == ReagentLevel.Good }.to
//    val min: Float = ranges.first().from
//    val max: Float = ranges.last().to
//
//    object PH : Reagent(
//        1, "", R.string.ph, 0, R.drawable.reagent_header_1, "",
//        listOf(
//            ReagentRange(4.5f, 6.25f, R.string.reagent_low, R.color.linen, R.string.ph_low_title, R.string.ph_low_description, ReagentLevel.Low, ReagentButtonAction.FOOD_SUPPLEMENT_PLAN),
//            ReagentRange(6.25f, 7.75f, R.string.reagent_good, R.color.primaryLight, R.string.ph_optimal_title, R.string.ph_optimal_description, ReagentLevel.Good, ReagentButtonAction.NONE),
//            ReagentRange(7.75f, 9f, R.string.reagent_high, R.color.linen, R.string.ph_high_title, R.string.ph_high_description, ReagentLevel.High, ReagentButtonAction.FOOD_SUPPLEMENT_PLAN)
//        )
//    )
//
//    object Hydration : Reagent(
//        2, "", R.string.hydration, R.string.empty, R.drawable.reagent_header_2, "",
//        listOf(
//            ReagentRange(1.034f, 1.028f, R.string.reagent_low, R.color.linen, R.string.hydration_low_title, R.string.hydration_low_description, ReagentLevel.Low, ReagentButtonAction.HYDRATION_PLAN),
//            ReagentRange(1.028f, 1.018f, R.string.reagent_low, R.color.linen, R.string.hydration_low_title, R.string.hydration_low_description, ReagentLevel.Low, ReagentButtonAction.HYDRATION_PLAN),
//            ReagentRange(1.018f, 1.008f, R.string.reagent_good, R.color.primaryLight, R.string.hydration_optimal_title, R.string.hydration_optimal_description, ReagentLevel.Good, ReagentButtonAction.NONE),
//            ReagentRange(1.008f, 1.003f, R.string.reagent_high, R.color.linen, R.string.hydration_high_title, R.string.hydration_high_description, ReagentLevel.High, ReagentButtonAction.NONE),
//            ReagentRange(1.003f, 0.999f, R.string.reagent_high, R.color.linen, R.string.hydration_high_title, R.string.hydration_high_description, ReagentLevel.High, ReagentButtonAction.NONE)
//        )
//    )
//
//    object Ketones : Reagent(
//        3, "", R.string.ketones, R.string.mmoL, R.drawable.reagent_header_3, "",
//        listOf(
//            ReagentRange(0f, 0.75f, R.string.reagent_low, R.color.primaryLight, R.string.ketones_low_title, R.string.ketones_low_description, ReagentLevel.Low, ReagentButtonAction.FOOD_PLAN),
//            ReagentRange(0.75f, 2f, R.string.reagent_low, R.color.primaryLight, R.string.ketones_low_title, R.string.ketones_low_description, ReagentLevel.Low, ReagentButtonAction.FOOD_PLAN),
//            ReagentRange(2f, 7f, R.string.reagent_moderate, R.color.linen, R.string.ketones_optimal_title, R.string.ketones_optimal_description, ReagentLevel.Good, ReagentButtonAction.NONE),
//            ReagentRange(7f, 10f, R.string.reagent_high, R.color.linen, R.string.ketones_high_title, R.string.ketones_high_description, ReagentLevel.High, ReagentButtonAction.FOOD_PLAN)
//        )
//    )
//
//    object VitaminC : Reagent(
//        4, "vitamin_c", R.string.vitamin_c, R.string.mgL, R.drawable.reagent_header_4, "vitaminc",
//        listOf(
//            ReagentRange(0f, 92f, R.string.reagent_low, R.color.linen, R.string.vitamin_c_low_title, R.string.vitamin_c_low_description, ReagentLevel.Low, ReagentButtonAction.FOOD_SUPPLEMENT_PLAN),
//            ReagentRange(92f, 240f, R.string.reagent_low, R.color.linen, R.string.vitamin_c_low_title, R.string.vitamin_c_low_description, ReagentLevel.Low, ReagentButtonAction.FOOD_SUPPLEMENT_PLAN),
//            ReagentRange(240f, 550f, R.string.reagent_good, R.color.primaryLight, R.string.vitamin_c_optimal_title, R.string.vitamin_c_optimal_description, ReagentLevel.Good, ReagentButtonAction.NONE),
//            ReagentRange(550f, 800f, R.string.reagent_good, R.color.primaryLight, R.string.vitamin_c_optimal_title, R.string.vitamin_c_optimal_description, ReagentLevel.Good, ReagentButtonAction.NONE),
//            ReagentRange(800f, 1000f, R.string.reagent_high, R.color.linen, R.string.vitamin_c_high_title, R.string.vitamin_c_high_description, ReagentLevel.High, ReagentButtonAction.NONE)
//        )
//    )
//
//    object Magnesium : Reagent(
//        5, "magnesium", R.string.magnesium, R.string.mg, R.drawable.reagent_header_5, "magnesiumglycinate",
//        listOf(
//            ReagentRange(0f, 200f, R.string.reagent_low, R.color.linen, R.string.magnesium_low_title, R.string.magnesium_low_description, ReagentLevel.Low, ReagentButtonAction.FOOD_SUPPLEMENT_PLAN),
//            ReagentRange(200f, 600f, R.string.reagent_good, R.color.primaryLight, R.string.magnesium_optimal_title, R.string.magnesium_optimal_description, ReagentLevel.Good, ReagentButtonAction.NONE),
//            ReagentRange(600f, 805f, R.string.reagent_high, R.color.linen, R.string.magnesium_high_title, R.string.magnesium_high_description, ReagentLevel.High, ReagentButtonAction.NONE)
//        )
//    )
//
//    object B9 : Reagent(
//        6, "b9", R.string.b9_folate, R.string.ugL, R.drawable.reagent_header_6, "vitaminb9",
//        listOf(
//            ReagentRange(0f, 20f, R.string.reagent_low, R.color.linen, R.string.b9_folate_low_title, R.string.b9_folate_low_description, ReagentLevel.Low, ReagentButtonAction.NONE),
//            ReagentRange(20f, 60f, R.string.reagent_good, R.color.primaryLight, R.string.b9_folate_optimal_title, R.string.b9_folate_optimal_description, ReagentLevel.Good, ReagentButtonAction.NONE),
//            ReagentRange(60f, 85f, R.string.reagent_high, R.color.linen, R.string.b9_folate_high_title, R.string.b9_folate_high_description, ReagentLevel.High, ReagentButtonAction.NONE)
//        )
//    )
//
//    object Cortisol : Reagent(
//        8, "", R.string.cortisol, R.string.ugL, R.drawable.reagent_header_8, "",
//        listOf(
//            ReagentRange(0f, 50f, R.string.reagent_low, R.color.linen, R.string.cortisol_low_title, R.string.cortisol_low_description, ReagentLevel.Low, ReagentButtonAction.NONE),
//            ReagentRange(50f, 150f, R.string.reagent_good, R.color.primaryLight, R.string.cortisol_optimal_title, R.string.cortisol_optimal_description, ReagentLevel.Good, ReagentButtonAction.NONE),
//            ReagentRange(150f, 405f, R.string.reagent_high, R.color.linen, R.string.cortisol_high_title, R.string.cortisol_high_description, ReagentLevel.High, ReagentButtonAction.NONE)
//        )
//    )
//
//    object B7 : Reagent(
//        11, "b7", R.string.b7_biotin, R.string.ugL, R.drawable.reagent_header_11, "biotinvitaminb7",
//        listOf(
//            ReagentRange(0f, 10f, R.string.reagent_low, R.color.linen, R.string.b7_biotin_low_title, R.string.b7_biotin_low_description, ReagentLevel.Low, ReagentButtonAction.NONE),
//            ReagentRange(10f, 30f, R.string.reagent_good, R.color.primaryLight, R.string.b7_biotin_optimal_title, R.string.b7_biotin_optimal_description, ReagentLevel.Good, ReagentButtonAction.NONE),
//            ReagentRange(30f, 50f, R.string.reagent_high, R.color.linen, R.string.b7_biotin_high_title, R.string.b7_biotin_high_description, ReagentLevel.High, ReagentButtonAction.NONE)
//        )
//    )
//
//    companion object {
//        fun fromId(id: Int) = when (id) {
//            PH.id -> PH
//            Hydration.id -> Hydration
//            Ketones.id -> Ketones
//            VitaminC.id -> VitaminC
//            Magnesium.id -> Magnesium
//            B9.id -> B9
//            Cortisol.id -> Cortisol
//            B7.id -> B7
//            else -> null
//        }
//
//        fun fromApiName(apiName: String) = when (apiName) {
//            VitaminC.apiName -> VitaminC
//            Magnesium.apiName -> Magnesium
//            B7.apiName -> B7
//            B9.apiName -> B9
//            else -> null
//        }
//    }
// }
//
// fun Reagent.isBetaTesting(): Boolean {
//    return when (this) {
//        is Reagent.Cortisol, Reagent.B9, Reagent.B7 -> true
//        else -> false
//    }
// }
//
// @Parcelize
// data class ReagentRange(
//    val from: Float,
//    val to: Float,
//    @StringRes val label: Int,
//    val color: Int,
//    @StringRes val hintTitle: Int,
//    @StringRes val hintDescription: Int,
//    val reagentType: ReagentLevel,
//    val rangeRecommendationAction: ReagentButtonAction = ReagentButtonAction.NONE
// ) : Parcelable
//
// enum class ReagentLevel {
//    Low,
//    Good,
//    High
// }
//
// enum class ReagentButtonAction {
//    FOOD_PLAN,
//    STRESS_RELIEF_PLAN,
//    FOOD_SUPPLEMENT_PLAN,
//    HYDRATION_PLAN,
//    NONE
// }
