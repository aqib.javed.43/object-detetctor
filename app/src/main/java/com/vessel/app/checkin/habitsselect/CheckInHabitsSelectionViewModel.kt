package com.vessel.app.checkin.habitsselect

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BadHabitManager
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.badhabitsselection.BadHabitSelect
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class CheckInHabitsSelectionViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    private val badHabitManager: BadHabitManager,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider),
    CheckInHabitAdapter.OnActionHandler,
    ToolbarHandler {
    val state = CheckInHabitsSelectState()
    var badHabitList: List<BadHabitSelect>? = null
    init {
        showLoading(true)
        val subGoals = goalsRepository.getUserSubGoal()
        fetchAllBadHabits(subGoals)
        val goalSelected = goalsRepository.getUserMainGoal()
        goalSelected?.let {
            setGoal(it)
        }
    }
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
    private fun fetchAllBadHabits(subGoalId: String?) {
        viewModelScope.launch {
            badHabitManager.fetchAllBadHabit(subGoalId)
                .onSuccess { badHabitData ->
                    state {
                        items.value = badHabitData.map {
                            BadHabitSelect(
                                it.id,
                                it.title,
                                false
                            )
                        }
                    }
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    fun onDoneClicked() {
        badHabitList = state.items.value.orEmpty()
            .filter { it.checked }
        badHabitList?.let {
            if (it.isEmpty()) return
            Constants.BADHABIT_SELECTS = it
            state.navigateTo.value = CheckInHabitSelectionFragmentDirections.actionCheckInHabitSelectionFragmentToTipsCheckInFragment()
        }
    }
    fun setGoal(goal: GoalSample) {
        Constants.GOALBUTTONS.firstOrNull {
            it.id == goal.id
        }?.let {
            state.selectedGoal.value = it
        }
    }
    override fun onHabitSelectClicked(item: BadHabitSelect) {
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, diet ->
                    if (diet.title == item.title) diet.copy(checked = !item.checked)
                    else diet
                }.toMutableList()
        }
    }
    fun onSkip(v: View) {
        Navigation.findNavController(v).popBackStack(R.id.homeScreenFragment, false)
    }
}
