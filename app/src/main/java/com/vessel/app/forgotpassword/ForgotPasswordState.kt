package com.vessel.app.forgotpassword

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class ForgotPasswordState @Inject constructor() {
    val userEmail = MutableLiveData("")

    operator fun invoke(block: ForgotPasswordState.() -> Unit) = apply(block)
}
