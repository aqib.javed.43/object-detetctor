package com.vessel.app.wellness.model

import android.os.Parcelable
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.common.model.Contact
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.extensions.divideToPercent
import com.vessel.app.wellness.RecommendationType
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class Tip(
    val id: Int,
    val title: String,
    val frequency: String?,
    val imageUrl: String?,
    val description: String = "",
    var likesCount: Int?,
    var dislikesCount: Int?,
    var likeStatus: LikeStatus,
    val isAddedToPlan: Boolean,
    val similar: List<Tip>? = null,
    val contact: Contact?,
    val sources: List<ScienceSource>?,
    val impactsGoals: List<GoalRecord>?,
    val currentGoalName: String? = null,
    val mainGoalId: Int?,
    val isChecked: Boolean = false,
) : Parcelable, IRecommendation {

    @Inject
    @IgnoredOnParcel
    lateinit var preferenceRepo: PreferencesRepository

    override fun getBackgroundImageUrl() = imageUrl

    override fun getDisplayedTitle() = title

    override fun getDisplayedFrequency() = frequency

    override fun getDisplayedDescription(): String {
        if (contact?.isVerified == true) {
            val mainGoalName = Constants.GOALS.firstOrNull { it.id == mainGoalId }?.name
            if (contactFullName == null && mainGoalName == null)
                return ""

            val result = StringBuilder("This is a recommendation")
            if (contactFullName.isNullOrEmpty().not()) {
                result.append(" from $contactFullName")
            }
            if (mainGoalName.isNullOrEmpty().not()) {
                result.append(" to help with $mainGoalName")
            }
            return result.toString()
        } else {
            return String()
        }
    }

    override fun hasSimilar() = similar.isNullOrEmpty().not()
    fun getLikesTotal(): String {
        return "$likesCount"
    }
    override fun getLikesTotalCount(): String {
        val percent = (likesCount ?: 0).divideToPercent((likesCount ?: 0) + (dislikesCount ?: 0))
        return "$percent %"
    }
    override fun getDislikesTotalCount(): String {
        val percent = (dislikesCount ?: 0).divideToPercent((likesCount ?: 0) + (dislikesCount ?: 0))
        return "$percent %"
    }
    fun getBackground(): Int {
        return if (likeStatus == LikeStatus.LIKE) getLikeBackground()
        else getDisLikeBackground()
    }
    override fun getLikeBackground() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.green_background_with_16_radius
    else
        R.drawable.gray_background_with_16_radius

    fun getDisLikeBackground() = if (likeStatus == LikeStatus.DISLIKE)
        R.drawable.red_rose_background
    else
        R.drawable.gray_background_with_16_radius

    override fun getThumbShapeIcon() =
        when (likeStatus) {
            LikeStatus.LIKE -> R.drawable.ic_like_review
            LikeStatus.DISLIKE -> R.drawable.ic_dislike_review
            else -> R.drawable.ic_outline_thumb_up
        }

    override fun getLikedStatus(): LikeStatus {
        return likeStatus
    }

    override fun getLikesCount(): Int {
        return likesCount ?: 0
    }
    override fun getDislikesCount(): Int {
        return dislikesCount ?: 0
    }
    override fun getButtonResId() = if (isAddedToPlan)
        R.string.remove_from_plan
    else
        R.string.add_to_plan

    override fun getType(): RecommendationType? = RecommendationType.Tip

    override fun getAddedToPlan() = isAddedToPlan

    @IgnoredOnParcel
    val isVerifiedContact = contact?.isVerified

    @IgnoredOnParcel
    val contactOccupation = contact?.occupation ?: ""

    @IgnoredOnParcel
    val contactFullName = contact?.fullName

    @IgnoredOnParcel
    val contactImageUrl = contact?.imageUrl

    override fun setLikesCount(count: Int) {
        likesCount = count
    }

    override fun setLikedStatus(status: LikeStatus) {
        likeStatus = status
    }
    fun getLikeIcon() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.ic_like_button
    else
        R.drawable.ic_thumb_up

    fun getDisLikeIcon() = if (likeStatus == LikeStatus.DISLIKE)
        R.drawable.ic_thumb_down_red
    else
        R.drawable.ic_thumb_down
}
