package com.vessel.app.cardchoices.model

import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.vessel.app.R

sealed class CardChoiceUIItem(@StringRes val title: Int, var checked: Boolean) {
    @ColorRes
    fun background() = if (checked)
        R.color.whiteAlpha70
    else
        R.color.whiteAlpha40

    abstract fun copy(cChecked: Boolean): CardChoiceUIItem
}

class GIFTED() : CardChoiceUIItem(R.string.gifted_card, false) {
    override fun copy(cChecked: Boolean) = GIFTED().apply {
        checked = cChecked
    }
}

class BOUGHT() : CardChoiceUIItem(R.string.bought_card, false) {
    override fun copy(cChecked: Boolean) = BOUGHT().apply {
        checked = cChecked
    }
}

class NO_CARD() : CardChoiceUIItem(R.string.no_test_card, false) {
    override fun copy(cChecked: Boolean) = NO_CARD().apply {
        checked = cChecked
    }
}
