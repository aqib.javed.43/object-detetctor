If your sodium is high, you should read the food labels/nutrition facts and try to limit your sodium intake to < 2,300mg per day (i.e., < 1 teaspoon of salt per day). Those people who are > 51 year of age, black, have high blood pressure, diabetes or chronic kidney disease should limit their intake to < 1500mg per day. When reading nutrition labels you should also be sure to pay attention to the number of servings you are eating. For instance if the label says “there are 400mg of sodium in  a 1-cup serving” and you are eating 2 cups (or 2 servings) then you are eating 2x the amount of sodium (i.e, 800mg).

You should try to avoid foods that are high in sodium, such as processed or pre-packaged foods. Here is a list of foods that are high in sodium:

Smoked, salted, and canned meat, fish, and poultry
Ham, bacon, hot dogs, and lunch meats
Hard and processed cheeses
Regular peanut butter
Crackers
Frozen prepared meals
Canned and dried soups, broths and veggies
Snacks, chips, and pretzels
Pickles and olives
Ketchup, mustard, steak sauce, soy sauce, salad dressings
Pre-seasoned rice, pasta, or other side dishes

Choose to eat foods that are low in sodium. Here is a list of foods low in sodium:
Fresh or frozen fruits and vegetables
Unsalted nuts or peanut butter
Dry beans or lentils, cooked without salt
Pasta, rice, or other grains made without salt
Whole-grain breads
Fish, meat, and poultry made without salt
Unsalted crackers or chips

Instead of adding salt to foods when cooking try adding different spices for flavoring, such as coriander, black pepper, nutmeg, parsley, cumin, cilantro, ginger, rosemary, marjoram, thyme, tarragon, garlic or onion powder, bay leaf, oregano, dry mustard, dill, lemon, lime or vinegar.

**References**

-  https://www.cardiosmart.org/~/media/Documents/Fact%20Sheets/en/zp3768.ashx
-  https://www.heart.org/-/media/data-import/downloadables/8/2/0/pe-abh-why-should-i-limit-sodium-ucm_300625.pdf


