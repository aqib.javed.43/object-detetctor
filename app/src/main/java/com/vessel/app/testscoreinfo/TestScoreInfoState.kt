package com.vessel.app.testscoreinfo

import androidx.lifecycle.MutableLiveData
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class TestScoreInfoState @Inject constructor(val wellnessScore: String) {
    val score = MutableLiveData(wellnessScore)
    val continueClicked = LiveEvent<Unit>()
    val navigateToPlan = LiveEvent<Unit>()
    val navigateToUpdatePlan = LiveEvent<Unit>()

    operator fun invoke(block: TestScoreInfoState.() -> Unit) = apply(block)
}
