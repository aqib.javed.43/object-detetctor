package com.vessel.app.wellness.net.service

import com.vessel.app.wellness.net.data.*
import retrofit2.http.*

interface TeamsService {
    @GET("/v2/social/feed/teamfeed/{id}")
    suspend fun fetchNewFeeds(@Path("id") id: Int): FeedResponseData
    @POST("/v2/social/feed/user/{contact_id}/follows/{user_id}")
    suspend fun followContact(@Path("contact_id") id: Int, @Path("user_id") userId: String): FeedActionResponseData
    @DELETE("/v2/social/feed/user/{contact_id}/follows/{user_id}")
    suspend fun unFollowContact(@Path("contact_id") id: Int, @Path("user_id") userId: String): FeedActionResponseData
    @POST("/v2/social/reaction")
    suspend fun reactionPost(@Body action: FeedReaction): FeedActionResponseData
    @POST("/v2/social/feed/reportfeed/post_report")
    suspend fun flagPost(@Body flag: FeedFlag): FeedActionResponseData
    @POST("/v2/social/feed/teamfeed/{main_goal_id}/")
    suspend fun createPoll(@Path("main_goal_id") goalId: Int, @Body createPollRequest: CreatePollRequest): FeedActionResponseData
}
