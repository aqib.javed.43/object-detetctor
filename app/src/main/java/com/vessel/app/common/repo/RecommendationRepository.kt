package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.FoodRecommendation
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.SampleRecommendations
import com.vessel.app.common.net.mapper.FoodRecommendationMapper
import com.vessel.app.common.net.mapper.RecommendationMapper
import com.vessel.app.common.net.service.RecommendationService
import com.vessel.app.wellness.net.data.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RecommendationRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val recommendationMapper: RecommendationMapper,
    private val recommendationService: RecommendationService,
    private val foodRecommendationMapper: FoodRecommendationMapper,
) : BaseNetworkRepository(moshi, loggingManager) {

    private var sampleRecommendationsData: SampleRecommendations? = null
    private var foodRecommendationsData: List<FoodRecommendation>? = null

    suspend fun getLatestSampleRecommendations(forceUpdate: Boolean = false): Result<SampleRecommendations, Nothing> {
        return if (forceUpdate || sampleRecommendationsData == null) {
            execute(
                recommendationMapper::map,
                {
                    recommendationService.getLatestSampleRecommendations()
                },
                {
                    sampleRecommendationsData = it
                }
            )
        } else {
            Result.Success(sampleRecommendationsData!!)
        }
    }
    suspend fun getFoodRecommendations(forceUpdate: Boolean = false, reagentId: Int): Result<List<FoodRecommendation>, Nothing> {
        return if (forceUpdate || foodRecommendationsData == null) {
            execute(
                foodRecommendationMapper::map,
                {
                    recommendationService.getFoodRecommendations(reagentId)
                },
                {
                    foodRecommendationsData = it.toMutableList()
                }
            )
        } else {
            Result.Success(foodRecommendationsData!!)
        }
    }
    suspend fun getLatestSampleFoodRecommendations(forceUpdate: Boolean = false): Result<List<FoodRecommendation>, Nothing> {
        return if (forceUpdate || foodRecommendationsData == null) {
            execute(
                foodRecommendationMapper::map,
                {
                    recommendationService.getLatestSampleFoodRecommendations()
                },
                {
                    foodRecommendationsData = it.toMutableList()
                }
            )
        } else {
            Result.Success(foodRecommendationsData!!)
        }
    }

    suspend fun sendLikeStatus(
        record_id: Int,
        category: String,
        likeStatus: Boolean,
    ) =
        executeWithErrorMessage {
            val likePayLoad =
                LikePayLoad(
                    record_id, category,
                    LikeStatus.fromBoolean(likeStatus).value
                )
            recommendationService.sendLikeStatus(likePayLoad)
        }

    suspend fun unLikeStatus(
        record_id: Int,
        category: String
    ) =
        executeWithErrorMessage {
            val deletePayLoad =
                DeleteLikePayLoad(
                    record_id,
                    category
                )
            recommendationService.deleteLike(deletePayLoad)
        }
    suspend fun postReview(
        request: ReviewRequest
    ) =
        executeWithErrorMessage {
            recommendationService.postReview(request)
        }
    suspend fun getReviews(
        record_id: Int,
        category: String
    ) =
        executeWithErrorMessage {
            recommendationService.getReviews(record_id, category)
        }
    fun clearCachedData() {
        foodRecommendationsData = null
        sampleRecommendationsData = null
    }

    suspend fun deleteReview(
        request: DeleteReviewRequest
    ) =
        executeWithErrorMessage {
            recommendationService.deleteReview(request)
        }
}
