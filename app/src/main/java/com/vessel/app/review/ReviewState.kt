package com.vessel.app.review

import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ReviewState @Inject constructor() {
    val closeEvent = LiveEvent<Unit>()

    operator fun invoke(block: ReviewState.() -> Unit) = apply(block)
}
