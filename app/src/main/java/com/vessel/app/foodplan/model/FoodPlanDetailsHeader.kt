package com.vessel.app.foodplan.model

import com.vessel.app.R
import com.vessel.app.util.extensions.divideToPercent
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.android.parcel.IgnoredOnParcel

data class FoodPlanDetailsHeader(
    val tittle: String,
    val description: String,
    val image: String,
    val isAdded: Boolean,
    val id: Int,
    val likeStatus: LikeStatus,
    val totalLikes: Int,
    val totalDislikes: Int
) {
    @IgnoredOnParcel
    val buttonResId = if (isAdded) R.string.remove_from_plan else R.string.add_to_plan
    fun getLikesTotalCountPercent(): String {
        val percent = (totalLikes ?: 0).divideToPercent((totalLikes ?: 0) + (totalDislikes ?: 0))
        return "$percent %"
    }
    fun getDislikesTotalCountPercent(): String {
        val percent = (totalDislikes ?: 0).divideToPercent((totalLikes ?: 0) + (totalDislikes ?: 0))
        return "$percent %"
    }
    fun getLikesTotalCount() = totalLikes.toString()
    fun getDislikesTotalCount() = totalDislikes.toString()
    fun getLikeBackground() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.green_background_with_16_radius
    else
        R.drawable.gray_background_with_16_radius

    fun getDisLikeBackground() = if (likeStatus == LikeStatus.DISLIKE)
        R.drawable.red_rose_background
    else
        R.drawable.gray_background_with_16_radius
    fun getThumbShapeIcon() =
        when (likeStatus) {
            LikeStatus.LIKE -> R.drawable.ic_like_review
            LikeStatus.DISLIKE -> R.drawable.ic_dislike_review
            else -> R.drawable.ic_outline_thumb_up
        }
    fun getLikeIcon() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.ic_like_button
    else
        R.drawable.ic_thumb_up

    fun getDisLikeIcon() = if (likeStatus == LikeStatus.DISLIKE)
        R.drawable.ic_thumb_down_red
    else
        R.drawable.ic_thumb_down
}
