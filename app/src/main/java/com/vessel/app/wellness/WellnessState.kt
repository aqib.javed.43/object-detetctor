package com.vessel.app.wellness

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.Program
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.plan.model.PlanItem
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.filterpopup.ItemFilterCategory
import com.vessel.app.wellness.filterpopup.ItemFilterChoice
import com.vessel.app.wellness.filterpopup.ItemFilterChoiceType
import com.vessel.app.wellness.model.*
import com.vessel.app.wellness.ui.FragmentLocationType
import javax.inject.Inject

class WellnessState @Inject constructor(val fragmentLocationType: FragmentLocationType) {
    val navigateToOverview = LiveEvent<Boolean>()
    val showPlanIntroduction = LiveEvent<Pair<Boolean, Int>>()
    val navigateTo = LiveEvent<NavDirections>()
    val showStickyGoalRecommendationsFilterContainer = MutableLiveData<Boolean>()
    val goals = MutableLiveData<List<GoalButton>>()
    val selectedGoal = MutableLiveData<GoalButton>()
    val reagentItems = MutableLiveData<List<ReagentItem>>()
    val showBottomDisclaimer = MutableLiveData<Boolean>()
    val showRateYourGoalHeader = MutableLiveData<Boolean>()
    val showProgramsLoading = MutableLiveData<Boolean>()
    val showSelectGoalLoading = MutableLiveData<Boolean>()
    val reagentVerticalItems = MutableLiveData<List<ReagentItem>>()
    val programsItems = MutableLiveData<List<Program>>()
    val launchLiveChatEvent = LiveEvent<Unit>()
    val showForceUpdate = LiveEvent<Boolean>()
    val dataEntry = MutableLiveData<List<DateEntry>>()
    val goalRecords = MutableLiveData<List<GoalRecord>>()

    var filterCategories = listOf(
        ItemFilterCategory(
            "Sort by",
            listOf(
                ItemFilterChoice("Recommendation", false, ItemFilterChoiceType.RECOMMENDATION),
                ItemFilterChoice("Newest", false, ItemFilterChoiceType.NEWEST),
                ItemFilterChoice("Highest rated", true, ItemFilterChoiceType.HIGHEST_RATE)
            ),
            singleSelection = true
        )
    )
    var score: Score? = null
    var averageScore: Score? = null
    var selectedTagsList: List<Tag>? = null
    val filterSortChoices = MutableLiveData(getFilterChoices())
    var recommendationsPaging = Paging(currentPage = 1, pageSize = 10)
    val recommendations = MutableLiveData<List<RecommendationItem>>(listOf())
    val showLoadingRecommendations = MutableLiveData<Boolean>(false)
    val showLockedGoalsPlaceholder = MutableLiveData<Boolean>(false)
    val openMessaging = LiveEvent<Any>()
    var refreshStateWhenResume = true
    fun getFilterChoices(): List<ItemFilterChoice> {
        val selectedChoices = mutableListOf<ItemFilterChoice>()
        filterCategories.filter { it.visibleInHome }.onEach { category ->
            selectedChoices.addAll(category.choiceItems.filter { it.isChecked })
        }
        return selectedChoices
    }

    val todayActivitiesItems = MutableLiveData<List<PlanItem>>()
    val todayActivitiesLoading = MutableLiveData<Boolean>()
    var userEnrolledInPrograms = MutableLiveData<Boolean>()
    operator fun invoke(block: WellnessState.() -> Unit) = apply(block)
}
