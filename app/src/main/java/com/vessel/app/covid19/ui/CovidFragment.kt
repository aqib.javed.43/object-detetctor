package com.vessel.app.covid19.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.covid19.CovidViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_covid.*

@AndroidEntryPoint
class CovidFragment : BaseFragment<CovidViewModel>() {
    override val viewModel: CovidViewModel by viewModels()
    override val layoutResId = R.layout.fragment_covid

    override fun onViewLoad(savedInstanceState: Bundle?) {
        covidItemsList.adapter = CovidAdapter(viewModel, viewModel)

        observe(viewModel.state.scrollTo) {
            covidItemsList?.post { covidItemsList?.scrollToPosition(it) }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }
}
