package com.vessel.app.common.model

import java.util.*

data class Assessment(
    val goalId: Int,
    val value: Int,
    val goalName: String,
    val createdAt: Date
)
