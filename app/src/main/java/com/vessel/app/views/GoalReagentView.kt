package com.vessel.app.views

import android.content.Context
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.google.android.material.card.MaterialCardView
import com.vessel.app.R
import com.vessel.app.common.binding.srcFromUrl
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.wellness.model.ReagentItem
import kotlinx.android.synthetic.main.item_reagent_goal_new.view.*

class GoalReagentView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    fun updateData(goal: GoalRecord) {
        val goalTitle = goal.name
        lblStatus.text = "${goal.tip_count} tips"
        lblTitle.text = goalTitle
        icon.srcFromUrl(goal.image_small_url)
        cardView.setCardBackgroundColor(context.getColor(R.color.beige_color))
    }
    fun updateData(reagent: ReagentItem) {
        reagent.reagentImage?.let {
            icon.setImageResource(it)
        }
        lblTitle.text = reagent.title
        lblStatus.text = reagent.status
        val endValue = ContextCompat.getColor(
            context,
            reagent.backgroundColor
        )
        cardView.setCardBackgroundColor(endValue)
    }
    private val cardView by lazy { findViewById<MaterialCardView>(R.id.cardContainer) }
    private val lblTitle by lazy { findViewById<TextView>(R.id.title) }
    private val lblStatus by lazy { findViewById<TextView>(R.id.status) }
    private val icon by lazy { findViewById<ImageView>(R.id.reagentIcon) }
    val lblViewDetails by lazy { findViewById<TextView>(R.id.viewDetails) }

    init {
        inflate(getContext(), R.layout.item_reagent_goal_new, this)

        lblViewDetails.apply {
            val text = SpannableString(text)
            text.setSpan(
                UnderlineSpan(),
                0, // start
                text.length, // end
                0 // flags
            )
            setText(text)
        }
    }

    fun extend(ok: Boolean) {
        lblViewDetails.isVisible = ok
    }
}
