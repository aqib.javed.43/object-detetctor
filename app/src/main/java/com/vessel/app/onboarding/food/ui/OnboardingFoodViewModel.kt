package com.vessel.app.onboarding.food.ui

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.FoodManager
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.RecommendationManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.net.data.plan.BuildPlanRequest
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.onboarding.food.OnboardingFoodState
import com.vessel.app.onboarding.food.model.FoodSelectModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import kotlinx.coroutines.launch

class OnboardingFoodViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    val planManager: PlanManager,
    val foodManager: FoodManager,
    val trackingManager: TrackingManager,
    @Assisted private val savedStateHandle: SavedStateHandle,
    val recommendationsManager: RecommendationManager,
    private val homeTabsManager: HomeTabsManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    FoodSelectAdapter.OnActionHandler {

    val state = OnboardingFoodState(savedStateHandle["isLastStep"]!!)
    val isBackVisible = savedStateHandle.get<Boolean>("showBackButton")

    val foodIds = mutableListOf<Int>()

    init {
        state.hint.value = if (preferencesRepository.testCompletedCount > 1) {
            getResString(R.string.select_few_food)
        } else {
            getResString(R.string.select_your_three_food)
        }
        loadFoodRecommendationsForOnboarding()
    }

    private fun loadFoodRecommendationsForOnboarding() {
        showLoading()
        viewModelScope.launch {
            val foodItemsResponse = foodManager.getRecommendedFoodItems()
            if (foodItemsResponse is Result.Success) {
                hideLoading(false)
                val foodList = foodItemsResponse.value
                state.items.value = foodList.map { food ->
                    FoodSelectModel(
                        food.foodId ?: 0,
                        food.name.orEmpty(),
                        food.imageUrl.orEmpty(),
                        false
                    )
                }
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onNextClicked() {
        showLoading()
        val buildPlanRequest = BuildPlanRequest(
            food_ids = foodIds,
            reagent_lifestyle_recommendations_ids = listOf(),
            supplements_ids = listOf(),
            null
        )
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PLAN_BUILD_SUBMITTED)
                .addProperty("data", buildPlanRequest.toString())
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        viewModelScope.launch {
            planManager.buildPlan(buildPlanRequest)
                .onSuccess {
                    hideLoading(false)
                    devicePreferencesRepository.showPlanUpdatedDialog = true
                    if (state.isLastStep) {
                        planManager.clearCachedData()
                        recommendationsManager.updateLatestSampleFoodRecommendationsCachedData()
                        recommendationsManager.updateLatestSampleRecommendationsCachedData()
                        homeTabsManager.clear()
                        state.navigateToHome.call()
                    } else {
                        state.navigateTo.value =
                            OnboardingFoodFragmentDirections.actionOnboardingFoodFragmentToCreatePlanRecommendationFragment(
                                RecommendationType.Supplement
                            )
                    }
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    override fun onFoodSelectClicked(item: FoodSelectModel) {
        if (foodIds.contains(item.id)) {
            foodIds.remove(item.id)
        } else {
            foodIds.add(item.id)
        }
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, food ->
                    if (food.id == item.id) food.copy(checked = !item.checked)
                    else food
                }.toMutableList()
            refreshItems.value = true
        }
    }
}
