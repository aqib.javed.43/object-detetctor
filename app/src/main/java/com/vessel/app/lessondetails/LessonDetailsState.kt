package com.vessel.app.lessondetails

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Answer
import com.vessel.app.common.model.Lesson
import com.vessel.app.common.model.Question
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class LessonDetailsState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val lesson = MutableLiveData<Lesson>()
    var currentQuestion = MutableLiveData<Question>()
    var answers = MutableLiveData<List<Answer>>()
    val backToHome = LiveEvent<Unit>()
    val currentIndex = MutableLiveData<Int>()
    val rightAnswer = MutableLiveData<String>()
    val isCompletedLesson = MutableLiveData<Boolean>()
    val showOrHideQuestion = MutableLiveData<Boolean>()
    operator fun invoke(block: LessonDetailsState.() -> Unit) = apply(block)
}
