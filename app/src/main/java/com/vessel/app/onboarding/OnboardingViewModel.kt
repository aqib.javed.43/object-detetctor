package com.vessel.app.onboarding

import android.view.View
import androidx.annotation.IdRes
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.model.Contact
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.ui.OnboardingFragmentDirections
import com.vessel.app.onboarding.ui.OnboardingPage
import com.vessel.app.util.ResourceRepository
import kotlin.reflect.KClass

class OnboardingViewModel @ViewModelInject constructor(
    val state: OnboardingState,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider), ToolbarHandler {
    private val heightClickedListener = View.OnClickListener { onHeightClicked(it) }
    private val weightClickedListener = View.OnClickListener { onWeightClicked(it) }

    init {
        state.pages
            .addAll(
                listOf(
                    OnboardingPage.Gender(),
                    OnboardingPage.Height(onClickListener = heightClickedListener),
                    OnboardingPage.Weight(onClickListener = weightClickedListener),
                    OnboardingPage.Age(::onDateSet)
                )
            )
    }

    private inline fun <reified T : OnboardingPage> getPage(clazz: KClass<T>): T {
        return state.pages.first { clazz.isInstance(it) } as T
    }

    private fun onHeightClicked(view: View) {
        val heightPage = getPage(OnboardingPage.Height::class)
        view.findNavController()
            .navigate(
                OnboardingFragmentDirections.actionOnboardingToHeightDialog(
                    Contact.Height.fromString(heightPage.userValue)
                )
            )
    }

    private fun onWeightClicked(view: View) {
        val weightPage = getPage(OnboardingPage.Weight::class)
        view.findNavController()
            .navigate(
                OnboardingFragmentDirections.actionOnboardingToWeightDialog(
                    if (weightPage.userValue.isBlank()) Contact.Weight.DEFAULT
                    else weightPage.userValue.toFloat()
                )
            )
    }

    override fun onBackButtonClicked(view: View) {
        if (state.pagePosition == 0) {
            view.findNavController().navigateUp()
        } else {
            state.goToPreviousPage.call()
        }
    }

    fun onNextClicked() {
        if (state.pagePosition == state.pages.size - 1) {
            val weight = Contact.Weight.fromString(getPage(OnboardingPage.Weight::class).userValue)
            val height = Contact.Height.fromString(getPage(OnboardingPage.Height::class).userValue)
            val gender = convertGenderValue(getPage(OnboardingPage.Gender::class).userValue)
            val birthDate = getPage(OnboardingPage.Age::class).userValue
            preferencesRepository.setUserTempData(gender, height, weight, birthDate)
            state.navigateTo.value = OnboardingFragmentDirections.actionOnboardingFragmentToOnboardingDietsFragment(isFirstTestFlow = false, isLastStep = false, false)
        } else {
            state.goToNextPage.call()
        }
    }

    private fun convertGenderValue(@IdRes userValue: Int) = when (userValue) {
        R.id.radioMale -> Contact.Gender.MALE
        R.id.radioFemale -> Contact.Gender.FEMALE
        R.id.radioOther -> Contact.Gender.OTHER
        else -> throw IllegalArgumentException("Unexpected gender value selected!")
    }

    fun onPageChanged(position: Int) {
        state {
            title.value = pages[position].title
            subtitle.value = pages[position].subtitle
            pagePosition = position

            nextButtonEnabled.value = when (val page = pages[position]) {
                is OnboardingPage.Gender -> true
                is OnboardingPage.Height -> page.userValue.isNotBlank()
                is OnboardingPage.Weight -> page.userValue.isNotBlank()
                is OnboardingPage.Age -> page.userValue.isNotBlank()
                else -> false
            }
        }
    }

    private fun onDateSet() {
        state.nextButtonEnabled.value = true
    }

    fun onHeightSet(value: Contact.Height) {
        state.pages[1] = OnboardingPage.Height(value.toString(), heightClickedListener)
        state.nextButtonEnabled.value = true
    }

    fun onWeightSet(value: Float) {
        state.pages[2] = OnboardingPage.Weight(value.toString(), weightClickedListener)
        state.nextButtonEnabled.value = true
    }
}
