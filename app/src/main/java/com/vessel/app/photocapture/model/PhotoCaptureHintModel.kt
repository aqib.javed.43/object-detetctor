package com.vessel.app.photocapture.model

import androidx.annotation.StringRes

data class PhotoCaptureHintModel(
    @StringRes val hintMessage: Int,
    val backgroundColor: Int
)
