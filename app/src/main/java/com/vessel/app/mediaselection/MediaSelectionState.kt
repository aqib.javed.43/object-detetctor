package com.vessel.app.mediaselection

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.helper.Album
import com.vessel.app.mediaselection.model.Item
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class MediaSelectionState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val medias = LiveEvent<List<Item>>()
    val albums = LiveEvent<List<Album>>()
    val nextButtonEnabled = MutableLiveData(true)
    operator fun invoke(block: MediaSelectionState.() -> Unit) = apply(block)
}
