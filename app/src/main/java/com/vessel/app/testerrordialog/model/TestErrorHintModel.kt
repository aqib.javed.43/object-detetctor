package com.vessel.app.testerrordialog.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import kotlinx.android.parcel.IgnoredOnParcel

data class TestErrorHintModel(
    @StringRes val stepNumber: Int,
    @StringRes val title: Int,
    @StringRes val description: Int,
    @DrawableRes val image: Int?,
    @StringRes val clickableLink: Int?
) {
    @IgnoredOnParcel
    val hasClickableLink: Boolean = clickableLink != null

    @IgnoredOnParcel
    val hasImage: Boolean = image != null
}
