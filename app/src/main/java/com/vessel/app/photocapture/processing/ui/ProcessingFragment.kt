package com.vessel.app.photocapture.processing.ui

import android.animation.Animator
import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.airbnb.lottie.LottieDrawable
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.photocapture.processing.ProcessingViewModel
import com.vessel.app.photocapture.processing.mapper.ProcessingScoreErrorMapper
import com.vessel.app.taketest.TakeTestViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_processing_capture.*

@AndroidEntryPoint
class ProcessingFragment : BaseFragment<ProcessingViewModel>() {
    override val viewModel: ProcessingViewModel by viewModels()
    override val layoutResId = R.layout.fragment_processing_capture
    private val takeTestViewModel by lazy { parentViewModel<TakeTestViewModel>() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        lottieAnimationView.apply {
            setAnimation(PROCESSING_FILE_NAME)
            scale = LOADING_VIEW_SCALE_FACTOR
            addAnimatorListener(object : android.animation.AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    setAnimation(LOADING_FILE_NAME)
                    repeatCount = LottieDrawable.INFINITE
                    playAnimation()
                }
            })
        }

        takeTestViewModel.uploadTestFileModel?.let {
            viewModel.associateTestUuid(it)
        }

        viewModel.state {
            observe(goToPostTest) {
                activity?.finish()
                findNavController().navigate(
                    ProcessingFragmentDirections.actionProcessingToPostTest()
                )
            }

            observe(goToLFAErrorView) { reagentsErrors ->
                takeTestViewModel.incrementFailureTestCounter()

                if (takeTestViewModel.failureTestCounter == TakeTestViewModel.FIRST_FAILURE_TEST) {
                    takeTestViewModel.firstFailedTestUUID = viewModel.state.uuid
                    findNavController().navigate(
                        ProcessingFragmentDirections.actionProcessingFragmentToStartNewTimerFragment()
                    )
                } else {
                    findNavController().navigate(
                        ProcessingFragmentDirections.actionProcessingFragmentToLFAErrorFragment(
                            ProcessingScoreErrorMapper.mapReagentsData(reagentsErrors)
                                .toTypedArray(),
                            isFirstTest = false
                        )
                    )
                }
            }

            observe(showErrorDialog) { errorUiModel ->
                takeTestViewModel.uploadTestFileModel?.let {
                    try {
                        if (findNavController().currentDestination?.id == R.id.processingFragment) {
                            takeTestViewModel.generateSampleUUID()
                            findNavController().navigate(
                                ProcessingFragmentDirections.actionProcessingFragmentToProcessingErrorDialogFragment(
                                    errorUiModel,
                                    viewModel.latestSampleUuid ?: it.uuid,
                                    it.wellnessCardUuid
                                )
                            )
                        }
                    } catch (ignored: Exception) {
                        viewModel.logNavigationPathIssue(ignored)
                    }
                }
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            showBackWarning(R.string.are_you_sure_to_back)
        }
    }

    companion object {
        const val PROCESSING_FILE_NAME = R.raw.processing
        const val LOADING_FILE_NAME = R.raw.loading_infinite
        const val LOADING_VIEW_SCALE_FACTOR = 0.2f
    }
}
