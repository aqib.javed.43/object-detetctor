package com.vessel.app.util.delegators

import com.vessel.app.util.LiveEvent
import kotlin.reflect.KProperty

class LiveEventProvider<T>(private val liveEvent: LiveEvent<T> = LiveEvent()) {

    operator fun getValue(thisRef: Any, property: KProperty<*>) = liveEvent
}
