package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.TipData
import com.vessel.app.common.net.data.TipsResponse
import com.vessel.app.common.net.payload.AddTipPayload
import retrofit2.http.*

interface TipService {
    @GET("tip/{id}")
    suspend fun getTipDetails(@Path("id") id: Int): TipData

    @POST("tip")
    suspend fun addTip(@Body addTipPayload: AddTipPayload): TipData

    @GET("tip")
    suspend fun getTipsByTags(@QueryMap options: Map<String, String>): TipsResponse
}
