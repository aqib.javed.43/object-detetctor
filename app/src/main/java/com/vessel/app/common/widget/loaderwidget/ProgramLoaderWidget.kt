package com.vessel.app.common.widget.loaderwidget

import android.annotation.SuppressLint
import android.content.Context
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import com.vessel.app.databinding.WidgetProgramLoaderBinding

class ProgramLoaderWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private lateinit var stepsList: MutableList<LoaderUiModel>
    private lateinit var binding: WidgetProgramLoaderBinding

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetProgramLoaderBinding.inflate(layoutInflater, this, true)
    }

    fun setProgress(progress: Int) {
        createSteps(progress)
        android.os.Handler(Looper.getMainLooper()).post {
            binding.progress.setProgress(progress, true)
        }
    }

    fun setProgressHint(hint: String) {
        binding.stepperHintLabel.text = hint
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun createSteps(progress: Int) {
        when {
            progress >= 50 -> binding.sign50Iv.background = context.getDrawable(R.drawable.ic_active)
            progress >= 75 -> {
                binding.sign50Iv.background = context.getDrawable(R.drawable.ic_active)
                binding.sign75Iv.background = context.getDrawable(R.drawable.ic_active)
            }
            progress == 100 -> {
                binding.sign50Iv.background = context.getDrawable(R.drawable.ic_active)
                binding.sign75Iv.background = context.getDrawable(R.drawable.ic_active)
                binding.sign100Iv.background = context.getDrawable(R.drawable.ic_active)
            }
        }
    }
}
