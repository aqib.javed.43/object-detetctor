package com.vessel.app.stressreliefdetails.ui

import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.R

class StressDetailsSpanSizeLookup(
    private val adapter: ConcatAdapter
) : GridLayoutManager.SpanSizeLookup() {
    private val spanCount = 2

    override fun getSpanSize(position: Int): Int {
        return when (adapter.getItemViewType(position)) {
            R.layout.item_impact_goal, R.layout.item_impact_test_result_reagent -> 1
            else -> spanCount
        }
    }
}
