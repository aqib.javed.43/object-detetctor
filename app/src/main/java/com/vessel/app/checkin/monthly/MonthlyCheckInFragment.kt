package com.vessel.app.checkin.monthly

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.checkin_body_layout.*
import kotlinx.android.synthetic.main.checkin_body_layout.view.*

@AndroidEntryPoint
class MonthlyCheckInFragment : BaseFragment<MonthlyCheckInViewModel>() {
    override val viewModel: MonthlyCheckInViewModel by viewModels()
    override val layoutResId = R.layout.fragment_monthly_membership_checkin

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        observe(viewModel.dismissDialog) { }
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(status) { status ->
                body.lblStatus.text = status
            }
            observe(progress) { progress ->
                body.progressView.setProgress(progress)
            }
        }
    }

    private fun setupViews() {
    }
}
