package com.vessel.app.common.net.mapper

import com.vessel.app.common.net.data.ScienceSourceData
import com.vessel.app.wellness.model.ScienceSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ScienceSourceMapper @Inject constructor() {
    fun map(data: ScienceSourceData) = ScienceSource(
        id = data.id,
        title = data.title,
        description = data.description,
        sourceUrl = data.source_url
    )

    fun map(data: ScienceSource) = ScienceSourceData(
        id = data.id,
        title = data.title,
        description = data.description,
        source_url = data.sourceUrl
    )
}
