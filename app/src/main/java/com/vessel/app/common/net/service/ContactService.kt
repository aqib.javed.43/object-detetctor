package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.AuthData
import com.vessel.app.common.net.data.ContactData
import com.vessel.app.common.net.data.EmailExistenceResponse
import com.vessel.app.common.net.payload.*
import retrofit2.http.*

interface ContactService {
    @POST("contact")
    suspend fun createContact(@Body payload: ContactPayload): AuthData

    @POST("contact/exists")
    suspend fun checkEmailExistence(@Body payload: EmailExistencePayload): EmailExistenceResponse

    @PUT("contact")
    suspend fun updateContact(@Body payload: ContactPayload): ContactData

    @GET("contact")
    suspend fun getContact(): ContactData

    @GET("contact/{id}")
    suspend fun getContactById(@Path("id") id: Int?): ContactData

    @POST("contact/change-password")
    suspend fun changePassword(@Body payload: ChangePasswordPayload)

    @POST("contact/diets")
    suspend fun addDiet(@Body diet: DietsPayload)

    @POST("contact/allergies")
    suspend fun addAllergies(@Body allergies: AllergiesPayload)

    @HTTP(method = "DELETE", path = "contact/diets", hasBody = true)
    suspend fun deleteDiet(@Body diet: DietsPayload)

    @HTTP(method = "DELETE", path = "contact/allergies", hasBody = true)
    suspend fun deleteAllergies(@Body allergies: AllergiesPayload)
}
