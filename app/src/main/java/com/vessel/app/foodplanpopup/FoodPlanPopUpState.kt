package com.vessel.app.foodplanpopup

import androidx.lifecycle.MutableLiveData
import com.vessel.app.foodplanpopup.model.PopUpDialogModel
import javax.inject.Inject

class FoodPlanPopUpState @Inject constructor(isRemoveFoodPlanItem: Boolean) {
    val popUpDialogModelState = MutableLiveData(PopUpDialogModel(isRemoveFoodPlanItem))
    operator fun invoke(block: FoodPlanPopUpState.() -> Unit) = apply(block)
}
