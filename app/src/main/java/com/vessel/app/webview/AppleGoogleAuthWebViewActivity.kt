package com.vessel.app.webview

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.vessel.app.BuildConfig.API_V2
import com.vessel.app.R
import com.vessel.app.common.model.AuthToken
import java.util.regex.Matcher
import java.util.regex.Pattern

class AppleGoogleAuthWebViewActivity : AppCompatActivity() {
    companion object {
        const val INTENT_EXTRA_KEY_SSO_PROVIDER = "INTENT_EXTRA_KEY_SSO_PROVIDER"
        const val INTENT_EXTRA_VAL_SSO_PROVIDER_APPLE = "apple"
        const val INTENT_EXTRA_VAL_SSO_PROVIDER_GOOGLE = "google"
        const val RESULT_INTENT_EXTRA_KEY_AUTH_TOKEN = "RESULT_INTENT_EXTRA_KEY_AUTH_TOKEN"

        private val TAG = AppleGoogleAuthWebViewActivity::class.java.simpleName
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_apple_google_auth_webview)

        val args = intent.extras
        val ssoProvider = args?.getString(INTENT_EXTRA_KEY_SSO_PROVIDER)
        val url = API_V2 + String.format("auth/%s/login", ssoProvider)
        val retrieveUrl = API_V2 + String.format("auth/%s/retrieve", ssoProvider)
        webView = findViewById(R.id.webview)
        webView.addJavascriptInterface(WebViewJSInterface(this), "JS_INTERFACE")
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.userAgentString = System.getProperty("http.agent")!! + ".vessel.android"
        CookieManager.getInstance().setAcceptCookie(true)
        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
        webView.webChromeClient = WebChromeClient()
        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.setSupportMultipleWindows(true)
        webSettings.domStorageEnabled = true
        webSettings.useWideViewPort = true
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                webView.visibility = if (request?.url?.toString()?.contains("retrieve") == true) View.GONE else View.VISIBLE
                return super.shouldOverrideUrlLoading(view, request)
            }
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                Log.d(TAG, "onPageStarted: $url")
                webView.visibility = if (url?.contains("retrieve") == true) View.GONE else View.VISIBLE
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                Log.d(TAG, "onPageFinished: $url")
                if (url?.contains("retrieve") == true) {
                    view?.loadUrl("javascript:window. " + "JS_INTERFACE" + ".processHTML(document.getElementsByTagName('body')[0].innerHTML);")
                }
            }
        }
        webView.loadUrl(url)
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
            return
        }
        super.onBackPressed()
    }

    private lateinit var webView: WebView

    private class WebViewJSInterface(val activity: AppleGoogleAuthWebViewActivity) {
        @JavascriptInterface
        fun processHTML(output: String) {
            val sanitizedOutput = output.replace("\n", "").replace("\r", "")
            Log.d(TAG, "santizedOutput: $sanitizedOutput")

            val pattern: Pattern = Pattern.compile("(?s).*?(\\{.*?\\}).*")
            val matcher: Matcher = pattern.matcher(sanitizedOutput)
            if (matcher.find()) {
                val json = matcher.group(1)
                Log.d(TAG, "json: $json")
                val authToken = Gson().fromJson(json, AuthToken::class.java)
                Log.d(TAG, "authToken: $authToken")

                val data = Intent()
                data.putExtra(RESULT_INTENT_EXTRA_KEY_AUTH_TOKEN, authToken)
                activity.setResult(RESULT_OK, data)
                activity.finish()
            } else {
                Log.d(TAG, "unable to parse auth tokens from sanitizedOutput")
            }
        }
    }
}
