package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.SurveyAnswerResponse
import com.vessel.app.common.net.data.SurveyDataResponse
import com.vessel.app.common.net.data.SurveyResponseData
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface SurveyService {
    @GET("survey/{surveyId}/survey_data")
    suspend fun getSurveyById(@Path("surveyId") surveyId: Long): SurveyDataResponse

    @POST("survey_response/{sampleId}")
    suspend fun submitSurveyResponse(@Path("sampleId") sampleId: String, @Body todoReminder: SurveyResponseData): Response<Unit>

    @GET("question/{questionId}/answer")
    suspend fun getAnswerById(@Path("questionId") questionId: Long): SurveyAnswerResponse
}
