package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.vessel.app.R
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.*

open class AppButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.app_button_view_group, this)
    }

    fun setText(text: String) {
        button.text = text
    }

    fun setText(@StringRes stringResId: Int) {
        button.setText(stringResId)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        button.setOnClickListener(l)
    }
}

class WhiteAppButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppButtonViewGroup(context, attrs, defStyle) {

    init {
        button.setTextColor(ContextCompat.getColor(context, R.color.darkText))
        blurView.setOverlayColor(ContextCompat.getColor(context, R.color.whiteAlpha40))
    }
}
class White70AppButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppButtonViewGroup(context, attrs, defStyle) {

    init {
        button.setTextColor(ContextCompat.getColor(context, R.color.darkText))
        blurView.setOverlayColor(ContextCompat.getColor(context, R.color.whiteAlpha70))
    }
}
class BlackAppButtonViewGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppButtonViewGroup(context, attrs, defStyle) {

    init {
        button.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        blurView.setOverlayColor(ContextCompat.getColor(context, R.color.blackAlpha70))
    }
}
