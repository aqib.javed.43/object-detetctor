package com.vessel.app.vesselfuelcheckout.ui

import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import com.vessel.app.vesselfuelcheckout.VesselFuelCheckoutViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VesselFuelCheckoutFragment : BaseFragment<VesselFuelCheckoutViewModel>() {
    private val vesselFuelItemsAdapter by lazy { VesselFuelItemsAdapter() }
    private val vesselFuelPaymentItemsAdapter by lazy { VesselFuelItemsAdapter() }

    override val viewModel: VesselFuelCheckoutViewModel by viewModels()
    override val layoutResId: Int = R.layout.fragment_vessel_fuel_checkout

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupPriceView()
        setupRecyclerView()
        setupObservers()
    }

    private fun setupPriceView() {
        requireView().findViewById<TextView>(R.id.supplementPrice).text =
            getString(R.string.price_formatter, viewModel.state.totalPrice)
        requireView().findViewById<TextView>(R.id.supplementPriceBreakdown).text =
            viewModel.state.pricePerDay
    }

    private fun setupRecyclerView() {
        requireView().findViewById<RecyclerView>(R.id.paymentItems).adapter =
            vesselFuelPaymentItemsAdapter
        requireView().findViewById<RecyclerView>(R.id.supplementItems).adapter =
            vesselFuelItemsAdapter
    }

    fun setupObservers() {
        viewModel.state {
            observe(vesselFuelItems) {
                vesselFuelItemsAdapter.submitList(it.toList())
            }
            observe(paymentItems) {
                vesselFuelPaymentItemsAdapter.submitList(it)
            }
            observe(addressData) {
                requireView().findViewById<TextView>(R.id.addressLabel).text = it
            }
            observe(navigateToWeb) {
                findNavController().navigate(HomeNavGraphDirections.globalActionToWeb(it))
            }
            observe(checkoutSuccessEvent) {
                findNavController().navigateUp()
                findNavController().navigateUp()
            }
            observe(cancelCheckoutClicked) {
                findNavController().navigateUp()
                findNavController().navigateUp()
            }
        }
    }
}
