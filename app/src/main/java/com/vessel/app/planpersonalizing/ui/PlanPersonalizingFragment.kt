package com.vessel.app.planpersonalizing.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.planpersonalizing.PlanPersonalizingViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PlanPersonalizingFragment : BaseFragment<PlanPersonalizingViewModel>() {
    override val viewModel: PlanPersonalizingViewModel by viewModels()
    override val layoutResId = R.layout.fragment_plan_personalizing

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)
        viewModel.navigateToHome.observe(
            viewLifecycleOwner
        ) {
            findNavController().navigate(PostTestNavGraphDirections.actionGoToHome())
        }
        viewModel.navigateToPlanRecommendation.observe(
            viewLifecycleOwner
        ) {
            findNavController().navigate(
                PlanPersonalizingFragmentDirections.actionPlanPersonalizingFragmentToReagentPersonalizingFragment(false)
            )
        }
    }
}
