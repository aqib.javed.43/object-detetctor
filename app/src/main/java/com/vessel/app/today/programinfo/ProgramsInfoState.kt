package com.vessel.app.today.programinfo

import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ProgramsInfoState @Inject constructor() {
    val items = LiveEvent<List<Program>>()
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: ProgramsInfoState.() -> Unit) = apply(block)
}
