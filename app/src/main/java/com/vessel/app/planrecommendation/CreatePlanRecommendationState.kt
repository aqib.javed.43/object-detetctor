package com.vessel.app.planrecommendation

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.RecommendationType
import javax.inject.Inject

class CreatePlanRecommendationState @Inject constructor(var currentPlanType: RecommendationType) {
    val planRecommendations = mutableListOf<PlanRecommendationModel>()
    val planRecommendationItem = MutableLiveData<PlanRecommendationModel>()
    val progress = MutableLiveData(0)
    val navigate = LiveEvent<Any>()
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: CreatePlanRecommendationState.() -> Unit) = apply(block)
}
