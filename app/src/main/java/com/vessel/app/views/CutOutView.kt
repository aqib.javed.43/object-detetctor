package com.vessel.app.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import com.vessel.app.R

class CutOutView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : View(context, attrs, defStyleAttr, defStyleRes) {
    @ColorInt
    var paintColor: Int = Color.WHITE

    private val curveWidth = resources.getDimensionPixelSize(R.dimen.cutout_curve_width)
    private val paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = paintColor
            style = Paint.Style.FILL
        }
    }
    private val path = Path().apply { fillType = Path.FillType.INVERSE_EVEN_ODD }
    private var bitmap: Bitmap? = null

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        if (w != 0 && h != 0) {
            super.onSizeChanged(w, h, oldw, oldh)
            bitmap?.recycle()
            bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888).also {
                Canvas(it).apply {
                    val fWidth = w.toFloat()
                    val fHeight = h.toFloat()
                    val curveHeight = fHeight
                    val curveStart = (fWidth / 2) - (curveWidth / 2)
                    val controlX = curveWidth / 17
                    val halfHeight = curveHeight / 2

                    path.apply {
                        reset()
                        moveTo(0f, 0f)
                        moveTo(curveStart, 0f)
                        quadTo(curveStart + controlX, 0f, curveStart + (curveWidth / 5), halfHeight)
                        quadTo(
                            curveStart + (curveWidth / 2),
                            curveHeight + (curveHeight / 2.5f),
                            curveStart + (curveWidth * 4 / 5),
                            halfHeight
                        )
                        quadTo(curveStart + curveWidth - controlX, 0f, curveStart + curveWidth, 0f)
                        moveTo(fWidth, 0f)
                        moveTo(fWidth, fHeight)
                        moveTo(0f, fHeight)
                    }

                    drawPath(path, paint)
                }
            }
        }
    }

    override fun onDraw(canvas: Canvas?) {
        bitmap?.let { canvas?.drawBitmap(it, 0f, 0f, null) }
    }
}
