package com.vessel.app

import com.vessel.app.BaseFaker.Fake.FAKER
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.Response

object RetrofitResponseFaker {
    fun basic(): Response<Any> = Response.error(
        FAKER.number().numberBetween(400, 599),
        FAKER.lorem().word().toResponseBody("application/plain".toMediaType())
    )
}
