package com.vessel.app.lfaerrors.ui

import android.os.Bundle
import android.text.method.LinkMovementMethod
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.lfaerrors.LFAErrorViewModel
import com.vessel.app.taketest.TakeTestViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_lfa_error.*

@AndroidEntryPoint
class LFAErrorFragment : BaseFragment<LFAErrorViewModel>() {
    override val viewModel: LFAErrorViewModel by viewModels()
    override val layoutResId = R.layout.fragment_lfa_error
    private val takeTestViewModel by lazy { parentViewModel<TakeTestViewModel>() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        subtitle.movementMethod = LinkMovementMethod.getInstance()

        viewModel.state {
            observe(goToPostScreen) {
                activity?.finish()
                findNavController().navigate(
                    LFAErrorFragmentDirections.actionLFAErrorFragmentToPostTestActivity()
                )
            }
            observe(goToErrorHintDialog) {
                findNavController().navigate(
                    LFAErrorFragmentDirections.actionLFAErrorFragmentToTestErrorHintDialog()
                )
            }
            observe(navigateToCapture) {
                takeTestViewModel.generateSampleUUID()
                findNavController().navigate(
                    LFAErrorFragmentDirections.actionLFAErrorFragmentToCapture()
                )
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            showBackWarning(R.string.are_you_sure_to_back)
        }
    }
}
