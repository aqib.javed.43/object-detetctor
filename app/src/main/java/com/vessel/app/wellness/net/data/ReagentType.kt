package com.vessel.app.wellness.net.data

enum class ReagentType(val value: String) {
    Colorimetric("Colorimetric"),
    Lfa("LFA");

    companion object {
        fun fromValue(value: String): ReagentType = when (value) {
            "Colorimetric" -> Colorimetric
            "LFA" -> Lfa
            else -> throw IllegalArgumentException()
        }
    }
}
