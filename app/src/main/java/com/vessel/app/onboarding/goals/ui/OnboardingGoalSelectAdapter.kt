package com.vessel.app.onboarding.goals.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.onboarding.goals.model.OnboardingGoalSelect

class OnboardingGoalSelectAdapter(
    private val handler: OnActionHandler
) : BaseAdapterWithDiffUtil<OnboardingGoalSelect>({ oldItem, newItem ->
    oldItem.id == newItem.id
}) {
    override fun getLayout(position: Int) = R.layout.item_onboarding_goal_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onGoalSelectClicked(item: OnboardingGoalSelect)
    }
}
