package com.vessel.app.welcome

import com.vessel.app.BaseTest
import com.vessel.app.MockProvider
import com.vessel.app.R
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.ResourceRepository
import com.vessel.app.common.manager.TrackingManager
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class WelcomeViewModelTest : BaseTest() {
    private val resourceRepository = mockk<ResourceRepository>()
    private lateinit var state: WelcomeState
    private lateinit var viewModel: WelcomeViewModel
    private val preferencesRepository = mockk<PreferencesRepository>()
    private val trackingManager = mockk<TrackingManager>()

    @BeforeEach
    fun setUp() {
        state = WelcomeState()
        viewModel = WelcomeViewModel(state, resourceRepository, preferencesRepository, trackingManager)
    }

    @Test
    fun `onCreateAccountClicked navigates to create account screen`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onCreateAccountClicked(view)

        verify { navController.navigate(R.id.action_welcomeFragment_to_emailCheckFragment) }
    }

    @Test
    fun `onSignInClicked navigates to sign in screen`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onSignInClicked(view)

        verify { navController.navigate(R.id.action_welcomeFragment_to_signInFragment) }
    }
}
