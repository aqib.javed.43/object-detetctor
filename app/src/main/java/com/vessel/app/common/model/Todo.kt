package com.vessel.app.common.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.vessel.app.BuildConfig
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.Food
import com.vessel.app.stressrelief.model.StressReliefItem
import com.vessel.app.util.ResourceRepository
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.text.DecimalFormat
import java.util.*

@Parcelize
data class Todo(
    val id: Int,
    val name: String,
    val quantity: Float,
    val insertDate: Date? = null,
    val usdaNdbNumber: Int = 0,
    val sampleId: Int? = 0,
    val completedDate: String? = null,
    val dueDate: String? = null,
    val dueDateTime: String? = null,
    val recurringTodoId: Int? = Constants.TODO_WITHOUT_RECURRING_TODO_ID,
    val food_id: Int? = null,
    val food: Food? = null,
    val has_reminder: Boolean? = null,
    val day_of_week: List<Int>? = null,
    val reminder_time_of_day: String? = null,
) : Parcelable {
    @IgnoredOnParcel
    val formattedDate = insertDate?.let {
        Constants.SERVER_INSERT_DATE_FORMAT.format(it)
    }

    fun getToDoDisplayName(resourceProvider: ResourceRepository, unit: String? = null): String =
        when (name) {
            StressReliefItem.MEDITATE.apiValue -> resourceProvider.getString(R.string.meditation)
            StressReliefItem.BREATH.apiValue -> resourceProvider.getString(R.string.breathwork)
            StressReliefItem.NIGHT.apiValue ->
                resourceProvider.getString(R.string.nighttime_routine)
            StressReliefItem.YOGA.apiValue -> resourceProvider.getString(R.string.yoga)
            StressReliefItem.CARDIO.apiValue -> resourceProvider.getString(R.string.cardio)
            StressReliefItem.FRIEND.apiValue -> resourceProvider.getString(R.string.hang_out)
            BuildPlanManager.WATER_PLAN_API_NAME -> resourceProvider.getString(
                R.string.water,
                quantity.toInt()
            )
            ReagentItem.B7.apiName -> resourceProvider.getString(
                R.string.b7_description,
                quantity.toInt()
            )
            ReagentItem.B9.apiName -> resourceProvider.getString(
                R.string.b9_description,
                quantity.toInt()
            )
            ReagentItem.Magnesium.apiName -> resourceProvider.getString(
                R.string.magnesium_description,
                quantity.toInt()
            )
            ReagentItem.VitaminC.apiName -> resourceProvider.getString(
                R.string.vitamin_c_description,
                quantity.toInt()
            )
            else -> getDisplayFoodName(unit)
        }

    private fun getDisplayFoodName(unit: String? = null): String {
        return if (quantity > 0 && unit.isNullOrEmpty().not()) {
            "$name ${DecimalFormat("0.#").format(quantity)} ${unit.orEmpty()}"
        } else {
            name
        }
    }

    fun getToDoDisplayDescription(resourceProvider: ResourceRepository): String? = when (name) {
        StressReliefItem.MEDITATE.apiValue -> resourceProvider.getQuantityString(
            R.plurals.meditate_description,
            quantity.toInt(),
            quantity.toInt()
        )
        StressReliefItem.BREATH.apiValue -> resourceProvider.getQuantityString(
            R.plurals.breath_description,
            quantity.toInt(),
            quantity.toInt()
        )
        StressReliefItem.YOGA.apiValue -> resourceProvider.getQuantityString(
            R.plurals.yoga_description,
            quantity.toInt(),
            quantity.toInt()
        )
        StressReliefItem.CARDIO.apiValue -> resourceProvider.getQuantityString(
            R.plurals.cardio_description,
            quantity.toInt(),
            quantity.toInt()
        )
        StressReliefItem.FRIEND.apiValue -> resourceProvider.getQuantityString(
            R.plurals.hang_out_description,
            quantity.toInt(),
            quantity.toInt()
        )
        else -> null
    }

    fun getToDoImageUrl(): String = when (name) {
        StressReliefItem.MEDITATE.apiValue,
        StressReliefItem.BREATH.apiValue,
        StressReliefItem.NIGHT.apiValue,
        StressReliefItem.YOGA.apiValue,
        StressReliefItem.CARDIO.apiValue,
        StressReliefItem.FRIEND.apiValue,
        BuildPlanManager.WATER_PLAN_API_NAME,
        BuildPlanManager.TEST_PLAN_API_NAME,
        ReagentItem.B7.apiName,
        ReagentItem.B9.apiName,
        ReagentItem.Magnesium.apiName,
        ReagentItem.VitaminC.apiName,
        BuildPlanManager.TEST_VESSEL_FUEL_API_NAME -> ""
        else -> "${BuildConfig.FOOD_IMAGE_URL_PREFIX}$usdaNdbNumber.jpg"
    }

    fun getToDoImageBackground(): Int = when (name) {
        StressReliefItem.MEDITATE.apiValue -> R.drawable.stress_relief_plan_meditation
        StressReliefItem.BREATH.apiValue -> R.drawable.stress_relief_plan_breathwork
        StressReliefItem.NIGHT.apiValue -> R.drawable.stress_relief_plan_nighttime_routine
        StressReliefItem.YOGA.apiValue -> R.drawable.stress_relief_plan_yoga
        StressReliefItem.CARDIO.apiValue -> R.drawable.stress_relief_plan_cardio
        StressReliefItem.FRIEND.apiValue -> R.drawable.stress_relief_plan_hangout
        BuildPlanManager.WATER_PLAN_API_NAME -> R.drawable.plan_water
        BuildPlanManager.TEST_PLAN_API_NAME -> R.drawable.test_plan_bk
        ReagentItem.B7.apiName,
        ReagentItem.B9.apiName,
        ReagentItem.Magnesium.apiName,
        ReagentItem.VitaminC.apiName,
        BuildPlanManager.TEST_VESSEL_FUEL_API_NAME -> R.drawable.plan_supplement_grey
        else -> 0
    }

    fun isStressReliefPlan(): Boolean = when (name) {
        StressReliefItem.MEDITATE.apiValue,
        StressReliefItem.BREATH.apiValue,
        StressReliefItem.NIGHT.apiValue,
        StressReliefItem.YOGA.apiValue,
        StressReliefItem.CARDIO.apiValue,
        StressReliefItem.FRIEND.apiValue -> true
        else -> false
    }

    @SuppressLint("SimpleDateFormat")
    fun getTimeFormattedWithMeridian(): String? {
        return getDate()?.let {
            Constants.SIMPLE_TIME_12_HOUR_FORMAT.format(it).orEmpty()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getTimeMeridian(): String? {
        return reminder_time_of_day?.split(":")?.get(0)?.let {
            if (Integer.valueOf(it) >= 12) "PM" else "AM"
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getDate() = if (reminder_time_of_day?.isNotEmpty() == true) {
        Constants.SIMPLE_TIME_24_HOUR_FORMAT.parse(reminder_time_of_day)
    } else {
        null
    }
}
