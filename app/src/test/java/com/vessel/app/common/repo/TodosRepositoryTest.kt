package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.BaseFaker.Fake.FAKER
import com.vessel.app.BaseTest
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.net.mapper.TodosMapper
import com.vessel.app.common.net.service.TodosService
import com.vessel.app.isEqualTo
import com.vessel.app.todo.TodoFaker
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TodosRepositoryTest : BaseTest() {
    private lateinit var repository: TodosRepository

    private val moshi = mockk<Moshi>()
    private val loggingManager = mockk<LoggingManager>()
    private val todosMapper = mockk<TodosMapper>()
    private val todosService = mockk<TodosService>()

    @BeforeEach
    fun setUp() {
        repository = TodosRepository(moshi, loggingManager, todosMapper, todosService)
    }

    @Test
    fun `it should return the recurring todos from the API`() = runBlockingTest {
        val data = TodoFaker.dataList()
        coEvery { todosService.getRecurringTodos() } returns data
        val result = TodoFaker.list()
        every { todosMapper.map(data) } returns result

        (repository.getRecurringTodos() as Result.Success).value isEqualTo result
    }

    @Test
    fun `it should create and add a todo to the API`() = runBlockingTest {
        val value = TodoFaker.basic()
        val data = TodoFaker.data()
        every { todosMapper.mapToData(value) } returns data
        val netData = TodoFaker.data()
        coEvery { todosService.addRecurringTodo(data) } returns netData
        val result = TodoFaker.basic()
        every { todosMapper.map(netData) } returns result

        (repository.addRecurringTodo(value) as Result.Success).value isEqualTo result
    }

    @Test
    fun `it should delete the recurring todo in the API from the given ID`() = runBlockingTest {
        val todoId = FAKER.number().randomDigit()
        coEvery { todosService.deleteRecurringTodo(todoId) } just runs

        repository.deleteRecurringTodo(todoId)

        coVerify { repository.deleteRecurringTodo(todoId) }
    }
}
