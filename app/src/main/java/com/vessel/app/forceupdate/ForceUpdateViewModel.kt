package com.vessel.app.forceupdate

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ForceUpdateManager
import com.vessel.app.common.manager.ForceUpdateType
import com.vessel.app.util.ResourceRepository

class ForceUpdateViewModel @ViewModelInject constructor(
    val state: ForceUpdateState,
    resourceProvider: ResourceRepository,
    private val forceUpdateManager: ForceUpdateManager,
) : BaseViewModel(resourceProvider) {

    init {
        val updateType = forceUpdateManager.isRequireUpdate()
        state.isDialogCancelable.value = updateType.second == ForceUpdateType.Optional
    }

    fun onLaterClicked() {
        forceUpdateManager.setOptionalUpdateDismissed()
        state.dismissDialog.call()
    }

    fun onUpdateClicked() {
        state.updateClicked.call()
    }
}
