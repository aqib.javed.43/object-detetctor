package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.TodoData
import retrofit2.http.*

interface TodosService {
    @GET("wellness/todos/recurring/today")
    suspend fun getTodayRecurringTodos(): List<TodoData>

    @GET("wellness/todos/recurring")
    suspend fun getRecurringTodos(): List<TodoData>

    @POST("wellness/todos/recurring")
    suspend fun addRecurringTodo(@Body todo: TodoData): TodoData

    @PUT("wellness/todo/recurring/{todoId}")
    suspend fun updateRecurringTodo(@Path("todoId") todoId: Int, @Body todo: TodoData): TodoData

    @DELETE("wellness/todo/recurring/{todoId}")
    suspend fun deleteRecurringTodo(@Path("todoId") todoId: Int)

    @PUT("wellness/todo/{todoId}/toggle")
    suspend fun updateRecurringTodoToggle(@Path("todoId") todoId: Int): TodoData
}
