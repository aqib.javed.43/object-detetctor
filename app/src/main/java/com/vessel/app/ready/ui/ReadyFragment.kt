package com.vessel.app.ready.ui

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.LoopingMediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.AssetDataSource
import com.google.android.exoplayer2.upstream.DataSource.Factory
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.ready.ReadyViewModel
import com.vessel.app.ready.videowarning.IncompleteVideoWarningDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

@AndroidEntryPoint
class ReadyFragment : BaseFragment<ReadyViewModel>() {
    override val viewModel: ReadyViewModel by viewModels()
    override val layoutResId = R.layout.fragment_ready
    private val exoPlayer by lazy {
        SimpleExoPlayer.Builder(requireContext())
            .build()
    }
    private val tipAdapter = TipsAdapter()
    private val parentJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + parentJob)

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observe(viewModel.state.closeEvent) { activity?.finish() }
        observe(viewModel.state.navigateTo) {
            exoPlayer.pause()
            findNavController().navigate(it)
        }
        requireView().findViewById<RecyclerView>(R.id.tips).also {
            it.adapter = tipAdapter
        }
        tipAdapter.submitList(viewModel.getTips())
        setupVideo()
        requireActivity().supportFragmentManager.setFragmentResultListener(IncompleteVideoWarningDialogFragment.REQUEST_KEY, viewLifecycleOwner) { _, _ ->
            exoPlayer.play()
        }
    }

    private fun setupVideo() {
        val videoPlayer = requireView().findViewById<PlayerView>(R.id.videoPlayer)
        videoPlayer.useController = viewModel.preferencesRepository.showTipVideoController
        videoPlayer.controllerAutoShow = false
        videoPlayer.player = exoPlayer
        exoPlayer.apply {
            val assetSourceFactory = Factory { AssetDataSource(requireContext()) }
            val mediaItem = MediaItem.fromUri(Uri.parse(VIDEO_PATH))
            val assetVideoSource = ProgressiveMediaSource.Factory(assetSourceFactory)
                .createMediaSource(mediaItem)
            val loopingMediaSource = LoopingMediaSource(assetVideoSource)
            addMediaSource(loopingMediaSource)
            prepare()
        }
    }

    override fun onResume() {
        super.onResume()
        exoPlayer.playWhenReady = true
        exoPlayer.play()
    }

    override fun onPause() {
        super.onPause()
        exoPlayer.playWhenReady = false
        exoPlayer.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        parentJob.cancel()
    }

    private companion object {
        const val VIDEO_PATH = "assets:///vessel_test_card_tutorial.mp4"
        const val MINIMUM_ACCEPTED_VIDEO_PROGRESS = 90L
    }
}
