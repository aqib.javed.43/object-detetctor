package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SurveyAnswerWrapperData(
    val answer_id: Int,
    val answer_sequence: Int,
    val answer: SurveyAnswerData
)
