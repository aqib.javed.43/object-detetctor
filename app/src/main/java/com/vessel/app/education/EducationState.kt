package com.vessel.app.education

import androidx.lifecycle.MutableLiveData
import com.vessel.app.base.MutableLiveArrayList
import com.vessel.app.education.ui.EducationPage
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class EducationState @Inject constructor() {
    val goToPreviousPage = LiveEvent<Unit>()
    val goToNextPage = LiveEvent<Unit>()
    val pages = MutableLiveArrayList<EducationPage>()
    val position = MutableLiveData(0)

    operator fun invoke(block: EducationState.() -> Unit) = apply(block)
}
