package com.vessel.app.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.abs

class SliderLayoutManager(
    private val recyclerView: RecyclerView,
    @RecyclerView.Orientation orientation: Int = RecyclerView.VERTICAL,
    private val animationRange: Float = 0.4f,
    private val callback: ((Int) -> Unit)? = null
) : LinearLayoutManager(recyclerView.context, orientation, false) {

    private val linearSnapHelper = LinearSnapHelper().apply {
        attachToRecyclerView(recyclerView)
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State) {
        super.onLayoutChildren(recycler, state)
        updateView()
    }

    override fun scrollVerticallyBy(
        dy: Int,
        recycler: RecyclerView.Recycler?,
        state: RecyclerView.State?
    ): Int {
        return if (orientation == VERTICAL) {
            val scrolled = super.scrollVerticallyBy(dy, recycler, state)
            updateView()
            scrolled
        } else {
            0
        }
    }

    override fun scrollHorizontallyBy(
        dx: Int,
        recycler: RecyclerView.Recycler?,
        state: RecyclerView.State?
    ): Int {
        return if (orientation == HORIZONTAL) {
            val scrolled = super.scrollHorizontallyBy(dx, recycler, state)
            updateView()
            scrolled
        } else {
            0
        }
    }

    private fun updateView() {
        val measurement = if (orientation == VERTICAL) height else width
        val mid = measurement / 2f
        for (i in 0 until childCount) {
            getChildAt(i)?.let {
                val childMid = (
                    if (orientation == VERTICAL) {
                        getDecoratedTop(it) + getDecoratedBottom(it)
                    } else {
                        getDecoratedLeft(it) + getDecoratedRight(it)
                    }
                    ) / 2f
                val distanceFromCenter = abs(mid - childMid)
                val percentFromEnd = 1 - distanceFromCenter / (measurement / 2)
                val percentWithFloor = animationRange * percentFromEnd + 1 - animationRange

                it.scaleX = percentWithFloor
                it.scaleY = percentWithFloor
                it.alpha = percentWithFloor
            }
        }
    }

    override fun onScrollStateChanged(state: Int) {
        super.onScrollStateChanged(state)

        if (state == RecyclerView.SCROLL_STATE_IDLE) {
            val position = linearSnapHelper.findSnapView(this)?.let {
                recyclerView.getChildAdapterPosition(it)
            } ?: -1

            if (position > -1) callback?.invoke(position)
        }
    }

    fun slideToPosition(position: Int) {
        if (position > -1) {
            recyclerView.post {
                recyclerView.scrollToPosition(position)
                recyclerView.post {
                    findViewByPosition(position)?.let {
                        val snapDistance = linearSnapHelper.calculateDistanceToFinalSnap(this, it)!!
                        if (snapDistance[0] != 0 || snapDistance[1] != 0) {
                            recyclerView.scrollBy(snapDistance[0], snapDistance[1])
                        }
                    }
                }
            }
        }
    }
}
