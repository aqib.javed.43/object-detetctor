package com.vessel.app.common.repo

import com.amazonaws.util.Base64
import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.KlavyioEvent
import com.vessel.app.common.model.KlavyioEventJsonAdapter
import com.vessel.app.common.net.service.TrackingService
import okhttp3.internal.toImmutableMap
import java.nio.charset.Charset
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrackingRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val trackingService: TrackingService
) : BaseNetworkRepository(moshi, loggingManager) {

    suspend fun logKlaviyoEvent(
        eventName: String,
        email: String,
        firstName: String?,
        lastName: String?,
        userId: Int,
        properties: Map<String, String>? = null
    ): Int {
        val userPropertiesMap = buildKlaviyoUserProperties(email, firstName, lastName, userId)
        val klavyioEvent = KlavyioEvent(eventName, userPropertiesMap, properties)
        val eventString = KlavyioEventJsonAdapter(moshi).toJson(klavyioEvent).toByteArray()
        val encodedString = Base64.encode(eventString).toString(Charset.defaultCharset())
        return trackingService.logKlaviyoEvent(data = encodedString)
    }

    private fun buildKlaviyoUserProperties(
        email: String,
        firstName: String?,
        lastName: String?,
        userId: Int
    ): Map<String, String> {
        val map: MutableMap<String, String> = mutableMapOf(Pair(KEY_EMAIL, email))
        firstName?.let {
            map.put(KEY_FIRST_NAME, it)
        }
        lastName?.let {
            map.put(KEY_LAST_NAME, it)
        }
        if (userId > 0)
            map.put(KEY_ID, userId.toString())
        return map.toImmutableMap()
    }

    private companion object {
        const val KEY_EMAIL = "\$email"
        const val KEY_FIRST_NAME = "\$first_name"
        const val KEY_LAST_NAME = "\$last_name"
        const val KEY_ID = "\$id"
    }
}
