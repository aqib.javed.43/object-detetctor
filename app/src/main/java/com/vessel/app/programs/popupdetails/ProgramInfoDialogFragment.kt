package com.vessel.app.programs.popupdetails

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isInvisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProgramInfoDialogFragment : BaseDialogFragment<ProgramInfoDialogViewModel>() {
    override val viewModel: ProgramInfoDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_program_info_dialog
    private val recommendedByExpertName: TextView by lazy { requireView().findViewById(R.id.expertName) }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
        isCancelable = false
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(dismiss) {
                dismiss()
            }
            observe(program) { program ->
                if (program.contactFullName.isNullOrEmpty().not()) {
                    val recommendedByText =
                        getString(R.string.recommended_by, program.contactFullName)
                    recommendedByExpertName.text = recommendedByText
                } else
                    recommendedByExpertName.isInvisible = true
            }
        }
    }
}
