package com.vessel.app.vesselfuelcheckout.model

import android.os.Parcelable
import com.vessel.app.supplementsbucket.model.Supplement
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VesselFuelItem(
    val totalPrice: Double,
    val pricePerDay: String,
    val checkoutUrl: String,
    val items: List<Supplement>,
) : Parcelable
