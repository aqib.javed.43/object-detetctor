package com.vessel.app.common.binding

import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.databinding.BindingAdapter

@BindingAdapter("onDone")
fun EditText.onDone(listener: OnDoneListener?) {
    if (listener == null) {
        setOnEditorActionListener(null)
    } else {
        imeOptions = EditorInfo.IME_ACTION_DONE
        setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) listener.onDone(v)
            false
        }
    }
}

interface OnDoneListener {
    fun onDone(v: View?)
}
