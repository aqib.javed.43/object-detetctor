package com.vessel.app.testerrordialog.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.testerrordialog.TestErrorHintDialogViewModel
import com.vessel.app.testerrordialog.model.TestErrorHintFooter
import com.vessel.app.testerrordialog.model.TestErrorHintHeader
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_test_error_hint_dialog.*

@AndroidEntryPoint
class TestErrorHintDialog : BaseDialogFragment<TestErrorHintDialogViewModel>() {
    override val viewModel: TestErrorHintDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_test_error_hint_dialog

    private val hintsAdapter by lazy { TestErrorHintAdapter() }
    private val headerAdapter by lazy { TestErrorHintHeaderAdapter(viewModel) }
    private val footerAdapter by lazy { TestErrorHintFooterAdapter(viewModel) }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
        isCancelable = false
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        hints.also {
            val adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                headerAdapter,
                hintsAdapter,
                footerAdapter
            )

            it.adapter = adapter
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(dismissDialog) { dismiss() }
            observe(errorHints) {
                hintsAdapter.submitList(it)
            }
        }
        headerAdapter.submitList(listOf(TestErrorHintHeader))
        footerAdapter.submitList(listOf(TestErrorHintFooter))
    }
}
