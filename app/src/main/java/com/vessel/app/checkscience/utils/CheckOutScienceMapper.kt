package com.vessel.app.checkscience.utils

import android.text.SpannableString
import android.text.Spanned
import android.text.style.ClickableSpan
import android.view.View
import com.vessel.app.R
import com.vessel.app.checkscience.model.CheckOutScienceViewItem
import com.vessel.app.checkscience.model.ScienceReference
import com.vessel.app.checkscience.ui.OnActionHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.makeClickableSpan

object CheckOutScienceMapper {
    fun mapReferencesToScienceViewItems(
        scienceReferences: Array<ScienceReference>,
        clickHandler: OnActionHandler,
        resourceProvider: ResourceRepository
    ): List<CheckOutScienceViewItem> {
        return scienceReferences.map { preference ->
            var link = resourceProvider.getString(preference.link)
            var text = buildString {
                append(resourceProvider.getString(R.string.reference))
                append(" ")
                append(scienceReferences.indexOf(preference) + 1)
            }
            var clickableText = text
            var showAsReference = true

            if (preference.text != null) {
                text = resourceProvider.getString(preference.text)
                clickableText = resourceProvider.getString(R.string.read_more)
                showAsReference = false
            }

            if (text.contains(link)) {
                CheckOutScienceViewItem(
                    text.replace(link, "").makeClickableSpan(
                        Pair(
                            clickableText,
                            View.OnClickListener {
                                clickHandler.onLinkButtonClick(
                                    it,
                                    link
                                )
                            }
                        )
                    ),
                    showAsReference,
                    preference.background
                )
            } else {
                val spanned = SpannableString(text)
                spanned.setSpan(
                    object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            clickHandler.onLinkButtonClick(
                                widget,
                                link
                            )
                        }
                    },
                    0, text.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                CheckOutScienceViewItem(
                    spanned,
                    true,
                    preference.background
                )
            }
        }
    }
}
