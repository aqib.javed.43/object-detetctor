package com.vessel.app.covid19

import com.vessel.app.covid19.db.CovidTestDb
import com.vessel.app.covid19.repo.CovidTestDbRepository
import com.vessel.app.covid19.repo.CovidTestNetRepository
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CovidTestManager @Inject constructor(
    private val covidTestNetRepository: CovidTestNetRepository,
    private val covidTestDatabaseRepository: CovidTestDbRepository
) {
    suspend fun isTestProcessed(testId: String) = covidTestNetRepository.isTestProcessed(testId)

    suspend fun getCovidTests() = covidTestDatabaseRepository.getCovidTests()
        .onStart {
            getAndCacheTests()
        }

    suspend fun getAndCacheTests() = covidTestNetRepository.getCovidTests()
        .then { result ->
            covidTestDatabaseRepository.addTests(
                result.value.map { covidTestNet ->
                    CovidTestDb(
                        covidTestNet.uuid,
                        covidTestNet.status.value,
                        covidTestNet.formattedDate
                    )
                }
            )
            result
        }
}
