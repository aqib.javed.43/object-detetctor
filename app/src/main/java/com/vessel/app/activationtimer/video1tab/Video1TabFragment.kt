package com.vessel.app.activationtimer.video1tab

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.viewModels
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Video1TabFragment : BaseFragment<Video1TabViewModel>() {
    override val viewModel: Video1TabViewModel by viewModels()
    override val layoutResId = R.layout.fragment_video_1_tab

    private val exoPlayer by lazy { SimpleExoPlayer.Builder(requireContext()).build() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupVideo()
    }

    private fun setupVideo() {
        requireView().findViewById<PlayerView>(R.id.video).player = exoPlayer
        exoPlayer.run {
            val defaultSourceFactory =
                DefaultDataSourceFactory(requireContext(), BuildConfig.APPLICATION_ID)
            val mediaItem = MediaItem.fromUri(Uri.parse(VIDEO_URL))
            val assetVideoSource = ProgressiveMediaSource.Factory(defaultSourceFactory)
                .createMediaSource(mediaItem)
            addMediaSource(assetVideoSource)
            prepare()
        }
    }

    override fun onResume() {
        super.onResume()
        exoPlayer.playWhenReady = true
    }

    override fun onPause() {
        super.onPause()
        exoPlayer.playWhenReady = false
    }

    override fun onStop() {
        super.onStop()
        exoPlayer.playWhenReady = false
        exoPlayer.stop()
    }

    companion object {
        const val VIDEO_URL = "https://d32fui7xaadmx6.cloudfront.net/wellness/timer-video.mp4"
    }
}
