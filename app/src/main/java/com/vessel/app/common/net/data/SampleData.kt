package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass
import com.vessel.app.common.net.data.plan.GoalRecord

@JsonClass(generateAdapter = true)
data class SampleData(
    val goals: List<GoalRecord>? = listOf(),
    val quantity: Float?,
    val dosage: Float?,
    val supplement_id: Int? = null,
    val unit: String? = null,
    val dosage_unit: String? = null,
    val lifestyle_recommendation_id: Int? = null,
    val id: Int? = null,
    val reagent_id: Int? = null,
    val in_plan: Boolean? = null,
    val activity_name: String? = null,
    val frequency: String? = null,
    val name: String? = null,
    val image_url: String? = null,
    val description: String? = null,
    val extra_images: List<String>? = null,
    val like_status: String? = null,
    val total_likes: Int? = null,
    val dislikes_count: Int? = null,
)

@JsonClass(generateAdapter = true)
data class SampleFoodData(
    val id: Int? = null,
    val food_title: String? = null,
    val serving_unit: String? = null,
    val serving_grams: Float? = null,
    val image_url: String? = null,
)
