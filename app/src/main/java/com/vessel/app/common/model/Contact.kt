package com.vessel.app.common.model

import android.os.Parcelable
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.util.extensions.formatMonthYearDate
import com.vessel.app.wellness.model.Tip
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.floor
import kotlin.math.roundToInt

@Parcelize
data class Contact(
    val id: Int?,
    val email: String?,
    val firstName: String?,
    val lastName: String?,
    val lastLogin: Date,
    val insertDate: Date,
    val gender: Gender?,
    val height: Height?,
    val weight: Weight?,
    val birthDate: Date?,
    val allergies: List<String?>?,
    val diets: List<Diet>?,
    val goals: List<GoalSample>?,
    val time_zone: String?,
    val tips: List<Tip>? = null,
    val occupation: String?,
    val imageUrl: String?,
    val isVerified: Boolean?,
    val about: String?,
    val location: String?,
    val surveyStatus: Boolean?,
    val testsTaken: Int?,
    val programs: List<Program>?,
    val mainGoalId: Int?
) : Parcelable {
    @IgnoredOnParcel
    val birthDateString = birthDate?.let { BIRTH_DATE_FORMAT.format(it) } ?: ""

    @IgnoredOnParcel
    val fullName = firstName?.plus(" ")?.plus(lastName)

    enum class Gender(val encoding: Char) {
        FEMALE('f'),
        MALE('m'),
        OTHER('o');

        companion object {
            fun fromEncoding(encoding: Char) = when (encoding) {
                'f' -> FEMALE
                'm' -> MALE
                'o' -> OTHER
                else -> throw IllegalArgumentException("unexpected gender encoding $encoding")
            }
        }
    }

    @Parcelize
    data class Height(val feet: Int, val inches: Int) : Parcelable {
        fun toCentimeters() = feet * 30.48 + inches * 2.54

        override fun toString() = "$feet' $inches\""

        companion object {
            fun fromCentimeters(heightInCm: Float): Height {
                val cmToFeet = heightInCm / 30.48
                val feet = floor(cmToFeet).toInt()
                val inches = (cmToFeet - feet) * 12
                return Height(feet, inches.roundToInt())
            }

            fun fromString(heightString: String): Height {
                if (heightString.isBlank()) {
                    return DEFAULT_HEIGHT
                }
                val splitString = heightString.split("'")
                return Height(
                    splitString[0].toInt(),
                    splitString[1].replace("\"", "").trim().toInt()
                )
            }

            val feetRange = (0..10).map { "$it'" }.toList()
            val inchesRange = (0..11).map { "$it\"" }.toList()

            val DEFAULT_HEIGHT = Height(5, 7)
        }
    }

    @Parcelize
    data class Weight(val weight: Float) : Parcelable {
        fun toKg() = weight * 0.45359237f

        override fun toString() = weight.toInt().toString()

        companion object {
            fun fromString(weightString: String) = Weight(weightString.toFloat())

            fun fromKg(weightKg: Float) = Weight(weightKg / 0.45359237f)

            val rangeHundreds = (0..6).map { "$it" }.toList()
            val rangeTens = (0..9).map { "$it" }.toList()
            val rangeUnits = (0..9).map { "$it" }.toList()

            const val DEFAULT = 185f
        }
    }

    fun occupationAndLocation() =
        listOfNotNull(occupation, location).filter { it.isNotEmpty() }.joinToString(", ")

    @IgnoredOnParcel
    val formattedAbout: String = about.orEmpty()

    @IgnoredOnParcel
    val testsCount: String = testsTaken?.toString().orEmpty()

    @IgnoredOnParcel
    val joinedDate: String = insertDate.formatMonthYearDate().orEmpty()

    companion object {
        val BIRTH_DATE_FORMAT = SimpleDateFormat("MM/dd/yy", Locale.ENGLISH)
        val BIRTH_DATE_REGEX = Regex("[0-9][0-9]/[0-9][0-9]/[0-9][0-9]")
    }
}
