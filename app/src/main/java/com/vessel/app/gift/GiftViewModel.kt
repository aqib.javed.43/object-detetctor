package com.vessel.app.gift

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class GiftViewModel @ViewModelInject constructor(
    val state: GiftState,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {

    fun onContinueClicked() {
        navigateBack()
    }
}
