package com.vessel.app.today.ui

import android.animation.Animator
import android.text.InputType
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.core.view.isVisible
import com.airbnb.lottie.LottieAnimationView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.plan.model.PlanItem
import kotlinx.android.synthetic.main.item_program_activity.view.*

class ProgramPlanAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<PlanItem>() {
    override fun getLayout(position: Int) = R.layout.item_program_activity

    override fun getHandler(position: Int) = handler
    var onActivityUpdate: (() -> Unit)? = null

    interface OnActionHandler {
        fun onProgramActivityCompleted(item: PlanItem?, view: View)
        fun onAnimationFinished(item: Any, view: View, parentPosition: Int)
        fun onProgramInformationClicked()
        fun onPlanItemClicked(item: Any, view: View, parentPosition: Int)
        fun onReactItemClicked(item: Any, view: View, parentPosition: Int)
        fun onGetNextPlanClicked()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<PlanItem>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position)
        if (item.type == PlanType.TestPlan) {
            holder.itemView.findViewById<View>(R.id.fadding).isVisible = false
            holder.itemView.findViewById<View>(R.id.improvesData).isVisible = false
            holder.itemView.findViewById<View>(R.id.goalsData).isVisible = false
        } else {
            val goals = item.currentPlan.getPlanGoals()
            val improves = item.currentPlan.getImproves()
            holder.itemView.findViewById<View>(R.id.fadding).isVisible =
                goals.trim().isNotEmpty() || improves.trim().isNotEmpty()
            holder.itemView.findViewById<TextView>(R.id.goalsData).apply {
                text = goals
                isVisible = goals.trim().isNotEmpty()
            }
            holder.itemView.findViewById<TextView>(R.id.improvesData).apply {
                text = improves
                isVisible = improves.trim().isNotEmpty()
            }

            if (improves.trim().isEmpty() || goals.trim().isEmpty()) {
                holder.itemView.findViewById<TextView>(R.id.goalsData).apply {
                    inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    isElegantTextHeight = true
                    isSingleLine = false
                }
                holder.itemView.findViewById<TextView>(R.id.improvesData).apply {
                    inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    isElegantTextHeight = true
                    isSingleLine = false
                }
            }
        }
        val checkbox = holder.itemView.findViewById<AppCompatCheckBox>(R.id.checkbox)
        val reactionLayout = holder.itemView.findViewById<View>(R.id.reactionLayout)
        val faddingView = holder.itemView.findViewById<View>(R.id.fadding)
        val bottomBlur = holder.itemView.findViewById<View>(R.id.bottomBlurBackground)
        val enableCheckBox = (item.isCompleted).not()
        holder.itemView.itemView.apply {
            if (!enableCheckBox) {
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                layoutParams.height = resources.getDimension(R.dimen.plan_item_height).toInt()
            }
        }
        holder.itemView.swipe.apply {
            if (!enableCheckBox) {
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                layoutParams.height = resources.getDimension(R.dimen.max_expert_name_width).toInt()
            }
        }
        checkbox.apply {
            isEnabled = enableCheckBox
            isClickable = enableCheckBox
        }
        reactionLayout.apply {
            visibility = if (enableCheckBox) View.GONE else View.VISIBLE
        }
        faddingView.apply {
            visibility = if (enableCheckBox) View.VISIBLE else View.GONE
        }
        bottomBlur.apply {
            visibility = if (enableCheckBox) View.VISIBLE else View.GONE
        }
        checkbox.setOnClickListener {
            if (item.isCompleted) return@setOnClickListener
            handleActivityCheckClick(item, holder, position)
            item.isCompleted = true
            onActivityUpdate?.invoke()
            notifyItemChanged(position)
        }
        holder.itemView.findViewById<View>(R.id.background).setOnClickListener {
            handler.onPlanItemClicked(item, it, position)
        }
        holder.itemView.findViewById<View>(R.id.reactionLayout).setOnClickListener {
            handler.onReactItemClicked(item, it, position)
        }
    }
    private fun handleActivityCheckClick(item: PlanItem, holder: BaseViewHolder<PlanItem>, position: Int) {
        if (item.isCompleted.not()) {
            holder.itemView.findViewById<LottieAnimationView>(R.id.planSuccessAnimationView)
                .playAnimation()
            holder.itemView.findViewById<LottieAnimationView>(R.id.planAddingAnimationView).apply {
                isVisible = true
                visibility = View.VISIBLE
                playAnimation()
            }
            holder.itemView.findViewById<View>(R.id.blurBackground).animate().alpha(1f).duration =
                ANIMATION_DURATION
        } else {
            this@ProgramPlanAdapter.handler.onProgramActivityCompleted(
                item,
                holder.itemView,
            )
            holder.itemView.findViewById<View>(R.id.blurBackground).animate().apply {
                setListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        this@ProgramPlanAdapter.handler.onAnimationFinished(
                            getItem(position),
                            holder.itemView,
                            holder.bindingAdapterPosition
                        )
                    }

                    override fun onAnimationCancel(animation: Animator) {
                    }

                    override fun onAnimationRepeat(animation: Animator) {
                    }
                })
                alpha(0f).duration = ANIMATION_DURATION
            }
        }
    }
    companion object {
        private const val ANIMATION_VIEW_SCALE_FACTOR = 0.4f
        private const val ANIMATION_DURATION: Long = 500
    }
}
