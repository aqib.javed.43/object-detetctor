package com.vessel.app.checkin.checkintips

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.widget.TipsProgramCardWidgetOnActionHandler
import com.vessel.app.wellness.model.RecommendationItem
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.android.synthetic.main.item_program_checkin.view.*

class TipsCheckInProgramAdapter(private val handler: TipsProgramCardWidgetOnActionHandler) :
    BaseAdapterWithDiffUtil<RecommendationItem>() {
    override fun getLayout(position: Int) = R.layout.item_program_checkin

    override fun getHandler(position: Int) = handler
    override fun onBindViewHolder(holder: BaseViewHolder<RecommendationItem>, position: Int) {
        val item = getItem(position)
        holder.itemView.likeTopBg?.setOnClickListener {
            handler.onItemLikeClicked(item)
            val likesCount = item.iRecommendation.getLikesCount()
            if (item.iRecommendation.getLikedStatus() != LikeStatus.LIKE) {
                item.iRecommendation.setLikesCount(likesCount.plus(1))
            } else {
                item.iRecommendation.setLikesCount(likesCount.minus(1))
            }
            item.iRecommendation.setLikedStatus(if (item.iRecommendation.getLikedStatus() != LikeStatus.LIKE) LikeStatus.LIKE else LikeStatus.NO_ACTION)
            notifyItemChanged(position)
        }
        super.onBindViewHolder(holder, position)
    }
}
