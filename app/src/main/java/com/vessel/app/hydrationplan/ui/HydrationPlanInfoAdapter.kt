package com.vessel.app.hydrationplan.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.hydrationplan.model.HydrationPlanInfoItem

class HydrationPlanInfoAdapter : BaseAdapterWithDiffUtil<HydrationPlanInfoItem>() {
    override fun getLayout(position: Int) = R.layout.item_hydration_plan_info
}
