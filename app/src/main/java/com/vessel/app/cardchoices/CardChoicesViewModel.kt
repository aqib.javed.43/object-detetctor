package com.vessel.app.cardchoices

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.cardchoices.model.BOUGHT
import com.vessel.app.cardchoices.model.CardChoiceUIItem
import com.vessel.app.cardchoices.model.GIFTED
import com.vessel.app.cardchoices.ui.CardChoicesAdapter
import com.vessel.app.cardchoices.ui.CardChoicesFragmentDirections
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.ResourceRepository

class CardChoicesViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    private val trackingManager: TrackingManager,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceRepository), CardChoicesAdapter.OnActionHandler {
    val state = CardChoicesState(savedStateHandle.get("email")!!)

    override fun onChoiceSelectClicked(item: CardChoiceUIItem, isChecked: Boolean) {
        state.choices.value = state.choices.value?.map {
            it.copy(
                if (it == item)
                    isChecked
                else
                    false
            )
        }
    }

    fun continueClicked() {
        val selected = state.choices.value?.firstOrNull {
            it.checked
        }

        state.navigateTo.value = when (selected) {
            is GIFTED -> {
                logEvent(TrackingConstants.SIGN_UP_GIFTED)
                CardChoicesFragmentDirections.actionCardChoicesFragmentToGiftedCardChoiceFragment(state.email, fromAppleGoogleSSOAuth)
            }
            is BOUGHT -> {
                logEvent(TrackingConstants.SIGN_UP_BOUGHT_ON_WEB)
                CardChoicesFragmentDirections.actionCardChoicesFragmentToBoughtCardChoiceFragment()
            }
            else -> {
                logEvent(TrackingConstants.SIGN_UP_NO_CARDS)
                CardChoicesFragmentDirections.actionCardChoicesFragmentToNoCardChoiceFragment(state.email)
            }
        }
    }

    fun onBackButtonClicked() {
        navigateBack()
    }

    private val fromAppleGoogleSSOAuth = savedStateHandle.get<Boolean>("fromAppleGoogleSSOAuth")!!

    private fun logEvent(property: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SIGN_UP_TYPE_SELECTED)
                .setEmail(state.email)
                .addProperty(TrackingConstants.KEY_PROPERTY, property)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
