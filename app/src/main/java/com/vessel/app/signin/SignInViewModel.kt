package com.vessel.app.signin

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.AuthToken
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.signin.ui.SignInFragmentDirections
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.validators.EmailValidator
import com.vessel.app.util.validators.PasswordValidator
import com.vessel.app.util.validators.ValidatorException
import kotlinx.coroutines.launch

class SignInViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    private val authManager: AuthManager,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,
) : BaseViewModel(resourceRepository), ToolbarHandler {
    val state = SignInState(savedStateHandle.get("email") ?: "")

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onAuthTokenReceived(authToken: AuthToken) {
        showLoading()

        viewModelScope.launch {
            authManager.onAppleGoogleAuth(
                authToken
            )
                .onSuccess {
                    preferencesRepository.onboardingCompleted = true
                    onSignInSuccess()
                }
                .onServiceError {
                    val errorMessage = it.error.message ?: getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onNetworkIOError {
                    val errorMessage = getResString(R.string.network_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onUnknownError {
                    val errorMessage = getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun onContinueClicked() {
        if (!validateParams()) {
            return
        }

        showLoading()
        viewModelScope.launch {
            authManager.login(
                state.userEmail.value.orEmpty(),
                state.userPassword.value.orEmpty()
            )
                .onSuccess {
                    onSignInSuccess()
                }
                .onServiceError {
                    val errorMessage = it.error.message ?: getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onNetworkIOError {
                    val errorMessage = getResString(R.string.network_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onUnknownError {
                    val errorMessage = getResString(R.string.unknown_error)
                    logFailureEvent(errorMessage)
                    showError(errorMessage)
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun onForgotPasswordClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.FORGOT_PASSWORD_TAPPED)
                .withFirebase()
                .withAmplitude()
                .create()
        )
        view.findNavController()
            .navigate(R.id.action_signInFragment_to_forgotPasswordFragment)
    }

    private fun onSignInSuccess() {
        hideLoading()

        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SIGN_IN_SUCCESS)
                .setEmail(state.userEmail.value!!)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )

        if (preferencesRepository.onboardingCompleted) {
            state.navigateTo.value = SignInFragmentDirections.actionSignInFragmentToHome()
            state.closeEvent.call()
        } else {
            state.navigateTo.value = SignInFragmentDirections.actionSignInFragmentToPrivacy()
        }
    }

    private fun logFailureEvent(errorMessage: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SIGN_IN_FAIL)
                .setEmail(state.userEmail.value!!)
                .setError(errorMessage)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun validateParams() = try {
        EmailValidator().validate(state.userEmail)
        PasswordValidator().validate(state.userPassword)
    } catch (exception: ValidatorException) {
        showError(exception)
        false
    }
}
