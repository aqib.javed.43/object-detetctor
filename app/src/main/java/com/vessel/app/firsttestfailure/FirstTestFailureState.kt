package com.vessel.app.firsttestfailure

import androidx.lifecycle.MutableLiveData
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class FirstTestFailureState @Inject constructor() {
    val buttonName = MutableLiveData<String>()
    val moveToTab = MutableLiveData<Int>()
    val showBackWarning = LiveEvent<Unit>()
    val navigateToCapture = LiveEvent<Unit>()

    operator fun invoke(block: FirstTestFailureState.() -> Unit) = apply(block)
}
