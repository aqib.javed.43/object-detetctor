package com.vessel.app.supplementsbucket.ui.incompatibilty

import androidx.lifecycle.MutableLiveData
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class IncompatibityState @Inject constructor(val supplementNameA: String, val supplementNameB: String) {
    val alertBody = MutableLiveData<String>()
    val dismissDialog = LiveEvent<Unit>()

    operator fun invoke(block: IncompatibityState.() -> Unit) = apply(block)
}
