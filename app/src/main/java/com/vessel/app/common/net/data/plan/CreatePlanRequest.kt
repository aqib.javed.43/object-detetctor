package com.vessel.app.common.net.data.plan

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class CreatePlanRequest(
    val food_id: Int? = null,
    val tip_id: Int? = null,
    val reagent_lifestyle_recommendation_id: Int? = null,
    val multiple: Int? = null,
    val day_of_week: List<Int>? = null,
    val time_of_day: String? = null,
    val is_supplements: Boolean? = null,
    val notification_enabled: Boolean? = null,
) : Parcelable

fun PlanData.toPlanRequest() = CreatePlanRequest(
    food_id = this.food_id,
    tip_id = this.tip_id,
    reagent_lifestyle_recommendation_id = this.reagent_lifestyle_recommendation_id,
    multiple = this.multiple,
    day_of_week = if (this.day_of_week.isNullOrEmpty()) listOf(0, 1, 2, 3, 4, 5, 6) else this.day_of_week,
    time_of_day = this.time_of_day,
    is_supplements = this.is_supplements,
    notification_enabled = this.notification_enabled,
)
