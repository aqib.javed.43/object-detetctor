package com.vessel.app.checkscience.ui

import android.view.View

interface OnActionHandler {
    fun onLinkButtonClick(view: View, link: String)
}
