package com.vessel.app.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.renderer.XAxisRenderer
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.Transformer
import com.github.mikephil.charting.utils.Utils
import com.github.mikephil.charting.utils.ViewPortHandler
import com.vessel.app.R
import com.vessel.app.common.model.DateEntry
import java.util.*
import kotlin.math.floor

class DateXAxisRendererWithRounded(
    private val context: Context,
    viewPortHandler: ViewPortHandler,
    xAxis: XAxis,
    trans: Transformer,
    var width: Int,
    var height: Int
) : XAxisRenderer(viewPortHandler, xAxis, trans) {

    private var entries: MutableList<DateEntry>? = null
    private var selectedEntryPosition: Int = -1
    private val paint = Paint(mAxisLabelPaint)
    private val monthTypeface by lazy { ResourcesCompat.getFont(context, R.font.noetext_regular) }
    private val dayTypeface by lazy { ResourcesCompat.getFont(context, R.font.bananagrotesk) }
    private val monthTextSize by lazy { context.resources.getDimensionPixelSize(R.dimen.font_1xs).toFloat() }
    private val monthMarginTop by lazy { context.resources.getDimensionPixelSize(R.dimen.spacing_small) }
    private val dayTextSize by lazy { context.resources.getDimensionPixelSize(R.dimen.font_small).toFloat() }
    private val dayMarginTop by lazy { context.resources.getDimensionPixelSize(R.dimen.spacing_small) }
    private var marginStart = 0
    override fun drawLabel(
        c: Canvas?,
        labelValue: String,
        x: Float,
        y: Float,
        anchor: MPPointF?,
        angleDegrees: Float
    ) {
        var formattedLabel: String? = null
        if (entries == null || labelValue.isBlank()) return
        val value = if (labelValue.toDouble() == floor(labelValue.toDouble())) labelValue.toDouble().toInt() else -1
        if (value in entries!!.indices && entries!![value.toInt()].date != null) {
            val cl = Calendar.getInstance()
            cl.timeInMillis = entries!![value.toInt()].date!!
            formattedLabel = HeaderChartView.DATE_FORMAT.format(cl.time)
        } else {
            formattedLabel = SAMPLE_DATE
        }

        var newY: Float
        formattedLabel?.let {
            if (it == SAMPLE_DATE) return
            it.split("\n").take(2).forEachIndexed { index, string ->
                if (index == 0) {
                    paint.typeface = monthTypeface
                    paint.textSize = monthTextSize
                    marginStart = context.resources.getDimensionPixelSize(R.dimen.spacing_xs)
                    paint.color = ContextCompat.getColor(context, R.color.darkText3)
                    newY = y + monthTextSize + monthMarginTop
                } else {
                    paint.typeface = dayTypeface
                    paint.textSize = dayTextSize
                    // marginStart = context.resources.getDimensionPixelSize(R.dimen.spacing_4xs)
                    paint.color = ContextCompat.getColor(context, R.color.black)
                    newY = y + dayTextSize + dayMarginTop + monthMarginTop
                }

                Utils.drawXAxisValue(
                    c,
                    string,
                    x + marginStart,
                    newY,
                    paint,
                    anchor,
                    angleDegrees
                )
            }

            if (value != selectedEntryPosition) {
                drawImage(
                    c, ContextCompat.getDrawable(context, R.drawable.goal_button_unselected_background),
                    x.toInt() + marginStart, (y + dayTextSize + dayMarginTop + monthMarginTop).toInt(), width, height
                )
            }
            val shouldRedrawLatestPoint = mXAxis.axisMaximum.toInt() < entries!!.size - 1
            if (shouldRedrawLatestPoint && value == entries!!.size - 2 && entries!!.size >= WellnessHeaderChartView.MAX_X_AXIS_LABELS) {
                drawLatestPosition(entries!!.size - 1, x, y, c, anchor, angleDegrees)
            }
        }
    }

    fun updateWidthHeight(width: Int, height: Int) {
        if (this.width == 0) this.width = width
        if (this.height == 0) this.height = height
    }
    fun drawImage(
        canvas: Canvas?,
        drawable: Drawable?,
        x: Int,
        y: Int,
        width: Int,
        height: Int
    ) {
        if (canvas == null) return
        val mDrawableBoundsCache = Rect()
        val drawOffset = MPPointF.getInstance()
        drawOffset.x = (x - width / 2).toFloat()
        drawOffset.y = (y - height / 2).toFloat()
        drawable?.copyBounds(mDrawableBoundsCache)
        drawable?.setBounds(
            mDrawableBoundsCache.left,
            mDrawableBoundsCache.top,
            mDrawableBoundsCache.left + width,
            mDrawableBoundsCache.top + height
        )
        val saveId = canvas.save()
        // translate to the correct position and draw
        canvas.translate(drawOffset.x, drawOffset.y)
        drawable?.draw(canvas)
        canvas.restoreToCount(saveId)
    }

    fun updateData(entries: MutableList<DateEntry>, selectedEntryPosition: Int) {
        Log.d("TAG", "updateData: $selectedEntryPosition")
        this.entries = entries
        this.selectedEntryPosition = selectedEntryPosition
    }
    companion object {
        const val SAMPLE_DATE = "--/n--"
    }
    fun drawLatestPosition(
        value: Int,
        x: Float,
        y: Float,
        c: Canvas?,
        anchor: MPPointF?,
        angleDegrees: Float
    ) {
        val cl = Calendar.getInstance()
        cl.timeInMillis = entries!![value.toInt()].date!!
        val formattedLabel = HeaderChartView.DATE_FORMAT.format(cl.time)
        var newY: Float
        val newX: Float = x + marginStart + width
        formattedLabel.let {
            if (it == SAMPLE_DATE) return
            it.split("\n").take(2).forEachIndexed { index, string ->
                if (index == 0) {
                    paint.typeface = monthTypeface
                    paint.textSize = monthTextSize
                    marginStart = context.resources.getDimensionPixelSize(R.dimen.spacing_xs)
                    paint.color = ContextCompat.getColor(context, R.color.darkText3)
                    newY = y + monthTextSize + monthMarginTop
                } else {
                    paint.typeface = dayTypeface
                    paint.textSize = dayTextSize
                    // marginStart = context.resources.getDimensionPixelSize(R.dimen.spacing_4xs)
                    paint.color = ContextCompat.getColor(context, R.color.black)
                    newY = y + dayTextSize + dayMarginTop + monthMarginTop
                }

                Utils.drawXAxisValue(
                    c,
                    string,
                    newX + marginStart,
                    newY,
                    paint,
                    anchor,
                    angleDegrees
                )
            }

            if (value != selectedEntryPosition) {
                drawImage(
                    c, ContextCompat.getDrawable(context, R.drawable.goal_button_unselected_background),
                    newX.toInt() + marginStart, (y + dayTextSize + dayMarginTop + monthMarginTop).toInt(), width, height
                )
            }
        }
    }
}
