package com.vessel.app.lfaerrors.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LFAErrorModel(
    val reagentId: Int?,
    val slotName: String?,
    val errorCode: Int?
) : Parcelable
