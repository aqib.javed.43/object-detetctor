package com.vessel.app.plan.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.plan.model.DayPlanItem
import com.vessel.app.today.ui.ActivityAdapter
import com.vessel.app.util.extensions.isSameDay
import kotlinx.android.synthetic.main.item_bonus_activities.view.*
import java.util.*

class BonusActivityListAdapter(private val handler: OnActionHandler, private val planHandler: ActivityAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<DayPlanItem>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is DayPlanItem -> R.layout.item_bonus_activities
        else -> throw IllegalStateException(
            "Unexpected WellnessAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }
    interface OnActionHandler {
        fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int)
        fun showBonusActivityInformation()
        fun onTakeTest()
    }
    override fun onBindViewHolder(holder: BaseViewHolder<DayPlanItem>, position: Int) {
        val item = getItem(position)
        holder.itemView.title.visibility = if (position == 0) View.VISIBLE else View.GONE
        holder.itemView.bonusLockLayout.visibility = if (position == 0 && item.isTested) View.GONE else View.VISIBLE
        holder.itemView.findViewById<RecyclerView>(R.id.bonusList).visibility = if (item.isTested) View.VISIBLE else View.GONE
        holder.itemView.takeATestButton.setOnClickListener {
            handler.onTakeTest()
        }
        holder.itemView.title.setOnClickListener {
            handler.showBonusActivityInformation()
        }
        val planAdapter = ActivityAdapter(planHandler, position, item.date.isSameDay(Calendar.getInstance().time))
        holder.itemView.wellDoneLayout.visibility = if (item.isFinishAll()) View.VISIBLE else View.GONE

        holder.itemView.findViewById<RecyclerView>(R.id.bonusList).apply {
            adapter = planAdapter
            itemAnimator = null
            setHasFixedSize(true)
        }

        planAdapter.submitList(item.items)
        super.onBindViewHolder(holder, position)
    }
}
