package com.vessel.app.education.item

import androidx.lifecycle.LifecycleOwner
import com.vessel.app.R
import com.vessel.app.base.BaseAdapter
import com.vessel.app.base.MutableLiveArrayList
import com.vessel.app.covid19.model.FAQ

class EducationItemFaqAdapter(
    items: MutableLiveArrayList<FAQ>,
    lifecycleOwner: LifecycleOwner
) : BaseAdapter<BaseAdapter.ViewHolder>(items, lifecycleOwner) {
    override fun getLayout(position: Int) = R.layout.item_faq
}
