package com.vessel.app.recommendations

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.TodosManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.recommendations.ui.RecommendationTab
import com.vessel.app.recommendations.ui.RecommendationsAdapter
import com.vessel.app.recommendations.ui.RecommendationsPage
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RecommendationsViewModel @ViewModelInject constructor(
    val state: RecommendationsState,
    resourceProvider: ResourceRepository,
    private val buildPlanManager: BuildPlanManager,
    private val todosManager: TodosManager,
    val preferencesRepository: PreferencesRepository,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider),
    RecommendationsAdapter.OnActionHandler,
    ToolbarHandler {

    init {
        loadRecommendationsPlans()
        observeTodayTodoDataState()
    }

    private fun loadRecommendationsPlans() {
        showLoading()
        viewModelScope.launch {
            state.pages.value = buildPlanManager.loadRecommendationsPlans().toMutableList()
            hideLoading(false)
        }
    }

    private fun observeTodayTodoDataState() {
        viewModelScope.launch {
            todosManager.getTodayTodosState().collect {
                if (it) {
                    buildPlanManager.loadPlanData()
                    state.position.value?.let { position ->
                        val pages = state.pages.value
                        pages?.get(position)?.let { recommendationsPage ->
                            pages[position] = RecommendationsPage(
                                recommendationsPage.recommendationTab,
                                recommendationsPage.recommendationsPageMessage,
                                recommendationsPage.recommendationsPageGoal,
                                recommendationsPage.isReagentDeficient,
                                buildPlanManager.checkIfStillHasPlan(recommendationsPage.recommendationTab),
                                buildPlanManager.checkHasSupplementPlan(recommendationsPage.recommendationTab),
                                buildPlanManager.showRedAlertPlanBadge(recommendationsPage.recommendationTab),
                                buildPlanManager.showRedAlertSupplementPlanBadge(recommendationsPage.recommendationTab),
                            )
                            state.pages.value = pages
                        }
                    }
                }
            }
        }
    }

    private fun onMakeSupplementPlanClick(view: View) {
        view.findNavController()
            .navigate(R.id.action_build_plan_to_supplement)
    }

    private fun onMakeFoodPlanClick(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.BUILD_A_FOOD_PLAN_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        view.findNavController()
            .navigate(R.id.action_build_plan_to_food_plan)
    }

    private fun onMakeStressReliefPlanClick(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.BUILD_STRESS_RELIEF_PLAN_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        view.findNavController()
            .navigate(R.id.action_build_plan_to_stress_relief_plan)
    }

    private fun onMakeHydrationPlanClick(view: View) {
        view.findNavController()
            .navigate(R.id.action_recommendationsFragment_to_hydrationPlanFragment)
    }

    private fun makeTestingPlan() {
        showLoading()
        viewModelScope.launch {
            buildPlanManager.onTestingPlanClicked()
                .onSuccess {
                    todosManager.clearCachedData()
                    todosManager.updateTodayTodosCachedData()
                    todosManager.updateRecurringTodosCachedData()
                    hideLoading(false)
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onPlanActionClicked(recommendationTab: RecommendationTab, view: View) {
        when (recommendationTab) {
            RecommendationTab.NUTRIENTS -> onMakeFoodPlanClick(view)
            RecommendationTab.CORTISOL -> onMakeStressReliefPlanClick(view)
            RecommendationTab.TESTING -> makeTestingPlan()
            RecommendationTab.HYDRATION -> onMakeHydrationPlanClick(view)
        }
    }

    override fun onSupplementPlanActionClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.BUILD_SUPPLEMENT_PLAN_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        onMakeSupplementPlanClick(view)
    }

    override fun onSetReminderClicked(view: View, canAddReminder: Boolean) {
        if (canAddReminder)
            navigateBack()
    }

    fun onPageChanged(position: Int) {
        state.position.value = position
    }
}
