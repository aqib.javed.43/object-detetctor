package com.vessel.app.testplan

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.testplan.model.TestingPlanHeader

class TestingPlanHeaderAdapter(
    val handler: TestingPlanHeaderHandler
) : BaseAdapterWithDiffUtil<TestingPlanHeader>() {

    override fun getLayout(position: Int) = R.layout.item_testing_plan_header

    override fun getHandler(position: Int): Any? = handler

    interface TestingPlanHeaderHandler {
        fun onTestingActionButtonClick()
        fun onTakeTestingActionButtonClick()
    }
}
