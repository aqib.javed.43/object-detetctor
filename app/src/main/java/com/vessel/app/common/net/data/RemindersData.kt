package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RemindersData(
    val pagination: PaginationData,
    val reminders: List<TodoReminderData>
)
