package com.vessel.app.ownplanitem.stepone

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class CreateOwnPlanItemStepOneViewModel @ViewModelInject constructor(
    val state: CreateOwnPlanItemStepOneState,
    private val resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {

    init {
        state.planItemTitleRemainingCharacters.value = getTitleRemainingCharacters()
        state.planItemFrequencyRemainingCharacters.value = getFrequencyRemainingCharacters()
    }

    fun onTitleTextChanged(s: CharSequence) {
        state.planItemTitle = s.toString()
        state.planItemTitleRemainingCharacters.value = getTitleRemainingCharacters()
    }

    fun onFrequencyTextChanged(s: CharSequence) {
        state.planItemFrequency = s.toString()
        state.planItemFrequencyRemainingCharacters.value = getFrequencyRemainingCharacters()
    }

    private fun getTitleRemainingCharacters() = getResString(
        R.string.remaining_characters,
        MAX_ALLOWED_CHARS_LIMIT - state.planItemTitle.length
    )

    private fun getFrequencyRemainingCharacters() = getResString(
        R.string.remaining_characters,
        MAX_ALLOWED_CHARS_LIMIT - state.planItemFrequency.length
    )

    fun onCloseClicked() {
        state.closeEvent.call()
    }

    fun onContinueClicked() {
        if (state.planItemFrequency.isNullOrEmpty().not() &&
            state.planItemTitle.isNullOrEmpty().not()
        ) {
            state.navigateTo.value =
                CreateOwnPlanItemStepOneFragmentDirections.actionCreateOwnPlanItemStepOneFragmentToCreateOwnPlanItemStepTwoFragment(
                    state.planItemTitle,
                    state.planItemFrequency
                )
        }
    }

    companion object {
        const val MAX_ALLOWED_CHARS_LIMIT = 30
    }
}
