package com.vessel.app.common.model

import androidx.annotation.StringRes
import com.vessel.app.R

enum class Allergies(@StringRes val title: Int, val apiName: String, val isNone: Boolean) {

    Eggs(R.string.eggs, "eggs", false),
    Fish(R.string.fish, "fish", false),
    TreeNuts(R.string.tree_nuts, "treeNuts", false),
    Wheat(R.string.wheat, "wheat", false),
    Soy(R.string.soy, "soybeans", false),
    Peanuts(R.string.peanuts, "peanuts", false),
    Crustacean(R.string.crustacean, "crustacean", false),
    Milk(R.string.milk, "milk", false),
    Gluten(R.string.gluten, "gluten", false),
    Seeds(R.string.seeds, "seeds", false),
    None(R.string.none_of_the_above, "none", true);

    companion object {
        fun fromApiName(apiName: String) = Allergies.values().firstOrNull { it.apiName == apiName }
    }
}
