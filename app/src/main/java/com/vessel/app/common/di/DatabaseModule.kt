package com.vessel.app.common.di

import android.content.Context
import androidx.room.Room
import com.vessel.app.common.database.DATABASE_NAME
import com.vessel.app.common.database.VesselDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {
    @Singleton
    @Provides
    fun database(@ApplicationContext context: Context) = Room.databaseBuilder(
        context,
        VesselDatabase::class.java,
        DATABASE_NAME
    ).build()

    @Singleton
    @Provides
    fun covidTestDao(vesselDatabase: VesselDatabase) = vesselDatabase.covidTestDao()
}
