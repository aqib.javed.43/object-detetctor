package com.vessel.app.coach

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.TrailLockItem
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CoachState @Inject constructor() {
    val openMessaging = LiveEvent<Any>()
    val navigateTo = LiveEvent<NavDirections>()
    val goal = LiveEvent<GoalRecord?>()
    val isTrialLocked = MutableLiveData<Boolean>()
    val trialLockItem = MutableLiveData<TrailLockItem>()
    operator fun invoke(block: CoachState.() -> Unit) = apply(block)
}
