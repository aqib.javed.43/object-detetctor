package com.vessel.app.onboarding.food

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.onboarding.food.model.FoodSelectModel
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnboardingFoodState @Inject constructor(val isLastStep: Boolean) {
    val items = MutableLiveData<List<FoodSelectModel>>()
    val hint = MutableLiveData<String>()
    val refreshItems = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()
    val navigateToHome = LiveEvent<Unit>()

    operator fun invoke(block: OnboardingFoodState.() -> Unit) = apply(block)
}
