Here are the reference ranges we use for urine pH:

Low: 0 - 6.25

Good: 6.25 - 7.75

High: >7.75

First of all, let’s talk about what a pH scale is. pH measures how acidic or basic a substance is (in this case, urine). The lower your pH, the more acidic your urine. The higher your pH, the more alkaline your urine. A pH scale is logarithmic; that is, a difference of 1 pH is actually a 10x difference in acidity. For example, a pH of 3 is 10x more acidic than a pH of 4 and 100x (10 x 10) more acidic than a pH of 5.

When you look at your graph in the app, you’ll see a low zone (acidic urine), good zone (balanced pH), and high zone (alkaline urine). The sweet spot for urine pH is between 6.25 and 7.75.

Less than that means your urine is very acidic and may need some additional mineral support to increase alkalinity. More than that means your urine is very alkaline which can be caused by mineral imbalance, infection, or high alkalinity foods and water.

**references:**

-   The reference below provides an introduction to the pH scale and how to interpret it.
-   http://chemistry.elmhurst.edu/vchembook/184ph.html

-   The reference below provides an introduction to the pH scale. This site also reviews urine pH and what you can extrapolate from it.
-   https://sciencebasedpharmacy.wordpress.com/2009/11/13/your-urine-is-not-a-window-to-your-body-ph-balancing-a-failed-hypothesis/

-   https://labtestsonline.org/tests/urinalysis

-   https://www.medicalnewstoday.com/articles/323957.php#summary

-   https://www.kidney.org/sites/default/files/11-10-1815_HBE_PatBro_Urinalysis_v6.pdf

-   https://sciencebasedpharmacy.wordpress.com/2009/11/13/your-urine-is-not-a-window-to-your-body-ph-balancing-a-failed-hypothesis/

-   References for pediatric ranges:
-   Note: Newborn babies have acidic urine (pH 5.4-5.9) and rises in 2 - 4 days after birth reaching 6.9-7.8 in breastfed babies and 5.4-6.9 in formula fed babies.

-   https://testresult.org/en/components-description/urinalysis/urine-ph

-   https://www.researchgate.net/publication/256823054_Pediatric_urine_testing

-   https://www.sciencedirect.com/science/article/pii/S1684118219301331?via%3Dihub#bib7
