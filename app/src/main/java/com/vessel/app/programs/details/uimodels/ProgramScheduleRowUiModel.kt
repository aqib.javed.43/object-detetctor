package com.vessel.app.programs.details.uimodels

import android.os.Parcelable
import android.text.format.DateUtils
import androidx.annotation.DrawableRes
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.common.model.SchedulePlanItem
import com.vessel.app.util.extensions.formatDayDate
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class ProgramScheduleRowUiModel(
    val date: Date,
    val day: Int,
    val isUserEnrolled: Boolean,
    val items: List<SchedulePlanItem>
) : Parcelable {
    @DrawableRes
    fun getBackground() =
        if (isUserEnrolled && DateUtils.isToday(date.time)) {
            R.drawable.green_background
        } else {
            if (isInFuture()) R.drawable.white_transparent_background
            else R.drawable.gray_background
        }

    fun getAlpha() =
        if (isUserEnrolled && date.before(Constants.DAY_DATE_FORMAT.parse(Date().formatDayDate()))) {
            0.5f
        } else {
            1f
        }
    fun isInFuture(): Boolean {
        return date.after(Calendar.getInstance().time)
    }
}
