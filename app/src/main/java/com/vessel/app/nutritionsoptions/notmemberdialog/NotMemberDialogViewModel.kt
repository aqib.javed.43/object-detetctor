package com.vessel.app.nutritionsoptions.notmemberdialog

import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.NavDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.MembershipManager
import com.vessel.app.common.manager.MembershipManager.Companion.ZENDESK_NUTRITIONIST_DEPARTMENT
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration

class NotMemberDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    val authManager: AuthManager,
    val membershipManager: MembershipManager,
    val preferencesRepository: PreferencesRepository,
    val remoteConfiguration: RemoteConfiguration,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    val dismissScreen = LiveEvent<Unit>()
    val openMessaging = LiveEvent<Any>()
    val navigateTo = LiveEvent<NavDirections>()

    init {
        remoteConfiguration.fetchConfigFromRemote()
    }

    fun dismiss() {
        dismissScreen.call()
    }

    fun onUnderstandClicked() {
        dismissScreen.call()
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TALK_TO_NUTRITIONIST_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        openMessaging.call()
    }

    fun getChatConfiguration() =
        membershipManager.getChatConfiguration(ZENDESK_NUTRITIONIST_DEPARTMENT)

    fun isLiveChat() = remoteConfiguration.getBoolean("use_livechat")
    fun getUserName() = preferencesRepository.userFirstName
    fun getUserEmail() = preferencesRepository.userEmail
}
