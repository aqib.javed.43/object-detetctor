package com.vessel.app.common.model

import android.os.Parcelable
import com.vessel.app.common.net.data.PaginationData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProgramModel(
    val program: List<Program>,
    val pagination: PaginationData
) : Parcelable
