package com.vessel.app.common.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScheduleItem(
    val day: Int,
    val plans: List<SchedulePlanItem>
) : Parcelable
