-   Proton pump inhibitors: if taken for longer periods (over 1 year) can lead to low magnesium levels. Examples include Nexium, Prilosec, Prevacid, Pronix, Aciphex.
-
    [https://www.fda.gov/drugs/drug-safety-and-availability/fda-drug-safety-communication-low-magnesium-levels-can-be-associated-long-term-use-proton-pump](https://www.fda.gov/drugs/drug-safety-and-availability/fda-drug-safety-communication-low-magnesium-levels-can-be-associated-long-term-use-proton-pump)
-   Diuretics: Thiazide diuretics (hydrochlorothiazide, chlorthalidone, and others) and loop diuretics (furosemide and bumetanide) increase urinary excretion of magnesium.
-   Potassium-sparing diuretics may reduce urinary excretion of magnesium. Magnesium supplementation could theoretically cause hypermagnesemia in patients taking potassium-sparing diuretics Examples include: amiloride (Midamor), spironolactone (Aldactone), and triamterene (Dyrenium).
-   Magnesium Drug Interactions
-
    [https://www.webmd.com/vitamins/ai/ingredientmono-998/magnesium](https://www.webmd.com/vitamins/ai/ingredientmono-998/magnesium)
