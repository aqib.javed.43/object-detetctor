package com.vessel.app.forceupdate

import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ForceUpdateState @Inject constructor() {
    val dismissDialog = LiveEvent<Unit>()
    val updateClicked = LiveEvent<Unit>()
    val isDialogCancelable = LiveEvent<Boolean>()
    operator fun invoke(block: ForceUpdateState.() -> Unit) = apply(block)
}
