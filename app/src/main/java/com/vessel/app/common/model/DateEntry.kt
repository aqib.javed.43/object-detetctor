package com.vessel.app.common.model

import com.github.mikephil.charting.data.Entry
import com.vessel.app.Constants

open class DateEntry(
    x: Float,
    y: Float?,
    val date: Long?,
    var reagentsCount: Int = 0,
    val showNumber: Boolean = true,
    val uuid: String? = ""
) : Entry(x, y ?: Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE)
