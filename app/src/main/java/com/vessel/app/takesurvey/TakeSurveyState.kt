package com.vessel.app.takesurvey

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.activationtimer.ActivationTimerState
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class TakeSurveyState @Inject constructor() {
    var uuid: String? = null
    lateinit var questions: Array<SurveyQuestion>
    private var currentQuestionIndex = 0

    fun nextQuestionIndex(): Int {
        if (currentQuestionIndex in 0..questions.size - 2)
            currentQuestionIndex += 1
        return currentQuestionIndex()
    }

    fun currentQuestionIndex() = currentQuestionIndex

    var timerStart: String? = null
    val title = MutableLiveData(R.string.your_card_is_activating)
    val minutes = MutableLiveData("03")
    val seconds = MutableLiveData("30")
    val progress = MutableLiveData(0)
    val navigateToIndex = LiveEvent<Int>()
    val navigateTo = LiveEvent<NavDirections>()
    val showTip = LiveEvent<Boolean>()
    val showGoodJob = LiveEvent<Boolean>()
    val finishSurvey = LiveEvent<Unit>()
    val skipSurvey = LiveEvent<Unit>()
    val currentQuestion = MutableLiveData<SurveyQuestion>()
    var completed = MutableLiveData<Boolean>(false)

    fun updateTimerText(minutes: String, seconds: String) {
        this.minutes.value = minutes
        this.seconds.value = seconds
    }

    fun setCompleted() {
        updateTimerText(ActivationTimerState.FINISH_VALUE, ActivationTimerState.FINISH_VALUE)
        progress.value = ActivationTimerState.PROGRESS_MAX
        title.value = R.string.time_is_up
        completed.value = true
    }

    companion object {
        const val PROGRESS_MAX = 10000
        const val FINISH_VALUE = "00"
    }

    operator fun invoke(block: TakeSurveyState.() -> Unit) = apply(block)
}
