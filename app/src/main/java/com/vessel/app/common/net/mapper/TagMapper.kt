package com.vessel.app.common.net.mapper

import com.vessel.app.common.net.data.*
import com.vessel.app.wellness.model.Tag
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TagMapper @Inject constructor() {
    fun map(data: TagsResponse) = data.tags.map { map(it) }

    fun map(data: TagData) = Tag(
        name = data.title,
        isActive = data.is_active,
        id = data.id,
        isSelected = false
    )
}
