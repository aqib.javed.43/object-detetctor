package com.vessel.app.foodplan.ui.details

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.activityreview.model.ActivityReviewItem
import com.vessel.app.activityreview.model.RecommendationReview
import com.vessel.app.activityreview.ui.ActivityReviewAdapter
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.adapter.genericlikeordislikeadapter.LikeOrDislikeAdapter
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.impactgoals.ImpactGoalsAdapter
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentAdapter
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentModel
import com.vessel.app.common.adapter.planedit.PlanEditAdapter
import com.vessel.app.common.adapter.planedit.PlanEditItem
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.common.adapter.planreminders.toReminderList
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.FoodRecommendation
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.Food
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.foodplan.model.FoodPlanDetailsHeader
import com.vessel.app.foodplan.model.FoodReviewItem
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.DeleteReviewRequest
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.NumberFormat
import kotlin.math.ceil

class FoodPlanDetailsViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val planManager: PlanManager,
    private val reminderManager: ReminderManager,
    private val foodManager: FoodManager,
    private val preferencesRepository: PreferencesRepository,
    private val recommendationManager: RecommendationManager,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    FoodPlanDetailsHeaderAdapter.OnActionHandler,
    PlanEditAdapter.OnActionHandler,
    ReminderRowAdapter.OnActionHandler,
    ImpactGoalsAdapter.OnActionHandler,
    ActivityReviewAdapter.OnActionHandler,
    LikeOrDislikeAdapter.OnActionHandler<FoodPlanDetailsHeader>,
    ImpactTestReagentAdapter.OnActionHandler {
    private var userPlans = listOf<PlanData>()
    private var recommendationItem: FoodRecommendation? = null

    val state =
        FoodDetailsState(savedStateHandle["foodDetailsItem"]!!)

    init {
        observeRemindersDataState()
        loadHeaderData()
        getGoals()
        getImpactReagents()
        getReviews()
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_VIEWED)
                .addProperty(TrackingConstants.KEY_ID, state.header.value?.id.toString())
                .addProperty(TrackingConstants.TYPE, LikeCategory.Food.value)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    private fun loadHeaderData(forceUpdate: Boolean = false) {
        showLoading()
        viewModelScope.launch {
            val recommendationsResponse = foodManager.getRecommendedFoodItems()
            val plansResponse = planManager.getUserPlans(forceUpdate)
            if (plansResponse is Result.Success && recommendationsResponse is Result.Success) {
                hideLoading(false)
                val plans = plansResponse.value.filter {
                    it.isFoodPlan() && it.food_id == state.itemDetails.foodId
                }

                val recommendedItem =
                    recommendationsResponse.value.firstOrNull { it.foodId == state.itemDetails.foodId }
                        ?: return@launch

                recommendationItem = recommendedItem

                setupReminderPlans(plans)
                state.header.value = FoodPlanDetailsHeader(
                    state.itemDetails.title,
                    state.itemDetails.description,
                    recommendedItem.imageUrl,
                    plans.isNotEmpty(),
                    recommendedItem.foodId!!,
                    recommendedItem.likeStatus!!,
                    recommendedItem.totalLikes ?: 0,
                    totalDislikes = recommendedItem.dislikes_count ?: 0
                )
            }
        }
    }

    private fun setupReminderPlans(foodPlans: List<PlanData>) {
        val plans = foodPlans.toReminderList()
        userPlans = foodPlans
        state.reminderItems.value = plans
    }

    private fun getGoals() {
        viewModelScope.launch {
            foodManager.getRecommendedFoodItems().onSuccess { foodRecommendation ->
                val foodPlan =
                    foodRecommendation.firstOrNull {
                        it.foodId == state.itemDetails.foodId
                    } ?: return@onSuccess
                val goals = foodPlan.goals.orEmpty().mapIndexed { index, goal ->
                    ImpactGoalModel(
                        goal.name,
                        goal.image_small_url.orEmpty(),
                        Goal.alternateBackground(index),
                    )
                }

                state.goalsItems.value = goals
            }
        }
    }

    private fun getImpactReagents() {
        viewModelScope.launch {
            foodManager.getRecommendedFoodItems().onSuccess { recommendation ->
                val foodPlan =
                    recommendation.firstOrNull {
                        it.foodId == state.itemDetails.foodId
                    } ?: return@onSuccess
                val impactedReagents =
                    foodPlan.nutrients.filter {
                        it.reagent.id != ReagentItem.B9.id
                    }.map {
                        ImpactTestReagentModel(
                            it.reagent.displayName,
                            it.reagent.headerImage,
                            "${
                            NumberFormat.getInstance().format(ceil(it.amount.toDouble()))
                            } ${it.reagent.consumptionUnit}"
                        )
                    }

                state.reagentInfo.value = impactedReagents
            }
        }
    }

    private fun observeRemindersDataState() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    planManager.clearCachedData()
                    loadHeaderData(true)
                }
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onPlanEditItemClick(item: PlanEditItem, view: View) {
        navigateToReminders(state.header.value!!)
    }

    override fun onReminderItemClick(item: ReminderRowUiModel, view: View) {
        navigateToReminders(state.header.value!!)
    }

    override fun onGoalItemClick(item: ImpactGoalModel, view: View) {
    }

    override fun onReagentItemClick(item: ImpactTestReagentModel, view: View) {
    }

    fun navigateToReminders(item: FoodPlanDetailsHeader) {
        state.navigateTo.value =
            FoodPlanDetailsFragmentDirections.actionFoodPlanDetailsFragmentToPlanReminderDialog(
                PlanReminderHeader(
                    item.tittle,
                    "${recommendationItem?.servingSize?.display_quantity} ${recommendationItem?.servingSize?.unit}",
                    item.image,
                    0,
                    if (item.isAdded) userPlans else listOf(
                        PlanData(
                            food_id = state.itemDetails.foodId,
                            food = Food(
                                food_title = state.itemDetails.title,
                                serving_unit = recommendationItem?.servingSize?.unit,
                                serving_quantity = recommendationItem?.servingSize?.quantity?.toDouble(),
                                image_url = item.image,
                                id = -1
                            ),
                            multiple = 1
                        )
                    ),
                    item.isAdded.not()
                )
            )
    }

    override fun onAddToPlanClicked(item: FoodPlanDetailsHeader) {
        if (item.isAdded) {
            if (userPlans.size > 1 || preferencesRepository.dontShowPlanRemoveConfirmationDialog) {
                navigateToReminders(item)
            } else {
                state.removeReminder.value = item
            }
        } else {
            navigateToReminders(item)
        }
    }

    fun deletePlanItem() {
        viewModelScope.launch {
            showLoading(false)
            planManager.deletePlanItem(userPlans.map { it.id ?: -1 })
                .onSuccess {
                    hideLoading(false)
                    planManager.updatePlanCachedData()
                    reminderManager.updateRemindersCachedData()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(
                                TrackingConstants.KEY_ID,
                                userPlans.map { it.id ?: -1 }.toString()
                            )
                            .addProperty(
                                TrackingConstants.TYPE,
                                LikeCategory.Food.value
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Food Plan Details")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    navigateBack()
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    override fun onCheckFoodPlanClicked() {
        state.navigateTo.value =
            FoodPlanDetailsFragmentDirections.actionFoodPlanDetailsFragmentToFoodPlanFragment()
    }

    override fun onItemLikeClicked(item: FoodPlanDetailsHeader) {
        onLikeClicked(item)
    }

    fun onDontShowAgainClicked(isChecked: Boolean) {
        preferencesRepository.dontShowPlanRemoveConfirmationDialog = isChecked
    }
    override fun onLikeClicked(header: FoodPlanDetailsHeader) {
        val likesCount = header.totalLikes
        val dislikesCount = header.totalDislikes
        val newLikesCount: Int
        var newDislikesCount: Int
        if (header.likeStatus != LikeStatus.LIKE) {
            newLikesCount = likesCount.plus(1) ?: 0
            newDislikesCount = header.totalDislikes ?: 0
            if (header.likeStatus == LikeStatus.DISLIKE) newDislikesCount = newDislikesCount.minus(1)
            sendLikeStatus(header, LikeStatus.LIKE, newLikesCount, newDislikesCount)
        } else {
            newLikesCount = likesCount.minus(1) ?: 0
            unLikeStatus(header, LikeStatus.NO_ACTION, newLikesCount, dislikesCount)
        }
    }

    override fun onDisLikeClicked(header: FoodPlanDetailsHeader) {
        val likesCount = header.totalLikes
        val dislikesCount = header.totalDislikes
        val newLikesCount: Int
        val newDislikesCount: Int
        if (header.likeStatus != LikeStatus.DISLIKE) {
            newLikesCount =
                when (header.likeStatus) {
                    LikeStatus.NO_ACTION -> likesCount ?: 0
                    LikeStatus.LIKE -> likesCount.minus(1) ?: 0
                    else -> return
                }
            newDislikesCount = dislikesCount.plus(1) ?: 0
            sendLikeStatus(header, LikeStatus.DISLIKE, newLikesCount, newDislikesCount)
        } else {
            newLikesCount = dislikesCount.minus(1) ?: 0
            unLikeStatus(header, LikeStatus.NO_ACTION, likesCount, newLikesCount)
        }
    }
    private fun sendLikeStatus(
        header: FoodPlanDetailsHeader,
        likeStatus: LikeStatus,
        likesCount: Int,
        dislikesCount: Int
    ) {
        state.header.value =
            state.header.value?.copy(likeStatus = likeStatus, totalLikes = likesCount, totalDislikes = dislikesCount)
        viewModelScope.launch {
            recommendationManager.sendLikesStatus(
                header.id,
                LikeCategory.Food.value,
                likeStatus == LikeStatus.LIKE
            )
            if (likeStatus == LikeStatus.LIKE) preferencesRepository.updateLikedPlans(state.itemDetails.foodId.toString())
            else preferencesRepository.updateDislikedPlans(state.itemDetails.foodId.toString())
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.CONTENT_LIKED)
                    .addProperty(TrackingConstants.KEY_ID, header.id.toString())
                    .addProperty(TrackingConstants.TYPE, LikeCategory.Food.value)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }

    private fun unLikeStatus(
        header: FoodPlanDetailsHeader,
        likeStatus: LikeStatus,
        likesCount: Int,
        dislikesCount: Int
    ) {
        state.header.value =
            state.header.value?.copy(likeStatus = LikeStatus.NO_ACTION, totalLikes = likesCount, totalDislikes = dislikesCount)
        viewModelScope.launch {
            recommendationManager.unLikesStatus(
                header.id,
                LikeCategory.Food.value,
            )
            preferencesRepository.updateUnlikedPlans(state.itemDetails.foodId.toString())
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.CONTENT_UNLIKED)
                    .addProperty(TrackingConstants.KEY_ID, header.id.toString())
                    .addProperty(TrackingConstants.TYPE, LikeCategory.Food.value)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }

    override fun onWriteNewReview() {
        gotoReview()
    }

    override fun onDeleteReview(item: RecommendationReview, position: Int) {
        deleteReview(position)
    }

    override fun onEditReview(item: RecommendationReview) {
        gotoReview()
    }

    override fun onFlagReview(item: RecommendationReview) {
    }
    fun gotoReview() {
        val reviewItem = ActivityReviewItem()
        reviewItem.category = LikeCategory.Food.value
        reviewItem.record_id = state.itemDetails.foodId
        reviewItem.likeStatus = state.header.value?.likeStatus
        reviewItem.image = state.header.value?.image
        reviewItem.title = state.itemDetails.title
        reviewItem.programId = state.itemDetails.foodId
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalActivityReviewFragment(reviewItem)
    }
    fun deleteReview(position: Int) {
        val request = DeleteReviewRequest(record_id = state.itemDetails.foodId, category = LikeCategory.Food.value)
        viewModelScope.launch {
            showLoading()
            recommendationManager.deleteReview(
                request
            ).onSuccess {
                hideLoading()
                state {
                    val items = reviews.value?.firstOrNull()?.items?.toMutableList() ?: arrayListOf()
                    if (!items.isNullOrEmpty())items.removeAt(position)
                    state.reviews.value = state.reviews.value.orEmpty().mapIndexed { index, foodReviewItem ->
                        if (index == 0) foodReviewItem.copy(items = items, title = String.format(getResString(R.string.member_review_count), items.size.toString()))
                        else foodReviewItem.copy()
                    }
                }
            }.onError {
                hideLoading()
                showError(it.toString())
            }
        }
    }
    private fun getReviews() {
        state.itemDetails.foodId.let {
            viewModelScope.launch {
                recommendationManager.getReviews(
                    it,
                    LikeCategory.Food.value
                )
                    .onSuccess { response ->
                        val items = response.reviews?.map {
                            it.copy(myContact = preferencesRepository.contactId)
                        } ?: arrayListOf()
                        val title = String.format(getResString(R.string.member_review_count), items.size.toString())
                        val foodReviewItem = FoodReviewItem(title, items)
                        state.reviews.value = listOf(foodReviewItem)
                    }
            }
        }
    }
}
