package com.vessel.app.hydrationplan.model

import android.text.SpannedString
import androidx.annotation.StringRes
import com.vessel.app.R

data class HydrationPlanHeaderModel(
    val title: SpannedString?,
    val isAdded: Boolean,
) {
    @StringRes
    val actionTitle: Int = if (isAdded) {
        R.string.remove_from_plan
    } else {
        R.string.add_to_plan
    }

    val showTitle = title.isNullOrEmpty().not()
}
