package com.vessel.app.onboardingintro.introhomepage

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class IntroHomePageState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: IntroHomePageState.() -> Unit) = apply(block)
}
