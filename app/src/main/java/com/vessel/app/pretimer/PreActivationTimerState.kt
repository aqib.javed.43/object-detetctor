package com.vessel.app.pretimer

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.activationtimer.ActivationTimerState
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class PreActivationTimerState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    var timerStart: String? = null
    val title = MutableLiveData(R.string.your_card_is_activating)
    val minutes = MutableLiveData("03")
    val seconds = MutableLiveData("30")
    val progress = MutableLiveData(0)
    var completed = MutableLiveData<Boolean>(false)
    val showBackWarning = LiveEvent<Unit>()

    fun updateTimerText(minutes: String, seconds: String) {
        this.minutes.value = minutes
        this.seconds.value = seconds
    }

    fun setCompleted() {
        updateTimerText(ActivationTimerState.FINISH_VALUE, ActivationTimerState.FINISH_VALUE)
        progress.value = ActivationTimerState.PROGRESS_MAX
        title.value = R.string.time_is_up
        completed.value = true
    }

    companion object {
        const val PROGRESS_MAX = 10000
        const val FINISH_VALUE = "00"
    }

    operator fun invoke(block: PreActivationTimerState.() -> Unit) = apply(block)
}
