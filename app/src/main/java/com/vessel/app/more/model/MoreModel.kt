package com.vessel.app.more.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MoreModel(
    @StringRes val itemName: Int,
    @DrawableRes val icon: Int,
) : Parcelable

enum class MoreItems(val moreModel: MoreModel) {
    MY_ACCOUNT(MoreModel(R.string.my_account, R.drawable.ic_my_account)),
    TAKE_A_TEST(MoreModel(R.string.take_a_test, R.drawable.ic_take_test_menu)),
    ORDER_CARDS(MoreModel(R.string.order_cards, R.drawable.ic_order_cards)),
    CUSTOM_SUPPLEMENT(MoreModel(R.string.custom_supplement, R.drawable.ic_pill)),
    CHAT_TO_NUTRITIONIST(MoreModel(R.string.chat_to_nutritionist, R.drawable.ic_chat_black_with_white_border)),
    BACKED_BY_SCIENCE(MoreModel(R.string.backed_by_science, R.drawable.ic_backed_by_science)),
    SUPPORT(MoreModel(R.string.support, R.drawable.ic_support))
}
