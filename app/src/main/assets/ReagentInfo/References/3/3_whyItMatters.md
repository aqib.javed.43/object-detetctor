Typically, your body burns glucose (sugar) for energy. When you cut carbs a LOT and replace the calories with fat and some protein, your body makes a metabolic shift to use ketones (aka ketone bodies) as the main fuel instead of glucose. There are three types of ketone bodies that the liver makes, acetoacetate, which accounts for about 20% of total ketones, beta-hydroxybutyrate, which accounts for about 78% of total ketones, and acetone which accounts for about 2% of total ketones.

When you initially start a ketogenic diet, the majority of your urinary ketones will be Ketone A (acetoacetate), and after several weeks this gradually is replaced by Ketone B (beta-hydroxybutyrate). Because of this we report the sum total of both Ketone A and B to give you an accurate measure of your total ketones whether you’re new to a ketogenic diet or a veteran.

Being in a state of nutritional ketosis may lead to improved brain function, clearer thoughts, more energy, fat loss, improvements in cholesterol, and more. Urine ketone measurements may also be an indicator of how well you’re doing with intermittent fasting, also called time-restricted eating, which may have additional health benefits.

Severely elevated ketones may be a sign of diabetic ketoacidosis (DKA), a life-threatening condition associated with Type I Diabetes that may lead to coma or even death.

**references:**

-   [https://pubmed.ncbi.nlm.nih.gov/28599043/](https://pubmed.ncbi.nlm.nih.gov/28599043/)

-   [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3826507/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3826507/)
