package com.vessel.app.foodplan.ui

import androidx.core.view.isVisible
import com.google.android.material.tabs.TabLayout
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodplan.model.TabsHeaderItem

class FoodTabsHeaderAdapter(
    val handler: FoodTabsHeaderHandler
) : BaseAdapterWithDiffUtil<TabsHeaderItem>() {
    override fun getLayout(position: Int) = R.layout.item_food_tabs_header

    override fun getHandler(position: Int): FoodTabsHeaderHandler = handler

    override fun onBindViewHolder(holder: BaseViewHolder<TabsHeaderItem>, position: Int) {
        super.onBindViewHolder(holder, position)

        val item = getItem(position)
        holder.itemView.findViewById<TabLayout>(R.id.filterTabLayout).apply {
            isVisible = item.showTabs
            clearOnTabSelectedListeners()
            removeAllTabs()
            addTab(this.newTab().setText(context.getString(R.string.daily_plan)))
            addTab(this.newTab().setText(context.getString(R.string.weekly_plan)))

            if (item.isDailySelected) {
                getTabAt(0)?.select()
            } else {
                getTabAt(1)?.select()
            }

            addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    if (tab.position == TabsHeaderItem.DAILY_TAB_POSITION) {
                        this@FoodTabsHeaderAdapter.handler.onDailySelected()
                    } else if (tab.position == TabsHeaderItem.WEEKLY_TAB_POSITION) {
                        this@FoodTabsHeaderAdapter.handler.onWeeklySelected()
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {
                }

                override fun onTabReselected(tab: TabLayout.Tab) {
                }
            })
        }
    }

    interface FoodTabsHeaderHandler {
        fun onDailySelected()
        fun onWeeklySelected()
    }
}
