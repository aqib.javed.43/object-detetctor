package com.vessel.app.foodplan

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.checkscience.model.ScienceReference
import com.vessel.app.filterfoodplandialog.model.FilterAndSortDialogOptions
import com.vessel.app.foodplan.model.FoodDailyItem
import com.vessel.app.foodplan.model.FoodLevelViewItem
import com.vessel.app.foodplan.model.FoodRecommendationViewItem
import com.vessel.app.foodplan.model.TabsHeaderItem
import com.vessel.app.foodplan.model.FoodPlanItem
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class FoodPlanState @Inject constructor() {
    val levels = MutableLiveData<List<FoodLevelViewItem>>()
    val recommendedFoodsNotSelected = MutableLiveData<List<FoodRecommendationViewItem>>()
    val recommendedFoodsSelected = MutableLiveData<List<FoodRecommendationViewItem>>()
    val recommendedFoodsSelectedHeader = MutableLiveData<List<Unit>>()
    val recommendedFoodsNotSelectedHeader = MutableLiveData<List<Unit>>()
    val foodTabs = MutableLiveData<List<TabsHeaderItem>>()
    val showStickyFoodPlanRecyclerView = MutableLiveData(false)
    val showStickySearchLayoutRecyclerView = MutableLiveData(false)
    var searchQuery = ""
    var removeSearchQuery = LiveEvent<Unit>()
    val showEmptySearchResultFoodItem = MutableLiveData(false)
    var selectedFilterAndSortOptions = FilterAndSortDialogOptions()
    val navigateTo = LiveEvent<NavDirections>()
    val showFoodPlanAddedRemovePopUp = MutableLiveData<Boolean>()

    var todayPlansData = mutableListOf<FoodDailyItem>()
    var weeklyPlansData = mutableListOf<FoodDailyItem>()
    var isDailyView: Boolean = true
    val foodItems = MutableLiveData<List<FoodDailyItem>>()
    val removeReminder = LiveEvent<FoodPlanItem>()

    fun getCheckOutReferences(): Array<ScienceReference> {
        return arrayOf(
            ScienceReference(
                R.string.food_supplement_reference_1,
                R.string.food_supplement_text_1,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_2,
                R.string.food_supplement_text_2,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_3,
                R.string.food_supplement_text_3,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_4,
                R.string.food_supplement_text_4,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_5,
                R.string.food_supplement_text_5,
                android.R.color.transparent
            ),
            ScienceReference(
                R.string.food_supplement_reference_6,
                R.string.food_supplement_text_6,
                android.R.color.transparent
            )
        )
    }

    val page = MutableLiveData(1)
    operator fun invoke(block: FoodPlanState.() -> Unit) = apply(block)
}
