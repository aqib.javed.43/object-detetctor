package com.vessel.app.common.manager

import com.vessel.app.common.model.ErrorMessage
import com.vessel.app.common.model.FoodRecommendation
import com.vessel.app.common.model.Result
import com.vessel.app.common.net.payload.SearchFilterPayload
import com.vessel.app.common.repo.FoodRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.filterfoodplandialog.model.SortOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FoodManager @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    private val foodRepository: FoodRepository,
) {
    private val foodRecommendationDataStateChannel = BroadcastChannel<Boolean>(1)

    var recommendedFoodItems: List<FoodRecommendation>? = null
    var foodItems: List<FoodRecommendation>? = null

    suspend fun searchFood(
        foodTitle: String = "",
        foodCategories: List<String>? = emptyList(),
        nutrients: List<String> = emptyList(),
        sortOption: SortOptions
    ) = foodRepository.searchFilterFood(
        SearchFilterPayload(
            food_title = foodTitle,
            food_categories = foodCategories.orEmpty(),
            diets = preferencesRepository.getContact()?.diets?.filterNotNull().orEmpty()
                .filterNot { it.isNone }.map { it.apiName },
            allergies = preferencesRepository.getContact()?.allergies?.filterNotNull().orEmpty()
                .filterNot { it.equals("none") },
            nutrients = nutrients
        ),
        sortOption = sortOption
    )

    suspend fun getRecommendedFoodItems(forceUpdate: Boolean = false): Result<List<FoodRecommendation>, ErrorMessage> {
        return if (forceUpdate || recommendedFoodItems.isNullOrEmpty()) {
            searchFood(sortOption = SortOptions.RECOMMENDED)
                .onSuccess {
                    recommendedFoodItems = it
                }
        } else {
            Result.Success(recommendedFoodItems!!)
        }
    }
    suspend fun getRecommendedFoodItems(forceUpdate: Boolean = false, apiName: String): Result<List<FoodRecommendation>, ErrorMessage> {
        return if (forceUpdate || recommendedFoodItems.isNullOrEmpty()) {
            searchFood(sortOption = SortOptions.RECOMMENDED)
                .onSuccess {
                    recommendedFoodItems = it
                }
        } else {
            Result.Success(recommendedFoodItems!!)
        }
    }
    suspend fun getFoodItems(forceUpdate: Boolean = false): Result<List<FoodRecommendation>, ErrorMessage> {
        return if (forceUpdate || foodItems.isNullOrEmpty()) {
            foodRepository.getFoodItems()
                .onSuccess {
                    foodItems = it
                }
        } else {
            Result.Success(foodItems!!)
        }
    }

    fun updateFoodCachedData(notifyUpdates: Boolean = true) {
        CoroutineScope(SupervisorJob()).launch {
            if (getRecommendedFoodItems(true) is Result.Success && notifyUpdates) {
                updateFoodCachedDataState()
            }
        }
    }

    fun getFoodCachedDataState() = foodRecommendationDataStateChannel.asFlow()

    private fun updateFoodCachedDataState() {
        foodRecommendationDataStateChannel.offer(true)
    }

    fun clearCachedData() {
        recommendedFoodItems = null
        foodItems = null
    }
}
