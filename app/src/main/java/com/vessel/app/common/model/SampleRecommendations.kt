package com.vessel.app.common.model

import com.squareup.moshi.JsonClass
import com.vessel.app.common.net.data.SampleFoodData

@JsonClass(generateAdapter = true)
data class SampleRecommendations(
    val supplements: List<Sample>,
    val food: List<SampleFoodData>,
    val lifestyle: List<Sample>
)
