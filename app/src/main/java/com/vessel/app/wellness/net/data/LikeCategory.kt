package com.vessel.app.wellness.net.data

enum class LikeCategory(val value: String) {
    Tip("tip"),
    Food("food"),
    Program("program"),
    Lifestyle("lifestyle");

    companion object {
        fun fromValue(value: String): LikeCategory = when (value) {
            "tip" -> Tip
            "food" -> Food
            "program" -> Program
            "lifestyle" -> Lifestyle
            else -> throw IllegalArgumentException()
        }
    }
}
