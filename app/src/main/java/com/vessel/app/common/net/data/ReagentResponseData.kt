package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ReagentResponseData(
    val reagents: List<ReagentResponseItemData?>?,
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class ReagentResponseItemData(
    val id: Int?,
    val state: String?,
    val name: String?,
    val unit: String?,
    val supplement_id: Int?,
    val consumption_unit: String?,
    val recommended_daily_allowance: Float?,
    val reagent_type: String?,
    val info: List<ReagentResponseInfoData>?,
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class ReagentResponseInfoData(
    val info_type: String,
    val id: Int?,
    val reagent_id: Int?,
    val title: String?,
    val coming_soon_tag_items: List<String>?,
    val description: String?,
    val coming_soon_image: String?
) : Parcelable
