package com.vessel.app.ready

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.activationtimer.util.TestSurveyStates
import com.vessel.app.activationtimer.util.TimerServiceManager
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.ready.ui.ReadyFragmentDirections
import com.vessel.app.util.ResourceRepository

class ReadyViewModel @ViewModelInject constructor(
    val state: ReadyState,
    val resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val devicePreferencesRepository: DevicePreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {

    fun onStartTheTimerClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.START_THE_TIMER_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )

        if (devicePreferencesRepository.showTipVideoAlertDialog) {
            state.navigateTo.value = ReadyFragmentDirections.actionReadyFragmentToIncompleteVideoWarningDialogFragment()
        } else {
            TimerServiceManager.startTimerCountDown()
            preferencesRepository.setSurveyStates(TestSurveyStates.PRETAKE)
            state.navigateTo.value = ReadyFragmentDirections.actionReadyToActivationTimer()
        }
    }

    fun getTips() = state.getTips() {
        state.navigateTo.value = ReadyFragmentDirections.actionReadyFragmentToInstructionsTipsFragment()
    }

    fun onBackClicked() {
        state.closeEvent.call()
    }
}
