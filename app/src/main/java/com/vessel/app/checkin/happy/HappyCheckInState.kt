package com.vessel.app.checkin.happy

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.plan.model.PlanItem
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.model.GoalButton
import javax.inject.Inject

class HappyCheckInState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val selectedGoal = MutableLiveData<GoalButton>()
    val activitiesItems = MutableLiveData<List<PlanItem>>()
    operator fun invoke(block: HappyCheckInState.() -> Unit) = apply(block)
}
