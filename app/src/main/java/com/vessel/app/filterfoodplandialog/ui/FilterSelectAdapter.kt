package com.vessel.app.filterfoodplandialog.ui

import android.widget.CheckBox
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.filterfoodplandialog.model.FilterSelect

class FilterSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<FilterSelect>() {
    override fun getLayout(position: Int) = R.layout.item_filter_select

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<FilterSelect>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.findViewById<CheckBox>(R.id.checkbox).apply {
            setOnClickListener {
                this@FilterSelectAdapter.handler.onFilterSelectChecked(getItem(position), isChecked)
            }
        }
    }

    interface OnActionHandler {
        fun onFilterSelectChecked(item: FilterSelect, checked: Boolean)
    }
}
