package com.vessel.app.onboarding.food.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.onboarding.food.model.FoodSelectModel

class FoodSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<FoodSelectModel>({ oldItem, newItem ->
        oldItem.id == newItem.id
    }) {
    override fun getLayout(position: Int) = R.layout.item_food_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onFoodSelectClicked(item: FoodSelectModel)
    }
}
