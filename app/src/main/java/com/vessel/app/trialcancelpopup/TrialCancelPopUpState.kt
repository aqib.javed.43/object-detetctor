package com.vessel.app.trialcancelpopup

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class TrialCancelPopUpState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: TrialCancelPopUpState.() -> Unit) = apply(block)
}
