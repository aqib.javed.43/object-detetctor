package com.vessel.app.findcoach.ui

import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.posttestlaststep.PostTestLastStepViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostTestLastStepFragment : BaseFragment<PostTestLastStepViewModel>() {
    override val viewModel: PostTestLastStepViewModel by viewModels()
    override val layoutResId = R.layout.fragment_post_test_last_step
}
