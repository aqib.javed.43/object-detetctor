package com.vessel.app.planreminderdialog.testingreminder

import androidx.lifecycle.MutableLiveData
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class TestingReminderState @Inject constructor(val planHeader: PlanReminderHeader) {

    val dismissDialog = LiveEvent<Unit>()
    val setRemindersEvent = LiveEvent<Unit>()
    val skipRemindersEvent = LiveEvent<Unit>()
    val showReminderWarning = MutableLiveData<Boolean>(false)
    operator fun invoke(block: TestingReminderState.() -> Unit) = apply(block)
}
