package com.vessel.app.common.manager

import com.vessel.app.common.model.Result
import com.vessel.app.common.net.data.plan.*
import com.vessel.app.common.repo.PlanRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlanManager @Inject constructor(
    private val planRepository: PlanRepository
) {
    private val planStateChannel = ConflatedBroadcastChannel(false)
    private val todayPlanItemCheckChange = ConflatedBroadcastChannel(false)

    suspend fun getUserPlans(forceUpdate: Boolean = false): Result<List<PlanData>, Nothing> = planRepository.getPlans(forceUpdate)
    suspend fun getUserPlans(forceUpdate: Boolean = false, isToday: Boolean? = false, date: String? = null): Result<List<PlanData>, Nothing> = planRepository.getPlans(forceUpdate, isToday, date)
    suspend fun getProgramPlans(forceUpdate: Boolean = false, isToday: Boolean? = false, date: String? = null, programId: Int?): Result<List<PlanData>, Nothing> = planRepository.getProgramPlans(forceUpdate, isToday, date, programId)

    suspend fun getPlansByDate(
        forceUpdate: Boolean = false,
        date: String
    ): Result<List<PlanData>, Nothing> =
        planRepository.getPlansByDate(forceUpdate, date)

    suspend fun addPlanItem(
        foodId: Int? = null,
        lifeStyleId: Int? = null
    ) = planRepository.addPlanItem(
        PlanData(
            id = -1,
            food_id = foodId,
            reagent_lifestyle_recommendation_id = lifeStyleId
        ).toPlanRequest()
    )

    suspend fun addPlanItem(planData: CreatePlanRequest) = planRepository.addPlanItem(planData)

    suspend fun updatePlanItem(planId: Int, planData: CreatePlanRequest) =
        planRepository.updatePlanItem(planId, planData)

    suspend fun deletePlanItem(planId: List<Int>) = planRepository.deletePlanItem(planId)

    suspend fun updatePlanToggle(planId: Int, date: String, isCompleted: Boolean, programDay: Int? = 0) =
        planRepository.updatePlanToggle(planId, date, isCompleted, programDay)

    suspend fun buildPlan(request: BuildPlanRequest) =
        planRepository.buildPlan(request)

    fun updatePlanCachedData() {
        CoroutineScope(SupervisorJob()).launch {
            if (getUserPlans(true) is Result.Success) {
                updatePlanState()
            }
        }
    }

    fun clearCachedData() {
        planRepository.clearCachedData()
    }

    fun getPlanDataState() = planStateChannel.asFlow()

    private fun updatePlanState() {
        planStateChannel.offer(true)
    }

    fun clearPlanState() {
        planStateChannel.offer(false)
    }

    fun getTodayPlanItemCheckChange() = todayPlanItemCheckChange.asFlow()

    fun todayPlanItemCheckChange() {
        todayPlanItemCheckChange.offer(true)
    }

    fun clearTodayPlanItemCheckChange() {
        todayPlanItemCheckChange.offer(false)
    }
    suspend fun getPlansByDateRange(
        date: String,
        endDate: String
    ): Result<List<PlanData>, Nothing> =
        planRepository.getPlansByDateRange(date, endDate)
    suspend fun getNextUserPlans(forceUpdate: Boolean = false, contactId: Int?, programId: Int?): Result<PlanData, Nothing> =
        planRepository.getNextPlans(forceUpdate, contactId, programId)

    suspend fun updateEssentialToggle(essentialToggleRequest: EssentialToggleRequest) =
        planRepository.updateEssentialToggle(essentialToggleRequest)
    suspend fun getUnlockedPlans(): List<PlanData> =
        planRepository.getNextPlans()
}
