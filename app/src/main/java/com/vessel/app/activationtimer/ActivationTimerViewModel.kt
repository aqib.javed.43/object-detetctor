package com.vessel.app.activationtimer

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants.CAPTURE_DATE_FORMAT
import com.vessel.app.activationtimer.ActivationTimerState.Companion.FINISH_VALUE
import com.vessel.app.activationtimer.ActivationTimerState.Companion.PROGRESS_MAX
import com.vessel.app.activationtimer.ui.ActivationTabsManager
import com.vessel.app.activationtimer.util.TimerService
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ReminderManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*

class ActivationTimerViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    val state: ActivationTimerState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
    val activationTabsManager: ActivationTabsManager,
    private val reminderManager: ReminderManager,
) : BaseViewModel(resourceProvider), TimerService.TimerHandler, ToolbarHandler {

    val afterFirstTest = preferencesRepository.afterFirstTest()
    private var openTheDefaultTab = true

    init {
        preferencesRepository.showTipVideoController = true
        logEvent(savedStateHandle)
        state.isNotificationSettingsAlertChecked.value =
            preferencesRepository.enableActivationTimerNotification
        observeRemindersChanges()
    }

    fun getDefaultTabNumber() {
        if (openTheDefaultTab) {
            openTheDefaultTab = false
            state.defaultTab.value = when (preferencesRepository.testCompletedCount) {
                BEFORE_FIRST_TEST -> 0
                BEFORE_SECOND_TEST -> 1
                else -> 2
            }
        }
    }

    private fun logEvent(savedStateHandle: SavedStateHandle) {
        val fromNotification = savedStateHandle.get<String>(TrackingConstants.KEY_LOCATION)
        if (fromNotification.equals(TrackingConstants.FROM_NOTIFICATION)) {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.PUSH_NOTIFICATION_TAPPED)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }

    fun onContinueClicked() {
        onSkipConfirm()
    }

    fun onSkipClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SKIP_VIDEO)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.showConfirmSkipAlertEvent.call()
    }

    fun onSkipConfirm() {
        state {
            forceCompleteTimerEvent.call()
        }
        navigateToCapture()
    }

    private fun navigateToCapture() {
        state.navigateToCapture.call()
    }

    override fun onUpdateText(minutes: String, seconds: String) {
        state.updateTimerText(minutes, seconds)
    }

    override fun onUpdateProgress(percentComplete: Double) {
        state {
            if (timerStart == null) {
                timerStart = CAPTURE_DATE_FORMAT.format(Date())
            }
            progress.value = (percentComplete * PROGRESS_MAX).toInt()
        }
    }

    override fun onFinish() {
        state {
            updateTimerText(FINISH_VALUE, FINISH_VALUE)
            progress.value = PROGRESS_MAX
            viewModelScope.launch {
                delay(500)
                setCompleted()
            }
        }
    }

    fun getStartDate(): String {
        return if (state.timerStart == null) {
            val calendar = Calendar.getInstance().apply {
                time = Date()
                add(Calendar.SECOND, -DEFAULT_WAITING_TIME_SECONDS)
            }
            CAPTURE_DATE_FORMAT.format(calendar.time)
        } else {
            state.timerStart!!
        }
    }

    override fun onBackButtonClicked(view: View) {
        state.stopTimerEvent.call()
        navigateBack()
    }

    fun logScanWarningContinueTapped() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SCAN_WARNING_CONTINUE_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun showNotification(isChecked: Boolean) {
        preferencesRepository.enableActivationTimerNotification = isChecked
        state.showNotificationSettingsAlert.value = isChecked
        if (!isChecked) {
            state.stopForegroundTimer.call()
        }
    }

    private fun observeRemindersChanges() {
        viewModelScope.launch {
            reminderManager.getFirstRemindersState().collectLatest {
                if (it) {
                    state.showFirstReminderHint.call()
                    reminderManager.clearFirstReminderState()
                }
            }
        }
    }

    companion object {
        private const val DEFAULT_WAITING_TIME_SECONDS = 210
        private const val BEFORE_FIRST_TEST = 0
        private const val BEFORE_SECOND_TEST = 1
    }
}
