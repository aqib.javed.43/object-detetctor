package com.vessel.app.planreminderdialog.testingreminder

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.ReminderManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.net.data.plan.CreatePlanRequest
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.ResourceRepository
import com.vessel.app.views.weekdays.DayOfWeekUiModel
import com.vessel.app.wellness.net.data.LikeCategory
import kotlinx.coroutines.launch

class TestingReminderViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val trackingManager: TrackingManager,
    private val reminderManager: ReminderManager,
    private val planManager: PlanManager,
) : BaseViewModel(resourceProvider) {
    val state = TestingReminderState(savedStateHandle["reminderHeader"]!!)

    fun onSetRemindersClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_PLAN_REMINDER_SET)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.setRemindersEvent.call()
    }

    fun onSkipThisClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_PLAN_REMINDER_SKIPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.skipRemindersEvent.call()
    }

    fun onReminderAdded(
        timeOfTheDay: String?,
        selectedDays: List<DayOfWeekUiModel>?,
        quantity: Float,
    ) {
        if (timeOfTheDay != null) {
            val duplicatedAddedPlans =
                state.planHeader.todos.firstOrNull {
                    it.time_of_day.orEmpty().startsWith(timeOfTheDay, true)
                }
            if (duplicatedAddedPlans != null) {
                showError(getResString(R.string.can_not_add_reminder_at_same_time))
                return
            }
        }
        showLoading()
        viewModelScope.launch {
            val todo = state.planHeader.todos.first()
            val createRequest = CreatePlanRequest(
                is_supplements = todo.is_supplements,
                notification_enabled = true,
                tip_id = todo.tip_id,
                food_id = todo.food_id,
                day_of_week = selectedDays?.map { it.planIndex },
                reagent_lifestyle_recommendation_id = todo.reagent_lifestyle_recommendation_id,
                multiple = (quantity / todo.getQuantityScale().toFloat()).toInt(),
                time_of_day = timeOfTheDay,
            )
            planManager.addPlanItem(createRequest)
                .onSuccess {
                    reminderManager.updateFirstReminderState()
                    state.dismissDialog.call()
                    planManager.clearCachedData()
                    reminderManager.updateRemindersCachedData()
                    val id =
                        todo.food_id ?: (todo.tip_id ?: todo.reagent_lifestyle_recommendation_id)
                    val type = when {
                        todo.food_id != null -> LikeCategory.Food.value
                        todo.tip_id != null -> LikeCategory.Tip.value
                        else -> LikeCategory.Lifestyle.value
                    }
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_ADDED)
                            .addProperty(TrackingConstants.KEY_ID, id.toString())
                            .addProperty(TrackingConstants.TYPE, type)
                            .addProperty(TrackingConstants.KEY_LOCATION, "Test Reminder Page")
                            .addProperty(
                                "has reminders",
                                (createRequest.time_of_day != null).toString()
                            )
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }
}
