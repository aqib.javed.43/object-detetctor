package com.vessel.app.views.weekdayview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.databinding.ItemDayOfWeekNewBinding
import com.vessel.app.views.weekdays.DayOfWeekUiModel

class DayOfWeekAdapter(private val clickListener: DayItemClickListener) : ListAdapter<DayOfWeekUiModel, DayOfWeekAdapter.ViewHolder>(DayOfWeekItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemDayOfWeekNewBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, position, clickListener)
        }
    }

    class ViewHolder constructor(val binding: ItemDayOfWeekNewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: DayOfWeekUiModel, position: Int, clickListener: DayItemClickListener) {
            binding.item = item
            binding.position = position
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }
}

class DayOfWeekItemDiffCallback : DiffUtil.ItemCallback<DayOfWeekUiModel>() {
    override fun areItemsTheSame(oldItem: DayOfWeekUiModel, newItem: DayOfWeekUiModel): Boolean {
        return oldItem === newItem
    }

    override fun areContentsTheSame(oldItem: DayOfWeekUiModel, newItem: DayOfWeekUiModel): Boolean {
        return oldItem == newItem
    }
}

class DayItemClickListener(val clickListener: (item: DayOfWeekUiModel, position: Int) -> Unit) {
    fun onItemClicked(item: DayOfWeekUiModel, position: Int) = clickListener(item, position)
}
