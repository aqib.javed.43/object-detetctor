package com.vessel.app.takesurvey.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SurveyResponseAnswer(
    val questionId: Int?,
    var answerId: Int? = null,
    var answerText: String? = null
) : Parcelable
