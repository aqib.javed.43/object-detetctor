package com.vessel.app.common.tracking

class TrackedEvent private constructor(builder: Builder) {
    val platforms = builder.platforms
    val properties = builder.properties
    val email = builder.email
    val name = builder.name

    class Builder(val name: String) {
        val platforms = mutableSetOf<TrackerPlatform>()
        val properties = mutableMapOf<String, String>()
        var email: String? = null

        fun addProperty(key: String, value: String): Builder {
            properties[key] = value
            return this
        }

        fun setEmail(value: String): Builder {
            email = value
            return this
        }

        fun withKlaviyo(): Builder {
            platforms.add(TrackerPlatform.KLAVIYO)
            return this
        }

        fun withFirebase(): Builder {
            platforms.add(TrackerPlatform.FIREBASE)
            return this
        }

        fun withAmplitude(): Builder {
            platforms.add(TrackerPlatform.AMPLITUDE)
            return this
        }

        fun setError(errorMessage: String): Builder {
            properties[TrackingConstants.KEY_ERROR] = errorMessage
            return this
        }

        fun setScreenName(screenName: String): Builder {
            properties[TrackingConstants.KEY_LOCATION] = screenName
            return this
        }

        fun create(): TrackedEvent {
            return TrackedEvent(this)
        }
    }
}

enum class TrackerPlatform {
    KLAVIYO, FIREBASE, AMPLITUDE
}
