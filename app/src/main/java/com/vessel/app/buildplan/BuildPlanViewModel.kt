package com.vessel.app.buildplan

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.buildplan.model.MakePlanItem
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.buildplan.ui.BuildPlanFragmentDirections
import com.vessel.app.buildplan.ui.MakePlanAdapter
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.ReminderManager
import com.vessel.app.common.manager.TodosManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.ArrayList

class BuildPlanViewModel @ViewModelInject constructor(
    val state: BuildPlanState,
    private val resourceProvider: ResourceRepository,
    private val buildPlanManager: BuildPlanManager,
    private val todosManager: TodosManager,
    private val reminderManager: ReminderManager,
    private val planManager: PlanManager,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider),
    MakePlanAdapter.OnActionHandler,
    ToolbarHandler {

    init {
        observeTodayTodoDataState()
        observeReminder()
    }

    fun loadMakePlan(forceUpdate: Boolean = false) {
        showLoading()
        val planList = ArrayList<MakePlanItem>()
        val userMainGoal = preferencesRepository.getUserMainGoal()
        userMainGoal?.let {
            val mainPlanItem = MakePlanItem(type = PlanType.MainPlan, title = it.name, buttonText = "Add ${it.name} Activities", image = 0, background = R.drawable.green_background)
            planList.add(mainPlanItem)
        }
        viewModelScope.launch {
            planList.addAll(buildPlanManager.loadPlansForAdding(forceUpdate))
            state.makePlanItems.value = planList
            hideLoading(false)
        }
    }

    private fun observeTodayTodoDataState() {
        viewModelScope.launch {
            todosManager.getTodayTodosState().collect {
                if (it) {
                    planManager.clearCachedData()
                    loadMakePlan(true)
                }
            }
        }
    }

    private fun observeReminder() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    loadMakePlan(true)
                }
            }
        }
    }

    override fun onMakeAPlanClick(item: MakePlanItem, view: View) {
        when (item.type) {
            PlanType.SupplementPlan -> onMakeSupplementPlanClick(view)
            PlanType.FoodPlan -> onMakeFoodPlanClick(view)
            PlanType.StressPlan -> onMakeStressReliefPlanClick(view)
            PlanType.TestPlan -> onMakeTestingPlanSelected(view)
            PlanType.WaterPlan -> onHydrationPlanSelected(view)
            PlanType.OwnPlan -> onCreateOwnPlanItemSelected(view)
            PlanType.MainPlan -> onMainGoalClick(view)
        }
    }

    private fun onMainGoalClick(view: View) {
        preferencesRepository.getUserMainGoal()?.let { goal ->
            state.navigateTo.value = HomeNavGraphDirections.actionGlobalGoalPageFragment(goal)
        }
    }

    private fun onMakeSupplementPlanClick(view: View) {
        view.findNavController()
            .navigate(R.id.action_build_plan_to_supplement)
    }

    private fun onMakeFoodPlanClick(view: View) {
        view.findNavController()
            .navigate(R.id.action_build_plan_to_food_plan)
    }

    private fun onMakeStressReliefPlanClick(view: View) {
        view.findNavController()
            .navigate(R.id.action_build_plan_to_stress_relief_plan)
    }

    private fun onHydrationPlanSelected(view: View) {
        view.findNavController()
            .navigate(R.id.action_build_plan_to_hydration_plan)
    }

    private fun onMakeTestingPlanSelected(view: View) {
        view.findNavController()
            .navigate(
                BuildPlanFragmentDirections.actionBuildPlanToTesting()
            )
    }

    private fun onCreateOwnPlanItemSelected(view: View) {
        view.findNavController()
            .navigate(R.id.globalAction_to_createOwnPlanItem)
    }

    fun onBackClicked() {
        navigateBack()
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
    fun onAddProgramClicked(view: View) {
        view.findNavController().navigate(BuildPlanFragmentDirections.actionBuildPlanFragmentToProgramSelectFragment())
    }
}
