package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.CompleteAnswer
import com.vessel.app.common.net.service.LessonService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LessonRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val lessonService: LessonService
) : BaseNetworkRepository(moshi, loggingManager) {

    suspend fun fetchAllLessons(
        isToday: Boolean? = true,
        contactId: Int,
        programId: Int,
        fetchAll: Boolean? = true,
        showAll: Boolean? = true,
        date: String? = null
    ) =
        executeWithErrorMessage() {
            lessonService.fetchAllLessons(isToday, contactId, programId, fetchAll, showAll, date)
        }
    suspend fun postAnswer(
        answer: CompleteAnswer
    ) =
        executeWithErrorMessage {
            lessonService.postAnswer(answer)
        }
}
