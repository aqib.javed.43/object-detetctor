package com.vessel.app.helper

import android.content.ContentResolver
import android.net.Uri
import android.text.TextUtils
import android.webkit.MimeTypeMap
import androidx.collection.arraySetOf
import com.vessel.app.helper.PhotoMetadataUtils.Companion.getPath
import java.util.*

enum class MimeType(private val mMimeTypeName: String, private val mExtensions: Set<String>) {
    JPEG(
        "image/jpeg",
        arraySetOf(
            "jpg",
            "jpeg"
        )
    ),
    PNG(
        "image/png",
        arraySetOf(
            "png"
        )
    ),
    GIF(
        "image/gif",
        arraySetOf(
            "gif"
        )
    ),
    BMP(
        "image/x-ms-bmp",
        arraySetOf(
            "bmp"
        )
    ),
    WEBP(
        "image/webp",
        arraySetOf(
            "webp"
        )
    ), // ============== videos ==============
    MPEG(
        "video/mpeg",
        arraySetOf(
            "mpeg",
            "mpg"
        )
    ),
    MP4(
        "video/mp4",
        arraySetOf(
            "mp4",
            "m4v"
        )
    ),
    QUICKTIME(
        "video/quicktime",
        arraySetOf(
            "mov"
        )
    ),
    THREEGPP(
        "video/3gpp",
        arraySetOf(
            "3gp",
            "3gpp"
        )
    ),
    THREEGPP2(
        "video/3gpp2",
        arraySetOf(
            "3g2",
            "3gpp2"
        )
    ),
    MKV(
        "video/x-matroska",
        arraySetOf(
            "mkv"
        )
    ),
    WEBM(
        "video/webm",
        arraySetOf(
            "webm"
        )
    ),
    TS(
        "video/mp2ts",
        arraySetOf(
            "ts"
        )
    ),
    AVI(
        "video/avi",
        arraySetOf(
            "avi"
        )
    );
    companion object {
        fun ofAll(): Set<MimeType> {
            return EnumSet.allOf(MimeType::class.java)
        }

        fun of(type: MimeType?, vararg rest: MimeType?): Set<MimeType> {
            return EnumSet.of(type, *rest)
        }

        fun ofImage(): Set<MimeType> {
            return EnumSet.of(JPEG, PNG, GIF, BMP, WEBP)
        }

        fun ofImage(onlyGif: Boolean): Set<MimeType> {
            return EnumSet.of(GIF)
        }

        fun ofGif(): Set<MimeType> {
            return ofImage(true)
        }

        fun ofVideo(): Set<MimeType> {
            return EnumSet.of(MPEG, MP4, QUICKTIME, THREEGPP, THREEGPP2, MKV, WEBM, TS, AVI)
        }

        fun isImage(mimeType: String?): Boolean {
            return mimeType?.startsWith("image") ?: false
        }

        fun isVideo(mimeType: String?): Boolean {
            return mimeType?.startsWith("video") ?: false
        }

        fun isGif(mimeType: String?): Boolean {
            return if (mimeType == null) false else mimeType == GIF.toString()
        }
    }
    override fun toString(): String {
        return mMimeTypeName
    }

    fun checkType(resolver: ContentResolver, uri: Uri?): Boolean {
        val map = MimeTypeMap.getSingleton()
        if (uri == null) {
            return false
        }
        val type = map.getExtensionFromMimeType(resolver.getType(uri))
        var path: String? = null
        // lazy load the path and prevent resolve for multiple times
        var pathParsed = false
        for (extension in mExtensions) {
            if (extension == type) {
                return true
            }
            if (!pathParsed) {
                // we only resolve the path for one time
                path = getPath(resolver, uri)
                if (!TextUtils.isEmpty(path)) {
                    path = path!!.toLowerCase(Locale.US)
                }
                pathParsed = true
            }
            if (path != null && path.endsWith(extension)) {
                return true
            }
        }
        return false
    }
}
