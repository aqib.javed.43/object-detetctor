package com.vessel.app.views.planintroduction

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import com.vessel.app.R
import com.vessel.app.databinding.PlanIntroductionHintDialogBinding

class PlanIntroductionHintDialog(
    private val activity: FragmentActivity,
    private val testCount: Int,
    private val firstTestPopupNumber: Int = 1,
    private val isPlanIntroductionHint: Boolean,
    private val onDismiss: () -> Unit
) {

    private val dialog: AlertDialog = AlertDialog.Builder(activity).create()
    private lateinit var binding: PlanIntroductionHintDialogBinding

    init {
        setupDialogView()
    }

    private fun setupDialogView() {
        val layoutInflater = LayoutInflater.from(activity)
        binding = PlanIntroductionHintDialogBinding.inflate(layoutInflater, null, false)
        dialog.setView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.BOTTOM)
        dialog.setCancelable(true)
        dialog.setOnCancelListener {
            isShown = false
            onDismiss.invoke()
        }

        binding.closeDialogButton.isVisible = isPlanIntroductionHint

        if (isPlanIntroductionHint) {
            binding.image.setImageResource(R.drawable.ic_plan_introcution)
        } else if (testCount == 1) {
            when (firstTestPopupNumber) {
                1 -> {
                    binding.image.setImageResource(R.drawable.ic_plan_introduction_ready)
                }
                2 -> {
                    binding.image.setImageResource(R.drawable.ic_plan_introduction_check_results)
                }
                3 -> {
                    binding.image.setImageResource(R.drawable.ic_plan_introduction_ask_any_thing)
                }
            }
        } else {
            binding.image.setImageResource(R.drawable.ic_plan_introcution_updated)
        }

        setupActionClick()
    }

    private fun setupActionClick() {
        binding.closeDialogButton.setOnClickListener {
            dialog.cancel()
        }
        binding.image.setOnClickListener {
            dialog.cancel()
        }
        binding.background.setOnClickListener {
            dialog.cancel()
        }
    }

    fun showDialog() {
        dialog.show()
    }

    companion object {
        var isShown = false
        fun showIntroductionDialog(
            activity: FragmentActivity,
            testCount: Int,
            firstTestPopupNumber: Int = 1,
            isPlanIntroductionHint: Boolean = false,
            onDismiss: () -> Unit
        ) {
            if (!isShown) {
                isShown = true
                PlanIntroductionHintDialog(
                    activity,
                    testCount,
                    firstTestPopupNumber = firstTestPopupNumber,
                    isPlanIntroductionHint = isPlanIntroductionHint,
                    onDismiss = onDismiss
                ).showDialog()
            }
        }
    }
}
