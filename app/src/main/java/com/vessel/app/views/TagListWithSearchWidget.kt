package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.vessel.app.databinding.WidgetFilterWithSearchBinding
import com.vessel.app.util.extensions.afterMeasured
import com.vessel.app.wellness.model.Tag
import com.vessel.app.wellness.ui.TagsAdapter

class TagListWithSearchWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var tagSearchQuery: String = ""
    private var tagsList: List<Tag>? = null
    private lateinit var binding: WidgetFilterWithSearchBinding
    private var handler: FilterWithSearchWidgetOnActionHandler? = null
    private var tagsHandler: TagsAdapter.OnActionHandler =
        object : TagsAdapter.OnActionHandler {
            override fun onTagSelected(tag: Tag) {
                tagsList = tagsList?.map {
                    if (it.name == tag.name)
                        it.copy(isSelected = tag.isSelected.not())
                    else
                        it.copy()
                }
                setList(tagsList)
            }
        }
    private val tagsAdapter = TagsAdapter(tagsHandler)

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetFilterWithSearchBinding.inflate(layoutInflater, this, true)
        binding.tagsInfoIcon.setOnClickListener { view -> handler?.onInfoIconClicked(view) }
        binding.tags.apply {
            adapter = tagsAdapter
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL)
        }
        binding.tagsSearchView.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return onSearchQuery(query)
                }

                override fun onQueryTextChange(query: String?): Boolean {
                    return onSearchQuery(query)
                }
            }
        )
    }

    fun setHandler(handler: FilterWithSearchWidgetOnActionHandler) {
        this.handler = handler
    }

    fun setList(tagsList: List<Tag>?) {
        this.tagsList = tagsList
        tagsAdapter.submitList(
            this.tagsList?.filter { tag ->
                tag.name.contains(tagSearchQuery, true)
            }
        )
    }

    private fun onSearchQuery(query: String?): Boolean {
        tagSearchQuery = query ?: ""
        return if (query != null) {
            setList(tagsList)
            if (query.isEmpty())
                binding.tags.afterMeasured {
                    binding.tags.smoothScrollToPosition(0)
                }
            true
        } else
            false
    }

    fun getTags() = tagsList
}

interface FilterWithSearchWidgetOnActionHandler {
    fun onInfoIconClicked(view: View)
}
