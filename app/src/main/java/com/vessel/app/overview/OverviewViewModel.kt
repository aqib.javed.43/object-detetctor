package com.vessel.app.overview

import android.os.Build
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.ForceUpdateManager
import com.vessel.app.common.manager.ScoreManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.overview.ui.OverviewFragmentDirections
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*

class OverviewViewModel @ViewModelInject constructor(
    val state: OverviewState,
    private val authManager: AuthManager,
    resourceRepository: ResourceRepository,
    private val scoreManager: ScoreManager,
    val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
    forceUpdateManager: ForceUpdateManager,
) : BaseViewModel(resourceRepository) {

    init {
        state.title.value =
            resourceRepository.getString(R.string.hi, preferencesRepository.userFirstName.orEmpty())
        state.showForceUpdate.value = forceUpdateManager.isRequireUpdate().first
        observeFirstTestRecommendationState()
        logOverviewAppearanceEvent()
    }

    private fun logOverviewAppearanceEvent() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.GET_STARTED_SHOWN)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun onTakeYourFirstTestClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_TAPPED)
                .setScreenName(TrackingConstants.FROM_HOME_FIRST_TEST)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        val isSupportedDevice =
            Constants.SUPPORTED_DEVICES_MODELS_PREFIX.any { Build.MODEL.startsWith(it, true) }
        if (isSupportedDevice) {
            onTestClicked(view)
        } else {
            view.findNavController()
                .navigate(HomeNavGraphDirections.globalActionToTestNotAccurate())
        }
    }

    private fun onTestClicked(view: View) {
        val now = Date()
        val fourThirtyAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 4)
                set(Calendar.MINUTE, 30)
                set(Calendar.SECOND, 0)
            }.time
        val nineAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 9)
                set(Calendar.MINUTE, 0)
                set(Calendar.SECOND, 0)
            }.time
        if (now.after(fourThirtyAm) && now.before(nineAm)) {
            if (preferencesRepository.skipPreTestTips)
                view.findNavController()
                    .navigate(HomeNavGraphDirections.globalActionToTakeTest())
            else
                view.findNavController()
                    .navigate(HomeNavGraphDirections.globalActionToPreTest())
        } else {
            view.findNavController()
                .navigate(HomeNavGraphDirections.globalActionToTimeOfDay())
        }
    }

    fun onOrderTestCardsClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.ORDER_CARDS_TAPPED)
                .setScreenName(TrackingConstants.FROM_HOME_FIRST_TEST)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        showLoading()
        viewModelScope.launch {
            authManager.multipass(Constants.PATH_ACCOUNT)
                .onSuccess {
                    hideLoading(false)
                    view.findNavController()
                        .navigate(OverviewFragmentDirections.actionOverviewToWeb(it.multipass_url))
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onHttpOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun onLogoutClicked() {
        viewModelScope.launch {
            authManager.logout()
        }
    }

    private fun observeFirstTestRecommendationState() {
        viewModelScope.launch {
            scoreManager.getFirstTestState().collect {
                if (it) {
                    scoreManager.clearFirstTestState()
                    state.backToHome.call()
                }
            }
        }
    }

    fun onBackPressed() {
        onLogoutClicked()
    }
}
