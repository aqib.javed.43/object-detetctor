package com.vessel.app.forgotpassword

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.validators.EmailValidator
import com.vessel.app.util.validators.ValidatorException
import kotlinx.coroutines.launch

class ForgotPasswordViewModel @ViewModelInject constructor(
    val state: ForgotPasswordState,
    resourceRepository: ResourceRepository,
    private val authManager: AuthManager,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository), ToolbarHandler {

    fun forgotPasswordButtonAction() {
        if (!validate()) {
            return
        }

        showLoading()

        viewModelScope.launch {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.FORGOT_PASSWORD_SUBMIT)
                    .setEmail(state.userEmail.value!!)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )

            authManager.forgotPassword(state.userEmail.value!!)
                .onSuccess {
                    navigateBack()
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun validate() = try {
        EmailValidator().validate(state.userEmail)
        true
    } catch (exception: ValidatorException) {
        showError(exception)
        false
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
}
