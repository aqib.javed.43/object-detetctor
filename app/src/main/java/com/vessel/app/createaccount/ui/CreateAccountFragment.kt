package com.vessel.app.createaccount.ui

import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.createaccount.CreateAccountViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateAccountFragment : BaseFragment<CreateAccountViewModel>() {
    override val viewModel: CreateAccountViewModel by viewModels()
    override val layoutResId = R.layout.fragment_create_account
}
