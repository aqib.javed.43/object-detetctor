package com.vessel.app.wellness.ui

import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.livechatinc.inappchat.ChatWindowActivity
import com.livechatinc.inappchat.ChatWindowConfiguration
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.manager.ChatManager
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.data.ReagentState
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.common.widget.ProgramCardWidget
import com.vessel.app.homescreen.HomeScreenFragmentDirections
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.*
import com.vessel.app.views.planintroduction.PlanIntroductionHintDialog
import com.vessel.app.views.todayactivities.TodayActivitiesWidget
import com.vessel.app.wellness.WellnessState
import com.vessel.app.wellness.WellnessViewModel
import com.vessel.app.wellness.filterpopup.FilterDialogFragment
import com.vessel.app.wellness.filterpopup.ItemFilterCategory
import com.vessel.app.wellness.model.GoalButton
import com.vessel.app.wellness.model.GoalRate
import com.vessel.app.wellness.model.ReagentItem
import com.vessel.app.wellness.model.Tag
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_header_chart.*
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity

@AndroidEntryPoint
class WellnessFragment() : BaseFragment<WellnessViewModel>() {

    override val viewModel: WellnessViewModel by viewModels()
    override val layoutResId = R.layout.fragment_wellness_new

    private lateinit var headerChartView: WellnessHeaderChartView
    private lateinit var goalList: GoalsListWidget
    private lateinit var programsWidget: ProgramCardWidget
    private lateinit var recommendationList: RecommendationListWithPagingWidget
    private lateinit var rateGoalContainer: ConstraintLayout
    private lateinit var goalRecommendationsFilterContainer: ConstraintLayout
    private lateinit var selectMainGoalContainer: ConstraintLayout
    private lateinit var selectProgramContainer: ConstraintLayout
    private lateinit var selectProgramTitle: TextView
    private lateinit var rateYourGoal: TextView
    private lateinit var goalRecommendationsTitle: TextView
    private lateinit var stickyGoalRecommendationsTitle: TextView
    private lateinit var testsThatAffectsGoal: TextView
    private lateinit var yourPlanFor: TextView
    private lateinit var viewYourPlanBtn: BlackRoundedButtonGroup
    private lateinit var openProgramButton: BlackRoundedButtonGroup
    private lateinit var testsThatAffectsGoalInfoIcon: ImageView
    private val filterSortItemAdapter by lazy { SelectedFilterItemAdapter(viewModel) }
    private var firstTestPopupNumber: Int = 1
    private lateinit var programsSubTitle: TextView
    private lateinit var programsTitle: TextView
    private lateinit var todayActivities: TodayActivitiesWidget
    private lateinit var loadingSelectMainGoal: ProgressBar
    private lateinit var selectProgramIcon: ImageView
    private lateinit var goalsAndReagentsLinkView: GoalsAndReagentsLinkView

    private val reagentAdapter by lazy { ReagentAdapter(viewModel) }
    private val reagentGridAdapter by lazy { ReagentGridAdapter(viewModel) }

    private var uuid: String? = null
    private val minProgressBarYPosition = 250
    private val wholeScreenScrollListener =
        NestedScrollView.OnScrollChangeListener { _, _, _, _, _ ->
            val location = IntArray(2)
            recommendationList.getLocationOnScreen(location)
            viewModel.state.showStickyGoalRecommendationsFilterContainer.value =
                (viewModel.state.selectedGoal.value?.isNotWellness == true && viewModel.state.fragmentLocationType == FragmentLocationType.HOME && location[1] < minProgressBarYPosition)
        }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        uuid = requireActivity().intent.extras?.getString("latest_sample_uuid")
        setupObservers()
        setupViews()
        setupGoalView()
        findNavController().currentBackStackEntry
            ?.savedStateHandle?.apply {
                getLiveData<Pair<List<Tag>?, List<ItemFilterCategory>>>(FilterDialogFragment.SELECTED_FILTER_ITEMS)
                    .observe(viewLifecycleOwner) { value ->
                        if (viewModel.state.selectedTagsList != value.first || viewModel.state.filterCategories != value.second) {
                            viewModel.state.selectedTagsList = value.first
                            viewModel.state.filterCategories = value.second
                            viewModel.updateFilterOptions()
                        }
                    }
            }
    }

    private fun setupViews() {
        headerChartView = requireView().findViewById(R.id.chart)
        goalList = requireView().findViewById(R.id.goalList)
        goalList.setHandler(viewModel)
        recommendationList = requireView().findViewById(R.id.recommendationList)
        recommendationList.setHandler(viewModel)
        rateGoalContainer = requireView().findViewById(R.id.goalRateContainer)
        goalRecommendationsFilterContainer =
            requireView().findViewById(R.id.goalRecommendationsFilterContainer)
        selectMainGoalContainer =
            requireView().findViewById(R.id.selectMainGoalContainer)
        selectProgramContainer =
            requireView().findViewById(R.id.selectProgramContainer)
        selectProgramTitle =
            requireView().findViewById(R.id.selectProgramTitle)
        openProgramButton =
            requireView().findViewById(R.id.openProgramButton)
        rateYourGoal = requireView().findViewById(R.id.rateYourGoal)
        goalRecommendationsTitle = requireView().findViewById(R.id.goalRecommendationsTitle)
        stickyGoalRecommendationsTitle =
            requireView().findViewById(R.id.stickyGoalRecommendationsTitle)
        yourPlanFor = requireView().findViewById(R.id.yourPlanFor)
        viewYourPlanBtn = requireView().findViewById(R.id.viewYourPlanBtn)
        testsThatAffectsGoal = requireView().findViewById(R.id.testsThatAffectsGoal)
        testsThatAffectsGoalInfoIcon = requireView().findViewById(R.id.testsThatAffectsGoalInfoIcon)
        requireView().findViewById<RecyclerView>(R.id.filterSortList).adapter =
            filterSortItemAdapter
        requireView().findViewById<NestedScrollView>(R.id.nestedScrollView)
            .setOnScrollChangeListener(wholeScreenScrollListener)
        programsSubTitle = requireView().findViewById(R.id.programsSubTitle)
        programsTitle = requireView().findViewById(R.id.programsTitle)
        loadingSelectMainGoal = requireView().findViewById(R.id.loadingSelectMainGoal)
        selectProgramIcon = requireView().findViewById(R.id.selectProgramIcon)

        programsWidget = requireView().findViewById(R.id.programsWidget)
        programsWidget.setHandler(viewModel)
        todayActivities = requireView().findViewById(R.id.todayActivities)
        todayActivities.setHandler(viewModel)
    }

    override fun onResume() {
        super.onResume()
        viewModel.checkUserHasPrograms()
    }

    private fun setupGoalView() {
        goalsAndReagentsLinkView = requireView().findViewById(R.id.testAndGoalView)
        goalsAndReagentsLinkView.onReagentDetails = { reagent, v ->
            viewModel.onReagentItemClick(reagent, v)
        }
        goalsAndReagentsLinkView.onGoalDetails = { goal, v ->
            val item = goalList.goalsList.firstOrNull {
                it.id == goal.id
            }
            item?.let {
                viewModel.onGoalItemClick(it)
            }
        }
    }

    private fun showIntroductionDialog(testCount: Int) {
        PlanIntroductionHintDialog.showIntroductionDialog(
            requireActivity(),
            testCount = testCount,
            firstTestPopupNumber = firstTestPopupNumber,
            isPlanIntroductionHint = false
        ) {
            if (testCount == 1) {
                if (firstTestPopupNumber < 3) {
                    firstTestPopupNumber++
                    showIntroductionDialog(testCount)
                } else {
                    viewModel.updatePlanHintState()
                }
            } else {
                viewModel.updatePlanHintState()
            }
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateToOverview) {
                findNavController().navigate(HomeScreenFragmentDirections.actionHomeScreenFragmentResultTabToOverview())
            }
            observe(navigateTo) {
                refreshStateWhenResume = false
                findNavController().navigate(it)
            }
            observe(showPlanIntroduction) {
                if (it.first) {
                    showIntroductionDialog(it.second)
                }
            }
            observe(selectedGoal) {
                headerChartView.setChartEntries(
                    it.chartEntries.sortedBy { date ->
                        date.x
                    },
                    it.averageChartEntries.sortedBy { date ->
                        date.x
                    }
                )
                headerChartView.isVisible = true
                rateGoalContainer.isVisible = it.isNotWellness
                rateYourGoal.text = getString(R.string.rate_your, getString(it.title))
                testsThatAffectsGoal.isVisible = it.isNotWellness
                testsThatAffectsGoalInfoIcon.isVisible = it.isNotWellness
                goalRecommendationsFilterContainer.isVisible = it.isNotWellness
                recommendationList.isVisible = it.isNotWellness

                updatePlanView(it)

                val goalTitle = getString(R.string.goal_recommendations, getString(it.title))
                goalRecommendationsTitle.text = goalTitle
                stickyGoalRecommendationsTitle.text = goalTitle

                testsThatAffectsGoal.text =
                    getString(R.string.tests_that_affects, getString(it.title))
                programsSubTitle.text =
                    getString(R.string.programs_sub_title, getString(it.title))
                programsTitle.text =
                    getString(R.string.programs_title, getString(it.title))

                if (it.isNotWellness && it.enableGoalRateSelector && GoalRate.valueOf(it.scoreValue) != GoalRate.UNKNOWN) {
                    if (refreshStateWhenResume) {
                        filterSortItemAdapter.submitList(getFilterChoices())
                    }
                }

                if (it.isNotWellness) {
                    if (refreshStateWhenResume) {
                        recommendations.value = listOf()
                        viewModel.loadRecommendations(it)
                    }
                    viewModel.getProgramsByGoal(it.id)
                } else {
                    viewModel.state.programsItems.value = null
                }
                if (uuid != null)
                    viewModel.selectScoreBasedOnUUID(uuid!!)
            }
            observe(goals) {
                goalList.setList(it)
            }
            observe(goalRecords) {
                // Todo update goal list view
                updateGoalListView(it)
            }
            observe(filterSortChoices) {
                filterSortItemAdapter.submitList(it)
            }
            observe(reagentItems) {
                // reagentGridAdapter.submitList(it)
                updateReagentListView(it)
            }
            observe(reagentVerticalItems) {
                // reagentAdapter.submitList(it)
            }
            observe(launchLiveChatEvent) {
                MessagingActivity.builder()
                    .withEngines(ChatEngine.engine())
                    .withBotAvatarDrawable(R.mipmap.ic_launcher_round)
                    .show(requireContext(), ChatManager.chatConfiguration)
            }
            observe(showForceUpdate) {
                if (it && isVisible && viewModel.state.fragmentLocationType == FragmentLocationType.HOME) {
                    findNavController().navigate(HomeScreenFragmentDirections.actionHomeScreenFragmentResultTabToForceUpdateDialog())
                }
            }
            observe(recommendations) {
                recommendationList.setList(it, recommendationsPaging)
            }

            observe(showLoadingRecommendations) {
                recommendationList.loading(it)
            }
            observe(openMessaging) {
                if (viewModel.isLiveChat()) {
                    val intent = Intent(requireContext(), ChatWindowActivity::class.java)
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_GROUP_ID,
                        BuildConfig.NUTRITIONIST_COACH_GROUP
                    )
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_LICENCE_NUMBER,
                        BuildConfig.LIVE_CHAT_LICENSE
                    )
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_VISITOR_NAME,
                        viewModel.getUserName()
                    )
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_VISITOR_EMAIL,
                        viewModel.getUserEmail()
                    )
                    startActivity(intent)
                } else {
                    MessagingActivity.builder()
                        .withToolbarTitle(getString(R.string.chat_to_nutritionist))
                        .withEngines(ChatEngine.engine())
                        .show(requireContext(), viewModel.getChatConfiguration())
                }
            }
            observe(dataEntry) {
                it.firstOrNull()?.x?.let { it1 ->
                    headerChartView.setEntrySelected(it1.toInt())
                    uuid = null
                }
            }
            observe(programsItems) {
                programsWidget.setList(it)
            }
            observe(todayActivitiesItems) {
                todayActivities.setList(it)
            }
            observe(todayActivitiesLoading) {
                todayActivities.loading(it)
            }
            observe(userEnrolledInPrograms) {
                showGoalOrProgramSelection()
            }
            observe(showSelectGoalLoading) {
                loadingSelectMainGoal.isVisible =
                    (it && !selectMainGoalContainer.isVisible && !selectProgramContainer.isVisible && !openProgramButton.isVisible)
            }
        }
    }

    private fun updateGoalListView(goals: List<GoalRecord>) {
        goalsAndReagentsLinkView.updateGoals(goals)
    }

    private fun updateReagentListView(reagents: List<ReagentItem>?) {

        goalsAndReagentsLinkView.updateReagents(
            reagents?.filter {
                it.state != ReagentState.COMING_SOON
            }
        )
    }

    private fun showGoalOrProgramSelection() {
        if (viewModel.isUserHasAMainGoal().not()) {
            selectMainGoalContainer.isVisible = true
            selectProgramContainer.isVisible = false
            openProgramButton.isVisible = false
        } else if (viewModel.state.userEnrolledInPrograms.value == false) {
            val goal = Goal.values().firstOrNull { it.id == viewModel.getUserMainGoalId() }
            if (goal != null) {
                selectProgramTitle.text =
                    getString(R.string.choose_your_program_template, getString(goal.title))
                selectProgramIcon.setImageResource(goal.icon)
                selectMainGoalContainer.isVisible = false
                selectProgramContainer.isVisible = true
                openProgramButton.isVisible = false
            }
        } else {
            selectMainGoalContainer.isVisible = false
            selectProgramContainer.isVisible = false
            openProgramButton.isVisible = true
        }
    }

    private fun WellnessState.updatePlanView(it: GoalButton) {
        yourPlanFor.isVisible =
            it.isNotWellness && fragmentLocationType == FragmentLocationType.HOME
        yourPlanFor.text = getString(R.string.your_plan_for, getString(it.title))
        viewYourPlanBtn.isVisible =
            it.isNotWellness && fragmentLocationType == FragmentLocationType.HOME
    }
}

enum class FragmentLocationType(val value: Int) {
    HOME(0),
    ACTIVATION_TIMER(1);

    companion object {
        fun fromInt(value: Int) = values().first { it.value == value }
    }
}
