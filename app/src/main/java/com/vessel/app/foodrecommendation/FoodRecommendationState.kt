package com.vessel.app.foodrecommendation

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.foodrecommendation.model.FoodRecommendationSelectModel
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class FoodRecommendationState @Inject constructor() {
    val items = MutableLiveData<List<FoodRecommendationSelectModel>>()
    val hint = MutableLiveData<String>()
    val title = MutableLiveData<String>()
    val refreshItems = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()
    val navigateToHome = LiveEvent<Unit>()
    operator fun invoke(block: FoodRecommendationState.() -> Unit) = apply(block)
}
