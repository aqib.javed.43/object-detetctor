package com.vessel.app.wellness.net.data

enum class ConsumptionUnit(val value: String) {
    Mcg("mcg"),
    Mg("mg"),
    PH("pH"),
    SPGr("sp gr");

    companion object {
        fun fromValue(value: String): ConsumptionUnit = when (value) {
            "mcg" -> Mcg
            "mg" -> Mg
            "pH" -> PH
            "sp gr" -> SPGr
            else -> throw IllegalArgumentException()
        }
    }
}
