package com.vessel.app.common.adapter.planedit

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class PlanEditAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<PlanEditItem>(
        areItemsTheSame = { oldItem, newItem -> oldItem.title == newItem.title }
    ) {
    override fun getLayout(position: Int) = R.layout.item_plan_edit

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onPlanEditItemClick(item: PlanEditItem, view: View)
    }
}
