package com.vessel.app.pretest

import android.os.Bundle
import androidx.activity.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseActivity
import com.vessel.app.util.extensions.applyFullscreenFlags
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PreTestActivity : BaseActivity<PreTestViewModel>() {
    public override val viewModel: PreTestViewModel by viewModels()
    override val layoutResId = R.layout.activity_pre_test

    override fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        applyFullscreenFlags()
    }
}
