package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.vessel.app.R
import com.vessel.app.common.binding.setTextSize
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.*

open class RoundedButtonGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.rounded_button_layout, this)
        button.backgroundTintList = ContextCompat.getColorStateList(context, R.color.black)
        button.setTextColor(ContextCompat.getColor(context, R.color.white))
    }

    fun setText(text: String) {
        button.text = text
    }

    fun setSmallText(smallText: Boolean) {
        if (smallText) {
            button.setTextSize(R.dimen.font_xs)
        }
    }

    fun setText(@StringRes stringResId: Int?) {
        stringResId?.let { button.setText(it) } ?: run { button.text = null }
    }

    fun setHint(hint: Int?) {
        hint?.let { button.text = hint.toString() } ?: run { button.text = null }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        button.isEnabled = enabled
    }

    override fun setOnClickListener(l: OnClickListener?) {
        button.setOnClickListener(l)
    }
}

class BlackRoundedButtonGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RoundedButtonGroup(context, attrs, defStyle) {
    init {
        button.backgroundTintList = ContextCompat.getColorStateList(context, R.color.black)
        button.setTextColor(ContextCompat.getColor(context, R.color.white))
    }
}

class WhiteRoundedButtonGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RoundedButtonGroup(context, attrs, defStyle) {
    init {
        button.backgroundTintList = ContextCompat.getColorStateList(context, R.color.white)
        button.setTextColor(ContextCompat.getColor(context, R.color.black))
    }
}

class GrayRoundedButtonGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RoundedButtonGroup(context, attrs, defStyle) {
    init {
        button.backgroundTintList =
            ContextCompat.getColorStateList(context, R.color.grayBg)
        button.setTextColor(ContextCompat.getColor(context, R.color.black))
    }
}
