package com.vessel.app.reagent.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.reagent.model.ReachInfo

class ReagentReachInfoAdapter : BaseAdapterWithDiffUtil<ReachInfo>() {
    override fun getLayout(position: Int) = R.layout.item_reagent_reach_info
}
