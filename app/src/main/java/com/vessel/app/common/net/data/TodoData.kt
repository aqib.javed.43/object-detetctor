package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass
import com.vessel.app.Constants

@JsonClass(generateAdapter = true)
data class TodoData(
    val id: Int,
    val name: String,
    val quantity: Float,
    val insert_date: String? = null,
    val usda_ndb_number: Int? = 0,
    val sample_id: Int? = 0,
    val completed_date: String? = null,
    val due_date: String? = null,
    val due_date_dt: String? = null,
    val recurring_todo_id: Int? = Constants.TODO_WITHOUT_RECURRING_TODO_ID,
    val food_id: Int? = null,
    val food: Food? = null,
    val has_reminder: Boolean? = null,
    val day_of_week: List<Int>? = null,
    val reminder_time_of_day: String? = null,
)
