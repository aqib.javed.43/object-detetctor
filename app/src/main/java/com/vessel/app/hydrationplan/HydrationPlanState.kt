package com.vessel.app.hydrationplan

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.checkscience.model.ScienceReference
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentModel
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.hydrationplan.model.HydrationPlanHeaderModel
import com.vessel.app.reagent.model.ReagentDetails
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class HydrationPlanState @Inject constructor() {
    val headerItems = MutableLiveData<HydrationPlanHeaderModel>()
    val reminderItems = MutableLiveData<List<ReminderRowUiModel>>()
    val hydrationReagentInfo = MutableLiveData<ReagentDetails>()
    val reagentInfo = MutableLiveData<List<ImpactTestReagentModel>>()
    val hydrationGoalsItems = MutableLiveData<List<ImpactGoalModel>>()
    val navigateTo = LiveEvent<NavDirections>()

    fun getCheckOutScience(): Array<ScienceReference> {
        return arrayOf(
            ScienceReference(R.string.water_link_1, R.string.water_text_1),
            ScienceReference(R.string.water_link_2, R.string.water_text_2),
            ScienceReference(R.string.water_link_3, R.string.water_text_3),
            ScienceReference(R.string.water_link_4, R.string.water_text_4),
            ScienceReference(R.string.water_link_5, R.string.water_text_5),
            ScienceReference(R.string.water_link_6, R.string.water_text_6),
            ScienceReference(R.string.water_link_7, R.string.water_text_7)
        )
    }

    operator fun invoke(block: HydrationPlanState.() -> Unit) = apply(block)
}
