package com.vessel.app.splashscreen

import android.os.Handler
import android.os.Looper
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.usecase.PrefetchUseCase
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import kotlinx.coroutines.launch

class SplashScreenViewModel @ViewModelInject constructor(
    val state: SplashScreenState,
    resourceProvider: ResourceRepository,
    private val authManager: AuthManager,
    private val preferencesRepository: PreferencesRepository,
    private val prefetchUseCase: PrefetchUseCase,
    private val remoteConfiguration: RemoteConfiguration,
    private val tabsManager: HomeTabsManager
) : BaseViewModel(resourceProvider) {

    init {
        tabsManager.clear()
    }

    fun init() {
        remoteConfiguration.fetchConfigFromRemote()
        preferencesRepository.checkCompletedProgram = true
        if (authManager.isAuthenticated()) {
            if (!preferencesRepository.onboardingCompleted) {
                navigate(SplashScreenState.NavigationDestination.ONBOARDING)
            } else {
                loadAppDate()
            }
        } else {
            navigate(SplashScreenState.NavigationDestination.WELCOME)
        }
    }

    fun navigate(destination: SplashScreenState.NavigationDestination) {
        val block = {
            state.navigateTo.postValue(destination)
        }
        Handler(Looper.getMainLooper()).postDelayed(block, DELAY_SPLASH_SCREEN)
    }

    private fun loadAppDate() {
        showLoading()
        viewModelScope.launch {
            prefetchUseCase.fetchAppData()
            saveAppsFlyerAttribute()
            hideLoading(false)
            navigate(SplashScreenState.NavigationDestination.HOME)
        }
        prefetchUseCase.loadOptionalAppData()
    }

    fun onAppOpenAttribution(data: MutableMap<String, String>?) {
        state.appsFlyerData = data
    }

    private fun saveAppsFlyerAttribute() {
        preferencesRepository.appsFlyerAttributeDeepLink = state.appsFlyerData?.get(DEEP_LINK_VALUE)
        preferencesRepository.appsFlyerAttributeUrl = state.appsFlyerData?.get(LINK_KEY)
        preferencesRepository.appsFlyerAttributeLinkId = state.appsFlyerData?.get(LINK_ID_KEY)
    }

    companion object {
        private const val DELAY_SPLASH_SCREEN = 2000L
        const val DEEP_LINK_VALUE = "deep_link_value"
        const val LINK_ID_KEY = "link_id"
        const val LINK_KEY = "link"
    }
}
