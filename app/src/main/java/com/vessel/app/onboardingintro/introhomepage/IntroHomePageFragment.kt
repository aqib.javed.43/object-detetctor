package com.vessel.app.onboardingintro.introhomepage

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class IntroHomePageFragment : BaseFragment<IntroHomePageViewModel>() {
    override val viewModel: IntroHomePageViewModel by viewModels()
    override val layoutResId = R.layout.fragment_onboarding_intro_homepage
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
        }
    }
}
