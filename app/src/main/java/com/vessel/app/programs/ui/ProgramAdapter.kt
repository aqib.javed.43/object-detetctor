package com.vessel.app.programs.ui

import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.Program
import com.vessel.app.common.widget.ProgramCardWidgetOnActionHandler

class ProgramAdapter(private val handler: ProgramCardWidgetOnActionHandler) :
    BaseAdapterWithDiffUtil<Program>(
        areItemsTheSame = { oldItem, newItem -> oldItem.isEnrolled == newItem.isEnrolled }
    ) {
    override fun getLayout(position: Int) = R.layout.item_program

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<Program>, position: Int) {
        super.onBindViewHolder(holder, position)
        val mainGoal = Goal.values().firstOrNull { it.id == getItem(position).mainGoalId }
        val programDescription =
            holder.itemView.findViewById<AppCompatTextView>(R.id.programDescription)
        if (mainGoal != null) {
            val goalName = holder.itemView.context.getString(mainGoal.title)
            programDescription.text =
                holder.itemView.context.getString(R.string.goal_program_template, goalName)
        } else {
            programDescription.isVisible = false
        }
    }
}
