package com.vessel.app.recommendations.ui

import android.graphics.Color
import android.os.Bundle
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.recommendations.RecommendationsViewModel
import com.vessel.app.util.WindowUtils
import com.vessel.app.util.extensions.afterMeasured
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_recommendations.*

@AndroidEntryPoint
class RecommendationsFragment : BaseFragment<RecommendationsViewModel>() {
    override val viewModel: RecommendationsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_recommendations
    private val recommendationsAdapter by lazy { RecommendationsAdapter(viewModel) }

    val firstTime = true
    private val onPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            viewModel.onPageChanged(position)
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        recommendationsViewPager.apply {
            isUserInputEnabled = false
            registerOnPageChangeCallback(onPageChangeCallback)
            adapter = recommendationsAdapter
        }

        setupObservers()
        setPagerHeight()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(pages) {
                recommendationsAdapter.submitList(it)
                TabLayoutMediator(
                    recommendationsTabLayout,
                    recommendationsViewPager
                ) { tab, position ->
                    tab.text = getString(it[position].title)
                }.attach()

                recommendationsAdapter.notifyDataSetChanged()
                it.forEachIndexed { index, recommendationsPage ->
                    val count = recommendationsPage.calculateTotalBadgeNumber()
                    val tabBadge = recommendationsTabLayout.getTabAt(index)?.orCreateBadge
                    if (count> 0) {
                        tabBadge?.number = count
                        setTabBadgeItem(tabBadge)
                    } else {
                        recommendationsTabLayout.getTabAt(index)?.removeBadge()
                    }
                }
                if (firstTime) {
                    updateTabWidthAndMode()
                }
            }
        }
    }

    private fun setTabBadgeItem(tabBadge: BadgeDrawable?) {
        tabBadge?.let {
            it.verticalOffset = Constants.TAB_VERTICAL_BADGE_OFFSET
            it.horizontalOffset = Constants.TAB_HORIZONTAL_BADGE_OFFSET
            it.backgroundColor = Color.RED
        }
    }

    private fun setPagerHeight() {
        recommendationsTabLayout.afterMeasured {
            val screenHeight = WindowUtils.getDisplayMetrics(requireContext()).heightPixels
            val tabViewY = this.y
            recommendationsViewPager.updateLayoutParams {
                height = (screenHeight - tabViewY).toInt()
            }
        }
    }

    private fun updateTabWidthAndMode() {
        recommendationsTabLayout.afterMeasured {
            val screenWidth = WindowUtils.getDisplayMetrics(requireContext()).widthPixels
            val tabCount = recommendationsTabLayout.tabCount
            var tabsWidth = 0
            for (index in 0..tabCount) {
                val tabWidth = recommendationsTabLayout.getTabAt(index)?.view?.width ?: 0
                tabsWidth += Constants.TAB_HORIZONTAL_BADGE_PADDING + tabWidth
                recommendationsTabLayout.getTabAt(index)?.view?.updateLayoutParams {
                    width = Constants.TAB_HORIZONTAL_BADGE_PADDING + tabWidth
                }
            }

            if (tabsWidth > screenWidth) {
                recommendationsTabLayout.tabMode = TabLayout.MODE_SCROLLABLE
            } else {
                recommendationsTabLayout.tabMode = TabLayout.MODE_FIXED
            }
            recommendationsTabLayout.getTabAt(recommendationsTabLayout.selectedTabPosition)?.select()
            recommendationsTabLayout.requestLayout()
        }
    }
}
