package com.vessel.app.covid19.db

import android.content.Context
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.covid19.model.CovidTest
import com.vessel.app.covid19.model.CovidTest.Companion.DATE_FORMAT
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.Date
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CovidTestDbMapper @Inject constructor(
    @ApplicationContext private val context: Context,
    private val preferencesRepository: PreferencesRepository
) {

    fun map(dbObjects: List<CovidTestDb>) = dbObjects.map {
        CovidTest(
            context = context,
            uuid = it.uuid,
            date = DATE_FORMAT.parse(it.insertDate) ?: Date(),
            status = CovidTest.Status.valueOf(it.status),
            email = preferencesRepository.userEmail.orEmpty()
        )
    }
}
