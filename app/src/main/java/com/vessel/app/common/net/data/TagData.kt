package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TagData(
    val title: String,
    val is_active: Boolean,
    val id: Int
)
