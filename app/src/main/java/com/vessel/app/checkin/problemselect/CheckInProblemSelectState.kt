package com.vessel.app.checkin.problemselect

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.onboarding.problemselection.model.ProblemSelect
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.model.GoalButton
import javax.inject.Inject

class CheckInProblemSelectState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val items = MutableLiveData<List<ProblemSelect>>()
    val selectedGoal = MutableLiveData<GoalButton>()
    operator fun invoke(block: CheckInProblemSelectState.() -> Unit) = apply(block)
}
