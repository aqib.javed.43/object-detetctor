package com.vessel.app.hydrationplan

import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.View
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.squareup.moshi.Moshi
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.checkscience.ui.OnActionHandler
import com.vessel.app.checkscience.utils.CheckOutScienceMapper
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.adapter.impactgoals.ImpactGoalsAdapter
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentAdapter
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentModel
import com.vessel.app.common.adapter.planedit.PlanEditAdapter
import com.vessel.app.common.adapter.planedit.PlanEditItem
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.common.adapter.planreminders.ReminderRowUiModel
import com.vessel.app.common.adapter.planreminders.toReminderList
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.ReagentEntry
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Sample
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.ReagentLifestyleRecommendation
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.hydrationplan.model.HydrationPlanHeaderModel
import com.vessel.app.hydrationplan.ui.HydrationPlanFragmentDirections
import com.vessel.app.hydrationplan.ui.HydrationPlanHeaderAdapter
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.reagent.ReagentConstant
import com.vessel.app.reagent.model.BenefitsOfBeingOptimal
import com.vessel.app.reagent.model.ReagentDetailsJsonAdapter
import com.vessel.app.reagent.model.ReagentHeader
import com.vessel.app.reagent.ui.ReagentEnergyAdapter
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HydrationPlanViewModel @ViewModelInject constructor(
    val state: HydrationPlanState,
    val resourceProvider: ResourceRepository,
    private val recommendationManager: RecommendationManager,
    private val planManager: PlanManager,
    private val scoreManager: ScoreManager,
    private val reagentsManager: ReagentsManager,
    private val reminderManager: ReminderManager,
    private val moshi: Moshi
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    HydrationPlanHeaderAdapter.HydrationPlanHeaderHandler,
    ReagentEnergyAdapter.OnActionHandler,
    ImpactGoalsAdapter.OnActionHandler,
    ImpactTestReagentAdapter.OnActionHandler,
    ReminderRowAdapter.OnActionHandler,
    PlanEditAdapter.OnActionHandler,
    OnActionHandler {

    private lateinit var hydrationPlan: Sample
    private var recommendedHydrationPlanTodoQuantity = 32f
    private var userPlans = listOf<PlanData>()

    init {
        observeRemindersDataState()
        loadHeaderData()
        readHydrationInfo()
        getHydrationGoals()
    }

    private fun observeRemindersDataState() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    planManager.clearCachedData()
                    loadHeaderData(true)
                }
            }
        }
    }

    private fun getHydrationGoals() {
        showLoading()
        viewModelScope.launch {
            recommendationManager.getLatestSampleRecommendations().onSuccess {
                hideLoading()
                hydrationPlan =
                    it.lifestyle.firstOrNull { it.lifestyle_recommendation_id == Constants.HYDRATION_PLAN_ID }
                    ?: return@onSuccess
                val goals = hydrationPlan.goals.orEmpty().mapIndexed { index, goal ->
                    ImpactGoalModel(
                        goal.name,
                        goal.image_small_url.orEmpty(),
                        Goal.alternateBackground(index),
                    )
                }

                state.hydrationGoalsItems.value = goals
            }
        }

        val hydrationReagent = reagentsManager.fromId(ReagentItem.Hydration.id)
        if (hydrationReagent != null) {
            state.reagentInfo.value = listOf(
                ImpactTestReagentModel(
                    hydrationReagent.displayName,
                    hydrationReagent.headerImage,
                    ""
                )
            )
        }
    }

    private fun loadHeaderData(forceUpdate: Boolean = false) {
        showLoading()
        viewModelScope.launch {
            val recommendationsResponse = recommendationManager.getLatestSampleRecommendations()
            val plansResponse = planManager.getUserPlans(forceUpdate)
            if (plansResponse is Result.Success && recommendationsResponse is Result.Success) {
                hideLoading(false)
                val waterPlans = plansResponse.value.filter { it.isHydrationPlan() }

                val recommendedHydrationPlanQuantity =
                    recommendationsResponse.value.lifestyle.firstOrNull { it.lifestyle_recommendation_id == Constants.HYDRATION_PLAN_ID }?.amount
                        ?: 0f

                val waterQuantity = if (recommendedHydrationPlanQuantity == 0f) {
                    recommendedHydrationPlanTodoQuantity // TODO remove this
                } else {
                    recommendedHydrationPlanTodoQuantity = recommendedHydrationPlanQuantity
                    recommendedHydrationPlanQuantity
                }

                val title = buildSpannedString {
                    append(getResString(R.string.water_needed))
                    appendLine()
                    inSpans(
                        RelativeSizeSpan(1.5f)
                    ) {
                        bold {
                            append(waterQuantity.toInt().toString())
                        }
                    }
                    inSpans(
                        RelativeSizeSpan(1.0f)
                    ) {
                        bold {
                            append(getResString(R.string.oz_quantity))
                        }
                    }
                    append(" ")
                    append(getResString(R.string.per_day))
                }

                setupWaterPlans(waterPlans)
                state.headerItems.value = HydrationPlanHeaderModel(title, waterPlans.isNotEmpty())
            }
        }
    }

    private fun setupWaterPlans(waterPlans: List<PlanData>) {
        val plans = waterPlans.toReminderList()
        userPlans = waterPlans
        state.reminderItems.value = plans
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onHydrationActionButtonClick() {
        openHydrationReminders()
    }

    fun openHydrationReminders() {
        if (userPlans.isEmpty()) {
            if (::hydrationPlan.isInitialized.not()) {
                getHydrationGoals()
                return
            }
            state.navigateTo.value =
                HydrationPlanFragmentDirections.actionHydrationPlanToPlanReminderDialog(
                    PlanReminderHeader(
                        getResString(R.string.hydration),
                        getHydrationPlanData().getDescription(),
                        "",
                        R.drawable.plan_add_hydration,
                        listOf(getHydrationPlanData()),
                        true
                    )
                )
        } else {
            state.navigateTo.value =
                HydrationPlanFragmentDirections.actionHydrationPlanToPlanReminderDialog(
                    PlanReminderHeader(
                        state.reminderItems.value.orEmpty().first().plan.getDisplayName(),
                        state.reminderItems.value.orEmpty().first().plan.getDescription(),
                        state.reminderItems.value.orEmpty().first().plan.getToDoImageUrl(),
                        0,
                        userPlans
                    )
                )
        }
    }

    override fun onCheckHydrationActionButtonClick() {
        viewModelScope.launch {
            scoreManager.getWellnessScores()
                .onSuccess { score ->
                    val hydrationScoreReagent =
                        score.reagents.filter { it.key.id == ReagentItem.Hydration.id }
                    if (hydrationScoreReagent.isEmpty()) {
                        // todo show error
                    } else {
                        navigateToHydration(
                            reagent = hydrationScoreReagent.keys.first(),
                            reagentValues = hydrationScoreReagent[
                                hydrationScoreReagent.keys.first { it.id == ReagentItem.Hydration.id }
                            ].orEmpty()
                        )
                    }
                }
        }
    }

    private fun navigateToHydration(
        reagent: com.vessel.app.common.model.data.Reagent,
        reagentValues: List<ReagentEntry>
    ) {
        state.navigateTo.value = HomeNavGraphDirections.globalActionToReagentDetails(
            ReagentHeader(
                id = reagent.id,
                title = reagent.displayName,
                unit = reagent.unit,
                chartEntries = reagentValues,
                lowerBound = reagent.lowerBound,
                upperBound = reagent.upperBound,
                minY = reagent.minValue,
                maxY = reagent.maxValue,
                background = reagent.headerImage,
                ranges = reagent.ranges,
                errorDescription = if (reagent.id == ReagentItem.B9.id) getResString(R.string.b9_not_available_message) else getResString(
                    R.string.reagent_test_results_unavailable,
                    reagent.displayName
                ),
                isBetaReagent = reagent.isBeta,
                betaHint = getResString(
                    R.string.reagent_beta_test_hint,
                    reagent.displayName
                ),
                consumptionUnit = reagent.consumptionUnit
            )
        )
    }

    private fun readHydrationInfo() {
        val hydrationId = ReagentItem.Hydration.id
        viewModelScope.launch {
            resourceProvider.readFile(
                ReagentConstant.FILE_TEMPLATE_JSON.format(
                    hydrationId,
                    hydrationId
                )
            )?.let {
                val reagentDetails = ReagentDetailsJsonAdapter(moshi).fromJson(it)
                state.hydrationReagentInfo.value = reagentDetails
            }
        }
    }

    fun getCheckOutScienceHeaderContent() =
        SpannableString(getResString(R.string.water_science_description))

    override fun onLinkButtonClick(view: View, link: String) {
        val navigateTo = when (link) {
            getResString(R.string.references) -> {
                HomeNavGraphDirections.globalActionToScienceReferencesDialogFragment(state.getCheckOutScience())
            }
            else -> {
                HomeNavGraphDirections.globalActionToWeb(link)
            }
        }
        view.findNavController().navigate(navigateTo)
    }

    fun getCheckOutScienceItems() = CheckOutScienceMapper.mapReferencesToScienceViewItems(
        state.getCheckOutScience(),
        this,
        resourceProvider
    )

    override fun onEnergyButtonClick(energy: BenefitsOfBeingOptimal) {
        state.navigateTo.value =
            HydrationPlanFragmentDirections.actionHydrationPlanToReagentSourceFragment(
                energy.sources.toTypedArray(),
                energy.information,
                getResString(R.string.hydration)
            )
    }

    override fun onGoalItemClick(item: ImpactGoalModel, view: View) {
    }

    override fun onReagentItemClick(item: ImpactTestReagentModel, view: View) {
    }

    override fun onReminderItemClick(item: ReminderRowUiModel, view: View) {
        openHydrationReminders()
    }

    override fun onPlanEditItemClick(item: PlanEditItem, view: View) {
        openHydrationReminders()
    }

    private fun getHydrationPlanData() = PlanData(
        multiple = 1,
        reagent_lifestyle_recommendation_id = hydrationPlan.id ?: Constants.HYDRATION_PLAN_ID,
        reagent_lifestyle_recommendation = ReagentLifestyleRecommendation(
            id = -1,
            lifestyle_recommendation_id = Constants.HYDRATION_PLAN_ID,
            quantity = hydrationPlan.amount,
            unit = hydrationPlan.unit,
            frequency = hydrationPlan.frequency
        )
    )
}
