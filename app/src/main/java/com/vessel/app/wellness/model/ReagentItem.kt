package com.vessel.app.wellness.model

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.data.*
import com.vessel.app.common.model.data.ReagentItem
import kotlinx.android.parcel.IgnoredOnParcel
import kotlin.math.roundToInt

data class ReagentItem(
    val id: Int,
    val title: String,
    val unit: String,
    val chartEntries: List<DateEntry>,
    val lowerBound: Float,
    val upperBound: Float,
    val minY: Float,
    val maxY: Float,
    val dimmed: Boolean,
    val ranges: List<ReagentRange> = listOf(),
    val isBetaTesting: Boolean,
    val isInverted: Boolean,
    val selectedChartEntityPosition: Int,
    val consumptionUnit: String?,
    val info: List<ReagentInfo>?,
    val state: ReagentState,
    val impact: Int? = null,
    val goalsImpacts: Map<Int, Int>? = null
) {

    @IgnoredOnParcel
    val lastChartValue =
        if (chartEntries.getOrNull(selectedChartEntityPosition)?.y == null) {
            Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE
        } else {
            chartEntries.getOrNull(selectedChartEntityPosition)!!.y
        }

    @IgnoredOnParcel
    val preLastChartValue =
        if (chartEntries.getOrNull(selectedChartEntityPosition - 1)?.y == null) {
            Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE
        } else {
            chartEntries.getOrNull(selectedChartEntityPosition - 1)!!.y
        }

    @IgnoredOnParcel
    val reagentChartValue = lastChartValue.toInt().toString()

    @IgnoredOnParcel
    val lastChartValuePercent = (lastChartValue / maxY * 100).roundToInt().toString()

    @IgnoredOnParcel
    val lastReagentChartRange = if (id == ReagentItem.Hydration.id) {
        ranges.firstOrNull { lastChartValue <= it.from && lastChartValue >= it.to }
    } else {
        ranges.firstOrNull { lastChartValue >= it.from && lastChartValue <= it.to }
    }

    @IgnoredOnParcel
    val preLastReagentChartRange = if (id == ReagentItem.Hydration.id) {
        ranges.firstOrNull { preLastChartValue <= it.from && preLastChartValue >= it.to }
    } else {
        ranges.firstOrNull { preLastChartValue >= it.from && preLastChartValue <= it.to }
    }

    @DrawableRes
    val background = when {
        impact == 0 -> R.drawable.gray_background
        state == ReagentState.COMING_SOON -> R.drawable.linen_background
        dimmed -> R.drawable.log_cabin_background
        lastChartValue == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE -> R.drawable.log_cabin_background
        lastReagentChartRange?.reagentType == ReagentLevel.Low || lastReagentChartRange?.reagentType == ReagentLevel.High -> R.drawable.rose_background
        else -> R.drawable.primary_background
    }

    @ColorRes
    val backgroundColor = when {
        impact == 0 -> R.color.grayBg
        state == ReagentState.COMING_SOON -> R.color.linen
        dimmed -> R.color.swirl
        lastChartValue == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE -> R.color.swirl
        lastReagentChartRange?.reagentType == ReagentLevel.Low || lastReagentChartRange?.reagentType == ReagentLevel.High -> R.color.roseGoldBg
        else -> R.color.pixiGreen
    }

    @ColorRes
    val preLastBackgroundColor = when {
        impact == 0 -> R.color.grayBg
        state == ReagentState.COMING_SOON -> R.color.linen
        dimmed -> R.color.swirl
        preLastChartValue == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE -> R.color.swirl
        preLastReagentChartRange?.reagentType == ReagentLevel.Low || preLastReagentChartRange?.reagentType == ReagentLevel.High -> R.color.roseGoldBg
        else -> R.color.pixiGreen
    }

    val status = when {
        state == ReagentState.COMING_SOON -> "Coming soon" // todo change the string to resource
        lastChartValue == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE -> "N/A"
        else -> lastReagentChartRange?.label ?: "N/A"
    }

    val preStatus = when {
        state == ReagentState.COMING_SOON -> "Coming soon" // todo change the string to resource
        preLastChartValue == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE -> "N/A"
        else -> preLastReagentChartRange?.label ?: "N/A"
    }

    @IgnoredOnParcel
    val showScoreValue = status != "N/A"

    @ColorRes
    val titleTextColor =
        if (state == ReagentState.COMING_SOON)
            R.color.darkText
        else if (dimmed || lastChartValue == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE) {
            R.color.grayAlpha50
        } else {
            R.color.darkText
        }

    @ColorRes
    val statusTextColor =
        if (state == ReagentState.COMING_SOON)
            R.color.grayMist
        else if (dimmed || lastChartValue == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE) {
            R.color.grayAlpha50
        } else {
            R.color.grayMist
        }

    @DrawableRes
    val impactImageIndicator =
        when (impact) {
            1 -> R.drawable.ic_one_black_dot_and_two_gray
            2 -> R.drawable.ic_two_black_dots_and_one_gray
            3 -> R.drawable.ic_three_black_dots
            else -> R.drawable.ic_three_gray_dots
        }

    @DrawableRes
    val reagentImage =
        when (ReagentItem.fromId(id)) {
            ReagentItem.Hydration -> R.drawable.hydration
            ReagentItem.B7 -> R.drawable.b7
            ReagentItem.Cortisol -> R.drawable.cortisol
            ReagentItem.Ketones -> R.drawable.ketones
            ReagentItem.Magnesium -> R.drawable.magnesium
            ReagentItem.PH -> R.drawable.ph
            ReagentItem.VitaminC -> R.drawable.vitaminc
            ReagentItem.Calcium -> R.drawable.calcium
            ReagentItem.Sodium -> R.drawable.sodium
            ReagentItem.Chloride -> R.drawable.sodium
            ReagentItem.Nitrites -> R.drawable.utis
            else -> null
        }
}
