package com.vessel.app.pretest.incuppee

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class InCupPeeState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: InCupPeeState.() -> Unit) = apply(block)
}
