package com.vessel.app.foodplan.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodplan.model.FoodHeaderItem

class FoodPlanHeaderAdapter(
    val handler: FoodHeaderHandler
) : BaseAdapterWithDiffUtil<FoodHeaderItem>() {
    override fun getLayout(position: Int) = R.layout.item_food_plan_header

    override fun getHandler(position: Int): FoodHeaderHandler = handler

    interface FoodHeaderHandler {
        fun onExportPlanButtonClick()
        fun onGetDeliveryButtonClick()
        fun onCloseClicked()
    }
}
