package com.vessel.app.views

import android.content.Context
import android.graphics.Color
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.ChartTouchListener
import com.github.mikephil.charting.listener.OnChartGestureListener
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.Utils
import com.vessel.app.Constants.RATE_IT_POINT
import com.vessel.app.R
import com.vessel.app.common.model.DateEntry
import com.vessel.app.wellness.ui.GoalButtonAdapter
import kotlinx.android.synthetic.main.view_chart_curve_select.view.*
import kotlinx.android.synthetic.main.view_header_chart.view.*
import java.text.SimpleDateFormat
import java.util.*

class HeaderChartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    companion object {
        private const val MAX_X_AXIS_LABELS = 5
        private const val MAX_SHOWN_ITEMS_IN_CHART = 5f
        private const val CHART_LABEL = "Data"
        val DATE_FORMAT = SimpleDateFormat("MMM\nd", Locale.ENGLISH)
        private const val SELECT_VIEW_ANIMATION_TIME = 900L
        private const val DRAG_CHART_ANIMATION_TIME = 800L
    }

    private val monthTextSize by lazy {
        context.resources.getDimensionPixelSize(R.dimen.font_2xs).toFloat()
    }
    private val monthMarginTop by lazy { context.resources.getDimensionPixelSize(R.dimen.spacing_large) }
    private val dayTextSize by lazy {
        context.resources.getDimensionPixelSize(R.dimen.font_title).toFloat()
    }
    private val dayMarginTop by lazy { context.resources.getDimensionPixelSize(R.dimen.spacing_3xs) }

    private val chartView by lazy { findViewById<LineChart>(R.id.chartView) }
    private val selectView by lazy { findViewById<ChartCurveSelectView>(R.id.chart_curve_select_view) }
    private val smileView by lazy { findViewById<SmileFaceView>(R.id.chart_smile_face_view) }

    private var isRescanErrorFlow = true
    private val selectedValueHighlightColor by lazy {
        ContextCompat.getColor(
            context,
            R.color.whiteAlpha60
        )
    }

    private lateinit var entries: MutableList<DateEntry>
    private lateinit var averageEntries: MutableList<DateEntry>
    private lateinit var animatedEntries: MutableList<DateEntry>
    private var isFirstEntitiesAppear = true
    private var selectedEntryPosition: Int = 0
    private var entrySelectionListener: ((Int) -> Unit)? = null
    var onInfoClick: (() -> Unit)? = null

    init {
        inflate(getContext(), R.layout.view_header_chart, this)
        clipChildren = false
        chartView.apply {
            xAxis.apply {
                setDrawGridLines(false)
                setDrawAxisLine(false)
                position = XAxis.XAxisPosition.BOTTOM
                valueFormatter = object : ValueFormatter() {
                    override fun getFormattedValue(value: Float): String {
                        updateAxisRender()
                        return value.toInt().toString()
                    }
                }
            }

            axisLeft.initAxis()
            axisRight.initAxis()

            setDrawGridBackground(false)
            setDrawBorders(false)
            setPinchZoom(false)
            setScaleEnabled(false)
            isHighlightPerDragEnabled = true
            isDoubleTapToZoomEnabled = false
            description.isEnabled = false
            legend.isEnabled = false
            viewPortHandler.setMinimumScaleX(1f)

            extraBottomOffset =
                Utils.convertPixelsToDp(monthTextSize + monthMarginTop + dayTextSize + dayMarginTop + 5)
            setXAxisRenderer(
                DateXAxisRendererWithRounded(
                    context,
                    viewPortHandler,
                    xAxis,
                    getTransformer(YAxis.AxisDependency.LEFT),
                    0,
                    0
                )
            )

            invalidate()
        }

        chartView.onChartGestureListener = object : OnChartGestureListener {
            override fun onChartGestureEnd(
                me: MotionEvent,
                lastPerformedGesture: ChartTouchListener.ChartGesture?
            ) {
                if (lastPerformedGesture == ChartTouchListener.ChartGesture.DRAG || lastPerformedGesture == ChartTouchListener.ChartGesture.FLING) {
                    Handler(Looper.getMainLooper()).postDelayed(
                        {
                            val middleShownValue =
                                (chartView.lowestVisibleX + chartView.visibleXRange / 2).toInt()
                            val closestEntryPosition = this@HeaderChartView.entries.toList()
                                .indexOfFirst { middleShownValue == it.x.toInt() }
                            if (closestEntryPosition > 0) {
                                setEntrySelected(closestEntryPosition)
                            }
                        },
                        DRAG_CHART_ANIMATION_TIME
                    )

                    // change to keep margin as the same between items
                    // val margin = context.getDimenInCalculatedPixels(R.dimen.spacing_9xl)

                    // chartView.setMargins(rightMarginPx = margin)
                }
            }

            override fun onChartFling(
                me1: MotionEvent?,
                me2: MotionEvent?,
                velocityX: Float,
                velocityY: Float
            ) {
                // Not used
            }

            override fun onChartSingleTapped(me: MotionEvent?) {
                // Not used
            }

            override fun onChartGestureStart(
                me: MotionEvent?,
                lastPerformedGesture: ChartTouchListener.ChartGesture?
            ) {
                // Not used
            }

            override fun onChartScale(me: MotionEvent?, scaleX: Float, scaleY: Float) {
                // Not used
            }

            override fun onChartLongPressed(me: MotionEvent?) {
                // Not used
            }

            override fun onChartDoubleTapped(me: MotionEvent?) {
                // Not used
            }

            override fun onChartTranslate(me: MotionEvent?, dX: Float, dY: Float) {
                // Not used
            }
        }

        chartView.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onNothingSelected() {
                // Not used
            }

            override fun onValueSelected(e: Entry, h: Highlight?) {
                val closestEntryPosition =
                    this@HeaderChartView.entries.toList().indexOfFirst { e.x == it.x }
                if (closestEntryPosition > 0) {
                    isRescanErrorFlow = false
                    setEntrySelected(closestEntryPosition)
                }
            }
        })
        chart_curve_select_view.setOnClickListener {
            onInfoClick?.invoke()
        }
    }

    fun setEntrySelected(entryPosition: Int) {
        if (this.entries[entryPosition].y != -1f) {
            this@HeaderChartView.selectedEntryPosition = entryPosition
            updateAxisRenderData()
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    moveHighlightToEntryPosition(selectedEntryPosition)
                },
                SELECT_VIEW_ANIMATION_TIME + 100
            )
            onEntrySelected(entryPosition)
            updateChartData(entryPosition)
            if (isRescanErrorFlow)
                chartView.centerViewTo(selectedEntryPosition.toFloat(), 0f, YAxis.AxisDependency.LEFT)
        }
    }

    private fun onEntrySelected(selectedEntryPosition: Int) {
        entrySelectionListener?.invoke(selectedEntryPosition)
    }

    fun setChartEntrySelectionListener(listener: (Int) -> Unit) {
        this.entrySelectionListener = listener
    }

    private fun moveHighlightToEntryPosition(entryPosition: Int) {
        val chartEntry = this.entries[entryPosition]
        val entryPoint = chartView.getTransformer(YAxis.AxisDependency.LEFT).getPixelForValues(
            chartEntry.x,
            chartEntry.y
        )
        selectView.startTransition(
            entryPoint.x.toFloat() - context.resources.getDimensionPixelSize(R.dimen.spacing_chart),
            entryPoint.y.toFloat(),
            chartEntry.y.toInt().toString(),
            showNumber = chartEntry.showNumber
        )
        smileView.startTransition(
            entryPoint.x.toFloat() - context.resources.getDimensionPixelSize(R.dimen.spacing_chart),
            entryPoint.y.toFloat(),
            chartEntry.y.toInt(),
            date = chartEntry.date,
            showSmileFace = chartEntry.showNumber.not()
        )
    }

    private fun updateEntryValue(entry: DateEntry) {
        val entryPoint = chartView.getTransformer(YAxis.AxisDependency.LEFT).getPixelForValues(
            entry.x,
            entry.y
        )

        selectView.updateLabelValue(
            entryPoint.y.toFloat(),
            entry.y.toInt().toString(),
            showNumber = entry.showNumber
        )
        smileView.updateLabelValue(
            entryPoint.y.toFloat(),
            entry.y.toInt(),
            date = entry.date,
            showSmileFace = entry.showNumber.not()
        )
    }

    private fun updateChartData(selectedEntryPosition: Int, moveToEnd: Boolean = false) {
        chartView.data = LineData().apply {
            if (entries.size >= MAX_X_AXIS_LABELS) {
                val transparentStartLine =
                    LineDataSet(averageEntries.subList(0, 1).toList(), CHART_LABEL).apply {
                        color = Color.TRANSPARENT
                        lineWidth = 0f
                        setDrawCircles(false)
                        setDrawCircleHole(false)
                        setDrawValues(false)
                        setDrawHorizontalHighlightIndicator(false)
                        setDrawVerticalHighlightIndicator(false)
                        enableDashedLine(12f, 8f, 0f)
                        mode = LineDataSet.Mode.CUBIC_BEZIER
                    }
                addDataSet(transparentStartLine)

                // Draw line for first actual reading
                addDataSet(
                    LineDataSet(
                        averageEntries.subList(1, averageEntries.size).toList(),
                        CHART_LABEL
                    ).apply {
                        color = ContextCompat.getColor(context, R.color.darkText4)
                        lineWidth = 1.25f
                        setDrawCircles(false)
                        setDrawCircleHole(false)
                        setDrawValues(false)
                        setDrawHorizontalHighlightIndicator(false)
                        setDrawVerticalHighlightIndicator(false)
                        enableDashedLine(12f, 8f, 0f)
                        mode = LineDataSet.Mode.CUBIC_BEZIER
                    }
                )
            }
            // Draw transparent Start Line for added point
            addDataSet(
                LineDataSet(entries.subList(0, 1).toList(), CHART_LABEL).apply {
                    color = Color.TRANSPARENT
                    circleHoleColor = Color.TRANSPARENT
                    circleHoleRadius = 3f
                    circleRadius = 6f
                    lineWidth = 2f
                    setDrawValues(false)

                    setDrawHorizontalHighlightIndicator(false)
                    setDrawVerticalHighlightIndicator(false)

                    val circleColors = mutableListOf<Int>()

                    circleColors.add(0, Color.TRANSPARENT)
                    circleColors.add(1, Color.TRANSPARENT)
                    setCircleColors(circleColors)
                    mode = LineDataSet.Mode.CUBIC_BEZIER
                }
            )
            // Draw invisible line for the null y data
            val firstYNull = entries.indexOfFirst { it.y == -1f }
            val lastYNull = entries.indexOfLast { it.y == -1f }
            if (firstYNull != -1 && lastYNull != -1) {
                val emptyScoreEntries = entries.subList(firstYNull, lastYNull)
                addDataSet(
                    LineDataSet(emptyScoreEntries.toList(), CHART_LABEL).apply {
                        color = Color.TRANSPARENT
                        circleHoleColor = Color.BLACK
                        circleHoleRadius = 3f
                        circleRadius = 6f
                        lineWidth = 2f
                        setDrawValues(false)

                        setDrawHorizontalHighlightIndicator(false)
                        setDrawVerticalHighlightIndicator(false)

                        val circleColors = mutableListOf<Int>()

                        for (index in 1..entries.size) {
                            if (index == selectedEntryPosition) {
                                circleColors.add(selectedValueHighlightColor)
                            } else {
                                circleColors.add(Color.TRANSPARENT)
                            }
                        }

                        setCircleColors(circleColors)
                        mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                    }
                )
            }
            val firstYNotNull = entries.indexOfFirst { it.y != -1f }
            val notEmptyData = if (firstYNotNull == 0 || firstYNotNull == -1) 1 else firstYNotNull
            val hideLastLine = entries.last().showNumber.not() && entries.last().y == RATE_IT_POINT
            val scoreEntries = if (hideLastLine)
                entries.subList(notEmptyData, entries.size - 1)
            else
                entries.subList(notEmptyData, entries.size)
            addDataSet(
                LineDataSet(scoreEntries.toList(), CHART_LABEL).apply {
                    color = Color.BLACK
                    circleHoleColor = Color.BLACK
                    circleHoleRadius = 3f
                    circleRadius = 6f
                    lineWidth = 2f
                    setDrawValues(false)

                    setDrawHorizontalHighlightIndicator(false)
                    setDrawVerticalHighlightIndicator(false)

                    val circleColors = mutableListOf<Int>()

                    val firstIndex = entries.indexOfFirst { it.y != -1f }
                    val addedIndex = if (firstIndex == 0) 0 else firstIndex - 1
                    val indexItem = selectedEntryPosition + addedIndex

                    for (index in 1..scoreEntries.size) {
                        if (index == indexItem) {
                            circleColors.add(selectedValueHighlightColor)
                        } else {
                            circleColors.add(Color.TRANSPARENT)
                        }
                    }
                    setCircleColors(circleColors)
                    mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                }
            )
            if (hideLastLine) {
                val lastEntry = entries.subList(entries.size - 1, entries.size)
                addDataSet(
                    LineDataSet(lastEntry.toList(), CHART_LABEL).apply {
                        color = Color.BLACK
                        circleHoleColor = Color.TRANSPARENT
                        circleHoleRadius = -1f
                        circleRadius = -1f
                        lineWidth = 2f
                        setDrawValues(false)

                        setDrawHorizontalHighlightIndicator(false)
                        setDrawVerticalHighlightIndicator(false)

                        setCircleColor(Color.TRANSPARENT)
                        mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                    }
                )
            }
        }
        if (moveToEnd) {
            if (entries.size >= MAX_SHOWN_ITEMS_IN_CHART)
                chartView.setVisibleXRangeMinimum(MAX_SHOWN_ITEMS_IN_CHART)
            else
                chartView.setVisibleXRangeMinimum(1f)
            chartView.setVisibleXRangeMaximum(MAX_SHOWN_ITEMS_IN_CHART)
            chartView.moveViewToX(entries[selectedEntryPosition].x)
            onEntrySelected(selectedEntryPosition)
        }
        chartView.notifyDataSetChanged()
        chartView.invalidate()
    }

    private fun YAxis.initAxis() {
        setDrawGridLines(false)
        setDrawAxisLine(false)
        setDrawLabels(false)
        axisMinimum = 0f
        axisMaximum = 100f
    }

    fun setEntries(items: List<DateEntry>, averageEntries: List<DateEntry>) {
        var isItemsUpdated = false
        if (::entries.isInitialized && items.size == entries.size - 1) {
            items.forEachIndexed { index, item ->
                val oldEntry = this.entries[index + 1]
                if (oldEntry.x != item.x || oldEntry.y != item.y) {
                    isItemsUpdated = true
                }
            }
        } else {
            isItemsUpdated = true
            selectedEntryPosition = items.size
        }

        if (isItemsUpdated.not() && ::entries.isInitialized) {
            return
        }

        this.entries = items.toMutableList()
        this.averageEntries = averageEntries.toMutableList()
        this.entries.add(
            0,
            DateEntry(
                0f,
                items.first().y,
                null
            )
        )
        this.averageEntries.add(
            0,
            DateEntry(
                0f,
                averageEntries.first().y,
                null
            )
        )
        chartView.apply {
            xAxis.setLabelCount(
                if (this@HeaderChartView.entries.size > MAX_X_AXIS_LABELS) MAX_X_AXIS_LABELS
                else this@HeaderChartView.entries.size,
                false
            )

            updateChartData(
                if (selectedEntryPosition == 0) this@HeaderChartView.entries.size - 1
                else selectedEntryPosition,
                true
            )

            showAnimation()

            if (isFirstEntitiesAppear || isItemsUpdated) {
                selectedEntryPosition = this@HeaderChartView.entries.size - 1
                updateAxisRenderData()
                Handler(Looper.getMainLooper()).postDelayed(
                    {
                        moveHighlightToEntryPosition(selectedEntryPosition)
                        selectView.isVisible = true
                        smileView.isVisible = true
                    },
                    SELECT_VIEW_ANIMATION_TIME + 100
                )
                isFirstEntitiesAppear = false
                if (isItemsUpdated) {
                    onEntrySelected(selectedEntryPosition)
                }
            } else {
                updateEntryValue(
                    this@HeaderChartView.entries[selectedEntryPosition]
                )
            }
        }
    }

    fun updateAxisRender() {
        (chartView.rendererXAxis as DateXAxisRendererWithRounded).updateWidthHeight(selectView.width, selectView.height / 3)
    }
    fun updateAxisRenderData() {
        (chartView.rendererXAxis as DateXAxisRendererWithRounded).updateData(this.entries, selectedEntryPosition)
    }
    private fun showAnimation() {
        val isSameListDataAnimated =
            ::animatedEntries.isInitialized && animatedEntries.size == entries.size &&
                animatedEntries.mapIndexed { index, item -> item.equalTo(entries[index]) }
                    .all { it }

        if (::animatedEntries.isInitialized.not() || !isSameListDataAnimated) {
            chartView.animateX(SELECT_VIEW_ANIMATION_TIME.toInt(), Easing.EaseInOutQuad)
        }
        animatedEntries = this.entries
    }
}

@BindingAdapter("entrySelectionListener")
fun HeaderChartView.setEntrySelectionListener(handler: GoalButtonAdapter.OnActionHandler?) {
    this.setChartEntrySelectionListener {
        if (it > 0) {
            handler?.onGoalEntryClicked(it - 1)
        }
    }
}

@BindingAdapter("entries", "averageEntries")
fun HeaderChartView.setChartEntries(entries: List<DateEntry>, averageEntries: List<DateEntry>) {
    setEntries(entries, averageEntries)
}
