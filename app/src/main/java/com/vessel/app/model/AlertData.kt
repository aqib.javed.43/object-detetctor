package com.vessel.app.model

data class AlertData(
    val title: String,
    val body: String
)
