package com.vessel.app.today.model

import com.vessel.app.common.model.Lesson
import java.util.*

data class LessonListItem(
    var items: List<Lesson>? = arrayListOf(),
    var programTitle: String? = null,
    var mainGoalTitle: String? = null,
    var noMoreLesson: Boolean = true,
    var isPreviousDay: Boolean? = false,
    val date: Date
) {
    fun isFinishAll(): Boolean {
        if (items.isNullOrEmpty()) return true
        var isFinishAll = true
        for (item in items!!) {
            if (!item.completed)isFinishAll = false
        }
        return isFinishAll
    }
    fun isFinishAllExcludeLesson(lessonId: Long): Boolean {
        if (items.isNullOrEmpty()) return true
        var isFinishAll = true
        for (item in items!!) {
            if (!item.completed && item.id != lessonId)isFinishAll = false
        }
        return isFinishAll
    }
    fun isFinishAllWithProgram(programId: Int): Boolean {
        if (items.isNullOrEmpty()) return false
        var isFinishAll = true
        val filters = items.orEmpty().filter { lesson ->
            lesson.programId == programId
        }
        for (item in filters) {
            if (item.completed.not())isFinishAll = false
        }
        return isFinishAll
    }
    fun getTitle(): String {
        return programTitle?.replace("Program", "") + "Lesson"
    }
    fun getGoalTitle(): String {
        return "$mainGoalTitle Lessons"
    }
    fun getFinishAllTitle(): String {
        return String.format("You’ve completed all the $mainGoalTitle lessons. Would you like to add another goal?")
    }
}
