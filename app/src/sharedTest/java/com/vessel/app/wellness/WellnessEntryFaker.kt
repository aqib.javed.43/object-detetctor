package com.vessel.app.wellness

import com.vessel.app.BaseFaker
import com.vessel.app.BaseFaker.Fake.FAKER
import com.vessel.app.common.model.DateEntry

object WellnessEntryFaker {
    fun basic() = FAKER.run {
        DateEntry(
            x = number().randomDigit().toFloat(),
            y = number().randomDigit().toFloat(),
            date = date().birthday().time
        )
    }

    fun list() = BaseFaker.list { basic() }
}
