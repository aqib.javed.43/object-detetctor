package com.vessel.app.onboarding.lifestyle

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.onboarding.lifestyle.model.LifestyleSelectModel
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnboardingLifestyleState @Inject constructor(val isLastStep: Boolean) {
    val items = MutableLiveData<List<LifestyleSelectModel>>()
    val refreshItems = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()
    val navigateToHome = LiveEvent<Unit>()

    operator fun invoke(block: OnboardingLifestyleState.() -> Unit) = apply(block)
}
