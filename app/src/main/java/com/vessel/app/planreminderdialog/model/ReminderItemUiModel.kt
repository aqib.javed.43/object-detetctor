package com.vessel.app.planreminderdialog.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.vessel.app.Constants
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.util.extensions.getDayFullName
import com.vessel.app.util.extensions.getDayShortName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class ReminderItemUiModel(
    val timeOfReminder: String?,
    val dayOfWeek: MutableList<Int>?,
    val todo: PlanData,
    val isUpdated: Boolean = false,
    val isTestingPlan: Boolean = false
) : Parcelable {

    fun getReminderDays(): String? {
        return if (dayOfWeek?.size == WEEK_DAYS_COUNT) {
            "Daily"
        } else if (dayOfWeek?.size == 2 &&
            dayOfWeek.contains(Calendar.SATURDAY) &&
            dayOfWeek.contains(Calendar.SUNDAY)
        ) {
            "Weekends"
        } else if (dayOfWeek?.size == 1) {
            return getDayFullName(dayOfWeek.first())
        } else {
            dayOfWeek?.sorted()
                ?.joinToString(", ") { getDayShortName(it) }
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getTime(): String? {
        return (getDate() ?: Constants.SIMPLE_TIME_24_HOUR_FORMAT.parse("12:00:00"))?.let {
            Constants.SIMPLE_TIME_12_HOUR_FORMAT.format(it).orEmpty().split(" ")[0]
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getTimeFormattedWithMeridian(): String? {
        return getDate()?.let {
            Constants.SIMPLE_TIME_12_HOUR_FORMAT.format(it).orEmpty()
        } ?: "Set time"
    }

    @SuppressLint("SimpleDateFormat")
    fun getTimeMeridian(): String? {
        return timeOfReminder?.split(":")?.get(0)?.let {
            if (Integer.valueOf(it) >= 12) "PM" else "AM"
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getTimeHoursMinutes(): Pair<Int, Int> {
        val time = timeOfReminder?.split(":") ?: listOf("12", "0")
        return Pair(time.first().toInt(), time.last().toInt())
    }

    @SuppressLint("SimpleDateFormat")
    fun getDate() = if (timeOfReminder?.isNotEmpty() == true) {
        Constants.SIMPLE_TIME_24_HOUR_FORMAT.parse(timeOfReminder)
    } else {
        null
    }

    private
    companion object {
        const val WEEK_DAYS_COUNT = 7
    }
}
