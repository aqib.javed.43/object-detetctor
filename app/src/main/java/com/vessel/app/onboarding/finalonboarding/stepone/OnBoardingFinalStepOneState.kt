package com.vessel.app.onboarding.finalonboarding.stepone

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnBoardingFinalStepOneState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: OnBoardingFinalStepOneState.() -> Unit) = apply(block)
}
