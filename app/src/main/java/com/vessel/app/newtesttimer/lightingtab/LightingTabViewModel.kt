package com.vessel.app.newtesttimer.lightingtab

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class LightingTabViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider)
