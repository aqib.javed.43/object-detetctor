package com.vessel.app.common.net

import com.vessel.app.common.ext.addConverterFactories
import com.vessel.app.common.ext.addInterceptors
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class RestClient(
    private val debug: Boolean,
    private val readTimeoutValue: Long,
    private val readTimeoutUnit: TimeUnit = TimeUnit.SECONDS,
    private val writeTimeoutValue: Long,
    private val writeTimeoutUnit: TimeUnit = TimeUnit.SECONDS,
    private val connectTimeoutValue: Long,
    private val connectTimeoutUnit: TimeUnit = TimeUnit.SECONDS,
    private val converterFactories: List<Converter.Factory>,
    private val interceptors: List<Interceptor>,
    private val tokenAuthenticator: TokenAuthenticator
) {
    private val okHttpClient = createOkHttpClient(false)
    private val okHttpClientAuth = createOkHttpClient(true)

    fun createRetrofitAdapter(baseUrl: String, auth: Boolean = false): Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .validateEagerly(debug)
        .client(if (auth) okHttpClientAuth else okHttpClient)
        .addConverterFactories(converterFactories)
        .build()

    private fun createOkHttpClient(auth: Boolean) = OkHttpClient.Builder()
        .readTimeout(readTimeoutValue, readTimeoutUnit)
        .writeTimeout(writeTimeoutValue, writeTimeoutUnit)
        .connectTimeout(connectTimeoutValue, connectTimeoutUnit)
        .addInterceptors(interceptors)
        .apply { if (auth) authenticator(tokenAuthenticator).addInterceptor(tokenAuthenticator) }
        .build()
}
