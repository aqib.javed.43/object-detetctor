package com.vessel.app.toprecommendations

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.model.Tip
import javax.inject.Inject

class TopRecommendationsState @Inject constructor(
    val goalSelected: GoalSelect,
    val resourceProvider: ResourceRepository
) {
    val items = MutableLiveData<List<Tip>>()
    val title = MutableLiveData<String>(
        resourceProvider.getString(
            R.string.pick_recommendation,
            resourceProvider.getString(goalSelected.title)
        )
    )
    val subtitle = MutableLiveData<String>(
        resourceProvider.getString(
            R.string.pick_recommendation_subtitle,
            resourceProvider.getString(goalSelected.title)
        )
    )
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: TopRecommendationsState.() -> Unit) = apply(block)
}
