package com.vessel.app.vesselfuelcheckout.model

import android.os.Parcelable
import com.vessel.app.supplementsbucket.model.Supplement
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VesselFuelItemUiModel(
    val title: String,
    val quantity: String,
    val price: Double
) : Parcelable

fun Supplement.toVesselFuelItem() = VesselFuelItemUiModel(
    name,
    amount(),
    price
)
