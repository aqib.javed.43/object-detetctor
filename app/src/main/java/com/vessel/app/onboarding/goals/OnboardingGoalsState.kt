package com.vessel.app.onboarding.goals

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.onboarding.goals.model.OnboardingGoalSelect
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnboardingGoalsState @Inject constructor() {
    val items = MutableLiveData<List<OnboardingGoalSelect>>()
    val refreshItems = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: OnboardingGoalsState.() -> Unit) = apply(block)
}
