package com.vessel.app.createaccount

import android.view.View
import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class CreateAccountState @Inject constructor(val email: String, val fromAppleGoogleSSOAuth: Boolean) {
    val userEmail = MutableLiveData(email)
    val userFirstName = MutableLiveData("")
    val userLastName = MutableLiveData("")
    val userPassword = MutableLiveData("")
    val passwordEditTextVisibility = MutableLiveData(if (fromAppleGoogleSSOAuth) View.GONE else View.VISIBLE)

    operator fun invoke(block: CreateAccountState.() -> Unit) = apply(block)
}
