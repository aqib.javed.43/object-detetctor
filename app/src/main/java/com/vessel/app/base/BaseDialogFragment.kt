package com.vessel.app.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.vessel.app.BR
import com.vessel.app.R
import com.vessel.app.util.extensions.observe

abstract class BaseDialogFragment<VM : BaseViewModel> : AppCompatDialogFragment() {
    protected abstract val viewModel: VM
    protected abstract val layoutResId: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setStyle(STYLE_NO_FRAME, R.style.CustomDialog)

        onBeforeViewLoad(savedInstanceState)

        val viewDataBinding =
            DataBindingUtil.inflate<ViewDataBinding>(inflater, layoutResId, container, false)

        return viewDataBinding?.apply {
            setVariable(BR.viewModel, viewModel)
            lifecycleOwner = this@BaseDialogFragment
        }?.root ?: inflater.inflate(layoutResId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        onViewLoad(savedInstanceState)
        initObservers()
    }

    open fun onBeforeViewLoad(savedInstanceState: Bundle?) {
        // Intentionally empty so that subclasses can override if necessary
    }

    open fun onViewLoad(savedInstanceState: Bundle?) {
        // Intentionally empty so that subclasses can override if necessary
    }

    private fun initObservers() {
        observe(viewModel.showAlert) {
            Toast.makeText(requireContext(), it.body, Toast.LENGTH_LONG).show()
        }
    }
}
