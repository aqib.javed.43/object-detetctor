package com.vessel.app.signin.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseAppleGoogleAuthFragment
import com.vessel.app.common.model.AuthToken
import com.vessel.app.signin.SignInViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInFragment : BaseAppleGoogleAuthFragment<SignInViewModel>() {
    override val viewModel: SignInViewModel by viewModels()
    override val layoutResId = R.layout.fragment_sign_in

    override fun onAuthTokenReceived(authToken: AuthToken) {
        viewModel.onAuthTokenReceived(authToken)
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)

        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(closeEvent) {
                activity?.finish()
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
