package com.vessel.app.ready.instructionstips.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.ready.instructionstips.InstructionsTipsViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InstructionsTipsFragment : BaseFragment<InstructionsTipsViewModel>() {
    override val viewModel: InstructionsTipsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_instructions_tips
    private val pagerAdapter = InstrucationsTabPagerAdapter()
    private lateinit var viewPager: ViewPager2
    private lateinit var tabLayout: TabLayout

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewPager = requireView().findViewById(R.id.viewPager)
        tabLayout = requireView().findViewById(R.id.tabLayout)
        setupViewPager()
        setupObservers()
    }

    private fun setupViewPager() {
        viewPager.isUserInputEnabled = false
        viewPager.adapter = pagerAdapter
        pagerAdapter.submitList(viewModel.state.tabs)
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = getString(viewModel.state.tabs[position].tabName)
        }.attach()
    }

    private fun setupObservers() {
        observe(viewModel.state.navigateTo) { findNavController().navigate(it) }
    }
}
