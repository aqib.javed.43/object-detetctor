package com.vessel.app.common.model.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class ReagentInfo(
    val infoType: ReagentInfoType,
    val id: Int?,
    val reagentId: Int?,
    val title: String?,
    val comingSoonTagItems: List<String>?,
    val description: String?,
    val imageUrl: String?
) : Parcelable

enum class ReagentInfoType {
    COMING_SOON, OVERVIEW, HOW_TO_READ, REAGENT_PAGE_OVERVIEW
}
