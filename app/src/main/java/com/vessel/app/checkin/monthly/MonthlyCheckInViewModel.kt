package com.vessel.app.checkin.monthly

import android.text.format.DateUtils
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AssessmentManager
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.GoalAssessment
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.PlanRecord
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.util.extensions.getDateBefore
import com.vessel.app.wellness.model.GoalButton
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.collections.ArrayList

class MonthlyCheckInViewModel @ViewModelInject constructor(
    val state: MonthlyCheckInState,
    @Assisted savedStateHandle: SavedStateHandle,
    private val resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    private val assessmentManager: AssessmentManager,
    private val goalsRepository: GoalsRepository,
    private val planManager: PlanManager
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    var selectedFeelings = FEELING_NOTHING
    val today: Date = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
    var goalTitle = ""

    init {
        val mainGoal = preferencesRepository.getUserMainGoal()
        goalTitle = String.format(getResString(R.string.how_was_goal), mainGoal?.name)
        getWeeklyPlans()
        loadAssessments()
    }

    fun dismiss() {
        navigateBack()
    }

    fun onSplitTypeChanged(id: Int) {
        selectedFeelings = when (id) {
            R.id.sadFaceRadioButton -> FEELING_UNHAPPY
            R.id.normalFaceRadioButton -> FEELING_MEH
            R.id.happyFaceRadioButton -> FEELING_HAPPY
            else -> FEELING_NOTHING
        }
        if (selectedFeelings == FEELING_HAPPY) {
            state.navigateTo.value = HomeNavGraphDirections.actionGlobalHappyCheckInFragment()
        } else {
            state.navigateTo.value =
                HomeNavGraphDirections.actionGlobalCheckInProblemSelectionFragment(selectedFeelings)
        }
    }

    private fun getWeeklyPlans() {
        viewModelScope.launch() {
            showLoading()
            planManager.getPlansByDateRange(
                today.getDateBefore(Constants.WEEKLY).formatDayDate(),
                today.formatDayDate()
            )
                .onSuccess { items ->
                    processPlans(items)
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun processPlans(items: List<PlanData>) {
        val plansList: ArrayList<PlanRecord> = arrayListOf()
        for (item in items) {
            item.plan_records?.let {
                plansList.addAll(it)
            }
        }
        val activityCount = items.count {
            it.program_id != null || it.goals?.firstOrNull { record ->
                record.impact ?: 0 >= 2
            } != null
        }
        val completeCount = plansList.count {
            it.completed == true
        }
        if (activityCount != 0) {
            state.progress.value = (
                (
                    BigDecimal(completeCount).divide(BigDecimal(activityCount), 2, RoundingMode.HALF_UP).setScale(2)
                        .multiply(
                            BigDecimal(100)
                        )
                    )

                ).toInt()
        }
        state.status.value = "$completeCount/$activityCount"
    }

    private fun loadAssessments() {
        viewModelScope.launch {
            showLoading()
            assessmentManager.getWellnessScores()
                .onSuccess {
                    buildHeaderData(it)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    private fun buildHeaderData(goalAssessmentList: List<GoalAssessment>) {

        val goalsButtonList = arrayListOf<GoalButton>()
        goalsRepository.getGoals()
            .map { it.key }
            .forEachIndexed { index, goal ->
                val entries = getAssessmentDataEntriesBasedOnTheGoal(
                    goalAssessmentList,
                    goal
                )

                goalsButtonList.add(
                    GoalButton(
                        id = goal.id,
                        title = goal.title,
                        chartEntries = entries,
                        averageChartEntries = listOf(DateEntry(0f, 0f, null)),
                        image = goal.image,
                        selected = false,
                        smallImageUrl = Constants.GOALS.firstOrNull { it.id == goal.id }?.image_small_url
                            ?: "",
                        selectedChartEntityPosition = entries.lastIndex
                    )
                )
            }
        Constants.GOALBUTTONS = goalsButtonList.toList()
    }

    private fun getAssessmentDataEntriesBasedOnTheGoal(
        goalAssessmentList: List<GoalAssessment>,
        goal: Goal,
    ): List<DateEntry> {
        val dataEntryList = mutableListOf<DateEntry>()
        val totalAssessments =
            goalAssessmentList.sumBy { it.assessments.filter { assessment -> assessment.goalId == goal.id }.size }
        var xAxis = maxOf(Constants.MIN_REQUIRED_X_POINTS, totalAssessments).toFloat()

        val todayAssessment =
            goalAssessmentList.firstOrNull { DateUtils.isToday(it.createdAt.time) }?.assessments?.firstOrNull { assessment -> assessment.goalId == goal.id }
        if (todayAssessment != null) {
            dataEntryList.add(
                0,
                DateEntry(
                    xAxis--,
                    todayAssessment.value.toFloat(),
                    todayAssessment.createdAt.time,
                    showNumber = false
                )
            )
        } else {
            if (totalAssessments >= Constants.MIN_REQUIRED_X_POINTS)
                xAxis++
            dataEntryList.add(
                0,
                DateEntry(
                    xAxis--,
                    Constants.RATE_IT_POINT,
                    Calendar.getInstance().timeInMillis,
                    showNumber = false
                )
            )
        }
        goalAssessmentList.filterNot { DateUtils.isToday(it.createdAt.time) }
            .sortedByDescending { it.createdAt }
            .onEach { goalAssessment ->
                goalAssessment.assessments.filter { assessment -> assessment.goalId == goal.id }
                    .forEach { assessment ->
                        dataEntryList.add(
                            0,
                            DateEntry(
                                xAxis--,
                                assessment.value.toFloat(),
                                assessment.createdAt.time,
                                showNumber = false
                            )
                        )
                    }
            }

        val unratedCount = Constants.MIN_REQUIRED_X_POINTS - dataEntryList.size - 1
        for (i in 0..unratedCount) {
            val calender = Calendar.getInstance()
            calender.timeInMillis = dataEntryList.first().date!!
            calender.add(Calendar.DATE, -1)
            dataEntryList.add(
                0,
                DateEntry(xAxis--, null, calender.timeInMillis, showNumber = false)
            )
        }
        return dataEntryList
    }

    companion object {
        const val FEELING_NOTHING = ""
        const val FEELING_UNHAPPY = "unhappy"
        const val FEELING_MEH = "meh"
        const val FEELING_HAPPY = "happy"
    }
}
