package com.vessel.app.wellnesscoreinfo.model

import com.vessel.app.wellness.model.ReagentItem

data class WellnessScore(
    var reagentItems: List<ReagentItem>? = arrayListOf(),
    var currentTestScore: Int? = 0
)
