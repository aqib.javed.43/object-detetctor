Here are the reference ranges we use for magnesium:

Low: 0-200 mg/L

Good: 200 - 600 mg/L

High: >600 mg/L


When you consume magnesium, about 33% is absorbed in the first part of your small intestine. Once absorbed into your bloodstream, magnesium goes to the tissues that need it. Approximately 20 to 28 g of magnesium is stored in the body of an adult, with about 60% stored in the skeleton, and 40% stored in our muscles and soft tissue. Only 1% of the body’s magnesium stores are distributed in extracellular fluid.

The body usually maintains a plasma magnesium concentration of 1.4 to 2.4 mg/dL. When blood levels decline, our kidneys compensate by re-absorbing filtered magnesium back into the bloodstream instead of going into our urine. When blood levels increase, the kidney’s increase the concentration of magnesium in our urine. Alcohol and certain blood pressure medications can increase loss of magnesium in the urine even when our bodies may be deficient.

When you're in the good zone (darker purple), your body is getting enough magnesium to support your wellness.

Too high means you may be getting more than you need, which is very rare when magnesium is coming from food sources alone.

Too low means your magnesium levels are too low, and you might want to up your intake. Check out the recommendations in the app for how to do that.

**References:**

-   [Nutrition in Clinical Practice: A Comprehensive, Evidence-Based Manual for the Practitioner (Nutrition in Clinical Practice), 2nd Edition by David Katz.](https://www.academia.edu/41923878/Nutrition_in_Clinical_Practice_A_Comprehensive_Evidence_Based_Manual_for_the_Practitioner_Nutrition_in_Clinical_Practice_2nd_Edition_by)

-   [https://www.ncbi.nlm.nih.gov/pubmed/28965563](https://www.ncbi.nlm.nih.gov/pubmed/28965563)

-   [https://www.sciencedirect.com/science/article/pii/S0022347697701628](https://www.sciencedirect.com/science/article/pii/S0022347697701628)

-   [https://adc.bmj.com/content/archdischild/49/2/97.full.pdf](https://www.sciencedirect.com/science/article/pii/S0022347697701628)
