package com.vessel.app.common.widget

import android.content.Context
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.vessel.app.R
import com.vessel.app.databinding.WidgetPercentProgressViewBinding

class WeeklyPercentWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {
    private lateinit var binding: WidgetPercentProgressViewBinding

    init {
        initView(context)
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetPercentProgressViewBinding.inflate(layoutInflater, this, true)
    }

    fun setProgress(progress: Int) {
        binding.lblPercent.text = "$progress%"
        createSteps(progress)
        android.os.Handler(Looper.getMainLooper()).post {
            binding.progress.setProgress(progress, true)
        }
    }

    private fun createSteps(progress: Int) {
        when (progress) {
            in 50..74 -> binding.sign50Iv.setBackgroundResource(R.drawable.ic_active)
            in 75..99 -> {
                binding.sign50Iv.setBackgroundResource(R.drawable.ic_active)
                binding.sign75Iv.setBackgroundResource(R.drawable.ic_active)
            }
            100 -> {
                binding.sign50Iv.setBackgroundResource(R.drawable.ic_active)
                binding.sign75Iv.setBackgroundResource(R.drawable.ic_active)
                binding.sign100Iv.setBackgroundResource(R.drawable.ic_active)
            }
        }
    }
}
