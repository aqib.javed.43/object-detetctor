package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.*
import com.vessel.app.common.net.data.CompleteProgramPayload
import com.vessel.app.common.net.data.ProgramPayLoadNew
import com.vessel.app.common.net.data.ProgramPayload
import com.vessel.app.common.net.mapper.ProgramMapper
import com.vessel.app.common.net.mapper.ProgramScheduleMapper
import com.vessel.app.common.net.service.ProgramService
import com.vessel.app.wellness.net.data.ProgramAction
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProgramRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val programMapper: ProgramMapper,
    private val programService: ProgramService,
    private val programScheduleMapper: ProgramScheduleMapper
) : BaseNetworkRepository(moshi, loggingManager) {

    private var enrolledProgramSchedule: List<ScheduleModel>? = null

    suspend fun getProgramsByGoal(goalId: Int, perPage: Int?, pageNumber: Int?) =
        executeWithErrorMessage(programMapper::map) {
            programService.getProgramsByGoal(
                goalId, perPage, pageNumber
            )
        }

    suspend fun getPrograms() = executeWithErrorMessage(programMapper::map) {
        programService.getPrograms()
    }

    suspend fun joinProgram(program: Program, action: ProgramAction) =
        executeWithErrorMessage {
            val programPayload = ProgramPayload(action.value)
            programService.joinProgram(program.id, programPayload)
        }

    suspend fun joinProgramString(program: Int, action: ProgramPayLoadNew) =
        executeWithErrorMessage {
            programService.joinProgramString(program, action)
        }

    suspend fun getProgramDetails(programId: Int) =
        executeWithErrorMessage(programMapper::mapProgram) {
            programService.getProgramDetails(programId)
        }

    suspend fun getProgramSchedule(forceUpdate: Boolean = false): Result<List<ScheduleModel>, ErrorMessage> {
        return if (forceUpdate || enrolledProgramSchedule == null) {
            executeWithErrorMessage(programScheduleMapper::mapSchedule) {
                programService.getProgramSchedule()
            }
                .onSuccess {
                    enrolledProgramSchedule = it
                }
        } else {
            Result.Success(enrolledProgramSchedule!!)
        }
    }

    suspend fun completeProgram(program: Program, action: ProgramAction, notes: String? = null) =
        executeWithErrorMessage {
            val programPayload = CompleteProgramPayload(action.value, notes)
            programService.completeProgram(program.id, programPayload)
        }
    suspend fun fetchProgramSummary(programId: Int) =
        executeWithErrorMessage {
            programService.getProgramSummary(programId)
        }
    fun clear() {
        enrolledProgramSchedule = null
    }
}
