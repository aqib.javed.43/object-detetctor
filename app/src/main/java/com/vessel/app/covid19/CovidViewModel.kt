package com.vessel.app.covid19

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.covid19.model.CovidHeader
import com.vessel.app.covid19.model.CovidTest
import com.vessel.app.covid19.model.CovidTest.Status.*
import com.vessel.app.covid19.model.SectionHeader
import com.vessel.app.covid19.repo.FAQRepository
import com.vessel.app.covid19.ui.CovidAdapter
import com.vessel.app.covid19.ui.CovidFragmentDirections
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class CovidViewModel @ViewModelInject constructor (
    val state: CovidState,
    resourceProvider: ResourceRepository,
    private val covidTestManager: CovidTestManager,
    private val faqRepository: FAQRepository
) : BaseViewModel(resourceProvider),
    CovidAdapter.CovidHeaderHandler,
    CovidAdapter.CovidTestHandler {
    companion object {
        private val MY_TESTS_HEADER = SectionHeader(R.string.my_tests)
        private val QUESTIONS_HEADER = SectionHeader(R.string.questions)

        private const val TESTS_REFRESH_DELAY_IN_SECONDS = 10L
    }

    private var updateTestsJob: Job? = null

    init {
        state.items.value = mutableListOf(CovidHeader, QUESTIONS_HEADER)
            .apply { addAll(faqRepository.faqs) }
        loadCovidTests()
    }

    private fun loadCovidTests() {
        viewModelScope.launch {
            covidTestManager.getCovidTests()
                .collect { covidTests ->
                    if (covidTests.isNotEmpty()) {
                        state.items.postValue(
                            mutableListOf<Any>(CovidHeader).apply {
                                add(MY_TESTS_HEADER)
                                addAll(covidTests)
                                add(QUESTIONS_HEADER)
                                addAll(faqRepository.faqs)
                            }
                        )
                    }
                }
        }
    }

    override fun onTakeTestClicked(view: View) {
        view.findNavController()
            .navigate(CovidFragmentDirections.actionCovidToTakeTest())
    }

    override fun onBuyTestClicked(view: View) {
        view.findNavController()
            .navigate(CovidFragmentDirections.actionCovidToWeb(BuildConfig.BUY_TEST_URL))
    }

    override fun onCovidTestButtonClicked(view: View, item: CovidTest) {
        when (item.status) {
            CONSULTATION_REQUIRED, UNREADABLE_PHOTO -> view.findNavController()
                .navigate(CovidFragmentDirections.actionCovidToWeb(BuildConfig.DOXY_ME_URL))
            NO_CONSULTATION_REQUIRED -> view.findNavController()
                .navigate(CovidFragmentDirections.actionCovidToGift())
            PENDING_DOCTOR_EVALUATION -> {
                // intentionally do nothing.
            }
        }
    }

    fun onResume() {
        updateTestsJob = viewModelScope.launch {
            while (true) {
                delay(TimeUnit.SECONDS.toMillis(TESTS_REFRESH_DELAY_IN_SECONDS))
                covidTestManager.getAndCacheTests()
            }
        }
    }

    fun onPause() {
        updateTestsJob?.cancel()
    }
}
