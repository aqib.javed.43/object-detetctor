package com.vessel.app.newtesttimer.glaretab

import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GlareTabFragment : BaseFragment<GlareTabViewModel>() {
    override val viewModel: GlareTabViewModel by viewModels()
    override val layoutResId = R.layout.fragment_glare_tab
}
