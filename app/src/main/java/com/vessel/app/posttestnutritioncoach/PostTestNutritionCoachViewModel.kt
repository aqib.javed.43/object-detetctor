package com.vessel.app.posttestnutritioncoach

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.posttestnutritioncoach.ui.PostTestNutritionCoachFragmentDirections
import com.vessel.app.util.ResourceRepository

class PostTestNutritionCoachViewModel @ViewModelInject constructor(
    val state: PostTestNutritionCoachState,
    val resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    fun onMakeAnAppointmentClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.BOOK_NUTRITIONIST_TAPPED)
                .setScreenName(TrackingConstants.FROM_TEST_POST_FLOW)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        view.findNavController().navigate(PostTestNavGraphDirections.globalActionToNutritionOptionsFragment())
    }

    fun onLaterClicked(view: View) {
        view.findNavController().navigate(PostTestNutritionCoachFragmentDirections.actionNutritionCoachFragmentToLastStepFragment())
    }
}
