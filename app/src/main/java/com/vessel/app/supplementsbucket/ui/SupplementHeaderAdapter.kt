package com.vessel.app.supplementsbucket.ui

import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.supplementsbucket.model.SupplementHeader

class SupplementHeaderAdapter(
    private val handler: OnBucketCheckoutActionHandler
) : BaseAdapterWithDiffUtil<SupplementHeader>() {
    override fun getLayout(position: Int) = R.layout.item_supplement_bucket_header

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<SupplementHeader>, position: Int) {
        val supplements = holder.itemView.findViewById<RecyclerView>(R.id.supplements)
        if (supplements.adapter == null) {
            supplements.adapter =
                SupplementBucketAdapter(handler as SupplementBucketAdapter.OnActionHandler)
        }
        (supplements.adapter as SupplementBucketAdapter).submitList(getItem(position).supplements)
        super.onBindViewHolder(holder, position)
    }
}
