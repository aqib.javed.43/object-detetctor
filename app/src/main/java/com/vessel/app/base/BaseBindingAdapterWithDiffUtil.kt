package com.vessel.app.base

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class BaseBindingAdapterWithDiffUtil<T>(
    diffCallback: DiffUtil.ItemCallback<T> = object : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem == newItem

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: T, newItem: T) = oldItem == newItem

        override fun getChangePayload(oldItem: T, newItem: T) = false
    }
) :
    ListAdapter<T, BaseBindingAdapterWithDiffUtil.BaseViewHolder<T>>(diffCallback) {
    private lateinit var layoutInflater: LayoutInflater
    protected abstract fun getLayout(position: Int): Int

    constructor(
        areItemsTheSame: (oldItem: T, newItem: T) -> Boolean =
            { oldItem, newItem -> oldItem == newItem },
        areContentsTheSame: (oldItem: T, newItem: T) -> Boolean =
            { oldItem, newItem -> oldItem == newItem },
        getChangePayload: (oldItem: T, newItem: T) -> Boolean = { _, _ -> false }
    ) : this(
        object : DiffUtil.ItemCallback<T>() {
            override fun areItemsTheSame(oldItem: T, newItem: T) = areItemsTheSame(oldItem, newItem)

            override fun areContentsTheSame(oldItem: T, newItem: T) =
                areContentsTheSame(oldItem, newItem)

            override fun getChangePayload(oldItem: T, newItem: T) =
                getChangePayload(oldItem, newItem)
        }
    )

    open fun getHandler(position: Int): Any? = null

    open fun getViewHolder(binding: ViewDataBinding): BaseViewHolder<T> = BaseViewHolder(binding)

    override fun getItemViewType(position: Int) = getLayout(position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T> {
        if (!::layoutInflater.isInitialized) layoutInflater = LayoutInflater.from(parent.context)
        return getViewHolder(
            DataBindingUtil.inflate(layoutInflater, viewType, parent, false)
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(getItem(position), getHandler(position), holder.binding, position)
    }

    open class BaseViewHolder<T>(val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        open fun bind(item: T, handler: Any?, binding: ViewDataBinding, position: Int) {
            binding.setVariable(BR.item, item)
            if (handler != null) binding.setVariable(BR.handler, handler)
            binding.executePendingBindings()
        }
    }
}
