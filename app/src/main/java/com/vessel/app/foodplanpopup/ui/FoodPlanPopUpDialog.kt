package com.vessel.app.foodplanpopup.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Handler
import android.os.Looper
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.foodplanpopup.FoodPlanPopUpViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FoodPlanPopUpDialog : BaseDialogFragment<FoodPlanPopUpViewModel>() {
    override val viewModel: FoodPlanPopUpViewModel by viewModels()
    override val layoutResId = R.layout.alert_food_plan_action_pop_up_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }

        isCancelable = false

        Handler(Looper.getMainLooper()).postDelayed(
            {
                if (dialog?.isShowing == true) {
                    dialog?.dismiss()
                }
            },
            TIME_TO_POP_UP
        )
    }

    companion object {
        private const val TIME_TO_POP_UP = 3000L
    }
}
