package com.vessel.app.onboarding.pickothergoal

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_goal.*
import kotlinx.android.synthetic.main.fragment_on_boarding_rate_goal_selection.*

@AndroidEntryPoint
class PickAnotherGoalFragment : BaseFragment<PickAnotherGoalViewModel>() {
    override val viewModel: PickAnotherGoalViewModel by viewModels()
    override val layoutResId = R.layout.fragment_on_boarding_pick_other_goal

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
        }
    }
}
