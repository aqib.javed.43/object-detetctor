package com.vessel.app.teampage

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.binding.setSrcResource
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home_team_page.*

@AndroidEntryPoint
class HomeTeamFragment : BaseFragment<HomeTeamViewModel>() {
    override val viewModel: HomeTeamViewModel by viewModels()
    override val layoutResId: Int = R.layout.fragment_home_team_page
    private val teamPostAdapter: TeamPostAdapter by lazy { TeamPostAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        setupList()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(goal) {
                updateHeader(it)
            }
            observe(feeds) {
                Log.d("TAG", "setupObservers: $it")
                teamPostAdapter.apply {
                    submitList(it)
                }
            }
        }
        viewModel.getTeamData()
        viewModel.fetchAllFeeds()
    }

    private fun updateHeader(goal: GoalRecord?) {
        goal?.let {
            lblTeamName.text = String.format(getString(R.string.team_title), it.name)
            headerBg.setSrcResource(
                Constants.GOALBUTTONS.firstOrNull { button ->
                    it.id == button.id
                }?.image
            )
            lblDescription.text = String.format(getString(R.string.team_description), it.name)
        }
    }

    private fun setupList() {
        rcvPosts.adapter = ConcatAdapter(
            ConcatAdapter.Config.Builder()
                .setIsolateViewTypes(false)
                .build(),
            teamPostAdapter
        )
    }
}
