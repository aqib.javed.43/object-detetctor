package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import com.vessel.app.R
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.wellness.RecommendationType
import com.vessel.app.wellness.model.IRecommendation
import com.vessel.app.wellness.model.RecommendedNutrient
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.text.NumberFormat

@JsonClass(generateAdapter = true)
@Parcelize
data class Food(
    val allergy_crustacean: Boolean? = null,
    val allergy_eggs: Boolean? = null,
    val allergy_fish: Boolean? = null,
    val allergy_gluten: Boolean? = null,
    val allergy_milk: Boolean? = null,
    val allergy_peanuts: Boolean? = null,
    val allergy_seeds: Boolean? = null,
    val allergy_soybeans: Boolean? = null,
    val allergy_treeNuts: Boolean? = null,
    val allergy_wheat: Boolean? = null,
    val diet_keto: Boolean? = null,
    val diet_lowCalorie: Boolean? = null,
    val diet_lowCarb: Boolean? = null,
    val diet_lowFat: Boolean? = null,
    val diet_lowSugar: Boolean? = null,
    val diet_paleo: Boolean? = null,
    val diet_vegan: Boolean? = null,
    val diet_vegetarian: Boolean? = null,
    val food_categories: List<String>? = null,
    val food_title: String? = null,
    val id: Int? = null,
    val image_url: String? = null,
    val nutrient_Magnesium: Double? = null,
    val nutrient_Tryptophan: Double? = null,
    val nutrient_VitaminB12: Double? = null,
    val nutrient_VitaminB7: Double? = null,
    val nutrient_VitaminB9: Double? = null,
    val nutrient_Calcium: Double? = null,
    val nutrient_VitaminC: Double? = null,
    val popularity: Int? = null,
    val serving_grams: Float? = null,
    val serving_quantity: Double? = null,
    val serving_unit: String? = null,
    val usda_ndb_number: String? = null,
    var total_likes: Int? = null,
    var dislikes_count: Int? = null,
    val like_status: String? = null,
    val frequency: String? = null,
    val description: String? = null,
    val isAddedToPlan: Boolean = false,
    val goals: List<GoalRecord>? = null,
    val nutrients: List<RecommendedNutrient>? = null,
) : Parcelable, IRecommendation {

    @IgnoredOnParcel
    var likeStatus: LikeStatus = LikeStatus.from(like_status)

    override fun getBackgroundImageUrl() = image_url

    override fun getDisplayedTitle() = food_title

    override fun getDisplayedFrequency() =
        "${NumberFormat.getInstance().format(serving_quantity ?: 0.toDouble())} $serving_unit"

    override fun getDisplayedDescription() = getFormattedDescription()

    override fun hasSimilar() = false

    override fun getLikesTotalCount() = total_likes?.toString()

    override fun getLikeBackground() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.green_background_with_16_radius
    else
        R.drawable.gray_background_with_16_radius

    override fun getButtonResId() = if (isAddedToPlan)
        R.string.remove_from_plan
    else
        R.string.add_to_plan

    private fun getFormattedDescription(): String {
        val result = StringBuilder()
        if (nutrients.isNullOrEmpty().not()) {
            result.append("Improves: ${nutrients?.filter { it.name == "vitaminB7" || it.name == "Magnesium" || it.name == "VitaminC" }?.joinToString { it.name.toString() }} \n")
        }
        if (goals.isNullOrEmpty().not()) {
            result.append("Goals: ${goals?.joinToString { it.name }}")
        }
        return result.toString()
    }

    fun getFoodImproves(): String =
        mutableListOf<String>().apply {
            if (nutrient_VitaminB7 != null && nutrient_VitaminB7 > 0) {
                add("vitamin B7")
            }
//            if (food?.nutrient_VitaminB9 != null && nutrient_VitaminB9 > 0) {
//                add("vitamin B9")
//            }
            if (nutrient_Magnesium != null && nutrient_Magnesium > 0) {
                add("Magnesium")
            }
            if (nutrient_VitaminC != null && nutrient_VitaminC > 0) {
                add("Vitamin C")
            }
        }.joinToString()

    override fun getType(): RecommendationType = RecommendationType.Food

    override fun getAddedToPlan() = isAddedToPlan
    override fun getThumbShapeIcon() = if (likeStatus == LikeStatus.LIKE)
        R.drawable.ic_filled_thumbs_up_green
    else
        R.drawable.ic_outline_thumb_up

    override fun getLikedStatus(): LikeStatus {
        return likeStatus
    }

    override fun getLikesCount(): Int {
        return total_likes ?: 0
    }

    override fun getDislikesCount(): Int {
        return dislikes_count ?: 0
    }

    override fun getDislikesTotalCount(): String? {
        return dislikes_count.toString()
    }

    override fun setLikesCount(count: Int) {
        total_likes = count
    }

    override fun setLikedStatus(status: LikeStatus) {
        likeStatus = status
    }
}
