package com.vessel.app.plan.ui

import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.plan.model.DayPlanItem
import com.vessel.app.util.extensions.isSameDay
import java.util.*

class DayPlanAdapter(private val handler: PlanAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<DayPlanItem>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is DayPlanItem -> R.layout.item_day_plan
        else -> throw IllegalStateException(
            "Unexpected WellnessAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }
    override fun onBindViewHolder(holder: BaseViewHolder<DayPlanItem>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as DayPlanItem
        val planAdapter = PlanAdapter(handler, position, item.date.isSameDay(Calendar.getInstance().time))
        holder.itemView.findViewById<RecyclerView>(R.id.planList).apply {
            adapter = planAdapter
            itemAnimator = null
            setHasFixedSize(true)
        }

        planAdapter.submitList(item.items)
    }
}
