package com.vessel.app.common.di

import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationManagerCompat
import com.amplitude.api.Amplitude
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.vessel.app.BuildConfig
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.FirebaseRemoteConfiguration
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import com.zendesk.logger.Logger
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import zendesk.chat.Chat
import zendesk.core.Zendesk
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DomainModule {
    @Singleton
    @Provides
    fun firebaseAnalytics(@ApplicationContext context: Context) =
        FirebaseAnalytics.getInstance(context)

    @Singleton
    @Provides
    fun amplitudeInstance(@ApplicationContext context: Context) =
        Amplitude.getInstance().initialize(context, BuildConfig.AMPLITUDE_API_KEY)

    @Singleton
    @Provides
    fun firebaseCrashlytics() = FirebaseCrashlytics.getInstance()

    @Singleton
    @Provides
    fun ConnectionChangeReceiver() = com.vessel.app.common.net.ConnectionChangeReceiver()

    @Singleton
    @Provides
    fun firebaseConfig() = FirebaseRemoteConfig.getInstance()

    @Singleton
    @Provides
    fun firebaseRemoteConfiguration(
        resourceRepository: ResourceRepository,
        config: FirebaseRemoteConfig
    ): RemoteConfiguration =
        FirebaseRemoteConfiguration(resourceRepository, config).apply {
            init()
        }

    @Singleton
    @Provides
    fun notificationManager(@ApplicationContext context: Context) =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    @Singleton
    @Provides
    fun notificationManagerCompat(@ApplicationContext context: Context) =
        NotificationManagerCompat.from(context)

    @Singleton
    @Provides
    fun zendesk(@ApplicationContext context: Context): Chat = Chat.INSTANCE.apply {
        init(context, BuildConfig.ZENDESK_KEY, "309523279005003777")
        Logger.setLoggable(BuildConfig.DEBUG)
    }

    @Singleton
    @Provides
    fun zendeskInstance(@ApplicationContext context: Context): Zendesk = Zendesk.INSTANCE.apply {
        init(
            context, BuildConfig.ZENDESK_URL,
            BuildConfig.ZENDESK_APP_ID,
            BuildConfig.ZENDESK_CLIENT_ID
        )
    }
}
