package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.net.mapper.GoalAssessmentMapper
import com.vessel.app.common.net.mapper.TagMapper
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.net.payload.GoalAssessmentPayload
import com.vessel.app.common.net.service.AssessmentService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AssessmentRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val assessmentService: AssessmentService,
    private val goalAssessmentMapper: GoalAssessmentMapper,
    private val tagMapper: TagMapper,
    private val tipMapper: TipMapper
) : BaseNetworkRepository(moshi, loggingManager) {

    suspend fun getGoalAssessments(startDate: String?, endDate: String?) =
        executeWithErrorMessage(goalAssessmentMapper::map) {
            assessmentService.getGoalAssessments(
                startDate,
                endDate
            )
        }

    suspend fun getTags(goalId: Int, rate: String?) =
        executeWithErrorMessage(tagMapper::map) {
            assessmentService.getTags(goalId, rate)
        }

    suspend fun putGoalAssessments(goalId: Int, value: Int, createdDate: String) =
        executeWithErrorMessage(goalAssessmentMapper::mapAssessments) {
            assessmentService.putGoalAssessments(
                GoalAssessmentPayload(goalId, value, createdDate)
            )
        }

    suspend fun getTips(
        goalId: Int?,
        tagIds: List<Int>?,
        level: String?,
        pageNumber: Int?,
        pageSize: Int?,
        sortingByLikes: Boolean?,
        sortingByNewest: Boolean?,
    ) =
        executeWithErrorMessage(tipMapper::map) {
            val byLikes = if (sortingByLikes == false) null else true // send it only if true
            val byNewest = if (sortingByNewest == false) null else true // send it only if true
            assessmentService.getTips(
                goalId,
                pageNumber,
                pageSize,
                byLikes,
                byNewest,
                level,
                tagIds
            )
        }
    suspend fun getTipsWithHabits(
        goalId: Int?,
        tagIds: List<Int>?,
        level: String?,
        pageNumber: Int?,
        pageSize: Int?,
        sortingByLikes: Boolean?,
        sortingByNewest: Boolean?,
        habitIds: List<Int>?,
    ) =
        executeWithErrorMessage(tipMapper::map) {
            val byLikes = if (sortingByLikes == false) null else true // send it only if true
            val byNewest = if (sortingByNewest == false) null else true // send it only if true
            assessmentService.getTipsWithHabits(
                goalId,
                pageNumber,
                pageSize,
                byLikes,
                byNewest,
                level,
                tagIds,
                habitIds
            )
        }
}
