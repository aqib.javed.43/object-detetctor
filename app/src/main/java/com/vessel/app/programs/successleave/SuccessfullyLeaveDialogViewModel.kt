package com.vessel.app.programs.successleave

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class SuccessfullyLeaveDialogViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()
    val notice = String.format(getResString(R.string.left_program_is_done), savedStateHandle.get<String>("programTitle"))

    fun onCloseClicked() {
        dismissDialog.call()
    }
}
