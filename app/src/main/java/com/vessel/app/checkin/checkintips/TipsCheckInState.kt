package com.vessel.app.checkin.checkintips

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.model.RecommendationItem
import javax.inject.Inject

class TipsCheckInState @Inject constructor() {
    val recommendations = MutableLiveData<List<RecommendationItem>>()
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: TipsCheckInState.() -> Unit) = apply(block)
}
