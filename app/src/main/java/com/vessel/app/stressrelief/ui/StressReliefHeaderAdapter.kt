package com.vessel.app.stressrelief.ui

import android.view.View
import androidx.core.view.isVisible
import com.google.android.material.tabs.TabLayout
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.stressrelief.model.StressReliefHeaderViewItem
import com.vessel.app.views.BlackAppButtonViewGroup

class StressReliefHeaderAdapter(
    private val handler: OnActionHandler
) : BaseAdapterWithDiffUtil<StressReliefHeaderViewItem>(
    areItemsTheSame = { _, _ -> true },
    areContentsTheSame = { item1, item2 ->
        item1.showLoadingCortisolLevel == item2.showLoadingCortisolLevel &&
            item1.buttonText == item2.buttonText &&
            item1.isDailySelected == item2.isDailySelected &&
            item1.showTabs == item2.showTabs
    }
) {

    override fun getLayout(position: Int) = R.layout.item_stress_relief_header

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(
        holder: BaseViewHolder<StressReliefHeaderViewItem>,
        position: Int
    ) {
        val item = getItem(position)
        holder.itemView.findViewById<TabLayout>(R.id.filterTabLayout).apply {
            isVisible = item.showTabs
            clearOnTabSelectedListeners()
            removeAllTabs()
            addTab(this.newTab().setText(context.getString(R.string.daily_plan)))
            addTab(this.newTab().setText(context.getString(R.string.weekly_plan)))

            if (item.isDailySelected) {
                getTabAt(0)?.select()
            } else {
                getTabAt(1)?.select()
            }

            addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    if (tab.position == DAILY_TAB_POSITION) {
                        this@StressReliefHeaderAdapter.handler.onDailySelected()
                    } else if (tab.position == WEEKLY_TAB_POSITION) {
                        this@StressReliefHeaderAdapter.handler.onWeeklySelected()
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {
                }

                override fun onTabReselected(tab: TabLayout.Tab) {
                }
            })
        }

        holder.itemView.findViewById<BlackAppButtonViewGroup>(R.id.seeCortisolLevelsButton).apply {
            setText(item?.buttonText.orEmpty())
        }
        holder.itemView.findViewById<View>(R.id.addPlanButton).setOnClickListener {
            this@StressReliefHeaderAdapter.handler.onAddPlanClicked()
        }
        holder.itemView.findViewById<View>(R.id.seeCortisolLevelsButton).setOnClickListener {
            this@StressReliefHeaderAdapter.handler.onSeeCortisolLevelsClicked()
        }
    }

    interface OnActionHandler {
        fun onSeeCortisolLevelsClicked()
        fun onAddPlanClicked()
        fun onDailySelected()
        fun onWeeklySelected()
    }

    companion object {
        const val DAILY_TAB_POSITION = 0
        const val WEEKLY_TAB_POSITION = 1
    }
}
