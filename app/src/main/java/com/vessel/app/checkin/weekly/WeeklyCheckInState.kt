package com.vessel.app.checkin.weekly

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class WeeklyCheckInState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val progress = LiveEvent<Int>()
    val status = LiveEvent<String>()
    operator fun invoke(block: WeeklyCheckInState.() -> Unit) = apply(block)
}
