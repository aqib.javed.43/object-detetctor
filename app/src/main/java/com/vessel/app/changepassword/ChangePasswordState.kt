package com.vessel.app.changepassword

import androidx.lifecycle.MutableLiveData
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ChangePasswordState @Inject constructor() {
    val currentPassword = MutableLiveData("")
    val newPassword = MutableLiveData("")
    val confirmNewPassword = MutableLiveData("")
    val showSuccessToast = LiveEvent<Unit>()

    operator fun invoke(block: ChangePasswordState.() -> Unit) = apply(block)
}
