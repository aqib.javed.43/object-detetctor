package com.vessel.app.common.manager

import com.vessel.app.common.model.*
import com.vessel.app.common.repo.LessonRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LessonManager @Inject constructor(
    private val lessonRepository: LessonRepository
) {
    private var lessonResponse: LessonResponse? = null

    suspend fun fetchAllLessons(
        isToday: Boolean? = true,
        contactId: Int,
        programId: Int,
        fetchAll: Boolean? = true,
        showAll: Boolean? = true,
        date: String? = null
    ): Result<LessonResponse, ErrorMessage> {
        return if (lessonResponse == null) {
            lessonRepository.fetchAllLessons(isToday, contactId, programId, fetchAll, showAll, date).onSuccess {
                Result.Success(it)
            }
        } else {
            Result.Success(lessonResponse!!)
        }
    }
    suspend fun postAnswer(
        answer: CompleteAnswer
    ): Result<Any, ErrorMessage> {
        return lessonRepository.postAnswer(answer)
    }
}
