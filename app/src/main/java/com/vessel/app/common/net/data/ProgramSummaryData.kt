package com.vessel.app.common.net.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProgramSummaryData(
    @Json(name = "previous_week")
    val previousWeek: List<PreviousWeek>? = null,
    @Json(name = "week_streak")
    val weekStreak: Long? = 0,
    @Json(name = "active_days")
    val activeDays: Long? = 0
)

@JsonClass(generateAdapter = true)
data class PreviousWeek(
    val date: String? = null,
    val complete: Boolean? = null,
    val day: Long? = null
)
