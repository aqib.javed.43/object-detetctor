package com.vessel.app.takesurvey.ui

import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.takesurvey.LayoutType
import com.vessel.app.takesurvey.SurveyQuestion
import com.vessel.app.takesurvey.model.SurveyAnswer
import com.vessel.app.util.extensions.hideKeyboard

class QuestionRecyclerViewAdapter(val handler: QuestionClickHandler) :
    BaseAdapterWithDiffUtil<SurveyQuestion>() {

    override fun getLayout(position: Int) = when (getItem(position).layoutType) {
        LayoutType.LAYOUT1 -> R.layout.item_question_layout1
        LayoutType.LAYOUT2 -> R.layout.item_question_layout2
        LayoutType.LAYOUT3 -> R.layout.item_question_layout3
        LayoutType.LAYOUT4 -> R.layout.item_question_layout4
    }

    override fun onBindViewHolder(holder: BaseViewHolder<SurveyQuestion>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position)
        when (item.layoutType) {
            LayoutType.LAYOUT1 -> bindLayout1(holder, item)
            LayoutType.LAYOUT2 -> bindLayout2(holder, item)
            LayoutType.LAYOUT3 -> bindLayout3(holder, item)
            LayoutType.LAYOUT4 -> bindLayout4(holder, item)
        }
    }

    private fun bindLayout1(
        holder: BaseViewHolder<SurveyQuestion>,
        item: SurveyQuestion
    ) {
        val answers = holder.itemView.findViewById<RecyclerView>(R.id.answers)
        val answersRecyclerViewAdapter =
            AnswersRecyclerViewAdapter(handler, R.layout.item_survey_answer_layout1)
        answers.layoutManager = GridLayoutManager(holder.itemView.context, 2)
        answers.adapter = answersRecyclerViewAdapter
        answersRecyclerViewAdapter.submitList(item.answers)
    }

    private fun bindLayout2(
        holder: BaseViewHolder<SurveyQuestion>,
        item: SurveyQuestion
    ) {
        item.answers?.let {
            val answers = holder.itemView.findViewById<RecyclerView>(R.id.answers)
            val answersRecyclerViewAdapter =
                AnswersRecyclerViewAdapter(handler, R.layout.item_survey_answer_layout2)
            val layoutManager = GridLayoutManager(holder.itemView.context, 2)
            layoutManager.spanSizeLookup = object : SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (it.lastIndex == position) 2 else 1
                }
            }
            answers.layoutManager = layoutManager
            answers.adapter = answersRecyclerViewAdapter
            answersRecyclerViewAdapter.submitList(it)
        }
    }

    private fun bindLayout3(
        holder: BaseViewHolder<SurveyQuestion>,
        item: SurveyQuestion
    ) {
        val answers = holder.itemView.findViewById<RecyclerView>(R.id.answers)
        val answersRecyclerViewAdapter =
            AnswersRecyclerViewAdapter(handler, R.layout.item_survey_answer_layout3)
        answers.layoutManager = LinearLayoutManager(holder.itemView.context)
        answers.adapter = answersRecyclerViewAdapter
        answersRecyclerViewAdapter.submitList(item.answers)
    }

    private fun bindLayout4(
        holder: BaseViewHolder<SurveyQuestion>,
        item: SurveyQuestion
    ) {
        val answer = holder.itemView.findViewById<EditText>(R.id.answer)
        answer.addTextChangedListener {
            handler.answerChangedListenerForLayout4(item, it.toString())
        }
        holder.itemView.setOnTouchListener { view, _ ->
            view.hideKeyboard()
            true
        }
    }

    override fun getHandler(position: Int) = handler

    interface QuestionClickHandler {
        fun onSkipClicked(question: SurveyQuestion)
        fun onAnswerClicked(answer: SurveyAnswer)
        fun answerChangedListenerForLayout4(item: SurveyQuestion, answer: String)
    }
}
