package com.vessel.app.profile.managedietandallergies.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FoodPreferencesSelect(
    @StringRes val title: Int,
    val checked: Boolean,
    val type: FoodPreferencesTab
) : Parcelable {

    @IgnoredOnParcel
    @DrawableRes
    val background =
        if (checked) {
            R.drawable.white_transparent70_background
        } else {
            R.drawable.rounded_white_alpha
        }
}
