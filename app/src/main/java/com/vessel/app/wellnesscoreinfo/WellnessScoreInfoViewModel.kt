package com.vessel.app.wellnesscoreinfo

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.BuildPlanManager
import com.vessel.app.common.manager.ReagentsManager
import com.vessel.app.common.manager.ScoreManager
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.testscore.TestScoreViewModel
import com.vessel.app.tooltipscore.ui.ReagentScoreAdapter
import com.vessel.app.tooltipscore.ui.ToolTipScoreFragmentDirections
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.model.ReagentItem

class WellnessScoreInfoViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    val state: WellnessScoreInfoState,
    resourceProvider: ResourceRepository,
    private val scoreManager: ScoreManager,
    val preferencesRepository: PreferencesRepository,
    private val reagentsManager: ReagentsManager,
    val homeTabsManager: HomeTabsManager,
    val buildPlanManager: BuildPlanManager
) : BaseViewModel(resourceProvider), ToolbarHandler, ReagentScoreAdapter.OnActionHandler {
    private val scoreInfo = Constants.wellnessScore
    init {
        loadScores()
    }

    private fun loadScores() {
        state {
            scoreInfo?.let { score ->
                val currentTestScore = score.currentTestScore
                scoreItems.value = currentTestScore.toString()
                reagentItems.value = score.reagentItems
                when (currentTestScore) {
                    in 0..25 -> TestScoreViewModel.ScoreLevel.Poor
                    in 26..50 -> TestScoreViewModel.ScoreLevel.Fair
                    in 51..75 -> TestScoreViewModel.ScoreLevel.Good
                    in 76..100 -> TestScoreViewModel.ScoreLevel.Great
                    else -> null
                }.also {
                    scoreLevel.value = it?.level
                    scoreLevelColor.value =
                        it?.color ?: TestScoreViewModel.ScoreLevel.Poor.color
                }
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onReagentSelected(item: ReagentItem) {
        state.navigateTo.value =
            ToolTipScoreFragmentDirections.actionToolTipScoreFragmentToReagentGraphFragment(
                com.vessel.app.reagent.model.ReagentHeader(
                    id = item.id,
                    title = item.title,
                    unit = item.unit,
                    chartEntries = item.chartEntries.map {
                        DateEntry(
                            it.x,
                            it.y,
                            it.date,
                            it.reagentsCount
                        )
                    },
                    lowerBound = item.lowerBound,
                    upperBound = item.upperBound,
                    minY = item.minY,
                    maxY = item.maxY,
                    background = reagentsManager.fromId(item.id)!!.headerImage,
                    ranges = item.ranges,
                    errorDescription = if (item.id == com.vessel.app.common.model.data.ReagentItem.B9.id) getResString(
                        R.string.b9_not_available_message
                    ) else getResString(
                        R.string.reagent_test_results_unavailable,
                        item.title
                    ),
                    isBetaReagent = item.isBetaTesting,
                    betaHint = getResString(
                        R.string.reagent_beta_test_hint,
                        item.title
                    ),
                    consumptionUnit = item.consumptionUnit
                )
            )
    }
}
