package com.vessel.app.views

import android.R
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.os.Build
import android.util.AttributeSet
import android.widget.LinearLayout

/**
 * Displays and animates a line graph painted on the background of the view.
 *
 * Created by kylewbanks on 2017-10-07.
 */
class AnimatedLineGraphView : LinearLayout {
    /**
     * Returns the animation duration, in milliseconds.
     *
     * @return
     */
    /**
     * Sets the animation duration, in milliseconds.
     *
     * @param animationDuration
     */
    var animationDuration = 0
    /**
     * Returns the radius of the circle.
     *
     * @return
     */
    /**
     * Sets the radius of the circle to draw.
     *
     * @param circleRadius
     */
    var circleRadius = 0
    /**
     * Returns the inner-padding as a percentage of the view size.
     *
     * @return
     */
    /**
     * Sets the inner-padding as a percentage of the view size.
     *
     * @param paddingPercent
     */
    var paddingPercent = 0f
    private var data: ArrayList<Float> = arrayListOf()
    private var linePaint: Paint? = null
    private var circlePaint: Paint? = null
    private var path: Path? = null
    private var pathPoints: ArrayList<ArrayList<Float>> = arrayListOf()
    private var width = 0f
    private var height = 0f
    private var animationStartTimeMs: Long = 0
    private var tickCount = 0

    constructor(context: Context?) : super(context) {
        init(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        width = w.toFloat()
        height = h.toFloat()
        calculatePath()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val invalidate = tick()
        canvas.drawPath(path!!, linePaint!!)
        if (invalidate) {
            val coords = currentCoordinates
            if (coords != null) canvas.drawCircle(
                coords[0],
                coords[1],
                circleRadius.toFloat(),
                circlePaint!!
            )
            postInvalidate()
        }
    }

    /**
     * Initializes the view using the provided AttributeSet to override defaults.
     *
     * @param attrs
     */
    private fun init(attrs: AttributeSet?) {
        // Ensures onDraw is called for this view.
        setWillNotDraw(false)
        linePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        linePaint!!.style = Paint.Style.STROKE
        circlePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        circlePaint!!.style = Paint.Style.FILL_AND_STROKE
        path = Path()
        if (attrs == null) {
            setDefaults()
            return
        }
        try {
            animationDuration = DEFAULT_ANIMATION_DURATION_MS
            paddingPercent = DEFAULT_GRAPH_PADDING_PCT
            setLineColor(DEFAULT_LINE_COLOR)
            setLineThickness(DEFAULT_LINE_THICKNESS)
            setCircleColor(DEFAULT_CIRCLE_COLOR)
            circleRadius = DEFAULT_CIRCLE_RADIUS
        } finally {
        }
    }

    private fun setDefaults() {
        animationDuration = DEFAULT_ANIMATION_DURATION_MS
        paddingPercent = DEFAULT_GRAPH_PADDING_PCT
        setLineColor(DEFAULT_LINE_COLOR)
        setLineThickness(DEFAULT_LINE_THICKNESS)
        setCircleColor(DEFAULT_CIRCLE_COLOR)
        circleRadius = DEFAULT_CIRCLE_RADIUS
    }

    private fun reset() {
        if (path != null) path!!.reset()
        pathPoints = arrayListOf()
        animationStartTimeMs = System.currentTimeMillis()
        tickCount = 0
        postInvalidate()
    }

    /**
     * Sets the data set to display and recalculates the line path.
     *
     * @param data
     */
    fun setData(data: ArrayList<Float>) {
        this.data = data
        calculatePath()
    }

    /**
     * Calculates and caches the full path that will be used to draw the data set.
     */
    private fun calculatePath() {
        reset()
        if (data == null || data!!.size <= 1) {
            return
        }
        var min = Float.MAX_VALUE
        var max = Float.MIN_VALUE
        val count = data!!.size
        for (point in data!!) {
            if (point < min) {
                min = point
            }
            if (point > max) {
                max = point
            }
        }
        min *= 1 - paddingPercent
        max *= 1 + paddingPercent
        val spread = max - min
        if (spread.toDouble() == 0.0) {
            return
        }
        for (i in 0 until pathPoints.size) {
            val point = data[i]
            val x = width * (i.toFloat() / (count - 1))
            val y = height * ((point - min) / spread)
            pathPoints[i] = arrayListOf(x, y)
        }
    }

    /**
     * Returns the x/y coordinates to be used based on the current animation progress.
     *
     * @return
     */
    private val currentCoordinates: ArrayList<Float>?
        private get() {
            if (pathPoints == null) {
                return null
            }
            val elapsed = System.currentTimeMillis() - animationStartTimeMs
            if (elapsed > animationDuration) {
                return null
            }
            val frame =
                (pathPoints!!.size * Math.min(1f, elapsed / animationDuration.toFloat())).toInt()
            return if (frame >= pathPoints!!.size) {
                null
            } else pathPoints!![frame]
        }

    /**
     * Adds the next animation coordinates to the line path.
     *
     * @return
     */
    private fun tick(): Boolean {
        val coords = currentCoordinates ?: return false
        if (tickCount == 0) {
            path!!.moveTo(pathPoints[0][0], pathPoints[0][1])
        } else {
            path!!.lineTo(coords[0], coords[1])
        }
        tickCount++
        return true
    }

    /**
     * Sets the thickness of the drawn line.
     *
     * @param lineThickness
     */
    fun setLineThickness(lineThickness: Int) {
        linePaint!!.strokeWidth = lineThickness.toFloat()
    }

    /**
     * Sets the color resource ID to use for the line.
     *
     * @param lineColor
     */
    fun setLineColor(lineColor: Int) {
        linePaint!!.color = resources.getColor(lineColor)
    }

    /**
     * Sets the color resource ID to use for the circle.
     *
     * @param circleColor
     */
    fun setCircleColor(circleColor: Int) {
        circlePaint!!.color = resources.getColor(circleColor)
    }

    companion object {
        private const val DEFAULT_ANIMATION_DURATION_MS = 900
        private const val DEFAULT_LINE_THICKNESS = 15
        private const val DEFAULT_CIRCLE_RADIUS = 20
        private const val DEFAULT_GRAPH_PADDING_PCT = 0.02f
        private const val DEFAULT_LINE_COLOR = R.color.black
        private const val DEFAULT_CIRCLE_COLOR = R.color.black
    }
}
