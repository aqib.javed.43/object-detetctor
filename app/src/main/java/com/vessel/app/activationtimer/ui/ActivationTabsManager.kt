package com.vessel.app.activationtimer.ui

import android.os.Bundle
import com.vessel.app.R
import com.vessel.app.activationtimer.video1tab.Video1TabFragment
import com.vessel.app.activationtimer.video2tab.Video2TabFragment
import com.vessel.app.base.BaseFragment
import com.vessel.app.wellness.ui.FragmentLocationType
import com.vessel.app.wellness.ui.WellnessFragment
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ActivationTabsManager @Inject constructor() {

    lateinit var tabs: List<BaseFragment<*>>
    val pages = arrayOf(R.string.video_1, R.string.video_2, R.string.goals)

    init {
        createTabs()
    }

    private fun createTabs() {
        tabs = listOf(
            Video1TabFragment(),
            Video2TabFragment(),
            WellnessFragment().apply {
                arguments = Bundle().apply {
                    putInt("fragmentLocationType", FragmentLocationType.ACTIVATION_TIMER.value)
                }
            },
        )
    }

    fun getCurrentTabs(): List<BaseFragment<*>> {
        if (::tabs.isInitialized.not() || (::tabs.isInitialized && tabs.isEmpty())) {
            createTabs()
        }
        return tabs
    }

    fun clear() {
        tabs = emptyList()
    }

    companion object {
        const val VIDEO_2_TAB = 1
        const val GOALS_TAB = 2
    }
}
