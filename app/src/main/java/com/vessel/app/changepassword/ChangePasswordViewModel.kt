package com.vessel.app.changepassword

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.validators.*
import kotlinx.coroutines.launch

class ChangePasswordViewModel @ViewModelInject constructor(
    val state: ChangePasswordState,
    resourceRepository: ResourceRepository,
    private val contactManager: ContactManager,
    val preferencesRepository: PreferencesRepository,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository),
    ToolbarHandler {

    fun onUpdatePasswordClick(view: View) {
        if (!validateParams()) return

        showLoading()

        viewModelScope.launch {
            contactManager.changePassword(
                currentPassword = state.currentPassword.value!!,
                newPassword = state.newPassword.value!!
            )
                .onSuccess {
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.CHANGE_PASSWORD_SUCCESS)
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    state.showSuccessToast.call()
                    view.findNavController().popBackStack()
                }
                .onServiceError {
                    val errorMessage = it.error.message ?: getResString(R.string.unknown_error)
                    logErrorEvent(errorMessage)
                    showError(errorMessage)
                }
                .onNetworkIOError {
                    val errorMessage = getResString(R.string.network_error)
                    logErrorEvent(errorMessage)
                    showError(errorMessage)
                }
                .onUnknownError {
                    val errorMessage = getResString(R.string.unknown_error)
                    logErrorEvent(errorMessage)
                    showError(errorMessage)
                }
                .onError {
                    hideLoading()
                }
        }
    }
    private fun logErrorEvent(errorMessage: String) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CHANGE_PASSWORD_FAIL)
                .setError(errorMessage)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
    private fun validateParams() = try {
        PasswordValidator(PasswordType.Current).validate(state.currentPassword)
        ConfirmNewPasswordValidator().validate(state.newPassword, state.confirmNewPassword)
    } catch (exception: ValidatorException) {
        logErrorEvent(exception.message.toString())
        showError(exception)
        false
    }

    override fun onBackButtonClicked(view: View) {
        view.findNavController().popBackStack()
    }
}
