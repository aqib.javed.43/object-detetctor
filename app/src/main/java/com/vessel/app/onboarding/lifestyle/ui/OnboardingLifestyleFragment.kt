package com.vessel.app.onboarding.lifestyle.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.home.HomeActivity
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingLifestyleFragment : BaseFragment<OnboardingLifestyleViewModel>() {
    override val viewModel: OnboardingLifestyleViewModel by viewModels()
    override val layoutResId = R.layout.fragment_lifestyle_onboarding

    val onboardingLifestyleSelectAdapter by lazy { LifestyleSelectAdapter(viewModel) }
    private lateinit var backButton: View
    override fun onViewLoad(savedInstanceState: Bundle?) {
        backButton = requireView().findViewById(R.id.toolbar)
        if (viewModel.isBackVisible == true) {
            backButton.isVisible = false
            requireActivity().onBackPressedDispatcher.addCallback(this) {}
        }
        val lifestyleList = requireView().findViewById<RecyclerView>(R.id.lifestyleList)
        lifestyleList.layoutManager = GridLayoutManager(context, Constants.SPAN_COUNT_GOAL_GRID)
        lifestyleList.adapter = onboardingLifestyleSelectAdapter
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(navigateToHome) {
                navigateToHomeActivity()
            }
            observe(items) {
                onboardingLifestyleSelectAdapter.submitList(it)
            }
            observe(refreshItems) {
                onboardingLifestyleSelectAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun navigateToHomeActivity() {
        val intent = Intent(requireContext(), HomeActivity::class.java)
        context?.startActivity(intent)
        activity?.finishAffinity()
    }
}
