package com.vessel.app.programs.details.adapters

import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.programs.details.uimodels.ProgramScheduleRowUiModel

class ProgramScheduleRowAdapter(val SchedulePlanItemHandler: ProgramSchedulePlanItemAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<ProgramScheduleRowUiModel>() {
    override fun getLayout(position: Int) = R.layout.item_schedule_row

    override fun onBindViewHolder(holder: BaseViewHolder<ProgramScheduleRowUiModel>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position)
        val dayNumber = holder.itemView.findViewById<AppCompatTextView>(R.id.dayNumber)
        val planScheduleList = holder.itemView.findViewById<RecyclerView>(R.id.planScheduleList)

        dayNumber.text = holder.itemView.context.getString(R.string.day_tamplate, (position + 1))
        if (!item.items.isNullOrEmpty()) {
            val scheduleItemAdapter = ProgramSchedulePlanItemAdapter(SchedulePlanItemHandler, item.isUserEnrolled)
            planScheduleList.adapter = scheduleItemAdapter
            scheduleItemAdapter.submitList(item.items)
        }
    }
}
