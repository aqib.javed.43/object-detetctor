Here are the reference ranges we use for calcium: 
Low: 0 - 40 mg mg/L
Good: 40 - 140 mg/L
High: >140 mg/L

When we eat calcium, only about 25% - 50% is absorbed in our small intestines. Food and gastric acid seem to improve absorption.  Also calcium absorption appears to be dependent on having enough vitamin D.  Once it enters our bloodstream, only 8% - 10% circulates in the metabolically active ionized form, while 40% - 45% circulates bound to proteins, and 40% - 45% circulates as free dissociated ions.  Serum levels are maintained at or near 10 mg/dL by the actions of parathyroid hormone, calcitonin, and vitamin D. 99% of our total body calcium is in our skeletal system while the other 1% is in an exchangeable pool in our other tissues and circulation.  Calcium regulation is influenced by the actions of glucocorticoids, thyroid hormone, growth hormone, insulin, and estrogen. Calcium losses vary with dietary intake and approximate 300 - 600mg each day.  Renal filtration in adults is approx 8.6 g/day of which all but 100 - 200mg is reabsorbed back into the circulation. Daily fecal losses include approximately 150 mg of calcium coming from intestinal secretions as well as unabsorbed dietary calcium. Small losses in sweat (15 mg/day) may occur as well. 

When you’re in the good zone (darker purple), your body is likely getting sufficient calcium to support your wellness. 

When you ingest more calcium than your body needs, more calcium is excreted in your urine and this likely means you may be getting more calcium than you need but it could be also a sign of a medical problem or medication side effect.  

Low urine calcium levels suggest that you aren’t getting enough calcium to support your general wellness, and you may benefit from increasing your intake of both calcium and possibly vitamin D.  Check out the recommendations in the app for how to do that.


**References:**

-   https://academic.oup.com/labmed/article/41/11/683/2504912
-   https://www.labcorp.com/tests/003269/calcium-urine
-   https://www.labcorp.com/tests/134908/calcium-creatinine-ratio
-   https://www.researchgate.net/figure/Correlation-Between-24-Hour-Urine-Calcium-and-the-Spot-Urine-Calcium-to-Creatinine-Ratio_fig1_26817705
-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3295234/
-   Pediatric range References:
-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1648734/
-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6449613/
-   https://academic.oup.com/labmed/article/41/11/683/2504912
-   Nutrition in Clinical Practice: A Comprehensive, Evidence-Based Manual for the Practitioner (Nutrition in Clinical Practice), 2nd Edition by David Katz.


