package com.vessel.app.stressrelief

import android.text.SpannableString
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.checkscience.ui.OnActionHandler
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.ReminderManager
import com.vessel.app.common.manager.ScoreManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.ReagentEntry
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.plan.ui.AddPlanFooterAdapter
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.reagent.model.ReagentHeader
import com.vessel.app.reagent.ui.V2ReagentFragmentArgs
import com.vessel.app.stressrelief.model.StressReliefDayItem
import com.vessel.app.stressrelief.model.StressReliefHeaderViewItem
import com.vessel.app.stressrelief.model.StressReliefViewItem
import com.vessel.app.stressrelief.ui.StressReliefAdapter
import com.vessel.app.stressrelief.ui.StressReliefFragmentDirections
import com.vessel.app.stressrelief.ui.StressReliefHeaderAdapter
import com.vessel.app.stressreliefdetails.model.StressReliefDetailsItemModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.*
import com.vessel.app.views.weekdays.getPlanDaysOfWeek
import com.vessel.app.views.weekdays.getPlanDaysOfWeekSorted
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class StressReliefViewModel @ViewModelInject constructor(
    val state: StressReliefState,
    resourceProvider: ResourceRepository,
    private val planManager: PlanManager,
    private val reminderManager: ReminderManager,
    private val scoreManager: ScoreManager,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceProvider),
    StressReliefAdapter.OnActionHandler,
    ToolbarHandler,
    StressReliefHeaderAdapter.OnActionHandler,
    AddPlanFooterAdapter.OnActionHandler,
    OnActionHandler {
    private val header =
        StressReliefHeaderViewItem(resourceProvider.getString(R.string.see_cortisol_levels))
//    private val headerLoading = StressReliefHeaderViewItem("", true)

    init {
        state {
            stressReliefHeader.value = listOf(header)
        }
        loadUserPlans()
        observeRemindersDataState()
        observePlanDataState()
    }

    private fun loadUserPlans(forceUpdate: Boolean = false) {
        viewModelScope.launch {
            planManager.getUserPlans(forceUpdate).onSuccess {
                val lifeStyleItems = it.filter { it.isStressPlan() }
                parseStressReliefItems(lifeStyleItems, forceUpdate)
                if (forceUpdate) {
                    if (state.isDailyView) {
                        onDailySelected()
                    } else {
                        onWeeklySelected()
                    }
                }
            }
        }
    }

    private fun createWeeklyItems(
        groupedPlans: Map<Int, List<PlanData>>,
        forceUpdate: Boolean
    ) {
        val planItems = mutableListOf<StressReliefViewItem>()

        groupedPlans.values.forEach { grouped ->
            planItems.add(
                mapPlanToStressItem(
                    grouped.first(),
                    true,
                    grouped.sumBy { it.getNonCompletedDays().size },
                    grouped
                )
            )
        }

        val weekDates = getCurrentWeekDates()

        planItems.removeIf { it.items.isEmpty() }

        if (planItems.isNotEmpty()) {
            state.stressReliefInWeekly = mutableListOf(
                StressReliefDayItem(
                    "${weekDates.first.formatPlanDate()} - ${weekDates.second.formatPlanDate()}",
                    planItems,
                    false
                )
            )
        } else {
            state.stressReliefInWeekly = mutableListOf()
        }

        if ((
            !state.isDailyView && state.stressReliefItems.value?.first.orEmpty().isEmpty()
            ) || (forceUpdate && !state.isDailyView)
        ) {
            state.stressReliefItems.value = Pair(state.stressReliefInWeekly, forceUpdate)
        }
    }

    private fun createDailyItems(
        groupedPlans: Map<Int, List<PlanData>>,
        forceUpdate: Boolean
    ) {
        val dailyItems = mutableListOf<StressReliefViewItem>()
        val dailyPlans = mutableListOf<StressReliefDayItem>()
        val daysPlans = mutableMapOf<Int, MutableList<StressReliefViewItem>>()
        val days = getPlanDaysOfWeekSorted().map { it.dayIndex }
        for (i in days) {
            daysPlans[i] = mutableListOf()
        }

        groupedPlans.values.forEach { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapPlanToStressItem(it, false, grouped.size, grouped)
                )
            }
        }
        dailyItems.map { planItem ->
            val splitDays = planItem.planData.getDaysOfWeek()
            for (day in splitDays) {
                daysPlans[day]?.add(planItem)
            }
        }

        val daysOfWeek = getPlanDaysOfWeek()
        daysPlans.forEach { daysPlan ->
            val dayItems = daysPlan.value
            val date =
                daysOfWeek.first { it.dayIndex == daysPlan.key }.dayIndex
                    .toCurrentWeekDate().formatDayName()
            dailyPlans.add(
                StressReliefDayItem(
                    date,
                    dayItems,
                    true
                )
            )
        }

        dailyPlans.removeIf { it.items.isEmpty() }
        state.stressReliefDailyItems = dailyPlans
        if ((
            state.isDailyView && state.stressReliefItems.value?.first.orEmpty()
                .isEmpty()
            ) || (forceUpdate && state.isDailyView)
        ) {
            state.stressReliefItems.value = Pair(state.stressReliefDailyItems, forceUpdate)
        }
    }

    private fun parseStressReliefItems(plans: List<PlanData>, forceUpdate: Boolean) {
        val groupedPlans =
            plans.filter { it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null }
                .groupBy {
                    it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id!!
                }

        state.stressReliefHeader.value = state.stressReliefHeader.value.orEmpty().map {
            it.copy(showTabs = groupedPlans.isNotEmpty(), isDailySelected = state.isDailyView)
        }
        createDailyItems(groupedPlans, forceUpdate)
        createWeeklyItems(groupedPlans, forceUpdate)
    }

    private fun observePlanDataState() {
        viewModelScope.launch {
            planManager.getPlanDataState().collect {
                if (it) {
                    loadUserPlans(true)
                }
            }
        }
        hideLoading(false)
    }

    private fun observeRemindersDataState() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect {
                if (it) {
                    loadUserPlans(true)
                }
            }
        }
    }

    override fun onSeeCortisolLevelsClicked() {
//        state.stressReliefHeader.value = listOf(headerLoading)
        viewModelScope.launch {
            scoreManager.getWellnessScores()
                .onSuccess { score ->
//                    state.stressReliefHeader.value = listOf(header)
                    val cortisolScoreReagent =
                        score.reagents.filter { it.key.id == ReagentItem.Cortisol.id }
                    if (cortisolScoreReagent.isEmpty()) {
                        // todo show error
                    } else {
                        navigateToCortisol(
                            reagent = cortisolScoreReagent.keys.first(),
                            reagentValues = cortisolScoreReagent[
                                cortisolScoreReagent.keys.first { it.id == ReagentItem.Cortisol.id }
                            ].orEmpty()
                        )
                    }
                }
        }
    }

    private fun navigateToCortisol(
        reagent: com.vessel.app.common.model.data.Reagent,
        reagentValues: List<ReagentEntry>
    ) {
        state.navigateToReagent.value = StressReliefState.NavigateTo(
            R.id.action_stress_relief_to_reagent,
            V2ReagentFragmentArgs(
                ReagentHeader(
                    id = reagent.id,
                    title = reagent.displayName,
                    unit = reagent.unit,
                    chartEntries = reagentValues,
                    lowerBound = reagent.lowerBound,
                    upperBound = reagent.upperBound,
                    minY = reagent.minValue,
                    maxY = reagent.maxValue,
                    background = reagent.headerImage,
                    ranges = reagent.ranges,
                    errorDescription = if (reagent.id == ReagentItem.B9.id) getResString(R.string.b9_not_available_message) else getResString(
                        R.string.reagent_test_results_unavailable,
                        reagent.displayName
                    ),
                    isBetaReagent = reagent.isBeta,
                    betaHint = getResString(
                        R.string.reagent_beta_test_hint,
                        reagent.displayName
                    ),
                    consumptionUnit = reagent.consumptionUnit
                )
            )
        )
    }

    private fun mapPlanToStressItem(
        planItem: PlanData,
        isGrouped: Boolean,
        groupCount: Int,
        items: List<PlanData>
    ) = StressReliefViewItem(
        planItem.getDisplayName(),
        planItem.getPlanQuantityWithUnit(),
        planItem,
        isGrouped,
        groupCount,
        items,
        planItem.getToDoImageUrl(),
        getRemindersTitle(planItem, isGrouped.not())
    )

    private fun getRemindersTitle(todo: PlanData?, isDailyView: Boolean): String {
        return if (isDailyView) {
            todo?.getTimeFormattedWithMeridian() ?: getResString(R.string.set_time)
        } else {
            if (todo?.day_of_week == null) getResString(R.string.set_time) else getResString(R.string.reminders)
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun getCheckOutScienceHeaderContent(): SpannableString {
        return getResString(R.string.stress_relief_plan_cortisol_description)
            .makeClickableSpan(
                Pair(
                    getResString(R.string.references),
                    View.OnClickListener {
                        onLinkButtonClick(it, getResString(R.string.references))
                    }
                )
            )
    }

    override fun onLinkButtonClick(view: View, link: String) {
        val navigateTo = when (link) {
            getResString(R.string.references) ->
                HomeNavGraphDirections.globalActionToScienceReferencesDialogFragment(
                    state.getCheckOutReferences()
                )
            else ->
                HomeNavGraphDirections.globalActionToWeb(link)
        }
        view.findNavController().navigate(navigateTo)
    }

    override fun onPlanItemClicked(item: StressReliefViewItem, view: View, parentPosition: Int) {
        state.navigateTo.value =
            StressReliefFragmentDirections.actionStressReliefFragmentToStressReliefDetailsFragment(
                StressReliefDetailsItemModel(
                    item.title,
                    item.planData.getDescription(),
                    item.planData.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                        ?: return,
                    true
                )
            )
    }

    override fun onItemSelected(
        item: StressReliefViewItem,
        view: View,
        position: Int,
        parentPosition: Int
    ) {
        state.navigateTo.value =
            StressReliefFragmentDirections.actionStressReliefFragmentToStressReliefDetailsFragment(
                StressReliefDetailsItemModel(
                    item.title,
                    item.planData.getDescription(),
                    item.planData.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                        ?: return,
                    true
                )
            )
    }

    override fun onAddReminderClicked(item: StressReliefViewItem, view: View, parentPosition: Int) {
        state.navigateTo.value =
            StressReliefFragmentDirections.actionStressReliefFragmentToPlanReminderDialog(
                PlanReminderHeader(
                    item.title,
                    item.planData.getDescription(),
                    item.image,
                    0,
                    item.items
                )
            )
    }

    override fun onReminderNotificationClicked(
        item: StressReliefViewItem,
        view: View,
        position: Int,
        parentPosition: Int
    ) {
    }

    override fun onItemDeleteClicked(
        item: StressReliefViewItem,
        view: View,
        position: Int,
        parentPosition: Int
    ) {
        viewModelScope.launch {
            val result = if (item.isGrouped) {
                planManager.deletePlanItem(item.items.map { it.id ?: -1 })
            } else {
                planManager.deletePlanItem(listOf(item.planData.id ?: -1))
            }
            result
                .onSuccess {
                    hideLoading(false)
                    val items = state.stressReliefItems.value?.first.orEmpty().toMutableList()
                    val parentItem = items[parentPosition]
                    val todos = parentItem.items.toMutableList()
                    if (position < todos.size)
                        todos.removeAt(position)

                    val updatedItem = parentItem.copy(
                        items = todos
                    )

                    items[parentPosition] = updatedItem
                    if (updatedItem.items.isEmpty()) {
                        items.removeAt(parentPosition)
                    }
                    state.stressReliefItems.value = Pair(items, false)

                    if (items.isEmpty()) {
                        state.stressReliefHeader.value =
                            state.stressReliefHeader.value.orEmpty().map {
                                it.copy(
                                    showTabs = items.isNotEmpty(),
                                    isDailySelected = state.isDailyView
                                )
                            }
                    }
                    reminderManager.updateRemindersCachedData()
                    planManager.updatePlanCachedData()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(
                                TrackingConstants.KEY_ID,
                                item.items.map { it.id ?: -1 }.toString()
                            )
                            .addProperty(
                                TrackingConstants.TYPE,
                                item.planData.getPlanType().title
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Stress Relief")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onError {
                    hideLoading(false)
                    showError(getResString(R.string.unknown_error))
                }
        }
    }

    override fun onItemEditClicked(item: StressReliefViewItem, view: View, parentPosition: Int) {
        state.navigateTo.value =
            StressReliefFragmentDirections.actionStressReliefFragmentToPlanReminderDialog(
                PlanReminderHeader(
                    item.title,
                    item.planData.getDescription(),
                    item.image,
                    0,
                    item.items
                )
            )
    }

    override fun onGroupedItemsClicked(
        item: StressReliefViewItem,
        view: View,
        parentPosition: Int
    ) {
    }

    override fun onAddPlanClicked() {
        state.navigateTo.value =
            StressReliefFragmentDirections.actionStressReliefFragmentToBuildStressReliefFragment()
    }

    override fun onDailySelected() {
        state.isDailyView = true
        state.stressReliefItems.value = Pair(state.stressReliefDailyItems, false)
    }

    override fun onWeeklySelected() {
        state.isDailyView = false
        state.stressReliefItems.value = Pair(state.stressReliefInWeekly, false)
    }

    override fun onAddPlanFooterClicked(item: Any, view: View) {
        state.navigateTo.value =
            StressReliefFragmentDirections.actionStressReliefFragmentToBuildStressReliefFragment()
    }
}
