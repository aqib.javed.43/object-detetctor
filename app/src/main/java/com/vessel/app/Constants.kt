package com.vessel.app

import android.Manifest
import android.os.Build
import com.vessel.app.common.net.data.plan.BadHabitRecord
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.common.net.data.plan.OnBoardingProgramRecord
import com.vessel.app.common.net.data.plan.SubGoalRecord
import com.vessel.app.onboarding.badhabitsselection.BadHabitSelect
import com.vessel.app.plan.model.PlanItem
import com.vessel.app.wellness.model.GoalButton
import com.vessel.app.wellness.net.data.ProgramAction
import com.vessel.app.wellnesscoreinfo.model.WellnessScore
import java.text.SimpleDateFormat
import java.util.*

object Constants {
    const val THREE_CARDS = 3
    const val APPLICATION_ID = "com.vesselhealth.android"
    const val WEEK_TIME: Long = 7 * 24 * 60 * 1000
    const val WEEKLY: Int = 7
    var TRIAL_FREE_DAYS: Int = 14
    const val MAIN_GOAL: String = "mainGoal"
    const val POLL_ITEM: String = "poll_question"
    const val TEXT_MAX_COUNT: Int = 500
    const val POST_ITEM: String = "post_item"
    const val POST_TYPE: String = "post_type"
    val SERVER_INSERT_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)

    val CAPTURE_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
    val LESSON_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.ENGLISH)

    val DAY_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    val MONTH_YEAR_DATE_FORMAT = SimpleDateFormat("MMM yyyy", Locale.ENGLISH)

    val DAY_NAME_FORMAT = SimpleDateFormat("EEEE", Locale.ENGLISH)
    val DAY_MONTH_NAME_FORMAT = SimpleDateFormat("dd MMM", Locale.ENGLISH)

    val PLAN_DAY_DATE_FORMAT = SimpleDateFormat("EEEE MMMM d", Locale.ENGLISH)

    const val REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE = -1f

    const val PATH_SUPPLEMENTS = "pages/supplement"
    const val PATH_ACCOUNT = "account"
    const val CONTACT_ID = "contact_id"
    const val TESTING_PROGRAM_PERIOD: Int = 30

    const val OPEN_APP_SETTINGS_REQUEST_CODE = 54321

    val SCAN_CARD_PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        listOf(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.READ_MEDIA_VIDEO
        )
    } else {
        listOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    const val SCAN_CARD_PERMISSIONS_REQUEST_CODE = 1000

    const val GRID_SPAN_COUNT = 2
    const val TODO_WITHOUT_RECURRING_TODO_ID = -1
    const val SPAN_COUNT_GOAL_GRID = 2
    const val TAB_VERTICAL_BADGE_OFFSET = 30
    const val TAB_HORIZONTAL_BADGE_OFFSET = -25
    const val TAB_HORIZONTAL_BADGE_PADDING = 60
    const val REMINDERS_TIME_FORMAT = "HH:mm:ssZ"
    const val REMINDERS_CREATED_AT_FORMAT = "yyyy-MM-dd"
    private const val REMINDERS_TIME_24_FORMAT = "H:mm"
    private const val REMINDERS_TIME_12_FORMAT = "h:mm aa"
    private const val REMINDERS_TIME_24_WITH_SECONDS_FORMAT = "H:mm:ss"
    val SIMPLE_TIME_24_HOUR_FORMAT = SimpleDateFormat(REMINDERS_TIME_24_FORMAT, Locale.US)
    val SIMPLE_TIME_12_HOUR_FORMAT = SimpleDateFormat(REMINDERS_TIME_12_FORMAT, Locale.US)
    val SIMPLE_TIME_24_WITH_SECONDS_FORMAT =
        SimpleDateFormat(REMINDERS_TIME_24_WITH_SECONDS_FORMAT, Locale.US)
    const val DAYS_IN_MONTH: Int =
        30 // it used in the calculation just to show an approximate number per day
    const val USER_ALLOWED_GOALS_SELECTION_COUNT = 3
    const val FIDUCIALS_COUNT = 4
    const val REMOTE_CONFIG_TIMER_SURVEY_ID = "timer_survey_v2_id"
    const val REMOTE_CONFIG_CUSTOM_TIP_IMAGE_URLS = "custom_tip_image_urls"
    const val LAST_TEST_VALID_DATE = 10
    const val RATE_IT_POINT = 99f
    const val MIN_REQUIRED_X_POINTS = 5
    const val UNRATED_POINT = 49f

    // Plan Ids
    var HYDRATION_PLAN_ID = 5
    var TEST_PLAN_ID = 41

    val SUPPORTED_DEVICES_MODELS_PREFIX = listOf(
        "SM-G77", // galaxy-s10-lite
        "SM-G96", // Galaxy S9
        "SM-G97", // Galaxy S10
        "SM-G98", // Galaxy S20
        "SM-G99", // Galaxy S21
        "SM-N96", // Galaxy Note 9
        "SM-N97", // Galaxy Note 10
        "SM-N77", // Galaxy Note 10 lite
        "SM-N98", // Galaxy Note 20
        "SM-N99", // Galaxy Note 21
        "SM-N79", // Galaxy Note 21 lite
//         Pixel 3
        "pixel3",
        "pixel 3",
        "G013",
//         Pixel 4
        "pixel4",
        "pixel 4",
        "G020",
        "GA0118",
        "GA01191",
        "G020M",
        "G020I",
        "GA01188",
        "GA01187",
        "GA01189",
        "GA01191",
        "GA01189",
//        Pixel 4XL
        "G020P",
        "G020",
        "GA01181",
        "GA01182",
        "GA01180",
//         Pixel 4a
        "pixel4a",
        "pixel 4a",
        "G025J",
        "GA02099",
//        Pixel 4a 5G
        "GD1YQ",
        "G025I",
//        Pixel 5
        "pixel5",
        "pixel 5",
        "GD1YQ",
        "GTT9Q"
    )

    val WEEK_DAYS = listOf(0, 1, 2, 3, 4, 5, 6)

    var GOALS = listOf<GoalRecord>()
    var GOALBUTTONS = listOf<GoalButton>()
    var SUBGOALS = listOf<SubGoalRecord>()
    var BADHABIT = listOf<BadHabitRecord>()
    var BADHABIT_SELECTS = listOf<BadHabitSelect>()
    var ONBOARDINGPROGRAM = listOf<OnBoardingProgramRecord>()
    var onGotoPreviousScreen: (() -> Unit)? = null
    var onTakeTest: (() -> Unit)? = null
    var onConfirm: (() -> Unit)? = null
    var onCancel: (() -> Unit)? = null
    var currentPlan: PlanItem? = null
    var onSelectConfirm: ((action: ProgramAction) -> Unit)? = null
    var onLessonCompleted: ((programId: Int, lessonId: Long?) -> Unit)? = null
    var wellnessScore: WellnessScore? = null

    const val LIFESTYLE_TEST_ID = "lifestyle_test_id"
    const val LIFESTYLE_HYDRATION_ID = "lifestyle_hydration_id"
    const val GOAL_TAB = "GOAL TAB"
    const val AUTO_BUILD_PLAN = "AUTO_BUILD_PLAN"
    const val CONGRATULATION_SCREEN = "CONGRATULATION_SCREEN"
    const val EXPERT_PAGE = "EXPERT PAGE"
    const val COMPLETE_PROGRAM_PAGE = "COMPLETE PROGRAM PAGE"
    const val PROGRAM_DETAILS_PAGE = "PROGRAM DETAILS PAGE"
    const val FREE_TRIAL_PERIOD = "free_trial_period_in_days"

    // Feelings
    const val FEELING_NOTHING = ""
    const val FEELING_UNHAPPY = "unhappy"
    const val FEELING_MEH = "meh"
    const val FEELING_HAPPY = "happy"

    enum class QuestionType(type: String) {
        QUIZ("QUIZ"), SURVEY("SURVEY"), INPUT("INPUT"), READONLY("READONLY")
    }
}
