package com.vessel.app.wellness.net.data
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SubGoalResponseData(
    val subgoals: List<SubGoalData>
)
