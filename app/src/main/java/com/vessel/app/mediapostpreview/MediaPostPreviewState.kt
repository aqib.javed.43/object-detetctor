package com.vessel.app.mediapostpreview

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class MediaPostPreviewState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: MediaPostPreviewState.() -> Unit) = apply(block)
}
