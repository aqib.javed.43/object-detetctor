package com.vessel.app.onboarding.terms

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnboardingTermsState @Inject constructor() {
    val closeEvent = LiveEvent<NavDirections>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: OnboardingTermsState.() -> Unit) = apply(block)
}
