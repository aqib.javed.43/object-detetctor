package com.vessel.app.testwinscore.ui

import android.content.Context
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.vessel.app.R

class TestWinScoreSpanLookup(
    context: Context,
    private val adapter: ConcatAdapter
) : GridLayoutManager.SpanSizeLookup() {
    private val spanCount = context.resources.getInteger(R.integer.test_win_score_list_span_count)

    override fun getSpanSize(position: Int): Int {
        return when (adapter.getItemViewType(position)) {
            R.layout.item_test_win_score_goal -> 1
            else -> spanCount
        }
    }
}
