package com.vessel.app.plan.ui

import android.view.View
import com.google.android.material.textview.MaterialTextView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.plan.model.AddPlanFooter

class AddPlanFooterAdapter(private val handler: OnActionHandler, private val addToPlanText: Int = R.string.add_to_plan) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        AddPlanFooter -> R.layout.item_add_plan_header
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<Any>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        holder.itemView.findViewById<MaterialTextView>(R.id.addToPlan).setText(addToPlanText)
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun getHandler(position: Int): Any? = handler

    interface OnActionHandler {
        fun onAddPlanFooterClicked(item: Any, view: View)
    }
}
