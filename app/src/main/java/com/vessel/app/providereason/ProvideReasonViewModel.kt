package com.vessel.app.providereason

import android.util.Log
import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.welcometeams.WelcomeTeamFragmentDirections
import com.vessel.app.welcometeams.WelcomeTeamViewModel

class ProvideReasonViewModel@ViewModelInject constructor(
    val state: ProvideReasonState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler {
    init {
    }
    override fun onBackButtonClicked(view: View) {
        view.findNavController().navigateUp()
    }
    fun onContinueClick() {
        // Todo process continue
        Log.d(WelcomeTeamViewModel.TAG, "onContinueClick: ")
        state.navigateTo.value = ProvideReasonFragmentDirections.actionProvideReasonFragmentToUploadPhotoFragment()
    }
    fun onLaterClicked(v: View) {
        state.navigateTo.value = WelcomeTeamFragmentDirections.actionGlobalHomeTeamFragment()
    }
    companion object {
        const val TAG = "ProvideReasonViewModel"
    }
}
