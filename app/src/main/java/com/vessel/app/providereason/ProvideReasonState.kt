package com.vessel.app.providereason

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ProvideReasonState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: ProvideReasonState.() -> Unit) = apply(block)
}
