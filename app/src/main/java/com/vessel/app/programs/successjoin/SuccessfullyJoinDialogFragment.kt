package com.vessel.app.programs.successjoin

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SuccessfullyJoinDialogFragment : BaseDialogFragment<SuccessfullyJoinDialogViewModel>() {
    override val viewModel: SuccessfullyJoinDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_successfully_join_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_0).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
            setGravity(Gravity.BOTTOM)
        }
        isCancelable = false
        observe(viewModel.dismissDialog) { dismiss() }
        observe(viewModel.navigateTo) { findNavController().navigate(it) }
    }
}
