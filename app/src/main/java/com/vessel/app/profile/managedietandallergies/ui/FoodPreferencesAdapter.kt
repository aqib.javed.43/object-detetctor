package com.vessel.app.profile.managedietandallergies.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.profile.managedietandallergies.model.FoodPreferencesSelect

class FoodPreferencesAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<FoodPreferencesSelect>() {
    override fun getLayout(position: Int) = R.layout.item_food_preferences_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onFilterSelectChecked(item: FoodPreferencesSelect, view: View)
    }
}
