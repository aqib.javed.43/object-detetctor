package com.vessel.app.common.net.mapper

import com.vessel.app.common.model.SampleRecommendations
import com.vessel.app.common.net.data.SampleRecommendationsData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RecommendationMapper @Inject constructor(private val sampleMapper: SampleMapper) {
    fun map(data: SampleRecommendationsData) = SampleRecommendations(
        supplements = sampleMapper.map(data.supplement),
        lifestyle = sampleMapper.map(data.lifestyle),
        food = data.food
    )
}
