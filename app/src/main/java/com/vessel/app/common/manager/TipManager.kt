package com.vessel.app.common.manager

import com.vessel.app.common.repo.TipRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TipManager @Inject constructor(private val tipRepository: TipRepository) {

    suspend fun getTipDetails(id: Int) =
        tipRepository.getTipDetails(id)

    suspend fun addTip(
        title: String,
        imageUrl: String,
        frequency: String,
        mainGoalId: Int
    ) = tipRepository.addTip(
        title,
        imageUrl,
        frequency,
        mainGoalId
    )
    suspend fun getTipsByTags(options: Map<String, String>) =
        tipRepository.getTipByTags(options)
}
