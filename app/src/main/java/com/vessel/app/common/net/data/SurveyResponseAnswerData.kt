package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SurveyResponseAnswerData(
    val question_id: Int?,
    val answer_id: Int? = null,
    val answer_text: String? = null
)
