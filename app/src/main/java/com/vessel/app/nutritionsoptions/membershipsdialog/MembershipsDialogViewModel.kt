package com.vessel.app.nutritionsoptions.membershipsdialog

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class MembershipsDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    val authManager: AuthManager,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    val dismissScreen = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()

    fun dismiss() {
        dismissScreen.call()
    }

    fun onManageMembershipClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.MANAGE_MEMBERSHIP_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        showLoading()
        viewModelScope.launch {
            authManager.multipass(Constants.PATH_ACCOUNT)
                .onSuccess {
                    hideLoading(false)
                    navigateTo.value = HomeNavGraphDirections.globalActionToWeb(it.multipass_url)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onHttpOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }
}
