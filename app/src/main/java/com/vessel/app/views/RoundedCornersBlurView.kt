package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import android.view.ViewOutlineProvider
import androidx.core.content.ContextCompat
import com.github.mmin18.widget.RealtimeBlurView
import com.vessel.app.R

open class RoundedCornersBlurView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : RealtimeBlurView(context, attrs) {

    init {
        if (!isInEditMode) {
            val drawableResId = attrs?.getAttributeResourceValue(
                "http://schemas.android.com/apk/res/android",
                "background",
                R.drawable.rounded_corners
            ) ?: R.drawable.rounded_corners
            setBackgroundResource(drawableResId)
            outlineProvider = ViewOutlineProvider.BACKGROUND
            clipToOutline = true
            setBlurRadius(20.0f)
        }
    }
}

class BlackRoundedCornersBlurView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : RoundedCornersBlurView(context, attrs) {

    init {
        setOverlayColor(ContextCompat.getColor(context, R.color.blackAlpha70))
    }
}

class WhiteRoundedCornersBlurView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : RoundedCornersBlurView(context, attrs) {

    init {
        setOverlayColor(ContextCompat.getColor(context, R.color.whiteAlpha40))
    }
}

class WhiteRoundedCorners70BlurView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : RoundedCornersBlurView(context, attrs) {

    init {
        setOverlayColor(ContextCompat.getColor(context, R.color.whiteAlpha70))
    }
}
