package com.vessel.app.checkin.happy

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.binding.srcFromUrl
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.check_in_chart_layout.*
import kotlinx.android.synthetic.main.check_in_chart_layout.view.*

@AndroidEntryPoint
class HappyCheckInFragment : BaseFragment<HappyCheckInViewModel>() {
    override val viewModel: HappyCheckInViewModel by viewModels()
    override val layoutResId = R.layout.fragment_monthly_checkin_happy
    private val happyCheckInAdapter by lazy { HappyCheckInAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        val rcvLessons = requireView().findViewById<RecyclerView>(R.id.rcvLessions)
        rcvLessons.layoutManager = LinearLayoutManager(context)
        rcvLessons.adapter = happyCheckInAdapter
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(selectedGoal) {
                rateYourGoalHeaderContainer.chart.setEntries(it.chartEntries, it.averageChartEntries)
                rateYourGoalHeaderContainer.headerBg.srcFromUrl(it.smallImageUrl)
                rateYourGoalHeaderContainer.chartTitle.text = String.format(getString(R.string.chart_title_check_in), "${it.chartEntries.size} ${getString(it.title)}")
            }
            observe(activitiesItems) {
                happyCheckInAdapter.submitList(it)
            }
        }
    }
}
