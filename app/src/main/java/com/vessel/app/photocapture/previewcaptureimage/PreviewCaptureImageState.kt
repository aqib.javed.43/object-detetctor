package com.vessel.app.photocapture.previewcaptureimage

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import javax.inject.Inject

class PreviewCaptureImageState @Inject constructor() {
    var uuid: String? = null
    val navigateTo = MutableLiveData<NavDirections>()

    operator fun invoke(block: PreviewCaptureImageState.() -> Unit) = apply(block)
}
