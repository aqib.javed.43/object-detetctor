package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass
import com.vessel.app.common.net.data.plan.GoalRecord

@JsonClass(generateAdapter = true)
data class LifestyleData(
    val id: Int,
    val activity_name: String,
    val quantity: Int?,
    val extra_images: List<String>?,
    val description: String?,
    val frequency: String?,
    val image_url: String?,
    val total_likes: Int?,
    val dislikes_count: Int?,
    val like_status: String?,
    val reagent_bucket_id: Int?,
    val lifestyle_recommendation_id: Int?,
    val unit: String?,
    val impactsGoals: List<GoalRecord>?
)
