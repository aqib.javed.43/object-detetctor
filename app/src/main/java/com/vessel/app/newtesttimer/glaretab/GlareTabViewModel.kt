package com.vessel.app.newtesttimer.glaretab

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class GlareTabViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider)
