package com.vessel.app.buildplan

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class BuildPlanState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val makePlanItems = MutableLiveData<List<Any>>()
    operator fun invoke(block: BuildPlanState.() -> Unit) = apply(block)
}
