package com.vessel.app.common.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import com.vessel.app.common.model.data.Reagent
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.wellness.net.data.LikeStatus
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Sample(
    val reagent: Reagent?,
    val goals: List<GoalRecord>?,
    val amount: Float,
    val apiName: String,
    val supplementId: Int?,
    val description: String? = null,
    val lifestyle_recommendation_id: Int? = null,
    val id: Int? = null,
    val unit: String? = null,
    val frequency: String? = null,
    val image_url: String? = null,
    val in_plan: Boolean? = null,
    val extra_images: List<String>? = null,
    val like_status: LikeStatus?,
    val total_likes: Int? = null,
    val dislikes_count: Int? = null,
) : Parcelable
