package com.vessel.app.coach

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.livechatinc.inappchat.ChatWindowActivity
import com.livechatinc.inappchat.ChatWindowConfiguration
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_teams.*
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity

@AndroidEntryPoint
class CoachFragment : BaseFragment<CoachViewModel>() {
    override val viewModel: CoachViewModel by viewModels()
    override val layoutResId: Int = R.layout.fragment_coach

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(openMessaging) {
                if (viewModel.isLiveChat()) {
                    val intent = Intent(requireContext(), ChatWindowActivity::class.java)
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_GROUP_ID,
                        BuildConfig.NUTRITIONIST_COACH_GROUP
                    )
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_LICENCE_NUMBER,
                        BuildConfig.LIVE_CHAT_LICENSE
                    )
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_VISITOR_NAME,
                        viewModel.getUserName()
                    )
                    intent.putExtra(
                        ChatWindowConfiguration.KEY_VISITOR_EMAIL,
                        viewModel.getUserEmail()
                    )
                    requireContext().startActivity(intent)
                } else {
                    MessagingActivity.builder()
                        .withToolbarTitle(getString(R.string.chat_to_nutritionist))
                        .withEngines(ChatEngine.engine())
                        .show(requireContext(), viewModel.getChatConfiguration())
                }
            }
        }
    }
}
