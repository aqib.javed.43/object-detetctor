package com.vessel.app.posttestnutritioncoach.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.posttestnutritioncoach.PostTestNutritionCoachViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_add_supplements.*

@AndroidEntryPoint
class PostTestNutritionCoachFragment : BaseFragment<PostTestNutritionCoachViewModel>() {
    override val viewModel: PostTestNutritionCoachViewModel by viewModels()
    override val layoutResId = R.layout.fragment_post_test_nutrition_coach
    val bulletStringAdapter by lazy { BulletStringAdapter() }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        alignedList.adapter = bulletStringAdapter
    }
}
