package com.vessel.app.onboardingintro.introprogam

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class IntroProgramViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    private val programManager: ProgramManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler {
    var message: String = ""
    val state = IntroProgramState()
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
    init {
        loadPrograms()
    }

    fun updateExistingUser() {
        preferencesRepository.isExistingUserFirstTime = false
    }
    fun onDoneClicked() {
        // Todo check user had main goal or not ?
        state.isJoinedProgram.value?.let {
            if (it) {
                state.navigateTo.value = IntroProgramFragmentDirections.actionIntroProgramFragmentToRateMainGoalSelectionFragment()
            } else {
                state.navigateTo.value = IntroProgramFragmentDirections.actionIntroProgramFragmentToGoalsSelectFragment(false)
            }
        }
    }

    fun loadPrograms() {
        viewModelScope.launch {
            showLoading()
            programManager.getEnrolledPrograms()
                .onSuccess { enrolledPrograms ->
                    hideLoading()
                    state.isJoinedProgram.value = enrolledPrograms.isNotEmpty()
                }
                .onServiceError {
                    hideLoading()
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }
                .onNetworkIOError {
                    hideLoading()
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    hideLoading()
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }
}
