package com.vessel.app.stressreliefdetails.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.stressrelief.model.StressReliefViewItem

class StressReliefContentAdapter() : BaseAdapterWithDiffUtil<StressReliefViewItem>() {

    override fun getLayout(position: Int) = R.layout.item_yoga_details

//        when (getItem(position).stressReliefItem) {
//        StressReliefItem.YOGA -> R.layout.item_yoga_details
//        StressReliefItem.MEDITATE -> R.layout.item_meditation_details
//        StressReliefItem.CARDIO -> R.layout.item_cardio_details
//        StressReliefItem.BREATH -> R.layout.item_breathwork_details
//        StressReliefItem.FRIEND -> R.layout.item_hangout_details
//        StressReliefItem.NIGHT -> R.layout.item_night_time_details
//        else -> throw IllegalStateException(
//            "Unexpected SupplementAdapter type at position $position for item ${getItem(position)}"
//        )
//    }
}
