package com.vessel.app.today.review

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_review_activity.*

@AndroidEntryPoint
class ActivityReviewFragment : BaseFragment<ActivityReviewViewModel>() {
    override val viewModel: ActivityReviewViewModel by viewModels()
    override val layoutResId = R.layout.fragment_review_activity

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
    }
    private fun setupObservers() {
        viewModel.state {
            observe(likeBackground) {
                likeBg.background = ContextCompat.getDrawable(requireContext(), it)
            }
            observe(disLikeBackground) {
                dislikeBg.background = ContextCompat.getDrawable(requireContext(), it)
            }
            observe(remainingCharacters) {
                remainingChars.text = it
            }
            observe(textColor) {
                remainingChars.setTextColor(ContextCompat.getColor(requireContext(), it))
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
