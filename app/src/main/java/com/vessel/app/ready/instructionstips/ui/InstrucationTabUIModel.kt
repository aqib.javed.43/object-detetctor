package com.vessel.app.ready.instructionstips.ui

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class InstrucationTabUIModel(
    @StringRes val tabName: Int,
    @StringRes val title: Int,
    @DrawableRes val periodIcon: Int,
    @DrawableRes val cardIcon: Int,
    val list: List<InstrucationTabTipsUIModel>
)
data class InstrucationTabTipsUIModel(
    @StringRes val text: Int,
    @DrawableRes val icon: Int
)
