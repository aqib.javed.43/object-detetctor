package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MembershipPropertiesData(
    val name: String,
    val value: String
)
