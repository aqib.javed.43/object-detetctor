package com.vessel.app.common.net.mapper

import com.vessel.app.common.model.AuthToken
import com.vessel.app.common.net.data.AuthData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthMapper @Inject constructor() {
    fun map(data: AuthData) = AuthToken(
        accessToken = data.access_token,
        refreshToken = data.refresh_token
    )
}
