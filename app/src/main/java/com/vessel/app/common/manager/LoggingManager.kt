package com.vessel.app.common.manager

import com.google.firebase.crashlytics.FirebaseCrashlytics
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoggingManager @Inject constructor(private val crashlytics: FirebaseCrashlytics) {
    fun setUserId(userId: Int) {
        crashlytics.setUserId(userId.toString())
    }

    fun logError(throwable: Throwable) {
        crashlytics.recordException(throwable)
    }

    fun addToLog(message: String) {
        crashlytics.log(message)
    }
}
