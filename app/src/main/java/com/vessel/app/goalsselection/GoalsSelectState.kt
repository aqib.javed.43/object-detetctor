package com.vessel.app.goalsselection

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class GoalsSelectState @Inject constructor(val goToOneGoalAfterFinish: Boolean) {
    val items = MutableLiveData<List<GoalSelect>>()
    val refreshItems = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: GoalsSelectState.() -> Unit) = apply(block)
}
