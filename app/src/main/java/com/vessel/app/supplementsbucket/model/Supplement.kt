package com.vessel.app.supplementsbucket.model

import android.content.Context
import android.os.Parcelable
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.vessel.app.R
import com.vessel.app.util.extensions.formatDigits
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import kotlin.math.roundToInt

@Parcelize
data class Supplement(
    val id: Int,
    val warningDescription: String?,
    val description: String?,
    var dosageUnit: String,
    val name: String,
    var dosage: Double,
    val volume: Double,
    var price: Double,
    val supplementAssociatedGoals: List<SupplementAssociatedGoalSample>,
    val isMultivitamin: Boolean,
    val supplementAssociation: List<SupplementAssociation>?,
    var isAdded: Boolean = false
) : Parcelable {

    @IgnoredOnParcel
    var canAddForFree: Boolean = true

    @DrawableRes
    fun background() = if (isAdded) {
        R.drawable.white_rounded_background
    } else {
        R.drawable.black_alpha70_background
    }

    @ColorRes
    fun textColor() = if (isAdded) {
        R.color.black
    } else {
        R.color.white
    }

    fun buttonText(context: Context) = if (isAdded) {
        context.getString(R.string.remove)
    } else {
        if (canAddForFree) {
            context.getString(R.string.add_for_free)
        } else {
            context.getString(R.string.add_plus_amount, price.formatDigits(2))
        }
    }

    @IgnoredOnParcel
    val showDescriptionIcon = description != null

    fun amount() = buildString {
        append(dosage.roundToInt())
        append(" ")
        append(dosageUnit)
    }
}
