package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GoalAssessmentData(
    val created_at: String,
    val assessments: List<AssessmentData>
)
