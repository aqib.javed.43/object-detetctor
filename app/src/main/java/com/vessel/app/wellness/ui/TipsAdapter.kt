package com.vessel.app.wellness.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.views.RecommendationListWidgetOnActionHandler
import com.vessel.app.wellness.model.RecommendationItem

class TipsAdapter(private val handler: RecommendationListWidgetOnActionHandler) : BaseAdapterWithDiffUtil<RecommendationItem>() {

    override fun getLayout(position: Int) = R.layout.item_expanded_tip

    override fun getHandler(position: Int) = handler
}
