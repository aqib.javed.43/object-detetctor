package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass
import com.vessel.app.common.net.data.plan.GoalRecord

@JsonClass(generateAdapter = true)
data class SearchedFilteredFoodList(
    val food_response: List<FoodResponse>?,
    val pagination: Pagination?
)

@JsonClass(generateAdapter = true)
data class FoodResponse(
    val allergy_Crustacean: Boolean?,
    val allergy_Milk: Boolean?,
    val allergy_Seeds: Boolean?,
    val diet_Keto: Boolean?,
    val diet_LowCalorie: Boolean?,
    val diet_LowCarb: Boolean?,
    val diet_LowFat: Boolean?,
    val diet_LowSugar: Boolean?,
    val diet_Paleo: Boolean?,
    val diet_Vegan: Boolean?,
    val diet_Vegetarian: Boolean?,
    val food_title: String?,
    val id: Int?,
    val image_url: String?,
    val nutrient_Magnesium: Float?,
    val nutrient_Tryptophan: Float?,
    val nutrient_VitaminB12: Float?,
    val nutrient_VitaminB7: Float?,
    val nutrient_VitaminB9: Float?,
    val nutrient_VitaminC: Float?,
    val popularity: Int?,
    val goals: List<GoalRecord>?,
    val serving_grams: Float?,
    val serving_quantity: Float?,
    val serving_unit: String?,
    val usda_ndb_number: Int?,
    val food_categories: List<String>?,
    val like_status: String?,
    val total_likes: Int?
)
@JsonClass(generateAdapter = true)
data class Pagination(
    val page: Int?,
    val pages: Int?,
    val per_page: Int?
)
