package com.vessel.app.common.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

// NOTE: @SerializedName only used here because we invoke Gson directly in:
// AppleGoogleAuthWebViewActivity
// If we do not annotation these fields, R8 obfuscation prevents Gson from deserializing
data class AuthToken(
    @SerializedName("access_token")
    val accessToken: String?,
    @SerializedName("refresh_token")
    val refreshToken: String?
) : Serializable
