package com.vessel.app.reagent.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BenefitsOfBeingOptimal(
    val informationTypeID: String,
    val informationType: String,
    val information: String,
    val sources: List<InfoSource>
)
