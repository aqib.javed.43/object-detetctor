// TODO fix the Unit test

// package com.vessel.app.wellness
//
// import androidx.lifecycle.Observer
// import com.vessel.app.*
// import com.vessel.app.common.manager.ScoreManager
// import com.vessel.app.common.model.Result
// import com.vessel.app.common.repo.GoalsRepository
// import com.vessel.app.util.ResourceRepository
// import io.mockk.*
// import org.junit.jupiter.api.Test
//
// class WellnessViewModelTest : BaseTest() {
//    private val resourceRepository = mockk<ResourceRepository>()
//    private val scoreManager = mockk<ScoreManager>()
//    private val goalsRepository = mockk<GoalsRepository>()
//
//    private lateinit var state: WellnessState
//    private lateinit var viewModel: WellnessViewModel
//
//    private fun init() {
//        state = WellnessState()
//
//        coEvery { scoreManager.getWellnessScores() }
//            .answers { Result.Success(ScoreFaker.basic()) }
//
//        viewModel = WellnessViewModel(
//            state,
//            resourceRepository,
//            scoreManager,
//            goalsRepository
//        )
//    }
//
//    @Test
//    fun `it should open the nutritionist web view when clicking the button`() {
//        init()
//        val (view, navController) = MockProvider.mockedNavigation()
//
//        viewModel.onTalkToNutritionistClick(view)
//
//        verify {
//            navController.navigate(HomeNavGraphDirections.globalActionToWeb(BuildConfig.NUTRITIONIST_URL))
//        }
//    }
//
//    @Test
//    fun `it should tell the view to launch the live chat`() {
//        init()
//        var launchedLiveChatEvent = false
//        val observer = Observer<Unit> { launchedLiveChatEvent = true }
//        state.launchLiveChatEvent.observeForever(observer)
//
//        viewModel.onCustomerSupportClick()
//
//        launchedLiveChatEvent.isTrue()
//        state.launchLiveChatEvent.removeObserver(observer)
//    }
// }
