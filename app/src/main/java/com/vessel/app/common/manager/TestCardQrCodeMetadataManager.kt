package com.vessel.app.common.manager

import android.content.Context
import android.graphics.Bitmap
import com.amazonaws.services.s3.model.ObjectMetadata
import com.google.gson.JsonObject
import com.google.zxing.Result
import com.google.zxing.ResultPoint
import com.google.zxing.qrcode.detector.AlignmentPattern
import com.google.zxing.qrcode.detector.FinderPattern
import dagger.hilt.android.qualifiers.ApplicationContext
import org.opencv.core.Rect
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TestCardQrCodeMetadataManager @Inject constructor(@ApplicationContext val context: Context) {
    private var fiducialFoundTemplateMatches: MutableList<Rect> = mutableListOf()
    private var findersPoints: MutableList<Pair<Float, Float>> = mutableListOf()
    private var alignmentPoint: Pair<Float, Float>? = null
    private var imageWidth = 0
    private var imageHeight = 0
    private var resizeScale = 0.toDouble()

    fun getExifMetaData(): String {
        return if (findersPoints.isNotEmpty() && alignmentPoint != null) {
            val metadata = QRMetaData(findersPoints, alignmentPoint!!)
            val cardTopLeftPoint = metadata.getTopLeftPoint()
            val cardTopRightPoint = metadata.getTopRightPoint()
            val cardBottomRightPoint = metadata.getBottomRightPoint()
            val cardBottomLeftPoint = metadata.getBottomLeftPoint()

            return JsonObject().apply {
                addProperty("topLeft_x", cardTopLeftPoint.first)
                addProperty("topLeft_y", cardTopLeftPoint.second)
                addProperty("topRight_x", cardTopRightPoint.first)
                addProperty("topRight_y", cardTopRightPoint.second)
                addProperty("bottomRight_x", cardBottomRightPoint.first)
                addProperty("bottomRight_y", cardBottomRightPoint.second)
                addProperty("bottomLeft_x", cardBottomLeftPoint.first)
                addProperty("bottomLeft_y", cardBottomLeftPoint.second)
            }.toString()
        } else {
            ""
        }
    }

    fun updateUploadMetaData(metadata: ObjectMetadata): ObjectMetadata {
        return if (findersPoints.isNotEmpty() && alignmentPoint != null) {
            val qrMetadata = QRMetaData(findersPoints, alignmentPoint!!)
            val cardTopLeftPoint = qrMetadata.getTopLeftPoint()
            val cardTopRightPoint = qrMetadata.getTopRightPoint()
            val cardBottomRightPoint = qrMetadata.getBottomRightPoint()
            val cardBottomLeftPoint = qrMetadata.getBottomLeftPoint()

            metadata.apply {
                userMetadata["x-amz-meta-qr-topLeft"] =
                    "${cardTopLeftPoint.first},${cardTopLeftPoint.second}"
                userMetadata["x-amz-meta-qr-topRight"] =
                    "${cardTopRightPoint.first},${cardTopRightPoint.second}"
                userMetadata["x-amz-meta-qr-bottomRight"] =
                    "${cardBottomRightPoint.first},${cardBottomRightPoint.second}"
                userMetadata["x-amz-meta-qr-bottomLeft"] =
                    "${cardBottomLeftPoint.first},${cardBottomLeftPoint.second}"

                if (fiducialFoundTemplateMatches.size == FIDUCIAL_BOX_CORNERS_COUNT) {
                    val fiducialTopLeftPoint = applyScreenRatioOnImagePoint(
                        fiducialFoundTemplateMatches[0].x.toFloat(),
                        fiducialFoundTemplateMatches[0].y.toFloat(),
                        resizeScale
                    )
                    val fiducialTopRightPoint = applyScreenRatioOnImagePoint(
                        fiducialFoundTemplateMatches[1].x.toFloat(),
                        fiducialFoundTemplateMatches[1].y.toFloat(),
                        resizeScale
                    )
                    val fiducialBottomRightPoint = applyScreenRatioOnImagePoint(
                        fiducialFoundTemplateMatches[2].x.toFloat(),
                        fiducialFoundTemplateMatches[2].y.toFloat(),
                        resizeScale
                    )
                    val fiducialBottomLeftPoint = applyScreenRatioOnImagePoint(
                        fiducialFoundTemplateMatches[3].x.toFloat(),
                        fiducialFoundTemplateMatches[3].y.toFloat(),
                        resizeScale
                    )

                    userMetadata["x-amz-meta-fiducial-topLeft"] =
                        "${fiducialTopLeftPoint.first},${fiducialTopLeftPoint.second}"
                    userMetadata["x-amz-meta-fiducial-topRight"] =
                        "${fiducialTopRightPoint.first},${fiducialTopRightPoint.second}"
                    userMetadata["x-amz-meta-fiducial-bottomRight"] =
                        "${fiducialBottomRightPoint.first},${fiducialBottomRightPoint.second}"
                    userMetadata["x-amz-meta-fiducial-bottomLeft"] =
                        "${fiducialBottomLeftPoint.first},${fiducialBottomLeftPoint.second}"
                }
            }
        } else {
            metadata
        }
    }

    private fun applyScreenRatioOnImagePoint(
        pointX: Float,
        pointY: Float,
        resizeScale: Double = 1.toDouble()
    ): Pair<Float, Float> {
        val xWithRatio = pointX / (imageWidth.toFloat() * resizeScale).toFloat()
        val yWithRatio = pointY / (imageHeight.toFloat() * resizeScale).toFloat()
        return Pair(xWithRatio, yWithRatio)
    }

    fun setQrCodeMetadataResult(
        bitmap: Bitmap,
        qrCodeMetadataResult: Result?,
        foundTemplateMatches: MutableList<Rect>,
        scale: Double,
        resultPoints: List<ResultPoint>
    ) {
        imageWidth = bitmap.width
        imageHeight = bitmap.height
        if (fiducialFoundTemplateMatches.isNullOrEmpty() && foundTemplateMatches.isNotEmpty()) {
            fiducialFoundTemplateMatches = foundTemplateMatches
        }
        resizeScale = scale
        if (resultPoints.size == FIDUCIAL_BOX_CORNERS_COUNT && resultPoints.firstOrNull {
            it is AlignmentPattern
        } != null
        ) {
            extractMetaData(resultPoints)
        } else {
            qrCodeMetadataResult?.let { extractMetaData(it) }
        }
    }

    private fun extractMetaData(result: Result) {
        for (point in result.resultPoints) {
            if (point is FinderPattern) {
                val pointAfterRatio = applyScreenRatioOnImagePoint(point.x, point.y)
                findersPoints.add(pointAfterRatio)
            } else if (point is AlignmentPattern) {
                val pointAfterRatio = applyScreenRatioOnImagePoint(point.x, point.y)
                alignmentPoint = pointAfterRatio
            }
        }
    }

    private fun extractMetaData(resultPoints: List<ResultPoint>) {
        for (point in resultPoints) {
            if (point is FinderPattern) {
                val pointAfterRatio = applyScreenRatioOnImagePoint(point.x, point.y)
                findersPoints.add(pointAfterRatio)
            } else if (point is AlignmentPattern) {
                val pointAfterRatio = applyScreenRatioOnImagePoint(point.x, point.y)
                alignmentPoint = pointAfterRatio
            }
        }
    }

    companion object {
        private const val FIDUCIAL_BOX_CORNERS_COUNT = 4
    }
}

data class QRMetaData(
    var finders: MutableList<Pair<Float, Float>>,
    var alignment: Pair<Float, Float>
) {

    fun getTopLeftPoint(): Pair<Float, Float> {
        val findersSortedByYAxis = finders.sortedBy { it.second }
        return if (findersSortedByYAxis[0].first < findersSortedByYAxis[1].first) findersSortedByYAxis[0] else findersSortedByYAxis[1]
    }

    fun getBottomLeftPoint(): Pair<Float, Float> {
        val findersSortedByYAxis = finders.sortedBy { it.second }.reversed()
        return findersSortedByYAxis[0]
    }

    fun getTopRightPoint(): Pair<Float, Float> {
        val findersSortedByYAxis = finders.sortedBy { it.second }
        return if (findersSortedByYAxis[0].first > findersSortedByYAxis[1].first) findersSortedByYAxis[0] else findersSortedByYAxis[1]
    }

    fun getBottomRightPoint(): Pair<Float, Float> {
        return alignment
    }
}
