package com.vessel.app.nutritionsoptions

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.livechatinc.inappchat.ChatWindowActivity
import com.livechatinc.inappchat.ChatWindowConfiguration.*
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity

@AndroidEntryPoint
class NutritionOptionsFragment : BaseFragment<NutritionOptionsViewModel>() {
    override val viewModel: NutritionOptionsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_nutrition_options

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observe(viewModel.dismissScreen) {
            findNavController().navigateUp()
        }

        observe(viewModel.navigateTo) {
            findNavController().navigate(it)
        }
        observe(viewModel.openMessaging) {
            if (viewModel.isLiveChat()) {
                val intent = Intent(requireContext(), ChatWindowActivity::class.java)
                intent.putExtra(KEY_GROUP_ID, BuildConfig.NUTRITIONIST_COACH_GROUP)
                intent.putExtra(KEY_LICENCE_NUMBER, BuildConfig.LIVE_CHAT_LICENSE)
                intent.putExtra(KEY_VISITOR_NAME, viewModel.getUserName())
                intent.putExtra(KEY_VISITOR_EMAIL, viewModel.getUserEmail())
                requireContext().startActivity(intent)
            } else {
                MessagingActivity.builder()
                    .withToolbarTitle(getString(R.string.chat_to_nutritionist))
                    .withEngines(ChatEngine.engine())
                    .show(requireContext(), viewModel.getChatConfiguration())
            }
        }
    }
}
