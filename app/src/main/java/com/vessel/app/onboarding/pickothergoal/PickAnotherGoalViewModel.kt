package com.vessel.app.onboarding.pickothergoal

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.util.ResourceRepository

class PickAnotherGoalViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    val homeTabsManager: HomeTabsManager,
    private val contactManager: ContactManager,
    private val preferencesRepository: PreferencesRepository,
) : BaseViewModel(resourceProvider),
    ToolbarHandler {
    val bgColor = if (preferencesRepository.isExistingUserFirstTime) R.color.beige_color else R.color.primaryLight
    val state = PickAnotherGoalState()

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        state.navigateTo.value = PickAnotherGoalFragmentDirections.actionPickAnotherGoalFragmentToOneGoalSelectionFragment()
    }
}
