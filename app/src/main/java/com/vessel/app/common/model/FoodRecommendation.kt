package com.vessel.app.common.model

import com.vessel.app.common.model.data.Reagent
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.wellness.net.data.LikeStatus

data class FoodRecommendation(
    val usdaNdbNumber: Int,
    val name: String,
    val imageUrl: String,
    val goals: List<GoalRecord>?,
    val nutrients: List<Nutrient>,
    val servingSize: FoodRecommendationServingSize?,
    val foodId: Int? = null,
    val likeStatus: LikeStatus?,
    val totalLikes: Int? = null,
    val dislikes_count: Int? = null
)

data class Nutrient(
    val reagent: Reagent,
    val amount: Float
)

data class FoodRecommendationServingSize(
    val quantity: Float?,
    val display_quantity: String?,
    val unit: String?
)
