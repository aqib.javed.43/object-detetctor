package com.vessel.app.welcometeams

import androidx.navigation.NavDirections
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class WelcomeTeamState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val goal = LiveEvent<GoalRecord?>()
    operator fun invoke(block: WelcomeTeamState.() -> Unit) = apply(block)
}
