package com.vessel.app.helper

import android.content.Context
import android.database.Cursor
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import java.lang.ref.WeakReference
import javax.inject.Singleton

@Singleton
class AlbumCollection : LoaderManager.LoaderCallbacks<Cursor> {
    private var mContext: WeakReference<Context>? = null
    private var mLoaderManager: LoaderManager? = null
    private var mCallbacks: AlbumCallbacks? = null
    var currentSelection = 0
    private var mLoadFinished = false
    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        mLoadFinished = false
        return AlbumLoader.newInstance(mContext!!.get()!!)
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        val context = mContext!!.get() ?: return
        if (!mLoadFinished) {
            mLoadFinished = true
            mCallbacks!!.onAlbumLoad(data)
        }
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        val context = mContext!!.get() ?: return
        mCallbacks!!.onAlbumReset()
    }

    fun onCreate(activity: FragmentActivity, callbacks: AlbumCallbacks?) {
        mContext = WeakReference(activity)
        mLoaderManager = activity.supportLoaderManager
        mCallbacks = callbacks
    }

    fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            return
        }
        currentSelection = savedInstanceState.getInt(STATE_CURRENT_SELECTION)
    }

    fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(STATE_CURRENT_SELECTION, currentSelection)
    }

    fun onDestroy() {
        if (mLoaderManager != null) {
            mLoaderManager!!.destroyLoader(LOADER_ID)
        }
        mCallbacks = null
    }

    fun loadAlbums() {
        mLoaderManager!!.initLoader(LOADER_ID, null, this)
    }

    fun setStateCurrentSelection(currentSelection: Int) {
        this.currentSelection = currentSelection
    }

    interface AlbumCallbacks {
        fun onAlbumLoad(cursor: Cursor?)
        fun onAlbumReset()
    }

    companion object {
        private const val LOADER_ID = 1
        private const val STATE_CURRENT_SELECTION = "state_current_selection"
    }
}
