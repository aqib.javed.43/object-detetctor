package com.vessel.app.education.item

import com.vessel.app.base.MutableLiveArrayList
import com.vessel.app.education.ui.EducationPage
import com.vessel.app.util.LiveEvent

class EducationItemState(page: EducationPage) {
    val number = page.number
    val title = page.title
    val videoUrl = page.videoUrl
    val stepA = page.stepA
    val stepB = page.stepB
    val stepC = page.stepC
    val faqs = MutableLiveArrayList(page.faqs)
    val backEvent = LiveEvent<Unit>()
    val continueEvent = LiveEvent<Unit>()
    val launchLiveChatEvent = LiveEvent<Unit>()

    operator fun invoke(block: EducationItemState.() -> Unit) = apply(block)
}
