package com.vessel.app.photocapture.previewcaptureimage.ui

import android.os.Bundle
import androidx.activity.addCallback
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.photocapture.previewcaptureimage.PreviewCaptureImageViewModel
import com.vessel.app.taketest.TakeTestViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PreviewCaptureImageFragment : BaseFragment<PreviewCaptureImageViewModel>() {
    override val viewModel: PreviewCaptureImageViewModel by viewModels()
    override val layoutResId = R.layout.fragment_preview_capture_image

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewModel.uuid = takeTestViewModel.uploadTestFileModel?.uuid
        requireView().findViewById<AppCompatImageView>(R.id.previewCaptureImage)
            .setImageBitmap(takeTestViewModel.uploadTestFileModel?.rotatedImage)
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            showBackWarning(R.string.are_you_sure_to_back)
        }
    }

    companion object {
        const val ROTATE_DEGREES = 90f
    }

    private val takeTestViewModel by lazy {
        parentViewModel<TakeTestViewModel>()
    }
}
