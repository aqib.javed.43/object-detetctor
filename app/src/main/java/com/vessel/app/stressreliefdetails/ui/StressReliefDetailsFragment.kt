package com.vessel.app.stressreliefdetails.ui

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.adapter.genericlikeordislikeadapter.LikeOrDislikeAdapter
import com.vessel.app.common.adapter.impactgoals.ImpactGoalsAdapter
import com.vessel.app.common.adapter.impacttestresults.ImpactTestReagentAdapter
import com.vessel.app.common.adapter.planedit.PlanEditAdapter
import com.vessel.app.common.adapter.planedit.PlanEditItem
import com.vessel.app.common.adapter.planreminders.ReminderRowAdapter
import com.vessel.app.stressreliefdetails.StressReliefDetailsViewModel
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.PlanDeleteDialog
import dagger.hilt.android.AndroidEntryPoint

@SuppressLint("ResourceType")
@AndroidEntryPoint
class StressReliefDetailsFragment : BaseFragment<StressReliefDetailsViewModel>() {
    override val viewModel: StressReliefDetailsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_stress_relief_details
    private val headerAdapter by lazy { StressReliefHeaderAdapter(viewModel) }
    private val stressDetailsLikeOrDislikeAdapter by lazy {
        LikeOrDislikeAdapter(
            viewModel,
            R.layout.item_like_or_dislike_stress_relief_details
        )
    }
    private val reviewAdapter by lazy { StressReliefReviewAdapter(viewModel) }
    private val planEditAdapter by lazy { PlanEditAdapter(viewModel) }
    private val impactGoalsAdapter by lazy { ImpactGoalsAdapter(viewModel) }
    private val reminderRowAdapter by lazy { ReminderRowAdapter(viewModel) }
    private val impactTestReagentsAdapter by lazy { ImpactTestReagentAdapter(viewModel) }
    private val planDescriptionAdapter = object : BaseAdapterWithDiffUtil<String>() {
        override fun getLayout(position: Int) = R.layout.item_stress_relief_description
    }
    private val planImagesAdapter = object : BaseAdapterWithDiffUtil<String>() {
        override fun getLayout(position: Int) = R.layout.item_stress_relief_image
    }
    private val impactGoalsTitleAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_impact_goals_title
    }
    private val impactTestResultsHeaderTitleAdapter = object : BaseAdapterWithDiffUtil<Any>() {
        override fun getLayout(position: Int) = R.layout.item_impact_test_results_title
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
        setupAdapter()
    }

    private fun setupAdapter() {
        view?.findViewById<RecyclerView>(R.id.contentRecyclerView)?.apply {
            val detailsAdapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setIsolateViewTypes(false)
                    .build(),
                headerAdapter,
                planEditAdapter,
                reminderRowAdapter,
                impactTestResultsHeaderTitleAdapter,
                impactTestReagentsAdapter,
                impactGoalsTitleAdapter,
                impactGoalsAdapter,
                planDescriptionAdapter,
                planImagesAdapter,
                stressDetailsLikeOrDislikeAdapter,
                reviewAdapter
            )

            val stressDetailsSizeLookup = StressDetailsSpanSizeLookup(detailsAdapter)

            adapter = detailsAdapter
            layoutManager = GridLayoutManager(requireContext(), 2).apply {
                spanSizeLookup = stressDetailsSizeLookup
            }
            itemAnimator = null
        }
    }

    fun setupObservers() {
        viewModel.state {
            observe(header) {
                headerAdapter.submitList(listOf(it)) {
                    scrollToTop()
                }
                stressDetailsLikeOrDislikeAdapter.submitList(listOf(it))
            }
            observe(goalsItems) {
                impactGoalsAdapter.submitList(it) {
                    scrollToTop()
                }
            }
            observe(descriptionData) {
                planDescriptionAdapter.submitList(listOf(it))
                scrollToTop()
            }
            observe(imagesItems) {
                planImagesAdapter.submitList(it)
                scrollToTop()
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(reagentInfo) {
                impactTestReagentsAdapter.submitList(it) {
                    scrollToTop()
                }
            }
            observe(reminderItems) {
                if (it.isNotEmpty()) {
                    planEditAdapter.submitList(listOf(PlanEditItem(getString(R.string.schedule))))
                    reminderRowAdapter.submitList(it)
                } else {
                    planEditAdapter.submitList(listOf())
                    reminderRowAdapter.submitList(listOf())
                }
            }
            observe(viewModel.state.removeReminder) {
                PlanDeleteDialog.showDialog(
                    requireActivity(),
                    viewModel.state.header.value?.tittle.orEmpty(),
                    {
                        // Not used
                    },
                    {
                        viewModel.deletePlanItem()
                    },
                    {
                        viewModel.onDontShowAgainClicked(it)
                    }
                )
            }
            observe(reviews) { reviews ->
                reviewAdapter.submitList(reviews)
            }
        }

        impactGoalsTitleAdapter.submitList(listOf(Unit))
        impactTestResultsHeaderTitleAdapter.submitList(listOf(Unit))
    }

    private fun scrollToTop() {
        view?.findViewById<RecyclerView>(R.id.contentRecyclerView)?.scrollToPosition(0)
    }
}
