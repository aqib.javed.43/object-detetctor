package com.vessel.app.common.net.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SupplementAssociatedGoalData(
    val id: Int,
    val name: String,
    val impact: Int?
) : Parcelable

// "" > 0,1,2,3,4,5,6
// 0,1,2,3
//
//
//
//
//    0
//    1
//    2
//    3
//
//
//    0
//    1
//    2
//    3
//    4
//    5
//    6
