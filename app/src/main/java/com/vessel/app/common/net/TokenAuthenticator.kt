package com.vessel.app.common.net

import com.vessel.app.common.manager.AuthManager
import kotlinx.coroutines.runBlocking
import okhttp3.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TokenAuthenticator @Inject constructor() : Authenticator, Interceptor {

    companion object {
        const val AUTHORIZATION_HEADER = "Authorization"
        const val BEARER_TOKEN = "Bearer"
    }

    private lateinit var authManager: AuthManager

    fun setAuthManager(authManager: AuthManager) {
        this.authManager = authManager
    }

    override fun authenticate(route: Route?, response: Response): Request? {
        var request: Request? = null
        runBlocking { authManager.refreshToken() }
            .onSuccess {
                request = response.request.setAuthenticationHeaders()
            }
            .onError {
                request = null
            }

        return request
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(chain.request().setAuthenticationHeaders())
    }

    private fun Request.setAuthenticationHeaders() = newBuilder()
        .removeHeader(AUTHORIZATION_HEADER)
        .apply {
            authManager.getAuthToken().accessToken?.let {
                addHeader(AUTHORIZATION_HEADER, "$BEARER_TOKEN $it")
            }
        }
        .build()
}
