package com.vessel.app.filterfoodplandialog.model

data class FilterHeaderItem(val title: String)
