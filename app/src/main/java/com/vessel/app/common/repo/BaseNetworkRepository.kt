package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.ErrorMessage
import com.vessel.app.common.model.ErrorMessageItem
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Result.*
import com.vessel.app.common.net.data.ErrorData
import com.vessel.app.common.net.data.ErrorMessageItemData
import retrofit2.HttpException
import java.io.IOException
import java.net.UnknownHostException

abstract class BaseNetworkRepository(
    protected val moshi: Moshi,
    private val loggingManager: LoggingManager
) {
    protected suspend fun <T> execute(apiCall: suspend () -> T): Result<T, Nothing> =
        execute({ it }, apiCall)

    protected suspend fun <R, T> execute(
        map: (R) -> T,
        apiCall: suspend () -> R
    ): Result<T, Nothing> =
        try {
            Success(map(apiCall()))
        } catch (throwable: Throwable) {
            logStep("line 35" + throwable.stackTrace)
            logError(apiCall, throwable)
            when (throwable) {
                is UnknownHostException -> UnknownError(throwable)
                is HttpException -> HttpError(throwable.code())
                is IOException -> NetworkIOError(throwable.cause ?: throwable)
                else -> UnknownError(throwable)
            }
        }

    protected suspend fun <T> executeBlock(block: suspend () -> Result<T, Nothing>): Result<T, Nothing> =
        try {
            block.invoke()
        } catch (throwable: Throwable) {
            logStep("line 54" + throwable.stackTrace)
            logError(block, throwable)
            when (throwable) {
                is UnknownHostException -> UnknownError(throwable)
                is HttpException -> HttpError(throwable.code())
                is IOException -> NetworkIOError(throwable.cause ?: throwable)
                else -> UnknownError(throwable)
            }
        }

    protected suspend fun <R, T> execute(
        map: (R) -> T,
        apiCall: suspend () -> R,
        also: (T) -> Unit
    ): Result<T, Nothing> =
        try {
            val data = map(apiCall())
            also.invoke(data)
            Success(data)
        } catch (throwable: Throwable) {
            logStep("line 79" + throwable.stackTrace)
            logError(apiCall, throwable)
            when (throwable) {
                is UnknownHostException -> UnknownError(throwable)
                is HttpException -> HttpError(throwable.code())
                is IOException -> NetworkIOError(throwable.cause ?: throwable)
                else -> UnknownError(throwable)
            }
        }

    private fun logStep(step: String) {
        loggingManager.addToLog(step)
    }

    protected suspend fun <R> executeWithCallback(
        apiCall: suspend () -> R,
        also: (R) -> Unit
    ): Result<R, Nothing> =
        try {
            val data = apiCall()
            also.invoke(data)
            Success(data)
        } catch (throwable: Throwable) {
            logStep("line 114" + throwable.stackTrace)
            logError(apiCall, throwable)
            when (throwable) {
                is UnknownHostException -> UnknownError(throwable)
                is HttpException -> HttpError(throwable.code())
                is IOException -> NetworkIOError(throwable.cause ?: throwable)
                else -> UnknownError(throwable)
            }
        }

    private fun <R> logError(apiCall: suspend () -> R, throwable: Throwable) {
        loggingManager.addToLog("Error trying to make network call: $apiCall")
        loggingManager.logError(throwable)
    }

    protected suspend fun <T> executeWithErrorMessage(apiCall: suspend () -> T): Result<T, ErrorMessage> =
        executeWithErrorMessage({ it }, apiCall)

    protected suspend fun <R, T> executeWithErrorMessage(
        map: (R) -> T,
        apiCall: suspend () -> R
    ): Result<T, ErrorMessage> =
        try {
            Success(map(apiCall()))
        } catch (throwable: Throwable) {
            logStep("line 158" + throwable.stackTrace)
            logError(apiCall, throwable)
            when (throwable) {
                is UnknownHostException -> UnknownError(throwable)
                is HttpException -> ServiceError(
                    throwable.code(),
                    mapError(throwable.convertErrorBody(ErrorData::class.java))
                )
                is IOException -> NetworkIOError(throwable)
                else -> UnknownError(throwable)
            }
        }

    private fun <E> HttpException.convertErrorBody(clazz: Class<E>): E? =
        try {
            response()?.errorBody()?.source()?.let {
                moshi.adapter(clazz).fromJson(it)
            }
        } catch (exception: Exception) {
            null
        }

    private fun mapError(data: ErrorData?) = ErrorMessage(
        latest_sample_uuid = data?.latest_sample_uuid,
        message = data?.message,
        errors = data?.errors?.map { mapErrorMessageItem(it) }
    )

    private fun mapErrorMessageItem(data: ErrorMessageItemData?) = ErrorMessageItem(
        code = data?.code,
        label = data?.label
    )
}
