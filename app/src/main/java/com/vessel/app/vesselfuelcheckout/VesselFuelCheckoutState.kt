package com.vessel.app.vesselfuelcheckout

import androidx.lifecycle.MutableLiveData
import com.vessel.app.util.LiveEvent
import com.vessel.app.vesselfuelcheckout.model.VesselFuelItem
import com.vessel.app.vesselfuelcheckout.model.VesselFuelItemUiModel
import com.vessel.app.vesselfuelcheckout.model.toVesselFuelItem
import javax.inject.Inject

class VesselFuelCheckoutState @Inject constructor(val fuelItems: VesselFuelItem) {
    val vesselFuelItems = MutableLiveData(fuelItems.items.map { it.toVesselFuelItem() })
    val checkoutSuccessEvent = LiveEvent<Any>()
    val cancelCheckoutClicked = LiveEvent<Any>()
    val navigateToWeb = LiveEvent<String>()
    val addressData = LiveEvent<String>()
    val paymentItems = LiveEvent<List<VesselFuelItemUiModel>>()
    val paymentOptionTitle = MutableLiveData<String>()
    val paymentOptionIcon = MutableLiveData<Int>()
    val checkoutUrl = fuelItems.checkoutUrl
    val pricePerDay = fuelItems.pricePerDay
    val totalPrice = fuelItems.totalPrice
    operator fun invoke(block: VesselFuelCheckoutState.() -> Unit) = apply(block)
}
