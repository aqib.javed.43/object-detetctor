package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.net.mapper.SurveyMapper
import com.vessel.app.common.net.mapper.SurveyResponseMapper
import com.vessel.app.common.net.service.SurveyService
import com.vessel.app.takesurvey.model.SurveyResponseAnswer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SurveyRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val surveyMapper: SurveyMapper,
    private val surveyResponseMapper: SurveyResponseMapper,
    private val surveyService: SurveyService
) : BaseNetworkRepository(moshi, loggingManager) {

    suspend fun getSurveyById(surveyId: Long) = withContext(Dispatchers.IO) {
        executeWithErrorMessage(surveyMapper::map) { surveyService.getSurveyById(surveyId) }
    }

    suspend fun submitSurveyResponse(surveyUUID: String, answers: List<SurveyResponseAnswer>) =
        execute {
            surveyService.submitSurveyResponse(
                surveyUUID,
                surveyResponseMapper.map(answers)
            )
        }
    suspend fun getAnswerById(questionId: Long) = withContext(Dispatchers.IO) {
        executeWithErrorMessage(surveyMapper::map) { surveyService.getAnswerById(questionId).copy(questionId = questionId.toInt()) }
    }
}
