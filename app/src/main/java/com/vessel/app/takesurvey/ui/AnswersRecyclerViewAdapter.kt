package com.vessel.app.takesurvey.ui

import androidx.annotation.LayoutRes
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.takesurvey.model.SurveyAnswer

class AnswersRecyclerViewAdapter(
    val handler: QuestionRecyclerViewAdapter.QuestionClickHandler,
    @LayoutRes val layoutResourceId: Int
) :
    BaseAdapterWithDiffUtil<SurveyAnswer>() {
    override fun getLayout(position: Int) = layoutResourceId
    override fun getHandler(position: Int) = handler
}
