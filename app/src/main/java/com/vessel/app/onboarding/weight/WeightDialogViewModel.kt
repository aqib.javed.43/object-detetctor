package com.vessel.app.onboarding.weight

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.onboarding.weight.WeightDialogFragment.Companion.WEIGHT_KEY
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import kotlin.math.floor

class WeightDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    @Assisted savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    val weight = savedStateHandle[WEIGHT_KEY] ?: 0f
    val dismissDialog = LiveEvent<Unit>()
    val weightErrorEvent = LiveEvent<Any>()
    val weightSet = LiveEvent<Float>()
    var hundreds: Int = floor(weight / 100).toInt()
    var tens: Int = floor(weight % 100 / 10).toInt()
    var units: Int = (weight % 10).toInt()

    fun onCancelClicked() {
        dismissDialog.call()
    }

    fun onSetClicked() {
        if (units == 0 && tens == 0 && hundreds == 0) {
            weightErrorEvent.call()
        } else {
            weightSet.value = (units + tens * 10 + hundreds * 100).toFloat()
            dismissDialog.call()
        }
    }

    fun onHundredsSelected(hundredsValue: Int) {
        hundreds = hundredsValue
    }

    fun onTensSelected(tensValue: Int) {
        tens = tensValue
    }

    fun onUnitsSelected(unitsValue: Int) {
        units = unitsValue
    }
}
