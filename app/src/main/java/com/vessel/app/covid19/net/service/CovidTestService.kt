package com.vessel.app.covid19.net.service

import com.vessel.app.covid19.net.data.CovidTestNet
import retrofit2.http.GET
import retrofit2.http.Path

interface CovidTestService {
    @GET("diagnosis-test/status")
    suspend fun getCovidTests(): List<CovidTestNet>

    @GET("diagnosis-test/{testId}")
    suspend fun isTestProcessed(@Path("testId") testId: String)
}
