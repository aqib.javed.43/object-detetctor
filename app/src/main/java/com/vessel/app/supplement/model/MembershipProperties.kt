package com.vessel.app.supplement.model

data class MembershipProperties(
    val name: String,
    val value: String
)
