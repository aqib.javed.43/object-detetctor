package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.ReagentResponseData
import retrofit2.http.*

interface ReagentService {
    @GET("reagent")
    suspend fun getReagents(): ReagentResponseData
}
