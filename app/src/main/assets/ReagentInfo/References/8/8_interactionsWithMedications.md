Taking synthetic corticosteroid medications can suppress your body's natural adrenal cortisol production and reduce your free cortisol test result making it less reliable. When you have been on these medications for a long time stopping suddenly can cause dangerously low cortisol levels.
    -   * prednisone (Prednisone Intensol)
    -   * prednisolone (Orapred, Prelone)
    -   * triamcinolone (Aristospan Intra-Articular, Aristospan Intralesional, Kenalog)
    -   * methylprednisolone (Medrol, Depo-Medrol, Solu-Medrol)
    -   * dexamethasone (Dexamethasone Intensol, DexPak 10 Day, DexPak 13 Day, DexPak 6 Day).

-   Estrogens (birth control or hormone replacement therapy) can sometimes increase cortisol levels.
-   https://www.ncbi.nlm.nih.gov/pubmed?term=7883828
-   https://www.ncbi.nlm.nih.gov/pubmed?term=7021007
-   https://www.ncbi.nlm.nih.gov/pubmed?term=3560936
-   https://www.ncbi.nlm.nih.gov/pubmed?term=4284083
