package com.vessel.app.planreminderdialog.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.planreminderdialog.model.AddNewReminderDialogHeader

class AddPlanReminderAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        AddNewReminderDialogHeader -> R.layout.item_plan_reminder_header
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any? = handler

    interface OnActionHandler {
        fun onAddClicked()
    }
}
