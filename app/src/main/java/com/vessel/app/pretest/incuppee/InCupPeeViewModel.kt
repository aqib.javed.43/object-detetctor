package com.vessel.app.pretest.incuppee

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.pretest.incuppee.ui.InCupPeeFragmentDirections
import com.vessel.app.util.ResourceRepository

class InCupPeeViewModel @ViewModelInject constructor(
    val state: InCupPeeState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider), ToolbarHandler {

    fun onContinueClicked() {
        state.navigateTo.value = InCupPeeFragmentDirections.actionInCupPeeFragmentToTakeTestActivity()
    }

    fun dontShowChecked(dontShowAgain: Boolean) {
        preferencesRepository.skipPreTestTips = dontShowAgain
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
}
