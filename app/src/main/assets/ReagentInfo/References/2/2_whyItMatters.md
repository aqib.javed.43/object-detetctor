Water is the single largest constituent of the human body, making up 60% - 75% of the adult and child human bodies, respectively. Maintaining optimal hydration throughout the day is important for many bodily functions, including mood, brain function, gastrointestinal function, energy levels, and even weight loss! Studies have found that up to 85% of the US population is dehydrated.

Specific gravity, in combination with other urinary findings, is a way to get important information about your organ function and general hydration levels.

Your urine contains water, minerals, and toxins or waste products that have been filtered by the kidneys. The specific gravity gives you a measure of how much stuff vs. water is in your urine.

Pure water has a specific gravity of 1.000. It is normal and healthy to have some minerals and waste in your urine, so the normal range is above 1.000 at 1.010 - 1.035. Values higher than 1.035 indicate there is a lot of dissolved minerals and waste in your urine and you are dehydrated. If you have been drinking too much water, your specific gravity will be close to 1.000.

**references:**

-   https://www.ncbi.nlm.nih.gov/pubmed/26271221

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6356561/#B42-nutrients-11-00070

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6356561/

-   [Kleiner SM](https://www.ncbi.nlm.nih.gov/pubmed/?term=Kleiner%20SM%5BAuthor%5D&cauthor=true&cauthor_uid=9972188)1. Water: an essential but overlooked nutrient. [J Am Diet Assoc.](https://www.ncbi.nlm.nih.gov/pubmed/9972188#) 1999 Feb;99(2):200-6. PMID 9972188

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5451964/

-   Cooper GM. The Cell: A Molecular Approach. 2nd edition. Sunderland (MA): Sinauer Associates; 2000. The Molecular Composition of Cells. Available from: https://www.ncbi.nlm.nih.gov/books/NBK9879/

-   Flasar, C. (2008). What is urine specific gravity? Nursing, 38(7), 14. https://doi.org/10.1097/01.nurse.0000325315.41513.a0
