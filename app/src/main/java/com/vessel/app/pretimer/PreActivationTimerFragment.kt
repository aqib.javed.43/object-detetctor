package com.vessel.app.pretimer

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.activationtimer.util.TimerServiceManager
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PreActivationTimerFragment : BaseFragment<PreActivationTimerViewModel>() {

    override val viewModel: PreActivationTimerViewModel by viewModels()
    override val layoutResId = R.layout.fragment_pre_activation_timer

    override fun onViewLoad(savedInstanceState: Bundle?) {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(showBackWarning) {
                showBackWarning(R.string.are_you_sure_to_back)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            viewModel.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        setupTimerHelper()
    }

    private fun setupTimerHelper() {
        TimerServiceManager.setHandler(viewModel)
        TimerServiceManager.stopForeground()
    }
}
