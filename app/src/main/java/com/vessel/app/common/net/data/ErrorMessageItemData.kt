package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ErrorMessageItemData(
    val code: Int?,
    val label: String?
)
