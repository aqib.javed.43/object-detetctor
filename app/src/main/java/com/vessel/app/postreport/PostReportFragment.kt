package com.vessel.app.postreport

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.teampage.model.TeamPostItem
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_flag_post.*

@AndroidEntryPoint
class PostReportFragment : BaseFragment<PostReportViewModel>() {
    override val viewModel: PostReportViewModel by viewModels()
    override val layoutResId = R.layout.fragment_flag_post

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        observeValues()
        setupBackPressedDispatcher()
    }

    private fun setupViews() {
        val postItem = arguments?.getSerializable(Constants.POST_ITEM) as TeamPostItem
        btnReport.setOnClickListener {
            val reason = edtIssue.text.toString()
            viewModel.flagPost(postItem, reason, it)
        }
    }

    private fun observeValues() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(remainingTextCount) {
                reportTextCount.text = String.format(getString(R.string.remaining_characters), it)
            }
        }
    }

    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.let { viewModel.onBackButtonClicked(it) }
        }
    }
}
