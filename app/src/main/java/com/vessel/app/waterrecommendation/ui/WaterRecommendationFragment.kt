package com.vessel.app.waterrecommendation.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.home.HomeActivity
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WaterRecommendationFragment : BaseFragment<WaterRecommendationViewModel>() {
    override val viewModel: WaterRecommendationViewModel by viewModels()
    override val layoutResId = R.layout.fragment_water_recommendation

    val waterRecommendationSelectAdapter by lazy { WaterRecommendationSelectAdapter(viewModel) }
    private lateinit var backButton: View
    override fun onViewLoad(savedInstanceState: Bundle?) {
        backButton = requireView().findViewById(R.id.toolbar)
        val recommendList = requireView().findViewById<RecyclerView>(R.id.recommendList)
        recommendList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recommendList.adapter = waterRecommendationSelectAdapter
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(navigateToHome) {
                navigateToHomeActivity()
            }
            observe(items) {
                waterRecommendationSelectAdapter.submitList(it)
            }
        }
    }

    private fun navigateToHomeActivity() {
        val intent = Intent(requireContext(), HomeActivity::class.java)
        context?.startActivity(intent)
        activity?.finishAffinity()
    }
}
