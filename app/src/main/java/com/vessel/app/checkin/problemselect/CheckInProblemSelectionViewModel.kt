package com.vessel.app.checkin.problemselect

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.GoalsManager
import com.vessel.app.common.manager.SubGoalManager
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.onboarding.problemselection.model.ProblemSelect
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class CheckInProblemSelectionViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val goalsRepository: GoalsRepository,
    private val subGoalsManager: SubGoalManager,
    private val goalManager: GoalsManager,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider),
    CheckInProblemSelectAdapter.OnActionHandler,
    ToolbarHandler {

    var subGoalList: List<ProblemSelect>? = null
    val state = CheckInProblemSelectState()
    var goalRepo: String = ""
    var subTitle: String = ""
    init {
        val goalSelected = goalsRepository.getUserMainGoal()
        goalSelected?.let {
            setGoal(it)
        }
    }
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    private fun fetchAllSubGoals(goalId: Int?) {
        viewModelScope.launch {
            showLoading()
            subGoalsManager.fetchAllSubGoals(goalId)
                .onSuccess { subGoalData ->
                    state {
                        items.value = subGoalData.map {
                            ProblemSelect(
                                it.id,
                                it.name,
                                false
                            )
                        }
                    }
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    fun onDoneClicked() {
        subGoalList = state.items.value.orEmpty()
            .filter { it.checked }
        if (subGoalList!!.isNotEmpty()) {
            var data = ""
            subGoalList!!.forEach {
                data = if (data.isEmpty()) {
                    it.id.toString()
                } else {
                    data + "," + it.id.toString()
                }
            }
            goalsRepository.setUserSubGoal(data)
        }
        state.navigateTo.value = HomeNavGraphDirections.actionGlobalCheckInHabitSelectionFragment()
    }
    fun onSkip(v: View) {
        Navigation.findNavController(v).popBackStack(R.id.homeScreenFragment, false)
    }
    fun setGoal(goal: GoalSample) {
        when (savedStateHandle.get<String>("feeling")) {
            FEELING_MEH -> {
                goalRepo = "What problems are you having with your " + goal.name + "?"
                subTitle = "Don't worry, together we'll fix em!"
            }
            FEELING_UNHAPPY -> {
                goalRepo = "Let's turn that frown upside down"
                subTitle = String.format(getResString(R.string.meh_sub), goal.name)
            }
        }
        fetchAllSubGoals(goal.id)
        Constants.GOALBUTTONS.firstOrNull {
            it.id == goal.id
        }?.let {
            state.selectedGoal.value = it
        }
    }
    override fun onGoalSelectClicked(items: List<ProblemSelect>) {
        subGoalList = items
    }
    companion object {
        const val FEELING_NOTHING = ""
        const val FEELING_UNHAPPY = "unhappy"
        const val FEELING_MEH = "meh"
        const val FEELING_HAPPY = "happy"
    }
}
