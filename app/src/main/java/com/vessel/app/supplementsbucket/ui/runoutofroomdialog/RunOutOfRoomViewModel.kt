package com.vessel.app.supplementsbucket.ui.runoutofroomdialog

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class RunOutOfRoomViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()

    fun onCloseDialogClicked() {
        dismissDialog.call()
    }
}
