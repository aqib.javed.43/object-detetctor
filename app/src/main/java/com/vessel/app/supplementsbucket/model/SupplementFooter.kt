package com.vessel.app.supplementsbucket.model

data class SupplementFooter(
    val totalPrice: Double
)
