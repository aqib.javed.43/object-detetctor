package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.net.payload.SearchFilterPayload
import com.vessel.app.common.net.service.FoodService
import com.vessel.app.filterfoodplandialog.model.SortOptions
import com.vessel.app.foodplan.model.FoodSearchMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FoodRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val foodService: FoodService,
    private val foodSearchMapper: FoodSearchMapper
) : BaseNetworkRepository(moshi, loggingManager) {
    suspend fun searchFilterFood(
        payload: SearchFilterPayload,
        sortOption: SortOptions,
    ) = executeWithErrorMessage(foodSearchMapper::map) {
        foodService.searchFilterFood(payload, sortOption.apiName, sortOption.isAscending)
    }

    suspend fun getFoodItems() = executeWithErrorMessage(foodSearchMapper::map) {
        foodService.getFoodItems()
    }
}
