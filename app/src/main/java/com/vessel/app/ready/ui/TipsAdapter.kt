package com.vessel.app.ready.ui

import android.text.method.LinkMovementMethod
import androidx.appcompat.widget.AppCompatTextView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class TipsAdapter() : BaseAdapterWithDiffUtil<Tip>() {
    override fun getLayout(position: Int) = R.layout.item_tip

    override fun onBindViewHolder(holder: BaseViewHolder<Tip>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.findViewById<AppCompatTextView>(R.id.tipText).movementMethod =
            LinkMovementMethod.getInstance()
    }
}
