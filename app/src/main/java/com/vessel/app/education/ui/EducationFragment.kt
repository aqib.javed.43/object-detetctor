package com.vessel.app.education.ui

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.education.EducationViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_education.*

@AndroidEntryPoint
class EducationFragment : BaseFragment<EducationViewModel>() {
    override val viewModel: EducationViewModel by viewModels()
    override val layoutResId = R.layout.fragment_education

    private val onPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            viewModel.onPageChanged(position)
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViewPager()
        observeValues()
        setupBackPressedDispatcher()
    }

    private fun setupViewPager() {
        instructionsViewPager.adapter = EducationAdapter(requireActivity(), viewModel.state.pages)
        instructionsViewPager.registerOnPageChangeCallback(onPageChangeCallback)
    }

    private fun observeValues() {
        viewModel.state {
            observe(goToNextPage) { instructionsViewPager.currentItem += 1 }
            observe(goToPreviousPage) { instructionsViewPager.currentItem -= 1 }
        }
    }

    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            onBackClicked()
        }
    }

    fun onBackClicked() {
        viewModel.onBackClicked()
    }

    fun onContinueClicked() {
        view?.let { viewModel.onContinueClicked(it) }
    }

    override fun onDestroyView() {
        instructionsViewPager.unregisterOnPageChangeCallback(onPageChangeCallback)
        super.onDestroyView()
    }
}
