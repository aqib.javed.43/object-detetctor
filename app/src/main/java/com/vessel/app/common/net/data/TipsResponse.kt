package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TipsResponse(
    val tips: List<TipData>,
    val data: List<TipSortingData>,
    val foods: List<Food>,
    val lifestyle: List<LifestyleData>,
    val pagination: PagingData
)
