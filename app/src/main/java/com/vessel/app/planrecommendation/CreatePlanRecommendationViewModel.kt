package com.vessel.app.planrecommendation

import android.os.CountDownTimer
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Contact
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Sample
import com.vessel.app.common.model.data.Reagent
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.model.data.ReagentLevel
import com.vessel.app.common.net.data.plan.BuildPlanRequest
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.ReagentLifestyleRecommendation
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.planrecommendation.ui.CreatePlanRecommendationFragmentDirections
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class CreatePlanRecommendationViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val scoreManager: ScoreManager,
    private val contactManager: ContactManager,
    private val reagentsManager: ReagentsManager,
    private val planManager: PlanManager,
    private val trackingManager: TrackingManager,
    private val buildPlanManager: BuildPlanManager,
    private val recommendationManager: RecommendationManager,
    private val preferencesRepository: PreferencesRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val goalsRepository: GoalsRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val homeTabsManager: HomeTabsManager,
    private val programManager: ProgramManager
) : BaseViewModel(resourceProvider) {
    private var supplements: List<ReagentItem> = arrayListOf()
    val state = CreatePlanRecommendationState(savedStateHandle.get("recommendationType")!!)
    var contact: Contact? = null
    var hasFoodRecommendation = false
    var hasStressRecommendation = false
    var hasHydrationRecommendation = false
    var hasSupplementRecommendation = false
    var isNavigated = false
    private lateinit var testingPlan: Sample
    private var hasTestPlan: Boolean = false

    init {
        showLoading()
        getCurrentPlan()
        loadPrograms()
        logEvent(TrackingConstants.PLAN_BUILDER_STARTED)
        loadPlans()
        loadContactInformation()
        loadTestingPlan()
    }

    private fun loadPrograms() {
        viewModelScope.launch {
            programManager.getEnrolledPrograms(true).onSuccess {
                if (it.isNotEmpty()) goalsRepository.setUserHasEnrolledProgram()
            }.onServiceError {
                showError(it.error.message ?: getResString(R.string.unknown_error))
            }.onNetworkIOError {
                showError(getResString(R.string.network_error))
            }.onUnknownError {
                showError(getResString(R.string.unknown_error))
            }.onError {
                hideLoading(false)
            }
        }
    }

    private fun loadPlans() {
        viewModelScope.launch {
            hasFoodRecommendation = buildPlanManager.hasFoodPlanRecommendations()
            hasStressRecommendation = buildPlanManager.hasStressRecommendations()
            hasHydrationRecommendation = buildPlanManager.hasHydrationRecommendation()
            hasSupplementRecommendation = buildPlanManager.getSupplementRecommendations().isNotEmpty()
            supplements = buildPlanManager.getSupplementRecommendations()
            loadScores()
        }
    }

    private fun loadContactInformation() {
        contact = preferencesRepository.getContact()
    }

    private fun showUpdateAutoPlanFlow() {
        var duration = 3L
        duration += if (hasFoodRecommendation) 5 else 0
        duration += if (hasStressRecommendation) 5 else 0
        duration += if (hasHydrationRecommendation) 5 else 0
        duration += if (hasSupplementRecommendation) 5 else 0
        val percentage = when {
            duration % 5 == 4L -> {
                20
            }
            duration % 5 == 3L -> {
                33
            }
            duration % 5 == 2L -> {
                50
            }
            else -> {
                67
            }
        }
        if (state.currentPlanType == RecommendationType.Tip) {
            val totalDuration = TimeUnit.SECONDS.toMillis(duration)
            val interval = TimeUnit.SECONDS.toMillis(1)
            object : CountDownTimer(totalDuration, interval) {
                override fun onTick(millisUntilFinished: Long) {
                    val progress =
                        100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                    val step = state.planRecommendations.first { it.type == RecommendationType.Tip }
                    state.planRecommendationItem.value = step
                    state.progress.value = progress
                    if (progress >= percentage) {
                        goToGoals()
                    }
                }

                override fun onFinish() {
                    goToGoals()
                }
            }.start()
        } else if (hasFoodRecommendation && state.currentPlanType == RecommendationType.Food) {
            val totalDuration = TimeUnit.SECONDS.toMillis(duration)
            val interval = TimeUnit.SECONDS.toMillis(1)
            object : CountDownTimer(totalDuration, interval) {
                override fun onTick(millisUntilFinished: Long) {
                    val progress =
                        100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                    val step =
                        state.planRecommendations.first { it.type == RecommendationType.Food }
                    state.planRecommendationItem.value = step
                    state.progress.value = progress
                    if (progress > percentage) {
                        goToDietsOrFood((hasHydrationRecommendation || hasStressRecommendation).not())
                    }
                }

                override fun onFinish() {
                    goToDietsOrFood((hasHydrationRecommendation || hasStressRecommendation).not())
                }
            }.start()
        } else if (hasSupplementRecommendation && state.currentPlanType == RecommendationType.Supplement) {
            val totalDuration = TimeUnit.SECONDS.toMillis(duration)
            addSupplementRecommendation()
            val interval = TimeUnit.SECONDS.toMillis(1)
            object : CountDownTimer(totalDuration, interval) {
                override fun onTick(millisUntilFinished: Long) {
                    val progress =
                        100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                    val step =
                        state.planRecommendations.first { it.type == RecommendationType.Supplement }
                    state.planRecommendationItem.value = step

                    state.progress.value =
                        if (hasStressRecommendation && hasFoodRecommendation) progress + percentage
                        else if (hasStressRecommendation || hasFoodRecommendation) progress + percentage
                        else progress
                    if (progress > percentage) {
                        if (hasStressRecommendation) {
                            preferencesRepository.isAddedSupplementRecommendation = true
                            state.currentPlanType = RecommendationType.Lifestyle
                            showUpdateAutoPlanFlow()
                        } else {
                            goToLifestyle(hasHydrationRecommendation.not())
                        }
                    }
                }

                override fun onFinish() {
                    if (hasStressRecommendation) {
                        preferencesRepository.isAddedSupplementRecommendation = true
                        state.currentPlanType = RecommendationType.Lifestyle
                        showUpdateAutoPlanFlow()
                    } else {
                        goToLifestyle(hasHydrationRecommendation.not())
                    }
                }
            }.start()
        } else if (hasStressRecommendation && (state.currentPlanType == RecommendationType.Food || state.currentPlanType == RecommendationType.Lifestyle || state.currentPlanType == RecommendationType.Supplement)) {
            val totalDuration = TimeUnit.SECONDS.toMillis(duration)
            val interval = TimeUnit.SECONDS.toMillis(1)
            object : CountDownTimer(totalDuration, interval) {
                override fun onTick(millisUntilFinished: Long) {
                    val progress =
                        100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                    val step =
                        state.planRecommendations.first { it.type == RecommendationType.Lifestyle }
                    state.planRecommendationItem.value = step
                    state.progress.value =
                        if (hasStressRecommendation && hasFoodRecommendation) progress + percentage
                        else if (hasStressRecommendation || hasFoodRecommendation) progress + percentage
                        else progress

                    if (progress > percentage) {
                        goToLifestyle(hasHydrationRecommendation.not())
                    }
                }

                override fun onFinish() {
                    goToLifestyle(hasHydrationRecommendation.not())
                }
            }.start()
        } else if (hasHydrationRecommendation && (state.currentPlanType == RecommendationType.Food || state.currentPlanType == RecommendationType.Lifestyle || state.currentPlanType == RecommendationType.Hydration || state.currentPlanType == RecommendationType.Supplement)) {
            val totalDuration = TimeUnit.SECONDS.toMillis(duration)
            addHydrationRecommendation()
            val interval = TimeUnit.SECONDS.toMillis(1)
            object : CountDownTimer(totalDuration, interval) {
                override fun onTick(millisUntilFinished: Long) {
                    val progress =
                        100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                    val step =
                        state.planRecommendations.first { it.type == RecommendationType.Hydration }
                    state.planRecommendationItem.value = step

                    state.progress.value =
                        if (hasStressRecommendation && hasFoodRecommendation && hasSupplementRecommendation) progress + (100 - percentage)
                        else if (hasStressRecommendation || hasFoodRecommendation || hasSupplementRecommendation) progress + (100 - percentage)
                        else progress
                    if (progress > percentage && !isNavigated) {
                        preferencesRepository.isAddedHydrationRecommendation = true
                        endTheFlow()
                    }
                }

                override fun onFinish() {
                    preferencesRepository.isAddedHydrationRecommendation = true
                    endTheFlow()
                }
            }.start()
        } else {
            if (!isNavigated) {
                endTheFlow()
            }
        }
    }

    private fun endTheFlow() {
        isNavigated = true
        devicePreferencesRepository.showPlanUpdatedDialog = false
        planManager.clearCachedData()
        recommendationManager.updateLatestSampleFoodRecommendationsCachedData()
        recommendationManager.updateLatestSampleRecommendationsCachedData()
        logEvent(TrackingConstants.PLAN_BUILDER_COMPLETE)
        homeTabsManager.clear()
        goToNextStep()
    }

    private fun goToLifestyle(isLastStep: Boolean) {
        state.navigateTo.value =
            CreatePlanRecommendationFragmentDirections.actionCreatePlanRecommendationFragmentToOnboardingLifestyleFragment(
                isLastStep,
                true
            )
    }

    private fun goToDietsOrFood(isLastStep: Boolean) {
        if (preferencesRepository.getContact()?.diets.isNullOrEmpty() || preferencesRepository.getContact()?.allergies.isNullOrEmpty()) {
            state.navigateTo.value =
                CreatePlanRecommendationFragmentDirections.actionCreatePlanRecommendationFragmentToOnboardingDietsFragment(
                    true,
                    isLastStep,
                    true
                )
        } else {
            state.navigateTo.value =
                CreatePlanRecommendationFragmentDirections.actionCreatePlanRecommendationFragmentToOnboardingFoodFragment(
                    isLastStep,
                    true
                )
        }
    }

    private fun goToGoals() {
        val userMainGoal = goalsRepository.getUserMainGoal()
        if (goalsRepository.getUserChosenGoals().size == Constants.USER_ALLOWED_GOALS_SELECTION_COUNT) {
            if (userMainGoal != null) {
                goToDietsOrFood(false)
            } else {
                state.navigateTo.value =
                    CreatePlanRecommendationFragmentDirections.actionCreatePlanRecommendationFragmentToOneGoalSelectionFragment(
                        true
                    )
            }
        } else {
            state.navigateTo.value =
                CreatePlanRecommendationFragmentDirections.actionCreatePlanRecommendationFragmentToPostTestGoalsSelectFragment()
        }
    }

    private fun addHydrationRecommendation() {
        if (!preferencesRepository.isAddedHydrationRecommendation) {
            preferencesRepository.isAddedHydrationRecommendation = true
            viewModelScope.launch {
                val recommendationResponse = recommendationManager.getLatestSampleRecommendations()
                if (recommendationResponse is Result.Success) {
                    val hydrationPlanId =
                        recommendationResponse.value.lifestyle.firstOrNull { it.lifestyle_recommendation_id == Constants.HYDRATION_PLAN_ID }?.id
                            ?: return@launch

                    val buildPlanRequest = BuildPlanRequest(
                        food_ids = listOf(),
                        reagent_lifestyle_recommendations_ids = listOf(hydrationPlanId),
                        supplements_ids = listOf(),
                        is_auto_build = true
                    )
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_BUILD_SUBMITTED)
                            .addProperty("data", buildPlanRequest.toString())
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    planManager.buildPlan(buildPlanRequest)
                }
            }
        }
    }

    private fun addSupplementRecommendation() {
        if (!preferencesRepository.isAddedSupplementRecommendation) {
            preferencesRepository.isAddedSupplementRecommendation = true
            viewModelScope.launch {
                val recommendationResponse = recommendationManager.getLatestSampleRecommendations()
                if (recommendationResponse is Result.Success) {
                    val supIds: ArrayList<Int> = arrayListOf()
                    for (sup in buildPlanManager.getSupplementRecommendations()) {
                        supIds.add(sup.id)
                    }

                    val buildPlanRequest = BuildPlanRequest(
                        food_ids = listOf(),
                        reagent_lifestyle_recommendations_ids = listOf(),
                        supplements_ids = supIds,
                        is_auto_build = true
                    )
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_BUILD_SUBMITTED)
                            .addProperty("data", buildPlanRequest.toString())
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    planManager.buildPlan(buildPlanRequest)
                }
            }
        }
    }

    private fun loadScores(forceUpdate: Boolean = false) {
        val lowReagents = mutableListOf<Reagent>()
        var cortisolLevel: ReagentLevel? = null
        var hydrationLevel: ReagentLevel? = null
        showLoading()
        viewModelScope.launch {
            scoreManager.getWellnessScores(forceUpdate)
                .onSuccess { score ->
                    state {
                        val reagents = reagentsManager.getReagent()
                        for (reagentIndex in reagents.indices) {
                            val reagent = reagents[reagentIndex]
                            val key = score.reagents.keys.firstOrNull { it.id == reagent.id }
                            val reagentScore = score.reagents[key]?.first()?.score ?: continue
                            val lastReagentChartRange =
                                if (reagent.id == ReagentItem.Hydration.id) {
                                    reagent.ranges.firstOrNull { reagentScore <= it.from && reagentScore >= it.to }
                                } else {
                                    reagent.ranges.firstOrNull { reagentScore >= it.from && reagentScore <= it.to }
                                }

                            if (lastReagentChartRange?.reagentType == ReagentLevel.Low) {
                                lowReagents.add(reagent)
                            }

                            if (ReagentItem.Cortisol.id == reagent.id) {
                                cortisolLevel = lastReagentChartRange?.reagentType
                            }
                            if (ReagentItem.Hydration.id == reagent.id) {
                                hydrationLevel = lastReagentChartRange?.reagentType
                            }
                            getRecommendedPlanHints(lowReagents, cortisolLevel, hydrationLevel)
                            hideLoading(false)
                        }
                    }
                }
                .onError {
                    loadScores(true)
                }
        }
    }

    private fun setupProgressCountDown() {
        if (state.currentPlanType == RecommendationType.Tip) {
            val totalDuration = TimeUnit.SECONDS.toMillis(21)
            val interval = TimeUnit.SECONDS.toMillis(1)
            object : CountDownTimer(totalDuration, interval) {
                override fun onTick(millisUntilFinished: Long) {
                    val progress =
                        100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                    state.planRecommendationItem.value =
                        state.planRecommendations.first { it.type == RecommendationType.Tip }
                    state.progress.value = progress
                    if (progress > 10) {
                        goToGoals()
                    }
                }

                override fun onFinish() {
                    goToGoals()
                }
            }.start()
        } else if (state.currentPlanType == RecommendationType.Food) {
            val totalDuration = TimeUnit.SECONDS.toMillis(21)
            val interval = TimeUnit.SECONDS.toMillis(1)
            object : CountDownTimer(totalDuration, interval) {
                override fun onTick(millisUntilFinished: Long) {
                    val progress =
                        100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                    state.planRecommendationItem.value =
                        state.planRecommendations.first { it.type == RecommendationType.Food }
                    state.progress.value = progress
                    if (progress > 25) {
                        goToDietsOrFood(false)
                    }
                }

                override fun onFinish() {
                    goToDietsOrFood(false)
                }
            }.start()
        } else if (state.currentPlanType == RecommendationType.Supplement) {
            val totalDuration = TimeUnit.SECONDS.toMillis(21)
            val interval = TimeUnit.SECONDS.toMillis(1)
            addSupplementRecommendation()
            object : CountDownTimer(totalDuration, interval) {
                override fun onTick(millisUntilFinished: Long) {
                    val progress =
                        100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                    state.progress.value = progress + 25
                    when {
                        progress > 50 -> {
                            state.currentPlanType = RecommendationType.Lifestyle
                            goToLifestyle(false)
                        }
                        progress in 25..50 -> {
                            state.planRecommendationItem.value =
                                state.planRecommendations.first { it.type == RecommendationType.Lifestyle }
                        }
                        else -> {
                            state.planRecommendationItem.value =
                                state.planRecommendations.first { it.type == RecommendationType.Supplement }
                        }
                    }
                }
                override fun onFinish() {
                    state.currentPlanType = RecommendationType.Lifestyle
                    goToLifestyle(false)
                }
            }.start()
        } else if (state.currentPlanType == RecommendationType.Lifestyle) {
            val totalDuration = TimeUnit.SECONDS.toMillis(21)
            val interval = TimeUnit.SECONDS.toMillis(1)
            object : CountDownTimer(totalDuration, interval) {
                override fun onTick(millisUntilFinished: Long) {
                    val progress = 100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                    state.progress.value = progress + 50
                    if (progress >= 50) {
                        goToLifestyle(false)
                    } else {
                        state.planRecommendationItem.value =
                            state.planRecommendations.first { it.type == RecommendationType.Lifestyle }
                    }
                }

                override fun onFinish() {
                    goToLifestyle(false)
                }
            }.start()
        } else if (state.currentPlanType == RecommendationType.Hydration) {
            if (!isNavigated) {
                endTheFlow()
            } else {
                addHydrationRecommendation()
                val totalDuration = TimeUnit.SECONDS.toMillis(21)
                val interval = TimeUnit.SECONDS.toMillis(1)
                object : CountDownTimer(totalDuration, interval) {
                    override fun onTick(millisUntilFinished: Long) {
                        val progress =
                            100 - ((millisUntilFinished.toDouble() / totalDuration) * 100).toInt()
                        state.progress.value = progress + 75
                        if (progress >= 90 && !isNavigated) {
                            state.progress.value = 100
                            endTheFlow()
                        } else {
                            state.planRecommendationItem.value =
                                state.planRecommendations.first { it.type == RecommendationType.Hydration }
                        }
                    }

                    override fun onFinish() {
                        state.progress.value = 100
                        endTheFlow()
                    }
                }.start()
            }
        }
    }

    private fun goToNextStep() {
        if (preferencesRepository.testCompletedCount == 1) {
            goToTestingReminder()
        } else if (preferencesRepository.testCompletedCount > 1 && preferencesRepository.showTestingTimerInThePostFlow) {
            preferencesRepository.showTestingTimerInThePostFlow = false
            goToTestingReminder()
        } else {
            state.navigate.call()
        }
    }

    private fun goToTestingReminder() {
        if (::testingPlan.isInitialized.not()) {
            loadTestingPlan(true)
            return
        }
        if (hasTestPlan) {
            state.navigate.call()
        } else {
            navigateToTestingReminder()
        }
    }

    private fun loadTestingPlan(showLoading: Boolean = false) {
        if (showLoading)
            showLoading()
        viewModelScope.launch {
            val plansResponse = planManager.getUserPlans(true)
            val recommendationResponse = recommendationManager.getLatestSampleRecommendations()
            if (plansResponse is Result.Success && recommendationResponse is Result.Success) {
                hideLoading(false)
                val recommendations = recommendationResponse.value.lifestyle
                testingPlan =
                    recommendations.firstOrNull { it.lifestyle_recommendation_id == Constants.TEST_PLAN_ID }
                    ?: return@launch
                hasTestPlan = plansResponse.value.any { it.isTestingPlan() }
            }
        }
    }

    private fun getTestingPlanData() = PlanData(
        multiple = 1,
        reagent_lifestyle_recommendation_id = testingPlan.id ?: Constants.TEST_PLAN_ID,
        reagent_lifestyle_recommendation = ReagentLifestyleRecommendation(
            id = -1,
            lifestyle_recommendation_id = Constants.TEST_PLAN_ID,
            quantity = testingPlan.amount,
            unit = testingPlan.unit,
            frequency = testingPlan.frequency
        )
    )

    private fun navigateToTestingReminder() {
        state.navigateTo.value =
            CreatePlanRecommendationFragmentDirections.actionCreatePlanRecommendationFragmentToTestingReminderDialog(
                PlanReminderHeader(
                    getResString(R.string.wellness_testing),
                    "",
                    "",
                    R.drawable.ic_take_test,
                    listOf(getTestingPlanData()),
                    isEmptyPlan = true,
                    isTestingPlan = true
                )
            )
    }

    private fun getCurrentPlan() {
        state.planRecommendationItem.value = when (state.currentPlanType) {
            RecommendationType.Food -> {
                PlanRecommendationModel(
                    R.drawable.ic_food_recommendation,
                    getPlanTitle(R.string.creating_your_food_plan, R.string.update_your_food_plan),
                    "",
                    RecommendationType.Food
                )
            }
            RecommendationType.Tip -> {
                PlanRecommendationModel(
                    R.drawable.ic_food_recommendation,
                    getPlanTitle(R.string.creating_your_plan, R.string.updating_your_plan),
                    "",
                    RecommendationType.Tip
                )
            }
            RecommendationType.Supplement -> {
                PlanRecommendationModel(
                    R.drawable.ic_supplement_recommendation,
                    getPlanTitle(
                        R.string.creating_your_supplement_plan,
                        R.string.update_your_supplement_plan
                    ),
                    "",
                    RecommendationType.Supplement
                )
            }
            RecommendationType.Lifestyle -> {
                PlanRecommendationModel(
                    R.drawable.ic_stress_recommendation,
                    getPlanTitle(
                        R.string.creating_your_stress_plan,
                        R.string.update_your_stress_plan
                    ),
                    "",
                    RecommendationType.Lifestyle
                )
            }
            RecommendationType.Hydration -> {
                PlanRecommendationModel(
                    R.drawable.ic_water_recommendation,
                    getPlanTitle(
                        R.string.creating_your_hydration_plan,
                        R.string.update_your_hydration_plan
                    ),
                    "",
                    RecommendationType.Hydration
                )
            }
        }
    }

    private fun getRecommendedPlanHints(
        lowLevelReagents: List<Reagent>,
        cortisolLevel: ReagentLevel?,
        hydrationLevel: ReagentLevel?
    ) {
        state.planRecommendations.add(
            PlanRecommendationModel(
                R.drawable.ic_food_recommendation,
                getPlanTitle(R.string.creating_your_plan, R.string.updating_your_plan),
                "",
                RecommendationType.Tip
            )
        )

        val foodStepDescription = if (lowLevelReagents.isNotEmpty()) {
            val reagents = lowLevelReagents.joinToString(", ") { it.displayName }
            getResString(R.string.food_recommendation_hint_low, reagents)
        } else {
            getResString(R.string.food_recommendation_hint_good)
        }

        state.planRecommendations.add(
            PlanRecommendationModel(
                R.drawable.ic_food_recommendation,
                getPlanTitle(R.string.creating_your_food_plan, R.string.update_your_food_plan),
                foodStepDescription,
                RecommendationType.Food
            )
        )

        val supplementStepDescription = if (supplements.isNotEmpty()) {
            val reagents = supplements.joinToString(", ") { it.name }
            getResString(R.string.supplement_recommendation_hint_low, reagents)
        } else {
            getResString(R.string.supplement_recommendation_hint_good)
        }
        state.planRecommendations.add(
            PlanRecommendationModel(
                R.drawable.ic_supplement_recommendation,
                getPlanTitle(
                    R.string.creating_your_supplement_plan,
                    R.string.update_your_supplement_plan
                ),
                supplementStepDescription,
                RecommendationType.Supplement
            )
        )

        state.planRecommendations.add(
            PlanRecommendationModel(
                R.drawable.ic_stress_recommendation,
                getPlanTitle(R.string.creating_your_stress_plan, R.string.update_your_stress_plan),
                getResString(if (cortisolLevel == ReagentLevel.High) R.string.cortisol_recommendation_hint_high else R.string.cortisol_recommendation_hint_low_good),
                RecommendationType.Lifestyle
            )
        )

        val hydrationHint = when (hydrationLevel) {
            ReagentLevel.High -> R.string.hydration_recommendation_hint_high
            ReagentLevel.Good -> R.string.hydration_recommendation_hint_good
            else -> R.string.hydration_recommendation_hint_low
        }

        state.planRecommendations.add(
            PlanRecommendationModel(
                R.drawable.ic_water_recommendation,
                getPlanTitle(
                    R.string.creating_your_hydration_plan,
                    R.string.update_your_hydration_plan
                ),
                getResString(hydrationHint),
                RecommendationType.Hydration
            )
        )
        if (state.currentPlanType == RecommendationType.Supplement) {
            state.planRecommendationItem.value =
                state.planRecommendations.first { it.type == RecommendationType.Supplement }
        } else {
            state.planRecommendationItem.value =
                state.planRecommendations.first { it.type == RecommendationType.Lifestyle }
        }

        if (preferencesRepository.testCompletedCount > 1) {
            // Update auto plan flow
            showUpdateAutoPlanFlow()
        } else {
            // Build auto plan flow
            setupProgressCountDown()
        }
    }

    private fun getPlanTitle(firstTestTitle: Int, secondTestTitle: Int): String {
        return getResString(if (preferencesRepository.testCompletedCount <= 1) firstTestTitle else secondTestTitle)
    }

    private fun logEvent(event: String) {
        trackingManager.log(
            TrackedEvent.Builder(event)
                .setScreenName(TrackingConstants.FROM_PLAN_BUILDER)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
