package com.vessel.app.activationtimer.video1tab

import javax.inject.Inject

class Video1TabState @Inject constructor() {

    operator fun invoke(block: Video1TabState.() -> Unit) = apply(block)
}
