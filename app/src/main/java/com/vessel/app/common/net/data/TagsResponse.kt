package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TagsResponse(
    val tags: List<TagData>
)
