package com.vessel.app.today.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.plan.model.DayPlanItem
import kotlinx.android.synthetic.main.activity_locked.view.*
import kotlinx.android.synthetic.main.item_activity_list.view.*
import kotlinx.android.synthetic.main.item_activity_list.view.notDoneLayout
import kotlinx.android.synthetic.main.item_activity_list.view.title

class ActivityListAdapter(private val handler: ActivityAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<DayPlanItem>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is DayPlanItem -> R.layout.item_activity_list
        else -> throw IllegalStateException(
            "Unexpected ActivityListAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }
    override fun onBindViewHolder(holder: BaseViewHolder<DayPlanItem>, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.itemView.title.visibility = if (position == 0) View.VISIBLE else View.GONE
        val item = getItem(position) as DayPlanItem
        if (item.isTested) {
            holder.itemView.lockedLayout.visibility = View.GONE
            val itemList = if (item.isPreviousDay == false) item.items else item.items.filter {
                it.isCompleted
            }
            holder.itemView.notDoneLayout.visibility =
                if (itemList.isNullOrEmpty() && item.isPreviousDay == true) View.VISIBLE else View.GONE
            val activityAdapter = ActivityAdapter(handler, position, true)
            holder.itemView.findViewById<RecyclerView>(R.id.activityList).apply {
                adapter = activityAdapter
                itemAnimator = null
                setHasFixedSize(true)
            }
            activityAdapter.submitList(itemList)
        } else {
            holder.itemView.notDoneLayout.visibility = View.GONE
            holder.itemView.findViewById<RecyclerView>(R.id.activityList).visibility = View.GONE
            holder.itemView.lockedLayout.visibility = if (position == 0) View.VISIBLE else View.GONE
            holder.itemView.testButton.setOnClickListener {
                handler.onTakeATest()
            }
        }
        holder.itemView.title.setOnClickListener {
            handler.onWellnessActivityInformationClicked()
        }
    }
}
