package com.vessel.app.views.stepper

import androidx.core.view.updateLayoutParams
import androidx.databinding.ViewDataBinding
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class StepperAdapter : BaseAdapterWithDiffUtil<StepperUiModel>() {
    private var _itemWidth: Int = 0
    val itemWidth: Int get() = _itemWidth

    fun setItemWidth(width: Int, changedItemsCount: Int) {
        _itemWidth = width
        notifyItemRangeChanged(0, changedItemsCount)
    }

    override fun getLayout(position: Int) = R.layout.item_stepper

    override fun getViewHolder(binding: ViewDataBinding): BaseViewHolder<StepperUiModel> =
        ViewHolder(binding, itemWidth)

    class ViewHolder(private val binding: ViewDataBinding, private val itemWidth: Int) :
        BaseAdapterWithDiffUtil.BaseViewHolder<StepperUiModel>(binding) {

        override fun bind(item: StepperUiModel, handler: Any?) {
            binding.root.updateLayoutParams {
                width = itemWidth
            }
            super.bind(item, handler)
        }
    }
}
