package com.vessel.app.newtesttimer.surveytab

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class SurveyState @Inject constructor() {
    val showCompleteSurvey = MutableLiveData<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: SurveyState.() -> Unit) = apply(block)
}
