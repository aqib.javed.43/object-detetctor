package com.vessel.app.reagent.energysources.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.reagent.energysources.ReagentSourceViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_reagent_source.*

@AndroidEntryPoint
class ReagentSourceFragment : BaseDialogFragment<ReagentSourceViewModel>() {
    override val viewModel: ReagentSourceViewModel by viewModels()
    override val layoutResId = R.layout.fragment_reagent_source
    private val sourceEnergyAdapter by lazy { SourceEnergyAdapter(viewModel) }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
        isCancelable = false
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        sources.adapter = sourceEnergyAdapter
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(dismissDialog) { dismiss() }
            observe(infoSource) { sourceEnergyAdapter.submitList(it.toList()) }
            observe(energyName) {
                val titleText = "${viewModel.state.headerTitle} & $it"
                title.text = titleText
            }
        }
    }
}
