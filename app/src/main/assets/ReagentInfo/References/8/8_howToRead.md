Here are the reference ranges we use for cortisol:

Low: 0 - 50 mcg/L
Good: 50 - 150 mcg/L
High: > 150 mcg/L

Cortisol is a stress hormone made by our adrenal glands. It is released in a pattern linked to our circadian rhythm, and its release is increased in response to psychological and physiological stress. The human stress response begins when the hypothalamus secretes corticotropin-releasing hormone, which travels straight to the anterior pituitary to induce adrenocorticotropic hormone release into the general circulation. Once this reaches the adrenal cortex, cortisol is released into the bloodstream. In a normal healthy stress response, this has an inhibitory effect on the hypothalamus which then reduces or stops secreting corticotropin releasing hormone, shutting down the stress response.

Once released it circulates in our bloodstream and is filtered by the kidneys into our urine. Therefore high urine cortisol corresponds to high blood cortisol and visa versa.

Cortisol production starts to ramp up around 3 AM and peaks around 8 AM. If you’re an early bird, and up before 6 AM, cortisol levels will likely be very low. A level of 0 mcg/L can be normal. However, if you’re a night owl and wake up later, then don’t be surprised if your morning cortisol levels are much higher. The later you wake up, the higher your cortisol will tend to be, so keep this in mind when you test.

Also remember that your kidneys are always making urine, which gets stored in your bladder until you urinate. Therefore, in addition to the time of day that you test, the time you last emptied your bladder is also relevant in interpreting your cortisol level. For example if you usually sleep through the night, your urine cortisol level would be the average of all cortisol levels since you last emptied your bladder. Some people who urinate frequently in the night may wish to empty their bladder right when they wake up in the morning, and then use the 2nd morning urine to test their cortisol levels (which would reflect more recent blood concentrations.

**references:**

-   https://www.ncbi.nlm.nih.gov/pubmed/18583623

-   https://www.ncbi.nlm.nih.gov/pubmed/25425285

-   https://www.ncbi.nlm.nih.gov/pubmed/25298042

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6447160/

-   https://www.mayocliniclabs.com/test-catalog/Clinical+and+Interpretive/82920

-   https://www.ncbi.nlm.nih.gov/pubmed/25425285

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2684472/

-   https://www.ncbi.nlm.nih.gov/pubmed/4326799/

-   https://www.ncbi.nlm.nih.gov/pubmed/14572876/

-   https://www.ncbi.nlm.nih.gov/pubmed/19223520/

Pediatric Reference Ranges (only available for urine cortisol: creatinine ratios)

-   https://pediatric.testcatalog.org/show/COCRU

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5785641/
