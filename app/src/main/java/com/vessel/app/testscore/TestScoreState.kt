package com.vessel.app.testscore

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.model.ReagentItem
import javax.inject.Inject

class TestScoreState @Inject constructor() {
    val scoreItems = MutableLiveData<Triple<Int, Int, Int>>()
    var isFirstAnimationDone = false
    var isReagentAnimationDone = false
    val startReagentAndHeaderScaleAnimation = LiveEvent<Unit>()
    val reagentItems = MutableLiveData<List<ReagentItem>>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: TestScoreState.() -> Unit) = apply(block)
}
