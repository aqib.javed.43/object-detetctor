package com.vessel.app.planreminderdialog.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import com.vessel.app.common.net.data.plan.PlanData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlanReminderHeader(
    val title: String?,
    val description: String?,
    val backgroundUrl: String?,
    @DrawableRes val background: Int?,
    val todos: List<PlanData>,
    val isEmptyPlan: Boolean = false,
    val isTestingPlan: Boolean = false,
    val finishTheActivity: Boolean = false,
    val skippable: Boolean = true,
) : Parcelable
