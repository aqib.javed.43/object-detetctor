package com.vessel.app.foodplanpopup.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PopUpDialogModel(val isRemoveFoodPlanItem: Boolean = false) : Parcelable {
    @StringRes
    @IgnoredOnParcel
    val title: Int = if (isRemoveFoodPlanItem)
        R.string.removed_from_plan
    else
        R.string.added_to_plan

    @DrawableRes
    @IgnoredOnParcel
    val icon: Int = if (isRemoveFoodPlanItem)
        R.drawable.ic_close_black_background
    else
        R.drawable.ic_radio_selected
}
