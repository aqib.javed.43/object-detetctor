package com.vessel.app.cardchoices.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.cardchoices.model.CardChoiceUIItem

class CardChoicesAdapter(
    private val handler: OnActionHandler
) : BaseAdapterWithDiffUtil<CardChoiceUIItem>({ oldItem, newItem ->
    oldItem.title == newItem.title
}) {
    override fun getLayout(position: Int) = R.layout.item_card_choices_select

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onChoiceSelectClicked(item: CardChoiceUIItem, isChecked: Boolean)
    }
}
