package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PagingData(
    val page: Int,
    val total: Int? = null,
    val per_page: Int,
    val pages: Int? = null
)
