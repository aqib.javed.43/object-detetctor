package com.vessel.app.today.selectprogram

import android.util.Log
import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.Program
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.goalsselection.model.GoalSelect
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.wellness.net.data.ProgramAction
import kotlinx.coroutines.launch
import java.util.*

class ProgramSelectViewModel @ViewModelInject constructor(
    val state: ProgramSelectState,
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
    private val programManager: ProgramManager
) : BaseViewModel(resourceProvider),
    ProgramSelectAdapter.OnActionHandler,
    ToolbarHandler {
    init {
        loadEnrolledPrograms()
    }
    private fun loadEnrolledPrograms() {
        showLoading()
        viewModelScope.launch() {
            programManager.getEnrolledPrograms()
                .onSuccess { enrolledPrograms ->
                    processEnrolledPrograms(enrolledPrograms)
                    hideLoading()
                }
                .onServiceError {
                    hideLoading()
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }
                .onNetworkIOError {
                    hideLoading()
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    hideLoading()
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun processEnrolledPrograms(enrolledPrograms: List<Program>) {
        var position = 0
        state.items.value = goalsRepository.getGoals()
            .map { entry ->
                entry.key.let { goal ->
                    GoalSelect(
                        id = goal.id,
                        title = goal.title,
                        recommendation1 = goal.recommendation1,
                        recommendation2 = goal.recommendation2,
                        background = Goal.alternateBackground(position++),
                        image = goal.icon,
                        checked = enrolledPrograms.any { it.mainGoalId == goal.id },
                    )
                }
            }
    }

    override fun onGoalSelectClicked(item: GoalSelect) {
        checkAndValidItem(item)
    }

    private fun checkAndValidItem(item: GoalSelect) {
        val currentItems = state.items.value?.filter {
            it.checked
        }
        val isExits = currentItems?.firstOrNull {
            it.id == item.id
        } != null
        if (isExits) {
            processUpdateProgram(item, LEAVE)
        } else {
            if (currentItems?.size ?: 0 >= 2) {
                rollbackSelected(item, false)
                state.navigateTo.value = ProgramSelectFragmentDirections.actionProgramSelectFragmentToNoticeDialogFragment()
                return
            }
            processUpdateProgram(item, ENROLL)
        }
    }
    fun rollbackSelected(item: GoalSelect, checked: Boolean) {
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, goalSelect ->
                    if (goalSelect.id == item.id) goalSelect.copy(checked = checked)
                    else goalSelect.copy()
                }
        }
    }

    private fun processUpdateProgram(item: GoalSelect, action: String) {
        if (action != ENROLL) {
            Constants.onSelectConfirm = { programAction ->
                loadAndUpdatePrograms(programAction, item, action)
            }
            Constants.onCancel = {
                state {
                    items.value =
                        items.value.orEmpty().mapIndexed { _, goalSelect ->
                            if (goalSelect.id == item.id) goalSelect.copy(checked = true)
                            else goalSelect.copy()
                        }
                }
            }
            state.navigateTo.value = ProgramSelectFragmentDirections.actionProgramSelectFragmentToLeaveConfirmDialogFragment()
        } else {
            loadAndUpdatePrograms(ProgramAction.KEEP, item, action)
        }
    }
    private fun completeLeaveProgram(program: Program, action: ProgramAction, item: GoalSelect) {
        showLoading()
        viewModelScope.launch {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.PROGRAM_LEAVE)
                    .addProperty(TrackingConstants.KEY_ID, program.id.toString())
                    .addProperty(TrackingConstants.LEAVE_TYPE, action.value)
                    .addProperty(
                        TrackingConstants.KEY_LOCATION,
                        Constants.PROGRAM_DETAILS_PAGE
                    )
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
            programManager.completeProgram(program, action)
                .onSuccess {
                    state {
                        items.value =
                            items.value.orEmpty().mapIndexed { _, goalSelect ->
                                if (goalSelect.id == item.id) goalSelect.copy(checked = item.checked.not())
                                else goalSelect.copy()
                            }
                    }
                    state.items.value?.let {
                        goalsRepository.setUserChosenGoals(
                            it.map { goal ->
                                GoalSample(
                                    goal.id,
                                    getResString(goal.title)
                                )
                            }
                        )
                    }
                    hideLoading()
                }
                .onServiceError {
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                    hideLoading()
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                    hideLoading()
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                    hideLoading()
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    override fun onGoalSelectSelected(item: GoalSelect) {
        viewModelScope.launch {
            showLoading()
            programManager.getProgramsByGoal(
                goalId = item.id

            ).onSuccess { programModel ->
                val programs = programModel.program
                if (programs.isNotEmpty()) {
                    val program = programs.first()
                    redirectProgramDetails(program)
                }
                hideLoading(false)
            }.onServiceError {
                showError(it.error.message ?: getResString(R.string.unknown_error))
            }.onNetworkIOError {
                showError(getResString(R.string.network_error))
            }.onUnknownError {
                showError(getResString(R.string.unknown_error))
            }.onError {
                hideLoading(false)
            }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        navigateToHome()
    }
    private fun redirectProgramDetails(program: Program) {
        state.navigateTo.value =
            ProgramSelectFragmentDirections.actionProgramSelectFragmentToProgramDetailsFragment(
                program
            )
    }
    private fun navigateToHome() {
        Log.d("TAG", "navigateToHome: ")
        state.backToHome.call()
    }

    fun onCheckIn() {
        preferencesRepository.isExistingUserFirstTime = true
        state.navigateTo.value =
            ProgramSelectFragmentDirections.actionProgramSelectFragmentToRateMainGoalSelectionFragment()
    }
    private fun loadAndUpdatePrograms(programAction: ProgramAction, item: GoalSelect, action: String) {
        viewModelScope.launch {
            showLoading()
            programManager.getProgramsByGoal(
                goalId = item.id

            ).onSuccess { programModel ->
                val programs = programModel.program
                if (programs.isNotEmpty()) {
                    val program = programs.first()
                    if (action != ENROLL) {
                        completeLeaveProgram(program, programAction, item)
                    } else {
                        enrollProgram(program, programAction, item)
                    }
                }
            }.onServiceError {
                showError(it.error.message ?: getResString(R.string.unknown_error))
            }.onNetworkIOError {
                showError(getResString(R.string.network_error))
            }.onUnknownError {
                showError(getResString(R.string.unknown_error))
            }.onError {
                hideLoading()
            }
        }
    }

    private fun enrollProgram(program: Program, action: ProgramAction, item: GoalSelect) {
        state {
            viewModelScope.launch {
                showLoading()
                programManager.joinProgram(program, action).onSuccess {
                    state {
                        items.value =
                            items.value.orEmpty().mapIndexed { _, goalSelect ->
                                if (goalSelect.id == item.id) goalSelect.copy(checked = item.checked.not())
                                else goalSelect.copy()
                            }
                    }
                    state.items.value?.let {
                        goalsRepository.setUserChosenGoals(
                            it.map { goal ->
                                GoalSample(
                                    goal.id,
                                    getResString(goal.title)
                                )
                            }
                        )
                    }

                    hideLoading()
                }.onServiceError {
                    rollbackSelected(item, false)
                    showError(it.error.message ?: getResString(R.string.unknown_error))
                }.onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }.onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }.onError { hideLoading() }
            }
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.PROGRAM_GOAL_SELECTED)
                .addProperty("program name", program.title.toString())
                .addProperty("time", Calendar.getInstance().time.formatDayDate())
                .setEmail(preferencesRepository.getContact()?.email ?: "")
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
    companion object {
        const val LEAVE = "LEAVE"
        const val ENROLL = "ENROLL"
    }
}
