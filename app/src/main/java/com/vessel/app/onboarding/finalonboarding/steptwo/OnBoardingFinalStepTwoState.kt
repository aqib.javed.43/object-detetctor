package com.vessel.app.onboarding.finalonboarding.steptwo

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnBoardingFinalStepTwoState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: OnBoardingFinalStepTwoState.() -> Unit) = apply(block)
}
