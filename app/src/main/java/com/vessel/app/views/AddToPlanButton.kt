package com.vessel.app.views

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.vessel.app.R
import kotlinx.android.synthetic.main.add_to_plan_button.view.*
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.blurView
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.button

open class AddToPlanButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    init {
        inflate(context, R.layout.add_to_plan_button, this)
    }

    fun setText(text: String) {
        button.text = text
    }

    fun setText(@StringRes stringResId: Int) {
        button.setText(stringResId)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        blurView.setOnClickListener(l)
    }
}

@BindingAdapter("isAdded")
fun AddToPlanButton.isAdded(isAdded: Boolean) {
    if (isAdded) {
        button.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        blurView.setOverlayColor(ContextCompat.getColor(context, R.color.blackAlpha70))
        icon.setImageResource(R.drawable.ic_white_cross)
    } else {
        button.setTextColor(ContextCompat.getColor(context, R.color.darkText))
        blurView.setOverlayColor(ContextCompat.getColor(context, R.color.whiteAlpha70))
        icon.setImageResource(R.drawable.ic_black_plus)
    }
}
