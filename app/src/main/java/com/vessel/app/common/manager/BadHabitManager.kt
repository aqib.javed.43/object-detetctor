package com.vessel.app.common.manager

import com.vessel.app.common.repo.BadHabitRespository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BadHabitManager @Inject constructor(
    private val badHabitRepository: BadHabitRespository
) {
    suspend fun fetchAllBadHabit(subGoalId: String?) = badHabitRepository.getAllBadHabits(subGoalId)
}
