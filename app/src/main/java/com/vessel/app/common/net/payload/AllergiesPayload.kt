package com.vessel.app.common.net.payload

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AllergiesPayload(val allergies: List<String>)
