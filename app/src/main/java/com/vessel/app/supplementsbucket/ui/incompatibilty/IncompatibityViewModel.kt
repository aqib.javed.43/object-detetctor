package com.vessel.app.supplementsbucket.ui.incompatibilty

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class IncompatibityViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {
    val state = IncompatibityState(
        savedStateHandle["supplementNameA"]!!,
        savedStateHandle["supplementNameB"]!!
    )
    init {
        state.alertBody.value = getResString(R.string.Incompatibility_alert_template, state.supplementNameA, state.supplementNameB)
    }

    fun dismiss() {
        state.dismissDialog.call()
    }
}
