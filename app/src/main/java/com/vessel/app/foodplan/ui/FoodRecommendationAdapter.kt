package com.vessel.app.foodplan.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodplan.model.FoodRecommendationViewItem
import com.vessel.app.foodplan.model.FoodRecommendationViewItem.Companion.FOOD_RECOMMENDATION_VIEW_ITEM_LOADING

class FoodRecommendationAdapter(
    val handler: FoodRecommendationHandler
) : BaseAdapterWithDiffUtil<FoodRecommendationViewItem>() {
    override fun getLayout(position: Int) =
        if (getItem(position) == FOOD_RECOMMENDATION_VIEW_ITEM_LOADING) {
            R.layout.item_loading
        } else {
            R.layout.item_food_recommendation
        }

    override fun getHandler(position: Int): Any? = handler

    interface FoodRecommendationHandler {
        fun onActionButtonClick(item: FoodRecommendationViewItem)
    }
}
