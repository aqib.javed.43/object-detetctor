package com.vessel.app.programs.details.schedule

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ProgramScheduleState @Inject constructor(val passedProgram: Program) {

    val program = MutableLiveData(passedProgram)
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: ProgramScheduleState.() -> Unit) = apply(block)
}
