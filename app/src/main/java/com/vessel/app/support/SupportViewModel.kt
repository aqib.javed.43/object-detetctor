package com.vessel.app.support

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.*
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.MembershipManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration

class SupportViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val remoteConfiguration: RemoteConfiguration,
    val membershipManager: MembershipManager,
    val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository), ToolbarHandler {
    val openMessaging = LiveEvent<Any>()

    init {
        remoteConfiguration.fetchConfigFromRemote()
    }

    fun onShareAnIdeaClicked(view: View) {
        view.findNavController()
            .navigate(SupportFragmentDirections.actionSupportToWeb(BuildConfig.FEEDBACK_URL))
    }

    fun onReportBugClicked(view: View) {
        view.findNavController()
            .navigate(SupportFragmentDirections.actionSupportToWeb(BuildConfig.REPORT_BUG_URL))
    }

    fun onContactSupportClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CHAT_WITH_SUPPORT)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        openMessaging.call()
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
    fun getChatConfiguration() =
        membershipManager.getChatConfiguration(MembershipManager.ZENDESK_SUPPORT_DEPARTMENT)
    fun isLiveChat() = remoteConfiguration.getBoolean("use_livechat")
    fun getUserName() = preferencesRepository.userFirstName
    fun getUserEmail() = preferencesRepository.userEmail
}
