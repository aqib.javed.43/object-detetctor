package com.vessel.app.newtesttimer.surveytab

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.activationtimer.util.TestSurveyStates
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.SurveyRepository
import com.vessel.app.newtesttimer.ui.NewTestTimerFragmentDirections
import com.vessel.app.takesurvey.SurveyQuestion
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SurveyViewModel @ViewModelInject constructor(
    val state: SurveyState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
    val surveyRepository: SurveyRepository,
    private val remoteConfiguration: RemoteConfiguration
) : BaseViewModel(resourceProvider) {

    private val surveyId = remoteConfiguration.getLong(Constants.REMOTE_CONFIG_TIMER_SURVEY_ID)

    init {
        checkSurveyStates()
    }

    private fun checkSurveyStates() {
        state.showCompleteSurvey.value =
            preferencesRepository.getSurveyStates() == TestSurveyStates.COMPLETED
    }

    fun onLetsDoItClicked() {
        showLoading()
        viewModelScope.launch {
            surveyRepository.getSurveyById(surveyId)
                .onSuccess {
                    processSurveyList(it)
                    hideLoading(true)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }
    private fun processSurveyList(items: List<SurveyQuestion>) {
        showLoading(false)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val multipleIds = items.map {
                    it.id
                }
                val content = arrayListOf<SurveyQuestion>()
                coroutineScope {
                    multipleIds.forEach { id ->
                        launch {
                            surveyRepository.getAnswerById(id.toLong()).onSuccess { answerResponse ->
                                items.firstOrNull { it.id == id }?.let {
                                    content.add(it.copy(answers = answerResponse))
                                }
                            }
                        }
                    }
                }
                navigateToTakeSurvey(content)
                hideLoading(false)
            }
        }
    }

    private fun navigateToTakeSurvey(questions: List<SurveyQuestion>) {
        state.navigateTo.value =
            NewTestTimerFragmentDirections.actionNewTestTimerFragmentToTakeSurvey(
                questions.toTypedArray(),
                surveyId.toString()
            )
    }
}
