package com.vessel.app.common.binding

import android.animation.ValueAnimator
import android.view.View
import android.widget.TextView
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter

@BindingAdapter("layout_height")
fun View.setLayoutHeight(@DimenRes heightRes: Int) {
    val layoutParams = layoutParams
    layoutParams.height = resources.getDimensionPixelSize(heightRes)
    setLayoutParams(layoutParams)
}

@BindingAdapter("visible")
fun View.setVisible(visible: Boolean?) {
    visibility = if (visible == true) View.VISIBLE else View.GONE
}

@BindingAdapter("invisible")
fun View.setInvisible(invisible: Boolean?) {
    visibility = if (invisible == true) View.INVISIBLE else View.VISIBLE
}

@BindingAdapter("resVisibility")
fun View.setResVisibility(resId: Int?) {
    visibility = if (resId == null) View.GONE else View.VISIBLE
}

@BindingAdapter("textVisibility")
fun View.setTextVisibility(text: String?) {
    visibility = if (text.isNullOrEmpty()) View.GONE else View.VISIBLE
}

@BindingAdapter("clipToOutline")
fun View.setClipToOutline(clipToOutline: Boolean?) {
    clipToOutline?.let { setClipToOutline(clipToOutline) }
}

@BindingAdapter("android:background")
fun View.setBackgroundResource(@DrawableRes resource: Int?) {
    resource?.let { setBackgroundResource(it) }
}

@BindingAdapter("layout_constraintVertical_bias")
fun View.setLayoutConstraintVerticalBias(bias: Float?) {
    bias?.let { (layoutParams as ConstraintLayout.LayoutParams).verticalBias = bias }
}

@BindingAdapter("isSelected")
fun View.setViewAsSelected(isSelected: Boolean) {
    this.isSelected = isSelected
}

@BindingAdapter("animatedScore")
fun TextView.setAnimatedScore(score: Int) {
    val anim = ValueAnimator.ofInt(0, score)
    anim.addUpdateListener { valueAnimator ->
        val currentScore = valueAnimator.animatedValue as Int
        this.text = currentScore.toString()
    }

    // this is random number for the duration based on the score coordinated with the iOS team to make the same animation duration
    val durationInMilliseconds = 52.2
    anim.duration = (score * durationInMilliseconds).toLong()
    anim.start()
}
