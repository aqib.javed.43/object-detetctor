package com.vessel.app.common.net.service

import com.vessel.app.common.net.data.*
import com.vessel.app.supplement.model.AddSupplementIngredientIdRequest
import com.vessel.app.vesselfuelcheckout.model.request.VesselFuelItemRequest
import retrofit2.http.*

interface SupplementsService {
    @GET("supplement")
    suspend fun getSupplements(): SupplementDataResponse

    @GET("memberships")
    suspend fun getMemberships(): List<MembershipData>

    @GET("memberships/addresses")
    suspend fun getMembershipsAddresses(): MembershipAddressDataResponse

    @GET("memberships/payment")
    suspend fun getMembershipsPayment(): MembershipPaymentDataResponse

    @POST("memberships")
    suspend fun checkoutVesselFuel(@Body request: VesselFuelItemRequest)

    @POST("plan/supplement")
    suspend fun addIngredientsToSupplements(@Body request: AddSupplementIngredientIdRequest)

    @DELETE("plan/supplement/{ingredientId}")
    suspend fun deleteIngredientsFromSupplements(@Path("ingredientId") id: Int)

    @GET("memberships")
    suspend fun getMembership(@Query("email") email: String): MembershipData
}
