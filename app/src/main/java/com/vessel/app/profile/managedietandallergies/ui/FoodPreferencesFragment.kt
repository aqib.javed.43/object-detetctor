package com.vessel.app.profile.managedietandallergies.ui

import android.os.Bundle
import androidx.core.view.ViewCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.profile.managedietandallergies.FoodPreferencesViewModel
import com.vessel.app.profile.managedietandallergies.model.FoodPreferencesTab
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FoodPreferencesFragment : BaseFragment<FoodPreferencesViewModel>() {
    override val viewModel: FoodPreferencesViewModel by viewModels()
    override val layoutResId = R.layout.fragment_food_preferences
    private val foodFilterAdapter by lazy { FoodPreferencesAdapter(viewModel) }
    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabSelected(tab: TabLayout.Tab?) {
            tab?.position?.let { position ->
                viewModel.onTabSelected(position)
                view?.findViewById<RecyclerView>(R.id.foodPreferencesRecyclerView)?.let {
                    ViewCompat.setNestedScrollingEnabled(it, false)
                }
            }
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabReselected(tab: TabLayout.Tab?) {
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
        setupViews()
    }

    private fun setupViews() {
        view?.findViewById<TabLayout>(R.id.foodPreferencesTablayout)?.apply {
            addOnTabSelectedListener(onTabSelectedListener)
            addTab(this.newTab().setText(FoodPreferencesTab.DIETS.title))
            addTab(this.newTab().setText(FoodPreferencesTab.ALLERGIES.title))
        }
        view?.findViewById<RecyclerView>(R.id.foodPreferencesRecyclerView)?.apply {
            adapter = foodFilterAdapter
            itemAnimator = null
        }
    }
    private fun setupObservers() {
        viewModel.state {
            observe(tabList) {
                foodFilterAdapter.submitList(it)
            }
        }
    }

    override fun onPause() {
        viewModel.onScreenClosed()
        super.onPause()
    }

    override fun onDestroyView() {
        view?.findViewById<TabLayout>(R.id.foodPreferencesTablayout)
            ?.removeOnTabSelectedListener(onTabSelectedListener)
        super.onDestroyView()
    }
}
