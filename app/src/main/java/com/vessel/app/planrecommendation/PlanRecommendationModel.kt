package com.vessel.app.planrecommendation

import androidx.annotation.DrawableRes
import com.vessel.app.wellness.RecommendationType

data class PlanRecommendationModel(
    @DrawableRes val icon: Int,
    val title: String,
    val description: String,
    val type: RecommendationType
)
