package com.vessel.app.photocapture.processing.errordialog

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.ActivityNavigator
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity

@AndroidEntryPoint
class ProcessingErrorDialogFragment : BaseDialogFragment<ProcessingErrorDialogViewModel>() {
    override val viewModel: ProcessingErrorDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_processing_error_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
            setGravity(Gravity.BOTTOM)
        }
        isCancelable = false

        observe(viewModel.navigateToHomeScreen) {
            dismiss()
            val extras = ActivityNavigator.Extras.Builder()
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .build()
            findNavController().navigate(it, extras)
            requireActivity().finish()
        }
        observe(viewModel.navigateTo) {
            dismiss()
            findNavController().navigate(it)
        }
        observe(viewModel.openChat) {
            MessagingActivity.builder()
                .withToolbarTitle(getString(R.string.customer_support))
                .withEngines(ChatEngine.engine())
                .show(requireContext(), viewModel.getChatConfiguration())
        }
    }
}
