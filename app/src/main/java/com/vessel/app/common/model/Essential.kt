package com.vessel.app.common.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
@Parcelize
@JsonClass(generateAdapter = true)
data class Essential(
    var title: String? = null,
    var link_url: String? = null,
    var link_text: String? = null,
    var description: String? = null,
    var days: List<Int>? = arrayListOf(),
    var id: Int? = null,
    var essential_record: List<EssentialRecord>? = arrayListOf(),
    var programTitle: String? = null,
    var completed: Boolean = false
) : Parcelable {
    fun checkCompleted(): Boolean {
        var isCompleted = true
        essential_record?.let {
            for (er in it) {
                if (!er.completed) isCompleted = false
            }
        }
        return isCompleted
    }
}

@Parcelize
@JsonClass(generateAdapter = true)
data class EssentialRecord(
    var id: Int?,
    var date: String?, // in "yyyy-MM-dd" format
    var completed: Boolean = false,
    var program_day: Int,
    var essential_id: Int,
    var contact_id: Int,
) : Parcelable
