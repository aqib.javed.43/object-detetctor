package com.vessel.app.photocapture.previewcaptureimage

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.photocapture.previewcaptureimage.ui.PreviewCaptureImageFragmentDirections
import com.vessel.app.util.ResourceRepository
import java.util.*

class PreviewCaptureImageViewModel @ViewModelInject constructor(
    val state: PreviewCaptureImageState,
    resourceRepository: ResourceRepository,
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository) {
    var uuid: String? = null

    fun onRetakeButtonClicked() {
        logImageEvent(TrackingConstants.SAMPLE_IMAGE_RETAKE)
        state.navigateTo.value =
            PreviewCaptureImageFragmentDirections.actionPreviewCaptureImageFragmentToCapture()
    }

    fun onContinueButtonClicked() {
        logImageEvent(TrackingConstants.SAMPLE_IMAGE_CONFIRMED)
        state.navigateTo.value =
            PreviewCaptureImageFragmentDirections.actionPreviewCaptureImageFragmentToProcessing()
    }

    private val startTestTimeInMilli = savedStateHandle.get<Long>("startTestTimeInMilli")!!

    private fun logImageEvent(eventName: String) {
        uuid?.let {
            trackingManager.log(
                TrackedEvent.Builder(eventName)
                    .addProperty(TrackingConstants.KEY_UUID, it)
                    .addProperty(
                        TrackingConstants.KEY_ATTEMPT_TIME_MS,
                        (Calendar.getInstance().timeInMillis - startTestTimeInMilli).toString()
                    )
                    .withKlaviyo()
                    .withAmplitude()
                    .withFirebase()
                    .create()
            )
        }
    }
}
