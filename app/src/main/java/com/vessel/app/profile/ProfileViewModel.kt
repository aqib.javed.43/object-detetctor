package com.vessel.app.profile

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class ProfileViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository
) : BaseViewModel(resourceRepository)
