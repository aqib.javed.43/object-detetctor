package com.vessel.app.covid19

import androidx.lifecycle.MutableLiveData
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CovidState @Inject constructor() {
    val items = MutableLiveData<List<Any>>()
    val scrollTo = LiveEvent<Int>()

    operator fun invoke(block: CovidState.() -> Unit) = apply(block)
}
