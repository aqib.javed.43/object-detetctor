package com.vessel.app.reagent.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.reagent.model.HighValueFood

class HighValueFoodAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is HighValueFood -> R.layout.item_high_value_food
        else -> throw IllegalStateException(
            "Unexpected WellnessFooterAdapter type at position $position for item ${getItem(position)}"
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
        super.onBindViewHolder(holder, position)
        if (position % 2 == 1) {
            holder.itemView.translationY = 50.0f
        }
    }
    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onViewFoodPlan()
    }
}
