package com.vessel.app.util.extensions

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.View
import com.vessel.app.Constants

fun Activity.openSystemSettings(requestCode: Int = Constants.OPEN_APP_SETTINGS_REQUEST_CODE) {
    startActivityForResult(
        Intent().apply {
            action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            data = Uri.fromParts("package", packageName, null)
        },
        requestCode
    )
}

fun Activity.applyFullscreenFlags() {
    window.decorView.systemUiVisibility = (
        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        )
}
