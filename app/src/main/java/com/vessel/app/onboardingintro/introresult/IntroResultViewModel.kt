package com.vessel.app.onboardingintro.introresult

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository

class IntroResultViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider),
    ToolbarHandler {

    val state = IntroResultState()
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
    fun onDoneClicked() {
        state.navigateTo.value = IntroResultFragmentDirections.actionIntroResultFragmentToIntroProgramFragment()
    }
}
