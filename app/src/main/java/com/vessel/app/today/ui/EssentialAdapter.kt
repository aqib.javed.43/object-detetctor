package com.vessel.app.today.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Essential
import kotlinx.android.synthetic.main.item_program_essentials.view.*

class EssentialAdapter(
    private val handler: OnActionHandler,
    private val parentPosition: Int,
) :
    BaseAdapterWithDiffUtil<Essential>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is Essential -> R.layout.item_program_essentials
        else -> throw IllegalStateException(
            "Unexpected WellnessAdapter type at position $position for item ${
            getItem(
                position
            )
            }"
        )
    }
    interface OnActionHandler {
        fun onEssentialSelected(item: Essential)
        fun showEssentialInformation()
        fun onEssentialCompleted(item: Essential, view: View, parentPosition: Int)
        fun onEssentialLinkSelected(item: Essential, view: View)
    }
    override fun onBindViewHolder(holder: BaseViewHolder<Essential>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position)
        item.days?.let {
            if (it.isNotEmpty()) {
                holder.itemView.valueDay.text = "Day ${it[0]}"
            }
        }
        holder.itemView.checkbox.setOnClickListener {
            handler.onEssentialCompleted(item, it, parentPosition)
        }
        holder.itemView.lblLink.setOnClickListener {
            handler.onEssentialLinkSelected(item, it)
        }
    }
}
