package com.vessel.app.score.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.score.model.ScoreGoalHeaderItem

class ScoreGoalHeaderAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<ScoreGoalHeaderItem>() {
    override fun getLayout(position: Int) = R.layout.item_score_goal_header

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onGoalScoreHeaderToolTipClicked(view: View)
    }
}
