package com.vessel.app.wellness.net.data

enum class LikeStatus(val value: String) {
    LIKE("like"),
    DISLIKE("dislike"),
    NO_ACTION("N/A");

    companion object {
        fun fromBoolean(value: Boolean): LikeStatus = if (value) LIKE else DISLIKE
        fun from(state: String?) = LikeStatus.values().firstOrNull { it.value == state } ?: NO_ACTION
    }
}
