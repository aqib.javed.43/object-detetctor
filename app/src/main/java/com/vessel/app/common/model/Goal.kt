package com.vessel.app.common.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.model.data.ReagentItem.*

sealed class Goal(
    val id: Int,
    @StringRes val title: Int,
    @StringRes val recommendation1: Int,
    @StringRes val recommendation2: Int,
    @DrawableRes val image: Int,
    val reagents: Set<ReagentItem>,
    @DrawableRes val icon: Int,
    @DrawableRes val formulaIcon: Int,
) {

    object Mood : Goal(1, R.string.mood, R.string.mood_recommendation_1, R.string.mood_recommendation_2, R.drawable.goals_mood, setOf(Hydration, Ketones, VitaminC, Magnesium, B9, Cortisol, B7, Calcium, Sodium, Nitrites, Chloride), R.drawable.ic_goal_mood, R.drawable.ic_formula_goal_mood)
    object Focus : Goal(2, R.string.focus, R.string.focus_recommendation_1, R.string.focus_recommendation_2, R.drawable.goals_focus, setOf(Hydration, Ketones, VitaminC, Magnesium, B9, Cortisol, Chloride), R.drawable.ic_goal_focus, R.drawable.ic_formula_goal_focus)
    object Calm : Goal(3, R.string.calm, R.string.calm_recommendation_1, R.string.calm_recommendation_2, R.drawable.goals_calm, setOf(Hydration, Ketones, VitaminC, Magnesium, Cortisol, Calcium, Sodium, Nitrites, Chloride), R.drawable.ic_goal_calm, R.drawable.ic_formula_goal_calm)
    object Sleep : Goal(4, R.string.sleep, R.string.sleep_recommendation_1, R.string.sleep_recommendation_2, R.drawable.goals_sleep, setOf(Hydration, Ketones, VitaminC, Magnesium, B9, Cortisol, Calcium, Sodium, Nitrites, Chloride), R.drawable.ic_goal_sleep, R.drawable.ic_formula_goal_sleep)
    object Energy : Goal(5, R.string.energy, R.string.energy_recommendation_1, R.string.energy_recommendation_2, R.drawable.goals_energy, setOf(PH, Hydration, Ketones, VitaminC, Magnesium, B9, Cortisol, B7, Calcium, Sodium, Nitrites, Chloride), R.drawable.ic_goal_energy, R.drawable.ic_formula_goal_energy)
    object Body : Goal(6, R.string.body, R.string.body_recommendation_1, R.string.body_recommendation_2, R.drawable.goals_body, setOf(PH, Hydration, Ketones, VitaminC, Magnesium, Cortisol, B7, Calcium, Sodium, Nitrites, Chloride), R.drawable.ic_goal_body, R.drawable.ic_formula_goal_body)
    object Beauty : Goal(7, R.string.beauty, R.string.beauty_recommendation_1, R.string.beauty_recommendation_2, R.drawable.goals_beauty, setOf(Hydration, Ketones, VitaminC, Magnesium, B9, Cortisol, B7, Calcium, Sodium, Nitrites, Chloride), R.drawable.ic_goal_beauty, R.drawable.ic_formula_goal_beauty)
    object Immunity : Goal(8, R.string.immunity, R.string.immunity_recommendation_1, R.string.immunity_recommendation_2, R.drawable.goals_immunity, setOf(Ketones, VitaminC, Magnesium, B9, Cortisol, B7, Calcium, Sodium, Nitrites, Chloride), R.drawable.ic_goal_immunity, R.drawable.ic_formula_goal_immunity)
    object Digestion : Goal(9, R.string.digestion, R.string.digestion_recommendation_1, R.string.digestion_recommendation_2, R.drawable.goals_digestion, setOf(Cortisol, Magnesium, Hydration, Calcium, Sodium, Nitrites, Chloride), R.drawable.ic_goal_digestion, R.drawable.ic_formula_goal_digestion)
    object Endurance : Goal(10, R.string.endurance, R.string.fitness_recommendation_1, R.string.fitness_recommendation_2, R.drawable.goals_endurance, setOf(PH, Hydration, Ketones, VitaminC, Magnesium, B9, Cortisol, Calcium, Sodium, Nitrites, Chloride), R.drawable.ic_goal_fitness, R.drawable.ic_formula_goal_fitness)
    object Wellness : Goal(11, R.string.wellness, 0, 0, R.drawable.goals_wellness, setOf(PH, Hydration, Ketones, VitaminC, Magnesium, B9, Cortisol, B7, Calcium, Sodium, Nitrites, Chloride), R.drawable.goals_wellness, R.drawable.ic_formula_goal_vitamin)

    companion object {
        fun fromId(id: Int) = values().firstOrNull { it.id == id }

        fun values() = listOf(
            Focus,
            Beauty,
            Immunity,
            Body,
            Sleep,
            Energy,
            Endurance,
            Calm,
            Mood,
            Digestion
        )

        fun alternateBackground(position: Int) = when (position % 3) {
            0 -> R.drawable.swirl_background
            1 -> R.drawable.primary_background
            else -> R.drawable.rose_background
        }
    }
}
