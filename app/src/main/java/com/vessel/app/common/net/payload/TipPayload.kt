package com.vessel.app.common.net.payload

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TipPayload(
    var tag_ids: List<Int> = arrayListOf(),
    var level: String = "",
    var goal_id: Int,
    var exclude_added: Boolean,
    var bad_habit_ids: List<Int> = arrayListOf()
)
