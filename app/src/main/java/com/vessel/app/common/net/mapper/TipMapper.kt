package com.vessel.app.common.net.mapper

import com.vessel.app.Constants
import com.vessel.app.common.model.*
import com.vessel.app.common.net.data.*
import com.vessel.app.common.net.data.Diet
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.repo.ContactRepository
import com.vessel.app.wellness.model.*
import com.vessel.app.wellness.net.data.DifficultyLevel
import com.vessel.app.wellness.net.data.LikeStatus
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TipMapper @Inject constructor(
    private val scienceSourceMapper: ScienceSourceMapper,
    private val goalsMapper: GoalsMapper,
) {
    fun map(data: TipsResponse) = RecommendationsPagingResponse(
        tips = data.tips.map { mapTipData(it) },
        foods = data.foods,
        lifestyle = data.lifestyle.map { mapLifestyle(it) },
        sequenceData = data.data.map { mapTipSequence(it) },
        paging = map(data.pagination)
    )

    fun map(data: PagingData) = Paging(
        currentPage = data.page,
        pageSize = data.per_page
    ).apply {
        totalSize = data.total
        totalPages = data.pages
    }

    private fun mapTipSequence(data: TipSortingData) = RecommendationsSequence(
        id = data.id,
        type = RecommendationsSequenceType.from(data.type)
    )

    fun mapTipData(data: TipData) = Tip(
        id = data.tip_id,
        title = data.title,
        frequency = data.frequency,
        description = data.description ?: "",
        imageUrl = data.image_url,
        likesCount = data.total_likes,
        dislikesCount = data.dislikes_count,
        likeStatus = LikeStatus.from(data.like_status),
        isAddedToPlan = false,
        similar = null,
        contact = data.contact?.let { mapContact(it) },
        sources = data.sources?.map { scienceSourceMapper.map(it) },
        impactsGoals = data.goals,
        mainGoalId = data.main_goal_id
    )

    fun mapTip(data: Tip) = TipData(
        tip_id = data.id,
        title = data.title,
        frequency = data.frequency,
        description = data.description,
        image_url = data.imageUrl,
        total_likes = data.likesCount,
        dislikes_count = data.dislikesCount,
        like_status = data.likeStatus.value,
        contact = data.contact?.let { mapContactData(it) },
        sources = data.sources?.map { scienceSourceMapper.map(it) },
        goals = data.impactsGoals,
        main_goal_id = data.mainGoalId
    )

    private fun mapLifestyle(data: LifestyleData) = Lifestyle(
        id = data.id,
        activityName = data.activity_name,
        quantity = data.quantity,
        extraImages = data.extra_images,
        description = data.description,
        frequency = data.frequency,
        imageUrl = data.image_url,
        totalLikes = data.total_likes,
        totalDislikes = data.dislikes_count,
        likeStatus = LikeStatus.from(data.like_status),
        reagentBucketId = data.reagent_bucket_id,
        unit = data.unit,
        lifestyleRecommendationId = data.lifestyle_recommendation_id ?: 0,
        isAddedToPlan = false,
        impactsGoals = data.impactsGoals
    )

    private fun mapContact(data: ContactData): Contact {
        return Contact(
            id = data.id,
            email = data.email.orEmpty(),
            firstName = data.first_name,
            lastName = data.last_name,
            lastLogin = data.last_login?.let { ContactRepository.SERVER_LAST_LOGIN_FORMAT.parse(it) }
                ?: Date(),
            insertDate = data.insert_date?.let { ContactRepository.SERVER_LAST_LOGIN_FORMAT.parse(it) }
                ?: Date(),
            gender = data.gender?.let { Contact.Gender.fromEncoding(it) },
            height = data.height?.let { Contact.Height.fromCentimeters(it) },
            weight = data.weight?.let { Contact.Weight.fromKg(it) },
            birthDate = data.birth_date?.let { ContactRepository.SERVER_DATE_FORMAT.parse(it) },
            allergies = data.allergies?.map { it.title },
            diets = data.diets?.map {
                com.vessel.app.common.model.Diet.fromApiName(
                    it.title ?: ""
                )
            },
            goals = data.goals?.let { goalsMapper.map(it) },
            time_zone = data.time_zone,
            tips = data.tips?.map { mapTipData(it) },
            occupation = data.occupation,
            imageUrl = data.image_url,
            isVerified = data.is_verified,
            about = data.description,
            location = data.location_description,
            surveyStatus = data.survey_status,
            testsTaken = data.tests_taken,
            programs = data.programs?.map { mapProgramToContact(it) },
            mainGoalId = data.main_goal_id
        )
    }

    private fun mapContactData(data: Contact): ContactData {
        return ContactData(
            id = data.id,
            email = data.email.orEmpty(),
            first_name = data.firstName,
            last_name = data.lastName,
            last_login = data.lastLogin.let { ContactRepository.SERVER_LAST_LOGIN_FORMAT.format(it) }
                ?: "",
            insert_date = data.insertDate.let { ContactRepository.SERVER_LAST_LOGIN_FORMAT.format(it) }
                ?: "",
            gender = data.gender?.encoding,
            height = data.height?.toCentimeters()?.toFloat(),
            weight = data.weight?.toKg(),
            birth_date = data.birthDateString,
            allergies = data.allergies?.map { Allergy(title = it, description = null) },
            diets = data.diets?.map { Diet(it.apiName) },
            goals = data.goals?.let { goalsMapper.mapToGoalData(it) },
            time_zone = data.time_zone,
            tips = data.tips?.map { mapTip(it) },
            occupation = data.occupation,
            image_url = data.imageUrl,
            is_verified = data.isVerified,
            description = data.about,
            location_description = data.location,
            survey_status = data.surveyStatus,
            tests_taken = data.testsTaken,
            programs = data.programs?.map { mapProgramToContact(it) },
            main_goal_id = data.mainGoalId
        )
    }

    private fun mapProgramToContact(program: ProgramData) = Program(
        id = program.id ?: 0,
        title = program.title.orEmpty(),
        likeStatus = LikeStatus.from(program.like_status),
        totalLikes = program.total_likes ?: 0,
        totalDislikes = program.dislikes_count ?: 0,
        contact = null,
        contactId = program.contact_id ?: 0,
        createdAt = program.created_at.orEmpty(),
        description = program.description.orEmpty(),
        difficulty = DifficultyLevel.from(program.difficulty.orEmpty()),
        durationDays = program.duration_days ?: 0,
        imageUrl = program.image_url.orEmpty(),
        isEnrolled = program.is_enrolled ?: false,
        mainGoalId = program.main_goal_id ?: 0,
        reviewedContactIds = program.reviewed_contact_ids,
        goals = program.impactsGoals,
        sources = program.sources?.map { scienceSourceMapper.map(it) },
        planData = program.plans?.toMutableList(),
        enrolledDate = program.enrolled_date?.let { Constants.DAY_DATE_FORMAT.parse(it) },
        frequency = program.frequency ?: 0,
        schedule = program.schedule?.map { mapScheduleProgram(it, program.plans?.toList()) }
    )

    private fun mapProgramToContact(program: Program) = ProgramData(
        id = program.id,
        title = program.title,
        like_status = program.likeStatus.value,
        total_likes = program.totalLikes,
        dislikes_count = program.totalDislikes ?: 0,
        contact = null,
        contact_id = program.contactId,
        created_at = program.createdAt,
        description = program.description,
        difficulty = program.difficulty.apiValue,
        duration_days = program.durationDays,
        image_url = program.imageUrl,
        is_enrolled = program.isEnrolled,
        main_goal_id = program.mainGoalId,
        reviewed_contact_ids = program.reviewedContactIds,
        impactsGoals = program.goals,
        sources = program.sources?.map { scienceSourceMapper.map(it) },
        plans = program.planData,
        enrolled_date = program.enrolledDate?.let { Constants.DAY_DATE_FORMAT.format(it) },
        frequency = program.frequency,
        schedule = program.schedule?.map { mapReversedScheduleProgram(it) }
    )

    fun mapScheduleProgram(scheduleProgramData: ScheduleProgramData, planData: List<PlanData>?) =
        ScheduleItem(
            day = scheduleProgramData.day,
            plans = scheduleProgramData.plan_ids.mapNotNull { planId ->
                planData?.firstOrNull { it.id == planId }?.let { plan ->
                    SchedulePlanItem(
                        planId,
                        plan.getDisplayName(),
                        plan.time_of_day,
                        false
                    )
                }
            }
        )

    private fun mapReversedScheduleProgram(schedule: ScheduleItem) =
        ScheduleProgramData(
            day = schedule.day,
            plan_ids = schedule.plans.map { it.planId }
        )
}
