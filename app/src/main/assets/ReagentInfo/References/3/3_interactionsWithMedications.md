-   Some Sulfur Containing Medications: Medications with a sulfhydryl group such as captopril, penicillamine, and mesna can cause a false elevation of your ketone result.

-   Liquid Medications: These often have added sweeteners which may add several grams of carbohydrates each day that may make achieving nutritional ketosis more difficult.
