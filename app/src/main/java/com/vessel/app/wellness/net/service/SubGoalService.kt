package com.vessel.app.wellness.net.service

import com.vessel.app.wellness.net.data.SubGoalResponseData
import retrofit2.http.GET
import retrofit2.http.Query

interface SubGoalService {
    @GET("subgoal")
    suspend fun getAllSubGoals(
        @Query("goal_id") goalId: Int?
    ): SubGoalResponseData
}
