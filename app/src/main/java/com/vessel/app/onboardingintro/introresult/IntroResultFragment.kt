package com.vessel.app.onboardingintro.introresult

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class IntroResultFragment : BaseFragment<IntroResultViewModel>() {
    override val viewModel: IntroResultViewModel by viewModels()
    override val layoutResId = R.layout.fragment_onboarding_intro_result
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
        }
    }
}
