package com.vessel.app.common.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SchedulePlanItem(
    val planId: Int,
    val title: String,
    private val timeOfDay: String?,
    val completed: Boolean
) : Parcelable {

    fun getTimeOfTheDay() = timeOfDay ?: "Set Time"
}
