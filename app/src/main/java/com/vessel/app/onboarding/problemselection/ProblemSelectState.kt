package com.vessel.app.onboarding.problemselection

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.onboarding.problemselection.model.ProblemSelect
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ProblemSelectState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val items = MutableLiveData<List<ProblemSelect>>()
    operator fun invoke(block: ProblemSelectState.() -> Unit) = apply(block)
}
