package com.vessel.app.appreview.programtip

import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.NavDirections
import com.vessel.app.appreview.model.ReviewState
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.MembershipManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.remoteconfig.RemoteConfiguration

class ProgramTipDialogViewModel @ViewModelInject constructor(
    var state: ReviewState,
    resourceProvider: ResourceRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
    val membershipManager: MembershipManager,
    val remoteConfiguration: RemoteConfiguration
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    var navigateTo = LiveEvent<NavDirections>()
    fun dismiss() {
        devicePreferencesRepository.showAppReviewPopup = false
        dismissDialog.call()
    }
    fun onChatTonNtritionistClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TALK_TO_NUTRITIONIST_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.openMessaging.call()
    }
    fun onTipsClicked() {
        state.openTips.value = tipsUrl
    }
    fun getChatConfiguration() =
        membershipManager.getChatConfiguration(MembershipManager.ZENDESK_NUTRITIONIST_DEPARTMENT)
    fun isLiveChat() = remoteConfiguration.getBoolean("use_livechat")
    fun getUserName() = preferencesRepository.userFirstName
    fun getUserEmail() = preferencesRepository.userEmail
    companion object {
        const val tipsUrl = "https://help.vesselhealth.com/hc/en-us/articles/4419528536855"
    }
}
