package com.vessel.app.triallockerpopup

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.TrailLockItem
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class TrialLockerPopUpState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val isTrialLocked = MutableLiveData<Boolean>()
    val trialLockItem = MutableLiveData<TrailLockItem>()
    operator fun invoke(block: TrialLockerPopUpState.() -> Unit) = apply(block)
}
