package com.vessel.app.cardchoices.nocard

import android.text.*
import android.view.View
import androidx.core.text.*
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavDirections
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.util.CustomTypefaceSpan
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.makeClickableSpan

class NoCardChoiceViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    private val trackingManager: TrackingManager,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceRepository) {
    val email = savedStateHandle.get<String>("email")!!
    val navigateTo = LiveEvent<NavDirections>()

    fun continueClicked() {
        logContinueEvent()
        navigateTo.value =
            NoCardChoiceFragmentDirections.actionNoCardChoiceFragmentToWeb(BuildConfig.WELLNESS_QUIZ_URL)
    }

    private fun logContinueEvent() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.SIGN_UP_BUY_ON_WEB)
                .setEmail(email)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun subTitleTextFormat(): SpannedString {
        val prefix = getResString(R.string.no_card_choice_subtitle_text_prefix)
        val boldPart = getResString(R.string.chat_with_support)
        return buildSpannedString {
            append(prefix)
            append(" ")
            inSpans(CustomTypefaceSpan("", getFont(R.font.bananagrotesk_bold)!!)) {
                append(
                    boldPart.makeClickableSpan(
                        Pair(
                            boldPart,
                            View.OnClickListener {
                                navigateTo.value =
                                    NoCardChoiceFragmentDirections.actionNoCardChoiceFragmentToWeb(
                                        BuildConfig.HELP_CENTER_URL
                                    )
                            }
                        )
                    )
                )
            }
        }
    }

    fun onBackButtonClicked() {
        navigateBack()
    }
}
