package com.vessel.app.appreview.review.model

import androidx.navigation.NavDirections

data class ReviewItem(
    val title: String = "",
    val action: NavDirections? = null,
    var isSelected: Boolean = false

)
