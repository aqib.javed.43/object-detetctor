package com.vessel.app.wellness.model

import androidx.annotation.*
import com.vessel.app.R
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.Goal
import kotlin.math.roundToInt

data class GoalButton(
    val id: Int,
    @StringRes val title: Int,
    val chartEntries: List<DateEntry>,
    val averageChartEntries: List<DateEntry>,
    @DrawableRes val image: Int,
    val selected: Boolean,
    var isPreviousSelected: Boolean = false,
    var isAnimatedBefore: Boolean = false,
    var isProgressAnimatedBefore: Boolean = false,
    val selectedChartEntityPosition: Int,
    val smallImageUrl: String = "",
    val tipCount: Int = 0
) {
    val score =
        if (chartEntries.isEmpty() || chartEntries.lastIndex < selectedChartEntityPosition) {
            ""
        } else {
            chartEntries[selectedChartEntityPosition].y.roundToInt().toString()
        }

    val scoreValue =
        if (chartEntries.isEmpty() || chartEntries.lastIndex < selectedChartEntityPosition) {
            0
        } else chartEntries[selectedChartEntityPosition].y.roundToInt()

    @DrawableRes
    val background = if (selected) {
        R.drawable.goal_button_selected_background
    } else {
        R.drawable.goal_button_unselected_background
    }

    @DimenRes
    val selectedButtonHeight = R.dimen.goal_button_selected_height
    @DimenRes
    val unselectedButtonHeight = R.dimen.goal_button_unselected_height

    @DimenRes
    val scoreTextSize = if (selected) {
        R.dimen.goal_button_selected_text_size
    } else {
        R.dimen.goal_button_unselected_text_size
    }

    @ColorRes
    val textColor = if (selected) {
        R.color.white
    } else {
        R.color.black
    }

    @DrawableRes
    fun getAssessmentImageRes(): Int {
        var lastScoreValue = scoreValue
        if (GoalRate.valueOf(scoreValue) == GoalRate.UNKNOWN && chartEntries.size > selectedChartEntityPosition - 1 && selectedChartEntityPosition - 1 >= 0)
            lastScoreValue = chartEntries[selectedChartEntityPosition - 1].y.roundToInt()
        return getAssessmentImageRes(lastScoreValue)
    }

    @DrawableRes
    private fun getAssessmentImageRes(scoreValue: Int) = when (GoalRate.valueOf(scoreValue)) {
        GoalRate.SAD -> R.drawable.sad_face_brown_without_background
        GoalRate.MEH -> R.drawable.normal_face_yellow_without_background
        GoalRate.HAPPY -> R.drawable.smile_face_green_without_background
        else -> R.drawable.nothing_face_without_background
    }

    @IdRes
    val checkedRate = when (GoalRate.valueOf(scoreValue)) {
        GoalRate.SAD -> R.id.sadFaceRadioButton
        GoalRate.MEH -> R.id.normalFaceRadioButton
        GoalRate.HAPPY -> R.id.happyFaceRadioButton
        else -> null
    }

    fun getRateApiValue() = if (GoalRate.valueOf(scoreValue) != GoalRate.UNKNOWN) {
        GoalRate.valueOf(scoreValue).name
    } else {
        val preIndex = selectedChartEntityPosition - 1
        val preScoreValue: Int
        if (preIndex >= 0 && preIndex < chartEntries.lastIndex) {
            preScoreValue = chartEntries[preIndex].y.roundToInt()
            if (GoalRate.valueOf(preScoreValue) != GoalRate.UNKNOWN)
                GoalRate.valueOf(preScoreValue).name
            else
                null
        } else
            null
    }

    val isNotWellness = (id != Goal.Wellness.id)
    val enableGoalRateSelector = (selectedChartEntityPosition == chartEntries.lastIndex)
}

enum class GoalRate(val value: Int) {
    UNKNOWN(-1), SAD(0), MEH(50), HAPPY(100);

    companion object {
        fun valueOf(status: Int?) = when (status) {
            0 -> SAD
            50 -> MEH
            100 -> HAPPY
            else -> UNKNOWN
        }
    }
}
