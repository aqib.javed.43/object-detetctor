package com.vessel.app.views

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

class ExpandableTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
) : AppCompatTextView(context, attrs, defStyle) {

    var collapsedLineCount = 5
    var isExpanded = false
        private set
    var targetLineCount = 0

    private var onStateChangeListener: (ExpandableTextView.(oldState: State, newState: State) -> Unit)? =
        null

    var state = State.Static
        private set(value) {
            if (field != value) {
                onStateChangeListener?.let { it(field, value) }
                field = value
            }
        }

    override fun setText(text: CharSequence?, type: BufferType?) {
        val textChanged = this.text != text
        super.setText(text, type)
        if (textChanged) {
            if (state != State.Expanded) {
                maxLines = collapsedLineCount
                isExpanded = false
            } else {
                maxLines = targetLineCount + collapsedLineCount
            }
        }
    }

    fun toggleExpanded() {
        setExpanded(!isExpanded)
    }

    private fun setExpanded(expand: Boolean) {
        if (isExpanded == expand) {
            return
        }
        isExpanded = expand
        if (expand) {
            targetLineCount = lineCount
            state = State.Expanded
        } else {
            targetLineCount = collapsedLineCount
            state = State.Collapsed
        }
        ObjectAnimator.ofInt(this, "maxLines", targetLineCount)
            .setDuration(100)
            .start()
    }

    override fun onSaveInstanceState(): Parcelable = super.onSaveInstanceState()?.let {
        SavedState(it).apply {
            state = this@ExpandableTextView.state
            lineCount = this@ExpandableTextView.targetLineCount
        }
    }!!

    override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as SavedState
        super.onRestoreInstanceState(state.superState)
        if (ss.state == State.Expanded) {
            isExpanded = true
            this.state = ss.state
        }
        targetLineCount = ss.lineCount
        post {
            when (ss.state) {
                State.Collapsed, State.Expanded -> updateState(restoredState = ss.state)
                else -> return@post
            }
        }
    }

    class SavedState(superState: Parcelable) : BaseSavedState(superState) {
        var state: State = State.Collapsed
        var lineCount = 0

        @SuppressLint("NewApi")
        override fun writeToParcel(dest: Parcel, flags: Int) {
            super.writeToParcel(dest, flags)
            dest.writeString(state.name)
            dest.writeInt(lineCount)
        }
    }

    private fun updateState(restoredState: State? = null) {
        if (restoredState == State.Collapsed) setExpanded(false)
        else if (restoredState == State.Expanded) setExpanded(true)
    }

    enum class State {
        Collapsed,
        Expanded,
        Static
    }
}
