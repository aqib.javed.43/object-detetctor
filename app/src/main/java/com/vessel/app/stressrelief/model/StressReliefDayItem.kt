package com.vessel.app.stressrelief.model

data class StressReliefDayItem(
    val title: String,
    val items: List<StressReliefViewItem>,
    val isDailyItem: Boolean
)
