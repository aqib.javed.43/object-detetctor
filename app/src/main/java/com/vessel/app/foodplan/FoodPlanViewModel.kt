package com.vessel.app.foodplan

import android.icu.text.NumberFormat
import android.text.SpannableString
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.checkscience.ui.OnActionHandler
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.FoodRecommendation
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.data.Reagent
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.model.data.ReagentState
import com.vessel.app.common.net.data.plan.CreatePlanRequest
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.toPlanRequest
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.foodplan.model.*
import com.vessel.app.foodplan.ui.*
import com.vessel.app.plan.ui.AddPlanFooterAdapter
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.*
import com.vessel.app.views.weekdays.getPlanDaysOfWeek
import com.vessel.app.views.weekdays.getPlanDaysOfWeekSorted
import com.vessel.app.wellness.net.data.LikeCategory
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*
import kotlin.math.max

class FoodPlanViewModel @ViewModelInject constructor(
    val state: FoodPlanState,
    private val resourceProvider: ResourceRepository,
    private val foodManager: FoodManager,
    private val scoreManager: ScoreManager,
    private val planManager: PlanManager,
    private val reminderManager: ReminderManager,
    private val contactManager: ContactManager,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    FoodRecommendationAdapter.FoodRecommendationHandler,
    FoodPlanHeaderAdapter.FoodHeaderHandler,
    FoodTabsHeaderAdapter.FoodTabsHeaderHandler,
    FoodItemsAdapter.OnActionHandler,
    FoodNutrientNeedHeaderAdapter.FoodNutrientNeedHandler,
    AddPlanFooterAdapter.OnActionHandler,
    OnActionHandler {
    private var todos = mutableListOf<PlanData>()
    private lateinit var recommendationsSelected: LinkedList<FoodRecommendation>
    private lateinit var recommendationsNotSelected: LinkedList<FoodRecommendation>
    private lateinit var reagentDeficiencies: Map<Reagent, Float>

    init {
        state.foodTabs.value = listOf(TabsHeaderItem(true))
        observeDietAllergiesState()
        observeFoodRecommendationsState()
        observeRemindersChanges()
        loadFoodPlans()
    }

    private fun loadFoodPlans(forceUpdate: Boolean = false) {
        showLoading()
        viewModelScope.launch {
            val foodRecommendationsResponse = foodManager.getRecommendedFoodItems()
            val plansResponse = planManager.getUserPlans(forceUpdate)
            val reagentDeficienciesResult = scoreManager.reagentDeficiencies()
            if (foodRecommendationsResponse is Result.Success && plansResponse is Result.Success && reagentDeficienciesResult is Result.Success) {
                hideLoading(false)
                val foodPlans = plansResponse.value.filter { it.isFoodPlan() }
                todos = foodPlans.toMutableList()
                val recommendations = foodRecommendationsResponse.value

                state.foodTabs.value = state.foodTabs.value.orEmpty().map {
                    it.copy(showTabs = foodPlans.isNotEmpty())
                }
                setupDailyData(forceUpdate, foodPlans, recommendations)
                setupWeeklyData(forceUpdate, foodPlans, recommendations)
                val filteredFoodList = foodRecommendationsResponse.value
                reagentDeficiencies = reagentDeficienciesResult.value
                val pair = mapFoodRecommendations(filteredFoodList)
                recommendationsSelected = pair.first
                recommendationsNotSelected = pair.second

                state.recommendedFoodsSelected.postValue(
                    mapFoodRecommendationViewItems(
                        recommendationsSelected,
                        true
                    )
                )
                state.recommendedFoodsNotSelected.postValue(
                    mapFoodRecommendationViewItems(
                        recommendationsNotSelected,
                        false
                    )
                )

                state.levels.postValue(
                    mapLevelsToViewItems(
                        recommendationsSelected,
                        reagentDeficiencies
                    )
                )
            }
        }
    }

    private fun mapFoodItem(
        planData: PlanData,
        isDailyView: Boolean,
        groupedItemsCount: Int,
        todos: List<PlanData>,
        plans: List<PlanData>,
        recommendedItem: FoodRecommendation?
    ) = FoodPlanItem(
        planData.getDisplayName(),
        planData.getDescription(),
        recommendedItem?.nutrients.orEmpty()
            .filter { it.amount >= 0 }
            .filter { it.reagent.id != ReagentItem.B9.id }
            .take(4)
            .joinToString("\n") {
                "•\t\t${it.reagent.displayName} ${
                Math.ceil(
                    it.amount.times(planData.multiple ?: 1).toDouble()
                ).toInt()
                } ${
                it.reagent.consumptionUnit
                }"
            },
        getRemindersTitle(planData, isDailyView),
        planData.getToDoImageUrl(),
        planData.getDisplayName(),
        resourceProvider.getString(R.string.remove_from_plan),
        isDailyView.not(),
        groupedItemsCount,
        todos,
        plans,
        planData.day_of_week,
        planData,
        recommendedItem = recommendedItem
    )

    private fun getRemindersTitle(todo: PlanData?, isDailyView: Boolean): String {
        return if (isDailyView) {
            todo?.getTimeFormattedWithMeridian() ?: getResString(R.string.set_time)
        } else {
            getResString(R.string.reminders)
        }
    }

    private fun setupDailyData(
        forceUpdate: Boolean,
        foodPlans: List<PlanData>,
        recommendations: List<FoodRecommendation>
    ) {
        val planItems = mutableListOf<FoodDailyItem>()
        val groupedFoodItems = foodPlans.groupBy { it.food_id }
        val dailyItems = mutableListOf<FoodPlanItem>()

        groupedFoodItems.values.map { grouped ->
            val recommendedItem =
                recommendations.firstOrNull { it.usdaNdbNumber.toString() == grouped.first().food?.usda_ndb_number }
            grouped.forEach {
                dailyItems.add(
                    mapFoodItem(
                        it,
                        true,
                        1,
                        grouped,
                        grouped,
                        recommendedItem
                    )
                )
            }
        }

        val daysPlans = mutableMapOf<Int, MutableList<FoodPlanItem>>()
        val days = getPlanDaysOfWeekSorted().map { it.dayIndex }
        for (i in days) {
            daysPlans[i] = mutableListOf()
        }

        dailyItems.sortedBy { it.valueProp1 }.map { planItem ->
            val splitDays = planItem.getDaysOfWeek()
            for (day in splitDays) {
                daysPlans[day]?.add(planItem)
            }
        }

        val daysOfWeek = getPlanDaysOfWeek()
        daysPlans.forEach { daysPlan ->
            val dayItems = daysPlan.value
            val date =
                daysOfWeek.first { it.dayIndex == daysPlan.key }.dayIndex
                    .toCurrentWeekDate()
            planItems.add(
                FoodDailyItem(
                    "${date.formatDayName()}'s Food Plan",
                    dayItems,
                    true
                )
            )
        }

        planItems.removeIf { it.items.isEmpty() }
        state.todayPlansData = planItems
        if ((
            state.isDailyView && state.foodItems.value.orEmpty()
                .isEmpty()
            ) || (forceUpdate && state.isDailyView)
        ) {
            state.foodItems.value = state.todayPlansData
        }
    }

    private fun setupWeeklyData(
        forceUpdate: Boolean,
        foodPlans: List<PlanData>,
        recommendations: List<FoodRecommendation>
    ) {

        val planItems = mutableListOf<FoodPlanItem>()
        val groupedFoodItems = foodPlans.groupBy { it.food_id }

        groupedFoodItems.forEach { fullGroup ->
            val recommendedItem =
                recommendations.firstOrNull { it.usdaNdbNumber.toString() == fullGroup.value.first().food?.usda_ndb_number }
            fullGroup.value.groupBy { it.multiple }.forEach { internalItem ->
                planItems.add(
                    mapFoodItem(
                        internalItem.value.first(),
                        false,
                        internalItem.value.sumBy { it.getNonCompletedDays().size },
                        internalItem.value,
                        fullGroup.value,
                        recommendedItem
                    )
                )
            }
        }

        val weekDates = getCurrentWeekDates()

        planItems.removeIf { it.todos.isEmpty() }

        if (planItems.isNotEmpty()) {
            state.weeklyPlansData = mutableListOf(
                FoodDailyItem(
                    "${weekDates.first.formatPlanDate()} - ${weekDates.second.formatPlanDate()}",
                    planItems.sortedBy { it.valueProp1 },
                    false
                )
            )
        } else {
            state.weeklyPlansData = mutableListOf()
        }

        if ((
            !state.isDailyView && state.foodItems.value.orEmpty()
                .isEmpty()
            ) || (forceUpdate && !state.isDailyView)
        ) {
            state.foodItems.value = state.weeklyPlansData
        }
    }

    private fun observeDietAllergiesState() {
        viewModelScope.launch {
            contactManager.getDietAllergiesState().collectLatest {
                if (it) {
                    contactManager.clearDietAllergiesState()
                    state.navigateTo.value =
                        FoodPlanFragmentDirections.actionFoodPlanFragmentToFoodPlanFilterDialog(
                            state.selectedFilterAndSortOptions
                        )
                }
            }
        }
    }

    private fun observeFoodRecommendationsState() {
        viewModelScope.launch {
            foodManager.getFoodCachedDataState().collectLatest {
                if (it) {
                    loadFoodPlans(true)
                }
            }
        }
    }

    private fun observeRemindersChanges() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collectLatest {
                if (it) {
                    loadFoodPlans(true)
                }
            }
        }
    }

    private fun mapFoodRecommendationViewItems(
        recommendations: MutableList<FoodRecommendation>,
        isSelected: Boolean
    ) =
        recommendations.map { foodRecommendation ->
            val multiple =
                todos.firstOrNull { it.food?.usda_ndb_number == foodRecommendation.usdaNdbNumber.toString() }?.multiple
                    ?: 1
            val quantityValue = foodRecommendation.servingSize?.display_quantity.orEmpty()
            FoodRecommendationViewItem(
                name = foodRecommendation.name,
                description = "${foodRecommendation.name} $quantityValue ${foodRecommendation.servingSize?.unit}",
                foodNutrientsText = foodRecommendation.nutrients
                    .filter { it.amount >= 0 }
                    .filter { it.reagent.id != ReagentItem.B9.id }
                    .take(4)
                    .joinToString("\n") {
                        "•\t\t${it.reagent.displayName} ${
                        NumberFormat.getInstance().format(it.amount * multiple)
                        } ${
                        it.reagent.consumptionUnit
                        }"
                    },
                isInPlan = isSelected,
                usdaNdbNumber = foodRecommendation.usdaNdbNumber,
                amount = (foodRecommendation.servingSize?.quantity ?: 0f * multiple)
            )
        }

    private fun mapFoodRecommendations(
        foodRecs: List<FoodRecommendation>
    ): Pair<LinkedList<FoodRecommendation>, LinkedList<FoodRecommendation>> {
        val todoNumbers = todos.map { it.food?.usda_ndb_number }
        val recommendationsSelected = LinkedList<FoodRecommendation>()
        val recommendationsNotSelected = LinkedList<FoodRecommendation>()

        foodRecs.filter { it.usdaNdbNumber > 0 }.forEach {
            if (todoNumbers.contains(it.usdaNdbNumber.toString())) {
                recommendationsSelected.add(it)
            } else {
                recommendationsNotSelected.add(it)
            }
        }

        return recommendationsSelected to recommendationsNotSelected
    }

    private fun mapLevelsToViewItems(
        recommendationsSelected: List<FoodRecommendation>,
        reagentDeficiencies: Map<Reagent, Float>
    ) =
        reagentDeficiencies.filter { it.key.id != ReagentItem.B9.id && it.key.state != ReagentState.COMING_SOON }
            .map { entry ->
                val (reagentNeeded, reagentInFoodPlan) = calculateReagentInFoodPlan(
                    recommendationsSelected,
                    entry
                )
                FoodLevelViewItem(
                    name = entry.key.displayName,
                    needed = if (reagentNeeded <= 0) {
                        resourceProvider.getString(R.string.good)
                    } else {
                        "${resourceProvider.getString(R.string.needed)} $reagentNeeded${entry.key.consumptionUnit}"
                    },
                    progress = if (reagentNeeded <= 0) {
                        100
                    } else {
                        (reagentInFoodPlan * 100 / entry.value).toInt()
                    }
                )
            }

    private fun calculateReagentInFoodPlan(
        recommendationsSelected: List<FoodRecommendation>,
        deficiency: Map.Entry<Reagent, Float>
    ): Pair<Int, Int> {
        val reagentInFoodPlan = recommendationsSelected.sumOf { foodRec ->
            val multiple =
                todos.firstOrNull { it.food?.usda_ndb_number == foodRec.usdaNdbNumber.toString() }?.multiple
                    ?: 1
            foodRec.nutrients.sumOf {
                if (it.reagent.id == deficiency.key.id) it.amount.times(multiple).toInt()
                else 0
            }
        }

        return max((deficiency.value - reagentInFoodPlan).toInt(), 0) to reagentInFoodPlan
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onActionButtonClick(item: FoodRecommendationViewItem) {
    }

    fun getFoodCheckOutScienceHeaderContent(): SpannableString {
        return getResString(R.string.food_science_description)
            .makeClickableSpan(
                Pair(
                    getResString(R.string.references),
                    View.OnClickListener {
                        onLinkButtonClick(it, getResString(R.string.references))
                    }
                )
            )
    }

    fun getSupplementCheckOutScienceHeaderContent(): SpannableString {
        return getResString(R.string.supplement_science_description)
            .makeClickableSpan(
                Pair(
                    getResString(R.string.references),
                    View.OnClickListener {
                        onLinkButtonClick(it, getResString(R.string.references))
                    }
                )
            )
    }

    override fun onLinkButtonClick(view: View, link: String) {
        val navigationLink = when (link) {
            getResString(R.string.references) ->
                HomeNavGraphDirections.globalActionToScienceReferencesDialogFragment(state.getCheckOutReferences())
            else ->
                HomeNavGraphDirections.globalActionToWeb(link)
        }
        view.findNavController().navigate(navigationLink)
    }

    override fun onCloseClicked() {
        navigateBack()
    }

    override fun onExportPlanButtonClick() {
        showError("onExportPlanButtonClick")
    }

    override fun onGetDeliveryButtonClick() {
        showError("onGetDeliveryButtonClick")
    }

    override fun onAddPlanClicked() {
        state.navigateTo.value =
            FoodPlanFragmentDirections.actionFoodPlanFragmentToBuildFoodPlanFragment()
    }

    override fun onDailySelected() {
        state.isDailyView = true
        state.foodItems.value = state.todayPlansData
    }

    override fun onWeeklySelected() {
        state.isDailyView = false
        state.foodItems.value = state.weeklyPlansData
    }

    override fun onPlanItemClicked(item: FoodPlanItem, view: View, parentPosition: Int) {
        if (item.todos.size > 1 || preferencesRepository.dontShowPlanRemoveConfirmationDialog) {
            viewModelScope.launch {
                val todo = item.todos.first()
                val createRequest = CreatePlanRequest(
                    is_supplements = todo.is_supplements,
                    notification_enabled = true,
                    tip_id = todo.tip_id,
                    food_id = todo.food_id,
                    day_of_week = null,
                    reagent_lifestyle_recommendation_id = todo.reagent_lifestyle_recommendation_id,
                    multiple = null,
                    time_of_day = null,
                )
                planManager.addPlanItem(createRequest)
                    .onSuccess {
                        reminderManager.updateFirstReminderState()
                        planManager.clearCachedData()
                        reminderManager.updateRemindersCachedData()
                        trackingManager.log(
                            TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_ADDED)
                                .addProperty(TrackingConstants.KEY_ID, todo.food_id.toString())
                                .addProperty(TrackingConstants.TYPE, LikeCategory.Food.value)
                                .addProperty(TrackingConstants.KEY_LOCATION, "Food Plan Page")
                                .addProperty("has reminders", "false")
                                .withKlaviyo()
                                .withFirebase()
                                .withAmplitude()
                                .create()
                        )
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }
                    .onError {
                        hideLoading()
                    }
            }
            state.navigateTo.value =
                FoodPlanFragmentDirections.actionFoodPlanFragmentToPlanReminderDialog(
                    PlanReminderHeader(
                        item.title,
                        item.valueProp1.orEmpty(),
                        item.backgroundUrl,
                        0,
                        item.plans,
                        false
                    )
                )
        } else {
            state.removeReminder.value = item
        }
    }

    override fun onItemSelected(
        item: FoodPlanItem,
        view: View,
        position: Int,
        parentPosition: Int
    ) {
        state.navigateTo.value =
            FoodPlanFragmentDirections.actionFoodPlanFragmentToFoodPlanDetailsFragment(
                FoodDetailsItemModel(
                    item.title,
                    item.valueProp1.orEmpty(),
                    item.plans.first().food_id ?: return,
                    item.isAdded
                )
            )
    }

    fun deletePlanItem(
        item: FoodPlanItem
    ) {
        viewModelScope.launch {
            planManager.deletePlanItem(listOf(item.currentPlan.id ?: -1))
                .onSuccess {
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(TrackingConstants.KEY_ID, item.currentPlan.id.toString())
                            .addProperty(
                                TrackingConstants.TYPE,
                                item.currentPlan.getPlanType().title
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Food Plan Page")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    loadFoodPlans(true)
                }
                .onError {
                    hideLoading(false)
                    showError(getResString(R.string.unknown_error))
                }
        }
    }

    override fun onItemDeleteClicked(
        item: FoodPlanItem,
        view: View,
        position: Int,
        parentPosition: Int
    ) {
        viewModelScope.launch {
            val ids = if (item.isGrouped) {
                item.todos.map { it.id ?: -1 }
            } else {
                listOf(item.currentPlan.id ?: -1)
            }
            planManager.deletePlanItem(listOf(item.currentPlan.id ?: -1))
                .onSuccess {
                    hideLoading(false)
                    val items = state.foodItems.value.orEmpty().toMutableList()
                    val parentItem = items[parentPosition]
                    val todos = parentItem.items.toMutableList()
                    todos.removeAt(position)

                    val updatedItem = parentItem.copy(items = todos)

                    items[parentPosition] = updatedItem
                    if (updatedItem.items.isEmpty()) {
                        items.removeAt(parentPosition)
                    }
                    state.foodItems.value = items
                    state.foodTabs.value = state.foodTabs.value.orEmpty().map {
                        it.copy(showTabs = items.isNotEmpty())
                    }
                    planManager.updatePlanCachedData()
                    loadFoodPlans(true)
                    todos.removeAll {
                        ids.contains(it.currentPlan.id)
                    }
                    state.levels.postValue(
                        mapLevelsToViewItems(
                            recommendationsSelected,
                            reagentDeficiencies
                        )
                    )
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(TrackingConstants.KEY_ID, item.currentPlan.id.toString())
                            .addProperty(
                                TrackingConstants.TYPE,
                                item.currentPlan.getPlanType().title
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Food Plan Page")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onError {
                    hideLoading(false)
                    showError(getResString(R.string.unknown_error))
                }
        }
    }

    override fun onItemEditClicked(item: FoodPlanItem, view: View, parentPosition: Int) {
        navigateToReminders(item)
    }

    override fun onGroupedItemsClicked(item: FoodPlanItem, view: View, parentPosition: Int) {
    }

    override fun onAddQuantityClicked(
        item: FoodPlanItem,
        multiple: Int,
        view: View,
        position: Int,
        parentPosition: Int
    ) {
        viewModelScope.launch {
            val updatedPlan = item.currentPlan.copy(
                multiple = multiple
            )
            planManager.updatePlanItem(
                updatedPlan.id ?: return@launch,
                updatedPlan.toPlanRequest()
            )
            updatePlanItem(item.copy(currentPlan = updatedPlan), position, parentPosition)
        }
    }

    override fun onSubtractQuantityClicked(
        item: FoodPlanItem,
        multiple: Int,
        view: View,
        position: Int,
        parentPosition: Int
    ) {
        viewModelScope.launch {
            val updatedPlan = item.currentPlan.copy(
                multiple = multiple
            )
            planManager.updatePlanItem(
                updatedPlan.id ?: return@launch,
                updatedPlan.toPlanRequest()
            )
            planManager.updatePlanCachedData()
            updatePlanItem(item.copy(currentPlan = updatedPlan), position, parentPosition)
        }
    }

    private fun updatePlanItem(planItem: FoodPlanItem, position: Int, parentPosition: Int) {
        val items = state.foodItems.value.orEmpty().toMutableList()
        val parentItem = items[parentPosition]
        val planItems = parentItem.items.toMutableList()
        planItems[position] = updateFoodLevelsAndNutrients(planItem)
        val updatedParentItem = parentItem.copy(
            title = parentItem.title,
            items = planItems
        )
        items[parentPosition] = updatedParentItem
        planManager.updatePlanCachedData()
        state.foodItems.value = items
    }

    private fun updateFoodLevelsAndNutrients(planItem: FoodPlanItem): FoodPlanItem {
        val updatedTodo = planItem.currentPlan

        val updatedPlanItem =
            planItem.copy(
                foodNutrientsText = planItem.recommendedItem?.nutrients.orEmpty()
                    .filter { it.amount >= 0 }
                    .filter { it.reagent.id != ReagentItem.B9.id }
                    .take(4)
                    .joinToString("\n") {
                        "•\t\t${it.reagent.displayName} ${
                        Math.ceil(
                            it.amount.times(planItem.currentPlan.multiple ?: 1).toDouble()
                        ).toInt()
                        } ${
                        it.reagent.consumptionUnit
                        }"
                    }
            )

        val index = todos.indexOfFirst { it.id == planItem.currentPlan.id }
        todos[index] = updatedTodo
        state.levels.postValue(
            mapLevelsToViewItems(
                recommendationsSelected,
                reagentDeficiencies
            )
        )
        return updatedPlanItem
    }

    fun navigateToReminders(item: FoodPlanItem) {
        state.navigateTo.value =
            FoodPlanFragmentDirections.actionFoodPlanFragmentToPlanReminderDialog(
                PlanReminderHeader(
                    item.title,
                    item.valueProp1.orEmpty(),
                    item.backgroundUrl,
                    0,
                    item.plans,
                    false
                )
            )
    }

    fun onDontShowAgainClicked(isChecked: Boolean) {
        preferencesRepository.dontShowPlanRemoveConfirmationDialog = isChecked
    }

    override fun onAddPlanFooterClicked(item: Any, view: View) {
        state.navigateTo.value =
            FoodPlanFragmentDirections.actionFoodPlanFragmentToBuildFoodPlanFragment()
    }
}
