package com.vessel.app.taketest.model

import android.graphics.Bitmap
import android.hardware.camera2.CaptureResult
import java.io.File

data class UploadTestFileModel(
    val uuid: String,
    val file: File,
    val jpgFile: File?,
    val firstFailedTestUUID: String?,
    val autoScan: Boolean,
    val captureResult: CaptureResult?,
    val timerStartDate: String,
    val captureDate: String,
    val wellnessCardUuid: String,
    val rotatedImage: Bitmap?
)
