package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SurveyAnswerData(
    val id: Int?,
    val primary_text: String?,
    val secondary_text: String?,
    val image: String?,
    val is_incorrect: Boolean?,
    val chained_survey_id: String?,
    val answer_id: Int?
)
