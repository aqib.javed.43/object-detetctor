package com.vessel.app.common.manager

import com.vessel.app.common.repo.AssessmentRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AssessmentManager @Inject constructor(private val assessmentRepository: AssessmentRepository) {
    suspend fun getWellnessScores(startDate: String? = null, endDate: String? = null) =
        assessmentRepository.getGoalAssessments(startDate, endDate)

    suspend fun putGoalAssessments(goalId: Int, value: Int, createdDate: String) =
        assessmentRepository.putGoalAssessments(goalId, value, createdDate)

    suspend fun getTags(goalId: Int, rate: String?) = assessmentRepository.getTags(goalId, rate)

    suspend fun getRecommendations(
        goalId: Int?,
        tagIds: List<Int>?,
        level: String?,
        pageNumber: Int?,
        pageSize: Int?,
        sortingByLikes: Boolean?,
        sortingByNewest: Boolean?,
    ) =
        assessmentRepository.getTips(
            goalId,
            tagIds,
            level,
            pageNumber,
            pageSize,
            sortingByLikes,
            sortingByNewest
        )

    suspend fun getRecommendationsWithHabits(
        goalId: Int?,
        tagIds: List<Int>? = null,
        level: String? = null,
        pageNumber: Int? = null,
        pageSize: Int? = null,
        sortingByLikes: Boolean?,
        sortingByNewest: Boolean?,
        habitIds: List<Int>?,
    ) =
        assessmentRepository.getTipsWithHabits(
            goalId,
            tagIds,
            level,
            pageNumber,
            pageSize,
            sortingByLikes,
            sortingByNewest,
            habitIds
        )
}
