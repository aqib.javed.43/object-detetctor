package com.vessel.app.testnotaccurate

import android.text.SpannableString
import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.NavDirections
import com.vessel.app.BuildConfig
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.makeClickableSpan
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import java.util.*

class TestNotAccurateViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    remoteConfiguration: RemoteConfiguration
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()

    init {
        remoteConfiguration.fetchConfigFromRemote()
    }

    fun onViewAndroidUpdates() {
        navigateTo.value = HomeNavGraphDirections.globalActionToWeb(BuildConfig.APP_STATUS_URL)
    }

    fun dismiss() {
        dismissDialog.call()
    }

    fun onProceedAnywayClicked() {
        dismissDialog.call()
        val now = Date()
        val fourThirtyAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 4)
                set(Calendar.MINUTE, 30)
                set(Calendar.SECOND, 0)
            }.time
        val nineAm = Calendar.getInstance()
            .apply {
                set(Calendar.HOUR_OF_DAY, 9)
                set(Calendar.MINUTE, 0)
                set(Calendar.SECOND, 0)
            }.time
        if (now.after(fourThirtyAm) && now.before(nineAm)) {
            navigateTo.value = if (preferencesRepository.skipPreTestTips)
                HomeNavGraphDirections.globalActionToTakeTest()
            else
                HomeNavGraphDirections.globalActionToPreTest()
        } else {
            navigateTo.value = HomeNavGraphDirections.globalActionToTimeOfDay()
        }
    }

    fun formattedDescription(): SpannableString {
        return getResString(R.string.test_not_accurate_description).makeClickableSpan(
            Pair(
                getResString(R.string.pause),
                View.OnClickListener {
                    navigateTo.value = HomeNavGraphDirections.globalActionToWeb(BuildConfig.PAUSE_MEMBERSHIP_URL)
                }
            )
        )
    }
}
