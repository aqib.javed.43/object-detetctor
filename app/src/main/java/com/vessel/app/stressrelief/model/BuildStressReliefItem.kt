package com.vessel.app.stressrelief.model

import android.os.Parcelable
import com.vessel.app.R
import com.vessel.app.common.model.Sample
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BuildStressReliefItem(
    val title: String,
    val image: String,
    val description: String,
    val isAdded: Boolean,
    val sample: Sample,
    val lifestyle_recommendation_id: Int?,
) : Parcelable {
    @IgnoredOnParcel
    val buttonResId = if (isAdded) R.string.remove_from_plan else R.string.add_to_plan
}
