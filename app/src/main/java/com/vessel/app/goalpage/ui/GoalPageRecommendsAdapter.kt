package com.vessel.app.goalpage.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.goalpage.model.GoalPageRecommends
import com.vessel.app.views.RecommendationListWidgetOnActionHandler
import com.vessel.app.wellness.model.Paging
import com.vessel.app.wellness.model.RecommendationItem
import kotlinx.android.synthetic.main.item_goal_page_recommend.view.*

class GoalPageRecommendsAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<GoalPageRecommends>(),
    RecommendationListWidgetOnActionHandler {
    override fun getLayout(position: Int) = when (getItem(position)) {
        is GoalPageRecommends -> R.layout.item_goal_page_recommend
        else -> throw IllegalStateException(
            "Unexpected WellnessFooterAdapter type at position $position for item ${getItem(position)}"
        )
    }

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onExpandViewClicked(recommendation: RecommendationItem)
        fun onRecommendationItemClicked(recommendation: RecommendationItem)
        fun onRecommendationItemLikeClicked(recommendation: RecommendationItem)
        fun onLoadMoreClicked(paging: Paging)
        fun onRecommendationAddToPlanClicked(recommendation: RecommendationItem)
        fun onFilterClicked()
    }
    override fun onBindViewHolder(holder: BaseViewHolder<GoalPageRecommends>, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as GoalPageRecommends
        holder.itemView.goalRecommendationsTitle.text = item.title
        item.items?.let {
            holder.itemView.recommendationList.setHandler(this)
            holder.itemView.recommendationList.setList(it, item.recommendationsPaging)
        }
        holder.itemView.filterButton.setOnClickListener {
            handler.onFilterClicked()
        }
    }
    override fun onExpandViewClicked(recommendation: RecommendationItem) {
        handler.onExpandViewClicked(recommendation)
    }

    override fun onRecommendationItemClicked(recommendation: RecommendationItem) {
        handler.onRecommendationItemClicked(recommendation)
    }

    override fun onRecommendationItemLikeClicked(recommendation: RecommendationItem) {
        handler.onRecommendationItemLikeClicked(recommendation)
    }

    override fun onRecommendationAddToPlanClicked(recommendation: RecommendationItem) {
        handler.onRecommendationAddToPlanClicked(recommendation)
    }

    override fun onLoadMoreClicked(paging: Paging) {
        handler.onLoadMoreClicked(paging)
    }
}
