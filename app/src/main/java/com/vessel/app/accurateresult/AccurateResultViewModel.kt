package com.vessel.app.accurateresult

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository

class AccurateResultViewModel @ViewModelInject constructor(
    val state: AccurateResultState,
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider), ToolbarHandler {

    fun dontShowChecked(isChecked: Boolean) {
        preferencesRepository.showAccurateResultDialogV2 = !isChecked
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onGotItClicked() {
        state.navigateToCapture.call()
    }
}
