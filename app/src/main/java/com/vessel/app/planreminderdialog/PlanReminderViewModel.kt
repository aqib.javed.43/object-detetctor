package com.vessel.app.planreminderdialog

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.ReminderManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.net.data.plan.CreatePlanRequest
import com.vessel.app.common.net.data.plan.toPlanRequest
import com.vessel.app.common.net.data.plan.toReminderUiModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.planreminderdialog.model.ReminderItemUiModel
import com.vessel.app.planreminderdialog.ui.PlanReminderAdapter
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.asyncAll
import com.vessel.app.views.weekdays.DayOfWeekUiModel
import com.vessel.app.views.weekdays.WeekDaysClickListener
import com.vessel.app.wellness.net.data.LikeCategory
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch

class PlanReminderViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val trackingManager: TrackingManager,
    private val reminderManager: ReminderManager,
    private val planManager: PlanManager,
    private val programManager: ProgramManager
) : BaseViewModel(resourceProvider),
    PlanReminderAdapter.OnActionHandler,
    WeekDaysClickListener {
    val state = PlanReminderState(savedStateHandle["reminderHeader"]!!)
    var isAddReminderClicked = false

    override fun onAlarmItemClicked(item: ReminderItemUiModel) {
        val index = state.reminderList.value.orEmpty().indexOf(item)
        val updatedItem =
            item.copy(
                isUpdated = true,
                todo = item.todo.copy(
                    notification_enabled = (item.todo.notification_enabled ?: false).not()
                )
            )
        val items = state.reminderList.value.orEmpty().toMutableList()
        items[index] = updatedItem
        state.reminderList.value = items
    }

    override fun onAlarmTimeClicked(item: ReminderItemUiModel) {
        state.updateReminderTime.value = item
    }

    override fun onReminderDaysSelectedClicked(
        item: ReminderItemUiModel,
        days: List<DayOfWeekUiModel>,
    ) {
        val index = state.reminderList.value.orEmpty().indexOf(item)
        val updatedItem =
            item.copy(
                isUpdated = true,
                dayOfWeek = days.map { it.planIndex }.toMutableList(),
                todo = item.todo.copy(day_of_week = days.map { it.planIndex })
            )
        val items = state.reminderList.value.orEmpty().toMutableList()
        items[index] = updatedItem
        state.reminderList.value = items
    }

    override fun onAddQuantityClicked(item: ReminderItemUiModel, multiple: Int) {
        val index = state.reminderList.value.orEmpty().indexOf(item)
        val updatedItem = item.copy(isUpdated = true, todo = item.todo.copy(multiple = multiple))
        val items = state.reminderList.value.orEmpty().toMutableList()
        items[index] = updatedItem
        state.reminderList.value = items
    }

    override fun onSubtractQuantityClicked(item: ReminderItemUiModel, multiple: Int) {
        val index = state.reminderList.value.orEmpty().indexOf(item)
        val updatedItem = item.copy(isUpdated = true, todo = item.todo.copy(multiple = multiple))
        val items = state.reminderList.value.orEmpty().toMutableList()
        items[index] = updatedItem
        state.reminderList.value = items
    }

    override fun onRemoveClicked(item: ReminderItemUiModel) {
        if (state.reminderList.value.orEmpty().size > 1 || preferencesRepository.dontShowPlanRemoveConfirmationDialog) {
            removeReminder(item)
        } else {
            state.removeReminder.value = item
        }
    }

    fun onDontShowAgainClicked(isChecked: Boolean) {
        preferencesRepository.dontShowPlanRemoveConfirmationDialog = isChecked
    }

    fun onRemovePlanConfirmed() {
        removeReminder(state.removeReminder.value ?: return)
    }

    private fun removeReminder(item: ReminderItemUiModel) {
        viewModelScope.launch {
            showLoading(false)
            planManager.deletePlanItem(listOf(item.todo.id ?: return@launch))
                .onSuccess {
                    hideLoading(false)
                    planManager.updatePlanCachedData()
                    planManager.todayPlanItemCheckChange()
                    reminderManager.updateRemindersCachedData()
                    val index = state.reminderList.value.orEmpty().indexOfFirst {
                        it.todo.id == item.todo.id
                    }
                    val items = state.reminderList.value.orEmpty().toMutableList()
                    items.removeAt(index)
                    state.reminderList.value = items
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(TrackingConstants.KEY_ID, item.todo.id.toString())
                            .addProperty(
                                TrackingConstants.TYPE,
                                item.todo.getPlanType().title
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Plan Reminder")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun onCloseDialogClicked() {
        val updatedItems = state.reminderList.value.orEmpty().filter { it.isUpdated }
        val hasItems = state.reminderList.value.orEmpty().isNotEmpty()
        if (updatedItems.isEmpty()) {
            if (!hasItems) {
                viewModelScope.launch {
                    val todo = state.planHeader.todos.first()
                    val createRequest = CreatePlanRequest(
                        is_supplements = todo.is_supplements,
                        notification_enabled = true,
                        tip_id = todo.tip_id,
                        food_id = todo.food_id,
                        day_of_week = null,
                        reagent_lifestyle_recommendation_id = todo.reagent_lifestyle_recommendation_id,
                        multiple = null,
                        time_of_day = null,
                    )
                    planManager.addPlanItem(createRequest)
                        .onSuccess {
                            val items = state.reminderList.value.orEmpty().toMutableList()
                            if (items.isEmpty() && state.planHeader.isEmptyPlan) {
                                reminderManager.updateFirstReminderState()
                                planManager.clearCachedData()
                                programManager.rebuildScheduleForProgram(todo.program_id)
                                reminderManager.updateRemindersCachedData()
                                planManager.todayPlanItemCheckChange()
                                state.dismissDialog.call()
                            } else {
                                state.dismissDialog.call()
                            }
                            val id = todo.food_id ?: (
                                todo.tip_id
                                    ?: todo.reagent_lifestyle_recommendation_id
                                )
                            val type = when {
                                todo.food_id != null -> LikeCategory.Food.value
                                todo.tip_id != null -> LikeCategory.Tip.value
                                else -> LikeCategory.Lifestyle.value
                            }
                            trackingManager.log(
                                TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_ADDED)
                                    .addProperty(TrackingConstants.KEY_ID, id.toString())
                                    .addProperty(TrackingConstants.TYPE, type)
                                    .addProperty(TrackingConstants.KEY_LOCATION, "Reminder Page")
                                    .addProperty("has reminders", "false")
                                    .withKlaviyo()
                                    .withFirebase()
                                    .withAmplitude()
                                    .create()
                            )
                        }
                        .onNetworkIOError {
                            showError(getResString(R.string.network_error))
                        }
                        .onUnknownError {
                            showError(getResString(R.string.unknown_error))
                        }
                        .onError {
                            hideLoading()
                        }
                }
            } else {
                state.dismissDialog.call()
            }
        } else {
            showLoading()
            viewModelScope.launch {
                asyncAll(updatedItems) { item ->
                    planManager.updatePlanItem(
                        item.todo.id ?: return@asyncAll,
                        item.todo.toPlanRequest()
                    )
                }.awaitAll()

                reminderManager.updateRemindersCachedData()
                planManager.updatePlanCachedData()
                programManager.rebuildScheduleForProgram(updatedItems.firstOrNull()?.todo?.program_id)
                hideLoading(false)
                reminderManager.updateUpdateReminderState()
                planManager.todayPlanItemCheckChange()
                state.dismissDialog.call()
            }
        }
    }

    fun onSetRemindersClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_PLAN_REMINDER_SET)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.setRemindersEvent.call()
    }

    fun onSkipThisClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TEST_PLAN_REMINDER_SKIPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.skipRemindersEvent.call()
    }

    fun onReminderAdded(
        timeOfTheDay: String?,
        selectedDays: List<DayOfWeekUiModel>?,
        quantity: Float,
    ) {
        if (timeOfTheDay != null) {
            val duplicatedAddedPlans =
                state.planHeader.todos.firstOrNull {
                    it.time_of_day.orEmpty().startsWith(timeOfTheDay, true)
                }
            if (duplicatedAddedPlans != null) {
                showError(getResString(R.string.can_not_add_reminder_at_same_time))
                return
            }
        }
        viewModelScope.launch {
            val todo = state.planHeader.todos.first()
            val createRequest = CreatePlanRequest(
                is_supplements = todo.is_supplements,
                notification_enabled = true,
                tip_id = todo.tip_id,
                food_id = todo.food_id,
                day_of_week = selectedDays?.map { it.planIndex },
                reagent_lifestyle_recommendation_id = todo.reagent_lifestyle_recommendation_id,
                multiple = (quantity / todo.getQuantityScale().toFloat()).toInt(),
                time_of_day = timeOfTheDay,
            )
            planManager.addPlanItem(createRequest)
                .onSuccess {
                    val items = state.reminderList.value.orEmpty().toMutableList()
                    if (items.isEmpty() && state.planHeader.isEmptyPlan) {
                        reminderManager.updateFirstReminderState()
                        programManager.rebuildScheduleForProgram(todo.program_id)
                        planManager.clearCachedData()
                        reminderManager.updateRemindersCachedData()
                        planManager.todayPlanItemCheckChange()
                        state.dismissDialog.call()
                    } else {
                        items.add(it.toReminderUiModel())
                        state.reminderList.value = items
                        reminderManager.updateUpdateReminderState()
                        planManager.clearCachedData()
                        reminderManager.updateRemindersCachedData()
                        planManager.todayPlanItemCheckChange()
                    }
                    val id =
                        todo.food_id ?: (todo.tip_id ?: todo.reagent_lifestyle_recommendation_id)
                    val type = when {
                        todo.food_id != null -> LikeCategory.Food.value
                        todo.tip_id != null -> LikeCategory.Tip.value
                        else -> LikeCategory.Lifestyle.value
                    }
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_ADDED)
                            .addProperty(TrackingConstants.KEY_ID, id.toString())
                            .addProperty(TrackingConstants.TYPE, type)
                            .addProperty(TrackingConstants.KEY_LOCATION, "Reminder Page")
                            .addProperty(
                                "has reminders",
                                (createRequest.time_of_day != null).toString()
                            )
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    override fun isNoneWeekDaysSelectedListener(isAnyWeekDaysSelected: Boolean) {
        state.showReminderWarning.value = isAnyWeekDaysSelected
    }

    override fun onWeekDaysSelectedListener(days: List<DayOfWeekUiModel>) {
    }

    fun onReminderTimeUpdated(item: ReminderItemUiModel, time: String) {
        val duplicatedAddedPlans =
            state.planHeader.todos.firstOrNull { it.time_of_day.orEmpty().startsWith(time, true) }
        if (duplicatedAddedPlans != null) {
            showError(getResString(R.string.can_not_add_reminder_at_same_time))
            state.reminderList.value = state.reminderList.value.orEmpty()
            return
        }
        val index = state.reminderList.value.orEmpty().indexOfFirst { it.todo.id == item.todo.id }
        val updatedItem =
            item.copy(
                isUpdated = true,
                timeOfReminder = time,
                todo = item.todo.copy(time_of_day = time)
            )
        val items = state.reminderList.value.orEmpty().toMutableList()
        if (index != -1) {
            items[index] = updatedItem
        }
        state.reminderList.value = items
    }
}
