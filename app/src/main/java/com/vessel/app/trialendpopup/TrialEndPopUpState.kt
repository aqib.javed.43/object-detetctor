package com.vessel.app.trialendpopup

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class TrialEndPopUpState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: TrialEndPopUpState.() -> Unit) = apply(block)
}
