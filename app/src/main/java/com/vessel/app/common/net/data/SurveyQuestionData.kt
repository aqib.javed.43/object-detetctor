package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SurveyQuestionData(
    val id: Int,
    val layout: String,
    val tip_text: String?,
    val tip_image: String?,
    val image: String?,
    val text: String,
    val answers: List<SurveyAnswerWrapperData>,
    val tip_image_alignment: String?,
    val image_alignment: String?,
    val success_heading: String?,
    val success_text: String?,
    val is_skippable: Boolean?
)
