package com.vessel.app.today.finishreview

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_review_activity.*

@AndroidEntryPoint
class FinishReviewFragment : BaseFragment<FinishReviewViewModel>() {
    override val viewModel: FinishReviewViewModel by viewModels()
    override val layoutResId = R.layout.fragment_review_finish

    override fun onViewLoad(savedInstanceState: Bundle?) {
    }
}
