package com.vessel.app.common.net.data.plan

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserPlansResponse(val plans: List<PlanData>)
