package com.vessel.app.common.manager

import com.vessel.app.common.repo.ReagentRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReagentsManager @Inject constructor(
    private val reagentRepository: ReagentRepository
) {
    suspend fun fetchReagent() = reagentRepository.fetchReagent()

    fun getReagent() = reagentRepository.getReagent()

    fun fromId(id: Int?) = reagentRepository.fromId(id)

    fun fromApiName(apiName: String) = reagentRepository.fromApiName(apiName)
}
