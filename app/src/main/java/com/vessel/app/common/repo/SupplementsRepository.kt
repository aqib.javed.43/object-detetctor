package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.net.mapper.MembershipsMapper
import com.vessel.app.common.net.mapper.SupplementsMapper
import com.vessel.app.common.net.service.SupplementsService
import com.vessel.app.supplement.model.AddSupplementIngredientIdRequest
import com.vessel.app.supplement.model.Membership
import com.vessel.app.vesselfuelcheckout.model.request.VesselFuelItemRequest
import com.vessel.app.vesselfuelcheckout.model.request.VesselFuelSupplementItemModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SupplementsRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val supplementsMapper: SupplementsMapper,
    private val membershipsMapper: MembershipsMapper,
    private val supplementsService: SupplementsService
) : BaseNetworkRepository(moshi, loggingManager) {
    private var membershipsData: List<Membership>? = null
    private var membershipData: Membership? = null
    suspend fun getSupplements() =
        executeWithErrorMessage(supplementsMapper::map) { supplementsService.getSupplements() }

    suspend fun getMemberships(forceUpdate: Boolean = false): Result<List<Membership>, Nothing> {
        return if (forceUpdate || membershipsData == null) {
            execute(
                membershipsMapper::map,
                {
                    supplementsService.getMemberships()
                },
                {
                    membershipsData = it
                }
            )
        } else {
            Result.Success(membershipsData!!)
        }
    }
    suspend fun getMembership(forceUpdate: Boolean = false, email: String): Result<Membership, Nothing> {
        return if (forceUpdate || membershipData == null) {
            execute(
                membershipsMapper::map,
                {
                    supplementsService.getMembership(email)
                },
                {
                    membershipData = it
                }
            )
        } else {
            Result.Success(membershipData!!)
        }
    }
    suspend fun getMembershipsPayment() =
        executeWithErrorMessage(membershipsMapper::mapPayment) { supplementsService.getMembershipsPayment().payment_sources }

    suspend fun getMembershipsAddresses() =
        executeWithErrorMessage(membershipsMapper::mapAddresses) { supplementsService.getMembershipsAddresses().addresses }

    suspend fun checkoutVesselFuel(
        addressId: Int,
        supplementItems: List<VesselFuelSupplementItemModel>
    ) = executeWithErrorMessage {
        supplementsService.checkoutVesselFuel(
            VesselFuelItemRequest(
                addressId,
                supplementItems
            )
        )
    }

    suspend fun addIngredientsToSupplements(
        supplementId: Int
    ) = executeWithErrorMessage {
        supplementsService.addIngredientsToSupplements(
            AddSupplementIngredientIdRequest(
                supplementId
            )
        )
    }

    suspend fun deleteIngredientsFromSupplements(
        supplementId: Int
    ) = executeWithErrorMessage {
        supplementsService.deleteIngredientsFromSupplements(
            supplementId
        )
    }
}
