package com.vessel.app.reagent

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.squareup.moshi.Moshi
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.MembershipManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.data.ReagentButtonAction
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.reagent.model.*
import com.vessel.app.reagent.ui.*
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class ReagentViewModel @ViewModelInject @Inject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    val moshi: Moshi,
    val state: ReagentState,
    val resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    val membershipManager: MembershipManager,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider),
    ReagentHeaderAdapter.OnActionHandler,
    ReagentEnergyAdapter.OnActionHandler,
    ReagentFooterAdapter.OnActionHandler,
    HighValueFoodAdapter.OnActionHandler,
    ToolbarHandler {
    val reagentHeader = savedStateHandle.get<ReagentHeader>("reagentHeader")!!
    init {
        val id = reagentHeader.id
        state.reagentHeaderItems.value = listOf(reagentHeader)
        readReagentInfo(id)
        readReachInfo(id)
        goToPopupDialog(id)
        state.reagentFooterItems.value = listOf(ReagentFooter)
    }

    private fun goToPopupDialog(id: Int) {
        if ((id == ReagentItem.Ketones.id && preferencesRepository.showKetonesPopup) ||
            (id == ReagentItem.Magnesium.id && preferencesRepository.showNewMagnesiumPopup)
        ) {
            state.navigateTo.value =
                V2ReagentFragmentDirections.actionReagentFragmentToReagentImproveAlertDialogFragmentDialog(id)
        }
    }

    override fun onInfoButtonClick() {
        // todo open the reagent score dialog
    }

    override fun onBetaTestingInfoClicked() {
        state.navigateTo.value =
            V2ReagentFragmentDirections.actionReagentFragmentToWeb(BuildConfig.HOME_PAGE_URL)
    }

    override fun onPlanActionClicked(action: ReagentButtonAction) {
        when (action) {
            ReagentButtonAction.FOOD_PLAN, ReagentButtonAction.FOOD_SUPPLEMENT_PLAN -> {
                state.navigateTo.value =
                    V2ReagentFragmentDirections.actionReagentFragmentToFoodPlanFragment()
            }
            ReagentButtonAction.STRESS_RELIEF_PLAN -> {
                state.navigateTo.value =
                    V2ReagentFragmentDirections.actionReagentFragmentToStressReliefFragment()
            }
            ReagentButtonAction.HYDRATION_PLAN -> {
                state.navigateTo.value =
                    V2ReagentFragmentDirections.actionReagentFragmentToHydrationPlanFragment()
            }
            else -> {}
        }
    }

    override fun onSupplementPlanActionClicked() {
        state.navigateTo.value =
            V2ReagentFragmentDirections.actionReagentFragmentToSupplementFragment()
    }

    override fun onCardErrorActionClicked() {
        state.navigateTo.value =
            V2ReagentFragmentDirections.actionReagentFragmentToTestErrorHintDialog()
    }

    private fun readReagentInfo(id: Int) {
        viewModelScope.launch {
            resourceProvider.readFile(ReagentConstant.FILE_TEMPLATE_JSON.format(id, id))?.let {
                val reagentDetails = ReagentDetailsJsonAdapter(moshi).fromJson(it)
                val foodRecommendation = ReagentFoodRecommendation(reagentDetails)
                state.reagentDetails.value = reagentDetails
                state.foodRecommendations.value = listOf(foodRecommendation)
            }
        }
    }

    private fun readReachInfo(id: Int) {
        viewModelScope.launch {
            val reachInfoList = arrayListOf<ReachInfo>()
            // change sorting will affect the UI
            resourceProvider.readFile(ReagentConstant.FILE_TEMPLATE_HOW_TO_READ.format(id, id))?.let {
                reachInfoList.add(ReachInfo(getResString(R.string.how_to_read), it))
            }
            resourceProvider.readFile(ReagentConstant.FILE_TEMPLATE_WHY_IT_MATTERS.format(id, id))?.let {
                reachInfoList.add(ReachInfo(getResString(R.string.why_it_matters), it))
            }
            resourceProvider.readFile(ReagentConstant.FILE_TEMPLATE_TIPS_TO_IMPROVE.format(id, id))?.let {
                reachInfoList.add(ReachInfo(getResString(R.string.tips_to_improve), it))
            }
            if (isUTI()) {
                resourceProvider.readFile(ReagentConstant.FILE_TEMPLATE_BACKEND_BY_SCIENCE.format(id, id))?.let {
                    reachInfoList.add(ReachInfo(getResString(R.string.back_by_science), it))
                }
            }
            if (ReagentConstant.SUPPORTED_MEDICATIONS_REAGENTS.contains(id)) {
                resourceProvider.readFile(
                    ReagentConstant.FILE_TEMPLATE_INTERACTIONS_WITH_MEDICATIONS.format(
                        id,
                        id
                    )
                )?.let {
                    reachInfoList.add(
                        ReachInfo(
                            getResString(R.string.interactions_with_medications),
                            it
                        )
                    )
                }
            }
            state.reachInfo.value = reachInfoList
        }
    }

    override fun onEnergyButtonClick(energy: BenefitsOfBeingOptimal) {
        state.navigateTo.value =
            V2ReagentFragmentDirections.actionReagentFragmentToReagentSourceFragment(
                energy.sources.toTypedArray(),
                energy.information,
                reagentHeader.title
            )
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    override fun onTalkToNutritionistClick(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TALK_TO_NUTRITIONIST_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value = V2ReagentFragmentDirections.actionReagentFragmentToNutritionOptionsFragment()
    }

    override fun onCustomerSupportClick() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CHAT_WITH_SUPPORT)
                .setScreenName(TrackingConstants.FROM_REAGENT_DETAILS)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value = V2ReagentFragmentDirections.actionReagentFragmentToWeb(BuildConfig.HELP_CENTER_URL)
    }

    override fun onViewFoodPlan() {
        state.navigateTo.value = V2ReagentFragmentDirections.globalActionToFoodPlanFragment()
    }

    fun isUTI(): Boolean {
        return reagentHeader.id == ReagentItem.Nitrites.id
    }
}
