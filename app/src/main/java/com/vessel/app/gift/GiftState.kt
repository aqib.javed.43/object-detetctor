package com.vessel.app.gift

import javax.inject.Inject

class GiftState @Inject constructor() {
    operator fun invoke(block: GiftState.() -> Unit) = apply(block)
}
