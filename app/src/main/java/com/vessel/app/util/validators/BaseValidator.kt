package com.vessel.app.util.validators

import androidx.lifecycle.LiveData

abstract class ValidatorException(message: String) : Exception(message)

abstract class BaseValidator<T> {

    @Throws(ValidatorException::class)
    abstract fun validate(value: T): Boolean

    @Throws(ValidatorException::class)
    abstract fun validate(value: LiveData<T>): Boolean

    abstract fun error(): ValidatorException
}
