package com.vessel.app.reagent.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.reagent.model.ReagentHeader

class ReagentScienceAdapter : BaseAdapterWithDiffUtil<ReagentHeader>() {
    override fun getLayout(position: Int) = R.layout.item_reagent_science
}
