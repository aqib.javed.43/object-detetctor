package com.vessel.app.startnewtimer

import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class StartNewTimerState @Inject constructor() {
    val showBackWarning = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: StartNewTimerState.() -> Unit) = apply(block)
}
