package com.vessel.app.foodplanpopup

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class FoodPlanPopUpViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
) : BaseViewModel(resourceProvider) {
    val state = FoodPlanPopUpState(savedStateHandle["isRemoveFoodPlanItem"]!!)
}
