package com.vessel.app.common.util

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class DisclaimerFooterAdapter : BaseAdapterWithDiffUtil<DisclaimerFooter>() {
    override fun getLayout(position: Int) = R.layout.item_disclaimer_footer
}
