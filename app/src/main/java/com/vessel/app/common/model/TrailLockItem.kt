package com.vessel.app.common.model

import android.view.View

class TrailLockItem(
    var isDisPlayIcon: Boolean? = false,
    var message: String? = "",
    var title: String? = "",
    var onDismiss: ((View) -> Unit)? = null,
    var onActiveMember: ((View) -> Unit)? = null
)
