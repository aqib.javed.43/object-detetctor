package com.vessel.app.wellness.homeprogromselection

import android.os.Bundle
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.model.Goal
import com.vessel.app.programs.join.JoinProgramFragment
import com.vessel.app.programs.programselection.ProgramSelectionAdapter
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeProgramSelectionFragment : BaseFragment<HomeProgramSelectionViewModel>() {
    override val viewModel: HomeProgramSelectionViewModel by viewModels()
    override val layoutResId = R.layout.fragment_home_program_selection
    private lateinit var programsList: RecyclerView
    private lateinit var title: AppCompatTextView
    private lateinit var loadMore: AppCompatTextView
    private val programAdapter by lazy { ProgramSelectionAdapter(viewModel) }
    private val joinObserver = Observer<Boolean> { value ->
        if (value) {
            viewModel.reloadPrograms()
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        title = requireView().findViewById(R.id.title)
        loadMore = requireView().findViewById(R.id.loadMore)
        programsList = requireView().findViewById(R.id.programsList)
        val goal = Goal.values().firstOrNull { it.id == viewModel.state.goalIdSelected.value }
        val goalTitle = goal?.title?.let { getString(it) }
        title.text = getString(R.string.choose_your_program_template, goalTitle)
        programsList.adapter = programAdapter
        programsList.itemAnimator = null
    }

    override fun onResume() {
        super.onResume()
        findNavController().currentBackStackEntry
            ?.savedStateHandle?.getLiveData<Boolean>(JoinProgramFragment.JOIN_PROGRAM)
            ?.observeForever(joinObserver)
    }

    private fun setupObservers() {
        observe(viewModel.state.programsItems) {
            programAdapter.submitList(it)
            programAdapter.notifyDataSetChanged()
        }
        observe(viewModel.state.navigateTo) {
            findNavController().navigate(it)
        }
        observe(viewModel.state.programPagination) {
            loadMore.isVisible = (it.page ?: 0) < (it.pages ?: 0)
        }
    }
}
