package com.vessel.app.foodplan.model

import com.vessel.app.Constants
import com.vessel.app.common.model.FoodRecommendation
import com.vessel.app.common.net.data.plan.PlanData

data class FoodPlanItem(
    val title: String,
    val valueProp1: String? = null,
    val foodNutrientsText: String? = null,
    val reminderTime: String? = null,
    val backgroundUrl: String,
    val planName: String,
    val actionTitle: String,
    val isGrouped: Boolean,
    val groupCount: Int,
    val todos: List<PlanData>,
    val plans: List<PlanData>,
    val dayOfWeek: List<Int>? = null,
    val currentPlan: PlanData,
    val canSwipe: Boolean = true,
    val isAdded: Boolean = true,
    val recommendedItem: FoodRecommendation? = null
) {
    fun getDaysOfWeek(): List<Int> {
        var splitDays = dayOfWeek.orEmpty()
        if (splitDays.isNullOrEmpty()) {
            splitDays = Constants.WEEK_DAYS
        }

        return splitDays
    }

    val showReminder = dayOfWeek.isNullOrEmpty().not() && reminderTime.isNullOrEmpty().not()
}
