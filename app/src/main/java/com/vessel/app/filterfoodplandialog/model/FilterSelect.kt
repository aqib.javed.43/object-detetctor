package com.vessel.app.filterfoodplandialog.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FilterSelect(
    @StringRes val title: Int,
    val checked: Boolean,
    val type: FoodFilterAndSortOptions
) : Parcelable {
    @IgnoredOnParcel
    @DrawableRes
    val selectItemBackground: Int = if (checked) {
        R.drawable.white_transparent70_background
    } else {
        R.drawable.rounded_white_alpha
    }
}
