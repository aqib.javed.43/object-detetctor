package com.vessel.app.photocapture.processing.mapper

import com.vessel.app.lfaerrors.model.LFAErrorModel
import com.vessel.app.wellness.net.data.ReagentData
import com.vessel.app.wellness.net.data.ScoreErrorData

object ProcessingScoreErrorMapper {
    fun map(errors: List<ScoreErrorData>) = errors.map {
        LFAErrorModel(
            it.reagent_id,
            it.slot_name,
            it.error_code
        )
    }

    fun mapReagentsData(errors: List<ReagentData>) = errors.map {
        LFAErrorModel(
            it.reagent_id,
            null,
            null
        )
    }
}
