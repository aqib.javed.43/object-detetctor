package com.vessel.app.common.manager

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.DocumentsContract
import android.provider.MediaStore
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FileManager @Inject constructor(
    @ApplicationContext private val context: Context
) {
    fun uriToFile(uri: Uri): File {
        val inputStream: InputStream? = context.contentResolver.openInputStream(uri)
        val file = createFile("jpg")
        val out: OutputStream = FileOutputStream(file)
        val buf = ByteArray(1024)
        var len: Int
        inputStream?.use { stream ->
            while (stream.read(buf).also { len = it } > 0) {
                out.write(buf, 0, len)
            }
            out.close()
        }
        return file
    }

    /**
     * Create a [File] named a using formatted timestamp with the current date and time.
     *
     * @return [File] created.
     */
    private fun createFile(extension: String): File {
        val sdf = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US)
        return File(context.filesDir, "IMG_${sdf.format(Date())}.$extension")
    }

    fun getMediaFile(selectedImageUri: Uri): Pair<String, File>? {
        var contentUri = selectedImageUri
        var realPath = String()
        var displayName = String()
        var cursor: Cursor? = null

        contentUri.path?.let {
            if (DOCUMENTS_DOWNLOADS_PREFIX == selectedImageUri.authority) {
                val id = DocumentsContract.getDocumentId(selectedImageUri).split(":").last()
                if (id.startsWith("msf:")) {
                    contentUri = ContentUris.withAppendedId(Uri.parse(DOCUMENTS_MEDIA_PROVIDER_PATH), id.toLong())
                } else {
                    return writeTempFile(selectedImageUri)
                }
            }

            try {
                val databaseUri: Uri
                val selection: String?
                val selectionArgs: Array<String>?

                val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.DISPLAY_NAME)
                if (DocumentsContract.isDocumentUri(context, contentUri) &&
                    DOCUMENTS_DOWNLOADS_PREFIX != contentUri.authority
                ) {
                    databaseUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    selection = MediaStore.Images.Media._ID + "=?"
                    selectionArgs = arrayOf(DocumentsContract.getDocumentId(selectedImageUri).split(":").last())
                } else {
                    // In case opened from gallery
                    databaseUri = contentUri
                    selection = null
                    selectionArgs = null
                }

                cursor = context.contentResolver.query(
                    databaseUri,
                    projection,
                    selection,
                    selectionArgs,
                    null
                )
                cursor?.let {
                    if (it.moveToFirst()) {
                        val columnIndex = it.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
                        realPath = it.getString(columnIndex) ?: ""
                        val displayNameColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
                        displayName = it.getString(displayNameColumn)?.split(".")?.firstOrNull() ?: ""
                    }

                    return Pair(displayName, File(realPath))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                cursor?.close()
            }
        }

        return null
    }

    private fun writeTempFile(selectedImageUri: Uri): Pair<String, File>? {

        val file = File(context.cacheDir, TEMP_FILE + Objects.requireNonNull(context.contentResolver.getType(selectedImageUri))?.split("/")?.last())
        try {
            context.contentResolver.openInputStream(selectedImageUri)?.use { inputStream ->
                FileOutputStream(file).use { output ->
                    val buffer = ByteArray(BUFFER_SIZE)
                    var read: Int
                    while (inputStream.read(buffer).also { read = it } != -1) {
                        output.write(buffer, 0, read)
                    }
                    output.flush()
                }
                val cursor = context.contentResolver.query(
                    selectedImageUri,
                    arrayOf(MediaStore.Images.Media.DISPLAY_NAME),
                    null,
                    null,
                    null
                )
                cursor?.let {
                    if (it.moveToFirst()) {
                        val displayNameColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
                        val displayName = cursor.getString(displayNameColumn)?.split(".")?.firstOrNull()
                            ?: ""
                        cursor.close()

                        return Pair(displayName, file)
                    }
                }
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return null
    }

    fun deleteTempFile() {
        val files = context.cacheDir.listFiles()
        files?.firstOrNull { it.name.contains(TEMP_FILE) }?.delete()
    }

    companion object {
        private const val DOCUMENTS_DOWNLOADS_PREFIX = "com.android.providers.downloads.documents"
        private const val DOCUMENTS_MEDIA_PROVIDER_PATH = "content://com.android.providers.media.documents/document/image:"
        private const val TEMP_FILE = "TEMP"
        private const val BUFFER_SIZE = 4 * 1024
    }
}
