package com.vessel.app.programs.programselection

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ProgramSelectionState @Inject constructor(private val passedGoalIdSelected: Int) {

    val goalIdSelected = MutableLiveData(passedGoalIdSelected)
    val programsItems = MutableLiveData<List<Program>>()
    val navigateTo = LiveEvent<NavDirections>()
    operator fun invoke(block: ProgramSelectionState.() -> Unit) = apply(block)
}
