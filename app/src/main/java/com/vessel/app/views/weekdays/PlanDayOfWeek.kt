package com.vessel.app.views.weekdays

import java.util.Calendar

data class PlanDayOfWeek(val calendarDay: Int, val dayIndex: Int)

fun getPlanDaysOfWeek() = listOf(
    PlanDayOfWeek(Calendar.MONDAY, 0),
    PlanDayOfWeek(Calendar.TUESDAY, 1),
    PlanDayOfWeek(Calendar.WEDNESDAY, 2),
    PlanDayOfWeek(Calendar.THURSDAY, 3),
    PlanDayOfWeek(Calendar.FRIDAY, 4),
    PlanDayOfWeek(Calendar.SATURDAY, 5),
    PlanDayOfWeek(Calendar.SUNDAY, 6)
)

fun getPlanDaysOfWeekSorted(): List<PlanDayOfWeek> {
    val currentDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
    val daysOfWeek = getPlanDaysOfWeek()
    val indexOfDay = daysOfWeek.indexOfFirst { it.calendarDay == currentDay }
    val firstItems = daysOfWeek.subList(0, indexOfDay)
    val lastItems = daysOfWeek.subList(indexOfDay, daysOfWeek.size)
    return mutableListOf<PlanDayOfWeek>().apply {
        addAll(lastItems)
        addAll(firstItems)
    }
}

fun getCalendarDayByIndex(index: Int) =
    getPlanDaysOfWeek().first { it.dayIndex == index }.calendarDay
