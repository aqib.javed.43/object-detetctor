package com.vessel.app.today.selectprogram

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_program_select.*

@AndroidEntryPoint
class ProgramSelectFragment : BaseFragment<ProgramSelectViewModel>() {
    override val viewModel: ProgramSelectViewModel by viewModels()
    override val layoutResId = R.layout.fragment_program_select

    val goalSelectAdapter by lazy { ProgramSelectAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        goalList.adapter = goalSelectAdapter
        goalList.itemAnimator = null
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(items) {
                goalSelectAdapter.submitList(it) {
                    goalSelectAdapter.notifyDataSetChanged()
                }
            }
            observe(backToHome) {
                findNavController().popBackStack(R.id.homeScreenFragment, false)
            }
        }
    }
}
