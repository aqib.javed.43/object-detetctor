package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ScoreErrorData(
    val error_code: Int?,
    val slot_name: String?,
    val reagent_id: Int?,
)
