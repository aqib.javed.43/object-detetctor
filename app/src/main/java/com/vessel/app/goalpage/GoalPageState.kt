package com.vessel.app.goalpage

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.goalpage.model.GoalPageHeader
import com.vessel.app.goalpage.model.GoalPageReagent
import com.vessel.app.goalpage.model.GoalPageRecommends
import com.vessel.app.goalpage.model.GoalPageTodayActivity
import com.vessel.app.util.LiveEvent
import com.vessel.app.wellness.filterpopup.ItemFilterCategory
import com.vessel.app.wellness.filterpopup.ItemFilterChoice
import com.vessel.app.wellness.filterpopup.ItemFilterChoiceType
import com.vessel.app.wellness.model.Score
import com.vessel.app.wellness.model.Tag
import javax.inject.Inject

class GoalPageState @Inject constructor() {
    val goalHeaderItems = MutableLiveData<GoalPageHeader>()
    val footerItems = MutableLiveData<List<Any>>()
    val navigateTo = LiveEvent<NavDirections>()
    val reagentItems = MutableLiveData<List<GoalPageReagent>>()
    var score: Score? = null
    var selectedTagsList: List<Tag>? = null
    val todayActivitiesItems = MutableLiveData<List<GoalPageTodayActivity>>()
    val recommendations = MutableLiveData<List<GoalPageRecommends>>()
    var filterCategories = listOf(
        ItemFilterCategory(
            "Sort by",
            listOf(
                ItemFilterChoice("Recommendation", false, ItemFilterChoiceType.RECOMMENDATION),
                ItemFilterChoice("Newest", false, ItemFilterChoiceType.NEWEST),
                ItemFilterChoice("Highest rated", true, ItemFilterChoiceType.HIGHEST_RATE),
            ),
            singleSelection = true
        )
    )
    val filterSortChoices = MutableLiveData(getFilterChoices())
    fun getFilterChoices(): List<ItemFilterChoice> {
        val selectedChoices = mutableListOf<ItemFilterChoice>()
        filterCategories.filter { it.visibleInHome }.onEach { category ->
            selectedChoices.addAll(category.choiceItems.filter { it.isChecked })
        }
        return selectedChoices
    }
    operator fun invoke(block: GoalPageState.() -> Unit) = apply(block)
}
