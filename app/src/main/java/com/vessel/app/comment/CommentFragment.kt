package com.vessel.app.comment

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.binding.setUnderlineSpan
import com.vessel.app.teampage.model.PostType
import com.vessel.app.teampage.model.TeamPostItem
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.ItemPollView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_comment.*
import kotlinx.android.synthetic.main.item_post_header.*
import kotlinx.android.synthetic.main.post_action_view.*

@AndroidEntryPoint
class CommentFragment : BaseFragment<CommentViewModel>() {
    override val viewModel: CommentViewModel by viewModels()
    override val layoutResId = R.layout.fragment_comment

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        observeValues()
        setupBackPressedDispatcher()
    }

    private fun setupViews() {
        btnFollow.setOnClickListener {
            viewModel.actionFollow()
        }
        btnFlag.setOnClickListener {
            viewModel.onReport()
        }
    }

    private fun updateView(item: TeamPostItem) {
        lblName.text = item.actor
        lblText.text = item.title
        pollLayout.visibility = if (item.type == PostType.POLL) View.VISIBLE else View.GONE
        rcvComments.visibility = if (item.type == PostType.POLL) View.GONE else View.VISIBLE
        lblTime.text = item.time
        if (item.type == PostType.POLL) {
            pollLayout.removeAllViews()
            item.pollQuestions?.question?.let {
                postContent.text = it
            }
            item.pollQuestions?.response1.let {
                addPollOptions(it, 1, item)
            }
            item.pollQuestions?.response2.let {
                addPollOptions(it, 2, item)
            }
            item.pollQuestions?.response3.let {
                addPollOptions(it, 3, item)
            }
            item.pollQuestions?.response4.let {
                addPollOptions(it, 4, item)
            }
        } else {
            postContent.text = item.custompr
        }
        btnFollow.setUnderlineSpan(if (item.isFollowing) "Following" else "Follow")
    }
    private fun addPollOptions(question: String?, response: Int, item: TeamPostItem) {
        if (question.isNullOrEmpty()) return
        val pollView = ItemPollView(requireContext())
        pollView.apply {
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            layoutParams.bottomMargin = 20
            setLayoutParams(layoutParams)
            setQuestion(question)
            onSelected = {
                viewModel.reactionPoll(response)
            }
            pollLayout.addView(this)
        }
    }
    private fun observeValues() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(postItem) {
                updateView(it)
            }
        }
        viewModel.setPostItem(
            postItem = arguments?.getSerializable(Constants.POST_ITEM) as TeamPostItem
        )
    }

    private fun setupBackPressedDispatcher() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.let { viewModel.onBackButtonClicked(it) }
        }
    }
}
