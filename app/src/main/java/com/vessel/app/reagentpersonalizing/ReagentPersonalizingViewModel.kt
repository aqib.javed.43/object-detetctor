package com.vessel.app.reagentpersonalizing

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.squareup.moshi.Moshi
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.Sample
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.model.data.ReagentLevel
import com.vessel.app.common.model.data.UtiLevel
import com.vessel.app.common.net.data.plan.BuildPlanRequest
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.ReagentLifestyleRecommendation
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.reagent.ReagentConstant
import com.vessel.app.reagent.model.ReagentDetailsJsonAdapter
import com.vessel.app.reagent.model.ReagentScreen
import com.vessel.app.reagent.model.ReagentScreens
import com.vessel.app.reagentpersonalizing.model.ReagentRecommendationModel
import com.vessel.app.reagentpersonalizing.ui.ReagentPersonalizingFragmentDirections
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.RecommendationType
import kotlinx.coroutines.launch

class ReagentPersonalizingViewModel @ViewModelInject constructor(
    val resourceProvider: ResourceRepository,
    val trackingManager: TrackingManager,
    val state: ReagentRecommendationState,
    val reagentsManager: ReagentsManager,
    val moshi: Moshi,
    private val homeTabsManager: HomeTabsManager,
    private val planManager: PlanManager,
    private val recommendationManager: RecommendationManager,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    val buildPlanManager: BuildPlanManager,
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider) {
    private var isAllDone: Boolean = false
    private var isFinalScreenProcessed: Boolean = false
    private var screens: List<ReagentScreen> = arrayListOf()
    private var finalScreen: ReagentScreen? = null
    private var currentPos = 0
    private var currentReagentPos = 0
    private lateinit var testingPlan: Sample
    private var hasTestPlan: Boolean = false
    var isNavigated = false
    private var shouldShowFinalScreen = savedStateHandle.get<Boolean>("showFinalScreen")

    init {
        logEvent(TrackingConstants.PLAN_BUILDER_STARTED)
        processReagentRecommendation()
        loadTestingPlan()
    }

    companion object {
        const val TAG = "processReagentRecommendation"
    }
    private fun processReagentRecommendation() {
        val reagents = buildPlanManager.getReagentRecommendation()
        Log.d(TAG, "processReagentRecommendation: $reagents")
        if (reagents.isEmpty()) {
            processLastScreen()
            isAllDone = true
        } else {
            val currentReagent = reagents[currentReagentPos]
            state.reagent.value = currentReagent
            state.icon.value = reagentsManager.fromId(currentReagent.reagentId)!!.headerImage
            fetchReagentInfo(currentReagent)
        }
    }

    private fun isLastStep(): Boolean {
        val reagents = buildPlanManager.getReagentRecommendation()
        return reagents.size == 1
    }
    private fun processLastScreen() {
        val lastReagent = buildPlanManager.getLastReagent()
        lastReagent?.let {
            state.icon.value = reagentsManager.fromId(it.reagentId)!!.headerImage
            state.title.value = resourceProvider.getString(R.string.build_plan_title_final)
            state.description.value = resourceProvider.getString(R.string.build_plan_description_final)
        }
    }

    private fun fetchReagentInfo(reagent: ReagentRecommendationModel) {
        viewModelScope.launch {
            resourceProvider.readFile(ReagentConstant.FILE_TEMPLATE_JSON.format(reagent.reagentId, reagent.reagentId))?.let {
                val reagentDetails = ReagentDetailsJsonAdapter(moshi).fromJson(it)
                val recommendation = if (reagent.level == ReagentLevel.Low || reagent.utiLevel == UtiLevel.Abnormal) reagentDetails?.lowRecommendations else reagentDetails?.highRecommendations
                if (recommendation == null) {
                    processAutoBuildPlan()
                } else {
                    if (shouldShowFinalScreen == false)processScreen(recommendation)
                    else {
                        shouldShowFinalScreen(recommendation)
                    }
                }
            }
        }
    }

    private fun shouldShowFinalScreen(reagentScreen: ReagentScreens) {
        this.finalScreen = reagentScreen.final
        state.title.value = finalScreen?.title
        state.description.value = finalScreen?.subTitle
        isFinalScreenProcessed = true
    }

    private fun processAutoBuildPlan() {
        when (state.reagent.value!!.recommendationType) {
            RecommendationType.Food -> {
                gotoFoodRecommend()
            }
            RecommendationType.Supplement -> {
                addSupplementRecommendation()
            }
            RecommendationType.Hydration -> {
                gotoWaterRecommendation()
            }
            RecommendationType.Tip -> {
                processNextStep()
            }
            RecommendationType.Lifestyle -> {
                goToLifestyle(false)
            }
            else -> {
                processNextStep()
            }
        }
    }
    private fun goToLifestyle(isLastStep: Boolean) {
        state.navigateTo.value =
            ReagentPersonalizingFragmentDirections.actionCreatePlanRecommendationFragmentToOnboardingLifestyleFragment(
                isLastStep,
                true
            )
    }
    private fun addHydrationRecommendation() {
        if (!preferencesRepository.isAddedHydrationRecommendation) {
            preferencesRepository.isAddedHydrationRecommendation = true
            showLoading()
            viewModelScope.launch {
                val recommendationResponse = recommendationManager.getLatestSampleRecommendations()
                hideLoading()
                if (recommendationResponse is Result.Success) {
                    val hydrationPlanId =
                        recommendationResponse.value.lifestyle.firstOrNull { it.lifestyle_recommendation_id == Constants.HYDRATION_PLAN_ID }?.id ?: return@launch

                    val buildPlanRequest = BuildPlanRequest(
                        food_ids = listOf(),
                        reagent_lifestyle_recommendations_ids = listOf(hydrationPlanId),
                        supplements_ids = listOf(),
                        is_auto_build = true
                    )
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_BUILD_SUBMITTED)
                            .addProperty("data", buildPlanRequest.toString())
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    processNextStep()
                }
            }
        } else {
            processNextStep()
        }
    }

    private fun addSupplementRecommendation() {
        if (!preferencesRepository.isAddedSupplementRecommendation) {
            preferencesRepository.isAddedSupplementRecommendation = true
            viewModelScope.launch {
                val recommendationResponse = recommendationManager.getLatestSampleRecommendations()
                if (recommendationResponse is Result.Success) {
                    val supIds: ArrayList<Int> = arrayListOf()
                    for (sup in buildPlanManager.getSupplementRecommendations()) {
                        supIds.add(sup.id)
                    }

                    val buildPlanRequest = BuildPlanRequest(
                        food_ids = listOf(),
                        reagent_lifestyle_recommendations_ids = listOf(),
                        supplements_ids = supIds,
                        is_auto_build = true
                    )
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_BUILD_SUBMITTED)
                            .addProperty("data", buildPlanRequest.toString())
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    processNextStep()
                }
            }
        }
    }

    private fun processNextStep() {
        shouldShowFinalScreen = false
        isFinalScreenProcessed = false
        state.reagent.value?.reagentId?.let { buildPlanManager.removeReagentRecommendation(reagentId = it) }
        processReagentRecommendation()
    }

    private fun processScreen(reagentScreen: ReagentScreens) {
        currentPos = 0
        this.finalScreen = reagentScreen.final
        reagentScreen.screens?.let {
            this.screens = it
            updateInfo()
        }
    }

    private fun updateInfo() {
        state.title.value = this.screens[currentPos].title
        state.description.value = this.screens[currentPos].subTitle
        currentPos += 1
    }

    fun onNextClicked() {
        if (isAllDone) endTheFlow()
        else {
            if (currentPos < this.screens.size) {
                updateInfo()
            } else {
                gotoFinalScreen()
            }
        }
    }

    private fun gotoFinalScreen() {
        if (isLastStep()) {
            val lastReagent = state.reagent.value
            lastReagent?.let {
                lastReagent.lastTitle = state.title.value ?: ""
                lastReagent.lastRecommendation = state.description.value ?: ""
                buildPlanManager.setLastReagent(lastReagent)
            }
        }
        if (isFinalScreenProcessed && shouldShowFinalScreen == true) {
            processNextStep()
        } else {
            if (finalScreen?.title.isNullOrEmpty() || isShouldShowFoodRecommendation()) isFinalScreenProcessed = true
            if (isFinalScreenProcessed) {
                processAutoBuildPlan()
            } else {
                state.title.value = finalScreen?.title
                state.description.value = finalScreen?.subTitle
                isFinalScreenProcessed = true
            }
        }
    }

    private fun isShouldShowFoodRecommendation(): Boolean {
        return state.reagent.value?.reagentId == ReagentItem.Magnesium.id || state.reagent.value?.reagentId == ReagentItem.VitaminC.id
    }

    private fun gotoFoodRecommend() {
        if (state.reagent.value?.level != ReagentLevel.Low && state.reagent.value?.reagentId != ReagentItem.Ketones.id) {
            processNextStep()
        } else {
            state.navigateTo.value = state.reagent.value?.reagentId?.let {
                ReagentPersonalizingFragmentDirections.actionReagentPersonalizingFragmentToFoodRecommendationFragment(
                    it
                )
            }
        }
    }
    private fun gotoWaterRecommendation() {
        state.navigateTo.value = ReagentPersonalizingFragmentDirections.actionReagentPersonalizingFragmentToWaterRecommendationFragment()
    }
    private fun goToNextStep() {
        if (preferencesRepository.testCompletedCount == 1) {
            goToTestingReminder()
        } else if (preferencesRepository.testCompletedCount > 1 && preferencesRepository.showTestingTimerInThePostFlow) {
            preferencesRepository.showTestingTimerInThePostFlow = false
            goToTestingReminder()
        } else {
            state.navigate.call()
        }
    }

    private fun goToTestingReminder() {
        if (::testingPlan.isInitialized.not()) {
            loadTestingPlan(true)
            return
        }
        if (hasTestPlan) {
            state.navigate.call()
        } else {
            navigateToTestingReminder()
        }
    }
    private fun navigateToTestingReminder() {
        state.navigateTo.value =
            ReagentPersonalizingFragmentDirections.actionReagentPersonalizingFragmentToTestingReminderDialog(
                PlanReminderHeader(
                    getResString(R.string.wellness_testing),
                    "",
                    "",
                    R.drawable.ic_take_test,
                    listOf(getTestingPlanData()),
                    isEmptyPlan = true,
                    isTestingPlan = true
                )
            )
    }
    private fun loadTestingPlan(showLoading: Boolean = false) {
        if (showLoading)showLoading()
        viewModelScope.launch {
            val plansResponse = planManager.getUserPlans(true)
            val recommendationResponse = recommendationManager.getLatestSampleRecommendations()
            if (plansResponse is Result.Success && recommendationResponse is Result.Success) {
                hideLoading(false)
                val recommendations = recommendationResponse.value.lifestyle
                testingPlan = recommendations.firstOrNull { it.lifestyle_recommendation_id == Constants.TEST_PLAN_ID } ?: return@launch
                hasTestPlan = plansResponse.value.any { it.isTestingPlan() }
            }
        }
    }

    private fun getTestingPlanData() = PlanData(
        multiple = 1,
        reagent_lifestyle_recommendation_id = testingPlan.id ?: Constants.TEST_PLAN_ID,
        reagent_lifestyle_recommendation = ReagentLifestyleRecommendation(
            id = -1,
            lifestyle_recommendation_id = Constants.TEST_PLAN_ID,
            quantity = testingPlan.amount,
            unit = testingPlan.unit,
            frequency = testingPlan.frequency
        )
    )
    private fun endTheFlow() {
        isNavigated = true
        devicePreferencesRepository.showPlanUpdatedDialog = false
        planManager.clearCachedData()
        recommendationManager.updateLatestSampleFoodRecommendationsCachedData()
        recommendationManager.updateLatestSampleRecommendationsCachedData()
        logEvent(TrackingConstants.PLAN_BUILDER_COMPLETE)
        homeTabsManager.clear()
        goToNextStep()
    }
    private fun logEvent(event: String) {
        trackingManager.log(
            TrackedEvent.Builder(event)
                .setScreenName(TrackingConstants.FROM_PLAN_BUILDER)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }
}
