package com.vessel.app.onboarding.diets.ui

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.onboarding.diets.OnboardingDietsViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingDietsFragment : BaseFragment<OnboardingDietsViewModel>() {
    override val viewModel: OnboardingDietsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_diets_onboarding

    val onboardingDietSelectAdapter by lazy { OnboardingDietSelectAdapter(viewModel) }
    private lateinit var backButton: View
    override fun onViewLoad(savedInstanceState: Bundle?) {
        backButton = requireView().findViewById(R.id.toolbar)
        if (viewModel.isBackVisible == true) {
            backButton.isVisible = false
            requireActivity().onBackPressedDispatcher.addCallback(this) {}
        }
        view?.findViewById<ConstraintLayout>(R.id.parentView)?.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                if (viewModel.state.isFirstTestFlow) {
                    R.color.yellow_light
                } else {
                    R.color.colorPrimary
                }
            )
        )
//        view?.findViewById<View>(R.id.backgroundImage)?.isVisible = viewModel.state.isFirstTestFlow.not()

        val dietsList = requireView().findViewById<RecyclerView>(R.id.dietList)
        dietsList.layoutManager = GridLayoutManager(context, Constants.SPAN_COUNT_GOAL_GRID)
        dietsList.adapter = onboardingDietSelectAdapter
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(items) {
                onboardingDietSelectAdapter.submitList(it)
            }
            observe(refreshItems) {
                onboardingDietSelectAdapter.notifyDataSetChanged()
            }
        }
    }
}
