package com.vessel.app.programs.popupdetails

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ProgramInfoDialogState @Inject constructor(private val passedProgram: Program) {

    val program = MutableLiveData(passedProgram)
    val navigateTo = LiveEvent<NavDirections>()
    val dismiss = LiveEvent<Unit>()

    operator fun invoke(block: ProgramInfoDialogState.() -> Unit) = apply(block)
}
