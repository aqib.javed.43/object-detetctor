package com.vessel.app.teams

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.Constants
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.util.ResourceRepository
import com.vessel.app.welcometeams.WelcomeTeamFragmentDirections

class TeamsViewModel@ViewModelInject constructor(
    val state: TeamsState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    fun getTeamData() {
        if (hadMainGoal()) {
            val id = getMainGoal()!!.id
            state.goal.value = Constants.GOALS.firstOrNull {
                it.id == id
            }
        }
    }
    fun onMyCoachClick() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CHAT_TAB_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.navigateTo.value = TeamsFragmentDirections.actionGlobalToNutritionOptionsFragment()
    }
    fun onMyTeamClick() {
        // To do check team lock and go to my team
        if (hadTested()) {
            if (isFirstTimeJoin())state.navigateTo.value = TeamsFragmentDirections.actionGlobalToWelcomeTeamFragment()
            else {
                if (notProfileUpdate()) state.navigateTo.value = TeamsFragmentDirections.actionGlobalProvideReasonFragment()
                else state.navigateTo.value = WelcomeTeamFragmentDirections.actionGlobalHomeTeamFragment()
            }
        } else {
            showAlert("Please take a first test", "")
        }
    }
    fun notProfileUpdate(): Boolean {
        return preferencesRepository.getContact()?.imageUrl.isNullOrEmpty()
    }
    fun hadMainGoal(): Boolean {
        return preferencesRepository.getUserMainGoal() != null
    }
    fun hadTested(): Boolean {
        return preferencesRepository.testCompletedCount > 0
    }
    fun getMainGoal(): GoalSample? {
        return preferencesRepository.getUserMainGoal()
    }
    fun isFirstTimeJoin(): Boolean {
        return preferencesRepository.isFirstTimeClickTeam
    }
}
