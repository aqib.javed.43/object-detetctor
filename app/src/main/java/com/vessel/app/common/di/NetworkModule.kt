package com.vessel.app.common.di

import android.content.Context
import com.amazonaws.ClientConfiguration
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.squareup.moshi.Moshi
import com.vessel.app.BuildConfig
import com.vessel.app.BuildConfig.DEBUG
import com.vessel.app.common.net.RestClient
import com.vessel.app.common.net.TokenAuthenticator
import com.vessel.app.common.net.customparse.GoalDataParseAdapter
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BASIC
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {
    @Singleton
    @Provides
    fun moshi(): Moshi = Moshi.Builder().add(GoalDataParseAdapter()).build()

    @Singleton
    @Provides
    fun moshiConverterFactory(moshi: Moshi): MoshiConverterFactory =
        MoshiConverterFactory.create(moshi)

    @Singleton
    @Provides
    fun httpLoggingInterceptor() = HttpLoggingInterceptor()
        .apply { level = if (DEBUG) BODY else BASIC }

    @Singleton
    @Provides
    fun restClient(
        moshiConverterFactory: MoshiConverterFactory,
        httpLoggingInterceptor: HttpLoggingInterceptor,
        tokenAuthenticator: TokenAuthenticator
    ) = RestClient(
        debug = DEBUG,
        connectTimeoutValue = BuildConfig.CONNECT_TIMEOUT.toLong(),
        readTimeoutValue = BuildConfig.READ_TIMEOUT.toLong(),
        writeTimeoutValue = BuildConfig.WRITE_TIMEOUT.toLong(),
        converterFactories = listOf(moshiConverterFactory),
        interceptors = listOf(httpLoggingInterceptor),
        tokenAuthenticator = tokenAuthenticator
    )

    @Singleton
    @Provides
    fun transferUtility(@ApplicationContext context: Context, remoteConfiguration: RemoteConfiguration): TransferUtility {
        TransferNetworkLossHandler.getInstance(context)
        val clientConfig = ClientConfiguration()
        clientConfig.socketTimeout = TIME_OUT_PERIOD.toInt()
        clientConfig.connectionTimeout = TIME_OUT_PERIOD.toInt()
        clientConfig.maxErrorRetry = MAX_ERROR_RETRY

        val credentials = BasicAWSCredentials(remoteConfiguration.getString(S3_ACCESS_KEY), remoteConfiguration.getString(S3_SECRET_KEY))
        val s3Client = AmazonS3Client(credentials, Region.getRegion(Regions.US_WEST_2), clientConfig)
        return TransferUtility.builder()
            .s3Client(s3Client)
            .context(context)
            .build()
    }

    companion object {
        private val TIME_OUT_PERIOD = TimeUnit.MINUTES.toMillis(2)
        private const val MAX_ERROR_RETRY = 2
        private const val S3_ACCESS_KEY = "S3_ACCESS_KEY"
        private const val S3_SECRET_KEY = "S3_SECRET_KEY"
    }
}
