package com.vessel.app.onboarding.ratemaingoal

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.GoalsRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.homescreen.HomeTabsManager
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import java.util.*

class RateMainGoalSelectionViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val goalsRepository: GoalsRepository,
    val homeTabsManager: HomeTabsManager,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
    private val contactManager: ContactManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler {
    val bgColor = if (preferencesRepository.isExistingUserFirstTime) R.color.beige_color else R.color.primaryLight
    val state = RateMainGoalSelectionState()

    init {
        val goalSelected = goalsRepository.getUserMainGoal()
        setGoal(goalSelected)
    }
    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    var selectedFeelings = FEELING_NOTHING

    fun onSplitTypeChanged(id: Int) {
        selectedFeelings = when (id) {
            R.id.sadFaceRadioButton -> FEELING_UNHAPPY
            R.id.normalFaceRadioButton -> FEELING_MEH
            R.id.happyFaceRadioButton -> FEELING_HAPPY
            else -> FEELING_NOTHING
        }
    }

    fun onDoneClicked() {
        if (selectedFeelings == FEELING_MEH || selectedFeelings == FEELING_UNHAPPY) {
            state.navigateTo.value = RateMainGoalSelectionFragmentDirections.actionRateMainGoalSelectionFragmentToProblemSelectionFragment()
        } else if (selectedFeelings == FEELING_HAPPY) {
            state.navigateTo.value = RateMainGoalSelectionFragmentDirections.actionRateMainGoalSelectionFragmentToPickAnotherGoalFragment()
        }
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.CHECKIN_STARTED)
                .addProperty("program name", state.goal.value?.name.toString())
                .addProperty("time", Calendar.getInstance().time.formatDayDate())
                .setEmail(preferencesRepository.getContact()?.email ?: "")
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun setGoal(goal: GoalSample?) {
        goal?.let {
            state.goal.value = it
        }
    }

    companion object {
        const val FEELING_NOTHING = ""
        const val FEELING_UNHAPPY = "unhappy"
        const val FEELING_MEH = "meh"
        const val FEELING_HAPPY = "happy"
    }
}
