package com.vessel.app.today.review

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ActivityReviewState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val remainingCharacters = MutableLiveData<String>()
    val likeBackground = MutableLiveData<Int>()
    val textColor = MutableLiveData<Int>()
    val disLikeBackground = MutableLiveData<Int>()
    operator fun invoke(block: ActivityReviewState.() -> Unit) = apply(block)
}
