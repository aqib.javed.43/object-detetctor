package com.vessel.app.postreport

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.Constants
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.TeamsRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.teampage.model.TeamPostItem
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.Action
import com.vessel.app.wellness.net.data.FeedFlag
import kotlinx.coroutines.launch

open class PostReportViewModel@ViewModelInject constructor(
    val state: PostReportState,
    val resourceProvider: ResourceRepository,
    private val teamsRepository: TeamsRepository,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler {
    init {
        state.remainingTextCount.value = Constants.TEXT_MAX_COUNT
    }
    override fun onBackButtonClicked(view: View) {
        view.findNavController().navigateUp()
    }
    // Flag post
    fun flagPost(postItem: TeamPostItem, reason: String, view: View) {
        val feedFlag = FeedFlag()
        feedFlag.activityId = postItem.id
        feedFlag.actor = postItem.actor
        feedFlag.verb = Action.REPORT.kind
        feedFlag.reason = reason
        viewModelScope.launch {
            showLoading()
            teamsRepository.flagPost(feedFlag)
                .onSuccess { result ->
                    hideLoading()
                    view.findNavController().navigateUp()
                }
                .onError {
                    hideLoading()
                }
        }
    }
    // On text change
    fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        state.remainingTextCount.value = Constants.TEXT_MAX_COUNT - s.length
    }
}
