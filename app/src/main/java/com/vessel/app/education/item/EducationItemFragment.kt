package com.vessel.app.education.item

import android.net.Uri
import android.os.Bundle
import android.text.format.DateUtils
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.manager.ChatManager
import com.vessel.app.education.ui.EducationFragment
import com.vessel.app.education.ui.EducationPage
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import zendesk.chat.ChatEngine
import zendesk.messaging.MessagingActivity

@AndroidEntryPoint
class EducationItemFragment : BaseFragment<EducationItemViewModel>() {
    override val viewModel: EducationItemViewModel by viewModels()
    override val layoutResId = R.layout.fragment_education_item

    private val exoPlayer by lazy { SimpleExoPlayer.Builder(requireContext()).build() }

    private var playWhenReady = false

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
        setupPlayer()
        setupAdapter()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(backEvent) {
                val parent =
                    parentFragmentManager.primaryNavigationFragment?.childFragmentManager?.fragments?.firstOrNull()
                if (parent is EducationFragment) parent.onBackClicked()
            }
            observe(continueEvent) {
                val parent =
                    parentFragmentManager.primaryNavigationFragment?.childFragmentManager?.fragments?.firstOrNull()
                if (parent is EducationFragment) parent.onContinueClicked()
            }
            observe(launchLiveChatEvent) {
                MessagingActivity.builder()
                    .withEngines(ChatEngine.engine())
                    .withBotAvatarDrawable(R.mipmap.ic_launcher_round)
                    .show(requireContext(), ChatManager.chatConfiguration)
            }
        }
    }

    private fun setupAdapter() {
        view?.findViewById<RecyclerView>(R.id.questions)
            ?.adapter = EducationItemFaqAdapter(viewModel.state.faqs, viewLifecycleOwner)
    }

    private fun setupPlayer() {
        requireView().findViewById<PlayerView>(R.id.instructionPlayerView)
            ?.run {
                player = exoPlayer
                controllerShowTimeoutMs = (2 * DateUtils.SECOND_IN_MILLIS).toInt()
            }
        exoPlayer.run {
            val defaultSourceFactory =
                DefaultDataSourceFactory(requireContext(), BuildConfig.APPLICATION_ID)
            val mediaItem = MediaItem.fromUri(Uri.parse(viewModel.state.videoUrl))
            val assetVideoSource = ProgressiveMediaSource.Factory(defaultSourceFactory)
                .createMediaSource(mediaItem)
            addMediaSource(assetVideoSource)
            prepare()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putLong(KEY_PLAYER_POSITION, exoPlayer.contentPosition)
        outState.putBoolean(KEY_PLAYER_PLAY_WHEN_READY, playWhenReady)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        if (savedInstanceState == null) {
            exoPlayer.playWhenReady = true
        } else {
            exoPlayer.seekTo(savedInstanceState.getLong(KEY_PLAYER_POSITION))
            exoPlayer.playWhenReady = savedInstanceState.getBoolean(KEY_PLAYER_PLAY_WHEN_READY)
        }
    }

    override fun onStop() {
        super.onStop()
        exoPlayer.playWhenReady = false
        exoPlayer.stop()
    }

    override fun onPause() {
        super.onPause()

        playWhenReady = exoPlayer.playWhenReady
        exoPlayer.playWhenReady = false
    }

    companion object {
        private const val KEY_PLAYER_POSITION = "EducationItemFragment.KEY_PLAYER_POSITION"
        private const val KEY_PLAYER_PLAY_WHEN_READY =
            "EducationItemFragment.KEY_PLAYER_PLAY_WHEN_READY"
        const val ARG_PAGE = "EducationItemFragment.ARG_PAGE"

        fun newInstance(page: EducationPage) = EducationItemFragment()
            .apply { arguments = bundleOf(ARG_PAGE to page) }
    }
}
