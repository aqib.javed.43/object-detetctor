package com.vessel.app.onboarding.height

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.base.MutableLiveArrayList
import com.vessel.app.common.model.Contact
import com.vessel.app.common.util.DefaultAdapter
import com.vessel.app.util.SliderLayoutManager
import com.vessel.app.util.WindowUtils
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HeightDialogFragment : BaseDialogFragment<HeightDialogViewModel>() {
    override val viewModel: HeightDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_height_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()

        setupHeightSelector(
            R.id.heightFeet,
            Contact.Height.feetRange,
            viewModel.height.feet
        )
        setupHeightSelector(
            R.id.heightInches,
            Contact.Height.inchesRange,
            viewModel.height.inches
        )
    }

    private fun setupObservers() {
        observe(viewModel.dismissDialog) {
            dismiss()
        }

        observe(viewModel.heightErrorEvent) {
            Toast.makeText(
                requireContext(),
                getString(R.string.invalid_height_value),
                Toast.LENGTH_SHORT
            ).show()
        }

        observe(viewModel.heightSet) {
            findNavController().previousBackStackEntry
                ?.savedStateHandle
                ?.set(HEIGHT_KEY, it)
        }
    }

    private fun setupHeightSelector(
        @IdRes recyclerViewIdRes: Int,
        range: List<String>,
        initialPosition: Int
    ) {
        view?.findViewById<RecyclerView>(recyclerViewIdRes)?.let {
            val padding = WindowUtils.getDisplayMetrics(requireContext()).widthPixels / 2
            it.setPadding(0, padding, 0, padding)

            val layoutManager = SliderLayoutManager(it) { positionSelected ->
                if (recyclerViewIdRes == R.id.heightFeet) {
                    viewModel.onFeetSelected(positionSelected)
                } else {
                    viewModel.onInchesSelected(positionSelected)
                }
            }
            it.layoutManager = layoutManager
            it.adapter = DefaultAdapter(
                MutableLiveArrayList(range),
                viewLifecycleOwner,
                R.layout.item_radio_option_vertical
            )

            layoutManager.slideToPosition(initialPosition)
        }
    }

    companion object {
        const val HEIGHT_KEY = "height"
    }
}
