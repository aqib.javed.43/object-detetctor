package com.vessel.app.score.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.score.model.ScoreGoalItem

class ScoreGoalAdapter : BaseAdapterWithDiffUtil<ScoreGoalItem>() {
    override fun getLayout(position: Int) = R.layout.item_score_goal
}
