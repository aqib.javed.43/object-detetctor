package com.vessel.app.onboarding.finalonboarding.stepthree

import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnBoardingFinalStepThreeState @Inject constructor() {
    val navigateTo = LiveEvent<NavigationDestination>()
    val backToHome = LiveEvent<Unit>()
    operator fun invoke(block: OnBoardingFinalStepThreeState.() -> Unit) = apply(block)
    enum class NavigationDestination {
        WELCOME, ONBOARDING, HOME
    }
}
