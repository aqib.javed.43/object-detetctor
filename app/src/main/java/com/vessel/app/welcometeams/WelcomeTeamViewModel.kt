package com.vessel.app.welcometeams

import android.util.Log
import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.findNavController
import com.vessel.app.Constants
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.util.ResourceRepository

class WelcomeTeamViewModel@ViewModelInject constructor(
    val state: WelcomeTeamState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler {
    init {
    }
    override fun onBackButtonClicked(view: View) {
        Log.d(TAG, "onBackButtonClicked: ")
        view.findNavController().navigateUp()
    }
    fun onContinueClick() {
        Log.d(TAG, "onContinueClick: ")
        // Todo process continue
        if (notProfileUpdate()) state.navigateTo.value = WelcomeTeamFragmentDirections.actionWelcomeTeamFragmentToProvideReasonFragment()
        else state.navigateTo.value = WelcomeTeamFragmentDirections.actionGlobalHomeTeamFragment()
    }
    companion object {
        const val TAG = "WelcomeTeamViewModel"
    }
    fun getTeamData() {
        if (hadMainGoal()) {
            val id = getMainGoal()!!.id
            state.goal.value = Constants.GOALS.firstOrNull {
                it.id == id
            }
        }
    }
    fun getMainGoal(): GoalSample? {
        return preferencesRepository.getUserMainGoal()
    }
    fun hadMainGoal(): Boolean {
        return preferencesRepository.getUserMainGoal() != null
    }
    fun notProfileUpdate(): Boolean {
        return preferencesRepository.getContact()?.imageUrl.isNullOrEmpty()
    }
}
