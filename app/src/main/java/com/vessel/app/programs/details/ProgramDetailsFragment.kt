package com.vessel.app.programs.details

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.adapter.genericlikeordislikeadapter.LikeOrDislikeAdapter
import com.vessel.app.programs.details.adapters.*
import com.vessel.app.programs.join.JoinProgramFragment
import com.vessel.app.programs.leave.LeaveProgramDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_program_details.*

@SuppressLint("ResourceType")
@AndroidEntryPoint
class ProgramDetailsFragment : BaseFragment<ProgramDetailsViewModel>() {
    override val viewModel: ProgramDetailsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_program_details_new

    private val programHeader by lazy { ProgramHeaderAdapter(viewModel) }
    private val recommendedByAdapter by lazy { RecommendedByAdapter(viewModel) }
    private val scheduleAdapter by lazy { ProgramScheduleAdapter(viewModel, viewModel) }
    private val essentialListAdapter by lazy { EssentialAdapter(viewModel) }
    private val likesOrDislikeAdapter by lazy {
        LikeOrDislikeAdapter(
            viewModel,
            R.layout.item_like_or_dislike_program_details
        )
    }
    private val programFooterAdapter by lazy { ProgramFooterAdapter(viewModel) }
    private val joinObserver = Observer<Boolean> { value ->
        if (value) {
            viewModel.getProgramDetails()
        }
    }
    private val leaveObserver = Observer<Boolean> { value ->
        if (value) {
            viewModel.markRemoved()
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    override fun onResume() {
        super.onResume()
        findNavController().currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData<Boolean>(LeaveProgramDialogFragment.REMOVE_PROGRAM)
            ?.observeForever(leaveObserver)

        findNavController().currentBackStackEntry
            ?.savedStateHandle?.getLiveData<Boolean>(JoinProgramFragment.JOIN_PROGRAM)
            ?.observeForever(joinObserver)
    }

    override fun onDestroy() {
        super.onDestroy()
        findNavController().currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData<Boolean>(LeaveProgramDialogFragment.REMOVE_PROGRAM)
            ?.removeObserver(leaveObserver)

        findNavController().currentBackStackEntry
            ?.savedStateHandle?.getLiveData<Boolean>(JoinProgramFragment.JOIN_PROGRAM)
            ?.removeObserver(joinObserver)
    }

    private fun setupList() {
        programDetailsList.also {
            it.itemAnimator = null
            it.adapter = getConcatAdapter()
        }
    }

    private fun getConcatAdapter(): ConcatAdapter {
        val adapters = if (viewModel.state.program.value?.isEnrolled == true)
            listOf(
                programHeader,
                scheduleAdapter,
                recommendedByAdapter,
                essentialListAdapter,
                likesOrDislikeAdapter,
                programFooterAdapter
            )
        else listOf(
            programHeader,
            recommendedByAdapter,
            scheduleAdapter,
            essentialListAdapter,
            likesOrDislikeAdapter,
            programFooterAdapter
        )
        return ConcatAdapter(
            ConcatAdapter.Config.Builder()
                .setIsolateViewTypes(false)
                .build(),
            adapters
        )
    }

    private fun setupObservers() {
        viewModel.state {
            observe(program) { program ->
                val essentials = program.essentials
                val programScheduleUiModel = program.getScheduleUiModel()
                setupList()
                if (programScheduleUiModel.programScheduleList.isNotEmpty()) {
                    scheduleAdapter.submitList(listOf(programScheduleUiModel))
                } else {
                    scheduleAdapter.submitList(listOf())
                }
                essentialListAdapter.submitList(essentials)
                programHeader.submitList(listOf(program))
                recommendedByAdapter.submitList(listOf(program))
                likesOrDislikeAdapter.submitList(listOf(program))
                programFooterAdapter.submitList(listOf(program))
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
