// TODO fix the unit test

// package com.vessel.app.foodplan
//
// import androidx.lifecycle.Observer
// import com.vessel.app.*
// import com.vessel.app.BaseFaker.Fake.FAKER
// import com.vessel.app.common.manager.*
// import com.vessel.app.common.model.*
// import com.vessel.app.recommendation.FoodRecommendationFaker
// import com.vessel.app.todo.TodoFaker
// import com.vessel.app.util.ResourceRepository
// import io.mockk.*
// import org.assertj.core.api.Assertions.assertThat
// import org.junit.jupiter.api.Test
//
// class FoodPlanViewModelTest : BaseTest() {
//    private lateinit var viewModel: FoodPlanViewModel
//
//    private val state = FoodPlanState()
//    private val resourceProvider = mockk<ResourceRepository>()
//    private val recommendationManager = mockk<RecommendationManager>()
//    private val scoreManager = mockk<ScoreManager>()
//    private val todosManager = mockk<TodosManager>()
//
//    private val eventObserver = mockk<Observer<Unit>>()
//    private val foodRecommendations = listOf(
//        FoodRecommendationFaker.basic(),
//        FoodRecommendationFaker.basic(),
//        FoodRecommendationFaker.basic()
//    )
//    private val deficiencies = mapOf(
//        Reagent.VitaminC to FAKER.number().randomNumber().toFloat(),
//        Reagent.Magnesium to FAKER.number().randomNumber().toFloat()
//    )
//    private val todos = listOf(
//        TodoFaker.basic(),
//        TodoFaker.basic(name = foodRecommendations.first().name),
//        TodoFaker.basic()
//    )
//    private val resourceString = FAKER.lorem().word()
//
//    private fun init() {
//        coEvery { scoreManager.reagentDeficiencies() } returns Result.Success(deficiencies)
//        coEvery { recommendationManager.getLatestSampleFoodRecommendations() }
//            .answers { Result.Success(foodRecommendations) }
//        coEvery { todosManager.getRecurringTodos() } returns Result.Success(todos)
//        every { eventObserver.onChanged(null) } just runs
//        every { resourceProvider.getString(any()) } returns resourceString
//
//        viewModel = FoodPlanViewModel(
//            state, resourceProvider, recommendationManager, scoreManager, todosManager
//        )
//    }
//
//    @Test
//    fun `it should build the selected and non-selected recommendation view items`() {
//        init()
//
//        val selectedFoodRec = state.recommendedFoodsSelected.value!!.first()
//        val notSelectedFoodRec1 = state.recommendedFoodsNotSelected.value!!.first()
//        val notSelectedFoodRec2 = state.recommendedFoodsNotSelected.value!![1]
//
//        selectedFoodRec.name isEqualTo foodRecommendations.first().name
//        selectedFoodRec.foodNutrientsText isEqualTo foodRecommendations.first().nutrients
//            .take(3)
//            .joinToString("\n") {
//                "•\t\t$resourceString ${it.amount.toInt()} $resourceString"
//            }
//        selectedFoodRec.buttonResId isEqualTo R.string.remove
//        selectedFoodRec.usdaNdbNumber isEqualTo foodRecommendations.first().usdaNdbNumber
//        selectedFoodRec.backgroundColorResId isEqualTo R.color.white
//
//        notSelectedFoodRec1.name isEqualTo foodRecommendations[1].name
//        notSelectedFoodRec1.foodNutrientsText isEqualTo foodRecommendations[1].nutrients
//            .take(3)
//            .joinToString("\n") {
//                "•\t\t$resourceString ${it.amount.toInt()} $resourceString"
//            }
//        notSelectedFoodRec1.buttonResId isEqualTo R.string.add_it
//        notSelectedFoodRec1.usdaNdbNumber isEqualTo foodRecommendations[1].usdaNdbNumber
//        notSelectedFoodRec1.backgroundColorResId isEqualTo R.color.grayBg
//
//        notSelectedFoodRec2.name isEqualTo foodRecommendations[2].name
//        notSelectedFoodRec2.foodNutrientsText isEqualTo foodRecommendations[2].nutrients
//            .take(3)
//            .joinToString("\n") {
//                "•\t\t$resourceString ${it.amount.toInt()} $resourceString"
//            }
//        notSelectedFoodRec2.buttonResId isEqualTo R.string.add_it
//        notSelectedFoodRec2.usdaNdbNumber isEqualTo foodRecommendations[2].usdaNdbNumber
//        notSelectedFoodRec2.backgroundColorResId isEqualTo R.color.grayBg
//    }
//
//    @Test
//    fun `it should build the reagent levels view items`() = runBlockingTest {
//        val deficiencies = mapOf(
//            Reagent.VitaminC to 10f,
//            Reagent.Magnesium to 20f
//        )
//        val foodRecommendations = listOf(
//            FoodRecommendationFaker.basic(listOf(Nutrient(Reagent.VitaminC, 9f))),
//            FoodRecommendationFaker.basic(listOf(Nutrient(Reagent.Magnesium, 21f)))
//        )
//        coEvery { scoreManager.reagentDeficiencies() } returns Result.Success(deficiencies)
//        coEvery { recommendationManager.getLatestSampleFoodRecommendations() }
//            .answers { Result.Success(foodRecommendations) }
//        val todos = listOf(
//            TodoFaker.basic(name = foodRecommendations[0].name),
//            TodoFaker.basic(name = foodRecommendations[1].name),
//        )
//        coEvery { todosManager.getRecurringTodos() } returns Result.Success(todos)
//        every { eventObserver.onChanged(null) } just runs
//        val good = "Good"
//        every { resourceProvider.getString(R.string.good) } returns good
//        val vitaminCName = "Vitamin C"
//        every { resourceProvider.getString(Reagent.VitaminC.displayName) } returns vitaminCName
//        val magnesiumName = "Magnesium"
//        every { resourceProvider.getString(Reagent.Magnesium.displayName) } returns magnesiumName
//        val needed = "needed"
//        every { resourceProvider.getString(R.string.needed) } returns needed
//
//        val vitaminCUnit = "mg"
//
//        viewModel = FoodPlanViewModel(
//            state, resourceProvider, recommendationManager, scoreManager, todosManager
//        )
//
//        val vitaminCLevel = state.levels.value!![0]
//        vitaminCLevel.name isEqualTo vitaminCName
//        vitaminCLevel.needed isEqualTo "1$vitaminCUnit $needed"
//        vitaminCLevel.progress isEqualTo 90
//        val magnesiumLevel = state.levels.value!![1]
//        magnesiumLevel.name isEqualTo magnesiumName
//        magnesiumLevel.needed isEqualTo good
//        magnesiumLevel.progress isEqualTo 100
//    }
//
//    @Test
//    fun `it should update the cells when adding a food to the plan`() = runBlockingTest {
//        init()
//        val notSelectedFoodRec1 = state.recommendedFoodsNotSelected.value!!.first()
//
//        val todo = TodoFaker.basic(notSelectedFoodRec1.name)
//        coEvery {
//            todosManager.addRecurringTodo(any(), any(), any(), any())
//        }.answers { Result.Success(todo) }
//
//        viewModel.onActionButtonClick(notSelectedFoodRec1)
//
//        state {
//            val firstRecommendedItem = recommendedFoodsSelected.value!![0]
//            firstRecommendedItem.name isEqualTo notSelectedFoodRec1.name
//            firstRecommendedItem.isInPlan.isTrue()
//
//            assertThat(recommendedFoodsNotSelected.value).doesNotContain(notSelectedFoodRec1)
//        }
//    }
//
//    @Test
//    fun `it should update the cells when removing a food from the plan`() = runBlockingTest {
//        init()
//        val selectedFoodRec = state.recommendedFoodsSelected.value!!.first()
//
//        coEvery { todosManager.deleteRecurringTodo(todos[1].id) }
//            .answers { Result.Success(Unit) }
//
//        viewModel.onActionButtonClick(selectedFoodRec)
//
//        state {
//            val firstRecommendedItemNotSelected = recommendedFoodsNotSelected.value!![0]
//            firstRecommendedItemNotSelected.name isEqualTo selectedFoodRec.name
//            firstRecommendedItemNotSelected.isInPlan.isFalse()
//
//            assertThat(recommendedFoodsSelected.value).doesNotContain(selectedFoodRec)
//        }
//    }
//
//    @Test
//    fun `it should navigate back when onBackButtonClicked is called`() {
//        init()
//        viewModel.navigateBack.observeForever(eventObserver)
//
//        viewModel.onBackButtonClicked(mockk())
//
//        verify { eventObserver.onChanged(null) }
//    }
// }
