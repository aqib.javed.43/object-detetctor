package com.vessel.app.flex.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MembershipOptionsResponse(
    val isFlexEnabled: Boolean? = null,
    val membershipOptions: List<MembershipOptionProduct>? = null
)

@JsonClass(generateAdapter = true)
data class MembershipOptionProduct(
    val title: String? = null,
    val subtitle: String? = null,
    val quantityLabel: String? = null,
    val price: Double? = null,
    val currencySymbol: String? = null,
    val frequency: String? = null,
    val buttonTitle: String? = null,
    val pageType: String? = null,
    val purchaseLink: String? = null
) {
    fun getMonthlyPriceText(): String {
        return "$price $currencySymbol"
    }
    fun getQuantityLabelText(): String {
        return "${subtitle?.replace("monthly" ,"")}"
    }
    fun getTitleLabelText(): String {
        return "${title?.replace("membership" ,"")}"
    }
    fun isFlexOption(): Boolean {
        return title?.contains("Flex") == true
    }
}
