package com.vessel.app.wellness.improvepopup

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class ReagentImproveAlertDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    @Assisted savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    private val reagentId = savedStateHandle.get<Int>("reagentId")

    fun getTitle() = if (reagentId == ReagentItem.Ketones.id)
        R.string.ketones_results
    else
        R.string.magnesium_results

    fun onGotItClicked() {
        if (reagentId == ReagentItem.Ketones.id)
            preferencesRepository.showKetonesPopup = false
        else
            preferencesRepository.showNewMagnesiumPopup = false
        dismissDialog.call()
    }
}
