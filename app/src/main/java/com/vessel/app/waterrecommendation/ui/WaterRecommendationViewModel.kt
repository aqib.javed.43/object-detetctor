package com.vessel.app.waterrecommendation.ui

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.common.net.data.plan.BuildPlanRequest
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.waterrecommendation.WaterRecommendationState
import com.vessel.app.waterrecommendation.model.WaterRecommendationSelectModel
import kotlinx.coroutines.launch

class WaterRecommendationViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
    val state: WaterRecommendationState,
    val buildPlanManager: BuildPlanManager,
    val planManager: PlanManager,
    val reagentsManager: ReagentsManager,
    private val recommendationManager: RecommendationManager,
    val foodManager: FoodManager,
    val trackingManager: TrackingManager,
    @Assisted private val savedStateHandle: SavedStateHandle,
    val recommendationsManager: RecommendationManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    WaterRecommendationSelectAdapter.OnActionHandler {

    init {
        val items = ArrayList<WaterRecommendationSelectModel>()
        items.add(WaterRecommendationSelectModel(8, 2, title = "16 oz"))
        items.add(WaterRecommendationSelectModel(8, 4, title = "32 oz"))
        items.add(WaterRecommendationSelectModel(8, 8, title = "64 oz"))
        items.add(WaterRecommendationSelectModel(8, 12, title = "100 oz"))
        state.items.value = items
    }
    override fun onBackButtonClicked(view: View) {
        buildPlanManager.removeReagentRecommendation(ReagentItem.Hydration.id)
        state.navigateTo.value = WaterRecommendationFragmentDirections.actionFoodRecommendationFragmentToReagentPersonalizingFragment(false)
    }

    fun onNextClicked() {
        addHydrationRecommendation()
    }

    override fun onItemSelectClicked(item: WaterRecommendationSelectModel) {
        state {
            items.value =
                items.value.orEmpty().mapIndexed { _, water ->
                    if (item.count == water.count) water.copy(checked = !item.checked)
                    else water.copy(checked = false)
                }.toMutableList()
        }
    }
    private fun addHydrationRecommendation() {
        if (!preferencesRepository.isAddedHydrationRecommendation) {
            preferencesRepository.isAddedHydrationRecommendation = true
            showLoading()
            viewModelScope.launch {
                val recommendationResponse = recommendationManager.getLatestSampleRecommendations()
                hideLoading()
                if (recommendationResponse is Result.Success) {
                    val hydrationPlanId =
                        recommendationResponse.value.lifestyle.firstOrNull { it.lifestyle_recommendation_id == Constants.HYDRATION_PLAN_ID }?.id
                            ?: return@launch

                    val buildPlanRequest = BuildPlanRequest(
                        food_ids = listOf(),
                        reagent_lifestyle_recommendations_ids = listOf(hydrationPlanId),
                        supplements_ids = listOf(),
                        is_auto_build = true
                    )
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_BUILD_SUBMITTED)
                            .addProperty("data", buildPlanRequest.toString())
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    processNextStep()
                }
            }
        } else {
            processNextStep()
        }
    }

    private fun processNextStep() {
        buildPlanManager.removeReagentRecommendation(ReagentItem.Hydration.id)
        state.navigateTo.value = WaterRecommendationFragmentDirections.actionFoodRecommendationFragmentToReagentPersonalizingFragment(false)
    }
}
