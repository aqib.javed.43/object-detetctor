package com.vessel.app.wellness.model

import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.ReagentEntry
import com.vessel.app.common.model.data.Reagent

data class Score(
    val wellness: List<DateEntry>,
    val reagents: Map<Reagent, List<ReagentEntry>>,
    val goals: Map<Goal, List<DateEntry>> = mapOf()
)
