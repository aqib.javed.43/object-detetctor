package com.vessel.app.home

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class HomeState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val isCalibrationVisible = MutableLiveData(false)
    val showNavigationItems = MutableLiveData(true)
    val showFirstReminderHint = LiveEvent<Any>()
    val showUpdateReminderHint = LiveEvent<Any>()
    val setupCoBrowseData = MutableLiveData<MutableMap<String, Any>>()

    operator fun invoke(block: HomeState.() -> Unit) = apply(block)
}
