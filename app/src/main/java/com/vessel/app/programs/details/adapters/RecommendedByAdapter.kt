package com.vessel.app.programs.details.adapters

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Program

class RecommendedByAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<Program>() {

    override fun getLayout(position: Int) = R.layout.item_recommended_by_updated
    override fun getHandler(position: Int): Any = handler
    override fun onBindViewHolder(holder: BaseViewHolder<Program>, position: Int) {
        super.onBindViewHolder(holder, position)
    }

    interface OnActionHandler {
        fun onExpertClicked()
    }
}
