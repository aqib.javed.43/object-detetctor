package com.vessel.app.taketest.retrycapturedialog

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class RetryCaptureDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()

    fun onGotItClicked() {
        dismissDialog.call()
    }
}
