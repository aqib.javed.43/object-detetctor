package com.vessel.app.today.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.today.model.LessonListItem
import kotlinx.android.synthetic.main.all_lesson_completed.view.*
import kotlinx.android.synthetic.main.item_lesson_list.view.*
import kotlinx.android.synthetic.main.lesson_unlock.view.*

class LessonListAdapter(private val handler: LessonAdapter.OnActionHandler) : BaseAdapterWithDiffUtil<LessonListItem>() {
    var isAllDone = false
    override fun getLayout(position: Int) = R.layout.item_lesson_list
    override fun onBindViewHolder(holder: BaseViewHolder<LessonListItem>, position: Int) {
        if (currentList.isNullOrEmpty() || position >= currentList.size)return
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as LessonListItem
        holder.itemView.allLessonsDone.visibility = if (isAllDone) View.VISIBLE else View.GONE
        if (isAllDone) {
            holder.itemView.title.text = item.getGoalTitle()
            holder.itemView.subTitle.text = item.getFinishAllTitle()
            holder.itemView.allLessonsDone.visibility = if (position == 0) View.VISIBLE else View.GONE
            holder.itemView.title.visibility = if (position == 0) View.VISIBLE else View.GONE
            holder.itemView.findViewById<RecyclerView>(R.id.lessonList).visibility = View.GONE
            holder.itemView.wellDoneLayout.visibility = View.GONE
            holder.itemView.unlockMoreLayout.visibility = View.GONE
            holder.itemView.btnAddOtherGoal.setOnClickListener {
                handler.onAddOtherGoal()
            }
        } else {
            holder.itemView.title.text = item.getTitle()
            val lessonAdapter = LessonAdapter(handler)
            holder.itemView.allLessonsDone.visibility = if (currentList.isNullOrEmpty()) View.VISIBLE else View.GONE
            holder.itemView.noLessonDone.visibility = if (item.items.isNullOrEmpty() && item.isPreviousDay == true) View.VISIBLE else View.GONE
            holder.itemView.wellDoneLayout.visibility = if (item.isFinishAll() && item.noMoreLesson && item.isPreviousDay == false) View.VISIBLE else View.GONE
            holder.itemView.unlockMoreLayout.visibility = if (item.isFinishAll() && !item.noMoreLesson && item.isPreviousDay == false) View.VISIBLE else View.GONE
            holder.itemView.findViewById<RecyclerView>(R.id.lessonList).apply {
                adapter = lessonAdapter
                itemAnimator = null
                setHasFixedSize(true)
            }
            lessonAdapter.onLessonUpdate = {
                holder.itemView.wellDoneLayout.visibility = if (item.isFinishAll() && item.noMoreLesson && item.isPreviousDay == false) View.VISIBLE else View.GONE
                holder.itemView.unlockMoreLayout.visibility = if (item.isFinishAll() && !item.noMoreLesson && item.isPreviousDay == false) View.VISIBLE else View.GONE
                isAllDone = true
                for (lessonList in currentList) {
                    if (!lessonList.isFinishAll()) isAllDone = false
                }
                if (isAllDone) notifyDataSetChanged()
            }
            holder.itemView.unlockButton.setOnClickListener {
                item.items?.lastOrNull()
                    ?.let { lesson -> handler.onUnlockLesson(lesson, item.items?.firstOrNull()?.programId, item.date) }
            }
            lessonAdapter.submitList(item.items)
            holder.itemView.title.setOnClickListener {
                handler.onLessonInformationClicked()
            }
        }
    }
}
