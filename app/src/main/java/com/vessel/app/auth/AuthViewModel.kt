package com.vessel.app.auth

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.ResourceRepository

class AuthViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    private val preferencesRepository: PreferencesRepository,
) : BaseViewModel(resourceRepository) {
    fun onReceiveNotification() {
        preferencesRepository.pushWooshAttributeDeepLink = "/plan"
    }
}
