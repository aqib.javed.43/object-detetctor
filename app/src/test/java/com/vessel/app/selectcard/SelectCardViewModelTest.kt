package com.vessel.app.selectcard

import com.vessel.app.BaseTest
import com.vessel.app.MockProvider
import com.vessel.app.selectcard.ui.SelectCardFragmentDirections
import com.vessel.app.util.ResourceRepository
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class SelectCardViewModelTest : BaseTest() {
    private val resourceRepository = mockk<ResourceRepository>()
    private lateinit var state: SelectCardState
    private lateinit var viewModel: SelectCardViewModel

    @BeforeEach
    fun setUp() {
        state = SelectCardState()
        viewModel = SelectCardViewModel(state, resourceRepository)
    }

    @Test
    fun `continueClicked navigates to education screen`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.continueClicked(view)

        verify { navController.navigate(SelectCardFragmentDirections.actionSelectCardToEducation()) }
    }
}
