package com.vessel.app.util.remoteconfig

import android.annotation.SuppressLint
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vessel.app.Constants
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import javax.inject.Inject

class FirebaseRemoteConfiguration @Inject constructor(
    private val resourceRepository: ResourceRepository,
    private val firebaseRemoteConfig: FirebaseRemoteConfig
) : RemoteConfiguration {

    override fun init(minimumFetchIntervalSeconds: Long) {
        setConfigSettings(minimumFetchIntervalSeconds)
        readDefaults()
        fetchConfigFromRemote()
    }

    private fun setConfigSettings(minimumFetchIntervalSeconds: Long) {
        firebaseRemoteConfig.setConfigSettingsAsync(
            FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(minimumFetchIntervalSeconds)
                .build()
        )
    }

    override fun fetchConfigFromRemote() {
        firebaseRemoteConfig.activate()
        firebaseRemoteConfig.fetchAndActivate()
    }

    override fun getString(key: String) = firebaseRemoteConfig.getString(key)

    override fun getBoolean(key: String) = firebaseRemoteConfig.getBoolean(key)

    override fun getDouble(key: String) = firebaseRemoteConfig.getDouble(key)

    override fun getLong(key: String) = firebaseRemoteConfig.getLong(key)

    private fun setDefaultsAsync(defaultsMap: Map<String, Any>) {
        firebaseRemoteConfig.setDefaultsAsync(defaultsMap)
        Constants.HYDRATION_PLAN_ID = (defaultsMap.get(Constants.LIFESTYLE_HYDRATION_ID) as String).toInt()
        Constants.TEST_PLAN_ID = (defaultsMap.get(Constants.LIFESTYLE_TEST_ID) as String).toInt()
        Constants.TRIAL_FREE_DAYS = (defaultsMap.get(Constants.FREE_TRIAL_PERIOD) as String).toInt()
    }

    @SuppressLint("CheckResult")
    private fun readDefaults() {
        CoroutineScope(SupervisorJob()).launch {
            val fileContent = resourceRepository.readFile(REMOTE_CONFIG_FILE_NAME)
            val type = object : TypeToken<Map<String, String>>() {}.type
            val jsonContent: Map<String, String> = Gson().fromJson(fileContent, type)
            setDefaultsAsync(jsonContent)
        }
    }

    companion object {
        private const val REMOTE_CONFIG_FILE_NAME = "configuration_defaults.json"
    }
}
