package com.vessel.app.common.adapter.impactgoals

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class ImpactGoalsAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<ImpactGoalModel>(
        areItemsTheSame = { oldItem, newItem -> oldItem.title == newItem.title }
    ) {
    override fun getLayout(position: Int) = R.layout.item_impact_goal

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onGoalItemClick(item: ImpactGoalModel, view: View)
    }
}
