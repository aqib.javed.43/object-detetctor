package com.vessel.app.planpersonalizing

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class PlanPersonalizingViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
) : BaseViewModel(resourceProvider) {
    val navigateToHome = LiveEvent<Any>()
    val navigateToPlanRecommendation = LiveEvent<Any>()

    fun onNotNowClicked() {
        navigateToHome.call()
    }
    fun onNextClicked() {
        navigateToPlanRecommendation.call()
    }
}
