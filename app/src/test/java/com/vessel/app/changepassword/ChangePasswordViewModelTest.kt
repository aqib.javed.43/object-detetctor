package com.vessel.app.changepassword

import android.view.View
import androidx.lifecycle.Observer
import com.vessel.app.*
import com.vessel.app.BaseFaker.Fake.FAKER
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Result
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.model.AlertData
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.validators.PasswordType
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ChangePasswordViewModelTest : BaseTest() {
    private val resourceRepository = mockk<ResourceRepository>()
    private val contactManager = mockk<ContactManager>()
    private val trackingManager = mockk<TrackingManager>()
    private val preferencesRepository = mockk<PreferencesRepository>()
    private val eventObserver = mockk<Observer<Unit>>(relaxed = true)

    private lateinit var state: ChangePasswordState

    private lateinit var viewModel: ChangePasswordViewModel

    @BeforeEach
    fun setUp() {
        state = ChangePasswordState()

        viewModel = ChangePasswordViewModel(state, resourceRepository, contactManager, preferencesRepository, trackingManager)
    }

    @Test
    fun `it should navigate to the previous fragment when successfully updating the password`() =
        runBlockingTest {
            val (view, navController) = MockProvider.mockedNavigation()
            val (currentPassword, newPassword) = setValidPasswordState()
            coEvery { contactManager.changePassword(currentPassword, newPassword) }
                .answers { Result.Success(Unit) }
            state.showSuccessToast.observeForever(eventObserver)

            viewModel.onUpdatePasswordClick(view)

            viewModel.isLoading.value.isTrue()
            verify {
                eventObserver.onChanged(null)
                navController.popBackStack()
            }
        }

    @Test
    fun `it should show an error when updating password if the network call fails`() =
        runBlockingTest {
            val view = mockk<View>()
            val (currentPassword, newPassword) = setValidPasswordState()
            val errorTitle = FAKER.chuckNorris().fact()
            val errorMessage = PasswordType.Current.error
            every { resourceRepository.getString(R.string.error) } returns errorTitle
            every { resourceRepository.getString(R.string.network_error) } returns errorMessage
            coEvery { contactManager.changePassword(currentPassword, newPassword) }
                .answers { Result.NetworkIOError(Exception()) }
            val loadingValues = mutableListOf<Boolean>()
            val observer: Observer<Boolean> = Observer { loadingValues.add(it) }
            viewModel.isLoading.observeForever(observer)

            viewModel.onUpdatePasswordClick(view)

            loadingValues[0].isTrue()
            loadingValues[1].isFalse()
            loadingValues.size isEqualTo 2
            viewModel.showAlert.value isEqualTo AlertData(errorTitle, errorMessage)
            viewModel.isLoading.removeObserver(observer)
        }

    @Test
    fun `it should show an error when updating password if passwords are invalid`() {
        val view = mockk<View>()
        val errorTitle = FAKER.chuckNorris().fact()
        val errorMessage = PasswordType.Current.error
        every { resourceRepository.getString(R.string.error) } returns errorTitle

        viewModel.onUpdatePasswordClick(view)

        viewModel.showAlert.value isEqualTo AlertData(errorTitle, errorMessage)
        verify { contactManager wasNot Called }
    }

    @Test
    fun `it should navigate to the previous fragment when clicking the back button`() {
        val (view, navController) = MockProvider.mockedNavigation()

        viewModel.onBackButtonClicked(view)

        verify { navController.popBackStack() }
    }

    private fun setValidPasswordState(): Pair<String, String> {
        val currentPassword = validPassword()
        val newPassword = validPassword()
        state.currentPassword.value = currentPassword
        state.newPassword.value = newPassword
        state.confirmNewPassword.value = newPassword

        return currentPassword to newPassword
    }
}
