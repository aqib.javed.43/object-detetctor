The detection of nitrites in a dipstick test can be used as a tool to diagnose UTIs. Nitrites are likely one of the most specific and sensitive markers when it comes to UTI testing.
https://pubmed.ncbi.nlm.nih.gov/29305250/ 

The dipstick test (leukocyte esterase) can be used as a diagnostic tool in detecting UTI cases.
https://pubmed.ncbi.nlm.nih.gov/24319857/

The urine dipstick test can be useful in all populations to exclude the presence of infection if the results of both nitrites and leukocyte-esterase are negative.
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC434513/

Proactive urinary testing of leukocytes showed 90% accuracy in predicting asymptomatic urinary tract infection in pregnant women. 
https://pubmed.ncbi.nlm.nih.gov/18850421/
