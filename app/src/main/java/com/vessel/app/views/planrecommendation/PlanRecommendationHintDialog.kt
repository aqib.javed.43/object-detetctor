package com.vessel.app.views.planrecommendation

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import com.vessel.app.databinding.PlanRecommendationHintDialogBinding

class PlanRecommendationHintDialog(
    private val activity: FragmentActivity,
    private val actionCallback: (() -> Unit)?
) {

    private val dialog: AlertDialog = AlertDialog.Builder(activity).create()
    private lateinit var binding: PlanRecommendationHintDialogBinding

    init {
        setupDialogView()
    }

    private fun setupDialogView() {
        val layoutInflater = LayoutInflater.from(activity)
        binding = PlanRecommendationHintDialogBinding.inflate(layoutInflater, null, false)
        dialog.setView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.BOTTOM)
        dialog.setCancelable(true)

        setupActionClick()
    }

    private fun setupActionClick() {
        binding.description.setOnClickListener {
            actionCallback?.invoke()
            hideDialog()
        }
    }

    fun showDialog() {
        dialog.show()
    }

    private fun hideDialog() {
        dialog.dismiss()
    }

    companion object {
        fun showRecommendationDialog(
            activity: FragmentActivity,
            actionCallback: (() -> Unit)?
        ) {
            PlanRecommendationHintDialog(
                activity,
                actionCallback
            ).showDialog()
        }
    }
}
