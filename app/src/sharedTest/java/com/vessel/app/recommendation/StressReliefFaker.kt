// package com.vessel.app.recommendation
//
// import com.vessel.app.BaseFaker
// import com.vessel.app.BaseFaker.Fake.FAKER
// import com.vessel.app.stressrelief.model.StressReliefItem
// import com.vessel.app.stressrelief.model.StressReliefViewItem
//
// object StressReliefFaker {
//    fun viewItem(
//        item: StressReliefItem = StressReliefItem.MEDITATE,
//        isInPlan: Boolean = FAKER.bool().bool(),
//    ) = StressReliefViewItem(
//        title = "",
//        isInPlan = isInPlan,
//        stressReliefName = ""
//    )
//
//    fun viewItemList() = BaseFaker.list { viewItem() }
// }
