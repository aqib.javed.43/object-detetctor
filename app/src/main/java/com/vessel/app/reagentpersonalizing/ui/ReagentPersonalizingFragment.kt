package com.vessel.app.reagentpersonalizing.ui

import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.home.HomeActivity
import com.vessel.app.reagentpersonalizing.ReagentPersonalizingViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_reagent_personalizing.*

@AndroidEntryPoint
class ReagentPersonalizingFragment : BaseFragment<ReagentPersonalizingViewModel>() {
    override val viewModel: ReagentPersonalizingViewModel by viewModels()
    override val layoutResId = R.layout.fragment_reagent_personalizing

    override fun onViewLoad(savedInstanceState: Bundle?) {
        super.onViewLoad(savedInstanceState)
        description.movementMethod = ScrollingMovementMethod()
        viewModel.state.navigate.observe(
            viewLifecycleOwner
        ) {
            navigateToHomeActivity()
        }
        viewModel.state.navigateTo.observe(
            viewLifecycleOwner
        ) {
            findNavController().navigate(it)
        }
    }

    private fun navigateToHomeActivity() {
        val intent = Intent(requireContext(), HomeActivity::class.java)
        context?.startActivity(intent)
        activity?.finishAffinity()
    }
}
