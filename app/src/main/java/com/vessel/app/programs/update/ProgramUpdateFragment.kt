package com.vessel.app.programs.update

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProgramUpdateFragment : BaseFragment<ProgramUpdateViewModel>() {
    override val viewModel: ProgramUpdateViewModel by viewModels()
    override val layoutResId = R.layout.fragment_update_program

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    private fun setupObservers() {

        observe(viewModel.state.navigateTo) {
            findNavController().navigate(it)
        }
    }
}
