package com.vessel.app.views.retrytabstips

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.vessel.app.R

data class TestFailureTipViewUiModel(
    @StringRes val title: Int,
    @StringRes val subTitle: Int,
    @DrawableRes val image: Int
)

fun getTipsList() =
    listOf<TestFailureTipViewUiModel>(
        TestFailureTipViewUiModel(R.string.droplets, R.string.droplets_subtitle, R.drawable.droplets_image),
        TestFailureTipViewUiModel(R.string.lighting, R.string.lighting_subtitle, R.drawable.lighting_image),
        TestFailureTipViewUiModel(R.string.glare, R.string.glare_subtitle, R.drawable.glare_image)
    )
