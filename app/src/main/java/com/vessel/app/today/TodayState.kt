package com.vessel.app.today

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.common.model.Program
import com.vessel.app.common.model.TrailLockItem
import com.vessel.app.plan.model.*
import com.vessel.app.today.model.ActivityListItem
import com.vessel.app.today.model.EssentialListItem
import com.vessel.app.today.model.LessonListItem
import com.vessel.app.util.LiveEvent
import java.util.*
import javax.inject.Inject

class TodayState @Inject constructor() {
    val todayActivitiesItems = LiveEvent<List<ActivityListItem>>()
    val essentialItems = MutableLiveData<List<EssentialListItem>>()
    val planItems = MutableLiveData<Pair<List<DayPlanItem>, Boolean>>()
    val dateList = MutableLiveData<List<DayPlanProgressItem>>()
    val dayPlanItems = MutableLiveData<List<DayPlanItem>>()
    val bonusItems = MutableLiveData<List<DayPlanItem>>()
    val deletePlanItem = LiveEvent<Int>()
    val addPlanChatFooterItem = MutableLiveData<PlanChatFooter>()
    val showPlanNotification = LiveEvent<Boolean>()
    val readyToPopulate = LiveEvent<Unit>()
    val planNotificationCount = MutableLiveData<Int>()
    val openMessaging = LiveEvent<Unit>()
    val openEssentialLink = LiveEvent<String>()
    var isDailyView: Boolean = true
    var todayTodosData: List<DayPlanItem> = listOf()
    var weeklyTodosData: List<DayPlanItem> = listOf()
    val showProgramLoading = MutableLiveData(false)
    val navigateTo = LiveEvent<NavDirections>()
    val removeReminder = LiveEvent<Triple<PlanItem, Int, Int>>()
    val programsItems = MutableLiveData<List<Program>>()
    val isTakeFirstTest = MutableLiveData<Boolean>()
    val lessonItems = LiveEvent<List<LessonListItem>>()
    val keepDateState = LiveEvent<Boolean>()
    val currentDate = LiveEvent<Date>()
    val isTrialLocked = MutableLiveData<Boolean>()
    val trialLockItem = MutableLiveData<TrailLockItem>()
    operator fun invoke(block: TodayState.() -> Unit) = apply(block)
}
