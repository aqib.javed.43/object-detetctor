package com.vessel.app.testscoreinfo

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.PostTestNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import com.vessel.app.wellness.RecommendationType
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TestScoreInfoFragment : BaseFragment<TestScoreInfoViewModel>() {
    override val viewModel: TestScoreInfoViewModel by viewModels()
    override val layoutResId = R.layout.fragment_test_score_info

    override fun onViewLoad(savedInstanceState: Bundle?) {
        observe(viewModel.state.continueClicked) {
            findNavController().navigate(PostTestNavGraphDirections.actionGoToHome())
        }
        observe(viewModel.state.navigateToPlan) {
            findNavController().navigate(TestScoreInfoFragmentDirections.actionTestScoreInfoFragmentToCreatePlanRecommendationFragment(RecommendationType.Tip))
        }
        observe(viewModel.state.navigateToUpdatePlan) {
            findNavController().navigate(TestScoreInfoFragmentDirections.actionTestScoreInfoFragmentToPlanNeedUpdatesFragment())
        }
    }
}
