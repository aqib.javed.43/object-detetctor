When we consume biotin, it is absorbed in the middle part of our intestines called the jejunum. Biotin is also produced by microbes in our large intestine. While we lose some biotin in feces, the majority of excess biotin is excreted in the urine. Urine levels rise as dietary intake rises, and fall as dietary intake of biotin falls.

When you’re in the good zone (darker purple), your body is likely getting enough biotin to support your wellness.

Too high means you may be getting more than you need. Although there isn’t much evidence of danger from high levels of biotin, you may be wasting money if you’re taking a supplement.

Too low means you likely are not getting enough biotin, and you might want to up your intake. Check out the recommendations in the app for how to do that.

**References:**

-   Book: Mock DM. Biotin. In: Ross AC, Caballero B, Cousins RJ, Tucker KL, Ziegler TR, eds. Modern Nutrition in Health and Disease
-   24hour urine biotin range in healthy adults is 18-127 nmol/24h urine

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4757853/
-   Healthy adults excrete ~100 nmol of biotin and catabolites per day into urine. If physiological or pharmacological doses of biotin are administered parenterally to humans, rats, or pigs, 43–75% of the dose is excreted into urine.

-   https://pubmed.ncbi.nlm.nih.gov/10075337/

-   https://pubmed.ncbi.nlm.nih.gov/9022537/

-   https://pubmed.ncbi.nlm.nih.gov/5081681/

-   https://pubmed.ncbi.nlm.nih.gov/18635909/
-   All urinary vitamins and their metabolite levels except vitamin B(12) increased linearly in a dose-dependent manner, and highly correlated with vitamin intake (r=0.959 for vitamin B(1), r=0.927 for vitamin B(2), r=0.965 for vitamin B(6), r=0.957 for niacin, r=0.934 for pantothenic acid, r=0.907 for folic acid, r=0.962 for biotin, and r=0.952 for vitamin C). These results suggest that measuring urinary water-soluble vitamins and their metabolite levels can be used as good nutritional markers for assessing vitamin intakes.

-   https://pubmed.ncbi.nlm.nih.gov/20417877/
-   “We conclude that urinary levels of water-soluble vitamins, except for B12, reflected their recent intake in free-living Japanese elderly females and could be used as a measure of their intake during the previous few days.”

-   https://pubmed.ncbi.nlm.nih.gov/16392702/
-   A study to determine average values of urine B vitamins and vitamin c in college students eating a monitored diet.

-   https://pubmed.ncbi.nlm.nih.gov/20086315/
-   A study of Japanese college students given a strict diet for 7 days, and measuring differences in urinary vitamin C and B vitamin levels.

-   https://pubmed.ncbi.nlm.nih.gov/23883688/
-   Urinary excretion levels of water-soluble vitamins in pregnant and lactating women in Japan- “No change was observed in the urinary excretion levels of vitamin B2, vitamin B6, vitamin B12, biotin or vitamin C among stages.”

-   https://www.ncbi.nlm.nih.gov/pubmed/23302490

-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1435357/

-   https://www.ncbi.nlm.nih.gov/pubmed/142069

-   https://www.ncbi.nlm.nih.gov/pubmed/9094878

-   https://www.ncbi.nlm.nih.gov/pubmed/9176832

-   https://www.ncbi.nlm.nih.gov/pubmed/9164991

-   https://www.ncbi.nlm.nih.gov/pubmed/8229299

-   https://www.ncbi.nlm.nih.gov/books/NBK114297/

