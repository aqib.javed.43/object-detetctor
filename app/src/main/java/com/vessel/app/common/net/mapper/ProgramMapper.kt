package com.vessel.app.common.net.mapper

import com.vessel.app.Constants
import com.vessel.app.common.model.*
import com.vessel.app.common.net.data.*
import com.vessel.app.wellness.net.data.DifficultyLevel
import com.vessel.app.wellness.net.data.LikeStatus
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProgramMapper @Inject constructor(
    private val contactMapper: ContactMapper,
    private val scienceSourceMapper: ScienceSourceMapper,
) {

    fun map(program: ProgramResponseData) =
        ProgramModel(
            program = program.program.filterNotNull().map { mapProgram(it) },
            pagination = program.pagination
        )

    fun mapProgram(program: ProgramData) = Program(
        id = program.id ?: 0,
        title = program.title.orEmpty(),
        likeStatus = LikeStatus.from(program.like_status),
        totalLikes = program.total_likes ?: 0,
        totalDislikes = program.dislikes_count ?: 0,
        contact = program.contact?.let { contactMapper.map(it) },
        contactId = program.contact_id ?: 0,
        createdAt = program.created_at.orEmpty(),
        description = program.description.orEmpty(),
        difficulty = DifficultyLevel.from(program.difficulty.orEmpty()),
        durationDays = program.duration_days ?: 0,
        imageUrl = program.image_url.orEmpty(),
        isEnrolled = program.is_enrolled ?: false,
        mainGoalId = program.main_goal_id ?: 0,
        reviewedContactIds = program.reviewed_contact_ids,
        goals = program.impactsGoals,
        sources = program.sources?.map { scienceSourceMapper.map(it) },
        planData = program.plans?.toMutableList(),
        enrolledDate = program.enrolled_date?.let { Constants.DAY_DATE_FORMAT.parse(it) },
        frequency = program.frequency ?: 0,
        schedule = program.schedule?.map {
            contactMapper.mapScheduleProgram(
                it,
                program.plans?.toList()
            )
        },
        essentials = program.getEssentialList()
    )
}
