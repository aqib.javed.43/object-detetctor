package com.vessel.app.wellness.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GoalData(
    val id: Int,
    val name: String,
    val image_small_url: String? = null,
    val image_medium_url: String? = null,
    val image_large_url: String? = null,
    val tip_count: Int = 0,

    val reagents: List<ReagentsData?>?
)
