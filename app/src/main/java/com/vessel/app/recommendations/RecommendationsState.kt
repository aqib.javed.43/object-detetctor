package com.vessel.app.recommendations

import androidx.lifecycle.MutableLiveData
import com.vessel.app.recommendations.ui.RecommendationsPage
import javax.inject.Inject

class RecommendationsState @Inject constructor() {
    val pages = MutableLiveData<MutableList<RecommendationsPage>>()
    val position = MutableLiveData(0)
    operator fun invoke(block: RecommendationsState.() -> Unit) = apply(block)
}
