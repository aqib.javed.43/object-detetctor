package com.vessel.app.wellness.net.data

enum class DifficultyLevel(val apiValue: String, val uiValue: String) {
    HARD("HARD", "Hard"),
    MEDIUM("MED", "Medium"),
    EASY("EASY", "Easy");

    companion object {
        fun from(state: String?) =
            DifficultyLevel.values().firstOrNull { it.apiValue == state } ?: EASY
    }
}
