package com.vessel.app.appreview.resulttip

import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.NavDirections
import com.vessel.app.appreview.model.ReviewState
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.MembershipManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class ResultTipDialogViewModel @ViewModelInject constructor(
    var state: ReviewState,
    resourceProvider: ResourceRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
    val membershipManager: MembershipManager
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    var navigateTo = LiveEvent<NavDirections>()
    init {
        state.videoUrl.value = videoUrl
        state.isPlayingVideo.value = false
    }
    fun dismiss() {
        devicePreferencesRepository.showAppReviewPopup = false
        dismissDialog.call()
    }
    fun onChatClicked() {
        state.openMessaging.call()
    }
    fun onPlayVideoClicked() {
        state.isPlayingVideo.value = true
    }
    fun onTipsClicked() {
        state.openTips.value = tipsUrl
    }
    fun onCallClicked() {
        state.onCalling.value = tel
    }
    fun getChatConfiguration() =
        membershipManager.getChatConfiguration(MembershipManager.ZENDESK_NUTRITIONIST_DEPARTMENT)
    companion object {
        const val tipsUrl = "https://help.vesselhealth.com/hc/en-us/articles/1500008848121-Tips-for-Scanning-Your-Wellness-Card"
        const val videoUrl = "https://d32fui7xaadmx6.cloudfront.net/wellness/clip_2.mp4"
        const val tel = "866–233–9726"
    }
}
