package com.vessel.app.wellness.net.mapper

import com.vessel.app.Constants
import com.vessel.app.common.manager.ReagentsManager
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.ReagentEntry
import com.vessel.app.common.model.data.Reagent
import com.vessel.app.common.model.data.ReagentState
import com.vessel.app.wellness.model.Score
import com.vessel.app.wellness.net.data.ReagentData
import com.vessel.app.wellness.net.data.ScoreData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ScoreMapper @Inject constructor(private val reagentsManager: ReagentsManager) {

    fun map(data: List<ScoreData>) = data
        .sortedBy { it.insertDate }
        .let { scoreData ->
            val wellnessEntries = mutableListOf<DateEntry>()
            val reagentMap = mutableMapOf<Reagent, MutableList<ReagentEntry>>()
            val indexOffset = 1f

            scoreData.forEachIndexed { index, score ->
                mapScoreData(index + indexOffset, score, wellnessEntries, reagentMap)
            }

            Score(
                wellness = wellnessEntries.toList(),
                reagents = reagentMap.mapValues { it.value.toList() }.toMap()
            )
        }
    fun mapLatest(data: List<ScoreData>) = data
        .sortedByDescending { it.insertDate }
        .let { scoreData ->
            val wellnessEntries = mutableListOf<DateEntry>()
            val reagentMap = mutableMapOf<Reagent, MutableList<ReagentEntry>>()
            val indexOffset = 1f

            scoreData.forEachIndexed { index, score ->
                if (index == 0) mapScoreData(index + indexOffset, score, wellnessEntries, reagentMap)
            }

            Score(
                wellness = wellnessEntries.toList(),
                reagents = reagentMap.mapValues { it.value.toList() }.toMap()
            )
        }
    fun mapAverageScore(data: Map<Int, List<ScoreData>>) = data
        .let { scoreData ->
            val wellnessEntries = mutableListOf<DateEntry>()
            val reagentMap = mutableMapOf<Reagent, MutableList<ReagentEntry>>()
            val indexOffset = 1f

            scoreData.forEach { (indexKey, scores) ->
                scores.firstOrNull()?.let {
                    mapScoreData(
                        indexKey + indexOffset,
                        it, wellnessEntries, reagentMap
                    )
                }
                if (scores.size > 1) {
                    mapScoreData(indexKey + indexOffset + 0.5f, scores.last(), wellnessEntries, reagentMap)
                }
            }

            Score(
                wellness = wellnessEntries.toList(),
                reagents = reagentMap.mapValues { it.value.toList() }.toMap()
            )
        }

    private fun mapScoreData(index: Float, score: ScoreData, wellnessEntries: MutableList<DateEntry>, reagentMap: MutableMap<Reagent, MutableList<ReagentEntry>>) {
        wellnessEntries.add(
            DateEntry(
                index,
                score.scores.wellness_score * 100,
                score.insertDate.time,
                uuid = score.sample_uuid
            )
        )
        reagentsManager.getReagent().forEach { reagent ->
            var reagentData: ReagentData? = null
            if (reagent.state != ReagentState.COMING_SOON)
                reagentData = score.scores.reagents.orEmpty().firstOrNull { reagent.id == it.reagent_id }

            reagentMap.getOrPut(reagent, { mutableListOf() })
                .apply {
                    add(
                        ReagentEntry(
                            index,
                            if (reagentData?.score != null && reagentData.score!! >= 0f) reagentData.value else Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE,
                            if (reagentData?.score != null && reagentData.score!! >= 0f) reagentData.score?.times(100) else Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE,
                            score.insertDate.time
                        )
                    )
                }
        }
    }
}
