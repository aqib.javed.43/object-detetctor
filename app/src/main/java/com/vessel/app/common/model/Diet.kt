package com.vessel.app.common.model

import android.os.Parcelable
import androidx.annotation.StringRes
import com.vessel.app.R
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class Diet(@StringRes val title: Int, val apiName: String, val isNone: Boolean) : Parcelable {

    Keto(R.string.keto, "keto", false),
    LowCalorie(R.string.low_calorie, "lowCalorie", false),
    Vegan(R.string.vegan, "vegan", false),
    LowFat(R.string.low_fate, "lowFat", false),
    Paleo(R.string.paleo, "paleo", false),
    LowSugar(R.string.low_sugar, "lowSugar", false),
    Vegetarian(R.string.vegetarian, "vegetarian", false),
    LowCarb(R.string.low_carb, "lowCarb", false),
    None(R.string.none_of_the_above, "none", true);

    companion object {
        fun fromApiName(apiName: String) = values().firstOrNull { it.apiName == apiName } ?: None
    }
}
