package com.vessel.app.planrecommendation.ready

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.PlanManager
import com.vessel.app.common.manager.RecommendationManager
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class PlanRecommendationReadyViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    planManager: PlanManager,
    recommendationManager: RecommendationManager,
) : BaseViewModel(resourceProvider) {
    val navigateToHome = LiveEvent<Any>()

    init {
        planManager.clearCachedData()
        recommendationManager.updateLatestSampleFoodRecommendationsCachedData()
        recommendationManager.updateLatestSampleRecommendationsCachedData()
    }
    fun onGotItClicked() {
        navigateToHome.call()
    }
}
