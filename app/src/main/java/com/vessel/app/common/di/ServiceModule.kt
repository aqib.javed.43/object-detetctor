package com.vessel.app.common.di

import com.vessel.app.BuildConfig
import com.vessel.app.common.net.RestClient
import com.vessel.app.common.net.service.*
import com.vessel.app.covid19.net.service.CovidTestService
import com.vessel.app.wellness.net.service.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ServiceModule {
    @Singleton
    @Provides
    fun authService(restClient: RestClient): AuthService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, false).create(AuthService::class.java)

    @Singleton
    @Provides
    fun contactService(restClient: RestClient): ContactService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true)
            .create(ContactService::class.java)

    @Singleton
    @Provides
    fun covidTestService(restClient: RestClient): CovidTestService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true)
            .create(CovidTestService::class.java)

    @Singleton
    @Provides
    fun scoreService(restClient: RestClient): ScoreService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(ScoreService::class.java)

    @Singleton
    @Provides
    fun scoreV3Service(restClient: RestClient): ScoreV3Service =
        restClient.createRetrofitAdapter(BuildConfig.API_V3, true)
            .create(ScoreV3Service::class.java)

    @Singleton
    @Provides
    fun recommendationService(restClient: RestClient): RecommendationService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true)
            .create(RecommendationService::class.java)

    @Singleton
    @Provides
    fun todosService(restClient: RestClient): TodosService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(TodosService::class.java)

    @Singleton
    @Provides
    fun planService(restClient: RestClient): PlanService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(PlanService::class.java)

    @Singleton
    @Provides
    fun sampleService(restClient: RestClient): SampleService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(SampleService::class.java)

    @Singleton
    @Provides
    fun uploadSampleService(restClient: RestClient): UploadSampleService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true)
            .create(UploadSampleService::class.java)

    @Singleton
    @Provides
    fun remindersService(restClient: RestClient): RemindersService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true)
            .create(RemindersService::class.java)

    @Singleton
    @Provides
    fun klavyioService(restClient: RestClient): TrackingService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, false)
            .create(TrackingService::class.java)

    @Singleton
    @Provides
    fun foodService(restClient: RestClient): FoodService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(FoodService::class.java)

    @Singleton
    @Provides
    fun supplementService(restClient: RestClient): SupplementsService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true)
            .create(SupplementsService::class.java)

    @Singleton
    @Provides
    fun surveyService(restClient: RestClient): SurveyService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(SurveyService::class.java)

    @Singleton
    @Provides
    fun reagentService(restClient: RestClient): ReagentService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true)
            .create(ReagentService::class.java)

    @Singleton
    @Provides
    fun assessmentService(restClient: RestClient): AssessmentService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true)
            .create(AssessmentService::class.java)

    @Singleton
    @Provides
    fun goalService(restClient: RestClient): GoalService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(GoalService::class.java)

    @Singleton
    @Provides
    fun subGoalService(restClient: RestClient): SubGoalService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(SubGoalService::class.java)

    @Singleton
    @Provides
    fun subBadHabit(restClient: RestClient): BadHabitService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(BadHabitService::class.java)

    @Singleton
    @Provides
    fun onBoardingProgram(restClient: RestClient): OnBoardingProgramService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(OnBoardingProgramService::class.java)

    @Singleton
    @Provides
    fun tipService(restClient: RestClient): TipService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(TipService::class.java)

    @Singleton
    @Provides
    fun programService(restClient: RestClient): ProgramService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true)
            .create(ProgramService::class.java)
    @Singleton
    @Provides
    fun teamsService(restClient: RestClient): TeamsService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(TeamsService::class.java)
    @Singleton
    @Provides
    fun lessonService(restClient: RestClient): LessonService =
        restClient.createRetrofitAdapter(BuildConfig.API_V2, true).create(LessonService::class.java)
}
