package com.vessel.app.reagent.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class InfoSource(
    val source: String,
    val sourceTitle: String,
    val sourceAuthor: String?
) : Parcelable
