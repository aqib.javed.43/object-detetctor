package com.vessel.app.programs.successjoin

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.homescreen.MainPagerFragmentTab
import com.vessel.app.programs.complete.steptwo.CompleteProgramStep2FragmentDirections
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class SuccessfullyJoinDialogViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()
    private val isCompleteFlow = savedStateHandle.get<Boolean>("isCompleteFlow")

    fun onCloseClicked() {
        if (isCompleteFlow == true)
            navigateTo.value = CompleteProgramStep2FragmentDirections.globalActionToWellness(
                MainPagerFragmentTab.PLAN
            )
        else dismissDialog.call()
    }
}
