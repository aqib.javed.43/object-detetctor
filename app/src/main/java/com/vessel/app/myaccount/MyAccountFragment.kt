package com.vessel.app.myaccount

import androidx.fragment.app.viewModels
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyAccountFragment : BaseFragment<MyAccountViewModel>() {
    override val viewModel: MyAccountViewModel by viewModels()
    override val layoutResId = R.layout.fragment_my_account
}
