package com.vessel.app.teampage

import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.binding.setUnderlineSpan
import com.vessel.app.teampage.model.CommentItem
import com.vessel.app.teampage.model.PostType
import com.vessel.app.teampage.model.TeamPostItem
import com.vessel.app.views.ItemPollView
import kotlinx.android.synthetic.main.item_post_footer.view.*
import kotlinx.android.synthetic.main.item_post_header.view.*
import kotlinx.android.synthetic.main.item_team_post.view.*
import kotlinx.android.synthetic.main.item_team_post.view.imgInteraction
import kotlinx.android.synthetic.main.post_action_view.view.*

class TeamPostAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<TeamPostItem>(), PostCommentAdapter.OnActionHandler {
    override fun getLayout(position: Int) = R.layout.item_team_post
    private val commentAdapter: PostCommentAdapter by lazy { PostCommentAdapter(this) }
    private val commentsMock = listOf<CommentItem>(CommentItem("Leah Cromwell", "Do you feel better?"), CommentItem("Sarah Russel", "So Great!"))
    override fun getHandler(position: Int): Any = handler

    interface OnActionHandler {
        fun onCommentClicked(item: Any, view: View, parentPosition: Int)
        fun onItemSelected(item: Any, view: View, position: Int, parentPosition: Int)
        fun onReportClicked(item: Any, view: View, parentPosition: Int)
        fun onLikeClick(item: Any, view: View, parentPosition: Int)
        fun onFollowClick(item: Any, view: View, parentPosition: Int)
        fun onUnFollowClick(item: Any, view: View, parentPosition: Int)
        fun onResponsePoll(item: Any, view: View, response: Int)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<TeamPostItem>, position: Int) {
        val item = getItem(position)
        holder.itemView.lblName.text = item.actor
        holder.itemView.lblText.text = item.title
        holder.itemView.lblTime.text = item.time
        holder.itemView.imgPost.visibility = if (item.image.isNotEmpty()) View.VISIBLE else View.GONE
        holder.itemView.pollLayout.visibility = if (item.type == PostType.POLL) View.VISIBLE else View.GONE
        holder.itemView.rcvComments.visibility = if (item.type == PostType.POLL) View.GONE else View.VISIBLE

        if (item.type == PostType.POLL) {
            holder.itemView.pollLayout.removeAllViews()
            item.pollQuestions?.question?.let {
                holder.itemView.postContent.text = it
            }
            item.pollQuestions?.response1.let {
                addPollOptions(holder, it, 1, item, handler)
            }
            item.pollQuestions?.response2.let {
                addPollOptions(holder, it, 2, item, handler)
            }
            item.pollQuestions?.response3.let {
                addPollOptions(holder, it, 3, item, handler)
            }
            item.pollQuestions?.response4.let {
                addPollOptions(holder, it, 4, item, handler)
            }
        } else {
            holder.itemView.postContent.text = item.custompr
        }
        holder.itemView.imgInteraction.isSelected = item.isLiked
        val hfText = if (item.isLiked)" You and ${item.likeCounts} others liked this" else "${item.likeCounts} likes"
        holder.itemView.lblHighFives.text = hfText
        holder.itemView.btnFlag.setOnClickListener {
            handler.onReportClicked(item, it, position)
        }
        holder.itemView.commentLayout.setOnClickListener {
            handler.onCommentClicked(item, it, position)
        }
        holder.itemView.imgInteraction.setOnClickListener {
            item.isLiked = !item.isLiked
            notifyItemChanged(position)
            handler.onLikeClick(item, it, position)
        }
        setupList(holder)

        // Todo need check follow status first
        holder.itemView.btnFollow.setUnderlineSpan(if (item.isFollowing) "Following" else "Follow")
        holder.itemView.btnFollow.setOnClickListener {
            if (item.isFollowing)handler.onUnFollowClick(item, it, holder.bindingAdapterPosition)
            else handler.onFollowClick(item, it, holder.bindingAdapterPosition)
            item.isFollowing = !item.isFollowing
            notifyItemChanged(holder.bindingAdapterPosition)
        }
    }

    private fun addPollOptions(
        holder: BaseViewHolder<TeamPostItem>,
        question: String?,
        response: Int,
        item: TeamPostItem,
        handler: OnActionHandler
    ) {
        if (question.isNullOrEmpty()) return
        val pollView = ItemPollView(holder.itemView.context)
        pollView.apply {
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            layoutParams.bottomMargin = 20
            setLayoutParams(layoutParams)
            setQuestion(question)
            onSelected = {
                handler.onResponsePoll(item, it, response)
            }
            holder.itemView.pollLayout.addView(this)
        }
    }

    private fun setupList(holder: BaseViewHolder<TeamPostItem>) {
        holder.itemView.rcvComments.adapter = ConcatAdapter(
            ConcatAdapter.Config.Builder()
                .setIsolateViewTypes(false)
                .build(),
            commentAdapter.apply {
                submitList(commentsMock)
            }
        )
    }

    override fun onLikeClick(item: Any, view: View, parentPosition: Int) {
        handler.onLikeClick(item, view, parentPosition)
    }
}
