package com.vessel.app.createnewtpost

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.teampage.model.PostType
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_create_new_post.*
import kotlinx.android.synthetic.main.fragment_teams.*

@AndroidEntryPoint
class CreateNewPostFragment : BaseFragment<CreateNewPostViewModel>() {
    private var postType: Int? = 0
    override val viewModel: CreateNewPostViewModel by viewModels()
    override val layoutResId: Int = R.layout.fragment_create_new_post
    private lateinit var myTeamView: ConstraintLayout
    var images: List<String> = arrayListOf()
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        postType = arguments?.getInt(Constants.POST_TYPE)
        val title = when (postType) {
            PostType.TEXT.ordinal -> "Create a text post?"
            PostType.PHOTO.ordinal -> ""
            PostType.VIDEO.ordinal -> ""
            PostType.POLL.ordinal -> "Create a poll?"
            else -> "Create a text post?"
        }
        lblTitle.text = title
        imgThumb.visibility = if (postType == PostType.TEXT.ordinal || postType == PostType.POLL.ordinal) View.GONE else View.VISIBLE
        lblTitle.visibility = if (postType == PostType.TEXT.ordinal || postType == PostType.POLL.ordinal) View.VISIBLE else View.GONE
    }

    private fun setupObservers() {
        viewModel.postType = postType
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
