package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BadHabitResponseData(
    val bad_habits: List<BadHabits>
)
@JsonClass(generateAdapter = true)
data class BadHabits(
    val id: Int,
    val title: String,
    val is_active: Boolean
)
