package com.vessel.app.nutritionsoptions.membershipsdialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ManageMembershipsDialogFragment : BaseDialogFragment<MembershipsDialogViewModel>() {
    override val viewModel: MembershipsDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_memberships_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setGravity(Gravity.BOTTOM)
            setBackgroundDrawable(inset)
        }
        isCancelable = false

        observe(viewModel.dismissScreen) { dismiss() }
        observe(viewModel.navigateTo) {
            dismiss()
            findNavController().navigate(it)
        }
    }
}
