// TODO fix the unit test

// package com.vessel.app.common.net.mapper
//
// import com.vessel.app.*
// import com.vessel.app.common.model.Reagent
// import com.vessel.app.recommendation.FoodRecommendationFaker
// import org.assertj.core.api.Assertions.assertThat
// import org.junit.jupiter.api.BeforeEach
// import org.junit.jupiter.api.Test
//
// class FoodRecommendationMapperTest : BaseTest() {
//    private lateinit var mapper: FoodRecommendationMapper
//
//    @BeforeEach
//    fun setUp() {
//        mapper = FoodRecommendationMapper()
//    }
//
//    @Test
//    fun map() {
//        val data = listOf(
//            FoodRecommendationFaker.data(b7 = 0f),
//            FoodRecommendationFaker.data(b9 = 0f),
//            FoodRecommendationFaker.data(b7 = 0f, b9 = 0f, magnesium = 0f, vitaminC = 0f)
//        )
//
//        mapper.map(data).forEachIndexed { index, foodRecommendation ->
//            foodRecommendation.name isEqualTo data[index].name
//            foodRecommendation.usdaNdbNumber isEqualTo data[index].usda_ndb_number
//
//            when (index) {
//                0 -> {
//                    foodRecommendation.nutrients.find { it.reagent == Reagent.B9 && it.amount == data[index].b9 }.isNotNull()
//                    foodRecommendation.nutrients.find { it.reagent == Reagent.Magnesium && it.amount == data[index].magnesium }.isNotNull()
//                    foodRecommendation.nutrients.find { it.reagent == Reagent.VitaminC && it.amount == data[index].vitamin_c }.isNotNull()
//                    foodRecommendation.nutrients.find { it.reagent == Reagent.B7 }.isNull()
//                }
//                1 -> {
//                    foodRecommendation.nutrients.find { it.reagent == Reagent.B9 }.isNull()
//                    foodRecommendation.nutrients.find { it.reagent == Reagent.Magnesium && it.amount == data[index].magnesium }.isNotNull()
//                    foodRecommendation.nutrients.find { it.reagent == Reagent.VitaminC && it.amount == data[index].vitamin_c }.isNotNull()
//                    foodRecommendation.nutrients.find { it.reagent == Reagent.B7 && it.amount == data[index].b7 }.isNotNull()
//                }
//                2 -> {
//                    assertThat(foodRecommendation.nutrients).isEmpty()
//                }
//            }
//        }
//    }
// }
