package com.vessel.app.startnewtimer.ui

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.activationtimer.util.TimerServiceManager
import com.vessel.app.base.BaseFragment
import com.vessel.app.startnewtimer.StartNewTimerViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StartNewTimerFragment : BaseFragment<StartNewTimerViewModel>() {
    override val viewModel: StartNewTimerViewModel by viewModels()
    override val layoutResId = R.layout.fragment_start_new_timer

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            viewModel.onBackPressed()
        }
    }

    private fun setupObservers() {
        viewModel.state.apply {
            observe(showBackWarning) {
                showBackWarning(R.string.are_you_sure_to_back)
            }

            observe(navigateTo) {
                TimerServiceManager.startTimerCountDown()
                findNavController().navigate(it)
            }
        }
    }
}
