package com.vessel.app.comment

import android.util.Log
import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.TeamsRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.teampage.model.TeamPostItem
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.Action
import com.vessel.app.wellness.net.data.FeedReaction
import com.vessel.app.wellness.net.data.ResponseData
import kotlinx.coroutines.launch

class CommentViewModel@ViewModelInject constructor(
    val state: CommentState,
    val resourceProvider: ResourceRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    private val teamsRepository: TeamsRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler {
    init {
    }
    override fun onBackButtonClicked(view: View) {
        Log.d(TAG, "onBackButtonClicked: ")
        view.findNavController().navigateUp()
    }

    companion object {
        const val TAG = "CommentViewModel"
    }

    // Follow contact
    fun followContact(id: Int, targetId: String) {
        viewModelScope.launch {
            showLoading()
            teamsRepository.followContact(id, targetId)
                .onSuccess { result ->
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }
    // Unfollow contact
    fun unFollowContact(id: Int, targetId: String) {
        viewModelScope.launch {
            showLoading()
            teamsRepository.unFollowContact(id, targetId)
                .onSuccess { result ->
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }

    fun actionFollow() {
        val postItem = state.postItem.value
        postItem?.let {
            val targetId = it.actor
            if (it.isFollowing) unFollowContact(preferencesRepository.contactId, targetId)
            else followContact(preferencesRepository.contactId, targetId)
            postItem.isFollowing = !it.isFollowing
            setPostItem(postItem)
        }
    }

    fun setPostItem(postItem: TeamPostItem) {
        state.postItem.value = postItem
    }
    fun onReport() {
        val postItem = state.postItem.value
        postItem?.let {
            state.navigateTo.value =
                CommentFragmentDirections.actionCommentFragmentToPostReportFragment(it)
        }
    }
    // Response a poll
    fun reactionPoll(response: Int) {
        val postItem = state.postItem.value
        val feedResponseAction = FeedReaction()
        feedResponseAction.activityId = postItem?.id
        feedResponseAction.userId = preferencesRepository.contactId.toString()
        feedResponseAction.kind = Action.POLL_RESPONSE.kind
        val data = ResponseData()
        data.response = response
        feedResponseAction.data = data
        viewModelScope.launch {
            showLoading()
            teamsRepository.reactionPost(feedResponseAction)
                .onSuccess { result ->
                    hideLoading()
                }
                .onError {
                    hideLoading()
                }
        }
    }
}
