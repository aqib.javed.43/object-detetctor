package com.vessel.app.appreview.feedback

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class AppReviewFeedbackDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    var feedbackText = ""
    val remainingCharacters = MutableLiveData(
        getRemainingCharacters()
    )

    fun sendFeedback() {
        if (feedbackText.length <= MAX_ALLOWED_FEEDBACK_CHARS_LIMIT) {
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.APP_REVIEW_COMMENTS)
                    .addProperty(TrackingConstants.KEY_TEXT, feedbackText)
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
            dismiss()
        }
    }

    fun dismiss() {
        devicePreferencesRepository.showAppReviewPopup = false
        dismissDialog.call()
    }

    fun onFeedbackTextChanged(s: CharSequence) {
        feedbackText = s.toString()
        remainingCharacters.value = getRemainingCharacters()
    }

    private fun getRemainingCharacters() = getResString(
        R.string.remaining_characters,
        MAX_ALLOWED_FEEDBACK_CHARS_LIMIT - feedbackText.length
    )

    companion object {
        const val MAX_ALLOWED_FEEDBACK_CHARS_LIMIT = 250
    }
}
