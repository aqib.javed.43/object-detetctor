package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MembershipPaymentData(
    val card_last4: Int,
    val card_exp_month: Int,
    val card_exp_year: Int,
    val payment_type: String
)

@JsonClass(generateAdapter = true)
data class MembershipPaymentDataResponse(
    val payment_sources: List<MembershipPaymentData>
)
