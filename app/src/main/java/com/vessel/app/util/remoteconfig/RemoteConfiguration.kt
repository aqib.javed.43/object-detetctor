package com.vessel.app.util.remoteconfig

interface RemoteConfiguration {
    fun init(minimumFetchIntervalSeconds: Long = DEFAULT_MINIMUM_FETCH_INTERVAL_IN_SECONDS)

    fun fetchConfigFromRemote()

    fun getString(key: String): String
    fun getBoolean(key: String): Boolean
    fun getDouble(key: String): Double
    fun getLong(key: String): Long

    companion object {
        private const val DEFAULT_MINIMUM_FETCH_INTERVAL_IN_SECONDS = 0L
    }
}
