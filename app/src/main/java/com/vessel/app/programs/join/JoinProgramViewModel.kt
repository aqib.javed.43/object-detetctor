package com.vessel.app.programs.join

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Program
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.ProgramAction
import kotlinx.coroutines.launch

class JoinProgramViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val programManager: ProgramManager,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceProvider),
    ToolbarHandler {

    val state = JoinProgramState(savedStateHandle.get<Program>("program")!!)
    val location = savedStateHandle.get<String>("location")!!

    fun onAddItemsToThePlanClicked() {
        enrollProgram(ProgramAction.KEEP)
    }

    fun onReplaceItemsToThePlanClicked() {
        enrollProgram(ProgramAction.CLEAR)
    }

    private fun enrollProgram(programAction: ProgramAction) {
        state {
            viewModelScope.launch {
                showLoading()
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.PROGRAM_ENROLLED)
                        .addProperty(TrackingConstants.KEY_ID, program.id.toString())
                        .addProperty(TrackingConstants.ENROLL_TYPE, programAction.value)
                        .addProperty(TrackingConstants.KEY_LOCATION, location)
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
                programManager.joinProgram(program, programAction)
                    .onSuccess {
                        navigateToSuccessfullyJoin()
                        hideLoading()
                    }
                    .onServiceError {
                        showError(it.error.message ?: getResString(R.string.unknown_error))
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }
                    .onError {
                        hideLoading()
                    }
            }
        }
    }

    private fun navigateToSuccessfullyJoin() {
        state.navigateTo.value =
            JoinProgramFragmentDirections.actionJoinProgramFragmentToSuccessfullyJoinDialogFragment(
                location == Constants.COMPLETE_PROGRAM_PAGE
            )
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
}
