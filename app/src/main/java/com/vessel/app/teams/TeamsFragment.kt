package com.vessel.app.teams

import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.binding.srcFromUrl
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_teams.*

@AndroidEntryPoint
class TeamsFragment : BaseFragment<TeamsViewModel>() {
    override val viewModel: TeamsViewModel by viewModels()
    override val layoutResId: Int = R.layout.fragment_teams
    private lateinit var myTeamView: ConstraintLayout
    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        myTeamView = requireView().findViewById(R.id.myTeamView)
        myTeamView.isVisible = viewModel.hadMainGoal()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(goal) {
                updateTeamView(it)
            }
        }
        viewModel.getTeamData()
    }

    private fun updateTeamView(it: GoalRecord?) {
        lblTeamName.text = String.format(getString(R.string.your_team_title), it?.name)
        imgTeams.srcFromUrl(it?.image_small_url)
    }
}
