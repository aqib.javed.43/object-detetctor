package com.vessel.app.toprecommendations

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.wellness.model.Tip

class RecommendationsSelectAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<Tip>({ oldItem, newItem ->
        oldItem.id == newItem.id
    }) {
    override fun getLayout(position: Int) = R.layout.item_select_recommendation

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onRecommendationSelectClicked(item: Tip)
        fun onRecommendationClicked(item: Tip)
    }
}
