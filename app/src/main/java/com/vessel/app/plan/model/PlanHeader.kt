package com.vessel.app.plan.model

data class PlanHeader(
    val canCheckMoreInfo: Boolean = true
)
