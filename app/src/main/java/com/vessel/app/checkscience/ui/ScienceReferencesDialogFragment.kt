package com.vessel.app.checkscience.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.checkscience.ScienceReferenceViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ScienceReferencesDialogFragment : BaseDialogFragment<ScienceReferenceViewModel>() {
    override val viewModel: ScienceReferenceViewModel by viewModels()
    override val layoutResId = R.layout.fragment_science_references
    private val checkOutScienceItemsAdapter by lazy { CheckOutScienceItemsAdapter(viewModel) }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
        isCancelable = false
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
        requireView().findViewById<RecyclerView>(R.id.references)?.adapter =
            checkOutScienceItemsAdapter
    }

    fun setupObservers() {
        viewModel.state {
            observe(dismissDialog) { dismiss() }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }
}
