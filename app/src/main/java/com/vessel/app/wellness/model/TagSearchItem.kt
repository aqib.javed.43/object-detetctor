package com.vessel.app.wellness.model

data class TagSearchItem(var searchQuery: String, var tagsList: List<Tag>)
