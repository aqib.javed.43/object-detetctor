package com.vessel.app.common.model.data

import com.vessel.app.common.net.data.ReagentResponseInfoData
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.wellness.net.data.GoalData
import com.vessel.app.wellness.net.data.ReagentsData

object GoalMapper {
    fun map(item: GoalData) = GoalRecord(
        item.id,
        item.name,
        item.image_small_url.orEmpty(),
        item.image_medium_url.orEmpty(),
        item.image_large_url.orEmpty(),
        item.reagents!!.map { mapReagent(it!!) },
        item.tip_count
    )

    private fun mapReagent(reagent: ReagentsData) = Reagent(
        id = reagent.id,
        consumptionUnit = reagent.consumption_unit,
        impact = reagent.impact,
        info = reagent.info.map { mapReagentInfo(it) },
        displayName = reagent.name,
        recommendedDailyAllowance = reagent.recommended_daily_allowance,
        state = reagent.state,
        unit = reagent.unit,
        apiName = "",
        headerImage = 0,
        isBeta = false,
        isInverted = false,
        lowerBound = 0f,
        maxValue = 0f,
        minValue = 0f,
        queryString = "",
        upperBound = 0f
    )

    private fun mapReagentInfo(reagentInfo: ReagentResponseInfoData) = ReagentInfo(
        id = reagentInfo.id,
        reagentId = reagentInfo.reagent_id,
        title = reagentInfo.title,
        comingSoonTagItems = reagentInfo.coming_soon_tag_items,
        description = reagentInfo.description,
        infoType = ReagentInfoType.valueOf(reagentInfo.info_type),
        imageUrl = reagentInfo.coming_soon_image
    )
}
