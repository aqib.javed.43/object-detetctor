package com.vessel.app.lessondetails.ui

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.model.Question
import com.vessel.app.lessondetails.LessonDetailsViewModel
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.BlackWithoutOverlayAppButtonViewGroup
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.auth_app_button_view_group.view.*
import kotlinx.android.synthetic.main.fragment_lesson_details.*
import kotlinx.android.synthetic.main.swipe_item_view.view.*

@AndroidEntryPoint
class LessonDetailsFragment : BaseFragment<LessonDetailsViewModel>() {
    override val viewModel: LessonDetailsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_lesson_details

    private val answerAdapter: AnswerAdapter by lazy { AnswerAdapter(viewModel) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        secondaryText.movementMethod = ScrollingMovementMethod()
        rcvAnswers.adapter = ConcatAdapter(
            ConcatAdapter.Config.Builder()
                .setIsolateViewTypes(false)
                .build(),
            answerAdapter
        )
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(lesson) { it ->
                it.questionCount?.let { count ->
                    progressLayout.setStepsCount(count.toInt(), 1)
                }
            }
            observe(currentIndex) {
                progressLayout.setActiveStep(it)
            }
            observe(currentQuestion) {
                processView(it)
            }
            observe(answers) {
                answerAdapter.submitList(it)
            }
            observe(backToHome) {
                findNavController().popBackStack(R.id.homeScreenFragment, false)
            }
            observe(rightAnswer) {
                rightAnswerText.visibility = if (it.isNullOrEmpty()) View.GONE else View.VISIBLE
                rightAnswerText.text = it
            }
            observe(showOrHideQuestion) { isShow ->
                primaryText.visibility = if (isShow) View.VISIBLE else View.GONE
                secondaryText.visibility = if (isShow) View.VISIBLE else View.GONE
            }
            observe(isCompletedLesson) {
                val buttonView = btnNext as BlackWithoutOverlayAppButtonViewGroup
                if (it == true) {
                    buttonView.setText(getString(R.string.lesson_completed))
                    buttonView.dotsImageView.visibility = View.GONE
                    buttonView.button.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                    buttonView.button.gravity = Gravity.CENTER
                } else {
                    buttonView.setText(getString(R.string.next))
                }
            }
        }
    }

    private fun processView(qs: Question) {
        when (qs.type) {
            Constants.QuestionType.QUIZ.name -> {
                primaryText.visibility = View.VISIBLE
                primaryText.text = qs.text
                secondaryText.visibility = View.GONE
                inputText.visibility = View.GONE
            }
            Constants.QuestionType.SURVEY.name -> {
                primaryText.visibility = View.VISIBLE
                primaryText.text = qs.text
                secondaryText.visibility = View.GONE
                inputText.visibility = View.GONE
            }
            Constants.QuestionType.INPUT.name -> {
                primaryText.visibility = View.VISIBLE
                primaryText.text = qs.text
                secondaryText.visibility = View.GONE
                inputText.visibility = View.VISIBLE
                inputText.hint = qs.placeholderText
            }
            Constants.QuestionType.READONLY.name -> {
                secondaryText.visibility = View.VISIBLE
                primaryText.visibility = View.GONE
                secondaryText.text = qs.text
                secondaryText.scrollTo(0, 0)
                inputText.visibility = View.GONE
            }
            else -> {
            }
        }
    }
}
