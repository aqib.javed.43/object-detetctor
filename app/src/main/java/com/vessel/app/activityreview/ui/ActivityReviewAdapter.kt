package com.vessel.app.activityreview.ui

import com.vessel.app.R
import com.vessel.app.activityreview.model.RecommendationReview
import com.vessel.app.base.BaseAdapterWithDiffUtil
import kotlinx.android.synthetic.main.item_activity_review.view.*

class ActivityReviewAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<RecommendationReview>() {
    override fun getLayout(position: Int) = R.layout.item_activity_review

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<RecommendationReview>, position: Int) {
        val item = getItem(position)
        holder.itemView.btnDelete.setOnClickListener {
            handler.onDeleteReview(item, position)
        }
        super.onBindViewHolder(holder, position)
    }
    interface OnActionHandler {
        fun onWriteNewReview()
        fun onDeleteReview(item: RecommendationReview, position: Int)
        fun onEditReview(item: RecommendationReview)
        fun onFlagReview(item: RecommendationReview)
    }
}
