package com.vessel.app.timeofdaydialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TimeOfDayDialogFragment : BaseDialogFragment<TimeOfDayDialogViewModel>() {
    override val viewModel: TimeOfDayDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_time_of_day_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
        isCancelable = false

        observe(viewModel.dismissDialog) { dismiss() }
        observe(viewModel.navigateTo) { findNavController().navigate(it) }
    }
}
