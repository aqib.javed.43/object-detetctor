package com.vessel.app.plan.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.plan.model.EmptyPlanItem

class EmptyPlanAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Any>() {
    override fun getLayout(position: Int) = when (getItem(position)) {
        EmptyPlanItem -> R.layout.item_empty_plan_header
        else -> throw IllegalStateException("Unexpected WellnessAdapter type at position $position for item ${getItem(position)}")
    }

    override fun getHandler(position: Int): Any? = handler

    interface OnActionHandler {
        fun onClosePlanClicked(view: View)
    }
}
