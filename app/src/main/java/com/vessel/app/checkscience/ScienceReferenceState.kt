package com.vessel.app.checkscience

import androidx.navigation.NavDirections
import com.vessel.app.checkscience.model.ScienceReference
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ScienceReferenceState @Inject constructor(val scienceReferences: Array<ScienceReference>) {
    val dismissDialog = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: ScienceReferenceState.() -> Unit) = apply(block)
}
