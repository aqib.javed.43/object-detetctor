package com.vessel.app.filterfoodplandialog.model

import androidx.annotation.StringRes
import com.vessel.app.R

enum class FilterNutrient(@StringRes val title: Int, val apiName: String, val reagentId: Int) {
    Magnesium(R.string.magnesium, "Magnesium", 5),
    B7(R.string.b7, "VitaminB7", 11),
    VitaminC(R.string.vitamin_c, "VitaminC", 4)
}
