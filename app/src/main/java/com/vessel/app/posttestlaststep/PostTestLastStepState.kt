package com.vessel.app.posttestlaststep

import javax.inject.Inject

class PostTestLastStepState @Inject constructor() {
    operator fun invoke(block: PostTestLastStepState.() -> Unit) = apply(block)
}
