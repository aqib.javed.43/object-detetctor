package com.vessel.app.programs.join

import android.os.Bundle
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class JoinProgramFragment : BaseFragment<JoinProgramViewModel>() {
    override val viewModel: JoinProgramViewModel by viewModels()
    override val layoutResId = R.layout.fragment_program_join

    override fun onViewLoad(savedInstanceState: Bundle?) {
        requireView().findViewById<AppCompatTextView>(R.id.title).text =
            getString(
                R.string.lets_add_new_items_to_your_plan,
                viewModel.state.program.getItemCount()
            )
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        findNavController().previousBackStackEntry
            ?.savedStateHandle?.apply {
                set(
                    JOIN_PROGRAM,
                    true
                )
            }
    }

    companion object {
        const val JOIN_PROGRAM = "join program"
    }
}
