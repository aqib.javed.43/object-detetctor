package com.vessel.app.programs.complete.steptwo

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.widget.ProgramCardWidget
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CompleteProgramStep2Fragment : BaseFragment<CompleteProgramStep2ViewModel>() {
    override val viewModel: CompleteProgramStep2ViewModel by viewModels()
    override val layoutResId = R.layout.fragment_complete_program_step_2
    private lateinit var programList: ProgramCardWidget
    override fun onViewLoad(savedInstanceState: Bundle?) {
        programList = requireView().findViewById(R.id.suggestedPrograms)
        programList.setHandler(viewModel)
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(suggestedPrograms) {
                programList.setList(it)
            }
        }
    }
}
