package com.vessel.app.common.di

import com.vessel.app.common.database.VesselDatabase
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.net.TokenAuthenticator
import com.vessel.app.common.repo.AuthRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.usecase.PrefetchUseCase
import com.vessel.app.homescreen.HomeTabsManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ManagerModule {
    @Singleton
    @Provides
    fun authManager(
        authRepository: AuthRepository,
        preferencesRepository: PreferencesRepository,
        contactManager: ContactManager,
        tokenAuthenticator: TokenAuthenticator,
        vesselDatabase: VesselDatabase,
        prefetchUseCase: PrefetchUseCase,
        homeTabsManager: HomeTabsManager,
        programManager: ProgramManager
    ) = AuthManager(
        authRepository,
        preferencesRepository,
        contactManager,
        vesselDatabase,
        prefetchUseCase,
        homeTabsManager,
        programManager
    ).also {
        contactManager.setAuthManager(it)
        tokenAuthenticator.setAuthManager(it)
    }
}
