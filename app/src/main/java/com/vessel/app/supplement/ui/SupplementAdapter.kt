package com.vessel.app.supplement.ui

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.supplement.model.SupplementFormula

class SupplementAdapter(
    private val handler: OnActionHandler
) : BaseAdapterWithDiffUtil<SupplementFormula>() {

    override fun getLayout(position: Int) = when (getItem(position)) {
        is SupplementFormula -> R.layout.item_supplement_formula
        else -> throw IllegalStateException(
            "Unexpected SupplementAdapter type at position $position for item ${getItem(position)}"
        )
    }

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onAddToPlanClick(item: SupplementFormula)
        fun onInfoIconClicked(view: View, item: SupplementFormula)
    }
}
