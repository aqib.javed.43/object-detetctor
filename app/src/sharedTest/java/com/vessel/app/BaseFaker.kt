package com.vessel.app

import com.github.javafaker.Faker
import com.vessel.app.BaseFaker.Fake.FAKER

object BaseFaker {
    fun <T> list(minCount: Int = 0, basic: () -> T): List<T> {
        val list = mutableListOf<T>()
        for (i in 0..minCount + (2..10).random()) list.add(basic())
        return list
    }

    inline fun <reified T> array(basic: () -> T): Array<T> {
        val itemCount = (2..10).random()
        return Array(itemCount) { basic() }
    }

    object Fake {
        val FAKER: Faker = Faker()
    }
}

fun validPassword(): String = FAKER.lorem().characters(8, 25)

fun randomDouble(
    min: Int = 0,
    max: Int = 1000
) = FAKER.number().randomDouble(2, min, max)
