package com.vessel.app.common.manager

import com.vessel.app.Constants
import com.vessel.app.common.model.DateEntry
import com.vessel.app.common.model.Goal
import com.vessel.app.common.model.ReagentEntry
import com.vessel.app.common.model.Result
import com.vessel.app.common.model.data.Reagent
import com.vessel.app.common.model.data.ReagentItem
import com.vessel.app.wellness.repo.ScoreRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.roundToInt

@Singleton
class ScoreManager @Inject constructor(
    private val scoreRepository: ScoreRepository,
    private val reagentsManager: ReagentsManager
) {
    private val scoreDataStateChannel = BroadcastChannel<Boolean>(1)
    private val firstTestStateChannel = BroadcastChannel<Boolean>(1)
    private val testStateChannel = BroadcastChannel<Boolean>(1)
    private val planRecommendationTestStateChannel = BroadcastChannel<Boolean>(1)

    suspend fun getWellnessScores(forceUpdate: Boolean = false) =
        scoreRepository.getWellnessScores(forceUpdate).let { result ->
            when (result) {
                is Result.Success -> result.value.let { score ->
                    Result.Success(
                        score.copy(
                            goals = Goal.values()
                                .map { it to it.getEntries(score.reagents) }
                                .toMap()
                        )
                    )
                }
                else -> result
            }
        }
    suspend fun getLatestWellnessScores(forceUpdate: Boolean = false) =
        scoreRepository.getLatestWellnessScores(forceUpdate).let { result ->
            when (result) {
                is Result.Success -> result.value.let { score ->
                    Result.Success(
                        score.copy(
                            goals = Goal.values()
                                .map { it to it.getEntries(score.reagents) }
                                .toMap()
                        )
                    )
                }
                else -> result
            }
        }

    suspend fun getAverageScores(forceUpdate: Boolean = false) =
        scoreRepository.getAverageScores(forceUpdate).let { result ->
            when (result) {
                is Result.Success -> result.value.let { score ->
                    Result.Success(
                        score.copy(
                            goals = Goal.values()
                                .map { it to it.getEntries(score.reagents) }
                                .toMap()
                        )
                    )
                }
                else -> result
            }
        }

    suspend fun getWellnessScoresWithAverage(forceUpdate: Boolean = false) =
        scoreRepository.getWellnessScoresWithAverage(forceUpdate).let { result ->
            when (result) {
                is Result.Success -> result.value.let { score ->
                    Result.Success(
                        Pair(
                            score.first.copy(
                                goals = Goal.values()
                                    .map { it to it.getEntries(score.first.reagents) }
                                    .toMap()
                            ),
                            score.second.copy(
                                goals = Goal.values()
                                    .map { it to it.getEntries(score.second.reagents) }
                                    .toMap()
                            )
                        )
                    )
                }
                else -> result
            }
        }

    private fun Goal.getEntries(reagentResult: Map<Reagent, List<ReagentEntry>>): List<DateEntry> =
        mutableListOf<DateEntry>().let { list ->
            val items = reagents.map { reagentsManager.fromId(it.id) }
            items.forEach { reagent ->
                if (reagentResult.containsKey(reagent)) {
                    reagentResult[reagent]?.forEachIndexed { index, reagentEntry ->
                        val isNotAvailableReagent =
                            reagentEntry.score == null || reagentEntry.score == Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE
                        if (list.size > index) {
                            list[index].y += if (isNotAvailableReagent) 0f else reagentEntry.score!!
                            list[index].reagentsCount =
                                if (isNotAvailableReagent) list[index].reagentsCount else list[index].reagentsCount + 1
                        } else {
                            list.add(
                                DateEntry(
                                    reagentEntry.x,
                                    reagentEntry.score
                                        ?: Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE,
                                    reagentEntry.date,
                                    if (isNotAvailableReagent) 0 else 1
                                )
                            )
                        }
                    }
                }
            }

            list.forEach {
                val value = it.y / it.reagentsCount
                it.y = if (value > 0f) value.roundToInt().toFloat() else 0f
            }

            list
        }

    fun updateReagentDeficienciesCachedData() {
        CoroutineScope(SupervisorJob()).launch {
            reagentDeficiencies(true)
        }
    }

    fun updateWellnessScoreCachedData() {
        CoroutineScope(SupervisorJob()).launch {
            if (getWellnessScoresWithAverage(true) is Result.Success) {
                updateScoreDataState()
            }
        }
    }

    suspend fun reagentDeficiencies(forceUpdate: Boolean = false): Result<Map<Reagent, Float>, Nothing> {
        return scoreRepository.getWellnessScores(forceUpdate).let {
            when (it) {
                is Result.Success -> {
                    val reagentToDeficiency = mutableMapOf<Reagent, Float>()
                    it.value.reagents.forEach { entry ->
                        when (entry.key.id) {
                            ReagentItem.VitaminC.id -> addVitaminCDeficiencies(
                                reagentToDeficiency,
                                entry.value.last().y
                            )
                            ReagentItem.B7.id -> addB7Deficiencies(
                                reagentToDeficiency,
                                entry.value.last().y
                            )
                            ReagentItem.B9.id -> addB9Deficiencies(
                                reagentToDeficiency,
                                entry.value.last().y
                            )
                            ReagentItem.Magnesium.id -> addMagnesiumDeficiencies(
                                reagentToDeficiency,
                                entry.value.last().y
                            )
                            ReagentItem.Calcium.id -> addCalciumDeficiencies(
                                reagentToDeficiency,
                                entry.value.last().y
                            )
                        }
                    }
                    Result.Success(reagentToDeficiency)
                }
                is Result.HttpError -> it
                is Result.NetworkIOError -> it
                is Result.UnknownError -> it
                is Result.ServiceError -> it
            }
        }
    }

    private fun addVitaminCDeficiencies(map: MutableMap<Reagent, Float>, value: Float) {
        val reagent = reagentsManager.fromId(ReagentItem.VitaminC.id) ?: return
        handleReagentReportedValue(map, value, reagent)
    }

    private fun addB7Deficiencies(map: MutableMap<Reagent, Float>, value: Float) {
        val reagent = reagentsManager.fromId(ReagentItem.B7.id) ?: return
        handleReagentReportedValue(map, value, reagent)
    }

    private fun addB9Deficiencies(map: MutableMap<Reagent, Float>, value: Float) {
        val reagent = reagentsManager.fromId(ReagentItem.B9.id) ?: return
        handleReagentReportedValue(map, value, reagent)
    }

    private fun addMagnesiumDeficiencies(map: MutableMap<Reagent, Float>, value: Float) {
        val reagent = reagentsManager.fromId(ReagentItem.Magnesium.id) ?: return
        handleReagentReportedValue(map, value, reagent)
    }
    private fun addCalciumDeficiencies(map: MutableMap<Reagent, Float>, value: Float) {
        val reagent = reagentsManager.fromId(ReagentItem.Calcium.id) ?: return
        handleReagentReportedValue(map, value, reagent)
    }
    private fun addSodiumDeficiencies(map: MutableMap<Reagent, Float>, value: Float) {
        val reagent = reagentsManager.fromId(ReagentItem.Sodium.id) ?: return
        handleReagentReportedValue(map, value, reagent)
    }
    private fun addChlorideDeficiencies(map: MutableMap<Reagent, Float>, value: Float) {
        val reagent = reagentsManager.fromId(ReagentItem.Chloride.id) ?: return
        handleReagentReportedValue(map, value, reagent)
    }
    private fun addNitritesDeficiencies(map: MutableMap<Reagent, Float>, value: Float) {
        val reagent = reagentsManager.fromId(ReagentItem.Nitrites.id) ?: return
        handleReagentReportedValue(map, value, reagent)
    }
    private fun handleReagentReportedValue(
        map: MutableMap<Reagent, Float>,
        value: Float,
        reagent: Reagent
    ) {
        val reportedRange = reagent.ranges.firstOrNull { it.reportedValue == value }
        val mappedValue = (
            reportedRange?.rangeRecommendation?.food?.rda_multiplier
                ?: 0f
            ) * (reagent.recommendedDailyAllowance ?: 0f)
        map[reagent] = mappedValue
    }

    fun getScoreDataState() = scoreDataStateChannel.asFlow()

    private fun updateScoreDataState() {
        scoreDataStateChannel.offer(true)
    }

    fun clearScoreDataState() {
        scoreDataStateChannel.offer(false)
    }

    fun getFirstTestState() = firstTestStateChannel.asFlow()

    fun updateFirstTestState() {
        firstTestStateChannel.offer(true)
    }

    fun clearFirstTestState() {
        firstTestStateChannel.offer(false)
    }

    fun getTestState() = testStateChannel.asFlow()

    fun updateTestState() {
        testStateChannel.offer(true)
    }

    fun clearTestState() {
        testStateChannel.offer(false)
    }

    fun getPlanRecommendationTestState() = planRecommendationTestStateChannel.asFlow()

    fun updatePlanRecommendationTestState() {
        planRecommendationTestStateChannel.offer(true)
    }

    fun clearPlanRecommendationTestState() {
        planRecommendationTestStateChannel.offer(false)
    }

    fun clearCachedData() {
        scoreRepository.clearCachedData()
    }
}
