package com.vessel.app.activationtimer.video2tab

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.util.ResourceRepository

class Video2TabViewModel @ViewModelInject constructor(
    val state: Video2TabState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider) {

    val afterFirstTest = preferencesRepository.afterFirstTest()
}
