package com.vessel.app.filterfoodplandialog.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.filterfoodplandialog.model.FilterHeaderItem

class FilterHeaderAdapter : BaseAdapterWithDiffUtil<FilterHeaderItem>() {
    override fun getLayout(position: Int) = R.layout.item_filter_header
}
