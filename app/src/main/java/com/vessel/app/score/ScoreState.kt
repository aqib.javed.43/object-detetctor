package com.vessel.app.score

import androidx.lifecycle.MutableLiveData
import com.vessel.app.score.model.ScoreGoalItem
import com.vessel.app.score.model.ScoreWellnessItem
import javax.inject.Inject

class ScoreState @Inject constructor() {
    val scoreWellnessItems = MutableLiveData<List<ScoreWellnessItem>>()
    val scoreGoalItems = MutableLiveData<List<ScoreGoalItem>>()

    operator fun invoke(block: ScoreState.() -> Unit) = apply(block)
}
