package com.vessel.app.programs.details.adapters

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.common.model.Program

class ProgramFooterAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<Program>() {

    override fun getLayout(position: Int) = R.layout.item_program_footer
    override fun getHandler(position: Int): Any = handler
    interface OnActionHandler {
        fun onLeaveProgramClicked(item: Program)
    }
}
