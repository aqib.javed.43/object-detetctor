package com.vessel.app.reagent.model

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan

data class ReachInfo(val title: String, val longText: String) {
    fun getFormattedText(): SpannableStringBuilder {
        val referencesIndex = longText.indexOf(REFERENCES_STRING, ignoreCase = true)
        if (referencesIndex != -1) {
            val str = SpannableStringBuilder(longText.replace(REFERENCES_STRING, NEW_REFERENCES_STRING, true))
            str.setSpan(
                StyleSpan(Typeface.BOLD),
                referencesIndex,
                referencesIndex + NEW_REFERENCES_STRING.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return str
        }
        return SpannableStringBuilder(longText)
    }
    private companion object {
        const val REFERENCES_STRING = "**references:**"
        const val NEW_REFERENCES_STRING = "References:"
    }
}
