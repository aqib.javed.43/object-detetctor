package com.vessel.app.activationtimer.video2tab

import javax.inject.Inject

class Video2TabState @Inject constructor() {

    operator fun invoke(block: Video2TabState.() -> Unit) = apply(block)
}
