package com.vessel.app.common.net.mapper

import com.vessel.app.Constants.SIMPLE_TIME_12_HOUR_FORMAT
import com.vessel.app.Constants.SIMPLE_TIME_24_WITH_SECONDS_FORMAT
import com.vessel.app.common.model.ScheduleItem
import com.vessel.app.common.model.ScheduleModel
import com.vessel.app.common.model.SchedulePlanItem
import com.vessel.app.common.net.data.ScheduleItemData
import com.vessel.app.common.net.data.SchedulePlanItemData
import com.vessel.app.common.net.data.ScheduleResponseData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProgramScheduleMapper @Inject constructor() {

    fun mapSchedule(scheduleResponseData: ScheduleResponseData) =
        scheduleResponseData.program_schedules.map {
            ScheduleModel(
                programId = it.program_id,
                schedule = it.schedule.map { rowData -> mapScheduleRow(rowData) }
            )
        }

    private fun mapScheduleRow(scheduleItemData: ScheduleItemData) =
        ScheduleItem(
            day = scheduleItemData.day,
            plans = scheduleItemData.plans.map { mapSchedulePlanData(it) }
        )

    private fun mapSchedulePlanData(schedulePlanItemData: SchedulePlanItemData) =
        SchedulePlanItem(
            planId = schedulePlanItemData.plan_id,
            title = schedulePlanItemData.title,
            timeOfDay = schedulePlanItemData.time_of_day?.let {
                SIMPLE_TIME_12_HOUR_FORMAT.format(
                    SIMPLE_TIME_24_WITH_SECONDS_FORMAT.parse(it)!!
                )
            },
            completed = schedulePlanItemData.completed ?: false
        )
}
