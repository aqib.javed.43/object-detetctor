package com.vessel.app.common.util

sealed class DisclaimerFooter(val paddingBottomVisible: Boolean) {
    object Default : DisclaimerFooter(false)
    object WithBottomNavPadding : DisclaimerFooter(true)
}
