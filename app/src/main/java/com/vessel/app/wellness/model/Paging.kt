package com.vessel.app.wellness.model

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Paging(
    val currentPage: Int,
    val pageSize: Int
) : Parcelable {
    @IgnoredOnParcel
    var totalSize: Int? = null

    @IgnoredOnParcel
    var totalPages: Int? = null

    @IgnoredOnParcel
    var showLoadMore: Boolean? = null
}
