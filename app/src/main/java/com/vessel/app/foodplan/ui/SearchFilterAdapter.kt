package com.vessel.app.foodplan.ui

import android.view.View
import androidx.appcompat.widget.SearchView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodplan.model.SearchFilterItem

class SearchFilterAdapter(
    val handler: SearchFilterHandler
) : BaseAdapterWithDiffUtil<SearchFilterItem>() {
    override fun getLayout(position: Int) = R.layout.layout_search_filter

    override fun getHandler(position: Int): Any = handler

    interface SearchFilterHandler {

        val onQueryTextListener: SearchView.OnQueryTextListener
            get() = object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return onSearchQueryTextChange(query)
                }

                override fun onQueryTextChange(query: String?): Boolean {
                    return onSearchQueryTextChange(query)
                }
            }

        fun onSearchQueryTextChange(query: String?): Boolean

        fun onFilterButtonClick(view: View)
    }
}
