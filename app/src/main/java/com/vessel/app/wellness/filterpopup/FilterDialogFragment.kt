package com.vessel.app.wellness.filterpopup

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.BR
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.supplementsbucket.ui.descriptionpopup.OnDescriptionActionHandler
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.FilterWithSearchWidgetOnActionHandler
import com.vessel.app.views.TagListWithSearchWidget
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FilterDialogFragment :
    BaseDialogFragment<FilterDialogViewModel>(),
    FilterWithSearchWidgetOnActionHandler,
    OnDescriptionActionHandler {
    override val viewModel: FilterDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_filter_dialog
    private val filterCategoryAdapter by lazy { FilterCategoryAdapter(viewModel) }
    private lateinit var popupWindow: PopupWindow
    private val descriptionPopupText = MutableLiveData<String>()

    private val tagListWithSearchWidget by lazy {
        requireView().findViewById<TagListWithSearchWidget>(
            R.id.tagListWithSearchWidget
        )
    }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
            setGravity(Gravity.BOTTOM)
        }
        setupPopup()
    }

    override fun onDismiss(dialog: DialogInterface) {
        findNavController().previousBackStackEntry
            ?.savedStateHandle?.apply {
                set(
                    SELECTED_FILTER_ITEMS,
                    Pair(tagListWithSearchWidget.getTags(), viewModel.filterCategory.value)
                )
            }
        super.onDismiss(dialog)
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        requireView().findViewById<RecyclerView>(R.id.dialogRecyclerView)
            ?.apply {
                adapter = filterCategoryAdapter
                itemAnimator = null
            }
        if (viewModel.tagsList.isNullOrEmpty().not()) {
            tagListWithSearchWidget.setList(viewModel.tagsList?.toList()!!)
            tagListWithSearchWidget.setHandler(this)
        } else {
            tagListWithSearchWidget.isVisible = false
        }
        setupObservers()
    }

    private fun setupObservers() {
        observe(viewModel.dismissDialog) {
            dismiss()
        }
        observe(viewModel.filterCategory) {
            filterCategoryAdapter.submitList(it)
        }
    }

    private fun setupPopup() {
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        val viewDataBinding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            R.layout.supplement_description_popup,
            null,
            false
        )
            .apply {
                setVariable(BR.item, descriptionPopupText)
                setVariable(BR.handler, this@FilterDialogFragment)
                lifecycleOwner = viewLifecycleOwner
            }
        popupWindow = PopupWindow(viewDataBinding.root, width, height, true)
    }

    override fun onInfoIconClicked(view: View) {
        val positionOnScreen = IntArray(2)
        view.getLocationInWindow(positionOnScreen)
        descriptionPopupText.value = getString(R.string.tags_info_text)
        val descriptionPopupBottomMargin =
            resources.getDimension(R.dimen.tags_info_popup_bottom_margin).toInt()
        popupWindow.showAtLocation(
            requireView(),
            Gravity.NO_GRAVITY,
            0,
            positionOnScreen[1] + descriptionPopupBottomMargin
        )
    }

    override fun onCloseDialogClicked(view: View) {
        popupWindow.dismiss()
    }

    companion object {
        const val SELECTED_FILTER_OPTIONS = "selectedFilterOptions"
        const val SELECTED_FILTER_TAGS = "tags"
        const val SELECTED_FILTER_ITEMS = "selectedFilterItems"
    }
}
