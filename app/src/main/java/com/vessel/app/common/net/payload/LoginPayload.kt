package com.vessel.app.common.net.payload

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class LoginPayload(
    val email: String,
    val password: String
)
