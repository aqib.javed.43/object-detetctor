package com.vessel.app.photocapture

import android.graphics.Bitmap
import android.graphics.RectF
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.google.zxing.*
import com.google.zxing.common.HybridBinarizer
import com.google.zxing.common.detector.WhiteRectangleDetector
import com.vessel.app.Constants
import com.vessel.app.common.manager.TfliteObjectDetectionManager
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc.*
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.image.TensorImage
import kotlin.math.roundToInt

// resize dimensions for cropped image
const val CROP_WIDTH = 480.toDouble()
const val CROP_HEIGHT = 640.toDouble()

class UpdatedImageProcessor(
    private val listener: Listener,
    private val detectionBundle: TfliteObjectDetectionManager.TfliteDetectionBundle
) {
    data class Config(
        val tolerance: Tolerance,
        val paddingAdjustment: PaddingAdjustment
    ) {
        companion object {
            val default = Config(
                Tolerance(0.82, 0.82, 0.76, 0.76),
                PaddingAdjustment(4.0, 4.0, 12.0, 4.0)
            )
        }

        data class Tolerance(
            val topLeft: Double,
            val topRight: Double,
            val bottomLeft: Double,
            val bottomRight: Double
        )

        data class PaddingAdjustment(
            val leftSidePadding: Double,
            val rightSidePadding: Double,
            val topPadding: Double,
            val bottomPadding: Double
        )
    }

    interface Listener {
        fun onBlockScanErrorDim()
        fun onBlockScanErrorBright()
        fun onBlockScanErrorShadow()
        fun onBlockScanErrorTooFar()
        fun onMoveCameraErrorAfterScan()
        fun onBlockScanErrorTooClose()
        fun onFiducialsDetected(items: List<MatchTemplateLocation>)
        fun onQrDataDetected(qrCodeMetadataResult: Result?)
        fun onAllQualifyingImagesCreated(
            bitmap: Bitmap,
            qrCodeMetadataResult: Result?,
            foundTemplateMatches: MutableList<Rect>,
            resizeScale: Double,
            isFinalProcessing: Boolean,
            resultPoints: List<ResultPoint>
        )
    }

    // found template match regions
    private var foundTemplateMatches: MutableList<Rect> = mutableListOf()
    private var foundTemplateMatchesRaw: MutableList<RectF> = mutableListOf()
    private var consecutiveAllFiducialDetected: Int = 0

    private val tfImageBuffer = TensorImage(DataType.UINT8)

    private fun detectQrCode(image: Mat) {
        val finalBitmapImage = Bitmap.createBitmap(
            image.cols(),
            image.rows(),
            Bitmap.Config.ARGB_8888
        )
        Utils.matToBitmap(image, finalBitmapImage)

        val intArray = IntArray(finalBitmapImage.width * finalBitmapImage.height)
        // copy pixel data from the Bitmap into the 'intArray' array
        finalBitmapImage.getPixels(
            intArray,
            0,
            finalBitmapImage.width,
            0,
            0,
            finalBitmapImage.width,
            finalBitmapImage.height
        )

        val source: LuminanceSource =
            RGBLuminanceSource(finalBitmapImage.width, finalBitmapImage.height, intArray)

        val hybridBinarizer = HybridBinarizer(source)
        val bitmap = BinaryBitmap(hybridBinarizer)

        val result = try {
            MultiFormatReader().decode(bitmap)
        } catch (e: NotFoundException) {
            Log.d("TAG", "Code Not Found")
            e.printStackTrace()
            null
        }
        if (result != null) {
            runOnUIThread {
                listener.onQrDataDetected(result)
            }
        }
    }

    fun processImage(image: Mat, isFinalProcessing: Boolean) {
        // do not validate crop size or image illumination until all 4 templates have been matched
        Log.d("process_image", "Processing Image")
        detectQrCode(image)
        if (templatesMatchForImage(image, isFinalProcessing)) {
            val finalBitmapImage = Bitmap.createBitmap(
                image.cols(),
                image.rows(),
                Bitmap.Config.ARGB_8888
            )

            Utils.matToBitmap(image, finalBitmapImage)

            val intArray =
                IntArray(finalBitmapImage.width * finalBitmapImage.height)
            // copy pixel data from the Bitmap into the 'intArray' array
            finalBitmapImage.getPixels(
                intArray,
                0,
                finalBitmapImage.width,
                0,
                0,
                finalBitmapImage.width,
                finalBitmapImage.height
            )

            val source: LuminanceSource =
                RGBLuminanceSource(
                    finalBitmapImage.width,
                    finalBitmapImage.height,
                    intArray
                )

            val hybridBinarizer = HybridBinarizer(source)
            val bitmap = BinaryBitmap(hybridBinarizer)

            val result = try {
                MultiFormatReader().decode(bitmap)
            } catch (e: NotFoundException) {
                Log.d("TAG", "Code Not Found")
                e.printStackTrace()
                null
            }
            val resultPoints = try {
                WhiteRectangleDetector(hybridBinarizer.blackMatrix).detect()
            } catch (e: NotFoundException) {
                Log.d("TAG", "Code Not Found")
                e.printStackTrace()
                null
            }

            if (result != null || isFinalProcessing) {
                Log.e("test", "result")

                runOnUIThread {
                    listener.onAllQualifyingImagesCreated(
                        finalBitmapImage,
                        result,
                        foundTemplateMatches,
                        CROP_HEIGHT / image.height(),
                        isFinalProcessing,
                        resultPoints.orEmpty().toList()
                    )
                }
            }
        } else if (isFinalProcessing) {
            listener.onMoveCameraErrorAfterScan()
        }
    }

    private fun getPredictionWithLabel(
        predictions: List<ObjectDetectionHelper.ObjectPrediction>,
        label: String
    ): ObjectDetectionHelper.ObjectPrediction? {
        val candidatePrediction = predictions.maxByOrNull {
            (if (it.label == label) 1.0 else 0.0) * it.score
        } ?: return null

        return if (candidatePrediction.label == label) {
            candidatePrediction
        } else {
            null
        }
    }

    private fun mapLocationToBufferCoords(
        location: RectF,
        bufferWidth: Double,
        bufferHeight: Double
    ): Rect {
        return Rect(
            (location.left * bufferWidth).roundToInt(),
            (location.top * bufferHeight).roundToInt(),
            ((location.right - location.left) * bufferWidth).roundToInt(),
            ((location.bottom - location.top) * bufferHeight).roundToInt()
        )
    }

    private fun templatesMatchForImage(image: Mat, isFinalProcessing: Boolean): Boolean {
        foundTemplateMatches.clear()
        foundTemplateMatchesRaw.clear()

        // store original
        val originalImage = Mat()
        cvtColor(image, originalImage, COLOR_BGRA2RGB)

        val tfBitmapImage = Bitmap.createBitmap(
            image.cols(),
            image.rows(),
            Bitmap.Config.ARGB_8888
        )
        Utils.matToBitmap(image, tfBitmapImage)
        Log.d("tflite_processing", "Pre-Processing bitmap to tfImage")
        val loadedImage = tfImageBuffer.apply {
            load(tfBitmapImage)
        }
        val tfImage = detectionBundle.processor.process(loadedImage)

        Log.d("tflite_processing", "Processing tfImage")
        val predictions = detectionBundle.detector.predict(tfImage)

        val predictionCard = getPredictionWithLabel(predictions, "card_good")
        val predictionUl = getPredictionWithLabel(predictions, "fiducial_ul")
        val predictionUr = getPredictionWithLabel(predictions, "fiducial_ur")
        val predictionLl = getPredictionWithLabel(predictions, "fiducial_ll")
        val predictionLr = getPredictionWithLabel(predictions, "fiducial_lr")

        val scoreThresholdCard = 0.25
        val scoreThresholdFiducial = 0.05
        val hasCardPrediction =
            (predictionCard != null) && (predictionCard.score > scoreThresholdCard)
        val hasUlPrediction =
            (predictionUl != null) && (predictionUl.score > scoreThresholdFiducial) && hasCardPrediction
        val hasUrPrediction =
            (predictionUr != null) && (predictionUr.score > scoreThresholdFiducial) && hasCardPrediction
        val hasLlPrediction =
            (predictionLl != null) && (predictionLl.score > scoreThresholdFiducial) && hasCardPrediction
        val hasLrPrediction =
            (predictionLr != null) && (predictionLr.score > scoreThresholdFiducial) && hasCardPrediction

        Log.d(
            "tflite_processing",
            "Found: " +
                " Ul: " + hasUlPrediction.toString() +
                " (" + predictionUl?.location.toString() + ")" +
                ", Ur: " + hasUrPrediction.toString() +
                " (" + predictionUr?.location.toString() + ")" +
                ", Ll: " + hasLlPrediction.toString() +
                " (" + predictionLl?.location.toString() + ")" +
                ", Lr: " + hasLrPrediction.toString() +
                " (" + predictionLr?.location.toString() + ")"
        )

        val detectedFiducails = mutableListOf<MatchTemplateLocation>()
        // convert image

        if (hasUlPrediction) {
            foundTemplateMatches.add(
                mapLocationToBufferCoords(
                    predictionUl!!.location,
                    CROP_WIDTH,
                    CROP_HEIGHT
                )
            )
            foundTemplateMatchesRaw.add(predictionUl.location)
            detectedFiducails.add(MatchTemplateLocation.TOP_LEFT)
        }

        if (hasUrPrediction) {
            foundTemplateMatches.add(
                mapLocationToBufferCoords(
                    predictionUr!!.location,
                    CROP_WIDTH,
                    CROP_HEIGHT
                )
            )
            foundTemplateMatchesRaw.add(predictionUr.location)
            detectedFiducails.add(MatchTemplateLocation.TOP_RIGHT)
        }

        if (hasLlPrediction) {
            foundTemplateMatches.add(
                mapLocationToBufferCoords(
                    predictionLl!!.location,
                    CROP_WIDTH,
                    CROP_HEIGHT
                )
            )
            foundTemplateMatchesRaw.add(predictionLl.location)
            detectedFiducails.add(MatchTemplateLocation.BOTTOM_LEFT)
        }

        if (hasLrPrediction) {
            foundTemplateMatches.add(
                mapLocationToBufferCoords(
                    predictionLr!!.location,
                    CROP_WIDTH,
                    CROP_HEIGHT
                )
            )
            foundTemplateMatchesRaw.add(predictionLr.location)
            detectedFiducails.add(MatchTemplateLocation.BOTTOM_RIGHT)
        }
        if (!isFinalProcessing) {
            runOnUIThread {
                listener.onFiducialsDetected(detectedFiducails)
            }
        }

        var numFiducialsDetectedThreshold = Constants.FIDUCIALS_COUNT
        val numMissingFiducialsAllowedOnFinal = 1
        if (isFinalProcessing) {
            numFiducialsDetectedThreshold =
                Constants.FIDUCIALS_COUNT - numMissingFiducialsAllowedOnFinal
        }

        var allFoundInStableSequence = false
        if (foundTemplateMatches.size >= numFiducialsDetectedThreshold) {
            consecutiveAllFiducialDetected += 1
            allFoundInStableSequence = consecutiveAllFiducialDetected > 0
        } else {
            consecutiveAllFiducialDetected = 0
        }
        return allFoundInStableSequence
    }

    enum class MatchTemplateLocation {
        TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT
    }

    private fun runOnUIThread(closure: () -> Unit) {
        Handler(Looper.getMainLooper()).post(closure)
    }
}
