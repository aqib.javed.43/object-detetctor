package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class CompleteProgramPayload(
    val action: String,
    val notes: String?,
)
