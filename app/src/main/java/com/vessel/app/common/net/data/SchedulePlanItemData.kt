package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SchedulePlanItemData(
    val plan_id: Int,
    val title: String,
    val time_of_day: String?,
    val completed: Boolean?
)
