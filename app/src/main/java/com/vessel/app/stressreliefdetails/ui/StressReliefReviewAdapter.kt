package com.vessel.app.stressreliefdetails.ui

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.activityreview.ui.ActivityReviewAdapter
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.stressreliefdetails.model.StressReliefReviewItem

class StressReliefReviewAdapter(private val handler: ActivityReviewAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<StressReliefReviewItem>() {
    override fun getLayout(position: Int) = R.layout.layout_review

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<StressReliefReviewItem>, position: Int) {
        val item = getItem(position)
        holder.itemView.findViewById<View>(R.id.btnWriteReview).setOnClickListener {
            handler.onWriteNewReview()
        }
        holder.itemView.findViewById<TextView>(R.id.memberReviewTitle).let {
            it.text = item.title
        }
        val activityReviewAdapter = ActivityReviewAdapter(handler)
        holder.itemView.findViewById<RecyclerView>(R.id.reviewList).apply {
            adapter = activityReviewAdapter
            setHasFixedSize(true)
        }
        activityReviewAdapter.submitList(item.items)
        super.onBindViewHolder(holder, position)
    }
}
