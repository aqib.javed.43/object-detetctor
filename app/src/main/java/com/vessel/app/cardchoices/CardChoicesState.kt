package com.vessel.app.cardchoices

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.cardchoices.model.BOUGHT
import com.vessel.app.cardchoices.model.GIFTED
import com.vessel.app.cardchoices.model.NO_CARD
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class CardChoicesState @Inject constructor(val email: String) {
    val navigateTo = LiveEvent<NavDirections>()
    val choices = MutableLiveData(
        listOf(
            GIFTED(),
            BOUGHT(),
            NO_CARD(),
        )
    )

    operator fun invoke(block: CardChoicesState.() -> Unit) = apply(block)
}
