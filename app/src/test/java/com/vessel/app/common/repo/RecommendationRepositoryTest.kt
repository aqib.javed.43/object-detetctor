// TODO fix the unit test

// package com.vessel.app.common.repo
//
// import android.util.Log
// import com.squareup.moshi.Moshi
// import com.vessel.app.*
// import com.vessel.app.BaseFaker.Fake.FAKER
// import com.vessel.app.common.manager.LoggingManager
// import com.vessel.app.common.model.*
// import com.vessel.app.common.net.mapper.FoodRecommendationMapper
// import com.vessel.app.common.net.mapper.RecommendationMapper
// import com.vessel.app.common.net.service.RecommendationService
// import com.vessel.app.recommendation.FoodRecommendationFaker
// import com.vessel.app.recommendation.SampleRecommendationsFaker
// import io.mockk.*
// import org.junit.jupiter.api.BeforeEach
// import org.junit.jupiter.api.Test
// import retrofit2.HttpException
// import java.io.IOException
//
// class RecommendationRepositoryTest : BaseTest() {
//    private lateinit var recommendationRepository: RecommendationRepository
//
//    private val moshi = mockk<Moshi>()
//    private val loggingManager = mockk<LoggingManager>(relaxed = true)
//    private val recommendationMapper = mockk<RecommendationMapper>()
//    private val recommendationService = mockk<RecommendationService>()
//    private val foodRecommendationMapper = mockk<FoodRecommendationMapper>()
//
//    @BeforeEach
//    fun setUp() {
//        mockkStatic(Log::class)
//        every { Log.e(any(), any()) } returns FAKER.number().randomDigit()
//
//        recommendationRepository = RecommendationRepository(
//            moshi,
//            loggingManager,
//            recommendationMapper,
//            recommendationService,
//            foodRecommendationMapper
//        )
//    }
//
//    @Test
//    fun `it should get the sample recommendations from the service and map it to the domain data`() =
//        runBlockingTest {
//            val data = SampleRecommendationsFaker.data()
//            coEvery { recommendationService.getLatestSampleRecommendations() } returns data
//            val mappedData = SampleRecommendationsFaker.basic()
//            every { recommendationMapper.map(data) } returns mappedData
//
//            (recommendationRepository.getLatestSampleRecommendations() as Result.Success<SampleRecommendations>).value isEqualTo mappedData
//        }
//
//    @Test
//    fun `it should log the error when the service returns an HttpException`() = runBlockingTest {
//        val exception = HttpException(RetrofitResponseFaker.basic())
//        coEvery { recommendationService.getLatestSampleRecommendations() } throws exception
//
//        (recommendationRepository.getLatestSampleRecommendations() as Result.GenericError).code isEqualTo exception.code()
//        verify { loggingManager.addToLog(any()) }
//        verify { loggingManager.logError(exception) }
//    }
//
//    @Test
//    fun `it should return a NetworkError when the service returns an IOException`() =
//        runBlockingTest {
//            val throwable = IOException()
//            coEvery { recommendationService.getLatestSampleRecommendations() } throws throwable
//
//            (recommendationRepository.getLatestSampleRecommendations() as Result.NetworkError).throwable isEqualTo throwable
//            verify { loggingManager wasNot Called }
//        }
//
//    @Test
//    fun `it should return an UnknownError when the service fails`() = runBlockingTest {
//        val throwable = Throwable()
//        coEvery { recommendationService.getLatestSampleRecommendations() } throws throwable
//
//        (recommendationRepository.getLatestSampleRecommendations() as Result.UnknownError).throwable isEqualTo throwable
//        verify { loggingManager.addToLog(any()) }
//        verify { loggingManager.logError(throwable) }
//    }
//
//    @Test
//    fun `it should get the food recommendations from the service and map it to the domain data`() =
//        runBlockingTest {
//            val data = FoodRecommendationFaker.dataList()
//            coEvery { recommendationService.getLatestSampleFoodRecommendations() } returns data
//            val mappedData = FoodRecommendationFaker.list()
//            every { foodRecommendationMapper.map(data) } returns mappedData
//
//            (recommendationRepository.getLatestSampleFoodRecommendations() as Result.Success<List<FoodRecommendation>>).value isEqualTo mappedData
//        }
//
//    @Test
//    fun `it should log the error when the service returns an HttpException while getting the food recs`() =
//        runBlockingTest {
//            val exception = HttpException(RetrofitResponseFaker.basic())
//            coEvery { recommendationService.getLatestSampleFoodRecommendations() } throws exception
//
//            (recommendationRepository.getLatestSampleFoodRecommendations() as Result.GenericError).code isEqualTo exception.code()
//            verify { loggingManager.addToLog(any()) }
//            verify { loggingManager.logError(exception) }
//        }
//
//    @Test
//    fun `it should return a NetworkError when the service returns an IOException while getting the food recs`() =
//        runBlockingTest {
//            val throwable = IOException()
//            coEvery { recommendationService.getLatestSampleFoodRecommendations() } throws throwable
//
//            (recommendationRepository.getLatestSampleFoodRecommendations() as Result.NetworkError).throwable isEqualTo throwable
//            verify { loggingManager wasNot Called }
//        }
//
//    @Test
//    fun `it should return an UnknownError when the service fails while getting the food recs`() =
//        runBlockingTest {
//            val throwable = Throwable()
//            coEvery { recommendationService.getLatestSampleFoodRecommendations() } throws throwable
//
//            (recommendationRepository.getLatestSampleFoodRecommendations() as Result.UnknownError).throwable isEqualTo throwable
//            verify { loggingManager.addToLog(any()) }
//            verify { loggingManager.logError(throwable) }
//        }
// }
