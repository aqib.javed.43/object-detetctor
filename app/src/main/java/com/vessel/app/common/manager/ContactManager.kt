package com.vessel.app.common.manager

import android.icu.util.TimeZone
import android.util.Log
import com.vessel.app.common.model.Contact
import com.vessel.app.common.model.Result.*
import com.vessel.app.common.repo.ContactRepository
import com.vessel.app.common.repo.PreferencesRepository
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.asFlow
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactManager @Inject constructor(
    private val contactRepository: ContactRepository,
    private val preferencesRepository: PreferencesRepository,
    private val chatManager: ChatManager
) {
    private lateinit var authManager: AuthManager

    private val dietAllergiesDataStateChannel = BroadcastChannel<Boolean>(1)

    fun setAuthManager(authManager: AuthManager) {
        this.authManager = authManager
    }

    suspend fun createContact(
        email: String,
        password: String,
        firstName: String,
        lastName: String,
        gender: Contact.Gender? = null,
        height: Contact.Height? = null,
        weight: Contact.Weight? = null,
        birthDate: Date? = null
    ) = contactRepository.createContact(
        email,
        password,
        firstName,
        lastName,
        gender,
        height,
        weight,
        birthDate
    ).let {
        when (it) {
            is Success -> {
                authManager.saveAuthToken(it.value)
                getContact()
            }
            is ServiceError -> it
            is HttpError -> it
            is NetworkIOError -> it
            is UnknownError -> it
        }
    }

    suspend fun checkEmailExistence(email: String) = contactRepository.checkEmailExistence(email)

    suspend fun updateContactTimezone() = contactRepository.updateContactTimezone(
        preferencesRepository.userFirstName.orEmpty(),
        preferencesRepository.userLastName.orEmpty(),
        preferencesRepository.userGender,
        Contact.Height.fromCentimeters(preferencesRepository.userHeight),
        Contact.Weight.fromKg(preferencesRepository.userWeight),
        ContactRepository.SERVER_DATE_FORMAT.parse(
            preferencesRepository.userBirthDate
                ?: ContactRepository.SERVER_DATE_FORMAT.format(Date())
        ),
        preferencesRepository.getUserChosenGoals().map { it.id },
        TimeZone.getDefault().id,
        preferencesRepository.getUserMainGoalId()
    )

    suspend fun updateContact(
        firstName: String = preferencesRepository.userFirstName.orEmpty(),
        lastName: String = preferencesRepository.userLastName.orEmpty(),
        gender: Contact.Gender,
        height: Contact.Height?,
        weight: Contact.Weight?,
        birthDate: Date?,
        goals: List<Int> = preferencesRepository.getUserChosenGoals().map {
            it.id
        },
        mainGoalId: Int? = preferencesRepository.getUserMainGoalId()
    ) = contactRepository.updateContact(
        firstName,
        lastName,
        gender,
        height,
        weight,
        birthDate,
        goals,
        mainGoalId
    )
        .onSuccess {
            preferencesRepository.setContact(it)
        }

    suspend fun updateGoals(
        goals: List<Int> = preferencesRepository.getUserChosenGoals().map {
            it.id
        }
    ) = contactRepository.updateContact(
        firstName = preferencesRepository.userFirstName!!,
        lastName = preferencesRepository.userLastName!!,
        gender = preferencesRepository.userGender ?: Contact.Gender.MALE,
        height = Contact.Height.fromCentimeters(preferencesRepository.userHeight),
        weight = Contact.Weight.fromKg(preferencesRepository.userWeight),
        birthDate = ContactRepository.SERVER_DATE_FORMAT.parse(
            preferencesRepository.userBirthDate ?: ""
        ),
        goals,
        mainGoalId = preferencesRepository.getUserMainGoalId()
    )
        .onSuccess {
            preferencesRepository.setContact(it)
        }

    suspend fun updateMainGoal(
        mainGoalId: Int? = preferencesRepository.getUserMainGoalId()
    ) = contactRepository.updateContact(
        firstName = preferencesRepository.userFirstName!!,
        lastName = preferencesRepository.userLastName!!,
        gender = preferencesRepository.userGender ?: Contact.Gender.MALE,
        height = Contact.Height.fromCentimeters(preferencesRepository.userHeight),
        weight = Contact.Weight.fromKg(preferencesRepository.userWeight),
        birthDate = ContactRepository.SERVER_DATE_FORMAT.parse(
            preferencesRepository.userBirthDate ?: ""
        ),
        preferencesRepository.getUserChosenGoals().map {
            it.id
        },
        mainGoalId
    )
        .onSuccess {
            preferencesRepository.setContact(it)
        }

    suspend fun getContact() = contactRepository.getContact()
        .onSuccess {
            Log.d("ContactManager", "Contact info received: $it")
            preferencesRepository.setContact(it)
            chatManager.setUserInfo()
            // until there is a dedicated flag, use the gender field to determine if onboarding was already completed
            if (it.gender != null) {
                Log.d("ContactManager", "Gender detected, setting onboardingCompleted to true")
                preferencesRepository.onboardingCompleted = true
            }
            Log.d("ContactManager", "Gender is null, onboardingCompleted=$preferencesRepository.onboardingCompleted")
        }
        .onError {
            preferencesRepository.getContact()
        }

    suspend fun getContactById(id: Int?) = contactRepository.getContactById(id)

    suspend fun changePassword(currentPassword: String, newPassword: String) =
        contactRepository.changePassword(currentPassword, newPassword)

    suspend fun addDiet(diets: List<String>) = contactRepository.addDiet(diets)

    suspend fun addAllergies(allergies: List<String>) = contactRepository.addAllergies(allergies)

    suspend fun deleteDiet(diet: List<String>) = contactRepository.deleteDiet(diet)

    suspend fun deleteAllergies(allergies: List<String>) =
        contactRepository.deleteAllergies(allergies)

    fun getDietAllergiesState() = dietAllergiesDataStateChannel.asFlow()

    fun updateDietAllergiesState() {
        dietAllergiesDataStateChannel.offer(true)
    }

    fun clearDietAllergiesState() {
        dietAllergiesDataStateChannel.offer(false)
    }
}
