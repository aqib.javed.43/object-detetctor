package com.vessel.app.welcome.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.viewModels
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.daimajia.androidanimations.library.YoYo.with
import com.tapadoo.alerter.Alerter
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.model.Goal
import com.vessel.app.util.BugseeUtils
import com.vessel.app.util.extensions.observe
import com.vessel.app.welcome.WelcomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WelcomeFragment : BaseFragment<WelcomeViewModel>() {
    override val viewModel: WelcomeViewModel by viewModels()
    override val layoutResId = R.layout.fragment_welcome
    private val handler = Handler(Looper.getMainLooper())
    private lateinit var runnable: Runnable
    private lateinit var animation: YoYo.YoYoString
    private val subtitle: AppCompatTextView by lazy { requireView().findViewById(R.id.subtitle) }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
        produceGoal()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        animation.stop()
        handler.removeCallbacks(runnable)
    }

    private fun setupObservers() {
        viewModel.state {
            observe(showBecomeTester) {
                Alerter.create(requireActivity())
                    .setTitle(R.string.you_are_now_a_tester)
                    .setText(R.string.tester_message)
                    .setBackgroundColorRes(R.color.colorAccent)
                    .enableSound(true)
                    .enableInfiniteDuration(true)
                    .setDismissable(false)
                    .disableOutsideTouch()
                    .addButton(
                        text = getString(R.string.ok),
                        onClick = {
                            Alerter.hide()
                            BugseeUtils.launch(requireActivity().application)
                            requireActivity().recreate()
                        }
                    )
                    .show()
            }
        }
    }

    private fun doAnimationOnSubTitle() {
        animation = with(Techniques.SlideInDown).onStart {
            with(Techniques.FadeInDown).duration(1000).playOn(subtitle)
        }.onEnd {
            with(Techniques.SlideOutDown).onStart {
                with(Techniques.FadeOutDown).duration(1000).playOn(subtitle)
            }
                .duration(1000)
                .pivotY(YoYo.CENTER_PIVOT)
                .playOn(subtitle)
        }
            .duration(1000)
            .pivotY(YoYo.CENTER_PIVOT)
            .playOn(subtitle)
    }

    private fun produceGoal() {
        var i = 0
        runnable = object : Runnable {
            override fun run() {
                subtitle.text = getString(Goal.values()[i].title)
                doAnimationOnSubTitle()
                i++
                if (i == Goal.values().size) {
                    i = 0
                }
                handler.postDelayed(this, (2000).toLong())
            }
        }
        handler.post(runnable)
    }
}
