package com.vessel.app.wellness.tipdetailspopup

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.common.adapter.impactgoals.ImpactGoalModel
import com.vessel.app.common.model.Goal
import com.vessel.app.util.extensions.observe
import com.vessel.app.views.ExpandableTextViewGroup
import com.vessel.app.views.setImpactsGoals
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TipDetailsDialogFragment : BaseDialogFragment<TipDetailDialogViewModel>() {
    override val viewModel: TipDetailDialogViewModel by viewModels()
    override val layoutResId = R.layout.fragment_tip_details_dialog
    private val aboutValue: ExpandableTextViewGroup by lazy { requireView().findViewById(R.id.aboutValue) }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
        isCancelable = false
        setupObservers()
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(tip) { tip ->
                val goals = tip.impactsGoals.orEmpty().mapIndexed { index, goal ->
                    ImpactGoalModel(
                        goal.name,
                        goal.image_small_url,
                        Goal.alternateBackground(index),
                    )
                }
                aboutValue.setImpactsGoals(goals)
            }
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(dismiss) {
                dismiss()
            }
        }
    }
}
