package com.vessel.app.score.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ConcatAdapter
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.score.ScoreViewModel
import com.vessel.app.score.model.ScoreGoalHeaderItem
import com.vessel.app.score.model.ScoreHeaderItem
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_score.*

@AndroidEntryPoint
class ScoreFragment : BaseFragment<ScoreViewModel>() {
    override val viewModel: ScoreViewModel by viewModels()
    override val layoutResId = R.layout.fragment_score

    private val scoreHeaderAdapter by lazy { ScoreHeaderAdapter() }
    private val scoreWellnessAdapter by lazy { ScoreWellnessAdapter(viewModel) }
    private val scoreGoalHeaderAdapter by lazy { ScoreGoalHeaderAdapter(viewModel) }
    private val scoreGoalAdapter = ScoreGoalAdapter()

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupList()
        setupObservers()
    }

    private fun setupList() {
        scoreList.adapter = ConcatAdapter(
            ConcatAdapter.Config.Builder()
                .setIsolateViewTypes(false)
                .build(),
            scoreHeaderAdapter,
            scoreWellnessAdapter,
            scoreGoalHeaderAdapter.apply { submitList(listOf(ScoreGoalHeaderItem(R.string.goals))) },
            scoreGoalAdapter
        )
    }

    private fun setupObservers() {
        viewModel.state {
            observe(scoreWellnessItems) { scoreWellnessAdapter.submitList(it) }
            observe(scoreGoalItems) { scoreGoalAdapter.submitList(it) }
        }
        scoreHeaderAdapter.submitList(listOf(ScoreHeaderItem))
    }
}
