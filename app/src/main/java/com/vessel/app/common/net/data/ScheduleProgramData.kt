package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ScheduleProgramData(
    val day: Int,
    val plan_ids: List<Int>
) : Parcelable
