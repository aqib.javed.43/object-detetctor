package com.vessel.app.foodplan.ui

import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.foodplan.model.FoodDailyItem

class FoodDayAdapter(private val handler: FoodItemsAdapter.OnActionHandler) :
    BaseAdapterWithDiffUtil<FoodDailyItem>() {

    override fun getLayout(position: Int) = R.layout.item_day_food_plan

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(
        holder: BaseViewHolder<FoodDailyItem>,
        position: Int
    ) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position) as FoodDailyItem
        val planAdapter = FoodItemsAdapter(handler, position)
        holder.itemView.findViewById<TextView>(R.id.title).apply {
            text = item.title
            setTextAppearance(if (item.isDailyItem) R.style.Title_SemiBold else R.style.NeoTextRegualr_Small)
        }

        holder.itemView.findViewById<RecyclerView>(R.id.planList).apply {
            adapter = planAdapter
            itemAnimator = null
        }

        planAdapter.submitList(item.items)
    }
}
