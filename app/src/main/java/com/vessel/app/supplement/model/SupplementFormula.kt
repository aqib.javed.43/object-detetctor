package com.vessel.app.supplement.model

import android.os.Parcelable
import androidx.annotation.StringRes
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import com.vessel.app.R
import com.vessel.app.common.model.data.Reagent
import com.vessel.app.common.net.data.plan.GoalRecord
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import kotlin.math.roundToInt

@Parcelize
data class SupplementFormula(
    val nutrient: String?,
    val apiName: String?,
    val unit: String?,
    val isInPlan: Boolean,
    val reagent: Reagent?,
    val quantity: Float,
    val supplementId: Int = 0,
    val description: String? = null,
    val consumptionUnit: String? = null,
    val goals: List<GoalRecord>? = null,
) : Parcelable {
    @IgnoredOnParcel
    val amount = if (nutrient == "Vessel Fuel") "Take Daily" else "${quantity.roundToInt()} ${consumptionUnit ?: (unit ?: "")}"

    @IgnoredOnParcel
    val impacts = buildSpannedString {
        val goalsCount = 2
        append("Impacts: ")
        bold {
            append(goals.orEmpty().take(goalsCount).joinToString { it.name })
            if (goals.orEmpty().size > goalsCount) {
                val remainingGoals = goals.orEmpty().size - goalsCount
                append("+ ")
                append(remainingGoals.toString())
                append(" more")
            }
        }
    }

    @IgnoredOnParcel
    @StringRes
    val buttonText = if (isInPlan) {
        R.string.remove
    } else {
        R.string.add
    }

    @IgnoredOnParcel
    val showDescriptionIcon = description != null
}
