package com.vessel.app.checkin.checkintips

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.common.widget.TipsProgramCardWidget
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TipsCheckInFragment : BaseFragment<TipsCheckInViewModel>() {
    override val viewModel: TipsCheckInViewModel by viewModels()
    override val layoutResId = R.layout.fragment_monthly_checkin_tips
    private lateinit var programsWidget: TipsProgramCardWidget

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        programsWidget = requireView().findViewById(R.id.programsWidget)
        programsWidget.setHandler(viewModel)
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(recommendations) {
                programsWidget.setList(it)
            }
        }
    }
}
