package com.vessel.app.wellness.uploadphoto

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.ViewGroup
import android.webkit.ValueCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.BuildConfig
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_upload_photo_select_dialog.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class UploadPhotoDialogFragment : BaseDialogFragment<UploadPhotoViewModel>() {
    override val viewModel: UploadPhotoViewModel by viewModels()
    override val layoutResId = R.layout.fragment_upload_photo_select_dialog
    private var mFilePathCallback: ValueCallback<Array<Uri?>?>? = null
    private var photoURI: Uri? = null
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        var results: Array<Uri?>? = null
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            // Todo process image data
            val data: Intent? = result.data
            val dataString: String = data?.data?.toString() ?: photoURI.toString()
            results = arrayOf(Uri.parse(dataString))
            mFilePathCallback?.onReceiveValue(results)
            mFilePathCallback = null
            photoURI = null
        }
    }
    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                0
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
        isCancelable = true
        setupObservers()
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        btnFromGallery.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            resultLauncher.launch(intent)
        }
        btnTakePhoto.setOnClickListener {
            openCamera()
        }
    }

    private fun setupObservers() {
        viewModel.state {
            observe(navigateTo) {
                findNavController().navigate(it)
            }
            observe(dismiss) {
                dismiss()
            }
        }
    }
    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
        }
    }

    fun openCamera() {
        context?.let {
            if (mFilePathCallback != null) {
                mFilePathCallback?.onReceiveValue(null)
            }
            var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(
                    it.applicationContext,
                    "${BuildConfig.APPLICATION_ID}.fileprovider", photoFile
                )
                takePictureIntent?.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            } else {
                takePictureIntent = null
            }
            takePictureIntent?.let { intent ->
                resultLauncher.launch(intent)
            }
        }
    }
}
