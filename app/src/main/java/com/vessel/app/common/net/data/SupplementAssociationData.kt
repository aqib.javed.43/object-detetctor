package com.vessel.app.common.net.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class SupplementAssociationData(
    val supplement_id_b: Int?,
    val association_type: String?,
) : Parcelable
