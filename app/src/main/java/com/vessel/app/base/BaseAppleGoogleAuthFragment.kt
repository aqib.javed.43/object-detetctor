package com.vessel.app.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.vessel.app.R
import com.vessel.app.common.model.AuthToken
import com.vessel.app.webview.AppleGoogleAuthWebViewActivity
import com.vessel.app.webview.AppleGoogleAuthWebViewActivity.Companion.INTENT_EXTRA_KEY_SSO_PROVIDER
import com.vessel.app.webview.AppleGoogleAuthWebViewActivity.Companion.INTENT_EXTRA_VAL_SSO_PROVIDER_APPLE
import com.vessel.app.webview.AppleGoogleAuthWebViewActivity.Companion.INTENT_EXTRA_VAL_SSO_PROVIDER_GOOGLE
import com.vessel.app.webview.AppleGoogleAuthWebViewActivity.Companion.RESULT_INTENT_EXTRA_KEY_AUTH_TOKEN

abstract class BaseAppleGoogleAuthFragment<VM : BaseViewModel> : BaseFragment<VM>() {
    protected abstract fun onAuthTokenReceived(authToken: AuthToken)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getAuthTokens = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data = result.data
                val authToken =
                    data?.getSerializableExtra(RESULT_INTENT_EXTRA_KEY_AUTH_TOKEN) as AuthToken
                Log.d("BaseAppleGoogleAuthFragment", "authToken: $authToken")
                if (authToken.accessToken != null &&
                    authToken.refreshToken != null
                ) {
                    onAuthTokenReceived(authToken)
                }
            }
        }
    }

    override fun onViewLoad(savedInstanceState: Bundle?) {
        view?.findViewById<View>(R.id.googleAuthButton)?.setOnClickListener {
            initGoogleWebAuth()
        }

        view?.findViewById<View>(R.id.appleAuthButton)?.setOnClickListener {
            initAppleWebAuth()
        }
    }

    private lateinit var getAuthTokens: ActivityResultLauncher<Intent>

    private fun initGoogleWebAuth() {
        val intent = Intent(requireContext(), AppleGoogleAuthWebViewActivity::class.java)
        intent.putExtra(
            INTENT_EXTRA_KEY_SSO_PROVIDER,
            INTENT_EXTRA_VAL_SSO_PROVIDER_GOOGLE
        )
        getAuthTokens.launch(intent)
    }

    private fun initAppleWebAuth() {
        val intent = Intent(requireContext(), AppleGoogleAuthWebViewActivity::class.java)
        intent.putExtra(
            INTENT_EXTRA_KEY_SSO_PROVIDER,
            INTENT_EXTRA_VAL_SSO_PROVIDER_APPLE
        )
        getAuthTokens.launch(intent)
    }
}
