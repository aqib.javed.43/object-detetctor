package com.vessel.app.appreview.review

import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.NavDirections
import com.vessel.app.appreview.review.model.ReviewItem
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class AppReviewDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider), ReviewChoiceAdapter.OnActionHandler {
    private var reviewItem: ReviewItem? = null
    val dismissDialog = LiveEvent<Unit>()
    var navigateTo = LiveEvent<NavDirections>()
    fun dismiss() {
        devicePreferencesRepository.showAppReviewPopup = false
        dismissDialog.call()
    }
    override fun onItemSelected(item: Any) {
        reviewItem = item as ReviewItem
    }

    fun processFeedBack() {
        reviewItem?.let {
            navigateTo.value = it.action
        }
    }
}
