package com.vessel.app.filterfoodplandialog

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.filterfoodplandialog.model.FilterAndSortDialogOptions
import com.vessel.app.filterfoodplandialog.model.FilterHeaderItem
import com.vessel.app.filterfoodplandialog.model.FilterSelect
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class FoodPlanFilterState @Inject constructor(val selectedFilterAndSortOptions: FilterAndSortDialogOptions) {
    val sortHeaderItem = MutableLiveData<FilterHeaderItem>()
    val sortOptionsList = MutableLiveData<List<FilterSelect>>()
    val foodCategoriesHeader = MutableLiveData<FilterHeaderItem>()
    val foodCategoriesList = MutableLiveData<List<FilterSelect>>()
    val nutrientHeaderItem = MutableLiveData<FilterHeaderItem>()
    val nutrientOptionsList = MutableLiveData<List<FilterSelect>>()
    val dismissDialog = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: FoodPlanFilterState.() -> Unit) = apply(block)
}
