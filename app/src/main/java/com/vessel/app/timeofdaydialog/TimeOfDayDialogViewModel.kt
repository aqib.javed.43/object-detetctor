package com.vessel.app.timeofdaydialog

import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.NavDirections
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.util.LiveEvent
import com.vessel.app.util.ResourceRepository

class TimeOfDayDialogViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceProvider) {
    val dismissDialog = LiveEvent<Unit>()
    val navigateTo = LiveEvent<NavDirections>()

    init {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TIME_WARNING_SHOWN)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
    }

    fun onTakeATestClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.WARNING_TEST_NOW_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        navigateTo.value = if (preferencesRepository.skipPreTestTips)
            HomeNavGraphDirections.globalActionToTakeTest()
        else
            HomeNavGraphDirections.globalActionToPreTest()
        dismissDialog.call()
    }

    fun onDontTakeATestClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.WARNING_CANCEL_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        dismissDialog.call()
    }
}
