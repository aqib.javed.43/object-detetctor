package com.vessel.app.common.adapter.genericlikeordislikeadapter

import androidx.annotation.IdRes
import com.vessel.app.base.BaseAdapterWithDiffUtil

class LikeOrDislikeAdapter<T : Any>(
    val handler: OnActionHandler<T>,
    @IdRes val layoutId: Int
) : BaseAdapterWithDiffUtil<T>() {

    override fun getLayout(position: Int) = layoutId
    override fun getHandler(position: Int) = handler

    interface OnActionHandler<T : Any> {
        fun onLikeClicked(header: T)
        fun onDisLikeClicked(header: T)
    }
}
