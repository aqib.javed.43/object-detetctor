package com.vessel.app.education.ui

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.vessel.app.education.item.EducationItemFragment

class EducationAdapter(
    activity: FragmentActivity,
    val items: List<EducationPage>
) : FragmentStateAdapter(activity) {
    override fun getItemCount() = items.size

    override fun createFragment(position: Int) = EducationItemFragment.newInstance(items[position])
}
