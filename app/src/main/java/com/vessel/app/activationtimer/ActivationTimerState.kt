package com.vessel.app.activationtimer

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.vessel.app.R
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ActivationTimerState @Inject constructor() {
    val showFirstReminderHint = LiveEvent<Any>()
    val navigateToCapture = LiveEvent<Unit>()
    companion object {
        const val PROGRESS_MAX = 10000
        const val FINISH_VALUE = "00"
    }

    var timerStart: String? = null

    val title = MutableLiveData(R.string.activation_timer_title)
    val subtitle = MutableLiveData(R.string.activation_timer_subtitle)
    val countDownCompleted = MutableLiveData(false)
    val showStickyProgress = MutableLiveData(false)
    val minutes = MutableLiveData("03")
    val seconds = MutableLiveData("30")
    val progress = MutableLiveData(0)
    val defaultTab = LiveEvent<Int>()
    val skipButtonVisibility = MutableLiveData(View.VISIBLE)
    val showConfirmSkipAlertEvent = LiveEvent<Unit>()
    val stopTimerEvent = LiveEvent<Unit>()
    val forceCompleteTimerEvent = LiveEvent<Unit>()
    val showNotificationSettingsAlert = LiveEvent<Boolean>()
    val isNotificationSettingsAlertChecked = MutableLiveData(true)
    val stopForegroundTimer = LiveEvent<Unit>()
    var completed = false
        private set

    fun updateTimerText(minutes: String, seconds: String) {
        this.minutes.value = minutes
        this.seconds.value = seconds
    }

    fun setCompleted() {
        updateTimerText(FINISH_VALUE, FINISH_VALUE)
        progress.value = PROGRESS_MAX
        title.value = R.string.activation_timer_complete_title
        subtitle.value = R.string.activation_timer_complete_body
        countDownCompleted.value = true
        skipButtonVisibility.value = View.GONE
        completed = true
    }

    operator fun invoke(block: ActivationTimerState.() -> Unit) = apply(block)
}
