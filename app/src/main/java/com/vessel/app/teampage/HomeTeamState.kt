package com.vessel.app.teampage

import androidx.navigation.NavDirections
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.teampage.model.TeamPostItem
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class HomeTeamState @Inject constructor() {
    val navigateTo = LiveEvent<NavDirections>()
    val goal = LiveEvent<GoalRecord?>()
    val feeds = LiveEvent<List<TeamPostItem>?>()
    operator fun invoke(block: HomeTeamState.() -> Unit) = apply(block)
}
