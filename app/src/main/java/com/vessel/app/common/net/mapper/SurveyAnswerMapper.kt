package com.vessel.app.common.net.mapper

import com.vessel.app.common.net.data.SurveyAnswerWrapperData
import com.vessel.app.takesurvey.model.SurveyAnswer
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SurveyAnswerMapper @Inject constructor() {
    fun map(data: List<SurveyAnswerWrapperData>, questionId: Int) = data.map { map(it, questionId) }
    fun map(data: SurveyAnswerWrapperData, questionId: Int) = SurveyAnswer(
        id = data.answer_id,
        questionId = questionId,
        primaryText = data.answer.primary_text,
        secondaryText = data.answer.secondary_text,
        imageUrl = data.answer.image,
        incorrect = data.answer.is_incorrect ?: false,
        chainedSurveyId = data.answer.chained_survey_id
    )
}
