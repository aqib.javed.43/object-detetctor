package com.vessel.app.onboarding.diets

import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.vessel.app.onboarding.diets.model.OnboardingDietSelect
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class OnboardingDietsState @Inject constructor(
    val isFirstTestFlow: Boolean,
    val isLastStep: Boolean,
) {
    val items = MutableLiveData<List<OnboardingDietSelect>>()
    val refreshItems = LiveEvent<Boolean>()
    val navigateTo = LiveEvent<NavDirections>()

    operator fun invoke(block: OnboardingDietsState.() -> Unit) = apply(block)
}
