package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.wellness.net.data.CreatePollRequest
import com.vessel.app.wellness.net.data.FeedFlag
import com.vessel.app.wellness.net.data.FeedReaction
import com.vessel.app.wellness.net.service.TeamsService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TeamsRepository @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val teamsService: TeamsService,
) : BaseNetworkRepository(moshi, loggingManager) {
    suspend fun fetchNewFeeds(id: Int) = executeWithErrorMessage {
        teamsService.fetchNewFeeds(id)
    }
    suspend fun followContact(id: Int, targetId: String) = executeWithErrorMessage {
        teamsService.followContact(id, targetId)
    }
    suspend fun unFollowContact(id: Int, targetId: String) = executeWithErrorMessage {
        teamsService.unFollowContact(id, targetId)
    }
    suspend fun reactionPost(feedReaction: FeedReaction) = executeWithErrorMessage {
        teamsService.reactionPost(feedReaction)
    }
    suspend fun flagPost(feedFlag: FeedFlag) = executeWithErrorMessage {
        teamsService.flagPost(feedFlag)
    }
    suspend fun createPoll(goalId: Int, createPollRequest: CreatePollRequest) = executeWithErrorMessage {
        teamsService.createPoll(goalId, createPollRequest)
    }
}
