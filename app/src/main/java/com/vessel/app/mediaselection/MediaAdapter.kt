package com.vessel.app.mediaselection

import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.helper.MediaItem
import com.vessel.app.mediaselection.model.Item
import com.vessel.app.views.CheckView
import com.vessel.app.views.MediaGrid
import kotlinx.android.synthetic.main.item_media_grid.view.*

class MediaAdapter(private val handler: OnActionHandler) : BaseAdapterWithDiffUtil<Item>() {
    override fun getLayout(position: Int) = R.layout.item_media_grid

    override fun getHandler(position: Int): Any = handler
    var selectedItems: ArrayList<Item> = arrayListOf()
    interface OnActionHandler {
        fun onSelect(
            item: Any,
            parentPosition: Int,
            isSelected: Boolean,
            selectedItems: ArrayList<Item>
        )
        fun onCapture()
    }

    override fun submitList(list: MutableList<Item>?, commitCallback: Runnable?) {
        super.submitList(list, commitCallback)
        selectedItems.clear()
    }
    override fun onBindViewHolder(holder: BaseViewHolder<Item>, position: Int) {
        val item = getItem(position)

        holder.itemView.mediaView.preBindMedia(
            MediaGrid.PreBindInfo(
                100,
                AppCompatResources.getDrawable(holder.itemView.context, R.drawable.img_capture)!!,
                false,
                holder
            )
        )
        holder.itemView.mediaView.bindMedia(item.mediaItem)
        holder.itemView.mediaView.setOnMediaGridClickListener(object : MediaGrid.OnMediaGridClickListener {
            override fun onThumbnailClicked(
                thumbnail: ImageView?,
                mediaItem: MediaItem?,
                hd: RecyclerView.ViewHolder?
            ) {
                if (holder.bindingAdapterPosition == 0) {
                    handler.onCapture()
                } else {
                    item.isChecked = !item.isChecked
                    holder.itemView.mediaView.mCheckView?.setChecked(item.isChecked)
                    holder.itemView.mediaView.mCheckView?.isVisible = item.isChecked
                    if (item.isChecked)selectedItems.add(item)
                    else selectedItems.remove(item)
                    handler.onSelect(item, holder.bindingAdapterPosition, item.isChecked, selectedItems)
                }
            }

            override fun onCheckViewClicked(
                checkView: CheckView?,
                mediaItem: MediaItem?,
                hd: RecyclerView.ViewHolder?
            ) {
                // Todo nothing
            }
        })
    }
}
