package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.net.service.RemindersService
import com.vessel.app.planreminderdialog.model.ReminderItemUIMapper
import com.vessel.app.planreminderdialog.model.ReminderItemUiModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemindersRepository @Inject constructor(
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val remindersService: RemindersService,
    private val reminderItemUIMapper: ReminderItemUIMapper
) : BaseNetworkRepository(moshi, loggingManager) {

    private var reminders: List<ReminderItemUiModel>? = null

    fun clearCachedData() {
        reminders = null
    }
}
