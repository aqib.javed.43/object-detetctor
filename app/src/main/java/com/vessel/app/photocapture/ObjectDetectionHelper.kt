/*
 * Copyright 2021 Vessel Health
 *
 * Other copyrights:
 * Adapted from code from Google, which used the Apache License
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 */

package com.vessel.app.photocapture

import android.graphics.RectF
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.support.image.TensorImage

/**
 * Helper class used to communicate between our app and the TF object detection model
 * tflite: An instantiated Tensorflow Interpreter
 * labels: Read from the labelmap.txt file
 */
class ObjectDetectionHelper(private val tflite: Interpreter, private val labels: List<String>) {

    /** Abstraction object that wraps a prediction output in an easy to parse way
     * location: Rectangle with the coordinates are returned as a fraction of whole width
     * label: Which label the prediction is ("card_good", "fiducial_ul" etc.)
     * score: A number between 0 and 1 representing confidence in the detection
     * */
    data class ObjectPrediction(val location: RectF, val label: String, val score: Float)

    private val locations = arrayOf(Array(OBJECT_COUNT) { FloatArray(4) })
    private val labelIndices = arrayOf(FloatArray(OBJECT_COUNT))
    private val scores = arrayOf(FloatArray(OBJECT_COUNT))

    /*
    A helper map to allow outputs from tensorflow to go into the helper instance attributes
     */
    private val outputBuffer = mapOf(
        0 to locations,
        1 to labelIndices,
        2 to scores,
        3 to FloatArray(1)
    )

    val predictions get() = (0 until OBJECT_COUNT).map {
        ObjectPrediction(

            // The locations are an array of [0, 1] floats for [top, left, bottom, right]
            location = locations[0][it].let {
                RectF(it[1], it[0], it[3], it[2])
            },

            // As opposed to SSD Mobilenet V1 Model
            // Efficientdet_lite0 does not assume a background class
            // while outputClasses correspond to class index from 0 to number_of_classes
            label = labels[0 + labelIndices[0][it].toInt()],

            // Score is a single value of [0, 1]
            score = scores[0][it]
        )
    }

    fun predict(image: TensorImage): List<ObjectPrediction> {
        tflite.runForMultipleInputsOutputs(arrayOf(image.buffer), outputBuffer)
        return predictions
    }

    companion object {
        const val OBJECT_COUNT = 25
    }
}
