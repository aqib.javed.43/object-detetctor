package com.vessel.app.wellness.model

import android.os.Parcelable
import com.vessel.app.common.net.data.Food
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RecommendationsPagingResponse(
    val tips: List<Tip>?,
    val foods: List<Food>?,
    val lifestyle: List<Lifestyle>?,
    val sequenceData: List<RecommendationsSequence>,
    val paging: Paging,
) : Parcelable
