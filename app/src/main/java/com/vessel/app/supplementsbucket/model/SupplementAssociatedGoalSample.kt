package com.vessel.app.supplementsbucket.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SupplementAssociatedGoalSample(
    val id: Int,
    val name: String,
    val impact: Int?
) : Parcelable
