package com.vessel.app.common.widget

import androidx.lifecycle.MutableLiveData

interface BottomNavHandler {
    val resultsSelected: MutableLiveData<Boolean>
    val planSelected: MutableLiveData<Boolean>
    val chatSelected: MutableLiveData<Boolean>
    val moreSelected: MutableLiveData<Boolean>
    val showPlanNotification: MutableLiveData<Boolean>
    val showChatNotification: MutableLiveData<Boolean>
    val planNotificationCount: MutableLiveData<Int>

    fun onBottomNavClicked(item: BottomNavItem)

    enum class BottomNavItem {
        RESULTS, TAKETEST, PLAN, CHAT, MORE
    }
}
