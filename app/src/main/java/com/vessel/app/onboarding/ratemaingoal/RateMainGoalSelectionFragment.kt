package com.vessel.app.onboarding.ratemaingoal

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_on_boarding_rate_goal_selection.*

@AndroidEntryPoint
class RateMainGoalSelectionFragment : BaseFragment<RateMainGoalSelectionViewModel>() {
    override val viewModel: RateMainGoalSelectionViewModel by viewModels()
    override val layoutResId = R.layout.fragment_on_boarding_rate_goal_selection

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    fun setupObservers() {
        viewModel.state {
            observe(navigateTo) { findNavController().navigate(it) }
            observe(goal) {
                lblTitle.text = String.format(getString(R.string.how_was_goal), it.name)
            }
        }
    }
}
