package com.vessel.app.common.adapter.planreminders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class ReminderRowAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<ReminderRowUiModel>() {
    override fun getLayout(position: Int) = R.layout.item_reminder_row

    override fun getHandler(position: Int) = handler

    override fun onBindViewHolder(holder: BaseViewHolder<ReminderRowUiModel>, position: Int) {
        val item = getItem(position)
        val quantityAdapter = object : BaseAdapterWithDiffUtil<String>() {
            override fun getLayout(position: Int) = R.layout.item_reminder_quantity

            override fun onBindViewHolder(holder: BaseViewHolder<String>, position: Int) {
                super.onBindViewHolder(holder, position)
                holder.itemView.findViewById<View>(R.id.quantity).apply {
                    setOnClickListener {
                        this@ReminderRowAdapter.handler.onReminderItemClick(item, it)
                    }
                }
            }
        }
        val timeAdapter = object : BaseAdapterWithDiffUtil<String>() {
            override fun getLayout(position: Int) = R.layout.item_reminder_time

            override fun onBindViewHolder(holder: BaseViewHolder<String>, position: Int) {
                super.onBindViewHolder(holder, position)
                holder.itemView.findViewById<View>(R.id.quantity).apply {
                    setOnClickListener {
                        this@ReminderRowAdapter.handler.onReminderItemClick(item, it)
                    }
                }
            }
        }

        holder.itemView.findViewById<RecyclerView>(R.id.quantity).apply {
            adapter = quantityAdapter
            itemAnimator = null
        }
        holder.itemView.findViewById<RecyclerView>(R.id.time).apply {
            adapter = timeAdapter
            itemAnimator = null
        }
        quantityAdapter.submitList(item.quantites)
        timeAdapter.submitList(item.times)
        super.onBindViewHolder(holder, position)
    }

    interface OnActionHandler {
        fun onReminderItemClick(item: ReminderRowUiModel, view: View)
    }
}
