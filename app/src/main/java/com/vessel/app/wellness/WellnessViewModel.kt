package com.vessel.app.wellness

import android.text.format.DateUtils
import android.util.Log
import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.*
import com.vessel.app.Constants.MIN_REQUIRED_X_POINTS
import com.vessel.app.activationtimer.ui.ActivationTimerFragmentDirections
import com.vessel.app.base.BaseViewModel
import com.vessel.app.buildplan.model.PlanType
import com.vessel.app.common.manager.*
import com.vessel.app.common.model.*
import com.vessel.app.common.model.data.ReagentInfoType
import com.vessel.app.common.model.data.ReagentState
import com.vessel.app.common.net.data.*
import com.vessel.app.common.net.data.plan.GoalRecord
import com.vessel.app.common.net.data.plan.PlanData
import com.vessel.app.common.net.data.plan.PlanRecord
import com.vessel.app.common.net.data.plan.ReagentLifestyleRecommendation
import com.vessel.app.common.net.mapper.ProgramMapper
import com.vessel.app.common.net.mapper.TipMapper
import com.vessel.app.common.repo.*
import com.vessel.app.common.repo.ContactRepository.Companion.SERVER_DATE_FORMAT
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ProgramCardWidgetOnActionHandler
import com.vessel.app.foodplan.model.FoodDetailsItemModel
import com.vessel.app.homescreen.HomeScreenFragmentDirections
import com.vessel.app.onboarding.goals.model.GoalSample
import com.vessel.app.plan.model.PlanItem
import com.vessel.app.planreminderdialog.model.PlanReminderHeader
import com.vessel.app.stressreliefdetails.model.StressReliefDetailsItemModel
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.util.extensions.isSameDay
import com.vessel.app.util.remoteconfig.RemoteConfiguration
import com.vessel.app.views.RecommendationListWidgetOnActionHandler
import com.vessel.app.views.todayactivities.TodayActivitiesWidgetOnActionHandler
import com.vessel.app.wellness.filterpopup.ItemFilterChoice
import com.vessel.app.wellness.filterpopup.ItemFilterChoiceType
import com.vessel.app.wellness.model.*
import com.vessel.app.wellness.model.RecommendationsSequenceType.*
import com.vessel.app.wellness.net.data.LikeCategory
import com.vessel.app.wellness.net.data.LikeStatus
import com.vessel.app.wellness.ui.*
import com.vessel.app.wellnesscoreinfo.model.WellnessScore
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import java.util.*
import kotlin.collections.ArrayList

class WellnessViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val scoreManager: ScoreManager,
    private val goalsRepository: GoalsRepository,
    private val reagentsManager: ReagentsManager,
    private val goalsManager: GoalsManager,
    private val forceUpdateManager: ForceUpdateManager,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    private val assessmentManager: AssessmentManager,
    private val buildPlanManager: BuildPlanManager,
    private val reminderManager: ReminderManager,
    private val membershipManager: MembershipManager,
    private val recommendationManager: RecommendationManager,
    private val planManager: PlanManager,
    private val tipMapper: TipMapper,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val remoteConfiguration: RemoteConfiguration,
    private val programManager: ProgramManager,
    private val supplementsManager: SupplementsManager,
    private val programMapper: ProgramMapper
) : BaseViewModel(resourceProvider),
    GoalButtonAdapter.OnActionHandler,
    ReagentAdapter.OnActionHandler,
    ReagentGridAdapter.OnActionHandler,
    SelectedFilterItemAdapter.OnActionHandler,
    ProgramCardWidgetOnActionHandler,
    RecommendationListWidgetOnActionHandler,
    TodayActivitiesWidgetOnActionHandler {

    private var reagentsList: List<ReagentItem> = arrayListOf()

    // adding an explicit job to handle loading the program,
    // because it doesn't work when the viewModelScope got cancelled by the system.
    private var completableJob: Job? = null
    private var coroutineScope: CoroutineScope? = null
    var today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
    private var enrolledPrograms: List<Program> = arrayListOf()
    private val afterFirstTest = preferencesRepository.afterFirstTest()
    val state = WellnessState(
        FragmentLocationType.fromInt(
            savedStateHandle.get<Int>("fragmentLocationType") ?: 0
        )
    )

    init {
        state.showBottomDisclaimer.value = (state.fragmentLocationType == FragmentLocationType.HOME)
        state.showRateYourGoalHeader.value =
            (state.fragmentLocationType == FragmentLocationType.ACTIVATION_TIMER)
        if (afterFirstTest.not() && state.fragmentLocationType == FragmentLocationType.ACTIVATION_TIMER) {
            state.showLockedGoalsPlaceholder.value = true
        } else {
            showLoading()
            if (state.refreshStateWhenResume) {
                loadScores()
                observeFirstTestScoreDataState()
                observeTestScoreDataState()
                observeScoreDataState()
                observeLoadRecommendation()
                observeTodayPlanCheckChange()
            }
        }
        remoteConfiguration.fetchConfigFromRemote()
    }

    private fun observeTodayPlanCheckChange() {
        viewModelScope.launch {
            planManager.getTodayPlanItemCheckChange().collect {
                if (it) {
                    planManager.clearTodayPlanItemCheckChange()
                    loadTodayActivities(showLoading = false, forceUpdate = true, today = today)
                }
            }
        }
    }

    private fun observeScoreDataState() {
        viewModelScope.launch {
            scoreManager.getScoreDataState().collect {
                if (it) {
                    loadScores(true)
                }
            }
        }
    }

    private fun observeLoadRecommendation() {
        viewModelScope.launch {
            reminderManager.getRemindersState().collect { reminder ->
                if (reminder) {
                    showLoading(false)
                    planManager.getUserPlans(true)
                        .onSuccess { planData ->
                            state.recommendations.value = state.recommendations.value?.map { item ->
                                var plans: List<PlanData>? = null
                                val iRecommendation = when (item.iRecommendation) {
                                    is Food -> {
                                        plans = planData.filter {
                                            it.isFoodPlan()
                                        }.filter {
                                            it.food_id == item.iRecommendation.id
                                        }
                                        val isAddedToPlan = plans.isNotEmpty()
                                        item.iRecommendation.copy(isAddedToPlan = isAddedToPlan)
                                    }
                                    is Lifestyle -> {
                                        plans = planData.filter {
                                            it.isStressPlan()
                                        }.filter {
                                            it.reagent_lifestyle_recommendation_id == item.iRecommendation.id
                                        }
                                        val isAddedToPlan = plans.isNotEmpty()
                                        item.iRecommendation.copy(isAddedToPlan = isAddedToPlan)
                                    }
                                    is Tip -> {
                                        plans = planData.filter {
                                            it.isTip()
                                        }.filter {
                                            it.tip_id == item.iRecommendation.id
                                        }
                                        val isAddedToPlan = plans.isNotEmpty()
                                        item.iRecommendation.copy(isAddedToPlan = isAddedToPlan)
                                    }
                                    else -> item.iRecommendation
                                }
                                item.copy(planData = plans, iRecommendation = iRecommendation)
                            }
                            hideLoading(false)
                        }
                        .onError {
                            hideLoading(false)
                        }
                }
            }
        }
    }

    private fun observeFirstTestScoreDataState() {
        viewModelScope.launch {
            scoreManager.getFirstTestState().collect {
                if (it) {
                    state.refreshStateWhenResume = true
                    scoreManager.clearFirstTestState()
                    loadScores(true)
                }
            }
        }
    }

    private fun observeTestScoreDataState() {
        viewModelScope.launch {
            scoreManager.getTestState().collect {
                if (it) {
                    state.refreshStateWhenResume = true
                    scoreManager.clearTestState()
                    loadScores(true)
                }
            }
        }
    }

    fun loadTags(goalId: Int, rate: String? = null) {
        showLoading()
        viewModelScope.launch {
            assessmentManager.getTags(goalId, rate)
                .onSuccess { tags ->
                    hideLoading()
                    state.selectedTagsList = tags
                    navigateToFilter(tags)
                }
                .onError {
                    hideLoading()
                    navigateToFilter()
                }
        }
    }

    private fun navigateToFilter(tags: List<Tag>? = null) {
        state.navigateTo.value =
            if (state.fragmentLocationType == FragmentLocationType.HOME) {
                HomeNavGraphDirections.globalActionToFilterDialogFragment(
                    state.filterCategories.toTypedArray(),
                    tags?.toTypedArray()
                )
            } else {
                MainNavGraphDirections.globalActionToFilterDialogFragment(
                    state.filterCategories.toTypedArray(),
                    tags?.toTypedArray()
                )
            }
    }

    fun loadRecommendations(goal: GoalButton) {
        state.showLoadingRecommendations.value = true
        state.recommendationsPaging = Paging(currentPage = 1, pageSize = 10)
        val tagIds = state.selectedTagsList?.filter { it.isSelected }?.map { it.id }
        val filterChoice = state.filterSortChoices.value?.firstOrNull()?.itemFilterChoiceType
        viewModelScope.launch {
            assessmentManager.getRecommendations(
                goal.id,
                if (tagIds?.isNotEmpty() == true) tagIds else null,
                goal.getRateApiValue(),
                state.recommendationsPaging.currentPage,
                state.recommendationsPaging.pageSize,
                sortingByLikes = filterChoice == ItemFilterChoiceType.HIGHEST_RATE,
                sortingByNewest = filterChoice == ItemFilterChoiceType.NEWEST
            )
                .onSuccess { response ->
                    viewModelScope.launch {
                        planManager.getUserPlans()
                            .onSuccess { planData ->
                                state.recommendationsPaging = response.paging
//                      show loadMore button only when have a tip list, will revert it later
                                state.recommendationsPaging.showLoadMore =
                                    response.tips?.size == response.paging.pageSize
//                        state.recommendationsPaging.showLoadMore =
//                            response.sequenceData?.size == state.recommendationsPaging.pageSize
                                state.recommendations.value =
                                    buildRecommendationsList(response, planData)
                                state.showLoadingRecommendations.value = false
                            }
                    }
                }
                .onError {
                    state.recommendations.value = listOf()
                    state.showLoadingRecommendations.value = false
                }
        }
    }

    private fun buildRecommendationsList(
        response: RecommendationsPagingResponse,
        planData: List<PlanData>
    ): List<RecommendationItem> {
        val recommendations = mutableListOf<RecommendationItem>()
//      adding tip filter to show tips only, will revert it later
        response.sequenceData.filter { it.type == TIP }.onEach { sequence ->
            var plans: List<PlanData>? = null
            val item = when (sequence.type) {
                FOOD -> {
                    plans = planData.filter { it.isFoodPlan() }.filter { it.food_id == sequence.id }
                    val isAddedToPlan = plans.isNotEmpty()
                    response.foods?.first { it.id == sequence.id }
                        ?.copy(isAddedToPlan = isAddedToPlan)
                }
                LIFESTYLE -> {
                    plans = planData.filter { it.isStressPlan() }
                        .filter { it.reagent_lifestyle_recommendation_id == sequence.id }
                    val isAddedToPlan = plans.isNotEmpty()
                    response.lifestyle?.first { it.id == sequence.id }
                        ?.copy(isAddedToPlan = isAddedToPlan)
                }
                TIP -> {
                    plans = planData.filter { it.isTip() }.filter { it.tip_id == sequence.id }
                    val isAddedToPlan = plans.isNotEmpty()
                    response.tips?.first { it.id == sequence.id }
                        ?.copy(isAddedToPlan = isAddedToPlan)
                }
                NOTHING -> null
            }
            item?.let { recommendations.add(RecommendationItem(plans, it)) }
        }
        return recommendations
    }

    private fun fetchAllGoals() {
        viewModelScope.launch {
            goalsManager.fetchAllGoals()
                .onSuccess { goalsData ->
                    state {
                        val goalList: ArrayList<GoalRecord> = arrayListOf()
                        for (i in 1 until goalsData.size) {
                            val go = goalsData[i]
                            goals.value?.firstOrNull {
                                it.id == go.id
                            }?.let {
                                if (go.id == getUserMainGoalId()) {
                                    goalList.add(0, go)
                                } else {
                                    goalList.add(go)
                                }
                            }
                        }
                        goalRecords.value = goalList
                        val reagentsItems = arrayListOf<ReagentItem>()
                        score?.reagents?.map {
                            Log.d("TAG", "fetchAllGoals: ${it.value} -- ${it.key.displayName}")
                            val goalsImpacts = mutableMapOf<Int, Int>()
                            goalsData.forEach { goalItem ->
                                goalItem.reagents!!.forEach { reagentData ->
                                    if (reagentData.id == it.key.id) {
                                        goalsImpacts[goalItem.id] = reagentData.impact ?: 0
                                    }
                                }
                            }
                            if (it.key.ranges.isNotEmpty()) {
                                val reagentItem = ReagentItem(
                                    id = it.key.id,
                                    title = it.key.displayName,
                                    unit = it.key.unit,
                                    chartEntries = it.value,
                                    lowerBound = it.key.lowerBound,
                                    upperBound = it.key.upperBound,
                                    minY = it.key.minValue,
                                    maxY = it.key.maxValue,
                                    dimmed = false,
                                    ranges = it.key.ranges,
                                    isBetaTesting = it.key.isBeta,
                                    isInverted = it.key.isInverted,
                                    selectedChartEntityPosition = it.value.lastIndex,
                                    consumptionUnit = it.key.consumptionUnit,
                                    info = it.key.info,
                                    state = it.key.state,
                                    goalsImpacts = goalsImpacts,
                                    impact = null
                                )
                                reagentsItems.add(
                                    reagentItem
                                )
                            }
                        }
                        reagentsList = reagentsItems
                        filterReagentsList(reagentsList.first().chartEntries.size - 1)
                    }
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }
    private fun filterReagentsList(pos: Int = 0) {
        Log.d("TAG", "filterReagentsList: $pos-- ${reagentsList.size}")
        state.reagentItems.value = reagentsList.filter {
            it.chartEntries.size > pos && it.chartEntries[pos].y != Constants.REAGENT_SCORE_DEFAULT_NOT_AVAILABLE_VALUE
        }
    }
    private fun loadScores(forceUpdate: Boolean = false) {
        showLoading()
        viewModelScope.launch {
            scoreManager.getWellnessScoresWithAverage(forceUpdate)
                .onSuccess { scoresData ->
                    val score = scoresData.first
                    val averageScore = scoresData.second

                    state {
                        if (score.wellness.isEmpty()) {
                            hideLoading(false)
                            if (state.fragmentLocationType == FragmentLocationType.HOME) {
                                if (navigateToOverview.value == null || navigateToOverview.value == false) {
//                                    navigateToOverview.value = true
                                }
                            }
                            return@state
                        }

                        if (score.reagents.isEmpty() ||
                            score.wellness.isEmpty() ||
                            averageScore.reagents.isEmpty() ||
                            averageScore.wellness.isEmpty()
                        ) {
                            verifyReagents()
                            loadScores(true)
                            return@state
                        }

                        showForceUpdate.value = forceUpdateManager.isRequireUpdate().first

                        if (devicePreferencesRepository.showPlanUpdatedDialog) {
                            trackingManager.log(
                                TrackedEvent.Builder("SHOW APP UPDATE DIALOG")
                                    .addProperty(
                                        "FLAG",
                                        devicePreferencesRepository.showPlanUpdatedDialog.toString()
                                    )
                                    .addProperty("SUCCESS", "TRUE")
                                    .withKlaviyo()
                                    .withFirebase()
                                    .withAmplitude()
                                    .create()
                            )
                            state.showPlanIntroduction.value =
                                Pair(true, preferencesRepository.testCompletedCount)
                        } else {
                            trackingManager.log(
                                TrackedEvent.Builder("SHOW APP UPDATE DIALOG")
                                    .addProperty(
                                        "FLAG",
                                        devicePreferencesRepository.showPlanUpdatedDialog.toString()
                                    )
                                    .addProperty("SUCCESS", "FALSE")
                                    .withKlaviyo()
                                    .withFirebase()
                                    .withAmplitude()
                                    .create()
                            )
                        }

                        this.score = score
                        this.averageScore = averageScore
                        loadAssessments()
                    }
                }
                .onError {
                    trackingManager.log(
                        TrackedEvent.Builder("ERROR GETTING RESULT TAB DATA")
                            .addProperty("ERROR", it.toString())
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    loadScores(true)
                }
        }
    }

    private fun loadAssessments() {
        viewModelScope.launch {
            assessmentManager.getWellnessScores()
                .onSuccess {
                    buildHeaderData(it)
                    fetchAllGoals()
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    private fun verifyReagents() {
        viewModelScope.launch {
            reagentsManager.fetchReagent()
        }
    }

    fun selectScoreBasedOnUUID(uuid: String) {
        viewModelScope.launch {
            state.dataEntry.value = state.score?.wellness?.filter {
                it.uuid == uuid
            }
        }
    }

    private fun buildHeaderData(goalAssessmentList: List<GoalAssessment>) {
        state {
            score?.let { score ->
                val goalsButtonList = mutableListOf<GoalButton>()
                val wellnessGoalButton = GoalButton(
                    id = Goal.Wellness.id,
                    title = Goal.Wellness.title,
                    chartEntries = score.wellness,
                    averageChartEntries = state.averageScore?.wellness.orEmpty(),
                    image = Goal.Wellness.image,
                    selected = true,
                    selectedChartEntityPosition = score.wellness.lastIndex
                )
                if (state.fragmentLocationType == FragmentLocationType.HOME) {
                    goalsButtonList.add(wellnessGoalButton)
                }
                goalsRepository.getGoals()
                    .map { it.key }
                    .forEachIndexed { index, goal ->
                        val entries = getAssessmentDataEntriesBasedOnTheGoal(
                            goalAssessmentList,
                            goal
                        )

                        val isSelected =
                            state.fragmentLocationType == FragmentLocationType.ACTIVATION_TIMER && index == 0
                        goalsButtonList.add(
                            GoalButton(
                                id = goal.id,
                                title = goal.title,
                                chartEntries = entries,
                                averageChartEntries = listOf(DateEntry(0f, 0f, null)),
                                image = goal.image,
                                selected = isSelected,
                                selectedChartEntityPosition = entries.lastIndex
                            )
                        )
                    }
                goals.value = goalsButtonList.toList()
                selectedGoal.value = goalsButtonList.first()
                Constants.GOALBUTTONS = goalsButtonList.toList()
            }
        }
    }

    private fun getAssessmentDataEntriesBasedOnTheGoal(
        goalAssessmentList: List<GoalAssessment>,
        goal: Goal
    ): List<DateEntry> {
        val dataEntryList = mutableListOf<DateEntry>()
        val totalAssessments =
            goalAssessmentList.sumBy { it.assessments.filter { assessment -> assessment.goalId == goal.id }.size }
        var xAxis = maxOf(MIN_REQUIRED_X_POINTS, totalAssessments).toFloat()

        val todayAssessment =
            goalAssessmentList.firstOrNull { DateUtils.isToday(it.createdAt.time) }?.assessments?.firstOrNull { assessment -> assessment.goalId == goal.id }
        if (todayAssessment != null) {
            dataEntryList.add(
                0,
                DateEntry(
                    xAxis--,
                    todayAssessment.value.toFloat(),
                    todayAssessment.createdAt.time,
                    showNumber = false
                )
            )
        } else {
            if (totalAssessments >= MIN_REQUIRED_X_POINTS) {
                xAxis++
            }
            dataEntryList.add(
                0,
                DateEntry(
                    xAxis--,
                    Constants.RATE_IT_POINT,
                    Calendar.getInstance().timeInMillis,
                    showNumber = false
                )
            )
        }
        goalAssessmentList.filterNot { DateUtils.isToday(it.createdAt.time) }
            .sortedByDescending { it.createdAt }
            .onEach { goalAssessment ->
                goalAssessment.assessments.filter { assessment -> assessment.goalId == goal.id }
                    .forEach { assessment ->
                        dataEntryList.add(
                            0,
                            DateEntry(
                                xAxis--,
                                assessment.value.toFloat(),
                                assessment.createdAt.time,
                                showNumber = false
                            )
                        )
                    }
            }

        val unratedCount = MIN_REQUIRED_X_POINTS - dataEntryList.size - 1
        for (i in 0..unratedCount) {
            val calender = Calendar.getInstance()
            calender.timeInMillis = dataEntryList.first().date!!
            calender.add(Calendar.DATE, -1)
            dataEntryList.add(
                0,
                DateEntry(xAxis--, null, calender.timeInMillis, showNumber = false)
            )
        }
        return dataEntryList
    }

    override fun onGoalButtonClick(item: GoalButton) {
        state.refreshStateWhenResume = true
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.GOAL_TAPPED)
                .addProperty(TrackingConstants.KEY_GOAL_NAME, getResString(item.title))
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        if (item.selected) return
        state {
            goals.value = goals.value?.map {
                it.copy(
                    isPreviousSelected = it.selected,
                    selected = it.id == item.id
                )
            }
            selectedGoal.value = item

            reagentItems.value = reagentItems.value?.map {
                it.copy(
                    impact = if (item.id == Goal.Wellness.id) null else it.goalsImpacts?.get(item.id)
                        ?: 0,
                    dimmed = when (item.id) {
                        0 -> false
                        Goal.Wellness.id -> Goal.Wellness.reagents.firstOrNull { reagentItem -> it.id == reagentItem.id } == null
                        else -> Goal.fromId(item.id)?.reagents?.firstOrNull { reagentItem -> it.id == reagentItem.id } == null
                    }
                )
            }?.sortedByDescending { it.impact }

            if (state.selectedGoal.value?.id != Goal.Wellness.id) {
                state.reagentVerticalItems.value = state.reagentItems.value?.filter {
                    it.state != ReagentState.COMING_SOON
                }
            }
            selectedTagsList = null
        }
    }

    fun getProgramsByGoal(goalId: Int) {
        val differentGoal = state.programsItems.value?.firstOrNull()?.mainGoalId != goalId
        state.showProgramsLoading.value = differentGoal
        if (differentGoal) {
            // reset the list
            state.programsItems.value = null
        }
        completableJob = Job()
        coroutineScope = CoroutineScope(Dispatchers.IO + completableJob!!)
        coroutineScope?.launch(Dispatchers.IO) {
            programManager.getProgramsByGoal(goalId = goalId, true)
                .onSuccess {
                    state.programsItems.postValue(it.program)
                    state.showProgramsLoading.postValue(false)
                }
                .onError {
                    state.showProgramsLoading.postValue(false)
                }
        }
    }

    override fun onCleared() {
        super.onCleared()
        completableJob?.cancel()
    }

    override fun onGoalEntryClicked(entryPosition: Int) {
        state.goals.value = state.goals.value?.map {
            val selectedEntryPosition = if (it.id == state.selectedGoal.value?.id) {
                entryPosition
            } else {
                it.chartEntries.lastIndex
            }

            it.copy(
                selectedChartEntityPosition = selectedEntryPosition,
                isProgressAnimatedBefore = false
            )
        }
        state.selectedGoal.value =
            state.selectedGoal.value!!.copy(selectedChartEntityPosition = entryPosition)
        filterReagentsList(entryPosition)

        state.reagentItems.value = state.reagentItems.value.orEmpty().map { reagent ->
            reagent.copy(
                selectedChartEntityPosition = if (entryPosition > reagent.chartEntries.lastIndex) {
                    reagent.chartEntries.lastIndex
                } else {
                    entryPosition
                }
            )
        }

        if (state.selectedGoal.value?.id != Goal.Wellness.id) {
            state.reagentVerticalItems.value = state.reagentItems.value?.filter {
                it.state != ReagentState.COMING_SOON
            }
        }
    }

    override fun onGoalButtonMoved(oldPosition: Int, newPosition: Int) {
        Collections.swap(
            state.goals.value.orEmpty(),
            oldPosition,
            newPosition
        )
        goalsRepository.setGoalsOrder(
            state.goals.value.orEmpty().map { it.id }
        )
    }

    override fun onInfoButtonClick(entryPosition: Int) {
        onWellnessScoreInfoClick(entryPosition)
    }

    fun onGoalsEditClick() {
        // todo, I'm not sure why the graph action doesn't get generated :/
        if (state.fragmentLocationType == FragmentLocationType.HOME) {
            state.navigateTo.value =
                HomeScreenFragmentDirections.actionHomeScreenFragmentResultTabToGoalSelect()
        }
    }

    fun onGoalItemClick(item: GoalButton) {
        // todo, I'm not sure why the graph action doesn't get generated :/
        if (state.fragmentLocationType == FragmentLocationType.HOME) {
            state.navigateTo.value = HomeNavGraphDirections.actionGlobalGoalPageFragment(GoalSample(item.id, getResString(item.title)))
        }
    }

    override fun onReagentItemClick(item: ReagentItem, view: View) {
        // todo, I'm not sure why the graph action doesn't get generated :/
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.REAGENT_TAPPED)
                .addProperty(TrackingConstants.KEY_REAGENT_NAME, item.title)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        if (state.fragmentLocationType == FragmentLocationType.HOME) {
            if (item.state == ReagentState.COMING_SOON) {
                val reagentInfo =
                    item.info?.firstOrNull { it.infoType == ReagentInfoType.COMING_SOON }
                reagentInfo?.let {
                    state.navigateTo.value =
                        HomeScreenFragmentDirections.actionHomeScreenFragmentToComingSoonReagentDialogFragment(
                            it
                        )
                }
            } else {
                state.navigateTo.value =
                    HomeScreenFragmentDirections.actionHomeScreenFragmentResultTabToReagent(
                        com.vessel.app.reagent.model.ReagentHeader(
                            id = item.id,
                            title = item.title,
                            unit = item.unit,
                            chartEntries = item.chartEntries.map {
                                DateEntry(
                                    it.x,
                                    it.y,
                                    it.date,
                                    it.reagentsCount
                                )
                            },
                            lowerBound = item.lowerBound,
                            upperBound = item.upperBound,
                            minY = item.minY,
                            maxY = item.maxY,
                            background = reagentsManager.fromId(item.id)!!.headerImage,
                            ranges = item.ranges,
                            errorDescription = if (item.id == com.vessel.app.common.model.data.ReagentItem.B9.id) getResString(
                                R.string.b9_not_available_message
                            ) else getResString(
                                R.string.reagent_test_results_unavailable,
                                item.title
                            ),
                            isBetaReagent = item.isBetaTesting,
                            betaHint = getResString(
                                R.string.reagent_beta_test_hint,
                                item.title
                            ),
                            consumptionUnit = item.consumptionUnit
                        )
                    )
            }
        }
    }

    override fun onReagentGridItemClick(item: ReagentItem, view: View) {
        // todo, I'm not sure why the graph action doesn't get generated :/
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.REAGENT_TAPPED)
                .addProperty(TrackingConstants.KEY_REAGENT_NAME, item.title)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        if (state.fragmentLocationType == FragmentLocationType.HOME) {
            if (item.state == ReagentState.COMING_SOON) {
                val reagentInfo =
                    item.info?.firstOrNull { it.infoType == ReagentInfoType.COMING_SOON }
                reagentInfo?.let {
                    state.navigateTo.value =
                        HomeScreenFragmentDirections.actionHomeScreenFragmentToComingSoonReagentDialogFragment(
                            it
                        )
                }
            } else {
                navigateToResultTab(item)
            }
        }
    }

    private fun navigateToResultTab(item: ReagentItem) {
        state.navigateTo.value =
            HomeScreenFragmentDirections.actionHomeScreenFragmentResultTabToReagent(
                com.vessel.app.reagent.model.ReagentHeader(
                    id = item.id,
                    title = item.title,
                    unit = item.unit,
                    chartEntries = item.chartEntries.map {
                        DateEntry(
                            it.x,
                            it.y,
                            it.date,
                            it.reagentsCount
                        )
                    },
                    lowerBound = item.lowerBound,
                    upperBound = item.upperBound,
                    minY = item.minY,
                    maxY = item.maxY,
                    background = reagentsManager.fromId(item.id)!!.headerImage,
                    ranges = item.ranges,
                    errorDescription = if (item.id == com.vessel.app.common.model.data.ReagentItem.B9.id) getResString(
                        R.string.b9_not_available_message
                    ) else getResString(
                        R.string.reagent_test_results_unavailable,
                        item.title
                    ),
                    isBetaReagent = item.isBetaTesting,
                    betaHint = getResString(
                        R.string.reagent_beta_test_hint,
                        item.title
                    ),
                    consumptionUnit = item.consumptionUnit
                )
            )
    }

    fun getChatConfiguration() =
        membershipManager.getChatConfiguration(MembershipManager.ZENDESK_NUTRITIONIST_DEPARTMENT)

    fun isLiveChat() = remoteConfiguration.getBoolean("use_livechat")
    fun getUserName() = preferencesRepository.userFirstName
    fun getUserEmail() = preferencesRepository.userEmail

    fun onTalkToNutritionistClick() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.TALK_TO_NUTRITIONIST_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        state.openMessaging.call()
    }

    fun onCustomerSupportClick() {
        state.launchLiveChatEvent.call()
    }

    fun onRateClick(goalRate: GoalRate, goalButton: GoalButton) {
        if (goalRate.value != goalButton.chartEntries[goalButton.selectedChartEntityPosition].y.toInt()) {
            state.refreshStateWhenResume = true
            val today = SERVER_DATE_FORMAT.format(Date())
            val chartEntries = goalButton.chartEntries.mapIndexed { index, dateEntry ->
                if (index == goalButton.selectedChartEntityPosition) {
                    DateEntry(
                        dateEntry.x,
                        goalRate.value.toFloat(),
                        dateEntry.date,
                        dateEntry.reagentsCount,
                        dateEntry.showNumber
                    )
                } else {
                    dateEntry
                }
            }
            state.goals.value = state.goals.value?.map {
                if (it.id == goalButton.id) {
                    it.copy(chartEntries = chartEntries)
                } else {
                    it.copy()
                }
            }
            state.selectedGoal.value = state.selectedGoal.value!!.copy(
                chartEntries = chartEntries
            )
            state.selectedTagsList = null

            viewModelScope.launch {
                assessmentManager.putGoalAssessments(goalButton.id, goalRate.value, today)
            }
        }
    }

    override fun onFilterItemRemoved(item: ItemFilterChoice) {
        val selectedGoal = state.selectedGoal.value!!
        state.filterCategories = state.filterCategories.map {
            val choices = it.choiceItems.map {
                if (it.title == item.title) {
                    it.copy(isChecked = false)
                } else {
                    it.copy()
                }
            }
            it.copy(choiceItems = choices)
        }
        state.filterSortChoices.value = state.getFilterChoices()

        if (selectedGoal.isNotWellness) {
            loadRecommendations(selectedGoal)
        }
    }

    fun onFilterButtonClick() {
//        hiding the tags section for now, and will show it later
//
//        if (state.selectedTagsList.isNullOrEmpty()) {
//            state.selectedGoal.value?.let {
//                loadTags(it.id, it.getRateApiValue())
//            }
//        } else {
//            navigateToFilter(state.selectedTagsList)
//        }
        navigateToFilter(state.selectedTagsList)
    }

    fun updateFilterOptions() {
        val selectedGoal = state.selectedGoal.value!!
        state.filterSortChoices.value = state.getFilterChoices()
        if (selectedGoal.isNotWellness) {
            state.recommendations.value = listOf()
            loadRecommendations(selectedGoal)
        }
    }

    override fun onExpandViewClicked(recommendation: RecommendationItem) {
        // todo add the click handler for the expanded view
    }

    override fun onRecommendationItemClicked(recommendation: RecommendationItem) {
        when (recommendation.iRecommendation) {
            is Tip -> {
                state.navigateTo.value =
                    if (state.fragmentLocationType == FragmentLocationType.HOME) {
                        HomeNavGraphDirections.globalActionToTipDetailsFragment(recommendation.iRecommendation)
                    } else {
                        MainNavGraphDirections.globalActionToTipDetailsDialogFragment(recommendation.iRecommendation)
                    }
            }
            is Food -> {
                state.navigateTo.value =
                    HomeNavGraphDirections.globalActionToFoodPlanDetailsFragment(
                        FoodDetailsItemModel(
                            recommendation.iRecommendation.getDisplayedTitle()!!,
                            recommendation.iRecommendation.getDisplayedFrequency().orEmpty(),
                            recommendation.iRecommendation.id!!,
                            recommendation.iRecommendation.isAddedToPlan
                        )
                    )
            }
            is Lifestyle -> {
                state.navigateTo.value =
                    HomeNavGraphDirections.globalActionToStressReliefDetailsFragment(
                        StressReliefDetailsItemModel(
                            title = recommendation.iRecommendation.getDisplayedTitle(),
                            description = recommendation.iRecommendation.description ?: "",
                            life_style_recommendation_id = recommendation.iRecommendation.lifestyleRecommendationId,
                            isAdded = recommendation.iRecommendation.isAddedToPlan
                        )
                    )
            }
        }
    }

    override fun onRecommendationItemLikeClicked(recommendation: RecommendationItem) {
        var recommendationId = 0
        var category = ""
        var updatedLikeFlag = false
        var likesCount = 0
        when (recommendation.iRecommendation) {
            is Tip -> {
                recommendationId = recommendation.iRecommendation.id
                category = LikeCategory.Tip.value
                likesCount = recommendation.iRecommendation.likesCount ?: 0
                updatedLikeFlag = recommendation.iRecommendation.likeStatus != LikeStatus.LIKE
            }
            is Food -> {
                recommendationId = recommendation.iRecommendation.id!!
                category = LikeCategory.Food.value
                likesCount = recommendation.iRecommendation.total_likes ?: 0
                updatedLikeFlag = recommendation.iRecommendation.likeStatus != LikeStatus.LIKE
            }
            is Lifestyle -> {
                recommendationId = recommendation.iRecommendation.id
                category = LikeCategory.Lifestyle.value
                likesCount = recommendation.iRecommendation.totalLikes ?: 0
                updatedLikeFlag = recommendation.iRecommendation.likeStatus != LikeStatus.LIKE
            }
        }

        if (updatedLikeFlag) likesCount++ else likesCount--
        state.recommendations.value = state.recommendations.value?.map {
            val copiedIRecommendation: IRecommendation = when (it.iRecommendation) {
                is Tip -> {
                    if (it.iRecommendation.getType() == recommendation.iRecommendation.getType() && it.iRecommendation.id == recommendationId) {
                        it.iRecommendation.copy(
                            likesCount = likesCount,
                            likeStatus = if (updatedLikeFlag) LikeStatus.LIKE else LikeStatus.DISLIKE
                        )
                    } else it.iRecommendation.copy()
                }
                is Food -> {
                    if (it.iRecommendation.getType() == recommendation.iRecommendation.getType() && it.iRecommendation.id == recommendationId) {
                        it.iRecommendation.copy(
                            total_likes = likesCount,
                            like_status = if (updatedLikeFlag) LikeStatus.LIKE.value else LikeStatus.DISLIKE.value
                        )
                    } else it.iRecommendation.copy()
                }
                is Lifestyle -> {
                    if (it.iRecommendation.getType() == recommendation.iRecommendation.getType() && it.iRecommendation.id == recommendationId) {
                        it.iRecommendation.copy(
                            totalLikes = likesCount,
                            likeStatus = if (updatedLikeFlag) LikeStatus.LIKE else LikeStatus.DISLIKE
                        )
                    } else it.iRecommendation.copy()
                }
                else -> {
                    it.iRecommendation
                }
            }
            it.copy(iRecommendation = copiedIRecommendation)
        }
        viewModelScope.launch {
            if (updatedLikeFlag) {
                recommendationManager.sendLikesStatus(
                    recommendationId,
                    category,
                    updatedLikeFlag
                )
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.CONTENT_LIKED)
                        .addProperty(TrackingConstants.KEY_ID, recommendationId.toString())
                        .addProperty(TrackingConstants.TYPE, category)
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
            } else {
                recommendationManager.unLikesStatus(recommendationId, category)
                trackingManager.log(
                    TrackedEvent.Builder(TrackingConstants.CONTENT_UNLIKED)
                        .addProperty(TrackingConstants.KEY_ID, recommendationId.toString())
                        .addProperty(TrackingConstants.TYPE, category)
                        .withKlaviyo()
                        .withFirebase()
                        .withAmplitude()
                        .create()
                )
            }
        }
    }

    override fun onRecommendationAddToPlanClicked(recommendation: RecommendationItem) {
        val isAddedToPlan = when (recommendation.iRecommendation) {
            is Food -> recommendation.iRecommendation.isAddedToPlan
            is Lifestyle -> recommendation.iRecommendation.isAddedToPlan
            is Tip -> recommendation.iRecommendation.isAddedToPlan
            else -> false
        }
        if (isAddedToPlan.not()) {
            val plans = if (recommendation.planData.isNullOrEmpty().not()) {
                recommendation.planData!!
            } else {
                listOf(getPlanData(recommendation))
            }
            val planReminderHeader = PlanReminderHeader(
                recommendation.iRecommendation.getDisplayedTitle(),
                recommendation.iRecommendation.getDisplayedFrequency(),
                recommendation.iRecommendation.getBackgroundImageUrl(),
                null,
                plans,
                isAddedToPlan.not()
            )
            state.navigateTo.value = if (state.fragmentLocationType == FragmentLocationType.HOME) {
                HomeNavGraphDirections.globalActionToPlanReminderDialog(planReminderHeader)
            } else {
                MainNavGraphDirections.globalActionToPlanReminderDialog(planReminderHeader)
            }
        } else {
            recommendation.planData?.let { plans ->
                deletePlanItem(plans)
            }
        }
    }

    private fun deletePlanItem(plans: List<PlanData>) {
        showLoading()
        viewModelScope.launch {
            planManager.deletePlanItem(plans.map { it.id ?: -1 })
                .onSuccess {
                    hideLoading(false)
                    planManager.updatePlanCachedData()
                    reminderManager.updateRemindersCachedData()
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PLAN_ITEM_DELETED)
                            .addProperty(
                                TrackingConstants.KEY_ID,
                                plans.map { it.id ?: -1 }.toString()
                            )
                            .addProperty(
                                TrackingConstants.TYPE,
                                plans.first().getPlanType().title
                            )
                            .addProperty(TrackingConstants.KEY_LOCATION, "Result Page")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    private fun getPlanData(item: RecommendationItem): PlanData = when (item.iRecommendation) {
        is Food -> PlanData(
            food_id = item.iRecommendation.id ?: 0,
            food = item.iRecommendation,
            multiple = 1
        )
        is Lifestyle -> PlanData(
            reagent_lifestyle_recommendation_id = item.iRecommendation.lifestyleRecommendationId,
            reagent_lifestyle_recommendation = ReagentLifestyleRecommendation(
                lifestyle_recommendation_id = item.iRecommendation.lifestyleRecommendationId,
                quantity = item.iRecommendation.quantity?.toFloat()!!,
                frequency = item.iRecommendation.frequency,
                unit = item.iRecommendation.unit,
                reagent_bucket_id = item.iRecommendation.reagentBucketId,
                activity_name = item.iRecommendation.activityName,
                image_url = item.iRecommendation.imageUrl,
                id = -1
            ),
            multiple = 1
        )
        is Tip -> PlanData(
            tip_id = item.iRecommendation.id,
            tip = tipMapper.mapTip(item.iRecommendation),
            multiple = 1
        )
        else -> PlanData()
    }

    override fun onLoadMoreClicked(paging: Paging) {
        state.recommendationsPaging = paging
        state.showLoadingRecommendations.value = true
        val selectedTags = state.selectedTagsList?.filter { it.isSelected }?.map { it.id }
        val selectedGoal = state.selectedGoal.value!!
        val filterChoice = state.filterSortChoices.value?.firstOrNull()?.itemFilterChoiceType

        viewModelScope.launch {
            assessmentManager.getRecommendations(
                selectedGoal.id,
                selectedTags,
                selectedGoal.getRateApiValue(),
                state.recommendationsPaging.currentPage,
                state.recommendationsPaging.pageSize,
                sortingByLikes = filterChoice == ItemFilterChoiceType.HIGHEST_RATE,
                sortingByNewest = filterChoice == ItemFilterChoiceType.NEWEST
            )
                .onSuccess { response ->
                    viewModelScope.launch {
                        planManager.getUserPlans()
                            .onSuccess { planData ->
                                state.recommendationsPaging = response.paging
//                      show loadMore button only when have a tip list
                                state.recommendationsPaging.showLoadMore =
                                    response.tips?.size == response.paging.pageSize
//                        state.recommendationsPaging.showLoadMore =
//                            response.sequenceData?.size == state.recommendationsPaging.pageSize
                                state.recommendations.value =
                                    mutableListOf<RecommendationItem>().apply {
                                        addAll(state.recommendations.value!!)
                                        addAll(buildRecommendationsList(response, planData))
                                    }
                                state.showLoadingRecommendations.value = false
                            }
                    }
                }
                .onError {
                    state.showLoadingRecommendations.value = false
                }
        }
    }

    fun onViewYourPlanClicked() {
        buildPlanManager.updateViewPlanState()
    }

    fun onReagentInfoClick() {
        if (state.fragmentLocationType == FragmentLocationType.HOME) {
            state.navigateTo.value =
                HomeScreenFragmentDirections.actionHomeScreenFragmentToImpactScorePopupDialogFragment()
        } else {
            state.navigateTo.value =
                ActivationTimerFragmentDirections.actionToImpactScorePopupDialogFragment()
        }
    }
    private fun onWellnessScoreInfoClick(entryPosition: Int) {
        val wellnessScore = WellnessScore()
        wellnessScore.reagentItems = state.reagentItems.value
        wellnessScore.currentTestScore = state.score?.wellness!![entryPosition].y.toInt()
        Constants.wellnessScore = wellnessScore
        state.navigateTo.value = HomeScreenFragmentDirections.actionGlobalWellnessScoreInfoFragment()
    }
    fun updatePlanHintState() {
        devicePreferencesRepository.showPlanUpdatedDialog = false
    }

    override fun onProgramItemClicked(program: Program) {
        state.navigateTo.value =
            WellnessFragmentDirections.globalActionToProgramDetailsFragment(program)
    }

    override fun onProgramInfoClicked(program: Program) {
        state.navigateTo.value =
            HomeScreenFragmentDirections.actionHomeScreenFragmentToProgramInfoDialogFragment(
                program
            )
    }

    override fun onJoinProgramClicked(program: Program) {
        if (program.isEnrolled.not()) {
            state.navigateTo.value =
                WellnessFragmentDirections.globalActionToJoinProgramFragment(
                    program,
                    Constants.GOAL_TAB
                )
        } else {
            onProgramItemClicked(program)
        }
    }

    private fun goToCompleteProgram(program: Program) {
        state.navigateTo.value =
            WellnessFragmentDirections.globalActionToCompleteProgramStep1Fragment(program)
    }

    private fun loadTodayActivities(
        showLoading: Boolean = true,
        forceUpdate: Boolean = false,
        today: Date
    ) {
        state.todayActivitiesLoading.value = showLoading
        viewModelScope.launch(Dispatchers.IO, CoroutineStart.ATOMIC) {
            planManager.getPlansByDate(forceUpdate, today.formatDayDate())
                .onSuccess { plans ->
                    val unlockedPlan = planManager.getUnlockedPlans()
                    val items: ArrayList<PlanData> = arrayListOf()
                    items.addAll(plans)
                    unlockedPlan.let {
                        items.addAll(it)
                    }
                    val groupedFoodItems = items.filter {
                        it.food_id != null
                    }.groupBy {
                        it.food_id
                    }

                    val groupedLifeStyleItemsItems = items.filter {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id != null
                    }.groupBy {
                        it.reagent_lifestyle_recommendation?.lifestyle_recommendation_id
                    }

                    val groupedSupplementItems = items.filter {
                        it.isSupplementPlan()
                    }

                    val groupedTips = items.filter {
                        it.isTip()
                    }.groupBy {
                        it.tip_id
                    }

                    state.todayActivitiesItems.postValue(
                        buildTodayActivities(
                            groupedFoodItems,
                            groupedLifeStyleItemsItems,
                            groupedSupplementItems,
                            groupedTips,
                            today
                        )
                    )
                    state.todayActivitiesLoading.postValue(false)
                }
                .onError {
                    state.todayActivitiesLoading.postValue(false)
                }
        }
    }

    private fun buildTodayActivities(
        groupedFoodItems: Map<Int?, List<PlanData>>,
        groupedLifeStyleItemsItems: Map<Int?, List<PlanData>>,
        groupedSupplementItems: List<PlanData>,
        groupedTips: Map<Int?, List<PlanData>>,
        today: Date
    ): List<PlanItem> {
        val dailyItems = mutableListOf<PlanItem>()
        groupedFoodItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it,
                        grouped,
                        today.formatDayDate()
                    )
                )
            }
        }
        groupedTips.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it,
                        grouped,
                        today.formatDayDate()
                    )
                )
            }
        }
        groupedLifeStyleItemsItems.values.map { grouped ->
            grouped.forEach {
                dailyItems.add(
                    mapTodoToPlanItem(
                        it,
                        grouped,
                        today.formatDayDate()
                    )
                )
            }
        }
        groupedSupplementItems.map { item ->
            dailyItems.add(
                mapTodoToPlanItem(
                    item,
                    groupedSupplementItems,
                    today.formatDayDate()
                )
            )
        }

        val filteredTodayActivities = dailyItems.filter { planItem ->
            planItem.currentPlan.program_id == null || this.enrolledPrograms.firstOrNull {
                it.id == planItem.currentPlan.program_id
            } != null
        }

        val planItems = togglePlanItems(filteredTodayActivities, today).sortedBy {
            it.currentPlan.getTimeFormattedAsNumber()
        }
        return planItems
    }

    private fun mapTodoToPlanItem(
        todo: PlanData,
        plans: List<PlanData>,
        date: String? = null
    ): PlanItem {
        val isCompleted = todo.plan_records?.firstOrNull { it.date == date }?.completed == true

        return PlanItem(
            todo.id ?: -1,
            todo.getDisplayName(),
            todo.getDescription(),
            todo.getTimeFormattedWithMeridian() ?: getResString(R.string.set_time),
            todo.getToDoImageUrl(),
            todo.getToDoImageBackground(),
            todo.getDisplayName(),
            isCompleted,
            false,
            1,
            plans,
            plans,
            todo.getPlanType(),
            todo.day_of_week,
            todo.program_dates?.map { Constants.DAY_DATE_FORMAT.parse(it) },
            program = todo.program?.let { programMapper.mapProgram(it) },
            todo
        )
    }

    private fun togglePlanItems(
        dayItems: List<PlanItem>,
        formatPlanDate: Date
    ): List<PlanItem> {
        return dayItems.map {
            it.copy(
                isCompleted = it.currentPlan.plan_records.orEmpty().firstOrNull {
                    it.completed == true && formatPlanDate.isSameDay(
                        Constants.DAY_DATE_FORMAT.parse(
                            it.date
                        )
                    )
                } != null
            )
        }
    }

    override fun onPlanItemClicked(item: Any, view: View) {}

    override fun onItemSelected(item: Any, position: Int) {
        if (item is PlanItem) {
            val date = today.formatDayDate()
            val planRecords = item.currentPlan.plan_records.orEmpty().toMutableList()
            planRecords.add(
                0,
                PlanRecord(
                    date = date,
                    completed = item.isCompleted.not()
                )
            )
            val updatedItem = item.currentPlan.copy(plan_records = planRecords)
            val updatedPlanItem = mapTodoToPlanItem(
                updatedItem,
                item.todos,
                date
            )

            state.todayActivitiesItems.value =
                state.todayActivitiesItems.value?.map { planItem ->
                    if (planItem.id == updatedItem.id) {
                        updatedPlanItem.copy(playAnimation = true)
                    } else {
                        planItem.copy(playAnimation = false)
                    }
                }
            viewModelScope.launch {
                planManager.updatePlanToggle(item.id, date, item.isCompleted.not(), item.currentPlan.program_days?.firstOrNull())
                programManager.markPlanRecordAsCompleted(updatedItem)
            }
        }
    }

    override fun onAddReminderClicked(item: Any, view: View) {
        if (item is PlanItem) {
            state.navigateTo.value = HomeScreenFragmentDirections.actionHomeScreenFragmentPlanTabToPlanReminderDialog(
                PlanReminderHeader(
                    title = item.title,
                    description = item.valueProp1.orEmpty(),
                    backgroundUrl = item.backgroundUrl,
                    background = item.background,
                    todos = item.plans,
                    isTestingPlan = item.type == PlanType.TestPlan
                )
            )
        }
    }

    override fun onAnimationFinished(item: Any) {
        state.todayActivitiesItems.value =
            state.todayActivitiesItems.value?.map { planItem ->
                planItem.copy(playAnimation = false)
            }
    }

    override fun onActivitiesClick() {
        state.navigateTo.value = HomeScreenFragmentDirections.globalActionToBuildPlan()
    }

    fun onSelectGoalClicked() {
        state.navigateTo.value =
            HomeScreenFragmentDirections.actionHomeScreenFragmentResultTabToGoalSelect(
                goToOneGoalAfterFinish = true
            )
    }

    fun onChooseProgramClicked() {
        preferencesRepository.getUserMainGoalId()?.let {
            state.navigateTo.value =
                HomeScreenFragmentDirections.actionHomeScreenFragmentResultTabToProgramSelectionFragment(
                    it
                )
        }
    }

    fun isUserHasAMainGoal() = preferencesRepository.getUserMainGoal() != null

    fun getUserMainGoalId() = preferencesRepository.getUserMainGoalId()

    fun checkUserHasPrograms() {
        today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())
        state.showSelectGoalLoading.value = true
        viewModelScope.launch {
            programManager.getEnrolledPrograms()
                .onSuccess { enrolledPrograms ->
                    state.userEnrolledInPrograms.value = enrolledPrograms.isNotEmpty()
                    state.showSelectGoalLoading.postValue(false)
                    this@WellnessViewModel.enrolledPrograms = enrolledPrograms
                    loadTodayActivities(showLoading = true, forceUpdate = true, today = today)
                }.onError {
                    state.userEnrolledInPrograms.postValue(false)
                    state.showSelectGoalLoading.postValue(false)
                }
        }
    }
}
