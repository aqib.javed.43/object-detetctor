package com.vessel.app.welcomesignup

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.navigation.findNavController
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository

class WelcomeSignUpViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceRepository), ToolbarHandler {
    val welcomeTitle = "Hi ${preferencesRepository.userFirstName}"
    fun onContinueClicked(view: View) {
        view.findNavController()
            .navigate(WelcomeSignUpFragmentDirections.actionWelcomeSignUpToOnboarding())
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
}
