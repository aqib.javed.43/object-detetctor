package com.vessel.app.filterfoodplandialog.model

import androidx.annotation.StringRes
import com.vessel.app.R

enum class FoodCategory(@StringRes val title: Int, val apiName: String) {
    VEGETABLES(R.string.vegetables, "vegetables"),
    FRUIT(R.string.fruits, "fruit"),
    MEAT_FISH(R.string.meat_fish, "meat/fish"),
    NUTS(R.string.nuts, "nuts")
}
