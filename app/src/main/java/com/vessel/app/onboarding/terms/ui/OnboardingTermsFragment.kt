package com.vessel.app.onboarding.terms.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseFragment
import com.vessel.app.onboarding.terms.OnboardingTermsViewModel
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingTermsFragment : BaseFragment<OnboardingTermsViewModel>() {
    override val viewModel: OnboardingTermsViewModel by viewModels()
    override val layoutResId = R.layout.fragment_terms_onboarding

    override fun onViewLoad(savedInstanceState: Bundle?) {
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state {
            observe(closeEvent) {
                findNavController().navigate(OnboardingTermsFragmentDirections.actionOnboardingTermsFragmentToHome())
            }
            observe(navigateTo) { findNavController().navigate(it) }
        }
    }
}
