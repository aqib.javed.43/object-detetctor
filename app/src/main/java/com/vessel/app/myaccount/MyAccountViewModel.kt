package com.vessel.app.myaccount

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.vessel.app.Constants
import com.vessel.app.HomeNavGraphDirections
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.AuthManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import kotlinx.coroutines.launch

class MyAccountViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository,
    private val authManager: AuthManager,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager
) : BaseViewModel(resourceRepository), ToolbarHandler {
    fun onLogOutClicked() {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.LOGOUT_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )

        viewModelScope.launch {
            authManager.logout()
        }
    }

    fun onProfileClicked(view: View) {
        view.findNavController().navigate(R.id.editProfileActivity)
    }

    fun onManageGoalsClicked(view: View) {
        view.findNavController()
            .navigate(MyAccountFragmentDirections.actionAccountToGoalSelect())
    }

    fun onManageDietAndAllergies(view: View) {
        view.findNavController()
            .navigate(MyAccountFragmentDirections.actionAccountToFoodPreferences(getResString(R.string.food_preferences)))
    }

    fun onManageMembershipClicked(view: View) {
        trackingManager.log(
            TrackedEvent.Builder(TrackingConstants.MANAGE_MEMBERSHIP_TAPPED)
                .withKlaviyo()
                .withFirebase()
                .withAmplitude()
                .create()
        )
        showLoading()
        viewModelScope.launch {
            authManager.multipass(Constants.PATH_ACCOUNT)
                .onSuccess {
                    hideLoading(false)
                    view.findNavController()
                        .navigate(HomeNavGraphDirections.globalActionToWeb(it.multipass_url))
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onHttpOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading()
                }
        }
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
}
