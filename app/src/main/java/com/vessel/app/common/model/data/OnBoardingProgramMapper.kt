package com.vessel.app.common.model.data

import com.vessel.app.common.net.data.OnBoardingProgramData
import com.vessel.app.common.net.data.plan.OnBoardingProgramRecord

object OnBoardingProgramMapper {
    fun map(item: OnBoardingProgramData) = OnBoardingProgramRecord(
        item.id
    )
}
