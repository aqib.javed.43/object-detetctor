package com.vessel.app.common.adapter.impacttestresults

import android.view.View
import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil

class ImpactTestReagentAdapter(private val handler: OnActionHandler) :
    BaseAdapterWithDiffUtil<ImpactTestReagentModel>(
        areItemsTheSame = { oldItem, newItem -> oldItem.title == newItem.title }
    ) {
    override fun getLayout(position: Int) = R.layout.item_impact_test_result_reagent

    override fun getHandler(position: Int) = handler

    interface OnActionHandler {
        fun onReagentItemClick(item: ImpactTestReagentModel, view: View)
    }
}
