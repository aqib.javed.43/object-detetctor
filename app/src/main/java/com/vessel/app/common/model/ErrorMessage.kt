package com.vessel.app.common.model

data class ErrorMessage(
    val latest_sample_uuid: String?,
    val message: String?,
    val errors: List<ErrorMessageItem>?
)
