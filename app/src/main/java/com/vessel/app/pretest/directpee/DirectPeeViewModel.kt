package com.vessel.app.pretest.directpee

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.pretest.directpee.ui.DirectPeeFragmentDirections
import com.vessel.app.util.ResourceRepository

class DirectPeeViewModel @ViewModelInject constructor(
    val state: DirectPeeState,
    resourceProvider: ResourceRepository,
    val preferencesRepository: PreferencesRepository
) : BaseViewModel(resourceProvider), ToolbarHandler {

    fun onContinueClicked() {
        state.navigateTo.value = DirectPeeFragmentDirections.actionDirectPeeFragmentToTakeTestActivity()
    }

    fun dontShowChecked(dontShowAgain: Boolean) {
        preferencesRepository.skipPreTestTips = dontShowAgain
    }

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }
}
