package com.vessel.app.onboarding.ui

import androidx.lifecycle.LifecycleOwner
import com.vessel.app.base.BaseAdapter
import com.vessel.app.base.MutableLiveArrayList

class OnboardingAdapter(
    items: MutableLiveArrayList<OnboardingPage>,
    lifecycleOwner: LifecycleOwner
) : BaseAdapter<BaseAdapter.ViewHolder>(items, lifecycleOwner) {
    override fun getLayout(position: Int) = (items[position] as OnboardingPage).layoutResId
}
