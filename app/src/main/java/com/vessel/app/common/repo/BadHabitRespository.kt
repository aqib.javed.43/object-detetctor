package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.Constants
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.data.BadHabitMapper
import com.vessel.app.wellness.net.service.BadHabitService
import javax.inject.Inject

class BadHabitRespository @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val badHabitService: BadHabitService,
) : BaseNetworkRepository(moshi, loggingManager) {

    suspend fun getAllBadHabits(subGoalId: String?) = executeWithErrorMessage {
        badHabitService.getAllBadHabits(subGoalId).bad_habits.map {
            BadHabitMapper.map(it)
        }.also {
            Constants.BADHABIT = it
        }
    }
}
