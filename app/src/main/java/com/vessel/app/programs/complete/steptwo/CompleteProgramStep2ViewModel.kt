package com.vessel.app.programs.complete.steptwo

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.vessel.app.Constants
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ProgramManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Program
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ProgramCardWidgetOnActionHandler
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.homescreen.MainPagerFragmentTab
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import com.vessel.app.wellness.net.data.DifficultyLevel
import com.vessel.app.wellness.net.data.ProgramAction
import kotlinx.coroutines.launch
import java.util.*

class CompleteProgramStep2ViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    resourceProvider: ResourceRepository,
    private val programManager: ProgramManager,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
) : BaseViewModel(resourceProvider),
    ToolbarHandler,
    ProgramCardWidgetOnActionHandler {

    val state = CompleteProgramStep2State(
        savedStateHandle.get<Program>("program")!!,
        savedStateHandle.get<String>("feedback")
    )

    init {
        loadSuggestedPrograms()
    }

    private fun loadSuggestedPrograms() {
        showLoading()
        viewModelScope.launch {
            programManager.getProgramsByGoal(state.passedProgram.mainGoalId)
                .onSuccess {
                    val completedPercent = state.passedProgram.getCompletedItemPercent()
                    var suggestedPrograms = it.program.filter { it.isEnrolled.not() }
                    if (completedPercent > 75 && state.passedProgram.difficulty == DifficultyLevel.EASY) {
                        // keep the medium and hard programs
                        suggestedPrograms =
                            suggestedPrograms.filterNot { it.difficulty == DifficultyLevel.EASY }
                    } else if (completedPercent < 50 && state.passedProgram.difficulty == DifficultyLevel.HARD) {
                        // keep the easy programs
                        suggestedPrograms =
                            suggestedPrograms.filter { it.difficulty == DifficultyLevel.EASY }
                    }
                    state.suggestedPrograms.value = suggestedPrograms
                    hideLoading(false)
                }
                .onNetworkIOError {
                    showError(getResString(R.string.network_error))
                }
                .onServiceOrUnknownError {
                    showError(getResString(R.string.unknown_error))
                }
                .onError {
                    hideLoading(false)
                }
        }
    }

    fun onRejoinProgramClicked() {
        enrollProgram()
    }

    fun onCloseClicked() {
        state.navigateTo.value = CompleteProgramStep2FragmentDirections.globalActionToWellness(
            MainPagerFragmentTab.RESULT
        )
    }
    private fun enrollProgram() {
        state {
            viewModelScope.launch {
                showLoading()
                program.value?.let {
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PROGRAM_ENROLLED)
                            .addProperty(TrackingConstants.KEY_ID, it.id.toString())
                            .addProperty(TrackingConstants.ENROLL_TYPE, ProgramAction.KEEP.value)
                            .addProperty(TrackingConstants.KEY_LOCATION, Constants.CONGRATULATION_SCREEN)
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                    programManager.joinProgram(it, ProgramAction.KEEP)
                        .onSuccess {
                            hideLoading()
                            state.navigateTo.value = CompleteProgramStep2FragmentDirections.actionCompleteProgramStep2FragmentToSuccessfullyJoinDialogFragment(
                                true
                            )
                        }
                        .onServiceError {
                            showError(it.error.message ?: getResString(R.string.unknown_error))
                        }
                        .onNetworkIOError {
                            showError(getResString(R.string.network_error))
                        }
                        .onUnknownError {
                            showError(getResString(R.string.unknown_error))
                        }
                        .onError {
                            hideLoading()
                        }
                    trackingManager.log(
                        TrackedEvent.Builder(TrackingConstants.PROGRAM_GOAL_SELECTED)
                            .addProperty("program name", it.title)
                            .addProperty("time", Calendar.getInstance().time.formatDayDate())
                            .setEmail(preferencesRepository.getContact()?.email ?: "")
                            .withKlaviyo()
                            .withFirebase()
                            .withAmplitude()
                            .create()
                    )
                }
            }
        }
    }

    override fun onProgramItemClicked(program: Program) {
        state.navigateTo.value =
            CompleteProgramStep2FragmentDirections.globalActionToProgramDetailsFragment(program)
    }

    override fun onProgramInfoClicked(program: Program) {
        state.navigateTo.value =
            CompleteProgramStep2FragmentDirections.globalActionToProgramInfoDialogFragment(
                program
            )
    }

    override fun onJoinProgramClicked(program: Program) {
        if (program.isEnrolled.not()) {
            state.navigateTo.value =
                CompleteProgramStep2FragmentDirections.globalActionToJoinProgramFragment(
                    program,
                    Constants.COMPLETE_PROGRAM_PAGE
                )
        } else {
            onProgramItemClicked(program)
        }
    }

    override fun onBackButtonClicked(view: View) {
        state.navigateTo.value = CompleteProgramStep2FragmentDirections.globalActionToWellness(
            MainPagerFragmentTab.RESULT
        )
    }
}
