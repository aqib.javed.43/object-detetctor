# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

-printusage build/outputs/usage.txt

# these two lines will make your app hit to breakpoints in fragments. otherwise it may baypass fragments
-keep class * extends androidx.fragment.app.Fragment

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#noinspection ShrinkerUnresolvedReference
# this is used in the shared prefs library. It's part of the AndroidX security lib
-keep class * extends com.google.protobuf.GeneratedMessageLite { *; }

# inner data classes
-keep class com.vessel.app.common.model.Contact$Height
-keep class com.vessel.app.common.model.Contact$Weight

# proguard/R8 strips out Kotlin reflection metadata, so the function forceClear in PreferencesRepository with the following code
# this::class.memberProperties would not work
-keep class kotlin.Metadata { *; }

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-keep class com.github.mikephil.charting.** { *; }

-keepnames class * implements android.os.Parcelable
-keepnames class * implements java.io.Serializable
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
