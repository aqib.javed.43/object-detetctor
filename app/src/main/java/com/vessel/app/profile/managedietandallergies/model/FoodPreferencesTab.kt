package com.vessel.app.profile.managedietandallergies.model

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.vessel.app.R

data class FoodPreferencesPage(
    val FoodPreferencesSelect: MutableLiveData<List<FoodPreferencesSelect>>
)

enum class FoodPreferencesTab(val position: Int, @StringRes val title: Int) {
    DIETS(0, R.string.diet),
    ALLERGIES(1, R.string.allergies)
}
