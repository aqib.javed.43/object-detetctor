package com.vessel.app.ownplanitem

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class CreateOwnPlanItemViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository
) : BaseViewModel(resourceRepository)
