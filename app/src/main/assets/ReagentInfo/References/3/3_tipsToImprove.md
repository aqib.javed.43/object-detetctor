If you’re not trying to follow a ketogenic diet, you can skip this section because you do not need to improve your urine ketone levels.

However, if you want to be in a state of nutritional ketosis, the best way is through changing your diet.

Start by calculating your “macros” which are the amounts of different macronutrients (fats, proteins, carbs) to consume each day based on your goals. The easiest way to do this is to use a free online macro calculator such as [https://perfectketo.com/keto-macro-calculator/](https://perfectketo.com/keto-macro-calculator/)

In general, ketogenic diets consist of about 75% fat, 20% protein, and 5% carbs. Most sources recommend having a maximum of 20g net carbs per day. Net carbs are the total carbs minus the fiber (minus sugar alcohols if applicable). For example, 1 cup of chopped bell pepper has 9 grams of carbs and 3 grams of fiber. The net carbs would be 6 grams in this case. Sugar alcohols are sometimes used in keto recipes to add sweetness without carbs. One of the best is erythritol, a sugar alcohol that has no carbs and doesn’t affect sugar levels.

When you first transition into ketosis you’re likely to experience what’s called “keto flu” which can have you feeling tired, dizzy, nauseated, and with brain fog. This can last anywhere from a couple of days to a couple of weeks. After you get through this, you may start to experience the benefits that most people report with nutritional ketoses such as more energy, a clearer, and sharper mind, the absence of sugar and carb cravings, and fat loss.

When you test, it’s important to be consistent about when you test each day. Ideally, you’d test two hours after waking up (while fasted) to get your baseline, and again 2 hours after meals.
