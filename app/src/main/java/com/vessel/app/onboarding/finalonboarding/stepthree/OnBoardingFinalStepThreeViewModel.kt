package com.vessel.app.onboarding.finalonboarding.stepthree

import android.view.View
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.vessel.app.R
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.ContactManager
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.model.Contact
import com.vessel.app.common.repo.ContactRepository
import com.vessel.app.common.repo.DevicePreferencesRepository
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.tracking.TrackedEvent
import com.vessel.app.common.tracking.TrackingConstants
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.util.extensions.formatDayDate
import kotlinx.coroutines.launch
import java.util.*

class OnBoardingFinalStepThreeViewModel @ViewModelInject constructor(
    resourceProvider: ResourceRepository,
    private val devicePreferencesRepository: DevicePreferencesRepository,
    private val preferencesRepository: PreferencesRepository,
    private val trackingManager: TrackingManager,
    private val contactManager: ContactManager
) : BaseViewModel(resourceProvider),
    ToolbarHandler {
    val bgColor = if (preferencesRepository.isExistingUserFirstTime) R.color.beige_color else R.color.primaryLight
    val state = OnBoardingFinalStepThreeState()

    override fun onBackButtonClicked(view: View) {
        navigateBack()
    }

    fun onDoneClicked() {
        if (preferencesRepository.isExistingUserFirstTime) {
            devicePreferencesRepository.showPlanReadyHint = true
            preferencesRepository.isExistingUserFirstTime = false
            state.backToHome.call()
        } else {
            val userGender = preferencesRepository.userGenderTemp!!
            val userWeight = Contact.Weight(preferencesRepository.userWeightTemp)
            val userHeight = Contact.Height.fromCentimeters(preferencesRepository.userHeightTemp)
            val userBirthDate = preferencesRepository.userBirthDateTemp!!
            showLoading()
            viewModelScope.launch {
                contactManager.updateContact(
                    gender = userGender,
                    weight = userWeight,
                    height = userHeight,
                    birthDate = ContactRepository.SERVER_DATE_FORMAT.parse(userBirthDate)
                )
                    .onSuccess {
                        hideLoading(false)
                        preferencesRepository.onboardingCompleted = true
                        devicePreferencesRepository.showPlanReadyHint = true
                        navigate(OnBoardingFinalStepThreeState.NavigationDestination.HOME)
                    }
                    .onServiceError {
                        showError(it.error.message ?: getResString(R.string.unknown_error))
                    }
                    .onNetworkIOError {
                        showError(getResString(R.string.network_error))
                    }
                    .onUnknownError {
                        showError(getResString(R.string.unknown_error))
                    }

                hideLoading()
            }
            trackingManager.log(
                TrackedEvent.Builder(TrackingConstants.CHECKIN_COMPLETED)
                    .addProperty("program name", preferencesRepository.getUserMainGoal()?.name.toString())
                    .addProperty("time", Calendar.getInstance().time.formatDayDate())
                    .setEmail(preferencesRepository.getContact()?.email ?: "")
                    .withKlaviyo()
                    .withFirebase()
                    .withAmplitude()
                    .create()
            )
        }
    }
    fun navigate(destination: OnBoardingFinalStepThreeState.NavigationDestination) {
        state.navigateTo.value = destination
    }
}
