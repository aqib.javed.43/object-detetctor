package com.vessel.app.common.repo

import com.squareup.moshi.Moshi
import com.vessel.app.Constants
import com.vessel.app.common.manager.LoggingManager
import com.vessel.app.common.model.data.OnBoardingProgramMapper
import com.vessel.app.wellness.net.service.OnBoardingProgramService
import javax.inject.Inject

class OnBoardingProgramRepository @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    moshi: Moshi,
    loggingManager: LoggingManager,
    private val onBoardingProgramService: OnBoardingProgramService,
) : BaseNetworkRepository(moshi, loggingManager) {

    suspend fun getOnBoardingProgram(goalId: Int?) = executeWithErrorMessage {
        onBoardingProgramService.getOnBoardingProgram(goalId).program.map {
            OnBoardingProgramMapper.map(it)
        }.also {
            Constants.ONBOARDINGPROGRAM = it
        }
    }
}
