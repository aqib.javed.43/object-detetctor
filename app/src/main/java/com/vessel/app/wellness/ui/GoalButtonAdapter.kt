package com.vessel.app.wellness.ui

import com.vessel.app.R
import com.vessel.app.base.BaseAdapterWithDiffUtil
import com.vessel.app.wellness.model.GoalButton

class GoalButtonAdapter(
    private val onActionHandler: OnActionHandler
) : BaseAdapterWithDiffUtil<GoalButton>(
    areItemsTheSame = { oldItem, newItem -> oldItem.id == newItem.id }
) {
    override fun getLayout(position: Int) = R.layout.item_goal_button

    override fun getHandler(position: Int) = onActionHandler

    interface OnActionHandler {
        fun onGoalButtonClick(item: GoalButton)
        fun onGoalEntryClicked(entryPosition: Int)
        fun onGoalButtonMoved(oldPosition: Int, newPosition: Int)
        fun onInfoButtonClick(entryPosition: Int)
    }
}
