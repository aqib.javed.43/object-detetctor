package com.vessel.app.appreview.model

import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class ReviewState @Inject constructor() {
    val openMessaging = LiveEvent<Any>()
    val videoUrl = LiveEvent<String>()
    val isPlayingVideo = LiveEvent<Boolean>()
    val openTips = LiveEvent<String>()
    val onCalling = LiveEvent<String>()
    operator fun invoke(block: ReviewState.() -> Unit) = apply(block)
}
