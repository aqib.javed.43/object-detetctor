package com.vessel.app.wellness.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class RecommendedNutrient(
    val id: Int,
    val reagent_id: Int? = null,
    val food_id: Int? = null,
    val serving_grams: Double? = null,
    val name: String? = null,
    val quantity: Double? = null,
) : Parcelable
