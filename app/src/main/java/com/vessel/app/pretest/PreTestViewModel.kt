package com.vessel.app.pretest

import androidx.hilt.lifecycle.ViewModelInject
import com.vessel.app.base.BaseViewModel
import com.vessel.app.util.ResourceRepository

class PreTestViewModel @ViewModelInject constructor(
    resourceRepository: ResourceRepository
) : BaseViewModel(resourceRepository)
