package com.vessel.app.pollpreview

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.vessel.app.base.BaseViewModel
import com.vessel.app.common.manager.TrackingManager
import com.vessel.app.common.repo.PreferencesRepository
import com.vessel.app.common.repo.TeamsRepository
import com.vessel.app.common.widget.ToolbarHandler
import com.vessel.app.util.ResourceRepository
import com.vessel.app.wellness.net.data.CreatePollRequest
import com.vessel.app.wellness.net.data.PollQuestions
import kotlinx.coroutines.launch

class PollPreviewViewModel@ViewModelInject constructor(
    val state: PollPreviewState,
    val resourceProvider: ResourceRepository,
    private val teamsRepository: TeamsRepository,
    private val trackingManager: TrackingManager,
    private val preferencesRepository: PreferencesRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel(resourceProvider), ToolbarHandler {
    init {
    }
    override fun onBackButtonClicked(view: View) {
        view.findNavController().navigateUp()
    }
    // Create Poll
    fun createPoll(pollQuestions: PollQuestions) {
        val moshi: Moshi = Moshi.Builder().build()
        val adapter: JsonAdapter<PollQuestions> = moshi.adapter(PollQuestions::class.java)
        val pollSchema = adapter.toJson(pollQuestions)
        val goalId = preferencesRepository.getUserMainGoalId()
        goalId?.let {
            val createPollRequest = CreatePollRequest()
            createPollRequest.actor = "user:${preferencesRepository.contactId}"
            createPollRequest.poll = pollSchema
            viewModelScope.launch {
                showLoading()
                teamsRepository.createPoll(it, createPollRequest)
                    .onSuccess { result ->
                        hideLoading()
                    }
                    .onError {
                        hideLoading()
                        showError(it.toString())
                    }
            }
        }
    }
}
