package com.vessel.app.supplement.model

data class MembershipAddress(
    val id: Int,
    val customer_id: Int,
    val address1: String,
    val address2: String?,
    val city: String,
    val country: String,
    val created_at: String,
    val first_name: String,
    val last_name: String,
    val province: String,
    val zip: String,
) {
    fun getAddressInformation() = "$first_name $last_name, ${listOfNotNull(address1, address2).joinToString(",")}\n$city, $province $zip"
}
