package com.vessel.app.util.extensions

import com.vessel.app.Constants
import com.vessel.app.Constants.WEEK_DAYS
import com.vessel.app.views.weekdays.getCalendarDayByIndex
import java.util.*
import java.util.concurrent.TimeUnit

fun Date.isSameDay(newDate: Date): Boolean {
    val calendar1 = Calendar.getInstance().also { it.time = this }
    val calendar2 = Calendar.getInstance().also { it.time = newDate }
    return calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
        calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
        calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)
}

fun Date.formatPlanDate(): String {
    val parsedDate = Constants.PLAN_DAY_DATE_FORMAT.format(this)
    val calendar = Calendar.getInstance().apply { time = this@formatPlanDate }
    val dayNumberSuffix: String = getDayNumberSuffix(calendar.get(Calendar.DAY_OF_MONTH))
    return "$parsedDate$dayNumberSuffix"
}
fun Date.formatLessonDayDate() = Constants.LESSON_DATE_FORMAT.format(this)
fun Date.formatDayDate() = Constants.DAY_DATE_FORMAT.format(this)
fun Date.formatMonthYearDate() = Constants.MONTH_YEAR_DATE_FORMAT.format(this)
fun Date.formatDayDateWithSecond() = Constants.CAPTURE_DATE_FORMAT.format(this)
fun Date.formatDayName() = Constants.DAY_NAME_FORMAT.format(this)
fun Date.formatDayMonthName() = Constants.DAY_MONTH_NAME_FORMAT.format(this)

private fun getDayNumberSuffix(day: Int): String {
    return if (day in 11..13) {
        "th"
    } else when (day % 10) {
        1 -> "st"
        2 -> "nd"
        3 -> "rd"
        else -> "th"
    }
}

fun getCurrentWeekDates(): Pair<Date, Date> {
    val calendar = Calendar.getInstance()
    calendar.time = Date()
    calendar.firstDayOfWeek = Calendar.MONDAY // Set the starting day of the week
    calendar.set(
        Calendar.DAY_OF_WEEK,
        Calendar.MONDAY
    ) // Pass whatever day you want to get inplace of `MONDAY`
    val startDate = calendar.time
    calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
    val endDate = calendar.time

    return Pair(startDate, endDate)
}

fun Int.toCurrentWeekDate(): Date {
    val calendar = Calendar.getInstance()
    calendar.time = Date()
    calendar.firstDayOfWeek = Calendar.MONDAY // Set the starting day of the week
    calendar.set(
        Calendar.DAY_OF_WEEK,
        getCalendarDayByIndex(this)
    )
    return calendar.time
}

fun Date.toCalender(): Calendar {
    val calender = Calendar.getInstance(Locale.US)
    calender.timeInMillis = time
    return calender
}
fun Date.getDateBefore(days: Int): Date {
    val calender = Calendar.getInstance(Locale.US)
    calender.add(Calendar.DAY_OF_YEAR, -days)
    return calender.time
}
fun Date.getDateAfter(days: Int): Date {
    val calender = Calendar.getInstance(Locale.US)
    calender.add(Calendar.DAY_OF_YEAR, days)
    return calender.time
}
fun Date.getDateDayDiff(date: Date): Int {
    val diff: Long = date.time - time
    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toInt()
}
fun Date.getListDates(): List<Date> {
    val splitDays = mutableListOf<Date>()
    val pastLimitCalender = Calendar.getInstance()
    pastLimitCalender.add(Calendar.DATE, -7)
    val today = Constants.DAY_DATE_FORMAT.parse(Calendar.getInstance().time.formatDayDate())!!
    val pastLimitDate =
        Constants.DAY_DATE_FORMAT.parse(pastLimitCalender.time.formatDayDate())

    WEEK_DAYS.onEach {
        val date = Constants.DAY_DATE_FORMAT.parse(it.toCurrentWeekDate().formatDayDate())
        date?.let {
            if (date.isSameDay(today) || (date.before(today) && date.after(pastLimitDate)))
                splitDays.add(date)
            val previousWeekOccurrence = date.toCalender()
            previousWeekOccurrence.add(Calendar.DATE, -7)
            val previousOccurrenceDate = previousWeekOccurrence.time
            if (previousOccurrenceDate.after(pastLimitDate))
                splitDays.add(Constants.DAY_DATE_FORMAT.parse(previousOccurrenceDate.formatDayDate()))
        }
    }
    return splitDays
}
