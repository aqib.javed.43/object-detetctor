package com.vessel.app.common.manager

import com.vessel.app.common.repo.GoalsRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GoalsManager @Inject constructor(
    private val goalsRepository: GoalsRepository
) {
    suspend fun fetchAllGoals() = goalsRepository.getAllGoals()
}
