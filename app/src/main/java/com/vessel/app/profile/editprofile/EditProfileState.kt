package com.vessel.app.profile.editprofile

import androidx.lifecycle.MutableLiveData
import com.vessel.app.R
import com.vessel.app.util.LiveEvent
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class EditProfileState @Inject constructor() {
    val userFirstName = MutableLiveData("")
    val userLastName = MutableLiveData("")
    val userEmail = MutableLiveData("")
    val gender = MutableLiveData(R.id.radioMale)
    val userHeight = MutableLiveData("")
    val userWeight = MutableLiveData("")
    val userBirthday = MutableLiveData("")
    val birthdayDateFormat = SimpleDateFormat("MM/dd/yy", Locale.ENGLISH)
    val dismissView = LiveEvent<Unit>()
    val profilePhoto = MutableLiveData("")
    val reason = MutableLiveData("")

    operator fun invoke(block: EditProfileState.() -> Unit) = apply(block)
}
