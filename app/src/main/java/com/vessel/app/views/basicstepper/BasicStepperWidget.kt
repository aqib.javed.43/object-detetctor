package com.vessel.app.views.basicstepper

import android.content.Context
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.IntRange
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import androidx.core.view.isVisible
import com.vessel.app.R
import com.vessel.app.databinding.WidgetBasicStepperBinding
import com.vessel.app.util.extensions.afterMeasured
import com.vessel.app.views.stepper.StepperUiModel

class BasicStepperWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private lateinit var stepsList: MutableList<StepperUiModel>
    private lateinit var binding: WidgetBasicStepperBinding
    private lateinit var adapterBasic: BasicStepperAdapter
    private var stepsCount = 0
    private var activePosition = 0
    private var inActiveIcon = R.drawable.ic_step_inactive

    init {
        initView(context)
        obtainStyledAttributes(context, attrs, defStyleAttr)
    }

    private fun obtainStyledAttributes(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        context.withStyledAttributes(
            set = attrs,
            attrs = R.styleable.StepperView,
            defStyleAttr = defStyleAttr
        ) {
            inActiveIcon = getResourceId(R.styleable.StepperView_inActiveIcon, R.drawable.ic_step_inactive)
        }
    }

    private fun initView(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        binding = WidgetBasicStepperBinding.inflate(layoutInflater, this, true)

        adapterBasic = BasicStepperAdapter()
        binding.recyclerView.adapter = adapterBasic
    }

    private fun calculateItemWidth(screenWidth: Int): Int {
        val stepsCount = if (stepsList.size == 0) {
            1
        } else {
            stepsList.size
        }

        return screenWidth / stepsCount
    }

    fun setStepperAsCompleted() {
        stepsList.forEach {
            it.isCompleted = true
        }
        adapterBasic.notifyDataSetChanged()
    }

    fun getStepCount() = stepsCount
    fun setStepsCount(@IntRange(from = 1) stepsCount: Int, activePosition: Int) {
        stepsList = mutableListOf()
        this.stepsCount = stepsCount
        createStepsList(stepsCount, activePosition)
    }

    fun getActiveStep() = activePosition
    fun setActiveStep(activePosition: Int) {
        this.activePosition = activePosition
        stepsList.forEachIndexed { index, item ->
            item.isCompleted = index <= activePosition - 1
        }
        adapterBasic.notifyDataSetChanged()
        setProgress(adapterBasic.itemWidth.times(activePosition))
    }

    private fun setProgress(progress: Int) {
        android.os.Handler(Looper.getMainLooper()).post {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                binding.progress.setProgress(progress, true)
            } else binding.progress.progress = progress
        }
    }

    private fun createStepsList(stepsCount: Int, activePosition: Int) {
        stepsList = mutableListOf()

        for (position in 0 until stepsCount) {
            stepsList.add(
                StepperUiModel(
                    stepNum = (position).toString(),
                    isCompleted = activePosition == position,
                    inActiveIcon = inActiveIcon
                )
            )
        }

        afterMeasured {
            val screenWidth = binding.recyclerView.measuredWidth
            binding.progress.max = screenWidth
            adapterBasic = BasicStepperAdapter()
            binding.recyclerView.adapter = adapterBasic

            val itemWidth = calculateItemWidth(screenWidth)
            adapterBasic.setItemWidth(itemWidth, stepsList.size)
            adapterBasic.submitList(stepsList)
            adapterBasic.notifyDataSetChanged()
            binding.recyclerView.isVisible = stepsList.size <= MAX_STEPS_COUNT
            setProgress(itemWidth.times(activePosition))
        }
    }

    companion object {
        private const val MAX_STEPS_COUNT = 10
    }
}
