package com.vessel.app.common.binding

import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.BulletSpan
import android.text.style.UnderlineSpan
import android.util.TypedValue
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.core.text.HtmlCompat.FROM_HTML_OPTION_USE_CSS_COLORS
import androidx.databinding.BindingAdapter
import com.vessel.app.R

@BindingAdapter("textColor")
fun TextView.setTextColorResId(@ColorRes textColorResId: Int) {
    setTextColor(ContextCompat.getColor(context, textColorResId))
}

@BindingAdapter("android:textSize")
fun TextView.setTextSize(@DimenRes textSizeRes: Int) {
    setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(textSizeRes))
}

@BindingAdapter("textRes")
fun TextView.setTextRes(@StringRes textRes: Int?) {
    textRes?.let { setText(textRes) }
}

@BindingAdapter("textHtml")
fun TextView.setTextHtml(texthtml: String?) {
    texthtml?.let { setText(HtmlCompat.fromHtml(it, FROM_HTML_OPTION_USE_CSS_COLORS), TextView.BufferType.SPANNABLE) }
}

@BindingAdapter(value = ["bulletSpan", "bulletGapWidth"], requireAll = false)
fun TextView.setBulletSpan(resolvedText: String?, bulletGapWidth: Float?) {
    resolvedText?.let {
        val gapWidth = bulletGapWidth?.toInt()
            ?: context.resources.getDimensionPixelSize(R.dimen.spacing_normal)
        text = SpannableString(resolvedText)
            .apply {
                setSpan(
                    BulletSpan(gapWidth),
                    0,
                    it.length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE
                )
            }
    }
}

@BindingAdapter(value = ["bulletSpan", "bulletGapWidth"], requireAll = false)
fun TextView.setBulletSpan(@StringRes textRes: Int?, bulletGapWidth: Float?) {
    textRes?.let { setBulletSpan(context.getString(textRes), bulletGapWidth) }
}

@BindingAdapter("underlineSpan")
fun TextView.setUnderlineSpan(resolvedText: String?) {
    resolvedText?.let {
        text = SpannableString(resolvedText).apply {
            setSpan(UnderlineSpan(), 0, resolvedText.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
}

@BindingAdapter("underlineSpan")
fun TextView.setUnderlineSpan(resolvedText: Int?) {
    resolvedText?.let {
        val value = context.getString(resolvedText)
        text = SpannableString(value).apply {
            setSpan(UnderlineSpan(), 0, value.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
}

@BindingAdapter(value = ["templateRes", "textAddedToTemplateRes"], requireAll = false)
fun TextView.setFormattedTextRes(templateRes: String?, textAddedToTemplateRes: String?) {
    templateRes?.let {
        textAddedToTemplateRes?.let {
            val text = templateRes.format(textAddedToTemplateRes)
            setText(text)
        }
    }
}

@BindingAdapter("setLinkText")
fun TextView.setLinkText(resolvedText: SpannableString?) {
    resolvedText?.let {
        text = resolvedText
    }
    this.movementMethod = LinkMovementMethod.getInstance()
}
