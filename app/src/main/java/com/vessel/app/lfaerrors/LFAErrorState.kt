package com.vessel.app.lfaerrors

import android.text.SpannableString
import androidx.lifecycle.MutableLiveData
import com.vessel.app.util.LiveEvent
import javax.inject.Inject

class LFAErrorState @Inject constructor() {
    val title = MutableLiveData<String>()
    val reagents = MutableLiveData<SpannableString>()
    val failedReagentsImage = MutableLiveData<Int>()
    val goToPostScreen = LiveEvent<Any>()
    val goToErrorHintDialog = LiveEvent<Any>()
    val navigateToCapture = LiveEvent<Any>()

    operator fun invoke(block: LFAErrorState.() -> Unit) = apply(block)
}
