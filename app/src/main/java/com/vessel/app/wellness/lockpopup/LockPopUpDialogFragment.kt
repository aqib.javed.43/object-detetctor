package com.vessel.app.wellness.lockpopup

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vessel.app.R
import com.vessel.app.base.BaseDialogFragment
import com.vessel.app.util.extensions.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LockPopUpDialogFragment : BaseDialogFragment<LockPopUpViewModel>() {
    override val viewModel: LockPopUpViewModel by viewModels()
    override val layoutResId = R.layout.fragment_app_result_lock_dialog

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.apply {
            val inset = InsetDrawable(
                ColorDrawable(Color.TRANSPARENT),
                resources.getDimension(R.dimen.spacing_2xl).toInt()
            )
            setLayout(width, height)
            setBackgroundDrawable(inset)
        }
        isCancelable = false
        observe(viewModel.dismissDialog) { dismiss() }
        observe(viewModel.state.navigateTo) {
            findNavController().navigate(it)
        }
    }
}
