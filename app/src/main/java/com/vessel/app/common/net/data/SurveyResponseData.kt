package com.vessel.app.common.net.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SurveyResponseData(
    val response: List<SurveyResponseAnswerData>
)
